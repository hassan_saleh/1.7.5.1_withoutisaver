function addWidgetsfrmCardsAddNickname() {
    frmCardsAddNickname.setDefaultUnit(kony.flex.DP);
    var flxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "flexTransparent",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var flxSettingsHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxSettingsHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "CopyslFbox0f07559cdcd2e4e",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSettingsHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_b7bdbafdffb441bbba0a434b709527db,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 4
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblBack0i5985fbe3b634e",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
    var flxTitle = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxTitle",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "65%",
        "zIndex": 2
    }, {}, {});
    flxTitle.setDefaultUnit(kony.flex.DP);
    var lblSettingsTitle = new kony.ui.Label({
        "centerX": "44%",
        "centerY": "50%",
        "id": "lblSettingsTitle",
        "isVisible": false,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.settings.settings"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccNumber = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "45%",
        "height": "90%",
        "id": "lblAccNumber",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": "Settings",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTitle.add(lblSettingsTitle, lblAccNumber);
    flxSettingsHeader.add(flxBack, flxTitle);
    var flxScrollBody = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0%",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "flxScrollBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknflxScrollBlue",
        "top": "9%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScrollBody.setDefaultUnit(kony.flex.DP);
    var lblAddNickNameDesc = new kony.ui.Label({
        "id": "lblAddNickNameDesc",
        "isVisible": true,
        "left": "5%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.cardsNicknameDesc"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxDescrip = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "15%",
        "id": "flxDescrip",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDescrip.setDefaultUnit(kony.flex.DP);
    var lblAddNickName = new kony.ui.Label({
        "id": "lblAddNickName",
        "isVisible": true,
        "left": "2%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.settings.addNicknamelbl"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtAddNickName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "45%",
        "id": "txtAddNickName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": 25,
        "onDone": AS_TextField_d65e49998c22433fad7720e1cba26e38,
        "onTextChange": AS_TextField_c75dda4f9cd84f8ca0d8afc4ba4f4f1d,
        "onTouchEnd": AS_TextField_e79b162b152a4257b073d5b7695572d3,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "40%",
        "width": "90%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "onEndEditing": AS_TextField_i7a2f776d88e44a8a0d4e6e04ab13688,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderDescription = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderDescription",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "skntextFieldDividerJomoPay",
        "width": "98%",
        "zIndex": 1
    }, {}, {});
    flxBorderDescription.setDefaultUnit(kony.flex.DP);
    flxBorderDescription.add();
    var btnEdit = new kony.ui.Button({
        "focusSkin": "sknBtnContactList",
        "height": "45%",
        "id": "btnEdit",
        "isVisible": true,
        "left": "85%",
        "onClick": AS_Button_hda4283d0635405190eeb22e41aede9d,
        "skin": "sknBtnContactList",
        "text": "0",
        "top": "40%",
        "width": "15%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxDescrip.add(lblAddNickName, txtAddNickName, flxBorderDescription, btnEdit);
    var flxShowHide = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxShowHide",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "flxBgBlueGradientRound8",
        "top": "3%",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxShowHide.setDefaultUnit(kony.flex.DP);
    var lblDeviceTitle1 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblDeviceTitle1",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.cardHiderShow"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxShowHideSwitchOff = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxShowHideSwitchOff",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_f23ebe8f000d4911958470cdc85a7af4,
        "right": "5%",
        "skin": "sknflxGrey",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxShowHideSwitchOff.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0a5e3eddfada24b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0a5e3eddfada24b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlueOff0a5e3eddfada24b.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0a5e3eddfada24b.add();
    var CopyflxNaveenbhaiKiLakeer0cb72155c61834c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0cb72155c61834c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 10,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0cb72155c61834c.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0cb72155c61834c.add();
    flxShowHideSwitchOff.add(CopyflxRoundDBlueOff0a5e3eddfada24b, CopyflxNaveenbhaiKiLakeer0cb72155c61834c);
    var flxShowHideSwitchOn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxShowHideSwitchOn",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_ae39b8bed2ad4dd6a2580303cc28af22,
        "right": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxShowHideSwitchOn.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0bbc73e3c0fb041 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0bbc73e3c0fb041",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0bbc73e3c0fb041.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0bbc73e3c0fb041.add();
    var Copyflxlakeer0h4a3ac20cf6948 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0h4a3ac20cf6948",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "isModalContainer": false,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0h4a3ac20cf6948.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0h4a3ac20cf6948.add();
    flxShowHideSwitchOn.add(CopyflxRoundDBlue0bbc73e3c0fb041, Copyflxlakeer0h4a3ac20cf6948);
    flxShowHide.add(lblDeviceTitle1, flxShowHideSwitchOff, flxShowHideSwitchOn);
    var flxAcc2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "12%",
        "id": "flxAcc2",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "onTouchStart": AS_FlexContainer_fddad76257964cc5b495f1d9848275f6,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAcc2.setDefaultUnit(kony.flex.DP);
    var lblAccountName2 = new kony.ui.Label({
        "bottom": "0%",
        "centerY": "50%",
        "id": "lblAccountName2",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.AccountNickName.subscribeeStatment"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxeStatmentSwitchOn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxeStatmentSwitchOn",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_c829900104934d019c545caee7d61292,
        "right": "0%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxeStatmentSwitchOn.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0d18c1b914b7d4d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0d18c1b914b7d4d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0d18c1b914b7d4d.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0d18c1b914b7d4d.add();
    var Copyflxlakeer0gcb3103ba69548 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0gcb3103ba69548",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "isModalContainer": false,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0gcb3103ba69548.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0gcb3103ba69548.add();
    flxeStatmentSwitchOn.add(CopyflxRoundDBlue0d18c1b914b7d4d, Copyflxlakeer0gcb3103ba69548);
    var flxeStatmentSwitchOff = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxeStatmentSwitchOff",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_e24f414930d2414da8393909a11202cc,
        "right": "0%",
        "skin": "sknflxGrey",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxeStatmentSwitchOff.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0a5ff28307ed44e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0a5ff28307ed44e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlueOff0a5ff28307ed44e.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0a5ff28307ed44e.add();
    var CopyflxNaveenbhaiKiLakeer0def2e0e4e6324d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0def2e0e4e6324d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 10,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0def2e0e4e6324d.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0def2e0e4e6324d.add();
    flxeStatmentSwitchOff.add(CopyflxRoundDBlueOff0a5ff28307ed44e, CopyflxNaveenbhaiKiLakeer0def2e0e4e6324d);
    flxAcc2.add(lblAccountName2, flxeStatmentSwitchOn, flxeStatmentSwitchOff);
    var signInButton = new kony.ui.Button({
        "centerX": "50.03%",
        "focusSkin": "sknprimaryAction",
        "height": "9%",
        "id": "signInButton",
        "isVisible": true,
        "onClick": AS_Button_dd6bcee77a104654974da8e88ac09369,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.save"),
        "top": "12%",
        "width": "75%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxScrollBody.add(lblAddNickNameDesc, flxDescrip, flxShowHide, flxAcc2, signInButton);
    flxMain.add(flxSettingsHeader, flxScrollBody);
    frmCardsAddNickname.add(flxMain);
};

function frmCardsAddNicknameGlobals() {
    frmCardsAddNickname = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCardsAddNickname,
        "enabledForIdleTimeout": true,
        "id": "frmCardsAddNickname",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_j0c3685acb7f4b75b31915979163cdac,
        "preShow": function(eventobject) {
            AS_Form_dafcf8d4ce124c689b55c56ff08598a4(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_d65e4939bd96473b9af0b489462fff6c,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};