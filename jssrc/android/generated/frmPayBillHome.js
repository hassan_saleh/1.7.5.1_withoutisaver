function addWidgetsfrmPayBillHome() {
    frmPayBillHome.setDefaultUnit(kony.flex.DP);
    var flxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "CopyslFbox0i5e0ac7d37b04e",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_h22984dbfd5b435889e026e2d675e577,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.PaymentDash.BillsPayment"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(flxBack, lblTitle);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "82%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var imgBeneficiary = new kony.ui.Image2({
        "centerX": "50%",
        "height": "25%",
        "id": "imgBeneficiary",
        "isVisible": true,
        "skin": "slImage",
        "src": "icon_beneficiary.png",
        "top": "8%",
        "width": "80%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblMessageTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "8%",
        "id": "lblMessageTitle",
        "isVisible": true,
        "skin": "sknLblMsg",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.PayYourBills"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMessage = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblMessage",
        "isVisible": true,
        "skin": "sknLblDetail",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.statement"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnScreen = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "10%",
        "id": "btnScreen",
        "isVisible": true,
        "maxWidth": "90%",
        "onClick": AS_Button_j99e1c53446b4acb87918593a3a00055,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.AddBill"),
        "top": "5%",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnQuickPay = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "10%",
        "id": "btnQuickPay",
        "isVisible": true,
        "maxWidth": "90%",
        "onClick": AS_Button_ga3baceb590e4968a0e1e0aca876a6ff,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.PayNow"),
        "top": "5%",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxSpace = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxSpace",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSpace.setDefaultUnit(kony.flex.DP);
    flxSpace.add();
    flxMainContainer.add(imgBeneficiary, lblMessageTitle, lblMessage, btnScreen, btnQuickPay, flxSpace);
    flxMain.add(flxHeader, flxMainContainer);
    var flxFooter = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "7%",
        "id": "flxFooter",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "menu",
        "top": "93%",
        "width": "100%"
    }, {}, {});
    flxFooter.setDefaultUnit(kony.flex.DP);
    var FlxAccounts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxAccounts",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_bb997be627004223ae93a96d9b24de9f,
        "skin": "sknfocusmenu",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    FlxAccounts.setDefaultUnit(kony.flex.DP);
    var img1 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img1",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_accounts_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label03174bff69bb54c = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label03174bff69bb54c",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnAccounts = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "40%",
        "focusSkin": "btnCardFoc",
        "height": "45dp",
        "id": "btnAccounts",
        "isVisible": true,
        "onClick": AS_Button_e13ba2a8826249a099d7179f02429b0a,
        "skin": "btnCard",
        "text": "H",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlxAccounts.add(img1, Label03174bff69bb54c, btnAccounts);
    var FlxTranfers = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxTranfers",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_c2880ca31aca4c30989d091e7d38a615,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    FlxTranfers.setDefaultUnit(kony.flex.DP);
    var img2 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img2",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_t_and_p_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label02bec01fd5baf4c = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label02bec01fd5baf4c",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnTransfers = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "40%",
        "focusSkin": "btnFooterDisablefocusskin",
        "height": "45dp",
        "id": "btnTransfers",
        "isVisible": true,
        "onClick": AS_Button_e4fe9cfb4eb34372b506f92a49a597a0,
        "skin": "btnFooterDisable",
        "text": "G",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlxTranfers.add(img2, Label02bec01fd5baf4c, btnTransfers);
    var FlxPayment = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxPayment",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_e921e086574c4de2ba840afe7dfc5e60,
        "skin": "slFbox",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    FlxPayment.setDefaultUnit(kony.flex.DP);
    var imgBot = new kony.ui.Image2({
        "centerX": "50%",
        "height": "40dp",
        "id": "imgBot",
        "isVisible": false,
        "left": "13dp",
        "skin": "slImage",
        "src": "chaticonactive.png",
        "top": "4dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPayment = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "40%",
        "focusSkin": "btnFooterDisableaccount",
        "height": "45dp",
        "id": "btnPayment",
        "isVisible": true,
        "skin": "btnFooterDisableaccount",
        "text": "i",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyLabel0i584dcaf2e8546 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0i584dcaf2e8546",
        "isVisible": true,
        "skin": "sknlblFootertitleFocus",
        "text": kony.i18n.getLocalizedString("i18n.alert.transfers"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlxPayment.add(imgBot, btnPayment, CopyLabel0i584dcaf2e8546);
    var FlxDeposits = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxDeposits",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_b50a237efd3c4809a0327abddc5d32de,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    FlxDeposits.setDefaultUnit(kony.flex.DP);
    var img3 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img3",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_deposits_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label04221a71494e848 = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label04221a71494e848",
        "isVisible": false,
        "skin": "sknlblmenu",
        "text": kony.i18n.getLocalizedString("i18n.common.deposits"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "34dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnDeposit = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "btnDeposit",
        "isVisible": true,
        "skin": "btnCard",
        "text": "J",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlxDeposits.add(img3, Label04221a71494e848, btnDeposit);
    var FlxMore = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxMore",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_fbc6a1b6a9e746c3879fbb0afe01d757,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    FlxMore.setDefaultUnit(kony.flex.DP);
    var img4 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img4",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_more_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label0e5331028c2ef41 = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label0e5331028c2ef41",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": kony.i18n.getLocalizedString("i18n.common.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnMore = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "40%",
        "focusSkin": "btnCardFoc",
        "height": "45dp",
        "id": "btnMore",
        "isVisible": true,
        "onClick": AS_Button_gf0e4bb5d73f4e219a7e3db07a6ed96a,
        "skin": "btnCard",
        "text": "K",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlxMore.add(img4, Label0e5331028c2ef41, btnMore);
    flxFooter.add(FlxAccounts, FlxTranfers, FlxPayment, FlxDeposits, FlxMore);
    frmPayBillHome.add(flxMain, flxFooter);
};

function frmPayBillHomeGlobals() {
    frmPayBillHome = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPayBillHome,
        "enabledForIdleTimeout": true,
        "id": "frmPayBillHome",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": function(eventobject) {
            AS_Form_i86ae149903d41c18e07dc6fc6e21ab7(eventobject);
        },
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};