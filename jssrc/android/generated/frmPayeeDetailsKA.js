function addWidgetsfrmPayeeDetailsKA() {
    frmPayeeDetailsKA.setDefaultUnit(kony.flex.DP);
    var flxTitleBarPayeeDetailsKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTitleBarPayeeDetailsKA",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTitleBarPayeeDetailsKA.setDefaultUnit(kony.flex.DP);
    var lblTitleKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblTitleKA",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeDetails"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnCloseKA = new kony.ui.Button({
        "focusSkin": "sknCopyinvisibleButtonFocus0df746dda29534b",
        "height": "50dp",
        "id": "btnCloseKA",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_57bf5e3b76064905896a2488bbbcdd30,
        "skin": "sknCopyinvisibleButton046d30d34316f47",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTitleBarPayeeDetailsKA.add(lblTitleKA, btnCloseKA);
    var mainScrollContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "mainScrollContainer",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkgGray",
        "top": "50dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainScrollContainer.setDefaultUnit(kony.flex.DP);
    var flxUpperContainerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxUpperContainerKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUpperContainerKA.setDefaultUnit(kony.flex.DP);
    var payeeNameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "payeeNameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    payeeNameContainer.setDefaultUnit(kony.flex.DP);
    var CopyLabel0b353ede7efad4c = new kony.ui.Label({
        "height": "32dp",
        "id": "CopyLabel0b353ede7efad4c",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsectionHeaderLabel",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.companyName"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyusernameContainer0c123209f151c45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "CopyusernameContainer0c123209f151c45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "right": "5%",
        "skin": "sknslFbox",
        "top": "35dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer0c123209f151c45.setDefaultUnit(kony.flex.DP);
    var flxDivider1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxDivider1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDivider1.setDefaultUnit(kony.flex.DP);
    flxDivider1.add();
    var Label05b62c7983b484e = new kony.ui.Label({
        "height": "38dp",
        "id": "Label05b62c7983b484e",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRegularFormlbl",
        "text": "Power Corporation Ltd.",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyusernameContainer0c123209f151c45.add(flxDivider1, Label05b62c7983b484e);
    payeeNameContainer.add(CopyLabel0b353ede7efad4c, CopyusernameContainer0c123209f151c45);
    var flxPayeeNickNameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "flxPayeeNickNameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPayeeNickNameContainer.setDefaultUnit(kony.flex.DP);
    var CopyLabel0927085edf80e42 = new kony.ui.Label({
        "height": "32dp",
        "id": "CopyLabel0927085edf80e42",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsectionHeaderLabel",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeNickName"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyusernameContainer0b764e42673504b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "CopyusernameContainer0b764e42673504b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "right": "5%",
        "skin": "sknslFbox",
        "top": "35dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer0b764e42673504b.setDefaultUnit(kony.flex.DP);
    var Label06b74b1ba15f34d = new kony.ui.Label({
        "height": "38dp",
        "id": "Label06b74b1ba15f34d",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRegularFormlbl",
        "text": "My shop's power bill",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxDivider0b294261c43344c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyflxDivider0b294261c43344c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxDivider0b294261c43344c.setDefaultUnit(kony.flex.DP);
    CopyflxDivider0b294261c43344c.add();
    CopyusernameContainer0b764e42673504b.add(Label06b74b1ba15f34d, CopyflxDivider0b294261c43344c);
    flxPayeeNickNameContainer.add(CopyLabel0927085edf80e42, CopyusernameContainer0b764e42673504b);
    var payeeAccountContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "85dp",
        "id": "payeeAccountContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    payeeAccountContainer.setDefaultUnit(kony.flex.DP);
    var CopyLabel0200c64cb34cb44 = new kony.ui.Label({
        "height": "32dp",
        "id": "CopyLabel0200c64cb34cb44",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsectionHeaderLabel",
        "text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyusernameContainer09871c009dbc247 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "54dp",
        "id": "CopyusernameContainer09871c009dbc247",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "isModalContainer": false,
        "right": "5%",
        "skin": "sknslFbox",
        "top": "35dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer09871c009dbc247.setDefaultUnit(kony.flex.DP);
    var Label05667124c8f964b = new kony.ui.Label({
        "height": "38dp",
        "id": "Label05667124c8f964b",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRegularFormlbl",
        "text": "123499484321",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxDivider07de2109b1c0a4d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyflxDivider07de2109b1c0a4d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxDivider07de2109b1c0a4d.setDefaultUnit(kony.flex.DP);
    CopyflxDivider07de2109b1c0a4d.add();
    CopyusernameContainer09871c009dbc247.add(Label05667124c8f964b, CopyflxDivider07de2109b1c0a4d);
    payeeAccountContainer.add(CopyLabel0200c64cb34cb44, CopyusernameContainer09871c009dbc247);
    flxUpperContainerKA.add(payeeNameContainer, flxPayeeNickNameContainer, payeeAccountContainer);
    var payeeAddressContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "payeeAddressContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    payeeAddressContainer.setDefaultUnit(kony.flex.DP);
    var CopyLabel0b1ac08ce375a4c = new kony.ui.Label({
        "height": "32dp",
        "id": "CopyLabel0b1ac08ce375a4c",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsectionHeaderLabel",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeAddressC"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyusernameContainer012a8b92d34cd4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "36dp",
        "id": "CopyusernameContainer012a8b92d34cd4b",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "right": "5%",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer012a8b92d34cd4b.setDefaultUnit(kony.flex.DP);
    var Label0809dec709c8b4e = new kony.ui.Label({
        "height": "38dp",
        "id": "Label0809dec709c8b4e",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRegularFormlbl",
        "text": "Power street",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyFlexContainer09fbd523e9a6c43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer09fbd523e9a6c43",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer09fbd523e9a6c43.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer09fbd523e9a6c43.add();
    CopyusernameContainer012a8b92d34cd4b.add(Label0809dec709c8b4e, CopyFlexContainer09fbd523e9a6c43);
    var CopyusernameContainer0eea4f72ba00743 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "38dp",
        "id": "CopyusernameContainer0eea4f72ba00743",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "right": "5%",
        "skin": "sknslFbox",
        "top": "6dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer0eea4f72ba00743.setDefaultUnit(kony.flex.DP);
    var Label0008aa4b9ba5641 = new kony.ui.Label({
        "height": "38dp",
        "id": "Label0008aa4b9ba5641",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRegularFormlbl",
        "text": "P.O.B 54424",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var FlexContainer0da5c3167759a45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "FlexContainer0da5c3167759a45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0da5c3167759a45.setDefaultUnit(kony.flex.DP);
    FlexContainer0da5c3167759a45.add();
    CopyusernameContainer0eea4f72ba00743.add(Label0008aa4b9ba5641, FlexContainer0da5c3167759a45);
    var CopyusernameContainer0740d926171f149 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "38dp",
        "id": "CopyusernameContainer0740d926171f149",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "right": "5%",
        "skin": "sknslFbox",
        "top": "6dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer0740d926171f149.setDefaultUnit(kony.flex.DP);
    var Label049c7aec24cab47 = new kony.ui.Label({
        "height": "38dp",
        "id": "Label049c7aec24cab47",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRegularFormlbl",
        "text": "New York",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyFlexContainer0f3582dde3eac40 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0f3582dde3eac40",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0f3582dde3eac40.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0f3582dde3eac40.add();
    CopyusernameContainer0740d926171f149.add(Label049c7aec24cab47, CopyFlexContainer0f3582dde3eac40);
    var CopyusernameContainer083753b47661045 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "38dp",
        "id": "CopyusernameContainer083753b47661045",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "right": "5%",
        "skin": "sknslFbox",
        "top": "6dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer083753b47661045.setDefaultUnit(kony.flex.DP);
    var Label0661b6cb4f47d4b = new kony.ui.Label({
        "height": "38dp",
        "id": "Label0661b6cb4f47d4b",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRegularFormlbl",
        "text": "New York",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyFlexContainer0e65f2830703a49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0e65f2830703a49",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0e65f2830703a49.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0e65f2830703a49.add();
    CopyusernameContainer083753b47661045.add(Label0661b6cb4f47d4b, CopyFlexContainer0e65f2830703a49);
    var CopyusernameContainer09da656c676db42 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "38dp",
        "id": "CopyusernameContainer09da656c676db42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "right": "5%",
        "skin": "sknslFbox",
        "top": "6dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer09da656c676db42.setDefaultUnit(kony.flex.DP);
    var Label023201109baf64b = new kony.ui.Label({
        "height": "38dp",
        "id": "Label023201109baf64b",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRegularFormlbl",
        "text": "500047",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyFlexContainer02077f851ad8c46 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer02077f851ad8c46",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer02077f851ad8c46.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer02077f851ad8c46.add();
    CopyusernameContainer09da656c676db42.add(Label023201109baf64b, CopyFlexContainer02077f851ad8c46);
    payeeAddressContainer.add(CopyLabel0b1ac08ce375a4c, CopyusernameContainer012a8b92d34cd4b, CopyusernameContainer0eea4f72ba00743, CopyusernameContainer0740d926171f149, CopyusernameContainer083753b47661045, CopyusernameContainer09da656c676db42);
    var buttonWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "350dp",
        "id": "buttonWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    buttonWrapper.setDefaultUnit(kony.flex.DP);
    var saveNewPayeeButton = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "50dp",
        "id": "saveNewPayeeButton",
        "isVisible": true,
        "onClick": AS_Button_43fbcd8830844f1bbebdc9c59e23a06f,
        "skin": "sknSecondaryActionWhiteBgKA",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.editPayee"),
        "top": "24dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnDeletePayeeKA = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "50dp",
        "id": "btnDeletePayeeKA",
        "isVisible": true,
        "onClick": AS_Button_70caf02b9b8e4885a95ec4b12612cbae,
        "skin": "sknSecondaryActionWhiteBgKA",
        "text": kony.i18n.getLocalizedString("i18n.transfer.deletePayee"),
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    buttonWrapper.add(saveNewPayeeButton, btnDeletePayeeKA);
    mainScrollContainer.add(flxUpperContainerKA, payeeAddressContainer, buttonWrapper);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxTop = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxTop",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTop.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_hd92cc663fcd4501be12b764ee78b818,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "70%",
        "id": "lblTitle",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.transfer.billDetails"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTop.add(flxBack, lblTitle);
    flxHeader.add(flxTop);
    var flxMain = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "91%",
        "horizontalScrollIndicator": true,
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var flxBiller = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxBiller",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBiller.setDefaultUnit(kony.flex.DP);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "sknFlxToIcon",
        "top": "20%",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "text": "BH",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon1.add(lblInitial);
    var flxDetail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxDetail",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "2%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    flxDetail.setDefaultUnit(kony.flex.DP);
    var lblNickName = new kony.ui.Label({
        "height": "17%",
        "id": "lblNickName",
        "isVisible": true,
        "left": "0dp",
        "skin": "slBillerDetailMain",
        "text": "Nick Name",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblName = new kony.ui.Label({
        "height": "17%",
        "id": "lblName",
        "isVisible": false,
        "left": "0dp",
        "skin": "slBillerDetailMain",
        "text": "Nick Name",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblServiceType = new kony.ui.Label({
        "height": "17%",
        "id": "lblServiceType",
        "isVisible": true,
        "left": "0dp",
        "skin": "slBillerType",
        "text": "Electricity",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblType = new kony.ui.Label({
        "height": "17%",
        "id": "lblType",
        "isVisible": true,
        "left": "0dp",
        "skin": "slBillerType",
        "text": "Prepaid",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber = new kony.ui.Label({
        "height": "17%",
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblAccNumBiller",
        "text": "48574839485",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDueAmount = new kony.ui.Label({
        "id": "lblDueAmount",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblAccNumBiller",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDetail.add(lblNickName, lblName, lblServiceType, lblType, lblAccountNumber, lblDueAmount);
    var btnDelete = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknDeleteFocus",
        "height": "50%",
        "id": "btnDelete",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_i77a9d5add9c46438b031fbd1d7046f9,
        "skin": "sknBtnBack",
        "text": "w",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblOperation = new kony.ui.Label({
        "id": "lblOperation",
        "isVisible": false,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "48dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBillerCode = new kony.ui.Label({
        "id": "lblBillerCode",
        "isVisible": false,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "48dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDummy = new kony.ui.Label({
        "id": "lblDummy",
        "isVisible": false,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "48dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBiller.add(flxIcon1, flxDetail, btnDelete, lblOperation, lblBillerCode, lblDummy);
    var btnPayNow = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "9%",
        "id": "btnPayNow",
        "isVisible": true,
        "onClick": AS_Button_c086d93cdede428a96b66c6dcd18fba4,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.cards.paynow"),
        "top": "3%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxTransaction = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxTransaction",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "5%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTransaction.setDefaultUnit(kony.flex.DP);
    var lblTitleTransaction = new kony.ui.Label({
        "id": "lblTitleTransaction",
        "isVisible": true,
        "left": "8%",
        "skin": "sknBeneTitle",
        "text": kony.i18n.getLocalizedString("i18n.common.recentTransactions"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var segTransaction = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "data": [{
            "lblBiller": "",
            "lblTime": ""
        }],
        "groupCells": false,
        "id": "segTransaction",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "slSegAllBene",
        "rowTemplate": flxTransactionDetail,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "15dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxLine": "flxLine",
            "flxTransactionDetail": "flxTransactionDetail",
            "lblBiller": "lblBiller",
            "lblTime": "lblTime"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblNoRecords = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblNoRecords",
        "isVisible": false,
        "skin": "sknAcclbl",
        "text": kony.i18n.getLocalizedString("i18n.common.noRecAvail"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "85%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblInfo = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblInfo",
        "isVisible": true,
        "skin": "sknAcclbl",
        "text": kony.i18n.getLocalizedString("i18n.bills.MoreDetails"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "85%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTransaction.add(lblTitleTransaction, segTransaction, lblNoRecords, lblInfo);
    flxMain.add(flxBiller, btnPayNow, flxTransaction);
    frmPayeeDetailsKA.add(flxTitleBarPayeeDetailsKA, mainScrollContainer, flxHeader, flxMain);
};

function frmPayeeDetailsKAGlobals() {
    frmPayeeDetailsKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPayeeDetailsKA,
        "enabledForIdleTimeout": true,
        "id": "frmPayeeDetailsKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "preShow": function(eventobject) {
            AS_Form_fa93e0d5b298420fb4c13fce1b735839(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "inTransitionConfig": {
            "formAnimation": 3
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};