function addWidgetsfrmCreditCardPayment() {
    frmCreditCardPayment.setDefaultUnit(kony.flex.DP);
    var flxBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBody",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "CopyslFbox0f1396257654f47",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBody.setDefaultUnit(kony.flex.DP);
    var flxCreditCardHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxCreditCardHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 2
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxCreditCardHeader.setDefaultUnit(kony.flex.DP);
    var lblCreditCard = new kony.ui.Label({
        "centerX": "50%",
        "height": "90%",
        "id": "lblCreditCard",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.cards.ccpayment"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnNext = new kony.ui.Button({
        "focusSkin": "jomopaynextDisabled",
        "height": "100%",
        "id": "btnNext",
        "isVisible": true,
        "onClick": AS_Button_bff471f8745643e38a6c14e519b82583,
        "right": "0.00%",
        "skin": "jomopaynextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "top": "0.18%",
        "width": "20%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": true
    }, {});
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_c24609342f0b469ba80b500434210773,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 2
    }, {
        "retainFlexPositionProperties": true,
        "retainFlowHorizontalAlignment": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    flxCreditCardHeader.add(lblCreditCard, btnNext, flxBack);
    var flxAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxAccount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "9%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccount.setDefaultUnit(kony.flex.DP);
    var lblAccountStaticText = new kony.ui.Label({
        "height": "35%",
        "id": "lblAccountStaticText",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.creditcard.selectaccount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBranchNum = new kony.ui.Label({
        "height": "35%",
        "id": "lblBranchNum",
        "isVisible": false,
        "left": "15%",
        "skin": "lblAccountStaticText",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "12%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFromCurr = new kony.ui.Label({
        "height": "35%",
        "id": "lblFromCurr",
        "isVisible": false,
        "left": "25%",
        "skin": "lblAccountStaticText",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "22%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var hiddenLblAccountNum = new kony.ui.Label({
        "height": "35%",
        "id": "hiddenLblAccountNum",
        "isVisible": false,
        "left": "25%",
        "skin": "lblAccountStaticText",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "22%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPaymentFlag = new kony.ui.Label({
        "height": "35%",
        "id": "lblPaymentFlag",
        "isVisible": false,
        "left": "35%",
        "skin": "lblAccountStaticText",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "32%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDum = new kony.ui.Label({
        "height": "35%",
        "id": "lblDum",
        "isVisible": false,
        "left": "45%",
        "skin": "lblAccountStaticText",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "42%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAccountType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxAccountType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "37%",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    flxAccountType.setDefaultUnit(kony.flex.DP);
    var btnDropDownAccount = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "100%",
        "id": "btnDropDownAccount",
        "isVisible": true,
        "onClick": AS_Button_cb350193e3f14ed5a9e81aca1e9b148f,
        "right": "9%",
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "2%",
        "width": "90%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxAccountNo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxAccountNo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "1%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "5%",
        "width": "80%",
        "zIndex": 2
    }, {}, {});
    flxAccountNo.setDefaultUnit(kony.flex.DP);
    var lblAccountType = new kony.ui.Label({
        "height": "70%",
        "id": "lblAccountType",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "text": "Current Account",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountCode = new kony.ui.Label({
        "height": "70%",
        "id": "lblAccountCode",
        "isVisible": false,
        "left": "3%",
        "skin": "lblsegtextsmall0b5a3b38d4be646",
        "text": " ***2002",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAccountNo.add(lblAccountType, lblAccountCode);
    flxAccountType.add(btnDropDownAccount, flxAccountNo);
    var flxBorderAccountType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "5%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderAccountType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "skntextFieldDividerGreen",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBorderAccountType.setDefaultUnit(kony.flex.DP);
    flxBorderAccountType.add();
    flxAccount.add(lblAccountStaticText, lblBranchNum, lblFromCurr, hiddenLblAccountNum, lblPaymentFlag, lblDum, flxAccountType, flxBorderAccountType);
    var flxCardDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxCardDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "27%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxCardDetails.setDefaultUnit(kony.flex.DP);
    var btnCard = new kony.ui.Button({
        "focusSkin": "sknRDOWhiteBRDBOJFont",
        "height": "20%",
        "id": "btnCard",
        "isVisible": true,
        "left": "5%",
        "onClick": AS_Button_fb3fd93f738d41238621624d47672227,
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "t",
        "top": "5%",
        "width": "7%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnOtherCard = new kony.ui.Button({
        "focusSkin": "sknRDOWhiteBRDBOJFont",
        "height": "20%",
        "id": "btnOtherCard",
        "isVisible": true,
        "left": "53%",
        "onClick": AS_Button_f3a6cffaa45a4775ac7416961c5ec427,
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "s",
        "top": "5%",
        "width": "7%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lBoxCardList = new kony.ui.ListBox({
        "height": "25%",
        "id": "lBoxCardList",
        "isVisible": false,
        "left": "5%",
        "masterData": [
            ["lb1", "4321 56** **** 3456"],
            ["lb2", "2143 56** **** 6543"],
            ["lbl3", "1234 56** **** 3456"]
        ],
        "onSelection": AS_ListBox_a84637181ee847d18d9d9716e125161c,
        "skin": "CopyslListBox0jdccc1df15ad4f",
        "top": "43%",
        "width": "87%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "placeholder": "Please Select a Card",
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var txtBoxOtherCard = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "CopyslTextBox0jcbb5d6dc23840",
        "height": "45%",
        "id": "txtBoxOtherCard",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "3%",
        "maxTextLength": 16,
        "onTextChange": AS_TextField_b4450214b4fa4eddaaeef3b478ce03a6,
        "placeholder": kony.i18n.getLocalizedString("i18n.card.entercardnumber"),
        "secureTextEntry": false,
        "skin": "CopyslTextBox0jcbb5d6dc23840",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "40%",
        "width": "90%",
        "zIndex": 2
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderCardNum = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderCardNum",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "skntextFieldDividerGreen",
        "top": "80%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBorderCardNum.setDefaultUnit(kony.flex.DP);
    flxBorderCardNum.add();
    var lblBOJCard = new kony.ui.Label({
        "height": "20%",
        "id": "lblBOJCard",
        "isVisible": true,
        "left": "15%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.creditcard.cardnumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblOtherCard = new kony.ui.Label({
        "id": "lblOtherCard",
        "isVisible": true,
        "left": "62%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.creditcard.othercard"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxCardNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxCardNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d74c135b12244246a932af5ebe8e6f39,
        "skin": "slFbox",
        "top": "40%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxCardNumber.setDefaultUnit(kony.flex.DP);
    var btnDropDownCard = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "100%",
        "id": "btnDropDownCard",
        "isVisible": true,
        "onClick": AS_Button_gc010130bb594a9c80894a7b91fffaa9,
        "right": "9%",
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "0%",
        "width": "90%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "1%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "8%",
        "width": "80%",
        "zIndex": 2
    }, {}, {});
    flxCard.setDefaultUnit(kony.flex.DP);
    var lblCardNumber = new kony.ui.Label({
        "height": "70%",
        "id": "lblCardNumber",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.cards.selectCard"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var hiddenCardNumber = new kony.ui.Label({
        "height": "70%",
        "id": "hiddenCardNumber",
        "isVisible": false,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCard.add(lblCardNumber, hiddenCardNumber);
    flxCardNumber.add(btnDropDownCard, flxCard);
    flxCardDetails.add(btnCard, btnOtherCard, lBoxCardList, txtBoxOtherCard, flxBorderCardNum, lblBOJCard, lblOtherCard, flxCardNumber);
    var flxDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55%",
        "id": "flxDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "45%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxDetails.setDefaultUnit(kony.flex.DP);
    var flxTotalUsedAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxTotalUsedAmount",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0.00%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTotalUsedAmount.setDefaultUnit(kony.flex.DP);
    var lblTotalUsedAmountStaticText = new kony.ui.Label({
        "height": "50%",
        "id": "lblTotalUsedAmountStaticText",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.creditcard.totalamount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxTUAmountInside = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxTUAmountInside",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxTUAmountInside.setDefaultUnit(kony.flex.DP);
    var lblTUAmount = new kony.ui.Label({
        "id": "lblTUAmount",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStaticJOD = new kony.ui.Label({
        "id": "lblStaticJOD",
        "isVisible": false,
        "left": "2%",
        "skin": "sknTransferType",
        "text": "JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTUAmountInside.add(lblTUAmount, lblStaticJOD);
    flxTotalUsedAmount.add(lblTotalUsedAmountStaticText, flxTUAmountInside);
    var flxPaymentValue = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxPaymentValue",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "20%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPaymentValue.setDefaultUnit(kony.flex.DP);
    var lblPaymentValueStaticText = new kony.ui.Label({
        "height": "50%",
        "id": "lblPaymentValueStaticText",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.bills.DueAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPV = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxPV",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxPV.setDefaultUnit(kony.flex.DP);
    var lblPaymentValue = new kony.ui.Label({
        "id": "lblPaymentValue",
        "isVisible": true,
        "left": "0.00%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStaticJOD1 = new kony.ui.Label({
        "id": "lblStaticJOD1",
        "isVisible": false,
        "left": "2%",
        "skin": "sknTransferType",
        "text": "JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPV.add(lblPaymentValue, lblStaticJOD1);
    flxPaymentValue.add(lblPaymentValueStaticText, flxPV);
    var flxAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "40%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAmount.setDefaultUnit(kony.flex.DP);
    var lblAmountStaticText = new kony.ui.Label({
        "id": "lblAmountStaticText",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtFieldAmount = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "45%",
        "id": "txtFieldAmount",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "left": "5%",
        "maxTextLength": 9,
        "onDone": AS_TextField_d95f6ac7bcad4cb4986a6cbbea503257,
        "onTextChange": AS_TextField_a38625f8a0e247b98d05d406f50bb2cc,
        "onTouchStart": AS_TextField_cce7105e7c5a415ebe77e51f49ec19d1,
        "placeholder": "0.000",
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "40%",
        "width": "90%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "onEndEditing": AS_TextField_a114ddfa993a49a3bac129e464ba5bbe,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDividerJomoPay",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBorderAmount.setDefaultUnit(kony.flex.DP);
    flxBorderAmount.add();
    var lblCurrency = new kony.ui.Label({
        "id": "lblCurrency",
        "isVisible": true,
        "left": "84%",
        "skin": "lblAccountStaticText",
        "text": "JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "42%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblHiddenAmount = new kony.ui.Label({
        "id": "lblHiddenAmount",
        "isVisible": false,
        "left": "94%",
        "skin": "lblAccountStaticText",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "52%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAmount.add(lblAmountStaticText, txtFieldAmount, flxBorderAmount, lblCurrency, lblHiddenAmount);
    var flxDueDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxDueDate",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "60%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDueDate.setDefaultUnit(kony.flex.DP);
    var lblDueDate = new kony.ui.Label({
        "height": "50%",
        "id": "lblDueDate",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.creditcard.paymentduedate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxDueDateCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxDueDateCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxDueDateCard.setDefaultUnit(kony.flex.DP);
    var lblDate = new kony.ui.Label({
        "id": "lblDate",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDueDateCard.add(lblDate);
    flxDueDate.add(lblDueDate, flxDueDateCard);
    var txtTransparent = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "1dp",
        "id": "txtTransparent",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "200%",
        "secureTextEntry": false,
        "skin": "slTextBox",
        "text": "TextBox2",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "10dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxDetails.add(flxTotalUsedAmount, flxPaymentValue, flxAmount, flxDueDate, txtTransparent);
    flxBody.add(flxCreditCardHeader, flxAccount, flxCardDetails, flxDetails);
    var flxConfirmPopUp = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxConfirmPopUp",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBlueBen",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxConfirmPopUp.setDefaultUnit(kony.flex.DP);
    var flxConfirmHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmHeader.setDefaultUnit(kony.flex.DP);
    var flxHeaderBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxHeaderBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_dc224a9327824c8492ae15938a3c78a6,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxHeaderBack.setDefaultUnit(kony.flex.DP);
    var lblHeaderBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": "j",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblHeaderBack = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeaderBack.add(lblHeaderBackIcon, lblHeaderBack);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnClose = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknDeleteFocus",
        "height": "80%",
        "id": "btnClose",
        "isVisible": true,
        "left": "85%",
        "onClick": AS_Button_i74fd9160e664557be82d7d8e352f1a3,
        "skin": "sknBtnBack",
        "text": "O",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxConfirmHeader.add(flxHeaderBack, lblHeaderTitle, btnClose);
    var flxOtherDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "72%",
        "id": "flxOtherDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxOtherDetails.setDefaultUnit(kony.flex.DP);
    var flxAccountConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "14%",
        "id": "flxAccountConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccountConfirm.setDefaultUnit(kony.flex.DP);
    var lblAccountStatic = new kony.ui.Label({
        "height": "45%",
        "id": "lblAccountStatic",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.settings.account"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber = new kony.ui.Label({
        "height": "45%",
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "8.04%",
        "skin": "skncardconfirm",
        "text": "654654654",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAccountConfirm.add(lblAccountStatic, lblAccountNumber);
    var flxCardDetailsConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "14%",
        "id": "flxCardDetailsConfirm",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCardDetailsConfirm.setDefaultUnit(kony.flex.DP);
    var lblCardType = new kony.ui.Label({
        "height": "45%",
        "id": "lblCardType",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.cards.CardTypec"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCard = new kony.ui.Label({
        "height": "45%",
        "id": "lblCard",
        "isVisible": true,
        "left": "8%",
        "skin": "skncardconfirm",
        "text": "BOJ Card",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCardDetailsConfirm.add(lblCardType, lblCard);
    var flxCardNumberConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "14%",
        "id": "flxCardNumberConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCardNumberConfirm.setDefaultUnit(kony.flex.DP);
    var lblCardNumberStatic = new kony.ui.Label({
        "height": "45%",
        "id": "lblCardNumberStatic",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.creditcard.cardnumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCardValue = new kony.ui.Label({
        "height": "45%",
        "id": "lblCardValue",
        "isVisible": true,
        "left": "8%",
        "skin": "skncardconfirm",
        "text": "1223",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCardNumberConfirm.add(lblCardNumberStatic, lblCardValue);
    var flxTUAmountConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "14%",
        "id": "flxTUAmountConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTUAmountConfirm.setDefaultUnit(kony.flex.DP);
    var lblTotUsedAmount = new kony.ui.Label({
        "height": "45%",
        "id": "lblTotUsedAmount",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.creditcard.totalamount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTUAmountValue = new kony.ui.Label({
        "height": "45%",
        "id": "lblTUAmountValue",
        "isVisible": true,
        "left": "8%",
        "skin": "skncardconfirm",
        "text": "50.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTUAmountConfirm.add(lblTotUsedAmount, lblTUAmountValue);
    var flxPVConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "14%",
        "id": "flxPVConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPVConfirm.setDefaultUnit(kony.flex.DP);
    var lblPVStatic = new kony.ui.Label({
        "height": "45%",
        "id": "lblPVStatic",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.bills.DueAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPVValue = new kony.ui.Label({
        "height": "45%",
        "id": "lblPVValue",
        "isVisible": true,
        "left": "8%",
        "skin": "skncardconfirm",
        "text": "10.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPVConfirm.add(lblPVStatic, lblPVValue);
    var flxAmountConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "14%",
        "id": "flxAmountConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAmountConfirm.setDefaultUnit(kony.flex.DP);
    var lblAmountStatic = new kony.ui.Label({
        "height": "45%",
        "id": "lblAmountStatic",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.cards.amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAmountValue = new kony.ui.Label({
        "height": "45%",
        "id": "lblAmountValue",
        "isVisible": true,
        "left": "8%",
        "skin": "skncardconfirm",
        "text": "15.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAmountConfirm.add(lblAmountStatic, lblAmountValue);
    var flxDueDateConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "14%",
        "id": "flxDueDateConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDueDateConfirm.setDefaultUnit(kony.flex.DP);
    var lblDueDateStatic = new kony.ui.Label({
        "height": "45%",
        "id": "lblDueDateStatic",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.bills.DueDate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDueDateValue = new kony.ui.Label({
        "height": "45%",
        "id": "lblDueDateValue",
        "isVisible": true,
        "left": "8%",
        "skin": "skncardconfirm",
        "text": "26 Jan 2018",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDueDateConfirm.add(lblDueDateStatic, lblDueDateValue);
    flxOtherDetails.add(flxAccountConfirm, flxCardDetailsConfirm, flxCardNumberConfirm, flxTUAmountConfirm, flxPVConfirm, flxAmountConfirm, flxDueDateConfirm);
    var flxButtonHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "18%",
        "id": "flxButtonHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxButtonHolder.setDefaultUnit(kony.flex.DP);
    var btnConfirm = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "slButtonWhite",
        "height": "50dp",
        "id": "btnConfirm",
        "isVisible": true,
        "onClick": AS_Button_i2d725944f6a469da9acde0682c13ac9,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxButtonHolder.add(btnConfirm);
    flxConfirmPopUp.add(flxConfirmHeader, flxOtherDetails, flxButtonHolder);
    frmCreditCardPayment.add(flxBody, flxConfirmPopUp);
};

function frmCreditCardPaymentGlobals() {
    frmCreditCardPayment = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCreditCardPayment,
        "enabledForIdleTimeout": true,
        "id": "frmCreditCardPayment",
        "init": AS_Form_ea7167d2fc894057b3c8384ec63c9b51,
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": function(eventobject) {
            AS_Form_c104e0903d86494aa8138597e7d16bb6(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_cff8bea3225f4c3b809878f43085a317,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};