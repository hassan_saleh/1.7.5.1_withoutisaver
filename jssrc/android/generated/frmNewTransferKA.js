function addWidgetsfrmNewTransferKA() {
    frmNewTransferKA.setDefaultUnit(kony.flex.DP);
    var mainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "mainContainer",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContainer.setDefaultUnit(kony.flex.DP);
    var titleBarNewTransfer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarNewTransfer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "101%",
        "zIndex": 1
    }, {}, {});
    titleBarNewTransfer.setDefaultUnit(kony.flex.DP);
    var androidTextBoxFocusFix = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "0dp",
        "id": "androidTextBoxFocusFix",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "74dp",
        "secureTextEntry": false,
        "skin": "sknCopyslTextBox084c844952def4b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "0dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var transferPayTitleLabel = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "transferPayTitleLabel",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.transfer.newTransfer"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var buttonLeft = new kony.ui.Button({
        "focusSkin": "skntitleBarTextButtonFocus",
        "height": "50dp",
        "id": "buttonLeft",
        "isVisible": true,
        "left": "0dp",
        "minWidth": "50dp",
        "onClick": AS_Button_b37365556cbe4bc79765405aa7463b53,
        "skin": "skntitleBarTextButton",
        "text": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    titleBarNewTransfer.add(androidTextBoxFocusFix, transferPayTitleLabel, buttonLeft);
    var fromCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "fromCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox007a422d071dd4e",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    fromCard.setDefaultUnit(kony.flex.DP);
    var fromCardInner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "fromCardInner",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "3%",
        "isModalContainer": false,
        "skin": "skntransferCardInner",
        "top": "60dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    fromCardInner.setDefaultUnit(kony.flex.DP);
    var CopytoInternalLabel0dd19c29451b147 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "CopytoInternalLabel0dd19c29451b147",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0581119363ce54c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    CopytoInternalLabel0dd19c29451b147.setDefaultUnit(kony.flex.DP);
    var CopyborderBottom041db968c787c4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyborderBottom041db968c787c4e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0a29a14ecfe6442",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    CopyborderBottom041db968c787c4e.setDefaultUnit(kony.flex.DP);
    CopyborderBottom041db968c787c4e.add();
    var CopyLabel03e39ab4661a845 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopyLabel03e39ab4661a845",
        "isVisible": true,
        "left": "4%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.transfer.xyzBankAccounts"),
        "top": "0dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopytoInternalLabel0dd19c29451b147.add(CopyborderBottom041db968c787c4e, CopyLabel03e39ab4661a845);
    var segInternalFromAccountsKA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "Label0bed5640cdb4b46": "Label",
            "amountAccount1": "$00.00",
            "amountcurrBal": "$00.00",
            "amtOutsatndingBal": "$00.00",
            "btnNav": "L",
            "lblAccountID": "Label",
            "lblColorKA": "",
            "lblCurrency": "JOD",
            "lblIncommingRing": "s",
            "lblMobile": "S",
            "lblRowSeparator": "Label",
            "nameAccount1": "RichText",
            "nameAccount12": "Account Name",
            "typeAccount": "Available Balance",
            "typeKA": ""
        }, {
            "Label0bed5640cdb4b46": "Label",
            "amountAccount1": "$00.00",
            "amountcurrBal": "$00.00",
            "amtOutsatndingBal": "$00.00",
            "btnNav": "L",
            "lblAccountID": "Label",
            "lblColorKA": "",
            "lblCurrency": "JOD",
            "lblIncommingRing": "s",
            "lblMobile": "S",
            "lblRowSeparator": "Label",
            "nameAccount1": "RichText",
            "nameAccount12": "Account Name",
            "typeAccount": "Available Balance",
            "typeKA": ""
        }, {
            "Label0bed5640cdb4b46": "Label",
            "amountAccount1": "$00.00",
            "amountcurrBal": "$00.00",
            "amtOutsatndingBal": "$00.00",
            "btnNav": "L",
            "lblAccountID": "Label",
            "lblColorKA": "",
            "lblCurrency": "JOD",
            "lblIncommingRing": "s",
            "lblMobile": "S",
            "lblRowSeparator": "Label",
            "nameAccount1": "RichText",
            "nameAccount12": "Account Name",
            "typeAccount": "Available Balance",
            "typeKA": ""
        }],
        "groupCells": false,
        "id": "segInternalFromAccountsKA",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_bf4eb17ceaef47bca6ac96616d9b1188,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": yourAccount1,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopylblIncommingRing0ac2c9e9c52d147": "CopylblIncommingRing0ac2c9e9c52d147",
            "Label0bed5640cdb4b46": "Label0bed5640cdb4b46",
            "amountAccount1": "amountAccount1",
            "amountcurrBal": "amountcurrBal",
            "amtOutsatndingBal": "amtOutsatndingBal",
            "btnNav": "btnNav",
            "btnNav1": "btnNav1",
            "dummyAccountName": "dummyAccountName",
            "dummyAccountNumber": "dummyAccountNumber",
            "flxLine": "flxLine",
            "flxSet": "flxSet",
            "flxamount": "flxamount",
            "isPFMLabel": "isPFMLabel",
            "lblAccNumber": "lblAccNumber",
            "lblAccountID": "lblAccountID",
            "lblBankName": "lblBankName",
            "lblColorKA": "lblColorKA",
            "lblCurrency": "lblCurrency",
            "lblIncommingRing": "lblIncommingRing",
            "lblMobile": "lblMobile",
            "lblRowSeparator": "lblRowSeparator",
            "nameAccount1": "nameAccount1",
            "nameAccount12": "nameAccount12",
            "nameContainer": "nameContainer",
            "typeAccount": "typeAccount",
            "typeKA": "typeKA",
            "yourAccount1": "yourAccount1"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblTransactionType = new kony.ui.Label({
        "id": "lblTransactionType",
        "isVisible": false,
        "left": "206dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    fromCardInner.add(CopytoInternalLabel0dd19c29451b147, segInternalFromAccountsKA, lblTransactionType);
    var fromCardTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "60dp",
        "id": "fromCardTitle",
        "isVisible": true,
        "skin": "skntransferPaySectionHeader",
        "text": kony.i18n.getLocalizedString("i18n.transfer.transferMoneyFromC"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var fromAccountPick = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknyourAccountCardFocus",
        "height": "84dp",
        "id": "fromAccountPick",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntransferPayCardPick",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    fromAccountPick.setDefaultUnit(kony.flex.DP);
    var fromAccountNameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": false,
        "height": "100%",
        "id": "fromAccountNameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "44%",
        "zIndex": 1
    }, {}, {});
    fromAccountNameContainer.setDefaultUnit(kony.flex.DP);
    var fromNamePick = new kony.ui.Label({
        "centerY": "50%",
        "id": "fromNamePick",
        "isVisible": true,
        "left": "13dp",
        "maxWidth": "90%",
        "skin": "sknaccountNamePick",
        "text": kony.i18n.getLocalizedString("i18n.common.accountName"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var fromlblAccountTypeKA = new kony.ui.Label({
        "centerY": "60%",
        "id": "fromlblAccountTypeKA",
        "isVisible": false,
        "left": "23dp",
        "maxWidth": "90%",
        "skin": "sknaccountNamePick",
        "text": kony.i18n.getLocalizedString("i18n.common.accountName"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var fromAccountColorPick = new kony.ui.Label({
        "centerY": "60%",
        "height": "50%",
        "id": "fromAccountColorPick",
        "isVisible": true,
        "left": "1dp",
        "maxWidth": "90%",
        "skin": "sknaccountNamePick",
        "width": "4dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var fromAccountBankNameKA = new kony.ui.Label({
        "id": "fromAccountBankNameKA",
        "isVisible": true,
        "left": "13dp",
        "skin": "skntransLeftLabel",
        "text": "XYZ Bank",
        "top": "55dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    fromAccountNameContainer.add(fromNamePick, fromlblAccountTypeKA, fromAccountColorPick, fromAccountBankNameKA);
    var fromAccountAmountContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "fromAccountAmountContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "11%",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "35%",
        "zIndex": 1
    }, {}, {});
    fromAccountAmountContainer.setDefaultUnit(kony.flex.DP);
    var fromAmountPick = new kony.ui.Label({
        "centerY": "50%",
        "id": "fromAmountPick",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknaccountAmountSelected",
        "text": "$00.00",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyamountAccountOne09f61920910454b = new kony.ui.Label({
        "bottom": "13dp",
        "id": "CopyamountAccountOne09f61920910454b",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknaccountAvailableBalanceLabelSelected",
        "text": kony.i18n.getLocalizedString("i18n.deposit.availableBalance"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    fromAccountAmountContainer.add(fromAmountPick, CopyamountAccountOne09f61920910454b);
    var editFromCard = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "skneditFormFocus",
        "height": "100%",
        "id": "editFromCard",
        "isVisible": true,
        "onClick": AS_Button_7262a2605242483584d0d15e8783de0e,
        "right": "0dp",
        "skin": "skneditForm",
        "text": kony.i18n.getLocalizedString("i18n.common.edit"),
        "width": "56dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var fromlblAccountNumberKA = new kony.ui.Label({
        "id": "fromlblAccountNumberKA",
        "isVisible": false,
        "left": "115dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var fromLblFlex = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "fromLblFlex",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    fromLblFlex.setDefaultUnit(kony.flex.DP);
    var fromLabel = new kony.ui.Label({
        "id": "fromLabel",
        "isVisible": true,
        "left": "13dp",
        "skin": "skntransLeftLabel",
        "text": kony.i18n.getLocalizedString("i18n.common.fromc"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    fromLblFlex.add(fromLabel);
    fromAccountPick.add(fromAccountNameContainer, fromAccountAmountContainer, editFromCard, fromlblAccountNumberKA, fromLblFlex);
    var lblAlerts1 = new kony.ui.Label({
        "centerX": "50%",
        "height": "60dp",
        "id": "lblAlerts1",
        "isVisible": false,
        "skin": "sknfrontback30363FwhiteBgKA",
        "text": kony.i18n.getLocalizedString("i18n.alerts.NofromAccounts"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    fromCard.add(fromCardInner, fromCardTitle, fromAccountPick, lblAlerts1);
    var toCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "toCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0320dc8ccb87b4d",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    toCard.setDefaultUnit(kony.flex.DP);
    var toCardInner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "toCardInner",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "3%",
        "isModalContainer": false,
        "skin": "skntransferCardInner",
        "top": "60dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    toCardInner.setDefaultUnit(kony.flex.DP);
    var toInternalLabel = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "toInternalLabel",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0581119363ce54c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    toInternalLabel.setDefaultUnit(kony.flex.DP);
    var CopyborderBottom06399950388e345 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyborderBottom06399950388e345",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0a29a14ecfe6442",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    CopyborderBottom06399950388e345.setDefaultUnit(kony.flex.DP);
    CopyborderBottom06399950388e345.add();
    var Label0cecf1132bf8049 = new kony.ui.Label({
        "centerY": "50%",
        "id": "Label0cecf1132bf8049",
        "isVisible": true,
        "left": "4%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.transfer.xyzBankAccounts"),
        "top": "0dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    toInternalLabel.add(CopyborderBottom06399950388e345, Label0cecf1132bf8049);
    var segInternalTOAccountsKA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "data": [{
            "Label0bed5640cdb4b46": "Label",
            "amountAccount1": "$00.00",
            "amountcurrBal": "$00.00",
            "amtOutsatndingBal": "$00.00",
            "btnNav": "L",
            "lblAccountID": "Label",
            "lblColorKA": "",
            "lblCurrency": "JOD",
            "lblIncommingRing": "s",
            "lblMobile": "S",
            "lblRowSeparator": "Label",
            "nameAccount1": "RichText",
            "nameAccount12": "Account Name",
            "typeAccount": "Available Balance",
            "typeKA": ""
        }, {
            "Label0bed5640cdb4b46": "Label",
            "amountAccount1": "$00.00",
            "amountcurrBal": "$00.00",
            "amtOutsatndingBal": "$00.00",
            "btnNav": "L",
            "lblAccountID": "Label",
            "lblColorKA": "",
            "lblCurrency": "JOD",
            "lblIncommingRing": "s",
            "lblMobile": "S",
            "lblRowSeparator": "Label",
            "nameAccount1": "RichText",
            "nameAccount12": "Account Name",
            "typeAccount": "Available Balance",
            "typeKA": ""
        }, {
            "Label0bed5640cdb4b46": "Label",
            "amountAccount1": "$00.00",
            "amountcurrBal": "$00.00",
            "amtOutsatndingBal": "$00.00",
            "btnNav": "L",
            "lblAccountID": "Label",
            "lblColorKA": "",
            "lblCurrency": "JOD",
            "lblIncommingRing": "s",
            "lblMobile": "S",
            "lblRowSeparator": "Label",
            "nameAccount1": "RichText",
            "nameAccount12": "Account Name",
            "typeAccount": "Available Balance",
            "typeKA": ""
        }],
        "groupCells": false,
        "id": "segInternalTOAccountsKA",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_627c6523c8d74d34acdef09badc22022,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": yourAccount1,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopylblIncommingRing0ac2c9e9c52d147": "CopylblIncommingRing0ac2c9e9c52d147",
            "Label0bed5640cdb4b46": "Label0bed5640cdb4b46",
            "amountAccount1": "amountAccount1",
            "amountcurrBal": "amountcurrBal",
            "amtOutsatndingBal": "amtOutsatndingBal",
            "btnNav": "btnNav",
            "btnNav1": "btnNav1",
            "dummyAccountName": "dummyAccountName",
            "dummyAccountNumber": "dummyAccountNumber",
            "flxLine": "flxLine",
            "flxSet": "flxSet",
            "flxamount": "flxamount",
            "isPFMLabel": "isPFMLabel",
            "lblAccNumber": "lblAccNumber",
            "lblAccountID": "lblAccountID",
            "lblBankName": "lblBankName",
            "lblColorKA": "lblColorKA",
            "lblCurrency": "lblCurrency",
            "lblIncommingRing": "lblIncommingRing",
            "lblMobile": "lblMobile",
            "lblRowSeparator": "lblRowSeparator",
            "nameAccount1": "nameAccount1",
            "nameAccount12": "nameAccount12",
            "nameContainer": "nameContainer",
            "typeAccount": "typeAccount",
            "typeKA": "typeKA",
            "yourAccount1": "yourAccount1"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblAlerts = new kony.ui.Label({
        "centerX": "50%",
        "height": "60dp",
        "id": "lblAlerts",
        "isVisible": false,
        "skin": "sknfrontback30363FwhiteBgKA",
        "text": kony.i18n.getLocalizedString("i18n.alerts.NoAccountsMsg"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var addExternalAccount = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "58dp",
        "id": "addExternalAccount",
        "isVisible": true,
        "onClick": AS_Button_fd389e2eb47247a6891b5bb124a9f8d7,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.addExternalAccount"),
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyborderBottom0i6244ff99e484e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "-10dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyborderBottom0i6244ff99e484e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0a29a14ecfe6442",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyborderBottom0i6244ff99e484e.setDefaultUnit(kony.flex.DP);
    CopyborderBottom0i6244ff99e484e.add();
    var btnViewAllKA = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "58dp",
        "id": "btnViewAllKA",
        "isVisible": true,
        "onClick": AS_Button_if1db169e591421b93f95c0202b765bb,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.transfer.viewAll"),
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    toCardInner.add(toInternalLabel, segInternalTOAccountsKA, lblAlerts, addExternalAccount, CopyborderBottom0i6244ff99e484e, btnViewAllKA);
    var toCardTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "60dp",
        "id": "toCardTitle",
        "isVisible": true,
        "skin": "skntransferPaySectionHeader",
        "text": kony.i18n.getLocalizedString("i18n.transfer.transferMoneyToC"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var toAccountPick = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknyourAccountCardFocus",
        "height": "84dp",
        "id": "toAccountPick",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntransferPayCardPick",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    toAccountPick.setDefaultUnit(kony.flex.DP);
    var toAccountNameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "toAccountNameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "44%",
        "zIndex": 1
    }, {}, {});
    toAccountNameContainer.setDefaultUnit(kony.flex.DP);
    var toNamePick = new kony.ui.Label({
        "centerY": "50%",
        "id": "toNamePick",
        "isVisible": true,
        "left": "13dp",
        "maxWidth": "90%",
        "skin": "sknaccountNamePick",
        "text": kony.i18n.getLocalizedString("i18n.common.accountName"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var tolblAccountTypeKA = new kony.ui.Label({
        "centerY": "60%",
        "id": "tolblAccountTypeKA",
        "isVisible": false,
        "left": "23dp",
        "maxWidth": "90%",
        "skin": "sknaccountNamePick",
        "text": kony.i18n.getLocalizedString("i18n.common.accountName"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var toAccountColorPick = new kony.ui.Label({
        "centerY": "60%",
        "height": "50%",
        "id": "toAccountColorPick",
        "isVisible": true,
        "left": "1dp",
        "skin": "sknaccountNamePick",
        "width": "4dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var toAccountBankNameKA = new kony.ui.Label({
        "id": "toAccountBankNameKA",
        "isVisible": true,
        "left": "13dp",
        "skin": "skntransLeftLabel",
        "text": "XYZ Bank",
        "top": "55dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    toAccountNameContainer.add(toNamePick, tolblAccountTypeKA, toAccountColorPick, toAccountBankNameKA);
    var toAccountAmountContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "toAccountAmountContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "11%",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "35%",
        "zIndex": 1
    }, {}, {});
    toAccountAmountContainer.setDefaultUnit(kony.flex.DP);
    var toAmountPick = new kony.ui.Label({
        "centerY": "50%",
        "id": "toAmountPick",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknaccountAmountSelected",
        "text": "$00.00",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyamountAccountOne03ee5831d718349 = new kony.ui.Label({
        "bottom": "13dp",
        "id": "CopyamountAccountOne03ee5831d718349",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknaccountAvailableBalanceLabelSelected",
        "text": kony.i18n.getLocalizedString("i18n.deposit.availableBalance"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    toAccountAmountContainer.add(toAmountPick, CopyamountAccountOne03ee5831d718349);
    var editToCard = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "skneditFormFocus",
        "height": "100%",
        "id": "editToCard",
        "isVisible": true,
        "onClick": AS_Button_49f8bbb9dddc498c885e6a9ba84aa6d1,
        "right": "0dp",
        "skin": "skneditForm",
        "text": kony.i18n.getLocalizedString("i18n.common.edit"),
        "width": "56dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var tolblAccountNumberKA = new kony.ui.Label({
        "id": "tolblAccountNumberKA",
        "isVisible": false,
        "left": "115dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var toLblFlex = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "toLblFlex",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    toLblFlex.setDefaultUnit(kony.flex.DP);
    var toLabel = new kony.ui.Label({
        "id": "toLabel",
        "isVisible": true,
        "left": "13dp",
        "skin": "skntransLeftLabel",
        "text": kony.i18n.getLocalizedString("i18n.common.To"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    toLblFlex.add(toLabel);
    toAccountPick.add(toAccountNameContainer, toAccountAmountContainer, editToCard, tolblAccountNumberKA, toLblFlex);
    var lblTransactionIdKA = new kony.ui.Label({
        "id": "lblTransactionIdKA",
        "isVisible": false,
        "left": "14dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    toCard.add(toCardInner, toCardTitle, toAccountPick, lblTransactionIdKA);
    var amountCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "120dp",
        "id": "amountCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox007a422d071dd4e",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    amountCard.setDefaultUnit(kony.flex.DP);
    var amountCardInner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "amountCardInner",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntransferCardInner",
        "top": "60dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    amountCardInner.setDefaultUnit(kony.flex.DP);
    var CopyamountWrapper0be5b03b6e9a449 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "60dp",
        "id": "CopyamountWrapper0be5b03b6e9a449",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox05b3140035fa546",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyamountWrapper0be5b03b6e9a449.setDefaultUnit(kony.flex.DP);
    var lblCurrencyType = new kony.ui.Label({
        "id": "lblCurrencyType",
        "isVisible": true,
        "left": "5%",
        "skin": "sknCopyslLabel09de2a4a1c0e745",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var amountTextContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "amountTextContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    amountTextContainer.setDefaultUnit(kony.flex.DP);
    var amountTextField = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknamountEntryField",
        "height": "60dp",
        "id": "amountTextField",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "left": "0dp",
        "maxTextLength": null,
        "onTextChange": AS_TextField_f3d3890a736346abade68994746c8249,
        "placeholder": "0",
        "secureTextEntry": false,
        "skin": "sknamountEntryField",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknamountEntryField",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblInvalidAmount = new kony.ui.Label({
        "bottom": "5%",
        "id": "lblInvalidAmount",
        "isVisible": true,
        "right": "4%",
        "skin": "sknD0021BFLatoRegular",
        "text": "Invalid Amount",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    amountTextContainer.add(amountTextField, lblInvalidAmount);
    CopyamountWrapper0be5b03b6e9a449.add(lblCurrencyType, amountTextContainer);
    amountCardInner.add(CopyamountWrapper0be5b03b6e9a449);
    var amountTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "60dp",
        "id": "amountTitle",
        "isVisible": true,
        "left": "0dp",
        "skin": "skntransferPaySectionHeader",
        "text": kony.i18n.getLocalizedString("i18n.deposit.amount"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    amountCard.add(amountCardInner, amountTitle);
    var transferNotes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "transferNotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox007a422d071dd4e",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    transferNotes.setDefaultUnit(kony.flex.DP);
    var transferNotesPick = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "transferNotesPick",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntransferPayCardPick",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    transferNotesPick.setDefaultUnit(kony.flex.DP);
    var transferNotesPickContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "transferNotesPickContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    transferNotesPickContainer.setDefaultUnit(kony.flex.DP);
    var lblTransferNotes = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_SENTENCES,
        "bottom": "0dp",
        "focusSkin": "sknloginTextField",
        "height": "100%",
        "id": "lblTransferNotes",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "13dp",
        "maxTextLength": null,
        "placeholder": kony.i18n.getLocalizedString("i18n.transfers.EnterOptionalNotePl"),
        "secureTextEntry": false,
        "skin": "sknloginTextField",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 4, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknloginTextField",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblNote = new kony.ui.Label({
        "id": "lblNote",
        "isVisible": true,
        "left": "13dp",
        "skin": "sknTransperantLabelKA",
        "text": kony.i18n.getLocalizedString("i18n.common.notec"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    transferNotesPickContainer.add(lblTransferNotes, lblNote);
    transferNotesPick.add(transferNotesPickContainer);
    transferNotes.add(transferNotesPick);
    var frequencyCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "frequencyCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox007a422d071dd4e",
        "top": "1dp",
        "width": "100%"
    }, {}, {});
    frequencyCard.setDefaultUnit(kony.flex.DP);
    var frequencyCardInner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "frequencyCardInner",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "3%",
        "isModalContainer": false,
        "skin": "skntransferCardInner",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    frequencyCardInner.setDefaultUnit(kony.flex.DP);
    var segFrequency = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "contactListDivider": "Label",
            "imgicontick": "imagedrag.png",
            "lblNameKA": "Example"
        }, {
            "contactListDivider": "Label",
            "imgicontick": "imagedrag.png",
            "lblNameKA": "Example"
        }, {
            "contactListDivider": "Label",
            "imgicontick": "imagedrag.png",
            "lblNameKA": "Example"
        }],
        "groupCells": false,
        "id": "segFrequency",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_57c73953248745ff9072abc48c58229c,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "Copyseg06c3e5242bee446",
        "rowTemplate": CopyFlexContainer0b2b1c26ffbf74f,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "62dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopyFlexContainer0b2b1c26ffbf74f": "CopyFlexContainer0b2b1c26ffbf74f",
            "contactListDivider": "contactListDivider",
            "imgicontick": "imgicontick",
            "lblNameKA": "lblNameKA"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    frequencyCardInner.add(segFrequency);
    var frequencyTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "60dp",
        "id": "frequencyTitle",
        "isVisible": true,
        "skin": "skntransferPaySectionHeader",
        "text": kony.i18n.getLocalizedString("i18n.transfer.frequencyC"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var frequencyPick = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknyourAccountCardFocus",
        "height": "62dp",
        "id": "frequencyPick",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntransferPayCard",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    frequencyPick.setDefaultUnit(kony.flex.DP);
    var frequencyPickContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "frequencyPickContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "65.63%",
        "zIndex": 1
    }, {}, {});
    frequencyPickContainer.setDefaultUnit(kony.flex.DP);
    var frequencyPickLabel = new kony.ui.Label({
        "centerY": "65%",
        "id": "frequencyPickLabel",
        "isVisible": true,
        "left": "13dp",
        "skin": "sknaccountNamePick",
        "text": kony.i18n.getLocalizedString("i18n.transfer.oneTime"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    frequencyPickContainer.add(frequencyPickLabel);
    var frequencyLblFlex = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "frequencyLblFlex",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    frequencyLblFlex.setDefaultUnit(kony.flex.DP);
    var frequencyLabel = new kony.ui.Label({
        "id": "frequencyLabel",
        "isVisible": true,
        "left": "13dp",
        "skin": "skntransLeftLabel",
        "text": kony.i18n.getLocalizedString("i18n.transfer.frequencyC"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    frequencyLblFlex.add(frequencyLabel);
    var frequencyEdit = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "skneditFormFocus",
        "height": "100%",
        "id": "frequencyEdit",
        "isVisible": true,
        "onClick": AS_Button_923ccdbc8ff640caa5b4421df9a18bbf,
        "right": "0dp",
        "skin": "skneditForm",
        "text": kony.i18n.getLocalizedString("i18n.common.edit"),
        "width": "56dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    frequencyPick.add(frequencyPickContainer, frequencyLblFlex, frequencyEdit);
    frequencyCard.add(frequencyCardInner, frequencyTitle, frequencyPick);
    var dateCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "dateCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox007a422d071dd4e",
        "top": "1dp",
        "width": "100%"
    }, {}, {});
    dateCard.setDefaultUnit(kony.flex.DP);
    var datePick = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknyourAccountCardFocus",
        "height": "62dp",
        "id": "datePick",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntransferPayCard",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    datePick.setDefaultUnit(kony.flex.DP);
    var datePickContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "datePickContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "zIndex": 1
    }, {}, {});
    datePickContainer.setDefaultUnit(kony.flex.DP);
    var calDateKA = new kony.ui.Calendar({
        "calendarIcon": "calendar.png",
        "centerY": "65%",
        "dateFormat": "dd/MMM/yyyy",
        "id": "calDateKA",
        "isVisible": true,
        "left": "13dp",
        "skin": "sknstandardDatepicker",
        "validEndDate": [1, 6, 2017],
        "validStartDate": [31, 5, 2016],
        "viewConfig": {
            "gridConfig": {
                "gridCellSelectedSkin": undefined,
                "gridCellSkin": undefined,
                "gridCellTodaySkin": undefined
            }
        },
        "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var dateLabel = new kony.ui.Label({
        "id": "dateLabel",
        "isVisible": true,
        "left": "13dp",
        "skin": "skntransLeftLabel",
        "text": "Date: ",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    datePickContainer.add(calDateKA, dateLabel);
    datePick.add(datePickContainer);
    dateCard.add(datePick);
    var recurrenceCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "recurrenceCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox007a422d071dd4e",
        "top": "1dp",
        "width": "100%"
    }, {}, {});
    recurrenceCard.setDefaultUnit(kony.flex.DP);
    var recurrenceCardInner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "recurrenceCardInner",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "3%",
        "isModalContainer": false,
        "skin": "skntransferCardInner",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    recurrenceCardInner.setDefaultUnit(kony.flex.DP);
    var flexNumberOfTimes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "76dp",
        "id": "flexNumberOfTimes",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0581119363ce54c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexNumberOfTimes.setDefaultUnit(kony.flex.DP);
    var lblNumberOfTimes = new kony.ui.Label({
        "height": "38dp",
        "id": "lblNumberOfTimes",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsegmentHeaderText",
        "text": "Number of Recurrences",
        "top": "0dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var tbxNumberOfTimes = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknloginTextField",
        "height": "38dp",
        "id": "tbxNumberOfTimes",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "left": "5%",
        "maxTextLength": null,
        "onDone": AS_TextField_674ba3f5a92b4b92aa00d87c16dc1304,
        "placeholder": "Enter Number of Recurrence",
        "secureTextEntry": false,
        "skin": "sknloginTextField",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 2, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onEndEditing": AS_TextField_3a810bdc955242e8ab94af3a3ea47d27,
        "placeholderSkin": "sknloginTextField",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flexNumberOfTimes.add(lblNumberOfTimes, tbxNumberOfTimes);
    var CopyborderBottom05128815acf3944 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyborderBottom05128815acf3944",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0a29a14ecfe6442",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    CopyborderBottom05128815acf3944.setDefaultUnit(kony.flex.DP);
    CopyborderBottom05128815acf3944.add();
    var flexRecurrenceDateRange = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "76dp",
        "id": "flexRecurrenceDateRange",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0581119363ce54c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flexRecurrenceDateRange.setDefaultUnit(kony.flex.DP);
    var lblDateRange = new kony.ui.Label({
        "height": "38dp",
        "id": "lblDateRange",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.transfers.DateRange"),
        "top": "0dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexRecurrenceCalendars = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "flexRecurrenceCalendars",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0581119363ce54c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flexRecurrenceCalendars.setDefaultUnit(kony.flex.DP);
    var flexRecurrenceFromContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "flexRecurrenceFromContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "45%",
        "zIndex": 1
    }, {}, {});
    flexRecurrenceFromContainer.setDefaultUnit(kony.flex.DP);
    var calRecurrenceFrom = new kony.ui.Calendar({
        "calendarIcon": "calbtn.png",
        "dateComponents": [10, 6, 2016, 0, 0, 0],
        "dateFormat": "dd/MMM/yyyy",
        "day": 10,
        "formattedDate": "10/Jun/2016",
        "height": "38dp",
        "hour": 0,
        "id": "calRecurrenceFrom",
        "isVisible": true,
        "left": "0dp",
        "minWidth": "100%",
        "minutes": 0,
        "month": 6,
        "onSelection": AS_Calendar_f1831fc55cd843f98b8b008f0cfdcfca,
        "seconds": 0,
        "skin": "sknstandardDatepicker",
        "top": "0dp",
        "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
        "width": "100%",
        "year": 2016,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexRecurrenceFromContainer.add(calRecurrenceFrom);
    var flexRecurrenceToContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "flexRecurrenceToContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flexRecurrenceToContainer.setDefaultUnit(kony.flex.DP);
    var calRecurrenceTo = new kony.ui.Calendar({
        "calendarIcon": "calbtn.png",
        "dateComponents": [10, 6, 2016, 0, 0, 0],
        "dateFormat": "dd/MMM/yyyy",
        "day": 10,
        "formattedDate": "10/Jun/2016",
        "height": "38dp",
        "hour": 0,
        "id": "calRecurrenceTo",
        "isVisible": true,
        "left": "0dp",
        "minWidth": "85%",
        "minutes": 0,
        "month": 6,
        "onSelection": AS_Calendar_df8365c63127411da6ce6b47d86463eb,
        "seconds": 0,
        "skin": "sknstandardDatepicker",
        "top": "0dp",
        "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
        "width": "85%",
        "year": 2016,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexRecurrenceToContainer.add(calRecurrenceTo);
    flexRecurrenceCalendars.add(flexRecurrenceFromContainer, flexRecurrenceToContainer);
    flexRecurrenceDateRange.add(lblDateRange, flexRecurrenceCalendars);
    recurrenceCardInner.add(flexNumberOfTimes, CopyborderBottom05128815acf3944, flexRecurrenceDateRange);
    var recurrenceTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "60dp",
        "id": "recurrenceTitle",
        "isVisible": true,
        "skin": "skntransferPaySectionHeader",
        "text": kony.i18n.getLocalizedString("i18n.transfers.RecurrenceC"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var recurrencePick = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknyourAccountCardFocus",
        "height": "62dp",
        "id": "recurrencePick",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntransferPayCard",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    recurrencePick.setDefaultUnit(kony.flex.DP);
    var CopyborderBottom093c77f9c0f454c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyborderBottom093c77f9c0f454c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0a29a14ecfe6442",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyborderBottom093c77f9c0f454c.setDefaultUnit(kony.flex.DP);
    CopyborderBottom093c77f9c0f454c.add();
    var recurrencePickContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "recurrencePickContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "65.63%",
        "zIndex": 1
    }, {}, {});
    recurrencePickContainer.setDefaultUnit(kony.flex.DP);
    var recurrencePickLabel = new kony.ui.Label({
        "centerY": "65%",
        "id": "recurrencePickLabel",
        "isVisible": true,
        "left": "13dp",
        "skin": "sknaccountNamePick",
        "text": kony.i18n.getLocalizedString("i18n.transfer.oneTime"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    recurrencePickContainer.add(recurrencePickLabel);
    var recurrenceLblFlex = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "recurrenceLblFlex",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    recurrenceLblFlex.setDefaultUnit(kony.flex.DP);
    var recurrenceLabel = new kony.ui.Label({
        "id": "recurrenceLabel",
        "isVisible": true,
        "left": "13dp",
        "skin": "skntransLeftLabel",
        "text": kony.i18n.getLocalizedString("i18n.transfers.RecurrenceC"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    recurrenceLblFlex.add(recurrenceLabel);
    var recurrenceEdit = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "skneditFormFocus",
        "height": "100%",
        "id": "recurrenceEdit",
        "isVisible": true,
        "onClick": AS_Button_3dc6f44b77c1439eb7f17c723db7c2ae,
        "right": "0dp",
        "skin": "skneditForm",
        "text": kony.i18n.getLocalizedString("i18n.common.edit"),
        "width": "56dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    recurrencePick.add(CopyborderBottom093c77f9c0f454c, recurrencePickContainer, recurrenceLblFlex, recurrenceEdit);
    recurrenceCard.add(recurrenceCardInner, recurrenceTitle, recurrencePick);
    var confirmContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "250dp",
        "id": "confirmContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0cb1e5738a8eb4e",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    confirmContainer.setDefaultUnit(kony.flex.DP);
    var confirmTransaction = new kony.ui.Button({
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "confirmTransaction",
        "isVisible": true,
        "left": "10%",
        "onClick": AS_Button_c9e9ab8f98bd4c3c837e50495f4b120c,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.transfer.confirmTransferC"),
        "top": "20dp",
        "width": "80%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    confirmContainer.add(confirmTransaction);
    mainContainer.add(titleBarNewTransfer, fromCard, toCard, amountCard, transferNotes, frequencyCard, dateCard, recurrenceCard, confirmContainer);
    var FlexContainer0bf4a82f408fe42 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "FlexContainer0bf4a82f408fe42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b7d74518f07a4d",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0bf4a82f408fe42.setDefaultUnit(kony.flex.DP);
    var Button0f1eb0c6bd0974f = new kony.ui.Button({
        "focusSkin": "btnBackFoc0a4aadf8ca3bb4a",
        "height": "100%",
        "id": "Button0f1eb0c6bd0974f",
        "isVisible": false,
        "left": "0dp",
        "onClick": AS_Button_b1414c9cb5bd44d1b5d8602a9f1306fd,
        "skin": "sknBtnBack",
        "text": "j",
        "top": "0dp",
        "width": "53dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label0g6814cba9e7841 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "Label0g6814cba9e7841",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.transfers.Transfer"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxNext = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "31dp",
        "id": "flxNext",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "2.04%",
        "skin": "slFbox",
        "top": "11.75%",
        "width": "17.33%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxNext.setDefaultUnit(kony.flex.DP);
    var lblNext = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Next Page"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblNext",
        "isVisible": true,
        "left": "0dp",
        "onTouchStart": AS_Label_ecd8dfd4f91f4d50aebfb74ccce3a212,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": true
    }, {
        "textCopyable": false
    });
    flxNext.add(lblNext);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_iecb56689d7a494e8507f0358d52da26,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true,
        "retainFlowHorizontalAlignment": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    FlexContainer0bf4a82f408fe42.add(Button0f1eb0c6bd0974f, Label0g6814cba9e7841, flxNext, flxBack);
    var flxMain = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "90%",
        "horizontalScrollIndicator": true,
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox0dba49a84680543",
        "top": "10%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var flxAccount1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxAccount1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0f0c75590716b41",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxAccount1.setDefaultUnit(kony.flex.DP);
    var lblFrom = new kony.ui.Label({
        "height": "35%",
        "id": "lblFrom",
        "isVisible": true,
        "left": "20dp",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.from"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAcc1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxAcc1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.09%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0.00%",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxAcc1.setDefaultUnit(kony.flex.DP);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "45dp",
        "id": "flxIcon1",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "6%",
        "isModalContainer": false,
        "skin": "sknFlxToIcon",
        "top": "10%",
        "width": "45dp",
        "zIndex": 100
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblIcon1 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblIcon1",
        "isVisible": true,
        "left": "19dp",
        "onTouchEnd": AS_Label_e258f2739b1a4ce0a772fe869d2de4fe,
        "skin": "sknLblFromIcon",
        "text": "BH",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "13dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon1.add(lblIcon1);
    var lblAccountName1 = new kony.ui.Label({
        "bottom": "0%",
        "height": "40%",
        "id": "lblAccountName1",
        "isVisible": true,
        "left": "21%",
        "skin": "sknLblAccNum",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber1 = new kony.ui.Label({
        "height": "35%",
        "id": "lblAccountNumber1",
        "isVisible": true,
        "left": "21%",
        "skin": "sknSmallForIban",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": "72%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnForward1 = new kony.ui.Button({
        "focusSkin": "sknBtnForwardDimmed",
        "height": "100%",
        "id": "btnForward1",
        "isVisible": true,
        "right": 0,
        "skin": "sknBtnForwardDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "top": "0dp",
        "width": "53dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopylblLine0i25ba22a14b94a = new kony.ui.Label({
        "height": "1.50%",
        "id": "CopylblLine0i25ba22a14b94a",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblsknToandFromAccLine",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblLine0a56eccb1a64345 = new kony.ui.Label({
        "bottom": "8%",
        "height": "1.50%",
        "id": "CopylblLine0a56eccb1a64345",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblsknToandFromAccLine",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAcc1.add(flxIcon1, lblAccountName1, lblAccountNumber1, btnForward1, CopylblLine0i25ba22a14b94a, CopylblLine0a56eccb1a64345);
    var lblSelectanAccount1 = new kony.ui.Label({
        "centerY": "-35%",
        "id": "lblSelectanAccount1",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.selectAcc"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFromAccCurr = new kony.ui.Label({
        "id": "lblFromAccCurr",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknTrans",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAccount1.add(lblFrom, flxAcc1, lblSelectanAccount1, lblFromAccCurr);
    var lblInvalidFromAccount = new kony.ui.Label({
        "id": "lblInvalidFromAccount",
        "isVisible": false,
        "right": "5%",
        "skin": "sknInvalidCredKA",
        "text": kony.i18n.getLocalizedString("i18.Transfer.enterFromAcc"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAccount2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxAccount2",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0f0c75590716b41",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxAccount2.setDefaultUnit(kony.flex.DP);
    var lblTo = new kony.ui.Label({
        "height": "35%",
        "id": "lblTo",
        "isVisible": true,
        "left": "20dp",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.common.To"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAcc2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxAcc2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.04%",
        "isModalContainer": false,
        "onTouchStart": AS_FlexContainer_c2307be089a04dca84eabfed19332cf6,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAcc2.setDefaultUnit(kony.flex.DP);
    var flxIcon2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "45dp",
        "id": "flxIcon2",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "6%",
        "isModalContainer": false,
        "skin": "sknFlxToIcon",
        "top": "10%",
        "width": "45dp",
        "zIndex": 1
    }, {}, {});
    flxIcon2.setDefaultUnit(kony.flex.DP);
    var lblIcon2 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblIcon2",
        "isVisible": true,
        "left": "19dp",
        "skin": "sknLblFromIcon",
        "text": "k",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "13dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon2.add(lblIcon2);
    var lblAccountName2 = new kony.ui.Label({
        "bottom": "0%",
        "height": "42%",
        "id": "lblAccountName2",
        "isVisible": false,
        "left": "21%",
        "skin": "sknLblAccNum",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8.99%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber2 = new kony.ui.Label({
        "height": "35%",
        "id": "lblAccountNumber2",
        "isVisible": false,
        "left": "21%",
        "skin": "sknSmallForIban",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": "72%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnForward2 = new kony.ui.Button({
        "focusSkin": "sknBtnForwardDimmed",
        "height": "100%",
        "id": "btnForward2",
        "isVisible": true,
        "right": 0,
        "skin": "sknBtnForwardDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "top": "0dp",
        "width": "53dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopylblLine0hd66bb44c34643 = new kony.ui.Label({
        "height": "1.50%",
        "id": "CopylblLine0hd66bb44c34643",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblsknToandFromAccLine",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblLine0b03cf015d52c4a = new kony.ui.Label({
        "bottom": "8%",
        "height": "1.50%",
        "id": "CopylblLine0b03cf015d52c4a",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblsknToandFromAccLine",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAcc2.add(flxIcon2, lblAccountName2, lblAccountNumber2, btnForward2, CopylblLine0hd66bb44c34643, CopylblLine0b03cf015d52c4a);
    var lblSelectanAccount2 = new kony.ui.Label({
        "centerY": "-35%",
        "id": "lblSelectanAccount2",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.selectAcc"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblToAccCurr = new kony.ui.Label({
        "id": "lblToAccCurr",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknTrans",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAccount2.add(lblTo, flxAcc2, lblSelectanAccount2, lblToAccCurr);
    var lblInvalidToAccount = new kony.ui.Label({
        "id": "lblInvalidToAccount",
        "isVisible": false,
        "right": "5%",
        "skin": "sknInvalidCredKA",
        "text": kony.i18n.getLocalizedString("i18.Transfer.enterToAcc"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "18%",
        "id": "flxAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0f0c75590716b41",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAmount.setDefaultUnit(kony.flex.DP);
    var txtBox = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0f4bacc0b630b4f",
        "height": "48%",
        "id": "txtBox",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "left": "20dp",
        "maxTextLength": 9,
        "onDone": AS_TextField_d89cd9d4d9804a29b8b9d35051f88f18,
        "onTextChange": AS_TextField_b751b9b023d545c8bdcbe3e5725b9a1b,
        "onTouchStart": AS_TextField_e529c8d4c0a9419491861170617350c6,
        "placeholder": "0.000",
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "19%",
        "width": "82%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "onEndEditing": AS_TextField_h38731e15fc54580a10f911f0a3e069f,
        "placeholderSkin": "sknPlaceHolder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lbl = new kony.ui.Label({
        "height": "32%",
        "id": "lbl",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurrencylocal = new kony.ui.Label({
        "height": "55%",
        "id": "lblCurrencylocal",
        "isVisible": false,
        "right": "15%",
        "skin": "lblCurrency0e6155741e27a4c",
        "text": kony.i18n.getLocalizedString("i18n.common.currencySymbol"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLine3 = new kony.ui.Label({
        "centerX": "50%",
        "height": "1%",
        "id": "lblLine3",
        "isVisible": true,
        "left": "20dp",
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "62%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCahngeCurr = new kony.ui.Button({
        "focusSkin": "btnBackFoc0a4aadf8ca3bb4a",
        "height": "70%",
        "id": "lblCahngeCurr",
        "isVisible": false,
        "right": 0,
        "skin": "sknBtnForwardDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "top": "0dp",
        "width": "53dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var FlexContainer0b7dc31ecc5cf40 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "FlexContainer0b7dc31ecc5cf40",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "65%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0b7dc31ecc5cf40.setDefaultUnit(kony.flex.DP);
    var lblVal = new kony.ui.Label({
        "id": "lblVal",
        "isVisible": true,
        "left": "20dp",
        "skin": "sknLblCurr",
        "text": "0.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFromCurr = new kony.ui.Label({
        "id": "lblFromCurr",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": "1 USD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblToCurr = new kony.ui.Label({
        "id": "lblToCurr",
        "isVisible": true,
        "left": "22%",
        "skin": "sknLblCurr",
        "text": "0.746464 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEquals = new kony.ui.Label({
        "id": "lblEquals",
        "isVisible": true,
        "left": "20%",
        "skin": "sknLblCurr",
        "text": "=",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0b7dc31ecc5cf40.add(lblVal, lblFromCurr, lblToCurr, lblEquals);
    var lblAvailableLimit = new kony.ui.Label({
        "height": "35%",
        "id": "lblAvailableLimit",
        "isVisible": false,
        "right": "6%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.transfer.frequencyC"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "60%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAmount.add(txtBox, lbl, lblCurrencylocal, lblLine3, lblCahngeCurr, FlexContainer0b7dc31ecc5cf40, lblAvailableLimit);
    var flxInvalidAmountField = new kony.ui.Label({
        "id": "flxInvalidAmountField",
        "isVisible": false,
        "right": "5%",
        "skin": "sknInvalidCredKA",
        "text": kony.i18n.getLocalizedString("i18.Transfer.enterAmt"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-1%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxChooseCurrency = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxChooseCurrency",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxChooseCurrency.setDefaultUnit(kony.flex.DP);
    var lblChooseCurrencyTitle = new kony.ui.Label({
        "height": "35%",
        "id": "lblChooseCurrencyTitle",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18.Transfer.chooseCurrency"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurrency = new kony.ui.Label({
        "bottom": "0%",
        "centerX": "50%",
        "height": "48%",
        "id": "lblCurrency",
        "isVisible": true,
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "28%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblChooseCurrencyLine = new kony.ui.Label({
        "centerX": "50%",
        "height": "2%",
        "id": "lblChooseCurrencyLine",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "77%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxChooseCurrencyArrow = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxChooseCurrencyArrow",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_Button_a981eea9879649daa9cd7b23890f7481,
        "right": "0dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "53dp",
        "zIndex": 1
    }, {}, {});
    flxChooseCurrencyArrow.setDefaultUnit(kony.flex.DP);
    var lblChooseCurrencyArrow = new kony.ui.Label({
        "centerX": "50%",
        "height": "100%",
        "id": "lblChooseCurrencyArrow",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblEditDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxChooseCurrencyArrow.add(lblChooseCurrencyArrow);
    flxChooseCurrency.add(lblChooseCurrencyTitle, lblCurrency, lblChooseCurrencyLine, flxChooseCurrencyArrow);
    var flxFrequency = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxFrequency",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0f0c75590716b41",
        "top": "0%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxFrequency.setDefaultUnit(kony.flex.DP);
    var lblFrequency = new kony.ui.Label({
        "height": "35%",
        "id": "lblFrequency",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.transfer.frequencyC"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFre = new kony.ui.Label({
        "bottom": "0%",
        "centerX": "50%",
        "height": "48%",
        "id": "lblFre",
        "isVisible": true,
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.Transfers.Instant"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "28%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLine4 = new kony.ui.Label({
        "centerX": "50%",
        "height": "2%",
        "id": "lblLine4",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "77%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxEdit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxEdit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "53dp",
        "zIndex": 1
    }, {}, {});
    flxEdit.setDefaultUnit(kony.flex.DP);
    var lblEdit = new kony.ui.Label({
        "height": "100%",
        "id": "lblEdit",
        "isVisible": true,
        "left": "0dp",
        "onTouchStart": AS_Label_f1a563b73db34019bf8526ea5be2031a,
        "skin": "sknLblEditDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEdit.add(lblEdit);
    flxFrequency.add(lblFrequency, lblFre, lblLine4, flxEdit);
    var flxRecurrence = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "43%",
        "id": "flxRecurrence",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRecurrence.setDefaultUnit(kony.flex.DP);
    var lblRecurrence = new kony.ui.Label({
        "bottom": "0%",
        "centerX": "50%",
        "height": "18%",
        "id": "lblRecurrence",
        "isVisible": true,
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.transfers.Recurrence"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblLine0a5155529c3d848 = new kony.ui.Label({
        "centerX": "50%",
        "height": "1%",
        "id": "CopylblLine0a5155529c3d848",
        "isVisible": false,
        "skin": "lblsknToandFromAccLine",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxFrequency0d4dacf1e8f8646 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "34%",
        "id": "CopyflxFrequency0d4dacf1e8f8646",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "75%",
        "zIndex": 1
    }, {}, {});
    CopyflxFrequency0d4dacf1e8f8646.setDefaultUnit(kony.flex.DP);
    var lblNumRecurr = new kony.ui.Label({
        "id": "lblNumRecurr",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblAccNum",
        "text": kony.i18n.getLocalizedString("i18n.transfers.NumberOfRecurrences"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtNumRecurrences = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "txtNumRecurrences",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": 2,
        "onDone": AS_TextField_c5a7dadb1ff643d4b6cfcfd6a1cb1b24,
        "onTextChange": AS_TextField_eda36ce490714eef8c395d979758a078,
        "onTouchEnd": AS_TextField_e3c04c46178f45fe87dc8bb8d34eba8a,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "100%",
        "zIndex": 10
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onEndEditing": AS_TextField_bc7e115dbfd148f3b3fcba6bbed61631,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    CopyflxFrequency0d4dacf1e8f8646.add(lblNumRecurr, txtNumRecurrences);
    var CopylblLine0iff8accf05a247 = new kony.ui.Label({
        "centerX": "50%",
        "height": "1%",
        "id": "CopylblLine0iff8accf05a247",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-5%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblInvalidRecurrence = new kony.ui.Label({
        "id": "lblInvalidRecurrence",
        "isVisible": false,
        "right": "5%",
        "skin": "sknInvalidCredKA",
        "text": kony.i18n.getLocalizedString("i18n.transfers.invalidReccurence"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxDates = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "30%",
        "id": "flxDates",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxDates.setDefaultUnit(kony.flex.DP);
    var frmFromDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "frmFromDate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0f0c75590716b41",
        "top": "0%",
        "width": "45%",
        "zIndex": 1
    }, {}, {});
    frmFromDate.setDefaultUnit(kony.flex.DP);
    var CalIconFrom = new kony.ui.Label({
        "height": "45%",
        "id": "CalIconFrom",
        "isVisible": true,
        "left": "85%",
        "skin": "lblCalender0b724ef8ef86e45",
        "text": "T",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var calFrom = new kony.ui.Calendar({
        "calendarIcon": "tran.png",
        "dateFormat": "dd/MM/yyyy",
        "height": "45%",
        "id": "calFrom",
        "isVisible": true,
        "left": "20dp",
        "onSelection": AS_Calendar_d780c47911ec49808afed14fede81fdf,
        "skin": "calender0f920c6b788b04c",
        "top": "50%",
        "validStartDate": [7, 2, 2018],
        "viewConfig": {
            "gridConfig": {
                "gridCellSkin": "Copycalender0h05b9ba7ebfb45",
                "gridSkin": "Copycalender0c4c48cb6d2174e",
                "leftNavigationImage": "directions_turn_left.png",
                "rightNavigationImage": "directions_turn_right.png"
            }
        },
        "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
        "width": "96.71%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLine6 = new kony.ui.Label({
        "bottom": "3%",
        "height": "2%",
        "id": "lblLine6",
        "isVisible": true,
        "left": "20dp",
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    frmFromDate.add(CalIconFrom, calFrom, lblLine6);
    var frmToDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "frmToDate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "right": "15dp",
        "skin": "slFbox0f0c75590716b41",
        "top": "0%",
        "width": "45%",
        "zIndex": 1
    }, {}, {});
    frmToDate.setDefaultUnit(kony.flex.DP);
    var calIconTo = new kony.ui.Label({
        "height": "45%",
        "id": "calIconTo",
        "isVisible": true,
        "left": "85%",
        "skin": "lblCalender0b724ef8ef86e45",
        "text": "T",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var calTo = new kony.ui.Calendar({
        "calendarIcon": "tran.png",
        "dateFormat": "dd/MM/yyyy",
        "height": "45%",
        "id": "calTo",
        "isVisible": true,
        "left": "19dp",
        "onSelection": AS_Calendar_ba360bf6fc8045dda03c706a890d5753,
        "skin": "calender0f920c6b788b04c",
        "top": "50%",
        "viewConfig": {
            "gridConfig": {
                "allowWeekendSelectable": true,
                "leftNavigationImage": "directions_turn_left.png",
                "rightNavigationImage": "directions_turn_right.png"
            }
        },
        "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
        "width": "157dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLine7 = new kony.ui.Label({
        "bottom": "3%",
        "height": "2%",
        "id": "lblLine7",
        "isVisible": true,
        "left": "20dp",
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    frmToDate.add(calIconTo, calTo, lblLine7);
    var lblDateRangeBoj = new kony.ui.Label({
        "height": "35%",
        "id": "lblDateRangeBoj",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.transfers.DateRange"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDates.add(frmFromDate, frmToDate, lblDateRangeBoj);
    flxRecurrence.add(lblRecurrence, CopylblLine0a5155529c3d848, CopyflxFrequency0d4dacf1e8f8646, CopylblLine0iff8accf05a247, lblInvalidRecurrence, flxDates);
    var InvalidDatefieldsTransfer = new kony.ui.Label({
        "id": "InvalidDatefieldsTransfer",
        "isVisible": false,
        "right": "5%",
        "skin": "sknInvalidCredKA",
        "text": kony.i18n.getLocalizedString("i18.Transfer.enterDates"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPurpose = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "26%",
        "id": "flxPurpose",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "1dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxPurpose.setDefaultUnit(kony.flex.DP);
    var lblPurpose = new kony.ui.Label({
        "id": "lblPurpose",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.transfer.transfersubpurpose"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "58%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lstPurposeOfTransfer = new kony.ui.ListBox({
        "height": "40dp",
        "id": "lstPurposeOfTransfer",
        "isVisible": false,
        "left": "17dp",
        "masterData": [
            ["lb1", "General Expenses"],
            ["lb2", "General Expenses 2"],
            ["lb3", "General Expenses 3"],
            ["lb4", "General Expenses 4"]
        ],
        "selectedKey": "lb1",
        "selectedKeyValue": ["lb1", "General Expenses"],
        "skin": "CopyslListBox0gbd98df00d024a",
        "top": "27%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "dropDownImage": "dropdown.png",
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var btnDropDownPOT = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "20%",
        "id": "btnDropDownPOT",
        "isVisible": true,
        "left": "5%",
        "onClick": AS_Button_f2638e5badc2403191f11170341b0151,
        "skin": "sknBtnForwardDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "top": "70%",
        "width": "90%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblPOT = new kony.ui.Label({
        "height": "20%",
        "id": "lblPOT",
        "isVisible": true,
        "left": "5%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.settings.select"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "70%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLine10 = new kony.ui.Label({
        "centerX": "50%",
        "height": "1%",
        "id": "lblLine10",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "91%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransferCategoryTitle = new kony.ui.Label({
        "id": "lblTransferCategoryTitle",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.transfer.transfermainpurpose"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransferCategory = new kony.ui.Label({
        "height": "20%",
        "id": "lblTransferCategory",
        "isVisible": true,
        "left": "5%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.settings.select"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransferCategoryLine = new kony.ui.Label({
        "centerX": "50%",
        "height": "1%",
        "id": "lblTransferCategoryLine",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "41%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnTransferCategory = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "20%",
        "id": "btnTransferCategory",
        "isVisible": true,
        "left": "5%",
        "onClick": AS_Button_e502a7ef8828435bbd0e4ff2e45d03b8,
        "skin": "sknBtnForwardDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "top": "20%",
        "width": "90%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPurpose.add(lblPurpose, lstPurposeOfTransfer, btnDropDownPOT, lblPOT, lblLine10, lblTransferCategoryTitle, lblTransferCategory, lblTransferCategoryLine, btnTransferCategory);
    var flxDescription = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "70dp",
        "id": "flxDescription",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDescription.setDefaultUnit(kony.flex.DP);
    var lblDescription = new kony.ui.Label({
        "id": "lblDescription",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblAccNum",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.Desc"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtDesc = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "txtDesc",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": 35,
        "onDone": AS_TextField_j2dddbff421d4c5dbf262b5b5432d252,
        "onTextChange": AS_TextField_d2e6eecfc7e548b3ba6d46eab132dd0c,
        "onTouchEnd": AS_TextField_c75637363683497285d7fa387d2640b7,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 10
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "onEndEditing": AS_TextField_d3dd672ec38c42e5a966fee9709bd264,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxDescription.add(lblDescription, txtDesc);
    var lblLine5 = new kony.ui.Label({
        "height": "0.25%",
        "id": "lblLine5",
        "isVisible": true,
        "left": "20dp",
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-1.50%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnEnglish = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "btnWhiteFoc0e324fb0d3eea42",
        "height": "0%",
        "id": "btnEnglish",
        "isVisible": false,
        "left": "0%",
        "skin": "btnWhite0g39bf812fd1149",
        "text": "Proceed",
        "top": "0%",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var FlexContainer0ge6813712be644 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "3%",
        "id": "FlexContainer0ge6813712be644",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0ge6813712be644.setDefaultUnit(kony.flex.DP);
    FlexContainer0ge6813712be644.add();
    var lblHiddenAmount = new kony.ui.Label({
        "id": "lblHiddenAmount",
        "isVisible": false,
        "left": "210dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDecimal = new kony.ui.Label({
        "id": "lblDecimal",
        "isVisible": false,
        "left": "210dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurrencyCode = new kony.ui.Label({
        "id": "lblCurrencyCode",
        "isVisible": false,
        "left": "210dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMain.add(flxAccount1, lblInvalidFromAccount, flxAccount2, lblInvalidToAccount, flxAmount, flxInvalidAmountField, flxChooseCurrency, flxFrequency, flxRecurrence, InvalidDatefieldsTransfer, flxPurpose, flxDescription, lblLine5, btnEnglish, FlexContainer0ge6813712be644, lblHiddenAmount, lblDecimal, lblCurrencyCode);
    var flxPOTPopup = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxPOTPopup",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_gc3b39bbbc114938a8c8e0511d9ece80,
        "skin": "sknFlxTrans",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPOTPopup.setDefaultUnit(kony.flex.DP);
    var flx = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flx",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0d602d9a3db4442",
        "top": "0%",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flx.setDefaultUnit(kony.flex.DP);
    var lblHead = new kony.ui.Label({
        "centerX": "50.00%",
        "id": "lblHead",
        "isVisible": true,
        "left": "30%",
        "skin": "lblTitleBar0f99a08b662d44c",
        "text": kony.i18n.getLocalizedString("i18.Transfer.sendmoneyto"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19.82%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblHead0ca0dea9f86664a = new kony.ui.Label({
        "height": "80%",
        "id": "CopylblHead0ca0dea9f86664a",
        "isVisible": true,
        "left": "-0.03%",
        "onTouchStart": AS_Label_i73926b10f1c4a2d85420208ccba54e0,
        "skin": "CopyslLabel0ca16c4d8f20545",
        "text": "j",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6.67%",
        "width": "18%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flx.add(lblHead, CopylblHead0ca0dea9f86664a);
    var segFre = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "data": [{
            "lblKey": "Label",
            "lblfrequency": "Label"
        }, {
            "lblKey": "Label",
            "lblfrequency": "Label"
        }, {
            "lblKey": "Label",
            "lblfrequency": "Label"
        }, {
            "lblKey": "Label",
            "lblfrequency": "Label"
        }, {
            "lblKey": "Label",
            "lblfrequency": "Label"
        }, {
            "lblKey": "Label",
            "lblfrequency": "Label"
        }, {
            "lblKey": "Label",
            "lblfrequency": "Label"
        }],
        "groupCells": false,
        "height": "80%",
        "id": "segFre",
        "isVisible": true,
        "left": "0%",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_hafe5d839c6c438fb6abe5dc96a7d0cd,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg0a4ef05dd3f1c4f",
        "rowSkin": "sknSegDblue",
        "rowTemplate": CopyflxFrequencyTmp0ba944e73638745,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader0d39ff84c48da4e",
        "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
        "separatorColor": "ffffff48",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "10%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopyflxFrequencyTmp0ba944e73638745": "CopyflxFrequencyTmp0ba944e73638745",
            "lblKey": "lblKey",
            "lblfrequency": "lblfrequency"
        },
        "width": "80%",
        "zIndex": 10,
        "enableReordering": false
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPOTPopup.add(flx, segFre);
    frmNewTransferKA.add(mainContainer, FlexContainer0bf4a82f408fe42, flxMain, flxPOTPopup);
};

function frmNewTransferKAGlobals() {
    frmNewTransferKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmNewTransferKA,
        "allowHorizontalBounce": true,
        "allowVerticalBounce": true,
        "bounces": true,
        "enableScrolling": true,
        "enabledForIdleTimeout": true,
        "id": "frmNewTransferKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": function(eventobject) {
            AS_Form_2c6c2ada167640f3aad28042887d1f94(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_b83b869f88684d768caf00545b527b5a,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};