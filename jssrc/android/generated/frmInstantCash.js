function addWidgetsfrmInstantCash() {
    frmInstantCash.setDefaultUnit(kony.flex.DP);
    var flxInstantCash = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxInstantCash",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknDetails",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxInstantCash.setDefaultUnit(kony.flex.DP);
    var flxInstantCashHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxInstantCashHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxInstantCashHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "93%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_i59cd9225d8e450d8ba19a032d9b59c7,
        "skin": "slFbox",
        "top": "0%",
        "width": "17%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "right": "0%",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "30%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblInstantCashTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "48%",
        "id": "lblInstantCashTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.cards.redeemnow"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnNext = new kony.ui.Button({
        "focusSkin": "jomopaynextDisabled",
        "height": "93%",
        "id": "btnNext",
        "isVisible": true,
        "onTouchEnd": AS_Button_a435f6204edc483b855d839d16f932fc,
        "right": "0%",
        "skin": "jomopaynextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {});
    flxInstantCashHeader.add(flxBack, lblInstantCashTitle, btnNext);
    var flxInstantCashBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxInstantCashBody",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxInstantCashBody.setDefaultUnit(kony.flex.DP);
    var flxInstantCashTotalRedeemPoints = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxInstantCashTotalRedeemPoints",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "3%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxInstantCashTotalRedeemPoints.setDefaultUnit(kony.flex.DP);
    var lblTotalRedeemPoints = new kony.ui.Label({
        "id": "lblTotalRedeemPoints",
        "isVisible": true,
        "left": "40%",
        "skin": "sknCarioBold60OP180",
        "text": "1,444,55 pts",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTotalRedeemPointsNote = new kony.ui.Label({
        "id": "lblTotalRedeemPointsNote",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhiteBG",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.pointbalance"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxBorderAmount0f30eb79d279b4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "CopyflxBorderAmount0f30eb79d279b4e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "40%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "53%",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    CopyflxBorderAmount0f30eb79d279b4e.setDefaultUnit(kony.flex.DP);
    CopyflxBorderAmount0f30eb79d279b4e.add();
    var lblCalculatedAmount = new kony.ui.Label({
        "id": "lblCalculatedAmount",
        "isVisible": true,
        "left": "40%",
        "skin": "sknCarioBold60OP180",
        "text": "1,113.75 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "53%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxInstantCashTotalRedeemPoints.add(lblTotalRedeemPoints, lblTotalRedeemPointsNote, CopyflxBorderAmount0f30eb79d279b4e, lblCalculatedAmount);
    var lblAllowedRewardTitle = new kony.ui.Label({
        "id": "lblAllowedRewardTitle",
        "isVisible": false,
        "left": "8%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.allowedrewardpoints"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "21%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAllowedRewardPoints = new kony.ui.Label({
        "id": "lblAllowedRewardPoints",
        "isVisible": false,
        "left": "8%",
        "skin": "sknTransferType",
        "text": "4,455.00 pts",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "24%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCalculatedAmountTitle = new kony.ui.Label({
        "id": "lblCalculatedAmountTitle",
        "isVisible": false,
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.calculatedamount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxRedeemedAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxRedeemedAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "8%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "23%",
        "width": "84%",
        "zIndex": 1
    }, {}, {});
    flxRedeemedAmount.setDefaultUnit(kony.flex.DP);
    var lblRedeemAmount = new kony.ui.Label({
        "id": "lblRedeemAmount",
        "isVisible": true,
        "left": "0%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.amounttoredeem"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtRedeemAmount = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "40%",
        "id": "txtRedeemAmount",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "left": "0%",
        "maxTextLength": 9,
        "onTextChange": AS_TextField_e57f99ac5bf34cc7b214eed78181c195,
        "placeholder": "0.000",
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "text": "455.00",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "32%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderAmountRedeemed = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderAmountRedeemed",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "74%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderAmountRedeemed.setDefaultUnit(kony.flex.DP);
    flxBorderAmountRedeemed.add();
    var lblCurrency = new kony.ui.Label({
        "id": "lblCurrency",
        "isVisible": true,
        "right": "5%",
        "skin": "lblCurrency0e6155741e27a4c",
        "text": kony.i18n.getLocalizedString("i18n.common.currencySymbol"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRedeemedAmount.add(lblRedeemAmount, txtRedeemAmount, flxBorderAmountRedeemed, lblCurrency);
    var flxRedeemingPoints = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxRedeemingPoints",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "8%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "38%",
        "width": "84%",
        "zIndex": 1
    }, {}, {});
    flxRedeemingPoints.setDefaultUnit(kony.flex.DP);
    var lblRedeemingPoints = new kony.ui.Label({
        "id": "lblRedeemingPoints",
        "isVisible": true,
        "left": "0%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.redeempoints"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtFieldAmount = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "40%",
        "id": "txtFieldAmount",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "left": "0%",
        "maxTextLength": 9,
        "onTextChange": AS_TextField_b5867012065942a6aced43f465d70424,
        "placeholder": "0",
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "text": "455.00",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "32%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "74%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderAmount.setDefaultUnit(kony.flex.DP);
    flxBorderAmount.add();
    var lblredeemedpoints = new kony.ui.Label({
        "id": "lblredeemedpoints",
        "isVisible": true,
        "right": "5%",
        "skin": "lblCurrency0e6155741e27a4c",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.points"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRedeemingPoints.add(lblRedeemingPoints, txtFieldAmount, flxBorderAmount, lblredeemedpoints);
    var flxRedeemedPoints = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxRedeemedPoints",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "8%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "48%",
        "width": "84%",
        "zIndex": 1
    }, {}, {});
    flxRedeemedPoints.setDefaultUnit(kony.flex.DP);
    var lblRedeemedPointsTitle = new kony.ui.Label({
        "id": "lblRedeemedPointsTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.redeemedamount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRedeemedAmount = new kony.ui.Label({
        "id": "lblRedeemedAmount",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "text": "111.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRedeemedPoints.add(lblRedeemedPointsTitle, lblRedeemedAmount);
    var lblminpntsnotreachedmsg = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "55%",
        "id": "lblminpntsnotreachedmsg",
        "isVisible": false,
        "skin": "sknInvalidCredKA",
        "text": kony.i18n.getLocalizedString("i18n.cards.minimumpointstoredeem"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRedeemMinPointsHint = new kony.ui.Label({
        "id": "lblRedeemMinPointsHint",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblSmallWhiteBold",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.spendpoints"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "60%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRedeemPointsValue = new kony.ui.Label({
        "id": "lblRedeemPointsValue",
        "isVisible": true,
        "left": "10%",
        "skin": "sknCairoRegular100White50OP",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.spendpointshint"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "65%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblExceedPointLimit = new kony.ui.Label({
        "id": "lblExceedPointLimit",
        "isVisible": false,
        "right": "5%",
        "skin": "sknInvalidCredKA",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.maxredemptionpointhint"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblRedeemMinPointsHint0j73f08a35bf146 = new kony.ui.Label({
        "id": "CopylblRedeemMinPointsHint0j73f08a35bf146",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblSmallWhiteBold",
        "text": kony.i18n.getLocalizedString("i18n.InstantCard.RedeemPointExp"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "75%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxInstantCashBody.add(flxInstantCashTotalRedeemPoints, lblAllowedRewardTitle, lblAllowedRewardPoints, lblCalculatedAmountTitle, flxRedeemedAmount, flxRedeemingPoints, flxRedeemedPoints, lblminpntsnotreachedmsg, lblRedeemMinPointsHint, lblRedeemPointsValue, lblExceedPointLimit, CopylblRedeemMinPointsHint0j73f08a35bf146);
    var flxInstantCashPreConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxInstantCashPreConfirm",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxInstantCashPreConfirm.setDefaultUnit(kony.flex.DP);
    var lblNoOfRewardPointsAvailableTitle = new kony.ui.Label({
        "id": "lblNoOfRewardPointsAvailableTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.availablerewardpoints"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNoOfRewardPointsAvailable = new kony.ui.Label({
        "id": "lblNoOfRewardPointsAvailable",
        "isVisible": true,
        "left": "8%",
        "skin": "sknTransferType",
        "text": "1,444,55.00",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "9%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPointsToBeRedeemedTitle = new kony.ui.Label({
        "id": "lblPointsToBeRedeemedTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.redeempoints"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPointsToBeRedeemed = new kony.ui.Label({
        "id": "lblPointsToBeRedeemed",
        "isVisible": true,
        "left": "8%",
        "skin": "sknTransferType",
        "text": "4,455.00 pts",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRedeemedAmountTitle = new kony.ui.Label({
        "id": "lblRedeemedAmountTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.instantcash.redeemedamount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRedeemedAmountValue = new kony.ui.Label({
        "id": "lblRedeemedAmountValue",
        "isVisible": true,
        "left": "8%",
        "skin": "sknTransferType",
        "text": "111.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "29%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnConfirmInstantCash = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "90%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "10%",
        "id": "btnConfirmInstantCash",
        "isVisible": true,
        "onClick": AS_Button_i0a78597c4b64619b770e0d5c9b43bbb,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxInstantCashPreConfirm.add(lblNoOfRewardPointsAvailableTitle, lblNoOfRewardPointsAvailable, lblPointsToBeRedeemedTitle, lblPointsToBeRedeemed, lblRedeemedAmountTitle, lblRedeemedAmountValue, btnConfirmInstantCash);
    flxInstantCash.add(flxInstantCashHeader, flxInstantCashBody, flxInstantCashPreConfirm);
    frmInstantCash.add(flxInstantCash);
};

function frmInstantCashGlobals() {
    frmInstantCash = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmInstantCash,
        "allowHorizontalBounce": false,
        "bounces": false,
        "enabledForIdleTimeout": true,
        "id": "frmInstantCash",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "slFormCommon",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};