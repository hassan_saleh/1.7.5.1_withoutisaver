function addWidgetsfrmNewSubAccountLandingNew() {
    frmNewSubAccountLandingNew.setDefaultUnit(kony.flex.DP);
    var FlexContainer0gb7b2c2f308747 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlexContainer0gb7b2c2f308747",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    FlexContainer0gb7b2c2f308747.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b7d74518f07a4d",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var lblFormHeading = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "45%",
        "height": "100%",
        "id": "lblFormHeading",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.NewSubAcc.Header"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxNext = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxNext",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_f655e09d982e4a01b13379d8d7b01004,
        "right": "2%",
        "skin": "slFbox",
        "top": "5%",
        "width": "18%",
        "zIndex": 2
    }, {
        "retainFlexPositionProperties": true,
        "retainFlowHorizontalAlignment": false
    }, {});
    flxNext.setDefaultUnit(kony.flex.DP);
    var lblNext = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Next Page"
        },
        "height": "90%",
        "id": "lblNext",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNext.add(lblNext);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_f190262699e649c79c85a75f18facad6,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true,
        "retainFlowHorizontalAlignment": false
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btn = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "0%",
        "id": "btn",
        "isVisible": true,
        "left": "0%",
        "skin": "btnBack0b71f859656c647",
        "top": "0%",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBack.add(lblBackIcon, lblBack, btn);
    flxHeader.add(lblFormHeading, flxNext, flxBack);
    var flxAccountType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxAccountType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_f80dd50c71af40429875749945cc8bc1,
        "skin": "slFbox0f0c75590716b41",
        "top": "0%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxAccountType.setDefaultUnit(kony.flex.DP);
    var lblFrequency = new kony.ui.Label({
        "height": "35%",
        "id": "lblFrequency",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.common.accountType"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFre = new kony.ui.Label({
        "bottom": "0%",
        "centerX": "50.00%",
        "height": "48%",
        "id": "lblFre",
        "isVisible": true,
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.common.accountType"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "28%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLine4 = new kony.ui.Label({
        "centerX": "50%",
        "height": "2%",
        "id": "lblLine4",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "77%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxEdit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxEdit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "53dp",
        "zIndex": 1
    }, {}, {});
    flxEdit.setDefaultUnit(kony.flex.DP);
    var lblEdit = new kony.ui.Label({
        "height": "100%",
        "id": "lblEdit",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblEditDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEdit.add(lblEdit);
    flxAccountType.add(lblFrequency, lblFre, lblLine4, flxEdit);
    var flxAccDesc = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxAccDesc",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccDesc.setDefaultUnit(kony.flex.DP);
    var lblAccDesc = new kony.ui.Label({
        "id": "lblAccDesc",
        "isVisible": true,
        "left": "5%",
        "skin": "latoRegular24px",
        "text": "label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAccDesc.add(lblAccDesc);
    var flxISaverSub = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxISaverSub",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxISaverSub.setDefaultUnit(kony.flex.DP);
    var btnIsaver = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "50dp",
        "id": "btnIsaver",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_j499976582d246eba756b3bee895f952,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.overview.interestRate"),
        "top": "0dp",
        "width": "60%",
        "zIndex": 100
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxISaverSub.add(btnIsaver);
    var flxCurrency = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxCurrency",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_jea830080b6b4c8193723e9b4bd13e08,
        "skin": "slFbox0f0c75590716b41",
        "top": "2%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxCurrency.setDefaultUnit(kony.flex.DP);
    var lblCurrHead = new kony.ui.Label({
        "height": "35%",
        "id": "lblCurrHead",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.common.currencyType"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurr = new kony.ui.Label({
        "bottom": "0%",
        "centerX": "50%",
        "height": "48%",
        "id": "lblCurr",
        "isVisible": true,
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.common.currencyType"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "28%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblLine0ib1721fa6b7140 = new kony.ui.Label({
        "centerX": "50%",
        "height": "2%",
        "id": "CopylblLine0ib1721fa6b7140",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "77%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxEdit0e3e34b1bf78049 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "CopyflxEdit0e3e34b1bf78049",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "53dp",
        "zIndex": 1
    }, {}, {});
    CopyflxEdit0e3e34b1bf78049.setDefaultUnit(kony.flex.DP);
    var CopylblEdit0b4a3f2847e3d44 = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblEdit0b4a3f2847e3d44",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblEditDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxEdit0e3e34b1bf78049.add(CopylblEdit0b4a3f2847e3d44);
    flxCurrency.add(lblCurrHead, lblCurr, CopylblLine0ib1721fa6b7140, CopyflxEdit0e3e34b1bf78049);
    FlexContainer0gb7b2c2f308747.add(flxHeader, flxAccountType, flxAccDesc, flxISaverSub, flxCurrency);
    frmNewSubAccountLandingNew.add(FlexContainer0gb7b2c2f308747);
};

function frmNewSubAccountLandingNewGlobals() {
    frmNewSubAccountLandingNew = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmNewSubAccountLandingNew,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmNewSubAccountLandingNew",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": function(eventobject) {
            AS_Form_i8afc33577e54131847cc9fa0626d673(eventobject);
        },
        "skin": "slFormCommon"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_fe2fdd9c0fc1424d9b41f0a4fa3f1942,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};