function addWidgetsfrmBulkPaymentKA() {
    frmBulkPaymentKA.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxTop = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxTop",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTop.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_hdad317b4622497e8290143cb234c32d,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "70%",
        "id": "lblTitle",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.bulkpayment.title"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNext = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNext",
        "isVisible": true,
        "left": "86%",
        "onTouchEnd": AS_Label_bbfdbe89bff844e481082a2c0de71dd2,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false
    });
    flxTop.add(flxBack, lblTitle, lblNext);
    flxHeader.add(flxTop);
    var flxBulkPaymentBody = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "91%",
        "horizontalScrollIndicator": true,
        "id": "flxBulkPaymentBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxBulkPaymentBody.setDefaultUnit(kony.flex.DP);
    var bulkpaymentsegmant = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "accountnumber": "",
            "btnBillsPayAccounts": "q",
            "dueAmount": "",
            "lblBillerType": "",
            "lblBulkSelection": "r",
            "lblInitial": "",
            "lblpaidAmount": "asdfghj98765432",
            "paidAmount": "Label",
            "payeename": "",
            "payeenickname": "",
            "txtpaidAmount": "TextBox2"
        }],
        "groupCells": false,
        "id": "bulkpaymentsegmant",
        "isVisible": true,
        "left": "0%",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "slSegSendMoney",
        "rowTemplate": flxBulkPaymentSeg,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "accountnumber": "accountnumber",
            "btnBillsPayAccounts": "btnBillsPayAccounts",
            "contactListDivider": "contactListDivider",
            "dueAmount": "dueAmount",
            "flxAnimate": "flxAnimate",
            "flxBulkPaymentSeg": "flxBulkPaymentSeg",
            "flxDetails": "flxDetails",
            "flxIcon1": "flxIcon1",
            "flxIconContainer": "flxIconContainer",
            "flxPaidAmount": "flxPaidAmount",
            "flxToAnimate": "flxToAnimate",
            "hiddenDueAmount": "hiddenDueAmount",
            "lblBillerType": "lblBillerType",
            "lblBulkSelection": "lblBulkSelection",
            "lblInitial": "lblInitial",
            "lblTick": "lblTick",
            "lblpaidAmount": "lblpaidAmount",
            "paidAmount": "paidAmount",
            "payeename": "payeename",
            "payeenickname": "payeenickname",
            "txtpaidAmount": "txtpaidAmount"
        },
        "width": "100%",
        "zIndex": 2
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBillsAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxBillsAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBillsAmount.setDefaultUnit(kony.flex.DP);
    var lblSelectedBills = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSelectedBills",
        "isVisible": true,
        "left": "10%",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.bulkpayment.selectedbillamount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTotAmount = new kony.ui.Label({
        "id": "lblTotAmount",
        "isVisible": false,
        "left": "10%",
        "skin": "lblAmountCurrency",
        "text": "Total Amount",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSelBillsAmt = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSelBillsAmt",
        "isVisible": true,
        "left": "55%",
        "skin": "lblAmountCurrency",
        "text": "50.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTotalAmount = new kony.ui.Label({
        "id": "lblTotalAmount",
        "isVisible": false,
        "left": "45%",
        "skin": "lblAmountCurrency",
        "text": "70.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblHiddenSelBillsAmt = new kony.ui.Label({
        "id": "lblHiddenSelBillsAmt",
        "isVisible": false,
        "left": "45%",
        "skin": "lblAmountCurrency",
        "text": "50.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblHiddenTotalAmount = new kony.ui.Label({
        "id": "lblHiddenTotalAmount",
        "isVisible": false,
        "left": "45%",
        "skin": "lblAmountCurrency",
        "text": "70.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBillsAmount.add(lblSelectedBills, lblTotAmount, lblSelBillsAmt, lblTotalAmount, lblHiddenSelBillsAmt, lblHiddenTotalAmount);
    var flxAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxAccount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "-2%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxAccount.setDefaultUnit(kony.flex.DP);
    var flxPaymentModeBulk = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "65%",
        "id": "flxPaymentModeBulk",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "isModalContainer": false,
        "right": 0,
        "skin": "slFbox",
        "top": "0.00%",
        "width": "90%",
        "zIndex": 2
    }, {}, {});
    flxPaymentModeBulk.setDefaultUnit(kony.flex.DP);
    var tbxPaymentModeBulk = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxPaymentModeBulk",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": null,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlinePaymentModeBulk = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "3%",
        "id": "flxUnderlinePaymentModeBulk",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlinePaymentModeBulk.setDefaultUnit(kony.flex.DP);
    flxUnderlinePaymentModeBulk.add();
    var flxPaymentModeTypeHolderBulk = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxPaymentModeTypeHolderBulk",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_ea323e6592234ad0a2513ff300b2552f,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxPaymentModeTypeHolderBulk.setDefaultUnit(kony.flex.DP);
    var lblPaymentMode = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblPaymentMode",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "88%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowPaymentMode = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowPaymentMode",
        "isVisible": true,
        "left": "93%",
        "skin": "sknBackIconDisabled",
        "text": kony.i18n.getLocalizedString("i18n.common.baclinfo"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBranchCode = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBranchCode",
        "isVisible": false,
        "left": "93%",
        "skin": "sknBackIconDisabled",
        "text": "1",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPaymentModeTypeHolderBulk.add(lblPaymentMode, lblArrowPaymentMode, lblBranchCode);
    flxPaymentModeBulk.add(tbxPaymentModeBulk, flxUnderlinePaymentModeBulk, flxPaymentModeTypeHolderBulk);
    var flxRadioAccCardsSelectionBulk = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "100%",
        "id": "flxRadioAccCardsSelectionBulk",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRadioAccCardsSelectionBulk.setDefaultUnit(kony.flex.DP);
    var RadioBtnAccCardsBulk = new kony.ui.RadioButtonGroup({
        "centerY": "65%",
        "height": "60%",
        "id": "RadioBtnAccCardsBulk",
        "isVisible": false,
        "left": "2%",
        "masterData": [
            ["1", kony.i18n.getLocalizedString("i18n.billsPay.Accounts")],
            ["2", kony.i18n.getLocalizedString("i18n.billsPay.Cards")]
        ],
        "onSelection": AS_RadioButtonGroup_df4a63610fe144fdad3b6ce44a906f4a,
        "selectedKey": "1",
        "selectedKeyValue": ["1", "الحساب"],
        "skin": "CopyslRadioButtonGroup0b4da25f7d5c746",
        "top": "17dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBillsPayAccountsBulk = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknRDOWhiteBRDBOJFont",
        "id": "btnBillsPayAccountsBulk",
        "isVisible": true,
        "left": "5%",
        "onClick": AS_Button_h747df0691b5458fbfb9036cd74b888e,
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "t",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBillsPayAccountsBulk = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBillsPayAccountsBulk",
        "isVisible": true,
        "left": "15%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnBillsPayCardsBulk = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknRDOWhiteBRDBOJFont",
        "id": "btnBillsPayCardsBulk",
        "isVisible": false,
        "left": "55%",
        "onClick": AS_Button_ddcca42030314a629c177e5411b6e4d4,
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "s",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBillsPayCardsBulk = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBillsPayCardsBulk",
        "isVisible": false,
        "left": "65%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRadioAccCardsSelectionBulk.add(RadioBtnAccCardsBulk, btnBillsPayAccountsBulk, lblBillsPayAccountsBulk, btnBillsPayCardsBulk, lblBillsPayCardsBulk);
    var flxConversionAmt = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "id": "flxConversionAmt",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "52%",
        "zIndex": 1
    }, {}, {});
    flxConversionAmt.setDefaultUnit(kony.flex.DP);
    var lblVal = new kony.ui.Label({
        "id": "lblVal",
        "isVisible": true,
        "left": "20dp",
        "skin": "sknLblCurr",
        "text": "0.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFromCurr = new kony.ui.Label({
        "id": "lblFromCurr",
        "isVisible": true,
        "left": "55%",
        "skin": "sknLblCurr",
        "text": "1 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblToCurr = new kony.ui.Label({
        "id": "lblToCurr",
        "isVisible": true,
        "left": "73%",
        "skin": "sknLblCurr",
        "text": "0.746464 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEquals = new kony.ui.Label({
        "id": "lblEquals",
        "isVisible": true,
        "left": "69%",
        "skin": "sknLblCurr",
        "text": "=",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConversionAmt.add(lblVal, lblFromCurr, lblToCurr, lblEquals);
    flxAccount.add(flxPaymentModeBulk, flxRadioAccCardsSelectionBulk, flxConversionAmt);
    flxBulkPaymentBody.add(bulkpaymentsegmant, flxBillsAmount, flxAccount);
    flxMainContainer.add(flxHeader, flxBulkPaymentBody);
    var flxConfirmPayment = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxConfirmPayment",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmPayment.setDefaultUnit(kony.flex.DP);
    var flxHeaderPaynow = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeaderPaynow",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeaderPaynow.setDefaultUnit(kony.flex.DP);
    var flxTopPayNow = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxTopPayNow",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopPayNow.setDefaultUnit(kony.flex.DP);
    var flxBackPayNow = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBackPayNow",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_c2e8fe1aa70e4bb4b85207de137fe2c9,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBackPayNow.setDefaultUnit(kony.flex.DP);
    var CopylblBackIcon0j69715f7147640 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblBackIcon0j69715f7147640",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblBack0f85cf5afc95843 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblBack0f85cf5afc95843",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBackPayNow.add(CopylblBackIcon0j69715f7147640, CopylblBack0f85cf5afc95843);
    var lblTitlePayNow = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "70%",
        "id": "lblTitlePayNow",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.PayNow"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNextPayNow = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNextPayNow",
        "isVisible": true,
        "left": "86%",
        "onTouchEnd": AS_Label_fa017bcba5074991b0526f59eaf371c1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false
    });
    flxTopPayNow.add(flxBackPayNow, lblTitlePayNow, lblNextPayNow);
    flxHeaderPaynow.add(flxTopPayNow);
    var flxAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "12%",
        "id": "flxAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "0.00%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxAmount.setDefaultUnit(kony.flex.DP);
    var lblAmount = new kony.ui.Label({
        "height": "40%",
        "id": "lblAmount",
        "isVisible": true,
        "left": "0%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "1%",
        "isModalContainer": false,
        "skin": "skntextFieldDividerJomoPay",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxBorderAmount.setDefaultUnit(kony.flex.DP);
    flxBorderAmount.add();
    var tbxAmount = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "tbxAmount",
        "isVisible": true,
        "skin": "sknLblBack",
        "text": "100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurrency = new kony.ui.Label({
        "bottom": "10%",
        "id": "lblCurrency",
        "isVisible": true,
        "left": "88%",
        "skin": "lblAccountStaticText",
        "text": "JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAmount.add(lblAmount, flxBorderAmount, tbxAmount, lblCurrency);
    var flxRadioAccCardsSelection = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "12%",
        "id": "flxRadioAccCardsSelection",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRadioAccCardsSelection.setDefaultUnit(kony.flex.DP);
    var RadioBtnAccCards = new kony.ui.RadioButtonGroup({
        "centerY": "65%",
        "height": "60%",
        "id": "RadioBtnAccCards",
        "isVisible": false,
        "left": "2%",
        "masterData": [
            ["1", kony.i18n.getLocalizedString("i18n.billsPay.Accounts")],
            ["2", kony.i18n.getLocalizedString("i18n.billsPay.Cards")]
        ],
        "onSelection": AS_RadioButtonGroup_jee35b89370e4aafacb81fba4d1df440,
        "selectedKey": "1",
        "selectedKeyValue": ["1", "الحساب"],
        "skin": "CopyslRadioButtonGroup0b4da25f7d5c746",
        "top": "17dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBillsPayAccounts = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknRDOWhiteBRDBOJFont",
        "id": "btnBillsPayAccounts",
        "isVisible": true,
        "left": "5%",
        "onClick": AS_Button_f5c858b3a7ac45ec8f9af5ca71959b54,
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "t",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBillsPayAccounts = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBillsPayAccounts",
        "isVisible": true,
        "left": "15%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnBillsPayCards = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknRDOWhiteBRDBOJFont",
        "id": "btnBillsPayCards",
        "isVisible": false,
        "left": "55%",
        "onClick": AS_Button_adcaf2ce408a427091f721a2e8c00eab,
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "s",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBillsPayCards = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBillsPayCards",
        "isVisible": false,
        "left": "65%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRadioAccCardsSelection.add(RadioBtnAccCards, btnBillsPayAccounts, lblBillsPayAccounts, btnBillsPayCards, lblBillsPayCards);
    var flxPaymentMode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "10%",
        "id": "flxPaymentMode",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "isModalContainer": false,
        "right": 0,
        "skin": "slFbox",
        "top": "0.00%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPaymentMode.setDefaultUnit(kony.flex.DP);
    var tbxPaymentMode = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxPaymentMode",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": null,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlinePaymentMode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlinePaymentMode",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlinePaymentMode.setDefaultUnit(kony.flex.DP);
    flxUnderlinePaymentMode.add();
    var flxPaymentModeTypeHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxPaymentModeTypeHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_g733e665ef5c4615a6838d59d51d50aa,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxPaymentModeTypeHolder.setDefaultUnit(kony.flex.DP);
    var lblPaymentMode1 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblPaymentMode1",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "88%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowPaymentMode1 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowPaymentMode1",
        "isVisible": true,
        "left": "93%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBranchCode1 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBranchCode1",
        "isVisible": false,
        "left": "93%",
        "skin": "sknBackIconDisabled",
        "text": "1",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPaymentModeTypeHolder.add(lblPaymentMode1, lblArrowPaymentMode1, lblBranchCode1);
    flxPaymentMode.add(tbxPaymentMode, flxUnderlinePaymentMode, flxPaymentModeTypeHolder);
    flxConfirmPayment.add(flxHeaderPaynow, flxAmount, flxRadioAccCardsSelection, flxPaymentMode);
    frmBulkPaymentKA.add(flxMainContainer, flxConfirmPayment);
};

function frmBulkPaymentKAGlobals() {
    frmBulkPaymentKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmBulkPaymentKA,
        "enabledForIdleTimeout": true,
        "id": "frmBulkPaymentKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "preShow": function(eventobject) {
            AS_Form_a1aa506af9ab41bc9896ce37598dbdf0(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_be1171dcbb78496280152f99c9b83081,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};