function initializeCopyFlexContainer0c881155c547a42() {
    CopyFlexContainer0c881155c547a42 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "CopyFlexContainer0c881155c547a42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "CopyslFSbox0a724e40393f744",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerWeight": 100
    }, {});
    CopyFlexContainer0c881155c547a42.setDefaultUnit(kony.flex.DP);
    var lbl1 = new kony.ui.Label({
        "height": "45%",
        "id": "lbl1",
        "isVisible": false,
        "left": "20%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Account",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "7dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var Symbol = new kony.ui.Label({
        "height": "100%",
        "id": "Symbol",
        "isVisible": false,
        "left": "0%",
        "skin": "CopylblSegName0i602998c2fe94f",
        "text": "B",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var lbl2 = new kony.ui.Label({
        "height": "50%",
        "id": "lbl2",
        "isVisible": false,
        "left": "20%",
        "skin": "lblsegtextsmall0b5a3b38d4be646",
        "text": "Shyam",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": "40%",
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var lbl3 = new kony.ui.Label({
        "height": "45%",
        "id": "lbl3",
        "isVisible": false,
        "right": "5%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Balance",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": "40%",
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var lbl4 = new kony.ui.Label({
        "height": "90%",
        "id": "lbl4",
        "isVisible": false,
        "right": "5%",
        "skin": "Copylblsegtextsmall0a16789105f8b49",
        "text": "0.00$",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": "40%",
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0c881155c547a42.add(lbl1, Symbol, lbl2, lbl3, lbl4);
}