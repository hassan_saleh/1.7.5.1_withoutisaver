function addWidgetsfrmCancelCardKA() {
    frmCancelCardKA.setDefaultUnit(kony.flex.DP);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox0b7d74518f07a4d",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var lblTitleStopCard = new kony.ui.Label({
        "centerX": "50%",
        "height": "90%",
        "id": "lblTitleStopCard",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.cards.stopCard"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnClose2 = new kony.ui.Button({
        "height": "90%",
        "id": "btnClose2",
        "isVisible": true,
        "onClick": AS_Button_fca20b95d649462091b45ee6a68a0f6a,
        "right": "0%",
        "skin": "CopyslButtonGlossBlue0e73a02c4810645",
        "text": "O",
        "top": "0%",
        "width": "15%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    androidTitleBar.add(lblTitleStopCard, btnClose2);
    var flxCardCancel = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxCardCancel",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "70dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCardCancel.setDefaultUnit(kony.flex.DP);
    var lblCardCancellationDesc = new kony.ui.Label({
        "id": "lblCardCancellationDesc",
        "isVisible": true,
        "left": "5%",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.cards.selectccanopt"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "26dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxCardReason = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxCardReason",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "15dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_a9f74fe969d642f8b92aba3878a964e6,
        "skin": "slFbox",
        "top": "150dp",
        "width": "90%",
        "zIndex": 5
    }, {}, {});
    flxCardReason.setDefaultUnit(kony.flex.DP);
    var lstCardReason = new kony.ui.ListBox({
        "id": "lstCardReason",
        "isVisible": false,
        "left": "5dp",
        "onSelection": AS_ListBox_af8ea77150e54c6088563ea19b34efc7,
        "skin": "slListBoxNew",
        "top": "5dp",
        "width": "92%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "dropDownImage": "dropdown.png",
        "placeholder": "Reason for Card Cancellation",
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var lblReason = new kony.ui.Label({
        "id": "lblReason",
        "isVisible": true,
        "left": "2.03%",
        "skin": "sknlblCairoL",
        "text": kony.i18n.getLocalizedString("i18n.cards.reasonforcancel"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20.00%",
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCardNumber = new kony.ui.Label({
        "id": "lblCardNumber",
        "isVisible": false,
        "left": "12%",
        "skin": "sknlblCairoL",
        "text": "Reason For Card Cancellation",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCardType = new kony.ui.Label({
        "id": "lblCardType",
        "isVisible": false,
        "left": "22%",
        "skin": "sknlblCairoL",
        "text": "Reason For Card Cancellation",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCustID = new kony.ui.Label({
        "id": "lblCustID",
        "isVisible": false,
        "left": "32%",
        "skin": "sknlblCairoL",
        "text": "Reason For Card Cancellation",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblReasonCode = new kony.ui.Label({
        "id": "lblReasonCode",
        "isVisible": false,
        "left": "42%",
        "skin": "sknlblCairoL",
        "text": "Reason For Card Cancellation",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "60%",
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnDropDown = new kony.ui.Button({
        "height": "100%",
        "id": "btnDropDown",
        "isVisible": true,
        "onClick": AS_Button_j374f6babe6c4b60b6d49cc95cc13f8d,
        "right": "2%",
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "o",
        "top": "2%",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxUnderlineBillerCategory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineBillerCategory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineBillerCategory.setDefaultUnit(kony.flex.DP);
    flxUnderlineBillerCategory.add();
    flxCardReason.add(lstCardReason, lblReason, lblCardNumber, lblCardType, lblCustID, lblReasonCode, btnDropDown, flxUnderlineBillerCategory);
    var flxDeliveryBranch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxDeliveryBranch",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "15dp",
        "isModalContainer": false,
        "onTouchStart": AS_FlexContainer_h7764010e6804466b14a29571c910a9b,
        "skin": "slFbox",
        "top": "260dp",
        "width": "90%",
        "zIndex": 3
    }, {}, {});
    flxDeliveryBranch.setDefaultUnit(kony.flex.DP);
    var lstDeliveryBranch = new kony.ui.ListBox({
        "id": "lstDeliveryBranch",
        "isVisible": false,
        "left": "4dp",
        "masterData": [
            ["lb1", "Select the Delivery Branch"],
            ["lb2", "Oman"],
            ["lb3", "Jordan"]
        ],
        "selectedKey": "lb1",
        "selectedKeyValue": ["lb1", "Select the Delivery Branch"],
        "skin": "slListBoxNew",
        "top": "1dp",
        "width": "92%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "dropDownImage": "dropdown.png",
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var CopybtnDropDown0cbbd92d7c84c4a = new kony.ui.Button({
        "height": "100%",
        "id": "CopybtnDropDown0cbbd92d7c84c4a",
        "isVisible": true,
        "right": "2%",
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "o",
        "top": "2%",
        "width": "90%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblDeliveryBranch = new kony.ui.Label({
        "id": "lblDeliveryBranch",
        "isVisible": true,
        "left": "2%",
        "skin": "sknlblCairoL",
        "text": "Select the Delivery Branch",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20%",
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxUnderlineBillerCategory0ef0fe8c3d30f4c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "CopyflxUnderlineBillerCategory0ef0fe8c3d30f4c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "94%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    CopyflxUnderlineBillerCategory0ef0fe8c3d30f4c.setDefaultUnit(kony.flex.DP);
    CopyflxUnderlineBillerCategory0ef0fe8c3d30f4c.add();
    flxDeliveryBranch.add(lstDeliveryBranch, CopybtnDropDown0cbbd92d7c84c4a, lblDeliveryBranch, CopyflxUnderlineBillerCategory0ef0fe8c3d30f4c);
    var flxBorderDeliveryBranch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "flxBorderDeliveryBranch",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "18dp",
        "isModalContainer": false,
        "skin": "skntLineDiv",
        "top": 277,
        "width": "78%",
        "zIndex": 1
    }, {}, {});
    flxBorderDeliveryBranch.setDefaultUnit(kony.flex.DP);
    flxBorderDeliveryBranch.add();
    var flxBorderTransferType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "flxBorderTransferType",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "18dp",
        "isModalContainer": false,
        "skin": "skntLineDiv",
        "top": 229,
        "width": "78%",
        "zIndex": 1
    }, {}, {});
    flxBorderTransferType.setDefaultUnit(kony.flex.DP);
    flxBorderTransferType.add();
    var flxCardCancellationOptions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "115dp",
        "id": "flxCardCancellationOptions",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "75dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxCardCancellationOptions.setDefaultUnit(kony.flex.DP);
    var btnCardCancel = new kony.ui.Button({
        "focusSkin": "sknRadioNotSelected",
        "height": "20%",
        "id": "btnCardCancel",
        "isVisible": true,
        "left": "5%",
        "onClick": AS_Button_a443a1a61ad04ae98744d99c61603eb5,
        "skin": "sknRadioNotSelected",
        "text": "t",
        "top": "5%",
        "width": "7%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCardCancel = new kony.ui.Label({
        "height": "25%",
        "id": "lblCardCancel",
        "isVisible": true,
        "left": "15%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.cards.description"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnCardReplace = new kony.ui.Button({
        "focusSkin": "sknRadioSelected",
        "height": "20%",
        "id": "btnCardReplace",
        "isVisible": false,
        "left": "5%",
        "onClick": AS_Button_c7ca4e3e7add4bce9b729f478b7cceb4,
        "skin": "sknRadioSelected",
        "top": "35%",
        "width": "7%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblOtherCard = new kony.ui.Label({
        "height": "25%",
        "id": "lblOtherCard",
        "isVisible": false,
        "left": "15%",
        "skin": "sknTransferType",
        "text": "Cancel and replace the Card",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCardCancellationOptions.add(btnCardCancel, lblCardCancel, btnCardReplace, lblOtherCard);
    flxCardCancel.add(lblCardCancellationDesc, flxCardReason, flxDeliveryBranch, flxBorderDeliveryBranch, flxBorderTransferType, flxCardCancellationOptions);
    var btnSubmit = new kony.ui.Button({
        "bottom": "8%",
        "centerX": "50%",
        "height": "50dp",
        "id": "btnSubmit",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_dec623cea8e743d5b9e9ec1871843416,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.NUO.Submit"),
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    frmCancelCardKA.add(androidTitleBar, flxCardCancel, btnSubmit);
};

function frmCancelCardKAGlobals() {
    frmCancelCardKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCancelCardKA,
        "enabledForIdleTimeout": true,
        "id": "frmCancelCardKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknBackground"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_g93639a27b0941309aa1b93eb9e23e06,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};