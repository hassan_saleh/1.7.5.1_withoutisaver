function addWidgetsfrmRegisterUser() {
    frmRegisterUser.setDefaultUnit(kony.flex.DP);
    var flxMain = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "CopysknFlxBackgrounf0hf3c1bfbde664f",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var imgBackground = new kony.ui.Image2({
        "height": "100%",
        "id": "imgBackground",
        "isVisible": false,
        "left": "0dp",
        "skin": "slImage",
        "src": "bg04.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxHead = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "12%",
        "id": "flxHead",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHead.setDefaultUnit(kony.flex.DP);
    var flxHeadMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80%",
        "id": "flxHeadMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxHeadMain.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_f0cbca532d314d1c92b4b22a0de45384,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblTitle = new kony.ui.Label({
        "height": "100%",
        "id": "lblTitle",
        "isVisible": true,
        "left": "20%",
        "skin": "lblAmountCurrency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxNext = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxNext",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_f9d0cdfd81f943a68b13e5cb50e3d05e,
        "right": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxNext.setDefaultUnit(kony.flex.DP);
    var lblNext = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Next Page"
        },
        "height": "100%",
        "id": "lblNext",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNext.add(lblNext);
    flxHeadMain.add(flxBack, lblTitle, flxNext);
    var flxProgress1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "11%",
        "id": "flxProgress1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10%",
        "isModalContainer": false,
        "skin": "CopysknFlxProgress0fd0051ac013643",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxProgress1.setDefaultUnit(kony.flex.DP);
    flxProgress1.add();
    var flxProgress2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "11%",
        "id": "flxProgress2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlxProgress",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProgress2.setDefaultUnit(kony.flex.DP);
    flxProgress2.add();
    flxHead.add(flxHeadMain, flxProgress1, flxProgress2);
    var flxDebitCard = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "86%",
        "horizontalScrollIndicator": true,
        "id": "flxDebitCard",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "14%",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDebitCard.setDefaultUnit(kony.flex.DP);
    var lblInstruction1 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblInstruction1",
        "isVisible": true,
        "left": "141dp",
        "skin": "sknLblWhite",
        "text": kony.i18n.getLocalizedString("i18n.reg.EnterDebit"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxEnterCardDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "52dp",
        "id": "flxEnterCardDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxEnterCardDetails.setDefaultUnit(kony.flex.DP);
    var flxText = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxText",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4dp",
        "width": "87%",
        "zIndex": 1
    }, {}, {});
    flxText.setDefaultUnit(kony.flex.DP);
    var flxCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "42dp",
        "id": "flxCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "CopyslFbox0b72106319d6145",
        "top": "0%",
        "width": "13%",
        "zIndex": 1
    }, {}, {});
    flxCard.setDefaultUnit(kony.flex.DP);
    var cardImg = new kony.ui.Image2({
        "centerY": "50%",
        "height": "100%",
        "id": "cardImg",
        "isVisible": false,
        "left": "0%",
        "skin": "slImage",
        "src": "card.png",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnCardReg = new kony.ui.Button({
        "focusSkin": "sknbtncard",
        "height": "100%",
        "id": "btnCardReg",
        "isVisible": true,
        "left": "0%",
        "skin": "sknbtncard",
        "text": "G",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCard.add(cardImg, btnCardReg);
    var txtCardNum = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Please Enter Debit Card Number"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknDebitCardNum",
        "height": "40dp",
        "id": "txtCardNum",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "14%",
        "maxTextLength": 16,
        "onDone": AS_TextField_a57dcb695d9f4919a18fb21739eac607,
        "onTextChange": AS_TextField_e286e2c0112f48a8bfdb8a664fe2f20a,
        "onTouchStart": AS_TextField_ce47004cd4674692adae12ac3408141c,
        "placeholder": kony.i18n.getLocalizedString("i18n.common.Enter16Digit"),
        "secureTextEntry": false,
        "skin": "sknDebitCardNum",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "86%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "placeholderSkin": "sknTxtplacehodler",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblClose = new kony.ui.Label({
        "height": "100%",
        "id": "lblClose",
        "isVisible": false,
        "left": "90%",
        "onTouchEnd": AS_Label_eef163aeeaec492fb08d8009c75c5d53,
        "skin": "sknClose",
        "text": "O",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxText.add(flxCard, txtCardNum, lblClose);
    var flxphoto = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "42dp",
        "id": "flxphoto",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "88%",
        "isModalContainer": false,
        "skin": "CopyslFbox0b72106319d6145",
        "top": "4dp",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxphoto.setDefaultUnit(kony.flex.DP);
    var imgScan = new kony.ui.Image2({
        "centerY": "50%",
        "height": "100%",
        "id": "imgScan",
        "isVisible": true,
        "left": "0%",
        "onTouchStart": AS_Image_h9f117c2109f4e76a16eeb3f647cd29f,
        "skin": "slImage",
        "src": "scan.png",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxphoto.add(imgScan);
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "47dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    flxEnterCardDetails.add(flxText, flxphoto, flxLine);
    var lblInstruction2 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblInstruction2",
        "isVisible": true,
        "left": "151dp",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.common.CardPin"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPIN = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxPIN",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_fac03e0d581146919bd54a60614ce2b8,
        "skin": "slFbox",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flxPIN.setDefaultUnit(kony.flex.DP);
    var lblCircle1 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Enter"
        },
        "height": "80%",
        "id": "lblCircle1",
        "isVisible": true,
        "left": "30.50%",
        "skin": "sknlblPinStar",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCircle2 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "the"
        },
        "height": "80%",
        "id": "lblCircle2",
        "isVisible": true,
        "left": "42.50%",
        "skin": "sknlblPinStar",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCircle3 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "PIN"
        },
        "height": "80%",
        "id": "lblCircle3",
        "isVisible": true,
        "left": "54.50%",
        "skin": "sknlblPinStar",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCircle4 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Number"
        },
        "height": "80%",
        "id": "lblCircle4",
        "isVisible": true,
        "left": "66.50%",
        "skin": "sknlblPinStar",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtPIN = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Please enter your PIN and click next"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknFlxPIN",
        "height": "40dp",
        "id": "txtPIN",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "300%",
        "maxTextLength": 4,
        "onTextChange": AS_TextField_a9b6aaf7339746899319bafac5bfe440,
        "secureTextEntry": false,
        "skin": "sknFlxPIN",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknFlxPIN",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxLinee1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "8%",
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLinee1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "28%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "width": "9%",
        "zIndex": 10
    }, {}, {});
    flxLinee1.setDefaultUnit(kony.flex.DP);
    flxLinee1.add();
    var flxLinee2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "8%",
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLinee2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "40%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "width": "9%",
        "zIndex": 10
    }, {}, {});
    flxLinee2.setDefaultUnit(kony.flex.DP);
    flxLinee2.add();
    var flxLinee3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "8%",
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLinee3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "52%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "width": "9%",
        "zIndex": 10
    }, {}, {});
    flxLinee3.setDefaultUnit(kony.flex.DP);
    flxLinee3.add();
    var flxLinee4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "8%",
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLinee4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "64%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "width": "9%",
        "zIndex": 10
    }, {}, {});
    flxLinee4.setDefaultUnit(kony.flex.DP);
    flxLinee4.add();
    flxPIN.add(lblCircle1, lblCircle2, lblCircle3, lblCircle4, txtPIN, flxLinee1, flxLinee2, flxLinee3, flxLinee4);
    var flxNationalIDButton = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxNationalIDButton",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "reverseLayoutDirection": false,
        "isModalContainer": false,
        "right": "0%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNationalIDButton.setDefaultUnit(kony.flex.DP);
    var lblNationalID = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblNationalID",
        "isVisible": true,
        "skin": "CopysknLblWhite0fbfcdb6497644a",
        "text": kony.i18n.getLocalizedString("i18n.register.nationalIDlbl"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRegisterClick = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblRegisterClick",
        "isVisible": true,
        "left": "0.50%",
        "onTouchStart": AS_Label_f41b041dbffc434bb337792aea0816e5,
        "skin": "lblsknregisterid",
        "text": kony.i18n.getLocalizedString("i18n.nationalidLink"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "38%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnNationalID = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknsecactionOnboarding",
        "height": "50dp",
        "id": "btnNationalID",
        "isVisible": false,
        "onClick": AS_Button_cb1c637e6e7f4e47a7ebc1b66ae3e978,
        "skin": "sknsecactionOnboarding",
        "text": kony.i18n.getLocalizedString("i18n.register.nationalIDBtn"),
        "top": "80dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxNationalIDButton.add(lblNationalID, lblRegisterClick, btnNationalID);
    var btnDummy = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "1dp",
        "id": "btnDummy",
        "isVisible": true,
        "left": "5000dp",
        "skin": "slButtonGlossBlue",
        "text": "Button",
        "top": "10dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblInvalidCredentialsKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblInvalidCredentialsKA",
        "isVisible": false,
        "right": "5%",
        "skin": "sknInvalidCredKA",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxTxtMobileNukber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "250dp",
        "id": "flxTxtMobileNukber",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "-5dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTxtMobileNukber.setDefaultUnit(kony.flex.DP);
    var lblEnterMobileNumber = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblEnterMobileNumber",
        "isVisible": true,
        "skin": "sknLblWhite",
        "text": kony.i18n.getLocalizedString("i18n.settings.enterYourMobileNumberPlh"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxMobContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxMobContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMobContainer.setDefaultUnit(kony.flex.DP);
    var lblCountryCodeTitle = new kony.ui.Label({
        "id": "lblCountryCodeTitle",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.common.countrycode"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtCountryCode = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknDebitCardNum",
        "height": "40dp",
        "id": "txtCountryCode",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "4%",
        "maxTextLength": 3,
        "onTextChange": AS_TextField_e1f51df4168c47acb1420746e8be8c28,
        "placeholder": "962",
        "secureTextEntry": false,
        "skin": "sknDebitCardNum",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "15dp",
        "width": "25%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTxtplacehodler",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxLine2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "55dp",
        "width": "28%",
        "zIndex": 1
    }, {}, {});
    flxLine2.setDefaultUnit(kony.flex.DP);
    flxLine2.add();
    var lblMobileNumberTitle = new kony.ui.Label({
        "id": "lblMobileNumberTitle",
        "isVisible": false,
        "left": "32%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.myprofile.MobileNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtMobileNumber = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknDebitCardNum",
        "height": "40dp",
        "id": "txtMobileNumber",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "32%",
        "maxTextLength": 20,
        "onTextChange": AS_TextField_e9288e87d81c4499834424923a3d70c6,
        "placeholder": kony.i18n.getLocalizedString("i18n.myprofile.MobileNumber"),
        "secureTextEntry": false,
        "skin": "sknDebitCardNum",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "15dp",
        "width": "69%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTxtplacehodler",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyflxLine0a4214bb8a7c146 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2dp",
        "id": "CopyflxLine0a4214bb8a7c146",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "32%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "55dp",
        "width": "68%",
        "zIndex": 1
    }, {}, {});
    CopyflxLine0a4214bb8a7c146.setDefaultUnit(kony.flex.DP);
    CopyflxLine0a4214bb8a7c146.add();
    var btnMobileCode = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "42dp",
        "id": "btnMobileCode",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_bbbc2c65e1f44ad1ab5535752d14ec6e,
        "skin": "CopysglossblueArrow140",
        "text": "d",
        "top": "15dp",
        "width": "27%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblMobileNumberClear = new kony.ui.Label({
        "id": "lblMobileNumberClear",
        "isVisible": false,
        "onTouchEnd": AS_Label_g5a8dde9d55f42b9a57da8c2e7e11e6d,
        "right": "3%",
        "skin": "sknClose",
        "text": "O",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMobContainer.add(lblCountryCodeTitle, txtCountryCode, flxLine2, lblMobileNumberTitle, txtMobileNumber, CopyflxLine0a4214bb8a7c146, btnMobileCode, lblMobileNumberClear);
    var lblHint = new kony.ui.Label({
        "id": "lblHint",
        "isVisible": true,
        "left": "32%",
        "skin": "CopysknCaironew70number",
        "text": kony.i18n.getLocalizedString("i18n.common.currentmobileno"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnMobileNumberConfirm = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhite",
        "height": "50dp",
        "id": "btnMobileNumberConfirm",
        "isVisible": true,
        "left": "25dp",
        "onClick": AS_Button_ae0c8fa3fc1d4b758e389919082b4370,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
        "top": "15%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTxtMobileNukber.add(lblEnterMobileNumber, flxMobContainer, lblHint, btnMobileNumberConfirm);
    flxDebitCard.add(lblInstruction1, flxEnterCardDetails, lblInstruction2, flxPIN, flxNationalIDButton, btnDummy, lblInvalidCredentialsKA, flxTxtMobileNukber);
    var flxNationalIDContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "86%",
        "id": "flxNationalIDContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "14%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNationalIDContainer.setDefaultUnit(kony.flex.DP);
    var flxAccountNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "70dp",
        "id": "flxAccountNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAccountNumber.setDefaultUnit(kony.flex.DP);
    var lblAccountNumberTitle = new kony.ui.Label({
        "id": "lblAccountNumberTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.CustomerID"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtAccountNumber = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "60%",
        "id": "txtAccountNumber",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "maxTextLength": 7,
        "onDone": AS_TextField_c17b4cdd4fb6418bbd8b299c204364f5,
        "onTextChange": AS_TextField_de95a56be40a4baabbf0056753601414,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_g3308bf66f4745a89199794fba2c1137,
        "onEndEditing": AS_TextField_hcfabc2139954cf3a0a60a3a7302946f,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderAccountNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderAccountNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderAccountNumber.setDefaultUnit(kony.flex.DP);
    flxBorderAccountNumber.add();
    flxAccountNumber.add(lblAccountNumberTitle, txtAccountNumber, flxBorderAccountNumber);
    var flxDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "top": "17%",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    flxDivider.setDefaultUnit(kony.flex.DP);
    flxDivider.add();
    var lblNationalIDInfoTitle = new kony.ui.Label({
        "id": "lblNationalIDInfoTitle",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhite100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxNationalIDInformation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "75%",
        "id": "flxNationalIDInformation",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "14%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNationalIDInformation.setDefaultUnit(kony.flex.DP);
    var lblIDinformation = new kony.ui.Label({
        "id": "lblIDinformation",
        "isVisible": true,
        "left": "7%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.idCardInfo"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxNationalID = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "70dp",
        "id": "flxNationalID",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNationalID.setDefaultUnit(kony.flex.DP);
    var lblNationalIDTitle = new kony.ui.Label({
        "id": "lblNationalIDTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.jomopay.nationalid"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtNationalID = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "60%",
        "id": "txtNationalID",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "0%",
        "maxTextLength": 10,
        "onDone": AS_TextField_b19efdcf634b442fa6bf5568337a879c,
        "onTextChange": AS_TextField_gf77d5b26c0045e48a0245fd4238c090,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_b2492f65ca644af69de91444fc412c3d,
        "onEndEditing": AS_TextField_ada3321807a4426388fef976421e1c4e,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderNationalID = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderNationalID",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderNationalID.setDefaultUnit(kony.flex.DP);
    flxBorderNationalID.add();
    flxNationalID.add(lblNationalIDTitle, txtNationalID, flxBorderNationalID);
    var flxIDCardNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "70dp",
        "id": "flxIDCardNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxIDCardNumber.setDefaultUnit(kony.flex.DP);
    var lblIDCardNumberTitle = new kony.ui.Label({
        "id": "lblIDCardNumberTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.creditcard.idcardnumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtIDCardNumber = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_ALL,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "60%",
        "id": "txtIDCardNumber",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0.00%",
        "maxTextLength": 8,
        "onDone": AS_TextField_a2b24fa7475e4780a3e5dc461cd45446,
        "onTextChange": AS_TextField_de2516fe81e24bf8b2bed2eb3f8bd174,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "35.00%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_df5a7b5a247c4012bde3d737a5ec01e0,
        "onEndEditing": AS_TextField_j56e8833ff3945c3becd884ad98c40b2,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderIDCardNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderIDCardNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderIDCardNumber.setDefaultUnit(kony.flex.DP);
    flxBorderIDCardNumber.add();
    flxIDCardNumber.add(lblIDCardNumberTitle, txtIDCardNumber, flxBorderIDCardNumber);
    var flxDateOfBirth = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "70dp",
        "id": "flxDateOfBirth",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDateOfBirth.setDefaultUnit(kony.flex.DP);
    var lblDateOfBirthTitle = new kony.ui.Label({
        "id": "lblDateOfBirthTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.login.iddateOfBirth"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDOBCalIcon = new kony.ui.Label({
        "height": "50%",
        "id": "lblDOBCalIcon",
        "isVisible": true,
        "right": "0%",
        "skin": "sknBOJFont2White146OPC55",
        "text": "B",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var calDateOfBirth = new kony.ui.Calendar({
        "calendarIcon": "tran.png",
        "dateFormat": "dd/MM/yyyy",
        "focusSkin": "sknCarioRegular",
        "height": "50%",
        "id": "calDateOfBirth",
        "isVisible": true,
        "left": "0%",
        "onSelection": AS_Calendar_e642fefaa6cc435f94d82fa4dbc74cdd,
        "onTouchStart": AS_Calendar_c833479dca1d4fc291188cc271db93f9,
        "skin": "sknCarioRegular",
        "top": "35%",
        "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblDateOfBirth = new kony.ui.Label({
        "id": "lblDateOfBirth",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhite100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderDateOfBirth = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderDateOfBirth",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderDateOfBirth.setDefaultUnit(kony.flex.DP);
    flxBorderDateOfBirth.add();
    flxDateOfBirth.add(lblDateOfBirthTitle, lblDOBCalIcon, calDateOfBirth, lblDateOfBirth, flxBorderDateOfBirth);
    var flxCSPDFileNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "70dp",
        "id": "flxCSPDFileNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxCSPDFileNumber.setDefaultUnit(kony.flex.DP);
    var lblCSPDFileNubmerTitle = new kony.ui.Label({
        "id": "lblCSPDFileNubmerTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.nationalID.FileNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtCSPDFileNumber = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "60%",
        "id": "txtCSPDFileNumber",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0.00%",
        "onDone": AS_TextField_d6ec2026af6d4206b938290d1e4cc694,
        "onTextChange": AS_TextField_cd032b43499d429fb613fa1cff7a47bc,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_a5cbeceb69b44f7093078647758dd109,
        "onEndEditing": AS_TextField_i71bbd4d895746a69ee57328ced4bedc,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderCSPDFileNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderCSPDFileNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderCSPDFileNumber.setDefaultUnit(kony.flex.DP);
    flxBorderCSPDFileNumber.add();
    flxCSPDFileNumber.add(lblCSPDFileNubmerTitle, txtCSPDFileNumber, flxBorderCSPDFileNumber);
    var flxIDExpiryDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "70dp",
        "id": "flxIDExpiryDate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxIDExpiryDate.setDefaultUnit(kony.flex.DP);
    var lblIDExpiryDatelblIDExpiryDate = new kony.ui.Label({
        "id": "lblIDExpiryDatelblIDExpiryDate",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.common.idexpirydate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIDExpiryDateCalIcon = new kony.ui.Label({
        "height": "50%",
        "id": "lblIDExpiryDateCalIcon",
        "isVisible": true,
        "right": "0%",
        "skin": "sknBOJFont2White146OPC55",
        "text": "B",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var calIDExpiryDate = new kony.ui.Calendar({
        "calendarIcon": "tran.png",
        "dateFormat": "dd/MM/yyyy",
        "focusSkin": "sknCarioRegular",
        "height": "50%",
        "id": "calIDExpiryDate",
        "isVisible": true,
        "left": "0%",
        "onSelection": AS_Calendar_j2a48395348848b0973109fc575b0980,
        "onTouchStart": AS_Calendar_a74ab14f90b54b749ec22e25fef8debc,
        "skin": "sknCarioRegular",
        "top": "35.00%",
        "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblIDExpiryDate = new kony.ui.Label({
        "id": "lblIDExpiryDate",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhite100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderIDExpiryDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderIDExpiryDate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderIDExpiryDate.setDefaultUnit(kony.flex.DP);
    flxBorderIDExpiryDate.add();
    flxIDExpiryDate.add(lblIDExpiryDatelblIDExpiryDate, lblIDExpiryDateCalIcon, calIDExpiryDate, lblIDExpiryDate, flxBorderIDExpiryDate);
    flxNationalIDInformation.add(lblIDinformation, flxNationalID, flxIDCardNumber, flxDateOfBirth, flxCSPDFileNumber, flxIDExpiryDate);
    flxNationalIDContainer.add(flxAccountNumber, flxDivider, lblNationalIDInfoTitle, flxNationalIDInformation);
    var lblCard = new kony.ui.Label({
        "centerX": "59.98%",
        "height": "0%",
        "id": "lblCard",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblTitle",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCode = new kony.ui.Label({
        "centerX": "69.97999999999999%",
        "height": "0%",
        "id": "lblCode",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblTitle",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMsg = new kony.ui.Label({
        "centerX": "79.97999999999999%",
        "height": "0%",
        "id": "lblMsg",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblTitle",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStatus = new kony.ui.Label({
        "centerX": "89.97999999999999%",
        "height": "0%",
        "id": "lblStatus",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblTitle",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPin = new kony.ui.Label({
        "centerX": "69.97999999999999%",
        "height": "0%",
        "id": "lblPin",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblTitle",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLanguage = new kony.ui.Label({
        "centerX": "79.97999999999999%",
        "height": "0%",
        "id": "lblLanguage",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblTitle",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMain.add(imgBackground, flxHead, flxDebitCard, flxNationalIDContainer, lblCard, lblCode, lblMsg, lblStatus, lblPin, lblLanguage);
    frmRegisterUser.add(flxMain);
};

function frmRegisterUserGlobals() {
    frmRegisterUser = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmRegisterUser,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmRegisterUser",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_fecba2b4ec254548803e4e064bddfbbc,
        "preShow": function(eventobject) {
            AS_Form_c0412af2d68c4c6589c8ca4f2ea7f006(eventobject);
        },
        "skin": "CopysknBackground0ac38f681673446"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_g53cc23ca45343a68b0346061ed6ec57,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};