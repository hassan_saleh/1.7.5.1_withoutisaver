function addWidgetsfrmRecentTransactionDetailsKA() {
    frmRecentTransactionDetailsKA.setDefaultUnit(kony.flex.DP);
    var titleBarWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarWrapper.setDefaultUnit(kony.flex.DP);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var androidTitleLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "androidTitleLabel",
        "isVisible": true,
        "left": "55dp",
        "skin": "sknnavBarTitle",
        "text": "Recent Transactions",
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var androidBack = new kony.ui.Button({
        "focusSkin": "sknandroidBackButtonFocus",
        "height": "50dp",
        "id": "androidBack",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_e3c6eee298c44f14a44cd8fc1fce382c,
        "skin": "sknandroidBackButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    androidTitleBar.add(androidTitleLabel, androidBack);
    var btnEditKA = new kony.ui.Button({
        "focusSkin": "sknbtn",
        "height": "50dp",
        "id": "btnEditKA",
        "isVisible": true,
        "onClick": AS_Button_e37f48203e78436c909c93b531a88c8c,
        "right": "10dp",
        "skin": "sknbtn",
        "text": "Edit",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    titleBarWrapper.add(androidTitleBar, btnEditKA);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": 0,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "50dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var FlexContainer042b0725667e643 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer042b0725667e643",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer042b0725667e643.setDefaultUnit(kony.flex.DP);
    var FlexContainer0f615b714fea843 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "150dp",
        "id": "FlexContainer0f615b714fea843",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0f615b714fea843.setDefaultUnit(kony.flex.DP);
    var transactionAmount = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionAmount",
        "isVisible": true,
        "skin": "skndetailPageNumber",
        "text": "$ 5006.00",
        "top": "30dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionTypeString = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionTypeString",
        "isVisible": true,
        "skin": "skndetailPageDate",
        "text": "Bill Payment to",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionName = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionName",
        "isVisible": true,
        "skin": "skn30363f110KA",
        "text": "Direct Energy 5445",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblP2PContactKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblP2PContactKA",
        "isVisible": true,
        "skin": "skndetailPageDate",
        "text": "+91 9848492905",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0f615b714fea843.add(transactionAmount, transactionTypeString, transactionName, lblP2PContactKA);
    var divider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    divider.setDefaultUnit(kony.flex.DP);
    divider.add();
    var transactionIdTemp = new kony.ui.Label({
        "id": "transactionIdTemp",
        "isVisible": false,
        "left": "8dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "11dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionType = new kony.ui.Label({
        "id": "transactionType",
        "isVisible": false,
        "left": "3dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer042b0725667e643.add(FlexContainer0f615b714fea843, divider, transactionIdTemp, transactionType);
    var additionalDetails1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75dp",
        "id": "additionalDetails1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    additionalDetails1.setDefaultUnit(kony.flex.DP);
    var Label0e7a26666ebf141 = new kony.ui.Label({
        "id": "Label0e7a26666ebf141",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsegmentHeaderText",
        "text": "From:",
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionFrom = new kony.ui.Label({
        "id": "transactionFrom",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "text": "Savings 2453",
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var divider2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    divider2.setDefaultUnit(kony.flex.DP);
    divider2.add();
    var lblseletedIndex = new kony.ui.Label({
        "id": "lblseletedIndex",
        "isVisible": false,
        "left": "251dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "26dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    additionalDetails1.add(Label0e7a26666ebf141, transactionFrom, divider2, lblseletedIndex);
    var CopyadditionalDetails0de6bb8c8818347 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75dp",
        "id": "CopyadditionalDetails0de6bb8c8818347",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyadditionalDetails0de6bb8c8818347.setDefaultUnit(kony.flex.DP);
    var CopyLabel00f281b798a674f = new kony.ui.Label({
        "id": "CopyLabel00f281b798a674f",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsegmentHeaderText",
        "text": "Transaction Id:",
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionId = new kony.ui.Label({
        "id": "transactionId",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "text": "Savings 2453",
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Copydivider0da2c2d28146c45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0da2c2d28146c45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider0da2c2d28146c45.setDefaultUnit(kony.flex.DP);
    Copydivider0da2c2d28146c45.add();
    var CopylblseletedIndex0b90e9d1c62b349 = new kony.ui.Label({
        "id": "CopylblseletedIndex0b90e9d1c62b349",
        "isVisible": false,
        "left": "251dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "26dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyadditionalDetails0de6bb8c8818347.add(CopyLabel00f281b798a674f, transactionId, Copydivider0da2c2d28146c45, CopylblseletedIndex0b90e9d1c62b349);
    var flxSchedule = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75dp",
        "id": "flxSchedule",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSchedule.setDefaultUnit(kony.flex.DP);
    var lblTransactionDateKA = new kony.ui.Label({
        "id": "lblTransactionDateKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsegmentHeaderText",
        "text": "Scheduled For :",
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransactionDateValueKA = new kony.ui.Label({
        "id": "lblTransactionDateValueKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "text": "05-20-2016",
        "top": "40dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Copydivider058b92c2528494d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider058b92c2528494d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider058b92c2528494d.setDefaultUnit(kony.flex.DP);
    Copydivider058b92c2528494d.add();
    flxSchedule.add(lblTransactionDateKA, lblTransactionDateValueKA, Copydivider058b92c2528494d);
    var flxReccurrence = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75dp",
        "id": "flxReccurrence",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxReccurrence.setDefaultUnit(kony.flex.DP);
    var lblReccurrenceLabelKA = new kony.ui.Label({
        "id": "lblReccurrenceLabelKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsegmentHeaderText",
        "text": "Reccurrence:",
        "top": "20dp",
        "width": "95%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblReccurrenceValueKA = new kony.ui.Label({
        "id": "lblReccurrenceValueKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "text": "Once a Month",
        "top": "40dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblReccurrenceNumberKA = new kony.ui.Label({
        "id": "lblReccurrenceNumberKA",
        "isVisible": false,
        "right": "5%",
        "skin": "sknNumber",
        "text": "Next 5 Times",
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Copydivider0d794f3c52b2a4f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0d794f3c52b2a4f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider0d794f3c52b2a4f.setDefaultUnit(kony.flex.DP);
    Copydivider0d794f3c52b2a4f.add();
    flxReccurrence.add(lblReccurrenceLabelKA, lblReccurrenceValueKA, lblReccurrenceNumberKA, Copydivider0d794f3c52b2a4f);
    var flxNotes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxNotes",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNotes.setDefaultUnit(kony.flex.DP);
    var flexNotesContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flexNotesContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexNotesContainer.setDefaultUnit(kony.flex.DP);
    var lblNotesLabelKA = new kony.ui.Label({
        "id": "lblNotesLabelKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsegmentHeaderText",
        "text": "Notes:",
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionNotes = new kony.ui.Label({
        "bottom": "20dp",
        "id": "transactionNotes",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "text": "Note details appear here. This can be a muliple line descripsion",
        "top": "10dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Copydivider0gad115d9e99342 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0gad115d9e99342",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider0gad115d9e99342.setDefaultUnit(kony.flex.DP);
    Copydivider0gad115d9e99342.add();
    flexNotesContainer.add(lblNotesLabelKA, transactionNotes, Copydivider0gad115d9e99342);
    var disputeTransactionFlex = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "disputeTransactionFlex",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d89955c9bd8b4438afbaf7c5a6a2cdf1,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    disputeTransactionFlex.setDefaultUnit(kony.flex.DP);
    var Copydivider0g40efbf51f2e43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 56,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0g40efbf51f2e43",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider0g40efbf51f2e43.setDefaultUnit(kony.flex.DP);
    Copydivider0g40efbf51f2e43.add();
    var Label0iaea4bf2e19d49 = new kony.ui.Label({
        "id": "Label0iaea4bf2e19d49",
        "isVisible": true,
        "left": "21dp",
        "skin": "sknNumber",
        "text": "Dispute Transaction",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Image0bc353ca63a3741 = new kony.ui.Image2({
        "height": "30dp",
        "id": "Image0bc353ca63a3741",
        "isVisible": true,
        "right": "5%",
        "skin": "slImage",
        "src": "phone_icon.png",
        "top": "14dp",
        "width": "30dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Copydivider0j4d309edce274d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0j4d309edce274d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider0j4d309edce274d.setDefaultUnit(kony.flex.DP);
    Copydivider0j4d309edce274d.add();
    disputeTransactionFlex.add(Copydivider0g40efbf51f2e43, Label0iaea4bf2e19d49, Image0bc353ca63a3741, Copydivider0j4d309edce274d);
    var repeatTransactionContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "repeatTransactionContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    repeatTransactionContainer.setDefaultUnit(kony.flex.DP);
    var repeatTransactionButton = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "60dp",
        "id": "repeatTransactionButton",
        "isVisible": true,
        "onClick": AS_Button_4d6a1510e635483f9b79d627343becf3,
        "skin": "sknsecondaryAction",
        "text": "Repeat Transaction",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Copydivider0b2398672408341 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0b2398672408341",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    Copydivider0b2398672408341.setDefaultUnit(kony.flex.DP);
    Copydivider0b2398672408341.add();
    repeatTransactionContainer.add(repeatTransactionButton, Copydivider0b2398672408341);
    flxNotes.add(flexNotesContainer, disputeTransactionFlex, repeatTransactionContainer);
    var FlexContainerConfirmandEditBtns = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainerConfirmandEditBtns",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainerConfirmandEditBtns.setDefaultUnit(kony.flex.DP);
    var flexLabelWithdraw = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flexLabelWithdraw",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexLabelWithdraw.setDefaultUnit(kony.flex.DP);
    var lblCashWithdrawCode = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblCashWithdrawCode",
        "isVisible": true,
        "skin": "sknPendingLabel",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.cashWithdrawalCode"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexLabelWithdraw.add(lblCashWithdrawCode);
    var flxPassCodeAndQRCode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxPassCodeAndQRCode",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPassCodeAndQRCode.setDefaultUnit(kony.flex.DP);
    var lblPassCode = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPassCode",
        "isVisible": true,
        "skin": "sknPassCodeLabel",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.passcode"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgQRCode = new kony.ui.Image2({
        "centerX": "50%",
        "height": "60dp",
        "id": "imgQRCode",
        "isVisible": true,
        "onTouchEnd": AS_Image_b3f45ffae91642aab2632c09b38915dc,
        "skin": "slImage",
        "src": "qrcode.png",
        "top": "5dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPassCodeAndQRCode.add(lblPassCode, imgQRCode);
    var flexExpiry = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flexExpiry",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexExpiry.setDefaultUnit(kony.flex.DP);
    var lblExpiryTime = new kony.ui.Label({
        "centerX": "42%",
        "id": "lblExpiryTime",
        "isVisible": false,
        "left": "28%",
        "skin": "sknErrorMessageEC223BKA",
        "text": "Expires in: ",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblExpiryTimeValue = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblExpiryTimeValue",
        "isVisible": true,
        "left": "1%",
        "right": "32%",
        "skin": "sknErrorMessageEC223BKA",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.expiryFullValue"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexExpiry.add(lblExpiryTime, lblExpiryTimeValue);
    var flxShareOptions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxShareOptions",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxShareOptions.setDefaultUnit(kony.flex.DP);
    var lblShareCode = new kony.ui.Label({
        "centerX": "35%",
        "centerY": "50%",
        "id": "lblShareCode",
        "isVisible": true,
        "skin": "sknPendingLabel",
        "text": "Share withdraw code",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var FlexContainer0cab5cb4a249945 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "80%",
        "id": "FlexContainer0cab5cb4a249945",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "width": "10%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0cab5cb4a249945.setDefaultUnit(kony.flex.DP);
    var imgMail = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgMail",
        "isVisible": true,
        "onTouchEnd": AS_Image_a35d599adb7044d9a1edd9350dde6f23,
        "skin": "slImage",
        "src": "mail_icon.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer0cab5cb4a249945.add(imgMail);
    var CopyFlexContainer0a08d08aacc6145 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "80%",
        "id": "CopyFlexContainer0a08d08aacc6145",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "width": "10%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0a08d08aacc6145.setDefaultUnit(kony.flex.DP);
    var imgMessage = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgMessage",
        "isVisible": true,
        "onTouchEnd": AS_Image_eb78e5b0eff448b9be22fff0d5cd46ad,
        "skin": "slImage",
        "src": "messageack_icon.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer0a08d08aacc6145.add(imgMessage);
    flxShareOptions.add(lblShareCode, FlexContainer0cab5cb4a249945, CopyFlexContainer0a08d08aacc6145);
    var flxBottomAck = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "65dp",
        "id": "flxBottomAck",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomAck.setDefaultUnit(kony.flex.DP);
    var flxWatchDemo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxWatchDemo",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "9%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_ed1393b891a74e72bc5ebc01f85f6567,
        "skin": "sknBottomFlex",
        "top": "6%",
        "width": "40%",
        "zIndex": 1
    }, {}, {});
    flxWatchDemo.setDefaultUnit(kony.flex.DP);
    var imgPlay = new kony.ui.Image2({
        "centerY": "50%",
        "height": "65%",
        "id": "imgPlay",
        "isVisible": true,
        "left": "5dp",
        "src": "play_blue.png",
        "top": "12dp",
        "width": "25%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblWatchDemo = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblWatchDemo",
        "isVisible": true,
        "left": "4%",
        "right": "5dp",
        "skin": "sknLabelPendingColors",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.watchdemovideo"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 5, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxWatchDemo.add(imgPlay, lblWatchDemo);
    var flxFindATM = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxFindATM",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": 9,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_b94cf2e11fbe4abcab6ed5c2f2dba4f7,
        "skin": "sknBottomFlex",
        "top": "6%",
        "width": "40%",
        "zIndex": 1
    }, {}, {});
    flxFindATM.setDefaultUnit(kony.flex.DP);
    var imgLocation = new kony.ui.Image2({
        "centerY": "50%",
        "height": "65%",
        "id": "imgLocation",
        "isVisible": true,
        "left": "5dp",
        "skin": "slImage",
        "src": "placeholder_blue.png",
        "top": "12dp",
        "width": "25%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblFindATM = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblFindATM",
        "isVisible": true,
        "left": "4%",
        "right": "5dp",
        "skin": "sknLabelPendingColors",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.findnearbyatm"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "75dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxFindATM.add(imgLocation, lblFindATM);
    flxBottomAck.add(flxWatchDemo, flxFindATM);
    var Copydivider0d9dbf93f2ab844 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0d9dbf93f2ab844",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    Copydivider0d9dbf93f2ab844.setDefaultUnit(kony.flex.DP);
    Copydivider0d9dbf93f2ab844.add();
    var FlexContainer0be1121ad10c545 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "FlexContainer0be1121ad10c545",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0be1121ad10c545.setDefaultUnit(kony.flex.DP);
    var lblCancelWithdraw = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "48%",
        "id": "lblCancelWithdraw",
        "isVisible": true,
        "skin": "sknLabelPendingColors",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.cancelWithdraw"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnCancel = new kony.ui.Button({
        "focusSkin": "skninvisibleButtonFocus",
        "height": "100%",
        "id": "btnCancel",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_a548552e86864764a2ec7eed19913395,
        "skin": "skninvisibleButtonNormal",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer0be1121ad10c545.add(lblCancelWithdraw, btnCancel);
    FlexContainerConfirmandEditBtns.add(flexLabelWithdraw, flxPassCodeAndQRCode, flexExpiry, flxShareOptions, flxBottomAck, Copydivider0d9dbf93f2ab844, FlexContainer0be1121ad10c545);
    mainContent.add(FlexContainer042b0725667e643, additionalDetails1, CopyadditionalDetails0de6bb8c8818347, flxSchedule, flxReccurrence, flxNotes, FlexContainerConfirmandEditBtns);
    frmRecentTransactionDetailsKA.add(titleBarWrapper, mainContent);
};

function frmRecentTransactionDetailsKAGlobals() {
    frmRecentTransactionDetailsKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmRecentTransactionDetailsKA,
        "allowHorizontalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmRecentTransactionDetailsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 3
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_f8251976753249fc8e1fab152b06233d,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};