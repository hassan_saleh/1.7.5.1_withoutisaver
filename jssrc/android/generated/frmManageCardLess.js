function addWidgetsfrmManageCardLess() {
    frmManageCardLess.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "23%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxTop = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35%",
        "id": "flxTop",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxTop.setDefaultUnit(kony.flex.DP);
    var flxback = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxback",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_ce2ae776a8514f3889cacee618fdc733,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 2
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxback.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0%",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxback.add(lblBackIcon, lblBack);
    var lblCardlessTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblCardlessTitle",
        "isVisible": true,
        "minHeight": "90%",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.Cardless.NewCardless"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "58%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnNextCardless = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "height": "100%",
        "id": "btnNextCardless",
        "isVisible": false,
        "right": "0%",
        "skin": "jomopaynextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "top": "0%",
        "width": "20%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTop.add(flxback, lblCardlessTitle, btnNextCardless);
    var flxTab = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "28%",
        "id": "flxTab",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTab.setDefaultUnit(kony.flex.DP);
    var flxContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "65%",
        "id": "flxContent",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFboxOuterRing",
        "top": "0dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxContent.setDefaultUnit(kony.flex.DP);
    var btnAll = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnAll",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_j2a9139b75c04ccab872a570a97ab7f3,
        "skin": "slButtonWhiteTab",
        "text": kony.i18n.getLocalizedString("i18n.Bene.All"),
        "top": "0dp",
        "width": "25%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {});
    var btnPending = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnPending",
        "isVisible": true,
        "left": "25%",
        "onClick": AS_Button_d5b76b070e0c49fbaecaa4064048e6d5,
        "skin": "slButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Cardless.pending"),
        "top": "0dp",
        "width": "25%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {});
    var btnAccepted = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "98%",
        "id": "btnAccepted",
        "isVisible": true,
        "left": "50%",
        "onClick": AS_Button_g6591d159ae44a0a90699cbb33a21892,
        "skin": "slButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Cardless.accepted"),
        "width": "25%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {});
    var btnCancel = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnCancel",
        "isVisible": true,
        "left": "75%",
        "onClick": AS_Button_da7309a46eec47aa9772221ad433b427,
        "skin": "slButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Cardless.Reject"),
        "top": "0dp",
        "width": "25%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {});
    flxContent.add(btnAll, btnPending, btnAccepted, btnCancel);
    flxTab.add(flxContent);
    var flxSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "28%",
        "id": "flxSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSearch.setDefaultUnit(kony.flex.DP);
    var lblSearch = new kony.ui.Label({
        "height": "100%",
        "id": "lblSearch",
        "isVisible": true,
        "left": "0%",
        "skin": "sknSearchIcon",
        "text": "h",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxUnderLineSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderLineSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "92%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderLineSearch.setDefaultUnit(kony.flex.DP);
    flxUnderLineSearch.add();
    var tbxSearch = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "focusSkin": "sknTxtBoxSearch",
        "height": "100%",
        "id": "tbxSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "10%",
        "onDone": AS_TextField_bd7ced25b9634305872de87768d51c05,
        "onTextChange": AS_TextField_i92c39cb415347a986e12fbfe3a94786,
        "placeholder": kony.i18n.getLocalizedString("i18n.transfer.searchPlh"),
        "secureTextEntry": false,
        "skin": "sknTxtBoxSearch",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "90%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknPlaceHolder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxSearch.add(lblSearch, flxUnderLineSearch, tbxSearch);
    flxHeader.add(flxTop, flxTab, flxSearch);
    var segCardlessTransactions = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "accountNumber": "Date",
            "amount": "",
            "amountLbl": "Amount",
            "btnDelete": "w",
            "lblStatus": "Label",
            "transactionDate": "",
            "transactionDateLbl": "Date"
        }, {
            "accountNumber": "Date",
            "amount": "",
            "amountLbl": "Amount",
            "btnDelete": "w",
            "lblStatus": "Label",
            "transactionDate": "",
            "transactionDateLbl": "Date"
        }, {
            "accountNumber": "Date",
            "amount": "",
            "amountLbl": "Amount",
            "btnDelete": "w",
            "lblStatus": "Label",
            "transactionDate": "",
            "transactionDateLbl": "Date"
        }],
        "groupCells": false,
        "height": "67%",
        "id": "segCardlessTransactions",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "slSegSendMoney",
        "rowTemplate": flxManageCardlessNormal,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "accountNumber": "accountNumber",
            "amount": "amount",
            "amountLbl": "amountLbl",
            "btnDelete": "btnDelete",
            "contactListDivider": "contactListDivider",
            "flxAnimate": "flxAnimate",
            "flxButtonHolder": "flxButtonHolder",
            "flxDetails": "flxDetails",
            "flxManageCardlessNormal": "flxManageCardlessNormal",
            "flxToAnimate": "flxToAnimate",
            "lblStatus": "lblStatus",
            "transactionDate": "transactionDate",
            "transactionDateLbl": "transactionDateLbl"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSearchResult = new kony.ui.Label({
        "centerX": "50%",
        "height": "68%",
        "id": "lblSearchResult",
        "isVisible": false,
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.Cardless.noTransactions"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxButtonAdd = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxButtonAdd",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxButtonAdd.setDefaultUnit(kony.flex.DP);
    var btnAddCardless = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "70%",
        "id": "btnAddCardless",
        "isVisible": true,
        "maxHeight": "70%",
        "onTouchEnd": AS_Button_d4029aa9fa854a2b886033a5e39a22f4,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Cardless.NewCardless"),
        "width": "70%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxButtonAdd.add(btnAddCardless);
    flxMainContainer.add(flxHeader, segCardlessTransactions, lblSearchResult, flxButtonAdd);
    frmManageCardLess.add(flxMainContainer);
};

function frmManageCardLessGlobals() {
    frmManageCardLess = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmManageCardLess,
        "enabledForIdleTimeout": false,
        "id": "frmManageCardLess",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "postShow": AS_Form_h97d62157c9c482b937da32a8e15957e,
        "skin": "slFormCommon"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};