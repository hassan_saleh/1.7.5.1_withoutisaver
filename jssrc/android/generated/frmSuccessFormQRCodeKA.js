function addWidgetsfrmSuccessFormQRCodeKA() {
    frmSuccessFormQRCodeKA.setDefaultUnit(kony.flex.DP);
    var successContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "successContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    successContainer.setDefaultUnit(kony.flex.DP);
    var FlexContainer09d07d8fed6c64b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "40%",
        "clipBounds": false,
        "height": "60dp",
        "id": "FlexContainer09d07d8fed6c64b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer09d07d8fed6c64b.setDefaultUnit(kony.flex.DP);
    var successIcon = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": false,
        "height": "50dp",
        "id": "successIcon",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknsuccessIcon",
        "top": "4dp",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    successIcon.setDefaultUnit(kony.flex.DP);
    successIcon.add();
    var successIcon2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": false,
        "height": "60dp",
        "id": "successIcon2",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknsuccessIcon",
        "top": "4dp",
        "width": "60dp",
        "zIndex": 1
    }, {}, {});
    successIcon2.setDefaultUnit(kony.flex.DP);
    var successImage2 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "50%",
        "id": "successImage2",
        "isVisible": true,
        "skin": "sknslImage",
        "src": "success_large_check.png",
        "width": "50%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    successIcon2.add(successImage2);
    FlexContainer09d07d8fed6c64b.add(successIcon, successIcon2);
    var processing = new kony.ui.Label({
        "id": "processing",
        "isVisible": true,
        "left": "15%",
        "skin": "sknCopyslLabel0aaa8f2eefd874b",
        "text": kony.i18n.getLocalizedString("i18n.success.processingTransaction"),
        "top": "10dp",
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var successInfoLabel = new kony.ui.Label({
        "id": "successInfoLabel",
        "isVisible": true,
        "left": "15%",
        "skin": "sknCopyslLabel0728dfcfd79a74e",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.successQrCodeInfo"),
        "top": "10dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var innerSuccessContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "200dp",
        "id": "innerSuccessContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    innerSuccessContainer.setDefaultUnit(kony.flex.DP);
    var successText = new kony.ui.Label({
        "id": "successText",
        "isVisible": true,
        "left": "15%",
        "skin": "sknCopyslLabel0728dfcfd79a74e",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.successText"),
        "top": "10dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var successTitle = new kony.ui.Label({
        "id": "successTitle",
        "isVisible": true,
        "left": "10%",
        "right": "10%",
        "skin": "sknCopyslLabel0728dfcfd79a74e",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.successTitle"),
        "top": "7dp",
        "width": "80%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var successContinue = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionReversedFocus",
        "height": "42dp",
        "id": "successContinue",
        "isVisible": true,
        "onClick": AS_Button_b644b50814b64ea1b08e0ecfd9fc025c,
        "skin": "sknsecondaryActionReversed",
        "text": kony.i18n.getLocalizedString("i18n.common.btnContinue"),
        "top": "30dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnEditTranscation = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "35dp",
        "id": "btnEditTranscation",
        "isVisible": false,
        "skin": "sknsecondaryActionLink",
        "text": kony.i18n.getLocalizedString("i18n.accountOverview.GotoAccountOverview"),
        "top": "5dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    innerSuccessContainer.add(successText, successTitle, successContinue, btnEditTranscation);
    successContainer.add(FlexContainer09d07d8fed6c64b, processing, successInfoLabel, innerSuccessContainer);
    var flxSignOutKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxSignOutKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSignOutKA.setDefaultUnit(kony.flex.DP);
    var signOutButton = new kony.ui.Button({
        "focusSkin": "skntitleBarTextButtonFocus",
        "height": "50dp",
        "id": "signOutButton",
        "isVisible": false,
        "right": "0.00%",
        "skin": "sknsecondaryActionLink",
        "text": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "top": "0dp",
        "width": "90dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSignOutKA.add(signOutButton);
    frmSuccessFormQRCodeKA.add(successContainer, flxSignOutKA);
};

function frmSuccessFormQRCodeKAGlobals() {
    frmSuccessFormQRCodeKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSuccessFormQRCodeKA,
        "bounces": false,
        "enabledForIdleTimeout": true,
        "id": "frmSuccessFormQRCodeKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_a94e31da69ba4554891a05623ee21254,
        "preShow": function(eventobject) {
            AS_Form_df1670c375c24f9c9efc48935455d66f(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_jd361bd30eee46c680efa465f419e8e5,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};