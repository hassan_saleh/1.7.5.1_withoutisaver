function initializetmpChooseAccountsSettings() {
    flxSegChooseAccounts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "95dp",
        "id": "flxSegChooseAccounts",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknflxtmpAccountDetailsScreen"
    }, {}, {});
    flxSegChooseAccounts.setDefaultUnit(kony.flex.DP);
    var lblAccountName = new kony.ui.Label({
        "id": "lblAccountName",
        "isVisible": true,
        "left": "8%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": "78%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {
        "textCopyable": false
    });
    var Symbol = new kony.ui.Label({
        "centerY": "50%",
        "id": "Symbol",
        "isVisible": true,
        "right": "4%",
        "skin": "sknBackIconDisabled",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "8%",
        "skin": "lblsegtextsmall0b5a3b38d4be646",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "17dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {
        "textCopyable": false
    });
    var lblDefaultAccPayment = new kony.ui.Label({
        "height": "45%",
        "id": "lblDefaultAccPayment",
        "isVisible": false,
        "right": "5%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Balance",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {
        "textCopyable": false
    });
    var lblAmount = new kony.ui.Label({
        "height": "45%",
        "id": "lblAmount",
        "isVisible": false,
        "right": "3%",
        "skin": "Copylblsegtextsmall0a16789105f8b49",
        "text": "2,7453 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {
        "textCopyable": false
    });
    var lblDefaultAccTransfer = new kony.ui.Label({
        "id": "lblDefaultAccTransfer",
        "isVisible": false,
        "left": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Salary Account",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {
        "textCopyable": false
    });
    var lblACCHideShow = new kony.ui.Label({
        "id": "lblACCHideShow",
        "isVisible": false,
        "left": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Salary Account",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {
        "textCopyable": false
    });
    var lblAccNickname = new kony.ui.Label({
        "id": "lblAccNickname",
        "isVisible": false,
        "left": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Salary Account",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {
        "textCopyable": false
    });
    var lblAccName = new kony.ui.Label({
        "id": "lblAccName",
        "isVisible": false,
        "left": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false
    }, {
        "textCopyable": false
    });
    flxSegChooseAccounts.add(lblAccountName, Symbol, lblAccountNumber, lblDefaultAccPayment, lblAmount, lblDefaultAccTransfer, lblACCHideShow, lblAccNickname, lblAccName);
}