function addWidgetsfrmRequestPinCard() {
    frmRequestPinCard.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b7d74518f07a4d",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var lblFormHeading = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "45%",
        "height": "100%",
        "id": "lblFormHeading",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.requestPinCard.Title"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxNext = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxNext",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_c12b53f391f84ea7b80a649f74fe508b,
        "right": "2%",
        "skin": "slFbox",
        "top": "5%",
        "width": "18%",
        "zIndex": 2
    }, {}, {});
    flxNext.setDefaultUnit(kony.flex.DP);
    var lblNext = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Next Page"
        },
        "height": "90%",
        "id": "lblNext",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNext.add(lblNext);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d1fb8356cb31462e886d0cf525830a36,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btn = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "0%",
        "id": "btn",
        "isVisible": true,
        "left": "0%",
        "skin": "btnBack0b71f859656c647",
        "top": "0%",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBack.add(lblBackIcon, lblBack, btn);
    flxHeader.add(lblFormHeading, flxNext, flxBack);
    var btnConfirm = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhite",
        "height": "8%",
        "id": "btnConfirm",
        "isVisible": true,
        "left": "10%",
        "onClick": AS_Button_j5768a5e27cc40fdbbecfacd5de9472f,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.P2P.confirm"),
        "top": "86%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "30%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "8dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "32%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var lblLock = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblLock",
        "isVisible": false,
        "skin": "sknlblBojFont2LargeIconnn",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPin = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "35%",
        "id": "flxPin",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "33dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_f48b8e9619ac4df997032df63a53adf9,
        "skin": "sknflxTransprnt",
        "top": "4%",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    flxPin.setDefaultUnit(kony.flex.DP);
    var lblCircle2 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "the"
        },
        "centerY": "50%",
        "id": "lblCircle2",
        "isVisible": false,
        "left": "30%",
        "skin": "sknlblBojFont2sTARiCON",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCircle3 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "PIN"
        },
        "centerY": "50%",
        "id": "lblCircle3",
        "isVisible": false,
        "left": "50%",
        "skin": "sknlblBojFont2sTARiCON",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCircle4 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Number"
        },
        "centerY": "50%",
        "id": "lblCircle4",
        "isVisible": false,
        "left": "70%",
        "skin": "sknlblBojFont2sTARiCON",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtPIN = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Please enter your PIN and click next"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknFlxPIN",
        "height": "40dp",
        "id": "txtPIN",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "300%",
        "maxTextLength": 4,
        "onTextChange": AS_TextField_e79ee9bcd2ca4c8cade3de2a115cb7a9,
        "secureTextEntry": false,
        "skin": "sknFlxPIN",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknFlxPIN",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxPin.add(lblCircle2, lblCircle3, lblCircle4, txtPIN);
    var lblCircle1 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Enter"
        },
        "centerX": "50%",
        "id": "lblCircle1",
        "isVisible": true,
        "left": "0%",
        "skin": "CopysknlblBojFontNewLarge",
        "text": "O",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMain.add(lblLock, flxPin, lblCircle1);
    var Label0bd39896ab0b246 = new kony.ui.Label({
        "id": "Label0bd39896ab0b246",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.reqNewPinDicl"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "70%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBranchCode = new kony.ui.Label({
        "height": "1px",
        "id": "lblBranchCode",
        "isVisible": false,
        "left": "100%",
        "skin": "sknLblWhite100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "1px",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBranch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxBranch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_h2ad52becff34f7f806f5ab6766a62bd,
        "skin": "slFbox0f0c75590716b41",
        "top": "12%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxBranch.setDefaultUnit(kony.flex.DP);
    var lblCurrHead = new kony.ui.Label({
        "height": "35%",
        "id": "lblCurrHead",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.cards.reasonhead3"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBranch = new kony.ui.Label({
        "bottom": "0%",
        "centerX": "50%",
        "height": "48%",
        "id": "lblBranch",
        "isVisible": true,
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.cards.reasonhead3"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "28%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLine1 = new kony.ui.Label({
        "centerX": "50%",
        "height": "2%",
        "id": "lblLine1",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "77%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxEdit0e3e34b1bf78049 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "CopyflxEdit0e3e34b1bf78049",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "53dp",
        "zIndex": 1
    }, {}, {});
    CopyflxEdit0e3e34b1bf78049.setDefaultUnit(kony.flex.DP);
    var CopylblEdit0b4a3f2847e3d44 = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblEdit0b4a3f2847e3d44",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblEditDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxEdit0e3e34b1bf78049.add(CopylblEdit0b4a3f2847e3d44);
    flxBranch.add(lblCurrHead, lblBranch, lblLine1, CopyflxEdit0e3e34b1bf78049);
    var flxInvalidAmountField = new kony.ui.Label({
        "id": "flxInvalidAmountField",
        "isVisible": false,
        "right": "5%",
        "skin": "sknInvalidCredKA",
        "text": kony.i18n.getLocalizedString("18n.deliveryBranchSelect"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "23%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    frmRequestPinCard.add(flxHeader, btnConfirm, flxMain, Label0bd39896ab0b246, lblBranchCode, flxBranch, flxInvalidAmountField);
};

function frmRequestPinCardGlobals() {
    frmRequestPinCard = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmRequestPinCard,
        "enabledForIdleTimeout": true,
        "id": "frmRequestPinCard",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_e9a1b0474d0144ce9a7ecb387fa6eb97,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};