function initializesegtmpltchecknocolor() {
    CopyFlexContainer011baa9bcad7141 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknflxTransprnt",
        "height": "70dp",
        "id": "CopyFlexContainer011baa9bcad7141",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknflxTransprnt"
    }, {}, {});
    CopyFlexContainer011baa9bcad7141.setDefaultUnit(kony.flex.DP);
    var Imgcheck = new kony.ui.Image2({
        "id": "Imgcheck",
        "isVisible": false,
        "left": "3%",
        "skin": "slImage",
        "src": "checkf.png",
        "top": "1%",
        "width": "5%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var transactionDate = new kony.ui.Label({
        "centerY": "27.78%",
        "id": "transactionDate",
        "isVisible": true,
        "left": "5%",
        "skin": "loansDealsTextSkin",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionAmount = new kony.ui.Label({
        "centerY": "30%",
        "id": "transactionAmount",
        "isVisible": true,
        "right": "8%",
        "skin": "sknNumber",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSepKA = new kony.ui.Label({
        "bottom": "0%",
        "id": "lblSepKA",
        "isVisible": false,
        "right": "0%",
        "skin": "sknLineEDEDEDKA",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "95%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxImageandNameKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxImageandNameKA",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "40%",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    flxImageandNameKA.setDefaultUnit(kony.flex.DP);
    flxImageandNameKA.add();
    var ImgWithDraw = new kony.ui.Image2({
        "centerY": "25%",
        "id": "ImgWithDraw",
        "isVisible": false,
        "left": "2%",
        "skin": "slImage",
        "src": "pending.png",
        "top": "0%",
        "width": "5%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblExpiryTime = new kony.ui.Label({
        "id": "lblExpiryTime",
        "isVisible": false,
        "right": "25%",
        "skin": "sknErrorMessageEC223BKA",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLastTransaction = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLastTransaction",
        "isVisible": false,
        "left": "5%",
        "skin": "sknNumber",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionAmountSI = new kony.ui.Label({
        "centerY": "50%",
        "id": "transactionAmountSI",
        "isVisible": false,
        "right": "10%",
        "skin": "sknNumber",
        "text": "$155.00",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionName = new kony.ui.Label({
        "centerY": "65%",
        "id": "transactionName",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNoData = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNoData",
        "isVisible": false,
        "left": "15dp",
        "skin": "lblSegName",
        "text": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer011baa9bcad7141.add(Imgcheck, transactionDate, transactionAmount, lblSepKA, flxImageandNameKA, ImgWithDraw, lblExpiryTime, lblLastTransaction, transactionAmountSI, transactionName, lblNoData);
}