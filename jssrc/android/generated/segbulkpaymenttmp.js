function initializesegbulkpaymenttmp() {
    flxBulkPaymentSeg = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxBulkPaymentSeg",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    flxBulkPaymentSeg.setDefaultUnit(kony.flex.DP);
    var flxToAnimate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxToAnimate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxToAnimate.setDefaultUnit(kony.flex.DP);
    var flxAnimate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxAnimate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxAnimate.setDefaultUnit(kony.flex.DP);
    var lblBillerType = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBillerType",
        "isVisible": false,
        "left": "80%",
        "skin": "lblsegtextsmall",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "18%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false
    });
    var flxDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "20%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxDetails.setDefaultUnit(kony.flex.DP);
    var payeename = new kony.ui.Label({
        "centerY": "15%",
        "id": "payeename",
        "isVisible": true,
        "left": "4%",
        "skin": "sknLblNextDisabled",
        "text": "wertyui",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var payeenickname = new kony.ui.Label({
        "centerY": "56.35%",
        "height": "20dp",
        "id": "payeenickname",
        "isVisible": false,
        "left": "7%",
        "skin": "sknRegisterMobileBank",
        "top": 50,
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var accountnumber = new kony.ui.Label({
        "centerY": "35%",
        "id": "accountnumber",
        "isVisible": true,
        "left": "4%",
        "skin": "sknLblAccNumBiller",
        "text": "09876543234567",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var dueAmount = new kony.ui.Label({
        "centerY": "55%",
        "id": "dueAmount",
        "isVisible": true,
        "left": "4%",
        "skin": "sknLblAccNumBiller",
        "text": "asdfghj98765432",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var hiddenDueAmount = new kony.ui.Label({
        "centerY": "75%",
        "height": "30%",
        "id": "hiddenDueAmount",
        "isVisible": false,
        "left": "4%",
        "skin": "sknLblAccNumBiller",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPaidAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "75%",
        "clipBounds": true,
        "height": "50%",
        "id": "flxPaidAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    flxPaidAmount.setDefaultUnit(kony.flex.DP);
    var lblpaidAmount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblpaidAmount",
        "isVisible": false,
        "left": "0%",
        "skin": "sknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.bulkpayment.paidamount"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtpaidAmount = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "focusSkin": "sknTxtBox90",
        "id": "txtpaidAmount",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "34%",
        "secureTextEntry": false,
        "skin": "sknTxtBox90",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var paidAmount = new kony.ui.Label({
        "centerY": "50%",
        "id": "paidAmount",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblAccNumBiller",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPaidAmount.add(lblpaidAmount, txtpaidAmount, paidAmount);
    flxDetails.add(payeename, payeenickname, accountnumber, dueAmount, hiddenDueAmount, flxPaidAmount);
    var flxIconContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIconContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "50dp",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxIconContainer.setDefaultUnit(kony.flex.DP);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxToIcon",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTick = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblTick",
        "isVisible": true,
        "skin": "sknBOJttfwhitee150",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon1.add(lblInitial, lblTick);
    flxIconContainer.add(flxIcon1);
    flxAnimate.add(lblBillerType, flxDetails, flxIconContainer);
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknsegmentDivider",
        "top": "99%",
        "width": "100%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    var btnBillsPayAccounts = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopysknSegmentDelete0h225a870b5144b",
        "height": "50%",
        "id": "btnBillsPayAccounts",
        "isVisible": true,
        "left": "5%",
        "onClick": AS_Button_a438737d3f0b4c7a8a371526c4acb10e,
        "skin": "CopysknSegmentDelete0h225a870b5144b",
        "width": "14%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {});
    var lblBulkSelection = new kony.ui.Label({
        "height": "22%",
        "id": "lblBulkSelection",
        "isVisible": false,
        "left": "14%",
        "skin": "sknBulkPaymentSelection",
        "text": "r",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19%",
        "width": "6%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false
    });
    flxToAnimate.add(flxAnimate, contactListDivider, btnBillsPayAccounts, lblBulkSelection);
    flxBulkPaymentSeg.add(flxToAnimate);
}