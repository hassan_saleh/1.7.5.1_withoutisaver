function addWidgetsfrmEstatementConfirmKA() {
    frmEstatementConfirmKA.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox0b7d74518f07a4d",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_e94e370703c841bc94b26a94b2422be3,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBackText = new kony.ui.Label({
        "height": "100%",
        "id": "lblBackText",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBackText);
    var lblHeader = new kony.ui.Label({
        "centerX": "50%",
        "height": "100%",
        "id": "lblHeader",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.Estmt.Confirm"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblClose = new kony.ui.Label({
        "centerX": "94%",
        "height": "100%",
        "id": "lblClose",
        "isVisible": false,
        "onTouchEnd": AS_Label_f330c2e5c5de446db41adde60324fa92,
        "skin": "sknCloseConfirm",
        "text": "O",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(flxBack, lblHeader, lblClose);
    var flxBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "91%",
        "id": "flxBody",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "9%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBody.setDefaultUnit(kony.flex.DP);
    var flxEmailData1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxEmailData1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxEmailData1.setDefaultUnit(kony.flex.DP);
    var lblEmailText1 = new kony.ui.Label({
        "height": "40%",
        "id": "lblEmailText1",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Estmt.Email1Text"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEamail1 = new kony.ui.Label({
        "height": "58%",
        "id": "lblEamail1",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblBack",
        "text": "YSenan@bankofjordan.com.jo",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "42%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEmailData1.add(lblEmailText1, lblEamail1);
    var flxEmailData2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxEmailData2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "17%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxEmailData2.setDefaultUnit(kony.flex.DP);
    var lblEmailText2 = new kony.ui.Label({
        "height": "40%",
        "id": "lblEmailText2",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Estmt.Email2Text"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEamail2 = new kony.ui.Label({
        "height": "58%",
        "id": "lblEamail2",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblBack",
        "text": "halsaleh@bankofjordan.com.jo",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "42%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEmailData2.add(lblEmailText2, lblEamail2);
    var lblWarningtext = new kony.ui.Label({
        "bottom": "20%",
        "id": "lblWarningtext",
        "isVisible": true,
        "left": "4%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Estmt.Warning"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "92%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnConfirmEstmt = new kony.ui.Button({
        "bottom": "5%",
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "9%",
        "id": "btnConfirmEstmt",
        "isVisible": true,
        "onClick": AS_Button_c115dc2e244d45609aade97c46ff0db4,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Estmt.Confirm"),
        "width": "80%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "pressedSkin": "slButtonWhiteFocus"
    });
    flxBody.add(flxEmailData1, flxEmailData2, lblWarningtext, btnConfirmEstmt);
    frmEstatementConfirmKA.add(flxHeader, flxBody);
};

function frmEstatementConfirmKAGlobals() {
    frmEstatementConfirmKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmEstatementConfirmKA,
        "allowHorizontalBounce": false,
        "allowVerticalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmEstatementConfirmKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": function(eventobject) {
            AS_Form_f4aaa6a7700348d39526f7f7bd6cbc0c(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceMenu": AS_Form_jc6acd88647a4638956fd5632d7f3443,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};