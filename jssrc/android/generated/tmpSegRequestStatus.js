function initializetmpSegRequestStatus() {
    flxtmpSegRequestStatus = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxtmpSegRequestStatus",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxtmpSegRequestStatus.setDefaultUnit(kony.flex.DP);
    var lblJoMoPay = new kony.ui.Label({
        "id": "lblJoMoPay",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblAmountCurrency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStatus = new kony.ui.Label({
        "id": "lblStatus",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblAccountStaticText",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStatusDesc = new kony.ui.Label({
        "id": "lblStatusDesc",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-5dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRequestDate = new kony.ui.Label({
        "id": "lblRequestDate",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblAccountStaticText",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRequestDateVale = new kony.ui.Label({
        "id": "lblRequestDateVale",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-5dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDelDate = new kony.ui.Label({
        "id": "lblDelDate",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblAccountStaticText",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDelDateValue = new kony.ui.Label({
        "id": "lblDelDateValue",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-5dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknflxLinewhiteOp",
        "top": "12dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    flxtmpSegRequestStatus.add(lblJoMoPay, lblStatus, lblStatusDesc, lblRequestDate, lblRequestDateVale, lblDelDate, lblDelDateValue, flxLine);
}