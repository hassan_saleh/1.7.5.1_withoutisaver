function addWidgetsfrmUserSettingsTouchIdKA() {
    frmUserSettingsTouchIdKA.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var lblTouchIdHeader = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblTouchIdHeader",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.common.touchID"),
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_eed8c4d0cfe64bdfa40b813f89cfdbb8,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBacktext = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblBacktext",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBacktext);
    flxHeader.add(lblTouchIdHeader, flxBack);
    var flxBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "9%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBody.setDefaultUnit(kony.flex.DP);
    var lblTouchBodytxt = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblTouchBodytxt",
        "isVisible": true,
        "skin": "sknstandardTextBold",
        "text": kony.i18n.getLocalizedString("i18n.common.touchText"),
        "top": "3%",
        "width": "90%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAlterAuthTouchID = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "8%",
        "id": "flxAlterAuthTouchID",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxLightGreyColor",
        "top": "3%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAlterAuthTouchID.setDefaultUnit(kony.flex.DP);
    var lblAltText = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAltText",
        "isVisible": true,
        "left": "5%",
        "skin": "sknlblTouchIdsmall",
        "text": kony.i18n.getLocalizedString("i18n.common.touchIDEnable"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxSwitchOffTouchLogin = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSwitchOffTouchLogin",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d1ea91157aa4431c8c002da4b74e7280,
        "right": "3%",
        "skin": "sknflxGrey",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSwitchOffTouchLogin.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0hfee6fe5ed994b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0hfee6fe5ed994b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0hfee6fe5ed994b.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0hfee6fe5ed994b.add();
    var Copyflxlakeer0a0654737f4504c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0a0654737f4504c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0a0654737f4504c.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0a0654737f4504c.add();
    flxSwitchOffTouchLogin.add(CopyflxRoundDBlue0hfee6fe5ed994b, Copyflxlakeer0a0654737f4504c);
    var flxSwitchOnTouchLogin = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSwitchOnTouchLogin",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d856355e89594d518c952fbacaeae3e6,
        "right": "3%",
        "skin": "sknflxyellow",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSwitchOnTouchLogin.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0j3222bd94cbc49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0j3222bd94cbc49",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 2
    }, {}, {});
    CopyflxRoundDBlueOff0j3222bd94cbc49.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0j3222bd94cbc49.add();
    var CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "isModalContainer": false,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.add();
    flxSwitchOnTouchLogin.add(CopyflxRoundDBlueOff0j3222bd94cbc49, CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b);
    flxAlterAuthTouchID.add(lblAltText, flxSwitchOffTouchLogin, flxSwitchOnTouchLogin);
    flxBody.add(lblTouchBodytxt, flxAlterAuthTouchID);
    var flxtouchIdAndrd = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxtouchIdAndrd",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_gc27fc186a9341cc8fff454eedbc81ff,
        "skin": "sknTouch0",
        "top": "0dp",
        "width": "100%",
        "zIndex": 50
    }, {}, {});
    flxtouchIdAndrd.setDefaultUnit(kony.flex.DP);
    var flxAndrdTouchAlert = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxAndrdTouchAlert",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknAndrdTouchId0a",
        "top": "20%",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxAndrdTouchAlert.setDefaultUnit(kony.flex.DP);
    var flxTouchIdheader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxTouchIdheader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "5%",
        "width": "100%",
        "zIndex": 5
    }, {}, {});
    flxTouchIdheader.setDefaultUnit(kony.flex.DP);
    var lblFingerprint = new kony.ui.Label({
        "centerX": "49.45%",
        "id": "lblFingerprint",
        "isVisible": true,
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.common.touchText"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "11dp",
        "width": "90%",
        "zIndex": 9
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblFingerprint05b69f1c5203740 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopylblFingerprint05b69f1c5203740",
        "isVisible": false,
        "skin": "skn383838LatoRegular107KA",
        "text": "For Bank of Jordan",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTouchIdheader.add(lblFingerprint, CopylblFingerprint05b69f1c5203740);
    var flxTouchIdImage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "25%",
        "id": "flxTouchIdImage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "28%",
        "zIndex": 1
    }, {}, {});
    flxTouchIdImage.setDefaultUnit(kony.flex.DP);
    var Image096dc60e703ce4f = new kony.ui.Image2({
        "centerX": "50.00%",
        "centerY": "50%",
        "height": "80%",
        "id": "Image096dc60e703ce4f",
        "isVisible": true,
        "skin": "slImage",
        "src": "touch_id_icon.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTouchIdImage.add(Image096dc60e703ce4f);
    var coseTouchId = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "coseTouchId",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "90%",
        "zIndex": 10
    }, {}, {});
    coseTouchId.setDefaultUnit(kony.flex.DP);
    var imgCloseTouchId = new kony.ui.Image2({
        "height": "38dp",
        "id": "imgCloseTouchId",
        "isVisible": true,
        "onTouchStart": AS_Image_bb169652645c4ce1abfca9550bbad8ab,
        "right": "0%",
        "skin": "slImage",
        "src": "close.png",
        "top": "-8dp",
        "width": "37dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    coseTouchId.add(imgCloseTouchId);
    var lblTouchText = new kony.ui.Label({
        "bottom": "8%",
        "centerX": "50%",
        "id": "lblTouchText",
        "isVisible": true,
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.common.FingerPrintValtext"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAndrdTouchAlert.add(flxTouchIdheader, flxTouchIdImage, coseTouchId, lblTouchText);
    flxtouchIdAndrd.add(flxAndrdTouchAlert);
    frmUserSettingsTouchIdKA.add(flxHeader, flxBody, flxtouchIdAndrd);
};

function frmUserSettingsTouchIdKAGlobals() {
    frmUserSettingsTouchIdKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmUserSettingsTouchIdKA,
        "allowHorizontalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmUserSettingsTouchIdKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": function(eventobject) {
            AS_Form_ceb90b0b375e41e693fb17f3602dc616(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_67bc9dcdc8934bf8ba9a56e1fa902f49,
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};