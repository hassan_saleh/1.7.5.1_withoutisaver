function addWidgetsfrmManagePayeeKA() {
    frmManagePayeeKA.setDefaultUnit(kony.flex.DP);
    var mainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "100%",
        "id": "mainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "minHeight": "100%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContainer.setDefaultUnit(kony.flex.DP);
    var flxAndroidTittleBarKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxAndroidTittleBarKA",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAndroidTittleBarKA.setDefaultUnit(kony.flex.DP);
    var titleBarLabel = new kony.ui.Label({
        "centerX": "50%",
        "id": "titleBarLabel",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.registeredPayees"),
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var androidBack = new kony.ui.Button({
        "focusSkin": "sknandroidBackButtonFocus",
        "height": "50dp",
        "id": "androidBack",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_e2704891ac6742b099ba732c3f6483aa,
        "skin": "sknandroidBackButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopysearchButton0fbb5aea20ae346 = new kony.ui.Button({
        "focusSkin": "skntitleBarSearchButtonFocus",
        "height": "50dp",
        "id": "CopysearchButton0fbb5aea20ae346",
        "isVisible": false,
        "right": "5dp",
        "skin": "skntitleBarSearchButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxAndroidTittleBarKA.add(titleBarLabel, androidBack, CopysearchButton0fbb5aea20ae346);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "23%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxTop = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "34%",
        "id": "flxTop",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTop.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_cf7372c1662a4101aa72788b7dc046d5,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0%",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.MyBills"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxIcons = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxIcons",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "90%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxIcons.setDefaultUnit(kony.flex.DP);
    var btnAddBeneficiary = new kony.ui.Button({
        "focusSkin": "slIconFocus",
        "height": "100%",
        "id": "btnAddBeneficiary",
        "isVisible": true,
        "left": "50%",
        "onClick": AS_Button_bef34ac826ce4125bb13e070189a5a42,
        "skin": "slIcon",
        "text": "u",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnAddBeneficiaryClose = new kony.ui.Button({
        "focusSkin": "slIconFocus",
        "height": "100%",
        "id": "btnAddBeneficiaryClose",
        "isVisible": false,
        "left": "0dp",
        "onTouchEnd": AS_FlexContainer_cf7372c1662a4101aa72788b7dc046d5,
        "skin": "slIconSmall",
        "text": "O",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxIcons.add(btnAddBeneficiary, btnAddBeneficiaryClose);
    var lblTitle2 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblTitle2",
        "isVisible": false,
        "skin": "sknLblSmallWhite",
        "text": "Registered Bills",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTop.add(flxBack, lblTitle, flxIcons, lblTitle2);
    var flxTab = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "28%",
        "id": "flxTab",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTab.setDefaultUnit(kony.flex.DP);
    var flxContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "65%",
        "id": "flxContent",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFboxOuterRing",
        "top": "0dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxContent.setDefaultUnit(kony.flex.DP);
    var btnAll = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnAll",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_b133711ae9304d83ae8ea075daf7f745,
        "skin": "slButtonWhiteTab",
        "text": kony.i18n.getLocalizedString("i18n.Bene.All"),
        "top": "0dp",
        "width": "33%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPostPaid = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnPostPaid",
        "isVisible": true,
        "left": "34%",
        "onClick": AS_Button_i5fea5cae22042bc8d50a546431cbf03,
        "skin": "slButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.PostPaid"),
        "top": "0dp",
        "width": "33%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPrePaid = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnPrePaid",
        "isVisible": true,
        "left": "68%",
        "onClick": AS_Button_c8bcf6718f904b27a784ca5ca4c88589,
        "skin": "slButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.PrePaid"),
        "top": "0dp",
        "width": "34%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnInternational = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "98%",
        "id": "btnInternational",
        "isVisible": false,
        "onClick": AS_Button_je0b2af62a0b4f71970966792713a3c5,
        "skin": "slButtonWhiteTabDisabled",
        "text": "QuickPay",
        "width": "28%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxContent.add(btnAll, btnPostPaid, btnPrePaid, btnInternational);
    flxTab.add(flxContent);
    var flxSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "28%",
        "id": "flxSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSearch.setDefaultUnit(kony.flex.DP);
    var lblSearch = new kony.ui.Label({
        "height": "100%",
        "id": "lblSearch",
        "isVisible": true,
        "left": "0%",
        "skin": "sknSearchIcon",
        "text": "h",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxUnderLineSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderLineSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "92%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderLineSearch.setDefaultUnit(kony.flex.DP);
    flxUnderLineSearch.add();
    var tbxSearch = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "focusSkin": "sknTxtBoxSearch",
        "height": "100%",
        "id": "tbxSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "10%",
        "onDone": AS_TextField_ab5861a1c5e040d1b612c0f145a32aa7,
        "onTextChange": AS_TextField_gb94d096b4284d7fac57e02f0b33bb42,
        "placeholder": kony.i18n.getLocalizedString("i18n.transfer.searchPlh"),
        "secureTextEntry": false,
        "skin": "sknTxtBoxSearch",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "90%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknPlaceHolder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxSearch.add(lblSearch, flxUnderLineSearch, tbxSearch);
    flxHeader.add(flxTop, flxTab, flxSearch);
    var FlexScrollContainer0dacb1b5f2c154d = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": true,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "68%",
        "horizontalScrollIndicator": true,
        "id": "FlexScrollContainer0dacb1b5f2c154d",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
        "skin": "slFSbox0dba49a84680543",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexScrollContainer0dacb1b5f2c154d.setDefaultUnit(kony.flex.DP);
    var Button06a2d7f3f118d47 = new kony.ui.Button({
        "bottom": "0dp",
        "centerX": "50%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "50dp",
        "id": "Button06a2d7f3f118d47",
        "isVisible": false,
        "left": "0dp",
        "onClick": AS_Button_1fe121a20c1d4d988b2b27103035291f,
        "skin": "sknSecondaryActionWhiteBgKA",
        "text": kony.i18n.getLocalizedString("i18n.common.addNewPayee"),
        "width": "100.00%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var LabelNoRecordsKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "35.519999999999996%",
        "id": "LabelNoRecordsKA",
        "isVisible": false,
        "left": "17.52%",
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.alerts.NoRegisteredPayees"),
        "top": "30dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexScrollContainer0dacb1b5f2c154d.add(Button06a2d7f3f118d47, LabelNoRecordsKA);
    var lblRequestLimitHint = new kony.ui.Label({
        "id": "lblRequestLimitHint",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.bulkpayment.hint"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var managepayeesegment = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "accountnumber": "",
            "btnBillsPayAccounts": "0",
            "btnDelete": "",
            "btnEditBillerDetails": "",
            "dueAmount": "Account Number",
            "lblBillerType": "",
            "lblBulkSelection": "Label",
            "lblInitial": "",
            "lblTick": "r",
            "payeename": "",
            "payeenickname": ""
        }],
        "groupCells": false,
        "height": "61.50%",
        "id": "managepayeesegment",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "sknSegDblue",
        "rowTemplate": flxManagePayeeNormal,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0.00%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "accountnumber": "accountnumber",
            "btnBillsPayAccounts": "btnBillsPayAccounts",
            "btnDelete": "btnDelete",
            "btnEditBillerDetails": "btnEditBillerDetails",
            "contactListDivider": "contactListDivider",
            "dueAmount": "dueAmount",
            "flxAnimate": "flxAnimate",
            "flxButtonHolder": "flxButtonHolder",
            "flxDetails": "flxDetails",
            "flxIcon1": "flxIcon1",
            "flxIconContainer": "flxIconContainer",
            "flxManagePayeeNormal": "flxManagePayeeNormal",
            "flxToAnimate": "flxToAnimate",
            "lblBillerType": "lblBillerType",
            "lblBulkSelection": "lblBulkSelection",
            "lblInitial": "lblInitial",
            "lblTick": "lblTick",
            "payeename": "payeename",
            "payeenickname": "payeenickname",
            "verticalDivider": "verticalDivider"
        },
        "width": "100%",
        "zIndex": 2
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSearchResult = new kony.ui.Label({
        "centerX": "50%",
        "height": "61.50%",
        "id": "lblSearchResult",
        "isVisible": false,
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.payee.noBillFound"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var FlexContainer0f1418ee9e2994a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "7%",
        "id": "FlexContainer0f1418ee9e2994a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "CopyflxsegSknblue0ac04f5ba015b48",
        "top": "0%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    FlexContainer0f1418ee9e2994a.setDefaultUnit(kony.flex.DP);
    var btnBulkPay = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "100%",
        "id": "btnBulkPay",
        "isVisible": false,
        "onClick": AS_Button_i07262e1aa3d42789040e40654c4e108,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.bulkpayment.title"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPayNow = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "100%",
        "id": "btnPayNow",
        "isVisible": true,
        "onClick": AS_Button_d4425b3e3ba642e3bb88c950c5cab78f,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.PayNow"),
        "width": "70%",
        "zIndex": 12
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer0f1418ee9e2994a.add(btnBulkPay, btnPayNow);
    mainContainer.add(flxAndroidTittleBarKA, flxHeader, FlexScrollContainer0dacb1b5f2c154d, lblRequestLimitHint, managepayeesegment, lblSearchResult, FlexContainer0f1418ee9e2994a);
    frmManagePayeeKA.add(mainContainer);
};

function frmManagePayeeKAGlobals() {
    frmManagePayeeKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmManagePayeeKA,
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "enableScrolling": true,
        "enabledForIdleTimeout": true,
        "id": "frmManagePayeeKA",
        "init": AS_Form_fae9ecbd34f0457dac6853b42d3c2228,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "preShow": function(eventobject) {
            AS_Form_b7beb66108cf4da4a9ffb2fa88d3e21c(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 3
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_j5d716ac3cd34d04939b2c9c89ae9f0f,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": false,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};