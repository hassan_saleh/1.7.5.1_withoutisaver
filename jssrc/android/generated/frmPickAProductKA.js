function addWidgetsfrmPickAProductKA() {
    frmPickAProductKA.setDefaultUnit(kony.flex.DP);
    var overview = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "overview",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    overview.setDefaultUnit(kony.flex.DP);
    var titleBarAccountInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "85dp",
        "id": "titleBarAccountInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarAccountInfo.setDefaultUnit(kony.flex.DP);
    var flxAndroidTittleBarKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "53dp",
        "id": "flxAndroidTittleBarKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAndroidTittleBarKA.setDefaultUnit(kony.flex.DP);
    var titleBarLabel = new kony.ui.Label({
        "centerX": "50%",
        "id": "titleBarLabel",
        "isVisible": true,
        "skin": "skniconButtonLabel",
        "text": kony.i18n.getLocalizedString("i18n.common.openinganAccount"),
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var androidBack = new kony.ui.Button({
        "focusSkin": "sknandroidBackButtonFocus",
        "height": "50dp",
        "id": "androidBack",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_a83c5dae30aa4e1faf4c9683fd86ab7a,
        "skin": "sknandroidBackButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxAndroidTittleBarKA.add(titleBarLabel, androidBack);
    var lblPickAProductKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPickAProductKA",
        "isVisible": true,
        "skin": "skniconButtonLabel",
        "text": kony.i18n.getLocalizedString("i18n.more.pickAProduct"),
        "top": "53dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxTransitionKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50.00%",
        "clipBounds": true,
        "height": "20dp",
        "id": "flxTransitionKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "35dp",
        "width": "200dp",
        "zIndex": 1
    }, {}, {});
    flxTransitionKA.setDefaultUnit(kony.flex.DP);
    var flx1KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "5dp",
        "id": "flx1KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": 0,
        "width": "20dp"
    }, {}, {});
    flx1KA.setDefaultUnit(kony.flex.DP);
    flx1KA.add();
    var flx2KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "5dp",
        "id": "flx2KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5dp",
        "isModalContainer": false,
        "skin": "sknNewAccount78a0c8KA",
        "top": "0dp",
        "width": "20dp"
    }, {}, {});
    flx2KA.setDefaultUnit(kony.flex.DP);
    flx2KA.add();
    var flx3KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "49.44%",
        "clipBounds": true,
        "height": "5dp",
        "id": "flx3KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5dp",
        "isModalContainer": false,
        "skin": "sknNewAccount78a0c8KA",
        "top": 0,
        "width": "20dp"
    }, {}, {});
    flx3KA.setDefaultUnit(kony.flex.DP);
    flx3KA.add();
    var flx4KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "5dp",
        "id": "flx4KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5dp",
        "isModalContainer": false,
        "skin": "sknNewAccount78a0c8KA",
        "top": 0,
        "width": "20dp"
    }, {}, {});
    flx4KA.setDefaultUnit(kony.flex.DP);
    flx4KA.add();
    var flx5KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "5dp",
        "id": "flx5KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5dp",
        "isModalContainer": false,
        "skin": "sknNewAccount78a0c8KA",
        "top": 0,
        "width": "20dp"
    }, {}, {});
    flx5KA.setDefaultUnit(kony.flex.DP);
    flx5KA.add();
    var flx6KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "5dp",
        "id": "flx6KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5dp",
        "isModalContainer": false,
        "skin": "sknNewAccount78a0c8KA",
        "top": 0,
        "width": "20dp"
    }, {}, {});
    flx6KA.setDefaultUnit(kony.flex.DP);
    flx6KA.add();
    var flx7KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "5dp",
        "id": "flx7KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5dp",
        "isModalContainer": false,
        "skin": "sknNewAccount78a0c8KA",
        "top": 0,
        "width": "20dp"
    }, {}, {});
    flx7KA.setDefaultUnit(kony.flex.DP);
    flx7KA.add();
    var flx8KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "5dp",
        "id": "flx8KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5dp",
        "isModalContainer": false,
        "skin": "sknNewAccount78a0c8KA",
        "top": 0,
        "width": "20dp"
    }, {}, {});
    flx8KA.setDefaultUnit(kony.flex.DP);
    flx8KA.add();
    flxTransitionKA.add(flx1KA, flx2KA, flx3KA, flx4KA, flx5KA, flx6KA, flx7KA, flx8KA);
    titleBarAccountInfo.add(flxAndroidTittleBarKA, lblPickAProductKA, flxTransitionKA);
    var accountsOuterScroll = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "accountsOuterScroll",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%"
    }, {}, {});
    accountsOuterScroll.setDefaultUnit(kony.flex.DP);
    var accountsInnerScroll = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "accountsInnerScroll",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknCopyslFSbox013d08d6060bd4b",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%"
    }, {}, {});
    accountsInnerScroll.setDefaultUnit(kony.flex.DP);
    var segAccountKA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "data": [{
            "lblColorKA": "",
            "nameAccount1": "Account Name",
            "typeKA": ""
        }, {
            "lblColorKA": "",
            "nameAccount1": "Account Name",
            "typeKA": ""
        }, {
            "lblColorKA": "",
            "nameAccount1": "Account Name",
            "typeKA": ""
        }],
        "groupCells": false,
        "height": "80%",
        "id": "segAccountKA",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_5af57cfacfdb4f8abbcf26b244058260,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": CopyyourAccount01dfa1aa140424b,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
        "separatorColor": "f7f7f700",
        "separatorRequired": true,
        "separatorThickness": 5,
        "showScrollbars": true,
        "top": "5%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopyyourAccount01dfa1aa140424b": "CopyyourAccount01dfa1aa140424b",
            "lblColorKA": "lblColorKA",
            "nameAccount1": "nameAccount1",
            "nameContainer": "nameContainer",
            "typeKA": "typeKA"
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var yourAccount4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknyourAccountCardFocus",
        "height": "62dp",
        "id": "yourAccount4",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d4a4251cabed44dc8bf9dc631ebfb50d,
        "skin": "sknyourAccountCard",
        "top": "10dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    yourAccount4.setDefaultUnit(kony.flex.DP);
    var colorAccount4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "100%",
        "id": "colorAccount4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknaccounttypeDeposit",
        "top": "0dp",
        "width": "6dp"
    }, {}, {});
    colorAccount4.setDefaultUnit(kony.flex.DP);
    colorAccount4.add();
    var CopynameContainer09cd63430de2f4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "CopynameContainer09cd63430de2f4b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    CopynameContainer09cd63430de2f4b.setDefaultUnit(kony.flex.DP);
    var nameAccount4 = new kony.ui.Label({
        "centerY": "50%",
        "id": "nameAccount4",
        "isVisible": true,
        "left": "15dp",
        "maxWidth": "90%",
        "skin": "sknaccountName",
        "text": kony.i18n.getLocalizedString("i18n.common.deposits"),
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopynameContainer09cd63430de2f4b.add(nameAccount4);
    yourAccount4.add(colorAccount4, CopynameContainer09cd63430de2f4b);
    accountsInnerScroll.add(segAccountKA, yourAccount4);
    accountsOuterScroll.add(accountsInnerScroll);
    overview.add(titleBarAccountInfo, accountsOuterScroll);
    frmPickAProductKA.add(overview);
};

function frmPickAProductKAGlobals() {
    frmPickAProductKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPickAProductKA,
        "bounces": true,
        "enableScrolling": true,
        "enabledForIdleTimeout": true,
        "id": "frmPickAProductKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_5d135a509b174585858421b7f7807c0c,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};