function initializetmpJomopaycontacts() {
    flxJomoPayContacts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxJomoPayContacts",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxJomoPayContacts.setDefaultUnit(kony.flex.DP);
    var flxProfilePic = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxProfilePic",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxProfilePic.setDefaultUnit(kony.flex.DP);
    var btnPic = new kony.ui.Button({
        "height": "60%",
        "id": "btnPic",
        "isVisible": true,
        "left": "20%",
        "skin": "CopyslButtonGlossBlue0gc9fc8149f6943",
        "top": "20%",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxProfilePic.add(btnPic);
    var flxUserDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxUserDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "20%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flxUserDetails.setDefaultUnit(kony.flex.DP);
    var lblPhoneNum = new kony.ui.Label({
        "id": "lblPhoneNum",
        "isVisible": true,
        "left": "5%",
        "skin": "sknSIDate",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblName = new kony.ui.Label({
        "id": "lblName",
        "isVisible": true,
        "left": "5%",
        "skin": "CopyslLabel0e2302468ce1241",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxUserDetails.add(lblPhoneNum, lblName);
    flxJomoPayContacts.add(flxProfilePic, flxUserDetails);
}