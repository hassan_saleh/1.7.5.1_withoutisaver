function initializetmpAccountSMSDisabled() {
    flxSMSNotificationDisabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxSMSNotificationDisabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSMSNotificationDisabled.setDefaultUnit(kony.flex.DP);
    var flxSMSSwitchDIsabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSMSSwitchDIsabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "5%",
        "skin": "sknflxWhiteOpacity60",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSMSSwitchDIsabled.setDefaultUnit(kony.flex.DP);
    var flxInnerDisabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxInnerDisabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    flxInnerDisabled.setDefaultUnit(kony.flex.DP);
    flxInnerDisabled.add();
    var flxILineDisabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "14dp",
        "id": "flxILineDisabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 10,
        "skin": "sknLineDarkBlueOpacity60",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    flxILineDisabled.setDefaultUnit(kony.flex.DP);
    flxILineDisabled.add();
    flxSMSSwitchDIsabled.add(flxInnerDisabled, flxILineDisabled);
    var flxSMSSwitchEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSMSSwitchEnabled",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSMSSwitchEnabled.setDefaultUnit(kony.flex.DP);
    var flxInnerEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxInnerEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    flxInnerEnabled.setDefaultUnit(kony.flex.DP);
    flxInnerEnabled.add();
    var flxLineEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "14dp",
        "id": "flxLineEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "isModalContainer": false,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    flxLineEnabled.setDefaultUnit(kony.flex.DP);
    flxLineEnabled.add();
    flxSMSSwitchEnabled.add(flxInnerEnabled, flxLineEnabled);
    var flxUnderLineSMS = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderLineSMS",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "flxUnderLine",
        "top": "98%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderLineSMS.setDefaultUnit(kony.flex.DP);
    flxUnderLineSMS.add();
    var flcContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flcContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "75%",
        "zIndex": 1
    }, {}, {});
    flcContainer.setDefaultUnit(kony.flex.DP);
    var lblAccountType = new kony.ui.Label({
        "id": "lblAccountType",
        "isVisible": true,
        "left": "0%",
        "maxNumberOfLines": 1,
        "skin": "lblAccountType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "23%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "0%",
        "maxNumberOfLines": 1,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flcContainer.add(lblAccountType, lblAccountNumber);
    flxSMSNotificationDisabled.add(flxSMSSwitchDIsabled, flxSMSSwitchEnabled, flxUnderLineSMS, flcContainer);
}