function addWidgetsForm1() {
    Form1.setDefaultUnit(kony.flex.DP);
    var flxFooter = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0%",
        "clipBounds": false,
        "height": "7%",
        "id": "flxFooter",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0",
        "isModalContainer": true,
        "skin": "menu",
        "width": "100%"
    }, {}, {});
    flxFooter.setDefaultUnit(kony.flex.DP);
    var FlxMore = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxMore",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "80%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d9cd3d0001794a299fd75546f521374a,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": false
    }, {});
    FlxMore.setDefaultUnit(kony.flex.DP);
    var img4 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img4",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_more_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label0e5331028c2ef41 = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label0e5331028c2ef41",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": kony.i18n.getLocalizedString("i18n.common.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnMore = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "btnMore",
        "isVisible": true,
        "onClick": AS_Button_de75f6e6596d4395af5ceb969e265c92,
        "skin": "btnCard",
        "text": "K",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 12],
        "paddingInPixel": false
    }, {});
    FlxMore.add(img4, Label0e5331028c2ef41, btnMore);
    var flxEfawatercoom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxEfawatercoom",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "60%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "23%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": false
    }, {});
    flxEfawatercoom.setDefaultUnit(kony.flex.DP);
    var btnEfawatercoom = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "btnEfawatercoom",
        "isVisible": true,
        "onClick": AS_Button_f187734a44b84a2992ba932b0c2d49a9,
        "skin": "btnCard",
        "text": "x",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 12],
        "paddingInPixel": false
    }, {});
    var lblEfawatercoom = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblEfawatercoom",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": kony.i18n.getLocalizedString("i18n.footerBill.BillPayment"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEfawatercoom.add(btnEfawatercoom, lblEfawatercoom);
    var FlxDeposits = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxDeposits",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_e913d2788de941cea1ac8ab857eb693e,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": false
    }, {});
    FlxDeposits.setDefaultUnit(kony.flex.DP);
    var img3 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img3",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_deposits_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label04221a71494e848 = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label04221a71494e848",
        "isVisible": false,
        "skin": "sknlblmenu",
        "text": kony.i18n.getLocalizedString("i18n.common.deposits"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "34dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnDeposit = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "btnDeposit",
        "isVisible": true,
        "skin": "btnCard",
        "text": "J",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlxDeposits.add(img3, Label04221a71494e848, btnDeposit);
    var FlxTranfers = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxTranfers",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "40%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_c2636b5bc493465aadd7377351f1c7fc,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": false
    }, {});
    FlxTranfers.setDefaultUnit(kony.flex.DP);
    var img2 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img2",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_t_and_p_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label02bec01fd5baf4c = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label02bec01fd5baf4c",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopybtnAccounts0ia3b1ff37c304b = new kony.ui.Button({
        "accessibilityConfig": {
            "a11yValue": "Cards"
        },
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnFooterDisablefocusskin",
        "height": "50dp",
        "id": "CopybtnAccounts0ia3b1ff37c304b",
        "isVisible": true,
        "onClick": AS_Button_cb688e1c2a504258ad767548c51420b0,
        "skin": "btnFooterDisable",
        "text": "G",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false
    }, {});
    FlxTranfers.add(img2, Label02bec01fd5baf4c, CopybtnAccounts0ia3b1ff37c304b);
    var FlxBot = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxBot",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "20%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_g349709f83e1428d92f8e4acf66d8dd9,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 2
    }, {
        "retainFlexPositionProperties": false
    }, {});
    FlxBot.setDefaultUnit(kony.flex.DP);
    var CopyimgBot0ic2beb5ebfa643 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "40dp",
        "id": "CopyimgBot0ic2beb5ebfa643",
        "isVisible": false,
        "left": "13dp",
        "skin": "slImage",
        "src": "chaticonactive.png",
        "top": "4dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopybtnAccounts0ff48f9feb0aa42 = new kony.ui.Button({
        "accessibilityConfig": {
            "a11yValue": "Transfer"
        },
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "CopybtnAccounts0ff48f9feb0aa42",
        "isVisible": true,
        "onClick": AS_Button_dd667975aefc42498d5e0870034a015e,
        "skin": "btnCard",
        "text": "i",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false
    }, {});
    var CopyLabel0f2642bec08c44e = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0f2642bec08c44e",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": kony.i18n.getLocalizedString("i18n.alert.transfers"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlxBot.add(CopyimgBot0ic2beb5ebfa643, CopybtnAccounts0ff48f9feb0aa42, CopyLabel0f2642bec08c44e);
    var FlxAccounts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxAccounts",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_dc7c3fc616f14b6cb36cfcfa045f3d70,
        "skin": "sknfocusmenu",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": false
    }, {});
    FlxAccounts.setDefaultUnit(kony.flex.DP);
    var img1 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img1",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_accounts_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {});
    var Label03174bff69bb54c = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label03174bff69bb54c",
        "isVisible": true,
        "skin": "sknlblFootertitleFocus",
        "text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false
    });
    var CopybtnAccounts0ca6db8b656254d = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "CopybtnAccounts0ca6db8b656254d",
        "isVisible": true,
        "onClick": AS_Button_a2678571862d4ae59deb6131cb1e5ae3,
        "skin": "btnFooterDisableaccount",
        "text": "H",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {});
    FlxAccounts.add(img1, Label03174bff69bb54c, CopybtnAccounts0ca6db8b656254d);
    flxFooter.add(FlxMore, flxEfawatercoom, FlxDeposits, FlxTranfers, FlxBot, FlxAccounts);
    Form1.add(flxFooter);
};

function Form1Globals() {
    Form1 = new kony.ui.Form2({
        "addWidgets": addWidgetsForm1,
        "enabledForIdleTimeout": false,
        "id": "Form1",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "slForm"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};