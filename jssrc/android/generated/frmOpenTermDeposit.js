function addWidgetsfrmOpenTermDeposit() {
    frmOpenTermDeposit.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2.00%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_b1c6acb2850a480b9f8c627242ee334d,
        "skin": "slFbox",
        "top": "-0.20%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblOpenTermDepositTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "90%",
        "id": "lblOpenTermDepositTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.openTermDeposit"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnNextDeposite = new kony.ui.Button({
        "centerY": "50%",
        "height": "50dp",
        "id": "btnNextDeposite",
        "isVisible": true,
        "left": "88%",
        "onClick": AS_Button_g473dc801a854ad7a77ca7255ad954aa,
        "skin": "jomopaynextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "top": "8dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeader.add(flxBack, lblOpenTermDepositTitle, btnNextDeposite);
    var flxDepositSelection1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "85%",
        "id": "flxDepositSelection1",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDepositSelection1.setDefaultUnit(kony.flex.DP);
    flxDepositSelection1.add();
    var flxDepositSelection = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "85%",
        "horizontalScrollIndicator": true,
        "id": "flxDepositSelection",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "10%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDepositSelection.setDefaultUnit(kony.flex.DP);
    var flxFundDeductionAccountNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxFundDeductionAccountNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxFundDeductionAccountNumber.setDefaultUnit(kony.flex.DP);
    var lblFundDeductionAccountNumberTitle = new kony.ui.Label({
        "id": "lblFundDeductionAccountNumberTitle",
        "isVisible": false,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.fundDeductionAccountNumberTitle"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLine1 = new kony.ui.Label({
        "height": "1.50%",
        "id": "lblLine1",
        "isVisible": false,
        "left": "0dp",
        "skin": "lblsknToandFromAccLine",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "32%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFundDeductionAccountNumber = new kony.ui.Label({
        "height": "50%",
        "id": "lblFundDeductionAccountNumber",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.fundDeductionAccountNumberTitle"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderFundDeductionAccountNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderFundDeductionAccountNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderFundDeductionAccountNumber.setDefaultUnit(kony.flex.DP);
    flxBorderFundDeductionAccountNumber.add();
    var btnFuncDeductionAccountNumber = new kony.ui.Button({
        "height": "50%",
        "id": "btnFuncDeductionAccountNumber",
        "isVisible": true,
        "left": "-0.03%",
        "onClick": AS_Button_c8ef89c77903423e8ae639c5c7e4c1f8,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": kony.i18n.getLocalizedString("i18n.common.reverseback"),
        "top": "35.00%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLine2 = new kony.ui.Label({
        "bottom": "8%",
        "height": "1.50%",
        "id": "lblLine2",
        "isVisible": false,
        "left": "0dp",
        "skin": "lblsknToandFromAccLine",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxFundDeductionAccountNumber.add(lblFundDeductionAccountNumberTitle, lblLine1, lblFundDeductionAccountNumber, flxBorderFundDeductionAccountNumber, btnFuncDeductionAccountNumber, lblLine2);
    var flxDepositAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "13%",
        "id": "flxDepositAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "5%",
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDepositAmount.setDefaultUnit(kony.flex.PERCENTAGE);
    var lblDepositAmountTitle = new kony.ui.Label({
        "id": "lblDepositAmountTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.depositAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderDepositAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderDepositAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderDepositAmount.setDefaultUnit(kony.flex.DP);
    flxBorderDepositAmount.add();
    var txtDepositAmount = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "50%",
        "id": "txtDepositAmount",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "left": "0%",
        "maxTextLength": null,
        "onDone": AS_TextField_e80f545b8ff04ac480dfd005e443789b,
        "onTextChange": AS_TextField_fdf09c51ffe14e02bdded06b88a52786,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "onBeginEditing": AS_TextField_d89b9bc0ea7842c78f82648c3880e596,
        "onEndEditing": AS_TextField_ja3cb9115d214a4884d23d341c920840,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblDepositCurrency = new kony.ui.Label({
        "id": "lblDepositCurrency",
        "isVisible": false,
        "left": "90%",
        "skin": "sknLblNextDisabled",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurrencyCode = new kony.ui.Label({
        "id": "lblCurrencyCode",
        "isVisible": false,
        "left": "279dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDepositAmount.add(lblDepositAmountTitle, flxBorderDepositAmount, txtDepositAmount, lblDepositCurrency, lblCurrencyCode);
    var lblMinJOD = new kony.ui.Label({
        "id": "lblMinJOD",
        "isVisible": false,
        "left": "40%",
        "skin": "CopysknInvalidCredKA0g10f8d36a5e842",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMinUSD = new kony.ui.Label({
        "id": "lblMinUSD",
        "isVisible": false,
        "left": "40%",
        "skin": "CopysknInvalidCredKA0g10f8d36a5e842",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxTener = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxTener",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTener.setDefaultUnit(kony.flex.DP);
    var flxTenerLable = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxTenerLable",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTenerLable.setDefaultUnit(kony.flex.DP);
    var lblTener = new kony.ui.Label({
        "id": "lblTener",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.tener"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTenerLable.add(lblTener);
    var flxTenerPeriods = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxTenerPeriods",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "5%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTenerPeriods.setDefaultUnit(kony.flex.DP);
    var btnTener1Month = new kony.ui.Button({
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "70%",
        "id": "btnTener1Month",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_c85c3842c44a4303a37fd2d814bd447e,
        "skin": "slButtonBlueFocus",
        "text": kony.i18n.getLocalizedString("i18n.termDepodit.tener1Month"),
        "top": "10%",
        "width": "24%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnTener3Months = new kony.ui.Button({
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "70%",
        "id": "btnTener3Months",
        "isVisible": true,
        "left": "1%",
        "onClick": AS_Button_a35432bbf8044b5b965345996b67b1f4,
        "skin": "slButtonBlueFocus",
        "text": kony.i18n.getLocalizedString("i18n.termDepodit.tener3Month"),
        "top": "10%",
        "width": "24%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnTener6Months = new kony.ui.Button({
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "70%",
        "id": "btnTener6Months",
        "isVisible": true,
        "left": "1%",
        "onClick": AS_Button_bb6320ffbdbf4cbebbacf54edd90ad43,
        "skin": "slButtonBlueFocus",
        "text": kony.i18n.getLocalizedString("i18n.termDepodit.tener6Month"),
        "top": "10%",
        "width": "24%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnTener1Year = new kony.ui.Button({
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "70%",
        "id": "btnTener1Year",
        "isVisible": true,
        "left": "1%",
        "onClick": AS_Button_d0f2404034a446e9a70ff9975fbecbe9,
        "skin": "slButtonBlueFocus",
        "text": kony.i18n.getLocalizedString("i18n.termDepodit.tener1Year"),
        "top": "10%",
        "width": "24%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTenerPeriods.add(btnTener1Month, btnTener3Months, btnTener6Months, btnTener1Year);
    flxTener.add(flxTenerLable, flxTenerPeriods);
    var flxDepositStartDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxDepositStartDate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDepositStartDate.setDefaultUnit(kony.flex.DP);
    var lblDepositStartDateTitle = new kony.ui.Label({
        "id": "lblDepositStartDateTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.effectivedate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDepositStartDate = new kony.ui.Label({
        "id": "lblDepositStartDate",
        "isVisible": true,
        "left": "0%",
        "skin": "lblAmountCurrency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "45%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Image0e7683ea123f546 = new kony.ui.Image2({
        "height": "60%",
        "id": "Image0e7683ea123f546",
        "isVisible": true,
        "right": "0%",
        "skin": "sknslImage",
        "src": "cal.png",
        "top": "25%",
        "width": "8%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var calOpenDeposite = new kony.ui.Calendar({
        "calendarIcon": "tran.png",
        "dateFormat": "dd/MM/yyyy",
        "height": "50%",
        "id": "calOpenDeposite",
        "isVisible": true,
        "left": "-0.32%",
        "onSelection": AS_Calendar_adef66f9976745a4afed6cf3c8daffd4,
        "onTouchStart": AS_Calendar_b875b06ed8034ec7bf6f78f1bb545817,
        "skin": "sknCarioRegular",
        "top": "35%",
        "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
        "width": "100%",
        "zIndex": 1000
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var FlexContainer0a01d5f3c26114a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "FlexContainer0a01d5f3c26114a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0a01d5f3c26114a.setDefaultUnit(kony.flex.DP);
    FlexContainer0a01d5f3c26114a.add();
    var lblMaturityDate = new kony.ui.Label({
        "id": "lblMaturityDate",
        "isVisible": false,
        "left": "67dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "33dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDepositStartDate.add(lblDepositStartDateTitle, lblDepositStartDate, Image0e7683ea123f546, calOpenDeposite, FlexContainer0a01d5f3c26114a, lblMaturityDate);
    var flxInterestRate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxInterestRate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxInterestRate.setDefaultUnit(kony.flex.DP);
    var lblInterest = new kony.ui.Label({
        "id": "lblInterest",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.overview.interestRate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxInterestRateDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxInterestRateDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "70%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxInterestRateDivider.setDefaultUnit(kony.flex.DP);
    flxInterestRateDivider.add();
    var flxInterestRateInner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxInterestRateInner",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flxInterestRateInner.setDefaultUnit(kony.flex.DP);
    var lblInterestRate = new kony.ui.Label({
        "id": "lblInterestRate",
        "isVisible": true,
        "left": 0,
        "skin": "lblAmountCurrency",
        "text": "0.000",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Label0ge5e4d40af9741 = new kony.ui.Label({
        "id": "Label0ge5e4d40af9741",
        "isVisible": true,
        "right": "1%",
        "skin": "lblAmountCurrency",
        "text": "%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxInterestRateInner.add(lblInterestRate, Label0ge5e4d40af9741);
    flxInterestRate.add(lblInterest, flxInterestRateDivider, flxInterestRateInner);
    var lblInterestAmount = new kony.ui.Label({
        "id": "lblInterestAmount",
        "isVisible": false,
        "right": "5%",
        "skin": "latoRegular24px",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxMaturityInstructions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxMaturityInstructions",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxMaturityInstructions.setDefaultUnit(kony.flex.DP);
    var btnMaturityInstruction = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "50%",
        "id": "btnMaturityInstruction",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_fb69fe5b965842bbb251aaf6cd9ffc5f,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": kony.i18n.getLocalizedString("i18n.common.reverseback"),
        "top": "45%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblMaturityInstructionTitle = new kony.ui.Label({
        "id": "lblMaturityInstructionTitle",
        "isVisible": false,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.renewalInstruction"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMaturityInstruction = new kony.ui.Label({
        "height": "30%",
        "id": "lblMaturityInstruction",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.renewalInstruction"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "45%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var FlexContainer0f3912fbe770845 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "FlexContainer0f3912fbe770845",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "95%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0f3912fbe770845.setDefaultUnit(kony.flex.DP);
    FlexContainer0f3912fbe770845.add();
    var btnWithInterest = new kony.ui.Button({
        "height": "60%",
        "id": "btnWithInterest",
        "isVisible": false,
        "left": "0%",
        "onClick": AS_Button_fb1b8e8493ec4de5aac7edabdb529ec3,
        "skin": "CopysknBtnBGBlueWhite0dc3d4cc762b747",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.renWithInterest"),
        "top": "5%",
        "width": "31%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnWithoutInterest = new kony.ui.Button({
        "height": "60%",
        "id": "btnWithoutInterest",
        "isVisible": false,
        "left": "1%",
        "onClick": AS_Button_b7a31fd2cd5f460795fa7a841d371bc0,
        "skin": "CopysknBtnBGBlueWhite0dc3d4cc762b747",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.renWithOutInterest"),
        "top": "5%",
        "width": "36%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnClose = new kony.ui.Button({
        "height": "60%",
        "id": "btnClose",
        "isVisible": false,
        "left": "1%",
        "onClick": AS_Button_ac6c62ba68c348f5a53e455a387e6f80,
        "skin": "CopysknBtnBGBlueWhite0dc3d4cc762b747",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.close"),
        "top": "5%",
        "width": "31%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMaturityInstructions.add(btnMaturityInstruction, lblMaturityInstructionTitle, lblMaturityInstruction, FlexContainer0f3912fbe770845, btnWithInterest, btnWithoutInterest, btnClose);
    var flxInterestPayable = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxInterestPayable",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxInterestPayable.setDefaultUnit(kony.flex.DP);
    var lblInerestPayable = new kony.ui.Label({
        "height": "30%",
        "id": "lblInerestPayable",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.interestPayable"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxInterestPayableSelects = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxInterestPayableSelects",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "8%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxInterestPayableSelects.setDefaultUnit(kony.flex.DP);
    var btnMonthly = new kony.ui.Button({
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "70%",
        "id": "btnMonthly",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_jd656f7ef24e453ca2bc2bb4a7f9e60c,
        "skin": "slButtonBlueFocus",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.intMonthly"),
        "top": "5%",
        "width": "49%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnOnMaturity = new kony.ui.Button({
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "70%",
        "id": "btnOnMaturity",
        "isVisible": true,
        "left": "1%",
        "onClick": AS_Button_db628fb56f274f69b10607565c2296a9,
        "skin": "slButtonBlueFocus",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.inOnMaturity"),
        "top": "5%",
        "width": "49%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxInterestPayableSelects.add(btnMonthly, btnOnMaturity);
    flxInterestPayable.add(lblInerestPayable, flxInterestPayableSelects);
    var flxNumberOfDepositPeriod = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxNumberOfDepositPeriod",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNumberOfDepositPeriod.setDefaultUnit(kony.flex.DP);
    var lblNumberOfDepositPeriodTitle = new kony.ui.Label({
        "id": "lblNumberOfDepositPeriodTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderNumberOfDepositPeriod = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderNumberOfDepositPeriod",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderNumberOfDepositPeriod.setDefaultUnit(kony.flex.DP);
    flxBorderNumberOfDepositPeriod.add();
    var txtNumberOfDepositPeriod = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "50%",
        "id": "txtNumberOfDepositPeriod",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxNumberOfDepositPeriod.add(lblNumberOfDepositPeriodTitle, flxBorderNumberOfDepositPeriod, txtNumberOfDepositPeriod);
    var flxDepositPeriod = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxDepositPeriod",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDepositPeriod.setDefaultUnit(kony.flex.DP);
    var lblDepositPeriodTitle = new kony.ui.Label({
        "id": "lblDepositPeriodTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDepositPeriod = new kony.ui.Label({
        "height": "50%",
        "id": "lblDepositPeriod",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderDepositPeriod = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderDepositPeriod",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderDepositPeriod.setDefaultUnit(kony.flex.DP);
    flxBorderDepositPeriod.add();
    var btnDepositPeriod = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "50%",
        "id": "btnDepositPeriod",
        "isVisible": true,
        "left": "0%",
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxDepositPeriod.add(lblDepositPeriodTitle, lblDepositPeriod, flxBorderDepositPeriod, btnDepositPeriod);
    var flxInterestToAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxInterestToAccount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxInterestToAccount.setDefaultUnit(kony.flex.DP);
    var lblIntrestToAccountTitle = new kony.ui.Label({
        "id": "lblIntrestToAccountTitle",
        "isVisible": false,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIntrestToAccount = new kony.ui.Label({
        "id": "lblIntrestToAccount",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnAccountToAddInterestTo = new kony.ui.Button({
        "height": "50%",
        "id": "btnAccountToAddInterestTo",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_da1c826cb6c149bfa8267cadf2beb3d9,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": kony.i18n.getLocalizedString("i18n.common.reverseback"),
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var uLineAccountToAddInterestTo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "uLineAccountToAddInterestTo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    uLineAccountToAddInterestTo.setDefaultUnit(kony.flex.DP);
    uLineAccountToAddInterestTo.add();
    flxInterestToAccount.add(lblIntrestToAccountTitle, lblIntrestToAccount, btnAccountToAddInterestTo, uLineAccountToAddInterestTo);
    flxDepositSelection.add(flxFundDeductionAccountNumber, flxDepositAmount, lblMinJOD, lblMinUSD, flxTener, flxDepositStartDate, flxInterestRate, lblInterestAmount, flxMaturityInstructions, flxInterestPayable, flxNumberOfDepositPeriod, flxDepositPeriod, flxInterestToAccount);
    var flxConfirmDeposite = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0%",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "90%",
        "horizontalScrollIndicator": true,
        "id": "flxConfirmDeposite",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "10%",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmDeposite.setDefaultUnit(kony.flex.DP);
    var flxConfirmAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmAccount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxConfirmAccount.setDefaultUnit(kony.flex.DP);
    var lblConfirmAccountTitle = new kony.ui.Label({
        "id": "lblConfirmAccountTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.settings.account"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmAccount = new kony.ui.Label({
        "id": "lblConfirmAccount",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmAccount.add(lblConfirmAccountTitle, lblConfirmAccount);
    var flxConfirmDepositeAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmDepositeAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxConfirmDepositeAmount.setDefaultUnit(kony.flex.DP);
    var lblConfirmDepositeAmountTitle = new kony.ui.Label({
        "id": "lblConfirmDepositeAmountTitle",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.depositAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmDepositeAmount = new kony.ui.Label({
        "id": "lblConfirmDepositeAmount",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmDepositeAmountCurr = new kony.ui.Label({
        "id": "lblConfirmDepositeAmountCurr",
        "isVisible": false,
        "left": "30%",
        "skin": "sknNumber",
        "text": "JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmDepositeAmount.add(lblConfirmDepositeAmountTitle, lblConfirmDepositeAmount, lblConfirmDepositeAmountCurr);
    var flxConfirmTener = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmTener",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxConfirmTener.setDefaultUnit(kony.flex.DP);
    var lblConfirmTenerTitle = new kony.ui.Label({
        "id": "lblConfirmTenerTitle",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.tener"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmTener = new kony.ui.Label({
        "id": "lblConfirmTener",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmTener.add(lblConfirmTenerTitle, lblConfirmTener);
    var flxInterestAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxInterestAccount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxInterestAccount.setDefaultUnit(kony.flex.DP);
    var lblInterestAccountTitle = new kony.ui.Label({
        "id": "lblInterestAccountTitle",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknlblBodytxt",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblInterestAccount = new kony.ui.Label({
        "id": "lblInterestAccount",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxInterestAccount.add(lblInterestAccountTitle, lblInterestAccount);
    var flxConfirmInterestRate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmInterestRate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxConfirmInterestRate.setDefaultUnit(kony.flex.DP);
    var lblConfirmInterestRateTitle = new kony.ui.Label({
        "id": "lblConfirmInterestRateTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.overview.interestRate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmInterestRate = new kony.ui.Label({
        "id": "lblConfirmInterestRate",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmInterestRate.add(lblConfirmInterestRateTitle, lblConfirmInterestRate);
    var flxConfirmInterestPayable = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmInterestPayable",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxConfirmInterestPayable.setDefaultUnit(kony.flex.DP);
    var lblConfirmInterestPayableTitle = new kony.ui.Label({
        "id": "lblConfirmInterestPayableTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.interestPayable"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmInterestPayable = new kony.ui.Label({
        "id": "lblConfirmInterestPayable",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmInterestPayable.add(lblConfirmInterestPayableTitle, lblConfirmInterestPayable);
    var flxConfirmStartDepositeDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmStartDepositeDate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxConfirmStartDepositeDate.setDefaultUnit(kony.flex.DP);
    var lblConfirmStartDepositeDateTitle = new kony.ui.Label({
        "id": "lblConfirmStartDepositeDateTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.effectivedate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmStartDepositeDate = new kony.ui.Label({
        "id": "lblConfirmStartDepositeDate",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmStartDepositeDate.add(lblConfirmStartDepositeDateTitle, lblConfirmStartDepositeDate);
    var flxConfirmProfit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmProfit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxConfirmProfit.setDefaultUnit(kony.flex.DP);
    var lblConfirmProfitTitle = new kony.ui.Label({
        "id": "lblConfirmProfitTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmProfit = new kony.ui.Label({
        "id": "lblConfirmProfit",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmProfit.add(lblConfirmProfitTitle, lblConfirmProfit);
    var flxConfirmMaturityInstruction = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmMaturityInstruction",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxConfirmMaturityInstruction.setDefaultUnit(kony.flex.DP);
    var lblConfirmMaturityInstructionTitle = new kony.ui.Label({
        "id": "lblConfirmMaturityInstructionTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.termDeposit.renewalInstruction"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmMaturityInstruction = new kony.ui.Label({
        "id": "lblConfirmMaturityInstruction",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmMaturityInstruction.add(lblConfirmMaturityInstructionTitle, lblConfirmMaturityInstruction);
    var btnConfirm = new kony.ui.Button({
        "bottom": "5%",
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "9%",
        "id": "btnConfirm",
        "isVisible": true,
        "onClick": AS_Button_fd728146bf1047818ea237fa00187c67,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.transfers.CONFIRM"),
        "top": "5%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxConfirmDeposite.add(flxConfirmAccount, flxConfirmDepositeAmount, flxConfirmTener, flxInterestAccount, flxConfirmInterestRate, flxConfirmInterestPayable, flxConfirmStartDepositeDate, flxConfirmProfit, flxConfirmMaturityInstruction, btnConfirm);
    frmOpenTermDeposit.add(flxHeader, flxDepositSelection1, flxDepositSelection, flxConfirmDeposite);
};

function frmOpenTermDepositGlobals() {
    frmOpenTermDeposit = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmOpenTermDeposit,
        "bounces": false,
        "enabledForIdleTimeout": true,
        "id": "frmOpenTermDeposit",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "postShow": AS_Form_a3b4e109cc5348e1943928cef50c9e7a,
        "skin": "sknmainGradient",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_dcf426bf4eeb477e99ec28c5cc5177ec,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};