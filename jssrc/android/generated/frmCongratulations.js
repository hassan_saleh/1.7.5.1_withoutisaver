function addWidgetsfrmCongratulations() {
    frmCongratulations.setDefaultUnit(kony.flex.DP);
    var flxCongratulations = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCongratulations",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCongratulations.setDefaultUnit(kony.flex.DP);
    var imgCongratulations = new kony.ui.Image2({
        "centerX": "50%",
        "height": "35%",
        "id": "imgCongratulations",
        "isVisible": true,
        "skin": "slImage",
        "src": "success.png",
        "top": "10%",
        "width": "90%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCongratulations = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblCongratulations",
        "isVisible": true,
        "skin": "sknLblMsg",
        "text": "Success!",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMessage = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblMessage",
        "isVisible": true,
        "skin": "CopysknLblNextDisabled0eafb56463b5e4c",
        "text": "You have successfully created an access to your Mobile Bank.",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "1%",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRefNumber = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblRefNumber",
        "isVisible": true,
        "skin": "CopysknLblNextDisabled0eafb56463b5e4c",
        "text": "243567",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "1%",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnScreen = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonGreenFocus",
        "height": "32dp",
        "id": "btnScreen",
        "isVisible": true,
        "maxWidth": "90%",
        "onClick": AS_Button_iec6b5e5ba844cc0abf9558d95c6dda4,
        "skin": "slButtonGreen",
        "text": "GO TO LOGIN SCREEN",
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [5, 0, 5, 0],
        "paddingInPixel": false
    }, {});
    var btnSecondary = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonTealFocus",
        "height": "32dp",
        "id": "btnSecondary",
        "isVisible": true,
        "maxWidth": "90%",
        "onClick": AS_Button_a711b69fbec244d3981dd7c1c1a3fdd3,
        "skin": "slButtonTeal",
        "text": "ADD NEW BENEFICIARY",
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [5, 0, 5, 0],
        "paddingInPixel": false
    }, {});
    var btnThree = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonLightBlueFocus",
        "height": "32dp",
        "id": "btnThree",
        "isVisible": true,
        "maxWidth": "90%",
        "skin": "slButtonLightBlue",
        "text": "PAY NOW",
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [5, 0, 5, 0],
        "paddingInPixel": false
    }, {});
    flxCongratulations.add(imgCongratulations, lblCongratulations, lblMessage, lblRefNumber, btnScreen, btnSecondary, btnThree);
    frmCongratulations.add(flxCongratulations);
};

function frmCongratulationsGlobals() {
    frmCongratulations = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCongratulations,
        "enabledForIdleTimeout": true,
        "id": "frmCongratulations",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "slFormCommon"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_j3e48d7d85c346fda2012ec18afbbf56,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};