function addWidgetsfrmOrderCheckBook() {
    frmOrderCheckBook.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b7d74518f07a4d",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var lblFormHeading = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "45%",
        "height": "100%",
        "id": "lblFormHeading",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.accounts.orderchequebook"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxNext = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxNext",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_b36c9127a5734c079b9252faa2de4847,
        "right": "2%",
        "skin": "slFbox",
        "top": "5%",
        "width": "18%",
        "zIndex": 2
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxNext.setDefaultUnit(kony.flex.DP);
    var lblNext = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Next Page"
        },
        "centerY": "45%",
        "height": "90%",
        "id": "lblNext",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNext.add(lblNext);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btn = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "0%",
        "id": "btn",
        "isVisible": true,
        "left": "0%",
        "skin": "btnBack0b71f859656c647",
        "top": "0%",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBack.add(lblBackIcon, lblBack, btn);
    flxHeader.add(lblFormHeading, flxNext, flxBack);
    var flxVertical = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxVertical",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "1%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxVertical.setDefaultUnit(kony.flex.DP);
    var flxAccountType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxAccountType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0f0c75590716b41",
        "top": "4%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxAccountType.setDefaultUnit(kony.flex.DP);
    var lblFrequency = new kony.ui.Label({
        "height": "35%",
        "id": "lblFrequency",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.settings.account"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFre = new kony.ui.Label({
        "bottom": "0%",
        "height": "48%",
        "id": "lblFre",
        "isVisible": true,
        "left": "5%",
        "onTouchEnd": AS_Label_f4eba97f8c854d1abb516d42e9795220,
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.settings.account"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "28%",
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNumber1 = new kony.ui.Label({
        "bottom": "0%",
        "height": "1%",
        "id": "lblNumber1",
        "isVisible": false,
        "left": "90%",
        "onTouchEnd": AS_Label_f4eba97f8c854d1abb516d42e9795220,
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.settings.account"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "38%",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLine4 = new kony.ui.Label({
        "centerX": "50%",
        "height": "2%",
        "id": "lblLine4",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "77%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxEdit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxEdit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "53dp",
        "zIndex": 1
    }, {}, {});
    flxEdit.setDefaultUnit(kony.flex.DP);
    var lblEdit = new kony.ui.Label({
        "height": "100%",
        "id": "lblEdit",
        "isVisible": true,
        "left": "0dp",
        "onTouchEnd": AS_Label_c4fb083e6255458fbdd8628f97c19a71,
        "skin": "sknLblEditDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEdit.add(lblEdit);
    flxAccountType.add(lblFrequency, lblFre, lblNumber1, lblLine4, flxEdit);
    var flxBranch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxBranch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_a3407852f85443838ff9f243ec611264,
        "skin": "slFbox0f0c75590716b41",
        "top": "4%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxBranch.setDefaultUnit(kony.flex.DP);
    var lblCurrHead = new kony.ui.Label({
        "height": "35%",
        "id": "lblCurrHead",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.locateus.branch"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBranch = new kony.ui.Label({
        "bottom": "0%",
        "centerX": "50%",
        "height": "48%",
        "id": "lblBranch",
        "isVisible": true,
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.locateus.branch"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "28%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblLine0ib1721fa6b7140 = new kony.ui.Label({
        "centerX": "50%",
        "height": "2%",
        "id": "CopylblLine0ib1721fa6b7140",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "77%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxEdit0e3e34b1bf78049 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "CopyflxEdit0e3e34b1bf78049",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "53dp",
        "zIndex": 1
    }, {}, {});
    CopyflxEdit0e3e34b1bf78049.setDefaultUnit(kony.flex.DP);
    var CopylblEdit0b4a3f2847e3d44 = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblEdit0b4a3f2847e3d44",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblEditDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxEdit0e3e34b1bf78049.add(CopylblEdit0b4a3f2847e3d44);
    flxBranch.add(lblCurrHead, lblBranch, CopylblLine0ib1721fa6b7140, CopyflxEdit0e3e34b1bf78049);
    var flxLeaves = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxLeaves",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_jaa81199f6ab4fc894ee06ea90e3416d,
        "skin": "slFbox0f0c75590716b41",
        "top": "4%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxLeaves.setDefaultUnit(kony.flex.DP);
    var CopylblFrequency0i1313ad52a414f = new kony.ui.Label({
        "height": "35%",
        "id": "CopylblFrequency0i1313ad52a414f",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.ordercheck.numberofLeaves"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLeaves = new kony.ui.Label({
        "bottom": "0%",
        "centerX": "50%",
        "height": "48%",
        "id": "lblLeaves",
        "isVisible": true,
        "skin": "sknLblWhike125",
        "text": "Number of Cheque Leaves",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "28%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblLine0fb169aada68946 = new kony.ui.Label({
        "centerX": "50%",
        "height": "2%",
        "id": "CopylblLine0fb169aada68946",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "77%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxEdit0e7db89c6b8c741 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "CopyflxEdit0e7db89c6b8c741",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "53dp",
        "zIndex": 1
    }, {}, {});
    CopyflxEdit0e7db89c6b8c741.setDefaultUnit(kony.flex.DP);
    var CopylblEdit0d902278d20054d = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblEdit0d902278d20054d",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblEditDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxEdit0e7db89c6b8c741.add(CopylblEdit0d902278d20054d);
    flxLeaves.add(CopylblFrequency0i1313ad52a414f, lblLeaves, CopylblLine0fb169aada68946, CopyflxEdit0e7db89c6b8c741);
    var flxBook = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxBook",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d050f4dd5a7f436b9381f3b0eaeeaed4,
        "skin": "slFbox0f0c75590716b41",
        "top": "4%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxBook.setDefaultUnit(kony.flex.DP);
    var lblBookHeader = new kony.ui.Label({
        "height": "35%",
        "id": "lblBookHeader",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.ordercheck.numberofbook"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBook = new kony.ui.Label({
        "bottom": "0%",
        "centerX": "50%",
        "height": "48%",
        "id": "lblBook",
        "isVisible": true,
        "skin": "sknLblWhike125",
        "text": "Number of Cheque Book",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "28%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblLine0a150866e36e549 = new kony.ui.Label({
        "centerX": "50%",
        "height": "2%",
        "id": "CopylblLine0a150866e36e549",
        "isVisible": true,
        "skin": "lblLine0h8b99d8a85f649",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "77%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxEdit0g29a329a2fad42 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "CopyflxEdit0g29a329a2fad42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "53dp",
        "zIndex": 1
    }, {}, {});
    CopyflxEdit0g29a329a2fad42.setDefaultUnit(kony.flex.DP);
    var CopylblEdit0j37fb44023984d = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblEdit0j37fb44023984d",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblEditDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxEdit0g29a329a2fad42.add(CopylblEdit0j37fb44023984d);
    flxBook.add(lblBookHeader, lblBook, CopylblLine0a150866e36e549, CopyflxEdit0g29a329a2fad42);
    var CopylblCurrHead0i311dddb0c4149 = new kony.ui.Label({
        "id": "CopylblCurrHead0i311dddb0c4149",
        "isVisible": false,
        "right": "5%",
        "skin": "sknLblCurrWhiteee",
        "text": kony.i18n.getLocalizedString("i18n.accounts.orderchequemsg"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxVertical.add(flxAccountType, flxBranch, flxLeaves, flxBook, CopylblCurrHead0i311dddb0c4149);
    frmOrderCheckBook.add(flxHeader, flxVertical);
};

function frmOrderCheckBookGlobals() {
    frmOrderCheckBook = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmOrderCheckBook,
        "enabledForIdleTimeout": true,
        "id": "frmOrderCheckBook",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "skin": "slFormCommon"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};