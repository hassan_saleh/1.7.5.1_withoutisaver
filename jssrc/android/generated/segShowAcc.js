function initializesegShowAcc() {
    flxSeg = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "105dp",
        "id": "flxSeg",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    flxSeg.setDefaultUnit(kony.flex.DP);
    var flxAnimateBenf = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxAnimateBenf",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d2180100b63445fe87e78ac338ca61fe,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAnimateBenf.setDefaultUnit(kony.flex.DP);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "1%",
        "isModalContainer": false,
        "skin": "sknFlxToIcon",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon1.add(lblInitial);
    var flxDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80%",
        "id": "flxDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "15%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "12%",
        "width": "58%",
        "zIndex": 1
    }, {}, {});
    flxDetails.setDefaultUnit(kony.flex.DP);
    var BenificiaryName = new kony.ui.Label({
        "id": "BenificiaryName",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblBack",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "2%",
        "width": "82%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var BenificiaryFullName = new kony.ui.Label({
        "id": "BenificiaryFullName",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblBack",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "2%",
        "width": "82%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var accountNumber = new kony.ui.Label({
        "id": "accountNumber",
        "isVisible": true,
        "left": "0%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0%",
        "width": "84%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDetails.add(BenificiaryName, BenificiaryFullName, accountNumber);
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "top": "99%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    var flxButtonHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxButtonHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "91%",
        "isModalContainer": false,
        "skin": "flexTransparent",
        "top": "0%",
        "width": "9%",
        "zIndex": 1
    }, {}, {});
    flxButtonHolder.setDefaultUnit(kony.flex.DP);
    var btnDelete = new kony.ui.Button({
        "focusSkin": "CopysknSegmentDelete0h225a870b5144b",
        "height": "100%",
        "id": "btnDelete",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_h5f988328ddf49b0a7b4b0755f24dc69,
        "skin": "CopysknSegmentDelete0h225a870b5144b",
        "text": "w",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxButtonHolder.add(btnDelete);
    var btnFav = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnBack",
        "height": "100%",
        "id": "btnFav",
        "isVisible": true,
        "onClick": AS_Button_d28ddf1dc8a34dcdb547d401188fc640,
        "right": "9%",
        "skin": "sknBtnBack",
        "text": "k",
        "width": "9%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnEditBeneficiary = new kony.ui.Button({
        "focusSkin": "CopysknSegmentDelete0h225a870b5144b",
        "height": "100%",
        "id": "btnEditBeneficiary",
        "isVisible": true,
        "onClick": AS_Button_c88e1f1560c14989b710135776ef94e1,
        "right": "18%",
        "skin": "CopysknSegmentDelete0h225a870b5144b",
        "text": "0",
        "top": "0dp",
        "width": "9%",
        "zIndex": 2000
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxAnimateBenf.add(flxIcon1, flxDetails, contactListDivider, flxButtonHolder, btnFav, btnEditBeneficiary);
    flxSeg.add(flxAnimateBenf);
}