function initializetmpFrequrncyData() {
    flxFrequencyTmp = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxFrequencyTmp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "CopyslFbox0h5adacabf4a940"
    }, {}, {});
    flxFrequencyTmp.setDefaultUnit(kony.flex.DP);
    var lblfrequency = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblfrequency",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblKey = new kony.ui.Label({
        "centerX": "60%",
        "centerY": "60%",
        "id": "lblKey",
        "isVisible": false,
        "left": "5%",
        "skin": "sknLblWhike125",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxFrequencyTmp.add(lblfrequency, lblKey);
}