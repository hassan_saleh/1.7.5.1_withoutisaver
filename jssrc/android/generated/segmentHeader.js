function initializesegmentHeader() {
    Copycontainer087f3a0994b334a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "Copycontainer087f3a0994b334a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknNone"
    }, {}, {});
    Copycontainer087f3a0994b334a.setDefaultUnit(kony.flex.DP);
    var lblHeaderKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "90%",
        "id": "lblHeaderKA",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhike125",
        "top": "5%",
        "width": "96%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "0%",
        "id": "contactListDivider",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "top": "94%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    Copycontainer087f3a0994b334a.add(lblHeaderKA, contactListDivider);
}