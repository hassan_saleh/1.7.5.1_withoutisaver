function addWidgetsfrmOrderChequeBookConfirm() {
    frmOrderChequeBookConfirm.setDefaultUnit(kony.flex.DP);
    var flxnewSubAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxnewSubAccount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxnewSubAccount.setDefaultUnit(kony.flex.DP);
    var lblJoMoPay = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "45%",
        "id": "lblJoMoPay",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnBack = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
        "height": "100%",
        "id": "btnBack",
        "isVisible": false,
        "left": "0%",
        "onClick": AS_Button_d1bb6aea9ef6470fa84a3333926c4fda,
        "skin": "CopyslButtonGlossBlue0e73a02c4810645",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0%",
        "width": "15%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_hecccea50fb44b9b811679bfb49978c3,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 2
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblBack0i5985fbe3b634e",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
    flxnewSubAccount.add(lblJoMoPay, btnBack, flxBack);
    var flxAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxAccount",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "14%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccount.setDefaultUnit(kony.flex.DP);
    var lblTransferTypeStatic = new kony.ui.Label({
        "height": "40%",
        "id": "lblTransferTypeStatic",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.settings.account"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxTransferType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45%",
        "id": "flxTransferType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "3%",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    flxTransferType.setDefaultUnit(kony.flex.DP);
    var lblAccount = new kony.ui.Label({
        "id": "lblAccount",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTransferType.add(lblAccount);
    flxAccount.add(lblTransferTypeStatic, flxTransferType);
    var flxBranch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxBranch",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "27%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBranch.setDefaultUnit(kony.flex.DP);
    var lblAccountStaticText = new kony.ui.Label({
        "height": "40%",
        "id": "lblAccountStaticText",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.locateus.branch"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBranch = new kony.ui.Label({
        "id": "lblBranch",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBranch.add(lblAccountStaticText, lblBranch);
    var btnConfirm = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhite",
        "height": "8%",
        "id": "btnConfirm",
        "isVisible": true,
        "left": "10%",
        "onClick": AS_Button_ef1d7687c2dc40818c312c5769ff0ea5,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.P2P.confirm"),
        "top": "87%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label0bd39896ab0b246 = new kony.ui.Label({
        "id": "Label0bd39896ab0b246",
        "isVisible": true,
        "left": "4%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.accounts.ordersubjecttofees"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "72%",
        "width": "92%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxAccount0ccd1462b83054a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "CopyflxAccount0ccd1462b83054a",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "40%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxAccount0ccd1462b83054a.setDefaultUnit(kony.flex.DP);
    var CopylblTransferTypeStatic0bb0a6097fb3e40 = new kony.ui.Label({
        "height": "40%",
        "id": "CopylblTransferTypeStatic0bb0a6097fb3e40",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.ordercheck.numberofLeaves"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxTransferType0fa9d05fdfe7e4c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45%",
        "id": "CopyflxTransferType0fa9d05fdfe7e4c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "3%",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    CopyflxTransferType0fa9d05fdfe7e4c.setDefaultUnit(kony.flex.DP);
    var lblLeaves = new kony.ui.Label({
        "id": "lblLeaves",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxTransferType0fa9d05fdfe7e4c.add(lblLeaves);
    CopyflxAccount0ccd1462b83054a.add(CopylblTransferTypeStatic0bb0a6097fb3e40, CopyflxTransferType0fa9d05fdfe7e4c);
    var CopyflxBranch0d4e114b0c2a741 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "CopyflxBranch0d4e114b0c2a741",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "53%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxBranch0d4e114b0c2a741.setDefaultUnit(kony.flex.DP);
    var CopylblAccountStaticText0b80c8a938e0945 = new kony.ui.Label({
        "height": "40%",
        "id": "CopylblAccountStaticText0b80c8a938e0945",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.ordercheck.numberofbook"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBook = new kony.ui.Label({
        "id": "lblBook",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxBranch0d4e114b0c2a741.add(CopylblAccountStaticText0b80c8a938e0945, lblBook);
    var lblBranchCode = new kony.ui.Label({
        "id": "lblBranchCode",
        "isVisible": false,
        "left": "100%",
        "skin": "slLabel",
        "text": "abel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "100%",
        "width": "1px",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": false,
        "left": "110%",
        "skin": "slLabel",
        "text": "abel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "110%",
        "width": "1px",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    frmOrderChequeBookConfirm.add(flxnewSubAccount, flxAccount, flxBranch, btnConfirm, Label0bd39896ab0b246, CopyflxAccount0ccd1462b83054a, CopyflxBranch0d4e114b0c2a741, lblBranchCode, lblAccountNumber);
};

function frmOrderChequeBookConfirmGlobals() {
    frmOrderChequeBookConfirm = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmOrderChequeBookConfirm,
        "enabledForIdleTimeout": true,
        "id": "frmOrderChequeBookConfirm",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};