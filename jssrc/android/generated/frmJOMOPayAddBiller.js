function addWidgetsfrmJOMOPayAddBiller() {
    frmJOMOPayAddBiller.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_aa6681b734a7454686fecd28170e02d0,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblBack0i5985fbe3b634e",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
    var lblJoMoPay = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "45%",
        "id": "lblJoMoPay",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Addbeneficiary"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(flxBack, lblJoMoPay);
    var flxFirstJomopay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxFirstJomopay",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFirstJomopay.setDefaultUnit(kony.flex.DP);
    var flxName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "12%",
        "id": "flxName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxName.setDefaultUnit(kony.flex.DP);
    var lblNameTitle = new kony.ui.Label({
        "id": "lblNameTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.common.name"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "45%",
        "id": "txtName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": 30,
        "onTextChange": AS_TextField_bb622a4caa0a48f59fa65a4cfb6f51d6,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "40%",
        "width": "99%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_ibb81040a82c42d08b8f0484f4de7c24,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "skntextFieldDividerJomoPay",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderName.setDefaultUnit(kony.flex.DP);
    flxBorderName.add();
    flxName.add(lblNameTitle, txtName, flxBorderName);
    var flxNickName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "12%",
        "id": "flxNickName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "14%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNickName.setDefaultUnit(kony.flex.DP);
    var lblNickNameTitle = new kony.ui.Label({
        "id": "lblNickNameTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtNickName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "45%",
        "id": "txtNickName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "onTextChange": AS_TextField_c9d555dc789740f289489033bf621acb,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "40%",
        "width": "99%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_e4a4873bacbe4246a48f6060ee5cac68,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderNickName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderNickName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "skntextFieldDividerJomoPay",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderNickName.setDefaultUnit(kony.flex.DP);
    flxBorderNickName.add();
    flxNickName.add(lblNickNameTitle, txtNickName, flxBorderNickName);
    var flxJoMoPayType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "12.50%",
        "id": "flxJoMoPayType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox0b3edf457af3b43",
        "top": "28%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxJoMoPayType.setDefaultUnit(kony.flex.DP);
    var flxJoMoPayTypeStaticText = new kony.ui.Label({
        "id": "flxJoMoPayTypeStaticText",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblCurr",
        "text": kony.i18n.getLocalizedString("i18n.jomopay.jomopaytype"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxJoMoPay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxJoMoPay",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "38%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxJoMoPay.setDefaultUnit(kony.flex.DP);
    var lblType = new kony.ui.Label({
        "height": "100%",
        "id": "lblType",
        "isVisible": true,
        "left": "1%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.settings.select"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnAliasDropDown = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "100%",
        "id": "btnAliasDropDown",
        "isVisible": true,
        "onClick": AS_Button_c391ab82e3eb4e84b9175ffe2db931bf,
        "right": "7%",
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "2%",
        "width": "93%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxJoMoPay.add(lblType, btnAliasDropDown);
    var flxBorderjomopaytype = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderjomopaytype",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "skntextFieldDividerJomoPay",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderjomopaytype.setDefaultUnit(kony.flex.DP);
    flxBorderjomopaytype.add();
    flxJoMoPayType.add(flxJoMoPayTypeStaticText, flxJoMoPay, flxBorderjomopaytype);
    var flxJomopayT = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "210dp",
        "id": "flxJomopayT",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "42%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxJomopayT.setDefaultUnit(kony.flex.DP);
    var flxBeneficiaryMobile = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "37%",
        "id": "flxBeneficiaryMobile",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBeneficiaryMobile.setDefaultUnit(kony.flex.DP);
    var lblBeneficiaryMobileTitle = new kony.ui.Label({
        "id": "lblBeneficiaryMobileTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.jomopay.mobile"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtBeneficiaryMobile = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "45%",
        "id": "txtBeneficiaryMobile",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "onTextChange": AS_TextField_facf8aec7c74486baae9c373049ddb98,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "50%",
        "width": "84%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_f998bd0e237941d5a884831c5722c505,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderBenificiary = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderBenificiary",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "skntextFieldDividerJomoPay",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderBenificiary.setDefaultUnit(kony.flex.DP);
    flxBorderBenificiary.add();
    var btnContactList = new kony.ui.Button({
        "focusSkin": "sknBtnContactList",
        "height": "45%",
        "id": "btnContactList",
        "isVisible": true,
        "left": "85%",
        "onClick": AS_Button_j9ff065888e24d8f848599e60d8fea2e,
        "skin": "sknBtnContactList",
        "text": "y",
        "top": "40%",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBeneficiaryMobile.add(lblBeneficiaryMobileTitle, txtBeneficiaryMobile, flxBorderBenificiary, btnContactList);
    var lblMobileIDHint = new kony.ui.Label({
        "id": "lblMobileIDHint",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.jomopay.mobilecashidhint"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-3%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBeneficiaryAlias = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "37%",
        "id": "flxBeneficiaryAlias",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBeneficiaryAlias.setDefaultUnit(kony.flex.DP);
    var lblBeneficiaryAlias = new kony.ui.Label({
        "id": "lblBeneficiaryAlias",
        "isVisible": true,
        "left": "2%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.jomopay.benificiaryalias"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtBeneficiaryAlias = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "45%",
        "id": "txtBeneficiaryAlias",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "onTextChange": AS_TextField_ce344326a9c648d6af36ea44d5b4e070,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "50%",
        "width": "99%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_j81ea6cb81954462aa266f4c0a8b3ef5,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderAlias = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderAlias",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "skntextFieldDividerJomoPay",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderAlias.setDefaultUnit(kony.flex.DP);
    flxBorderAlias.add();
    flxBeneficiaryAlias.add(lblBeneficiaryAlias, txtBeneficiaryAlias, flxBorderAlias);
    var flxFav = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "25%",
        "id": "flxFav",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFav.setDefaultUnit(kony.flex.DP);
    var btnFavCheckBox = new kony.ui.Button({
        "focusSkin": "sknRDOWhiteBRDBOJFont",
        "height": "100%",
        "id": "btnFavCheckBox",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_ecad49a2f10f442d9fbad39dd075420d,
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "q",
        "top": "0%",
        "width": "8%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblFavHint = new kony.ui.Label({
        "height": "100%",
        "id": "lblFavHint",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.jomopay.favaddbillerhint"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxFav.add(btnFavCheckBox, lblFavHint);
    flxJomopayT.add(flxBeneficiaryMobile, lblMobileIDHint, flxBeneficiaryAlias, flxFav);
    var btnAddBilller = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "90%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "8%",
        "id": "btnAddBilller",
        "isVisible": true,
        "onClick": AS_Button_db508af1834440b79b0baf84cc110587,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Addbeneficiary"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnEditBeneficiary = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "90%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "8%",
        "id": "btnEditBeneficiary",
        "isVisible": false,
        "onClick": AS_Button_e19951c8c37848229c0aa62bdf8dfc5c,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.jomopay.editJomoBene"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxFirstJomopay.add(flxName, flxNickName, flxJoMoPayType, flxJomopayT, btnAddBilller, btnEditBeneficiary);
    var flxConfirmJomopay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxConfirmJomopay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxConfirmJomopay.setDefaultUnit(kony.flex.DP);
    var flxNameConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxNameConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0.00%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNameConfirm.setDefaultUnit(kony.flex.DP);
    var lblNameConfirmStaticText = new kony.ui.Label({
        "height": "50%",
        "id": "lblNameConfirmStaticText",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.common.name"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxNameConfirm1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxNameConfirm1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxNameConfirm1.setDefaultUnit(kony.flex.DP);
    var lblNameConfirm = new kony.ui.Label({
        "id": "lblNameConfirm",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNameConfirm1.add(lblNameConfirm);
    flxNameConfirm.add(lblNameConfirmStaticText, flxNameConfirm1);
    var flxNickNameConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxNickNameConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0.00%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "14%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNickNameConfirm.setDefaultUnit(kony.flex.DP);
    var lblNickNameConfirmStaticText = new kony.ui.Label({
        "height": "50%",
        "id": "lblNickNameConfirmStaticText",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxNickNameConfirm1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxNickNameConfirm1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxNickNameConfirm1.setDefaultUnit(kony.flex.DP);
    var lblNickNameConfirm = new kony.ui.Label({
        "id": "lblNickNameConfirm",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNickNameConfirm1.add(lblNickNameConfirm);
    flxNickNameConfirm.add(lblNickNameConfirmStaticText, flxNickNameConfirm1);
    var flxJoMoPayTypeConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxJoMoPayTypeConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0.00%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "28%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxJoMoPayTypeConfirm.setDefaultUnit(kony.flex.DP);
    var lblJoMoPayTypeConfirmStaticText = new kony.ui.Label({
        "height": "50%",
        "id": "lblJoMoPayTypeConfirmStaticText",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.jomopay.jomopaytype"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxJoMoPayTypeConfirm1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxJoMoPayTypeConfirm1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxJoMoPayTypeConfirm1.setDefaultUnit(kony.flex.DP);
    var lblJoMoPayTypeConfirm = new kony.ui.Label({
        "id": "lblJoMoPayTypeConfirm",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxJoMoPayTypeConfirm1.add(lblJoMoPayTypeConfirm);
    flxJoMoPayTypeConfirm.add(lblJoMoPayTypeConfirmStaticText, flxJoMoPayTypeConfirm1);
    var flxAccountConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxAccountConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0.00%",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "42%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccountConfirm.setDefaultUnit(kony.flex.DP);
    var lblAccountConfirmStaticTex = new kony.ui.Label({
        "height": "50%",
        "id": "lblAccountConfirmStaticTex",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.settings.account"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAccountConfirm1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxAccountConfirm1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxAccountConfirm1.setDefaultUnit(kony.flex.DP);
    var lblAccountConfirm = new kony.ui.Label({
        "id": "lblAccountConfirm",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAccountConfirm1.add(lblAccountConfirm);
    flxAccountConfirm.add(lblAccountConfirmStaticTex, flxAccountConfirm1);
    var flxFavConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxFavConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "55%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFavConfirm.setDefaultUnit(kony.flex.DP);
    var btnFavConfirm = new kony.ui.Button({
        "focusSkin": "sknRDOWhiteBRDBOJFont",
        "height": "100%",
        "id": "btnFavConfirm",
        "isVisible": true,
        "left": "0%",
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "q",
        "top": "0%",
        "width": "8%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblFavConfirm = new kony.ui.Label({
        "height": "100%",
        "id": "lblFavConfirm",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.common.Favourite"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxFavConfirm.add(btnFavConfirm, lblFavConfirm);
    var btnConfirm = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhite",
        "height": "8%",
        "id": "btnConfirm",
        "isVisible": true,
        "left": "10%",
        "onClick": AS_Button_ec684d1f2c804687bb5cb3d56775b0ee,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.P2P.confirm"),
        "top": "80%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxConfirmJomopay.add(flxNameConfirm, flxNickNameConfirm, flxJoMoPayTypeConfirm, flxAccountConfirm, flxFavConfirm, btnConfirm);
    frmJOMOPayAddBiller.add(flxHeader, flxFirstJomopay, flxConfirmJomopay);
};

function frmJOMOPayAddBillerGlobals() {
    frmJOMOPayAddBiller = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmJOMOPayAddBiller,
        "bounces": false,
        "enabledForIdleTimeout": false,
        "id": "frmJOMOPayAddBiller",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "postShow": AS_Form_b347419299ca4fb5a8ed6822518c74df,
        "skin": "sknSuccessBkg",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_edb97a33718c44b2a6022550e2ad621f,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};