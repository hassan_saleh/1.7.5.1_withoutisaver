function addWidgetsfrmMoreInterestRatesKA() {
    frmMoreInterestRatesKA.setDefaultUnit(kony.flex.DP);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var androidTitleLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "androidTitleLabel",
        "isVisible": true,
        "left": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.more.standardCdRates"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var androidBack = new kony.ui.Button({
        "focusSkin": "sknandroidBackButtonFocus",
        "height": "50dp",
        "id": "androidBack",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_14d7f5ca90ac4169ac45fbabdb554349,
        "skin": "sknandroidBackButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    androidTitleBar.add(androidTitleLabel, androidBack);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": 0,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "50dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var interestRatesHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "interestRatesHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    interestRatesHeader.setDefaultUnit(kony.flex.DP);
    var lblCDTermHdrKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCDTermHdrKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.more.cdTerm"),
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAPYHdrKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblAPYHdrKA",
        "isVisible": true,
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.more.apy"),
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMinDopositHdrKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblMinDopositHdrKA",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.more.minimumDeposit"),
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    interestRatesHeader.add(lblCDTermHdrKA, lblAPYHdrKA, lblMinDopositHdrKA);
    if (typeof initializeCopyFBox02bb1e4273cee42 === 'function') initializeCopyFBox02bb1e4273cee42();
    var segInterestRatesKA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblAPYValKA": ".5%",
            "lblCDTermValKA": "3 months",
            "lblMinDepositValKA": "2000.00"
        }, {
            "lblAPYValKA": "",
            "lblCDTermValKA": "",
            "lblMinDepositValKA": ""
        }, {
            "lblAPYValKA": "",
            "lblCDTermValKA": "",
            "lblMinDepositValKA": ""
        }, {
            "lblAPYValKA": "",
            "lblCDTermValKA": "",
            "lblMinDepositValKA": ""
        }, {
            "lblAPYValKA": "",
            "lblCDTermValKA": "",
            "lblMinDepositValKA": ""
        }, {
            "lblAPYValKA": "",
            "lblCDTermValKA": "",
            "lblMinDepositValKA": ""
        }, {
            "lblAPYValKA": "",
            "lblCDTermValKA": "",
            "lblMinDepositValKA": ""
        }, {
            "lblAPYValKA": "",
            "lblCDTermValKA": "",
            "lblMinDepositValKA": ""
        }, {
            "lblAPYValKA": "",
            "lblCDTermValKA": "",
            "lblMinDepositValKA": ""
        }],
        "groupCells": false,
        "id": "segInterestRatesKA",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": CopyFBox02bb1e4273cee42,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "eeeeee00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "lblAPYValKA": "lblAPYValKA",
            "lblCDTermValKA": "lblCDTermValKA",
            "lblMinDepositValKA": "lblMinDepositValKA"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    mainContent.add(interestRatesHeader, segInterestRatesKA);
    frmMoreInterestRatesKA.add(androidTitleBar, mainContent);
};

function frmMoreInterestRatesKAGlobals() {
    frmMoreInterestRatesKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmMoreInterestRatesKA,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmMoreInterestRatesKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "inTransitionConfig": {
            "formAnimation": 3
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_0ce4420912074765a3ce2a469225684f,
        "outTransitionConfig": {
            "formAnimation": 4
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};