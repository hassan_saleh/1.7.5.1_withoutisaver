function initializesegListViewKA() {
    flxListViewKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxListViewKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    flxListViewKA.setDefaultUnit(kony.flex.DP);
    var informationListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "informationListDivider",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "top": "99dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    informationListDivider.setDefaultUnit(kony.flex.DP);
    informationListDivider.add();
    var informationListIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "28dp",
        "id": "informationListIcon",
        "isVisible": true,
        "left": "5%",
        "skin": "sknslImage",
        "src": "imagedrag.png",
        "top": "0dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var informationListLabel = new kony.ui.Label({
        "id": "informationListLabel",
        "isVisible": true,
        "left": "18%",
        "maxNumberOfLines": 1,
        "skin": "skn",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "22dp",
        "width": "256dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var addressLine1 = new kony.ui.Label({
        "id": "addressLine1",
        "isVisible": true,
        "left": "18%",
        "maxNumberOfLines": 1,
        "skin": "sknlocatorListAddress",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "48dp",
        "width": "256dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var addressLine2 = new kony.ui.Label({
        "id": "addressLine2",
        "isVisible": false,
        "left": "18%",
        "skin": "sknlocatorListAddress",
        "text": "Label",
        "top": "63dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblListStatus = new kony.ui.Label({
        "id": "lblListStatus",
        "isVisible": true,
        "left": "27%",
        "skin": "sknlocatorListAddress",
        "top": "72dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var distanceLabel = new kony.ui.Label({
        "id": "distanceLabel",
        "isVisible": true,
        "right": "10%",
        "skin": "sknDistance",
        "top": "39dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var rightChevron1 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "14dp",
        "id": "rightChevron1",
        "isVisible": true,
        "right": "3%",
        "skin": "sknslImage",
        "src": "right_chevron_icon.png",
        "width": "14dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgListStatus = new kony.ui.Image2({
        "height": "45dp",
        "id": "imgListStatus",
        "isVisible": true,
        "left": "15%",
        "skin": "slImage",
        "src": "location_open.png",
        "top": "59dp",
        "width": "45dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxListViewKA.add(informationListDivider, informationListIcon, informationListLabel, addressLine1, addressLine2, lblListStatus, distanceLabel, rightChevron1, imgListStatus);
}