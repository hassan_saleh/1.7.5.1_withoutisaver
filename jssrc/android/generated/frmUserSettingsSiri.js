function addWidgetsfrmUserSettingsSiri() {
    frmUserSettingsSiri.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var lblTouchIdHeader = new kony.ui.Label({
        "centerX": "50.00%",
        "centerY": "50.20%",
        "id": "lblTouchIdHeader",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.siri.header"),
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_ae28ab931713402da567cd6543e10e31,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBacktext = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblBacktext",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBacktext);
    flxHeader.add(lblTouchIdHeader, flxBack);
    var flxBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBody",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "9%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBody.setDefaultUnit(kony.flex.DP);
    var lblDeviceSupportSiriMsg = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblDeviceSupportSiriMsg",
        "isVisible": true,
        "maxNumberOfLines": 2,
        "skin": "sknstandardTextBold",
        "text": kony.i18n.getLocalizedString("i18n.Siri.siri1"),
        "top": "3%",
        "width": "92%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAlterAuthTouchID = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "8%",
        "id": "flxAlterAuthTouchID",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxLightGreyColor",
        "top": "18%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAlterAuthTouchID.setDefaultUnit(kony.flex.DP);
    var lblAltText = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAltText",
        "isVisible": true,
        "left": "5%",
        "skin": "sknlblTouchIdsmall",
        "text": kony.i18n.getLocalizedString("i18n.siri.enableSiri"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxSwitchOffTouchLogin = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSwitchOffTouchLogin",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_j7ceeae0de4b46f0b0437ccc3d75b918,
        "right": "3%",
        "skin": "sknflxGrey",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSwitchOffTouchLogin.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0hfee6fe5ed994b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0hfee6fe5ed994b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0hfee6fe5ed994b.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0hfee6fe5ed994b.add();
    var Copyflxlakeer0a0654737f4504c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0a0654737f4504c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0a0654737f4504c.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0a0654737f4504c.add();
    flxSwitchOffTouchLogin.add(CopyflxRoundDBlue0hfee6fe5ed994b, Copyflxlakeer0a0654737f4504c);
    var flxSwitchOnTouchLogin = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSwitchOnTouchLogin",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_a6922ef6bda24557995b7eff09e616e8,
        "right": "3%",
        "skin": "sknflxyellow",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSwitchOnTouchLogin.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0j3222bd94cbc49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0j3222bd94cbc49",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 2
    }, {}, {});
    CopyflxRoundDBlueOff0j3222bd94cbc49.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0j3222bd94cbc49.add();
    var CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "isModalContainer": false,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.add();
    flxSwitchOnTouchLogin.add(CopyflxRoundDBlueOff0j3222bd94cbc49, CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b);
    flxAlterAuthTouchID.add(lblAltText, flxSwitchOffTouchLogin, flxSwitchOnTouchLogin);
    var flexContainerAccountTransfer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flexContainerAccountTransfer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_i36b8e3f7bb54563a73e89454a826e97,
        "skin": "slFbox",
        "top": "28%",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    flexContainerAccountTransfer.setDefaultUnit(kony.flex.DP);
    var lblAccountTransfer1 = new kony.ui.Label({
        "id": "lblAccountTransfer1",
        "isVisible": false,
        "left": "2%",
        "skin": "sknlblanimated75",
        "text": kony.i18n.getLocalizedString("i18n.siri.accountTransfer"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "12%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexContainerborder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "3%",
        "id": "flexContainerborder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "95%",
        "zIndex": 2
    }, {}, {});
    flexContainerborder.setDefaultUnit(kony.flex.DP);
    flexContainerborder.add();
    var FlexContainer0de871f983c5847 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "FlexContainer0de871f983c5847",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    FlexContainer0de871f983c5847.setDefaultUnit(kony.flex.DP);
    var lblAccountTransfer = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAccountTransfer",
        "isVisible": true,
        "left": "2%",
        "skin": "slLabelTitle",
        "text": kony.i18n.getLocalizedString("i18n.siri.accountTransfer"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrow = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrow",
        "isVisible": true,
        "left": "90%",
        "skin": "sknLblEditDimmed",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0de871f983c5847.add(lblAccountTransfer, lblArrow);
    flexContainerAccountTransfer.add(lblAccountTransfer1, flexContainerborder, FlexContainer0de871f983c5847);
    var flexContainerAccountPayments = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flexContainerAccountPayments",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_ic43e2c0cf694d6fbbcf1e6244ea4b4b,
        "skin": "slFbox",
        "top": "40%",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    flexContainerAccountPayments.setDefaultUnit(kony.flex.DP);
    var labelAccountPayments1 = new kony.ui.Label({
        "id": "labelAccountPayments1",
        "isVisible": false,
        "left": "2%",
        "skin": "sknlblanimated75",
        "text": kony.i18n.getLocalizedString("i18n.siri.accountPayment"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "12%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexContainerAccountPaymentsBorder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "3%",
        "id": "flexContainerAccountPaymentsBorder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "95%",
        "zIndex": 2
    }, {}, {});
    flexContainerAccountPaymentsBorder.setDefaultUnit(kony.flex.DP);
    flexContainerAccountPaymentsBorder.add();
    var flexContainerAccountPayments1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flexContainerAccountPayments1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flexContainerAccountPayments1.setDefaultUnit(kony.flex.DP);
    var labelAccountPayments = new kony.ui.Label({
        "centerY": "50%",
        "id": "labelAccountPayments",
        "isVisible": true,
        "left": "2%",
        "skin": "slLabelTitle",
        "text": kony.i18n.getLocalizedString("i18n.siri.accountPayment"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var labelAccountPaymentsArrow1 = new kony.ui.Label({
        "centerY": "50%",
        "id": "labelAccountPaymentsArrow1",
        "isVisible": true,
        "left": "90%",
        "skin": "sknLblEditDimmed",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexContainerAccountPayments1.add(labelAccountPayments, labelAccountPaymentsArrow1);
    flexContainerAccountPayments.add(labelAccountPayments1, flexContainerAccountPaymentsBorder, flexContainerAccountPayments1);
    var flxSetSiriLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "10%",
        "id": "flxSetSiriLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "55%",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    flxSetSiriLimit.setDefaultUnit(kony.flex.DP);
    var txtSiriLimit = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "txtSiriLimit",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "onDone": AS_TextField_be51211c2b6146469735d857fee977b7,
        "onTextChange": AS_TextField_f64da55feacc40bbb9484ecefef86e04,
        "onTouchEnd": AS_TextField_bb791637741e4a9b8d50ae5801c32c89,
        "onTouchStart": AS_TextField_f71d1ca230004036afcade7c184e94a4,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "100%",
        "zIndex": 10
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var borderBottom1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "3%",
        "id": "borderBottom1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "95%",
        "zIndex": 2
    }, {}, {});
    borderBottom1.setDefaultUnit(kony.flex.DP);
    borderBottom1.add();
    var lblSiriTransLimit = new kony.ui.Label({
        "id": "lblSiriTransLimit",
        "isVisible": true,
        "left": "2%",
        "skin": "sknCaiRegWhite50Op",
        "text": "SIRI Transaction limit",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblClose1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblClose1",
        "isVisible": true,
        "onTouchStart": AS_Label_bf105407db054a46b2cdd17093859a18,
        "right": "2%",
        "skin": "sknClose",
        "text": "O",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "10%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSetSiriLimit.add(txtSiriLimit, borderBottom1, lblSiriTransLimit, lblClose1);
    var btnUpdateLimit = new kony.ui.Button({
        "bottom": "5%",
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "9%",
        "id": "btnUpdateLimit",
        "isVisible": true,
        "onClick": AS_Button_b3d0beb15e1a459a8138ffd38aa49a29,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Siri.siri4"),
        "top": "75%",
        "width": "45%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "pressedSkin": "slButtonWhiteFocus"
    });
    var lblhint = new kony.ui.Label({
        "centerX": "50%",
        "height": "7%",
        "id": "lblhint",
        "isVisible": true,
        "onTouchEnd": AS_Label_ad142f42455247eabbc4aafdc9fd40fe,
        "skin": "sknlblBodytxtUnderlined",
        "text": kony.i18n.getLocalizedString("i18n.Siri.siri2"),
        "top": "93%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBody.add(lblDeviceSupportSiriMsg, flxAlterAuthTouchID, flexContainerAccountTransfer, flexContainerAccountPayments, flxSetSiriLimit, btnUpdateLimit, lblhint);
    var flxSiriinfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxSiriinfo",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_ee7003bb67c74a5494053c5c17ac05e5,
        "skin": "CopyslFbox0dc0bf360039f47",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSiriinfo.setDefaultUnit(kony.flex.DP);
    var flxSeg = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "id": "flxSeg",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "CopyslFbox0baae4744ec8246",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxSeg.setDefaultUnit(kony.flex.DP);
    var lblHead = new kony.ui.Label({
        "id": "lblHead",
        "isVisible": true,
        "left": "0%",
        "skin": "sknCairoBold120",
        "text": "SIRI Info",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "9dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "CopyslFbox0c0057f7b02484a",
        "top": "1dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    if (typeof initializeFBox0a8248f803e1745 === 'function') initializeFBox0a8248f803e1745();
    var segmentSIRI = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblData": "    For Account Balance Speak:"
        }, {
            "lblData": "                         Account Search Balance"
        }, {
            "lblData": "    For Transfer Speak:"
        }, {
            "lblData": "                         Send Money"
        }, {
            "lblData": "                         Transfer Money"
        }],
        "groupCells": false,
        "id": "segmentSIRI",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": FBox0a8248f803e1745,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "5dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "lblData": "lblData"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnOK = new kony.ui.Button({
        "bottom": 17,
        "centerX": "50%",
        "focusSkin": "slButtonBlueFocus",
        "height": "35dp",
        "id": "btnOK",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_i6e9013b81e64df58acd4ac9f16f85d5,
        "skin": "sknCancleBtn",
        "text": kony.i18n.getLocalizedString("i18n.NUO.OKay"),
        "top": "12dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSeg.add(lblHead, flxLine, segmentSIRI, btnOK);
    flxSiriinfo.add(flxSeg);
    frmUserSettingsSiri.add(flxHeader, flxBody, flxSiriinfo);
};

function frmUserSettingsSiriGlobals() {
    frmUserSettingsSiri = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmUserSettingsSiri,
        "allowHorizontalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmUserSettingsSiri",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_ff828f677de645aabc52a2528edfa621,
        "preShow": function(eventobject) {
            AS_Form_g30ef15bb8c34de99644d811a59498d0(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_idf12fc90b44421aa88572d502ba675f,
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};