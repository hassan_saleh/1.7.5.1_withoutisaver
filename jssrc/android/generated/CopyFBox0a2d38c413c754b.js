function initializeCopyFBox0a2d38c413c754b() {
    CopyFBox0a2d38c413c754b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "CopyFBox0a2d38c413c754b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox0a2d38c413c754b.setDefaultUnit(kony.flex.DP);
    var lblServiceList = new kony.ui.Label({
        "id": "lblServiceList",
        "isVisible": true,
        "left": "0dp",
        "minHeight": "20dp",
        "skin": "sknNumber",
        "text": "Sun Closed",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    CopyFBox0a2d38c413c754b.add(lblServiceList);
}