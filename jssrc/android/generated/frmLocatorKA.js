function addWidgetsfrmLocatorKA() {
    frmLocatorKA.setDefaultUnit(kony.flex.DP);
    var titleBarLocator = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "titleBarLocator",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarLocator.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "47%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_f40ae57c3d6c4e7d9378318cac65c67c,
        "skin": "slFbox",
        "top": "0%",
        "width": "17%",
        "zIndex": 20
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnLogin = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "btnLogin",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, btnLogin);
    var lblMapHeading = new kony.ui.Label({
        "height": "50%",
        "id": "lblMapHeading",
        "isVisible": true,
        "left": "18.50%",
        "skin": "CopylblAmountCurrency0c837c10057bd49",
        "text": kony.i18n.getLocalizedString("i18n.mappageHeader"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "67%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnSearch = new kony.ui.Button({
        "focusSkin": "sknandroidBackButton",
        "height": "50%",
        "id": "btnSearch",
        "isVisible": true,
        "onClick": AS_Button_b7099d1415e24d9ba8a42a274a4a8009,
        "right": "0%",
        "skin": "sknandroidBackButton",
        "text": "h",
        "top": "0dp",
        "width": "14%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {});
    var searchContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "searchContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "20%",
        "isModalContainer": false,
        "top": "10dp",
        "width": "65%",
        "zIndex": 1
    }, {}, {});
    searchContainer.setDefaultUnit(kony.flex.DP);
    var locatorSearchTextField = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "focusSkin": "skntitleBarSearch",
        "height": "90%",
        "id": "locatorSearchTextField",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": 0,
        "onDone": AS_TextField_7e377cac57f84c878befa5270356f7ef,
        "onTextChange": AS_TextField_ab91688b2b5348c48760222fb58dbb3f,
        "placeholder": kony.i18n.getLocalizedString("i18n.locateus.ZipCodeOrCity"),
        "right": 0,
        "secureTextEntry": false,
        "skin": "skntitleBarSearch",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [15, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknPlaceHolder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var Image0bc597ce2b70244 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "50%",
        "id": "Image0bc597ce2b70244",
        "isVisible": true,
        "left": "4dp",
        "skin": "sknslImage",
        "src": "search_icon.png",
        "top": "0dp",
        "width": "30dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    searchContainer.add(locatorSearchTextField, Image0bc597ce2b70244);
    var cancelButton = new kony.ui.Button({
        "focusSkin": "skntitleBarTextButtonFocus",
        "height": "50dp",
        "id": "cancelButton",
        "isVisible": true,
        "minWidth": "50dp",
        "onClick": AS_Button_1d5966c5f19b416db5e133969c1a36e4,
        "right": "-15%",
        "skin": "snCancelLocator",
        "text": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "top": "0dp",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var MapListControllerWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "37dp",
        "id": "MapListControllerWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "flxBGKA",
        "top": "50dp",
        "width": "88%",
        "zIndex": 20
    }, {}, {});
    MapListControllerWrapper.setDefaultUnit(kony.flex.DP);
    var btnMapKA = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnBGWhiteBlue105Rd10",
        "height": "100%",
        "id": "btnMapKA",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_f8f5f87b783b455caa73172bd1d69462,
        "skin": "sknBtnBGWhiteBlue105Rd10",
        "text": kony.i18n.getLocalizedString("i18n.Map.Map"),
        "width": "55%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnListKA = new kony.ui.Button({
        "centerY": "50.00%",
        "focusSkin": "sknBtnBGWhiteBlue105Rd10",
        "height": "100%",
        "id": "btnListKA",
        "isVisible": true,
        "onClick": AS_Button_b415de7d36b640b980a9069e59bfb302,
        "right": "0%",
        "skin": "sknBtnBGBlueWhite105Rd10",
        "text": kony.i18n.getLocalizedString("i18n.locateus.list"),
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    MapListControllerWrapper.add(btnMapKA, btnListKA);
    titleBarLocator.add(flxBack, lblMapHeading, btnSearch, searchContainer, cancelButton, MapListControllerWrapper);
    var mainContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skntitleBarGradient",
        "top": "100dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var searchAccountFilters = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "7%",
        "id": "searchAccountFilters",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "sknFlxBGWhiteRD10",
        "top": "3%",
        "width": "92%",
        "zIndex": 2
    }, {}, {});
    searchAccountFilters.setDefaultUnit(kony.flex.DP);
    var btnBothKA = new kony.ui.Button({
        "focusSkin": "sknBtnBGWhiteBlue105Rd10",
        "height": "100%",
        "id": "btnBothKA",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_dcc31819d9d44ee6af9c16ca3f0cc8b0,
        "skin": "sknBtnBGBlueWhite105Rd10",
        "text": kony.i18n.getLocalizedString("i18n.Bene.All"),
        "top": "0dp",
        "width": "32%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [6, 0, 6, 0],
        "paddingInPixel": false
    }, {});
    var btnBranchKA = new kony.ui.Button({
        "focusSkin": "sknBtnBGWhiteBlue105Rd10",
        "height": "100%",
        "id": "btnBranchKA",
        "isVisible": true,
        "left": "1%",
        "onClick": AS_Button_62ada913c28c4564be79058f9b834dbe,
        "skin": "sknBtnBGWhiteBlue105Rd10",
        "text": kony.i18n.getLocalizedString("i18n.locateus.branch"),
        "top": "0dp",
        "width": "32%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [6, 0, 6, 0],
        "paddingInPixel": false
    }, {});
    var btnATMKA = new kony.ui.Button({
        "focusSkin": "sknBtnBGWhiteBlue105Rd10",
        "height": "100%",
        "id": "btnATMKA",
        "isVisible": true,
        "left": "1%",
        "onClick": AS_Button_89201458963f44e9849204e8f36ec007,
        "skin": "sknBtnBGWhiteBlue105Rd10",
        "text": kony.i18n.getLocalizedString("i18n.locateus.btnATM"),
        "top": "0dp",
        "width": "33%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [6, 0, 6, 0],
        "paddingInPixel": false
    }, {});
    searchAccountFilters.add(btnBothKA, btnBranchKA, btnATMKA);
    var locatorMapView = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "locatorMapView",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    locatorMapView.setDefaultUnit(kony.flex.DP);
    var locatorMap = new kony.ui.Map({
        "calloutTemplate": flxMapATMBranchWithoutLocation,
        "calloutWidth": 100,
        "defaultPinImage": "atm_icon_inactive.png",
        "height": "100%",
        "id": "locatorMap",
        "isVisible": true,
        "left": "0dp",
        "locationData": [{
            "desc": "206 Guadalupe St",
            "lat": "17.447326",
            "lon": "78.371358",
            "name": "Guadalupe ATM"
        }, {
            "desc": "2119 E Seventh St",
            "lat": "17.441839",
            "lon": "78.380928",
            "name": "East Seventh"
        }, {
            "desc": "Austin, TX 78701",
            "lat": "28.449340",
            "lon": "-81.481519",
            "name": "South Lamar ATM"
        }, {
            "desc": "Colorado, TX 78701",
            "lat": "28.449340",
            "lon": "-81.481519",
            "name": "Lake Austin"
        }, {
            "desc": "Alaska, TX 78701",
            "lat": "60.441839",
            "lon": "31.481519",
            "name": "Marcos Lavino"
        }],
        "onPinClick": AS_Map_2464ff6721d245329319c682ad6079e8,
        "onSelection": AS_Map_cce9580a9e3c4bfda9132e2fcbd13eb5,
        "provider": constants.MAP_PROVIDER_GOOGLE,
        "top": "0dp",
        "widgetDataMapForCallout": {
            "btnClose": "btnClose",
            "btnDirections": "btnDirections",
            "btnReserveQueue": "btnReserveQueue",
            "flxBottom": "flxBottom",
            "flxMapATMBranchWithoutLocation": "flxMapATMBranchWithoutLocation",
            "flxTop1": "flxTop1",
            "flxTopDetails": "flxTopDetails",
            "lblAddress": "lblAddress",
            "lblCarIcon": "lblCarIcon",
            "lblIcon1": "lblIcon1",
            "lblIcon2": "lblIcon2",
            "lblLat": "lblLat",
            "lblLong": "lblLong",
            "lblName": "lblName",
            "lblType": "lblType"
        },
        "width": "100%"
    }, {}, {
        "mode": constants.MAP_VIEW_MODE_NORMAL,
        "showZoomControl": true,
        "zoomLevel": 12
    });
    var mapCurrentLocationWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "3%",
        "clipBounds": false,
        "height": "60dp",
        "id": "mapCurrentLocationWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "5%",
        "skin": "slFbox",
        "width": "60dp",
        "zIndex": 1
    }, {}, {});
    mapCurrentLocationWrapper.setDefaultUnit(kony.flex.DP);
    var Image02e834c4c0b774d = new kony.ui.Image2({
        "centerX": "52%",
        "centerY": "52%",
        "height": "60dp",
        "id": "Image02e834c4c0b774d",
        "isVisible": true,
        "skin": "sknslImage",
        "src": "locate.png",
        "width": "60dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var currentLocationButton = new kony.ui.Button({
        "focusSkin": "sknCopyslButtonGlossBlue0f07d963e023c4d",
        "height": "100%",
        "id": "currentLocationButton",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_5c2a953fdcf140d8b887d19c16061eec,
        "skin": "sknCopyslButtonGlossBlue0773d36c6abc444",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    mapCurrentLocationWrapper.add(Image02e834c4c0b774d, currentLocationButton);
    var closeCalloutButton = new kony.ui.Button({
        "focusSkin": "skninvisibleButton",
        "height": "100%",
        "id": "closeCalloutButton",
        "isVisible": false,
        "left": "0dp",
        "onClick": AS_Button_27cf391d148f4e2183f6bf0e5c26f7ad,
        "skin": "skninvisibleButton",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var mapCalloutExample = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "160dp",
        "id": "mapCalloutExample",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "15%",
        "isModalContainer": false,
        "skin": "sknCopyslFbox033ff342629d94d",
        "top": "100dp",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    mapCalloutExample.setDefaultUnit(kony.flex.DP);
    var calloutCarrot = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "calloutCarrot",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "102dp",
        "isModalContainer": false,
        "skin": "skngenericCard",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    calloutCarrot.setDefaultUnit(kony.flex.DP);
    calloutCarrot.add();
    var calloutCardWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "140dp",
        "id": "calloutCardWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skngenericCard",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    calloutCardWrapper.setDefaultUnit(kony.flex.DP);
    var calloutLeftWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "calloutLeftWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox09caa3e3987dd40",
        "top": "0dp",
        "width": "35%"
    }, {}, {});
    calloutLeftWrapper.setDefaultUnit(kony.flex.DP);
    var Label0a12bd6aecff249 = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label0a12bd6aecff249",
        "isVisible": true,
        "skin": "sknCopyslLabel02567faa931c641",
        "text": "0.12",
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Label05d7e98b32dde4c = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label05d7e98b32dde4c",
        "isVisible": true,
        "skin": "sknCopyslLabel009743950851440",
        "text": kony.i18n.getLocalizedString("i18n.locateus.miles"),
        "top": "75dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    calloutLeftWrapper.add(Label0a12bd6aecff249, Label05d7e98b32dde4c);
    var calloutRightWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "60dp",
        "id": "calloutRightWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "35%",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "65%",
        "zIndex": 1
    }, {}, {});
    calloutRightWrapper.setDefaultUnit(kony.flex.DP);
    var Label0f31c1a03265843 = new kony.ui.Label({
        "id": "Label0f31c1a03265843",
        "isVisible": true,
        "left": "10%",
        "skin": "skn",
        "text": "Downtown Sixth",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0c58db39540364d = new kony.ui.Label({
        "id": "CopyLabel0c58db39540364d",
        "isVisible": true,
        "left": "10%",
        "skin": "sknlocatorListAddress",
        "text": "221 West Sixth St",
        "top": "25dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0045e6c9dee8243 = new kony.ui.Label({
        "id": "CopyLabel0045e6c9dee8243",
        "isVisible": true,
        "left": "10%",
        "skin": "sknlocatorListAddress",
        "text": "Austin, TX 78704",
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    calloutRightWrapper.add(Label0f31c1a03265843, CopyLabel0c58db39540364d, CopyLabel0045e6c9dee8243);
    var Button05baf58314fc446 = new kony.ui.Button({
        "focusSkin": "skninvisibleButtonFocus",
        "height": "100%",
        "id": "Button05baf58314fc446",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_bfd9032274c84fe99577677825415efe,
        "skin": "skninvisibleButton",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    calloutCardWrapper.add(calloutLeftWrapper, calloutRightWrapper, Button05baf58314fc446);
    mapCalloutExample.add(calloutCarrot, calloutCardWrapper);
    locatorMapView.add(locatorMap, mapCurrentLocationWrapper, closeCalloutButton, mapCalloutExample);
    var locatorListView = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0%",
        "clipBounds": true,
        "height": "87%",
        "id": "locatorListView",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "13%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    locatorListView.setDefaultUnit(kony.flex.DP);
    var dividerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "dividerKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "CopysknsegmentDivider0a71f9547429641",
        "top": "0%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    dividerKA.setDefaultUnit(kony.flex.DP);
    dividerKA.add();
    var locatorSegmentList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50.00%",
        "data": [{
            "btnAlpha": "B",
            "lblLocationName": "Bank of Jordan",
            "lblRightChevron": "Label"
        }],
        "groupCells": false,
        "height": "100%",
        "id": "locatorSegmentList",
        "isVisible": true,
        "needPageIndicator": true,
        "onRowClick": AS_Segment_1267dc6be4174eb1abe781c436283113,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowSkin": "segRowSkinMap",
        "rowTemplate": flxLocationDetails,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "1dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "btnAlpha": "btnAlpha",
            "flxLocationDetails": "flxLocationDetails",
            "lblLocationName": "lblLocationName",
            "lblRightChevron": "lblRightChevron"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var bottomListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "bottomListDivider",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "sknsegmentDivider",
        "top": "-1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    bottomListDivider.setDefaultUnit(kony.flex.DP);
    bottomListDivider.add();
    var noSearchResults = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "noSearchResults",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    noSearchResults.setDefaultUnit(kony.flex.DP);
    var CopyImage0c9980dffa3f24b = new kony.ui.Image2({
        "centerX": "50%",
        "height": "100dp",
        "id": "CopyImage0c9980dffa3f24b",
        "isVisible": true,
        "skin": "sknslImage",
        "src": "search_empty.png",
        "top": "60dp",
        "width": "100dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyLabel01cf823c2e98e4a = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel01cf823c2e98e4a",
        "isVisible": true,
        "left": 0,
        "skin": "sknnoSearchResultsText",
        "text": kony.i18n.getLocalizedString("i18n.locateus.enterZipCodeOrCity"),
        "top": "10dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var atmLink = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknsecondaryActionFocus",
        "id": "atmLink",
        "isVisible": true,
        "onClick": AS_Button_23cc519761fd4422990f8dc12182c016,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.locateus.atmLink"),
        "top": "20dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    noSearchResults.add(CopyImage0c9980dffa3f24b, CopyLabel01cf823c2e98e4a, atmLink);
    var LabelNoRecordsKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "35.52%",
        "id": "LabelNoRecordsKA",
        "isVisible": false,
        "left": "17.52%",
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.search.noSearchResults"),
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    locatorListView.add(dividerKA, locatorSegmentList, bottomListDivider, noSearchResults, LabelNoRecordsKA);
    var searchFocus = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "91%",
        "id": "searchFocus",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox061e616a387ca49",
        "top": "9%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    searchFocus.setDefaultUnit(kony.flex.DP);
    var searchCurrentLocationWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "searchCurrentLocationWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgBorderKA",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    searchCurrentLocationWrapper.setDefaultUnit(kony.flex.DP);
    var Image03409386204724e = new kony.ui.Image2({
        "centerY": "50%",
        "height": "25dp",
        "id": "Image03409386204724e",
        "isVisible": true,
        "left": "5%",
        "skin": "sknslImage",
        "src": "locator_icon_outline_blue.png",
        "width": "25dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyLabel05dfd79fd44784e = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopyLabel05dfd79fd44784e",
        "isVisible": true,
        "left": "18%",
        "skin": "skn",
        "text": kony.i18n.getLocalizedString("i18n.locateus.searchByCurrentLocation"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Button0de7cd327cd5942 = new kony.ui.Button({
        "focusSkin": "skninvisibleButtonGreyFocus",
        "height": "100%",
        "id": "Button0de7cd327cd5942",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_403e9f59368b4474a6d48ed926bbdbc5,
        "skin": "skninvisibleButtonNormal",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    searchCurrentLocationWrapper.add(Image03409386204724e, CopyLabel05dfd79fd44784e, Button0de7cd327cd5942);
    searchFocus.add(searchCurrentLocationWrapper);
    var locatorSegmentedControllerContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "locatorSegmentedControllerContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknCopyslFbox0428257c1718247",
        "top": "0dp",
        "width": "100%",
        "zIndex": 9999
    }, {}, {});
    locatorSegmentedControllerContainer.setDefaultUnit(kony.flex.DP);
    var androidSegmentedController = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "56dp",
        "id": "androidSegmentedController",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidSegmentedController.setDefaultUnit(kony.flex.DP);
    var btnATMFilterKA = new kony.ui.Button({
        "focusSkin": "sknaccountFilterButtonFocus",
        "height": "52dp",
        "id": "btnATMFilterKA",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_6daee20140ee4fc58eaeae274df45fe1,
        "skin": "sknandroidSegmentedTextInactive",
        "text": kony.i18n.getLocalizedString("i18n.locateus.btnATM"),
        "top": "0dp",
        "width": "33.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBothFilterKA = new kony.ui.Button({
        "focusSkin": "sknaccountFilterButtonFocus",
        "height": "52dp",
        "id": "btnBothFilterKA",
        "isVisible": true,
        "left": "33.30%",
        "onClick": AS_Button_6ecb94c82db44686b648dc909710da12,
        "skin": "sknandroidSegmentedTextActive",
        "text": kony.i18n.getLocalizedString("i18n.locateus.btnBoth"),
        "top": "0dp",
        "width": "33.40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [6, 0, 6, 0],
        "paddingInPixel": false
    }, {});
    var btnBranchFilterKA = new kony.ui.Button({
        "focusSkin": "sknaccountFilterButtonFocus",
        "height": "52dp",
        "id": "btnBranchFilterKA",
        "isVisible": true,
        "left": "66.70%",
        "onClick": AS_Button_07aed51d4f4e46389a5cc0a322740c05,
        "skin": "sknandroidSegmentedTextInactive",
        "text": kony.i18n.getLocalizedString("i18n.locateus.btnBranch"),
        "top": "0dp",
        "width": "33.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [6, 0, 6, 0],
        "paddingInPixel": false
    }, {});
    var flxSelectedIndicatorKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "5dp",
        "clipBounds": true,
        "height": "5dp",
        "id": "flxSelectedIndicatorKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "33.30%",
        "isModalContainer": false,
        "skin": "sknCopyslFbox031e643cbc0cd4b",
        "width": "33.30%",
        "zIndex": 1
    }, {}, {});
    flxSelectedIndicatorKA.setDefaultUnit(kony.flex.DP);
    flxSelectedIndicatorKA.add();
    androidSegmentedController.add(btnATMFilterKA, btnBothFilterKA, btnBranchFilterKA, flxSelectedIndicatorKA);
    locatorSegmentedControllerContainer.add(androidSegmentedController);
    var CopydividerKA0ef8193c8f69747 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "CopydividerKA0ef8193c8f69747",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 0,
        "skin": "CopysknsegmentDivider0a71f9547429641",
        "top": "0%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    CopydividerKA0ef8193c8f69747.setDefaultUnit(kony.flex.DP);
    CopydividerKA0ef8193c8f69747.add();
    var lblNoSearchResults = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "30%",
        "height": "10%",
        "id": "lblNoSearchResults",
        "isVisible": false,
        "skin": "CopylblAmountCurrency0c837c10057bd49",
        "text": kony.i18n.getLocalizedString("i18n.search.noSearchResults"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    mainContent.add(searchAccountFilters, locatorMapView, locatorListView, searchFocus, locatorSegmentedControllerContainer, CopydividerKA0ef8193c8f69747, lblNoSearchResults);
    frmLocatorKA.add(titleBarLocator, mainContent);
};

function frmLocatorKAGlobals() {
    frmLocatorKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmLocatorKA,
        "bounces": true,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmLocatorKA",
        "init": AS_Form_4745b0cf680149a293d213e619aa1edf,
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_ce69880089804585826dada9ae7dd2a7,
        "preShow": function(eventobject) {
            AS_Form_j2ac951601904b16813a12e79ec1db74(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_365f6b1c8dda439b9d358e885575134f,
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};