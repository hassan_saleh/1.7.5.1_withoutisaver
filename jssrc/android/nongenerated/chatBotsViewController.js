kony = kony || {};
/**
 *chatbots namespace
 */
kony.chatbots = kony.chatbots || {};
/**
 *bot namespace in chatbots
 */
kony.chatbots.bot = kony.chatbots.bot || {};
/**
 *user namespace in chatbots
 */
kony.chatbots.user = kony.chatbots.user || {};
/**
 *util namespace in chatbots
 */
kony.chatbots.util = kony.chatbots.util || {};
//variable declaration
kony.chatbots.bot.icon = "bot.png";
kony.chatbots.bot.isLoadingScreenOn = false;
kony.chatbots.bot.previousForm = "";
/**
 *this function adds the bot icon and message
 * @param {string} message- the text to be displayed
 */
kony.chatbots.bot.addMessageWithIcon = function(message) {
    try {
        kony.print("$$-- start bot.addMessageWithIcon function --$$");
        if (message !== null && message !== undefined && kony.chatbots.bot.icon) {
            kony.print("$$-- got the message " + JSON.stringify(message) + "--$$");
            this.addIcon();
            var isIconAdded = true;
            this.addMessage(isIconAdded, message);
        }
        kony.print("$$-- end bot.addMessageWithIcon function --$$");
    } catch (err) {
        kony.print("$$-- in catch of bot addMessageWithIcon. error message is :" + err + "--$$");
    }
};
/**
 *this function adds the bot icon
 */
kony.chatbots.bot.addIcon = function() {
    try {
        kony.print("$$-- start bot addIcon function --$$");
        var IMAGE = new kony.ui.Image2({
            "id": "botIcon" + kony.chatbots.util.getRandomString(),
            "left": "1dp",
            "top": "3%",
            "height": "30dp",
            "width": "30dp",
            "isVisible": true,
            "src": kony.chatbots.bot.icon
        }, {}, {});
        frmChatBots.flexScrollChatBox.add(IMAGE);
        frmChatBots.flexScrollChatBox.scrollToEnd();
        kony.print("$$-- end bot addIcon function --$$");
    } catch (err) {
        kony.print("in catch of bot addIcon with error message : " + err);
    }
};
/**
 *this function adds the bot message
 * @param {string} message- the text to be displayed
 * @param {boolean} isIconAdded - this is useful to adjust the height of the bot text.
 */
kony.chatbots.bot.addMessage = function(isIconAdded, message) {
    try {
        kony.print("$$--start bot addMessage function --$$");
        var msgTop = "";
        kony.print("$$-- isIconAdded value " + isIconAdded + "--$$");
        if (isIconAdded === true) {
            msgTop = "-29dp";
        } else {
            msgTop = "1%";
        }
        var MESSAGE = new kony.ui.Label({
            "id": "botMessage" + kony.chatbots.util.getRandomString(),
            "text": message,
            "left": "33dp",
            "top": msgTop,
            "width": "preferred",
            "maxWidth": "240dp",
            "isVisible": true,
            "skin": "sknBotText",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "zindex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        frmChatBots.flexScrollChatBox.add(MESSAGE);
        frmChatBots.flexScrollChatBox.scrollToEnd();
        kony.print("$$--end bot addMessage function --$$");
    } catch (err) {
        kony.print("$$-- in catch of bot addeMessage with error message " + err);
    }
};
kony.chatbots.bot.addMessageAt = function(isIconAdded, message, index) {
    try {
        kony.print("$$--start bot addMessage function --$$");
        var msgTop = "";
        kony.print("$$-- isIconAdded value " + isIconAdded + "--$$");
        if (isIconAdded === true) {
            msgTop = "-29dp";
        } else {
            msgTop = "1%";
        }
        var MESSAGE = new kony.ui.Label({
            "id": "botMessage" + kony.chatbots.util.getRandomString(),
            "text": message,
            "left": "33dp",
            "top": msgTop,
            "width": "preferred",
            "maxWidth": "240dp",
            "isVisible": true,
            "skin": "sknBotText",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "zindex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        frmChatBots.flexScrollChatBox.addAt(MESSAGE, index);
        //frmChatBots.flexScrollChatBox.scrollToEnd();
        kony.print("$$--end bot addMessage function --$$");
    } catch (err) {
        kony.print("$$-- in catch of bot addeMessage with error message " + err);
    }
};
kony.chatbots.bot.addAccountBalance = function(row1, row2) {
    try {
        row2 = "Available Balance : " + row2;
        var msg = row1 + " \n" + row2;
        kony.chatbots.bot.addMessageWithIcon(msg);
    } catch (err) {
        kony.print(err);
    }
};
/**
 *this function adds the time on bot side
 */
kony.chatbots.bot.addTime = function() {
    try {
        var currentTime = kony.chatbots.util.getTime();
        kony.chatbots.addTime("bot", currentTime);
    } catch (err) {}
};
/**
 *this function adds action button on bot side
 * @param {array} data
 * data must be in format of
   data = [
   {
   "text" : "the text for button",
   "onClickCallback" : "function to call after clickingperticulat button (OPTIONAL)
   "parameter" : "parameter to pass to the above callback(OPTIONAL)
   }
   ]
*/
kony.chatbots.bot.addButtons = function(data) {
    try {
        var length = data.length;
        var maxLength = 40;
        var currentLength = 0;
        var temp = [];
        for (var i = 0; i < data.length; i++) {
            currentLength += data[i].text.length + 2;
            if (currentLength <= maxLength) {
                temp.push(data[i]);
            } else {
                if (temp.length > 0) {
                    kony.chatbots.bot.addButtonsHelper(temp, i + "");
                    currentLength = data[i].text.length + 2;
                    temp = [];
                    temp.push(data[i]);
                } else {
                    temp.push(data[i]);
                }
            }
        }
        if (temp.length > 0) {
            kony.chatbots.bot.addButtonsHelper(temp, "end");
        }
    } catch (err) {
        kony.print(err + JSON.stringify(err));
    }
};
kony.chatbots.bot.addButtonsHelper = function(data, tail) {
    try {
        var buttonLayOut = {
            "margin": [2, 2, 2, 2],
            "contentAlignment": constants.CONTENT_ALIGN_CENTER
        };
        var HorizontalFlex = new kony.ui.FlexContainer({
            "id": "botHorizontalFlex" + tail + kony.chatbots.util.getRandomString(),
            "left": "31dp",
            "top": "4dp",
            "wigth": "90%",
            "height": "40dp",
            "zIndex": 1,
            "isVisible": true,
            "clipBounds": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL
        }, {
            "vExpand": false
        }, {});
        HorizontalFlex.setDefaultUnit(kony.flex.DP);
        for (var i = 0; i < data.length; i++) {
            var BUTTON = new kony.ui.Button({
                "id": "botButton" + i + "" + kony.chatbots.util.getRandomString(),
                "text": data[i].text,
                "left": "4dp",
                "height": "40dp",
                "width": "preferred",
                "top": "0%",
                "skin": "sknBotButton",
                "focusSkin": "sknBotButtonFocus",
                "zIndex": 10,
                "onClick": kony.chatbots.bot.buttonClickHandler.bind(this, data[i])
            }, buttonLayOut, {});
            HorizontalFlex.add(BUTTON);
        }
        frmChatBots.flexScrollChatBox.add(HorizontalFlex);
        frmChatBots.flexScrollChatBox.scrollToEnd();
    } catch (err) {
        kony.print(err);
    }
};
kony.chatbots.bot.buttonClickHandler = function(data, object) {
    if (kony.chatbots.bot.isLoadingScreenOn === false) {
        var callback = data.onClickCallback;
        if (callback) {
            callback(data.parameter, data.text);
        }
    }
};
kony.chatbots.bot.clearChatBox = function() {
    try {
        frmChatBots.flexScrollChatBox.removeAll();
    } catch (err) {}
};
kony.chatbots.bot.showLoading = function() {
    try {
        kony.chatbots.bot.isLoadingScreenOn = true;
        frmChatBots.flxVoice.setEnabled(false);
        this.addIcon();
        var IMAGE = new kony.ui.Image2({
            "id": "botLoading" + kony.chatbots.util.getRandomString(),
            "isVisible": true,
            "src": "botload.gif",
            "left": "35dp",
            "top": "-29dp",
            "height": "28dp",
            "width": "75dp"
        }, {
            imageScaleMode: constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS
        }, {});
        frmChatBots.flexScrollChatBox.add(IMAGE);
        frmChatBots.flexScrollChatBox.scrollToEnd();
    } catch (err) {
        kony.print(err + JSON.stringify(err));
    }
};
kony.chatbots.bot.dismissLoading = function() {
    try {
        if (kony.chatbots.bot.isLoadingScreenOn) {
            var widgets = frmChatBots.flexScrollChatBox.widgets();
            var widgetsLength = widgets.length;
            frmChatBots.flxVoice.setEnabled(true);
            frmChatBots.flexScrollChatBox.removeAt(widgetsLength - 1);
            frmChatBots.flexScrollChatBox.removeAt(widgetsLength - 2);
            kony.chatbots.bot.isLoadingScreenOn = false;
        }
    } catch (err) {}
};
kony.chatbots.bot.clean = function() {
    try {
        frmChatBots.flexScrollChatBox.removeAll();
        frmChatBots.txtBoxUserText.text = "";
        (new kony.chatbotPresentation()).clean();
    } catch (err) {}
};
kony.chatbots.bot.backToChatBot = function() {
    try {
        kony.retailBanking.globalData.fromChat = false;
        //var message = "We’re back! - Can I help you with anything else?";
        //this.addMessageWithIcon(message);
        //this.addTime();
        //kony.chatbotPresentation.lastTyped = "bot";
        frmChatBots.show();
    } catch (err) {}
};
kony.chatbots.bot.showBotSplash = function(previousForm) {
    //   try{
    //     kony.chatbots.bot.previousForm = previousForm;
    //     frmBotSplash.show();
    //     (new kony.chatbotPresentation()).firstTimeOpen();
    //   }catch(err){
    //   }
};
kony.chatbots.bot.addAtmDetailFlex = function(data, index) {
    try {
        var MainFlex = kony.chatbots.bot.getAtmDetailMainFlex();
        var flexLeft = kony.chatbots.bot.getAtmDetailLeftFlex(data);
        var flexSeperator = kony.chatbots.bot.getAtmDetailVerticalSeperatorFlex();
        var flexRight = kony.chatbots.bot.getAtmDetailRightFlex(data);
        MainFlex.add(flexLeft, flexSeperator, flexRight);
        if (index) {
            frmChatBots.flexScrollChatBox.addAt(MainFlex, index);
        } else {
            frmChatBots.flexScrollChatBox.add(MainFlex);
            frmChatBots.flexScrollChatBox.scrollToEnd();
        }
    } catch (err) {}
};
kony.chatbots.bot.getAtmDetailMainFlex = function() {
    try {
        var MainFlex = new kony.ui.FlexContainer({
            "id": "MainFlexAtmDetails",
            "top": "3dp",
            "left": "33dp",
            "width": "75%",
            "height": "90dp",
            "zIndex": 1,
            "skin": "sknBotAtmListMainFlex",
            "isVisible": true,
            "clipBounds": true,
            "layoutType": kony.flex.FREE_FORM
        }, {}, {});
        MainFlex.setDefaultUnit(kony.flex.DP);
        return MainFlex;
    } catch (err) {}
};
kony.chatbots.bot.getAtmDetailLeftFlex = function(data) {
    try {
        var leftFlex = new kony.ui.FlexContainer({
            "id": "leftFlex",
            "top": "0dp",
            "left": "0dp",
            "width": "74%",
            "height": "90dp",
            "zIndex": 1,
            "skin": "",
            "onClick": this.onClickOfAtmDetailsLeftFlex.bind(this, data),
            "isVisible": true,
            "clipBounds": true,
            "layoutType": kony.flex.FLOW_VERTICAL
        }, {}, {});
        leftFlex.setDefaultUnit(kony.flex.DP);
        var lblAtmName = kony.chatbots.bot.getLabelAtmName(data.informationTitle);
        var lblAtmAddress = kony.chatbots.bot.getLabelAtmAddress(data.addressLine2);
        var lblDistanceAndStatus = kony.chatbots.bot.getLabelAtmDistnceAndStatus("", data.status);
        leftFlex.add(lblAtmName, lblAtmAddress, lblDistanceAndStatus);
        return leftFlex;
    } catch (err) {}
};
kony.chatbots.bot.getLabelAtmName = function(msg) {
    try {
        var text = msg;
        var skin = "sknBotAtmName";
        var top = "10dp";
        var maxNoOfLines = 1;
        var lblAtmName = kony.chatbots.bot.getAtmLabel(text, skin, top, maxNoOfLines);
        return lblAtmName;
    } catch (err) {}
};
kony.chatbots.bot.getLabelAtmAddress = function(msg) {
    try {
        var text = msg;
        var skin = "sknBotAtmAddress";
        var top = "5dp";
        var maxNoOfLines = 2;
        var lblAtmAddress = kony.chatbots.bot.getAtmLabel(text, skin, top, maxNoOfLines);
        return lblAtmAddress;
    } catch (err) {}
};
kony.chatbots.bot.getLabelAtmDistnceAndStatus = function(distance, status) {
    try {
        var text = "";
        if (distance) {
            text = distance + "    ";
        }
        text = text + status;
        var skin = "sknBotAtmStatus";
        var top = "5dp";
        var maxNoOfLines = 1;
        var lblAtmDistanceAndStatus = kony.chatbots.bot.getAtmLabel(text, skin, top, maxNoOfLines);
        return lblAtmDistanceAndStatus;
    } catch (err) {}
};
kony.chatbots.bot.getAtmLabel = function(text, skin, top, maxNoOfLines) {
    try {
        var AtmLabel = new kony.ui.Label({
            "id": "AtmLabel",
            "height": "preferred",
            "left": "5%",
            "top": top,
            "width": "94%",
            "text": text,
            "isVisible": true,
            "skin": skin,
            "zIndex": 1,
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "maxNumberOfLines": maxNoOfLines,
            "textTruncatePosition": constants.TEXT_TRUNCATE_END
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        return AtmLabel;
    } catch (err) {}
};
kony.chatbots.bot.getAtmDetailVerticalSeperatorFlex = function() {
    try {
        var flex = new kony.ui.FlexContainer({
            "id": "seperator" + kony.chatbots.util.getRandomString(),
            "top": "0dp",
            "left": "74%",
            "width": "2dp",
            "height": "100%",
            "zIndex": 1,
            "skin": "sknBotAtmListVerticalSeperator",
            "isVisible": true,
            "clipBounds": true
        }, {}, {});
        flex.setDefaultUnit(kony.flex.DP);
        return flex;
    } catch (err) {
        alert(err);
    }
};
kony.chatbots.bot.getAtmDetailRightFlex = function(data) {
    try {
        var rightFlex = new kony.ui.FlexContainer({
            "id": "atmRightFlex" + kony.chatbots.util.getRandomString(),
            "top": "0dp",
            "left": "76%",
            "width": "24%",
            "height": "90dp",
            "zIndex": 1,
            "skin": "",
            "isVisible": true,
            "onClick": this.onClickOfRightFlexInAtmsDisplay.bind(this, data),
            "clipBounds": true,
            "layoutType": kony.flex.FREE_FORM
        }, {}, {});
        rightFlex.setDefaultUnit(kony.flex.DP);
        var IMAGE = new kony.ui.Image2({
            "id": "imageMap",
            "isVisible": true,
            "src": "botmap.png",
            "height": "50dp",
            "width": "40dp",
            "centerX": "50%",
            "centerY": "50%"
                //"imageScaleMode":constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO
        }, {}, {});
        rightFlex.add(IMAGE);
        return rightFlex;
    } catch (err) {
        kony.print(err);
    }
};
kony.chatbots.bot.onClickOfAtmDetailsLeftFlex = function(data) {
    try {
        if (kony.chatbots.bot.isLoadingScreenOn === false) {
            (new kony.chatbotPresentation()).navigateToATMDetailsScreen(data);
        }
    } catch (err) {}
};
kony.chatbots.bot.onClickOfRightFlexInAtmsDisplay = function(data) {
    try {
        if (kony.chatbots.bot.isLoadingScreenOn === false) {
            (new kony.chatbotPresentation()).navigateToMapWithOneRecord(data);
        }
    } catch (err) {
        kony.print(err);
    }
};
kony.chatbots.bot.addMoreItemsFlexForMaps = function(data, text, index) {
    try {
        var widgets = frmChatBots.flexScrollChatBox.widgets();
        var length = 0;
        if (index) {
            length = index;
        } else {
            if (widgets) {
                length = widgets.length;
            }
        }
        var MoreFLEX = new kony.ui.FlexContainer({
            "id": "FLEXMOREITEMS" + kony.chatbots.util.getRandomString(),
            "top": "4dp",
            "left": "33dp",
            "width": "75%",
            "height": "80dp",
            "zIndex": 1,
            "skin": "sknFlxBotItemsMore",
            "onClick": this.onClickMapHandler.bind(this, data, length),
            "isVisible": true,
            "clipBounds": true,
            "layoutType": kony.flex.FREE_FORM
        }, {}, {});
        MoreFLEX.setDefaultUnit(kony.flex.DP);
        var MESSAGE = new kony.ui.Label({
            "id": "Message" + kony.chatbots.util.getRandomString(),
            "text": text,
            "left": "5dp",
            "top": "0dp",
            "width": "70%",
            "height": "100%",
            "centerX": "50%",
            "CenterY": "50%",
            "isVisible": true,
            "skin": "SknBotMoreItemsText",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "zindex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        MoreFLEX.add(MESSAGE);
        if (index) {
            frmChatBots.flexScrollChatBox.addAt(MoreFLEX, index);
        } else {
            frmChatBots.flexScrollChatBox.add(MoreFLEX);
            frmChatBots.flexScrollChatBox.scrollToEnd();
        }
    } catch (err) {
        kony.print(err);
    }
};
kony.chatbots.bot.addMoreItemsForAccountBalance = function(data, text, noOfRecordsDisplayed, index) {
    try {
        var widgets = frmChatBots.flexScrollChatBox.widgets();
        var length = 0;
        if (index) {
            length = index;
        } else {
            if (widgets) {
                length = widgets.length;
            }
        }
        var MoreFLEX = new kony.ui.FlexContainer({
            "id": "FLEXMOREITEMS" + kony.chatbots.util.getRandomString(),
            "top": "4dp",
            "left": "33dp",
            "width": "55%",
            "height": "40dp",
            "zIndex": 1,
            "skin": "sknFlxBotItemsMore",
            "onClick": this.onClickAccountBalanceHandler.bind(this, data, noOfRecordsDisplayed, length),
            "isVisible": true,
            "clipBounds": true,
            "layoutType": kony.flex.FREE_FORM
        }, {}, {});
        MoreFLEX.setDefaultUnit(kony.flex.DP);
        var MESSAGE = new kony.ui.Label({
            "id": "Message" + kony.chatbots.util.getRandomString(),
            "text": text,
            "left": "5dp",
            "top": "0dp",
            "width": "70%",
            "height": "100%",
            "centerX": "50%",
            "CenterY": "50%",
            "isVisible": true,
            "skin": "SknBotMoreItemsText",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "zindex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        MoreFLEX.add(MESSAGE);
        if (index) {
            frmChatBots.flexScrollChatBox.addAt(MoreFLEX, index);
        } else {
            frmChatBots.flexScrollChatBox.add(MoreFLEX);
            frmChatBots.flexScrollChatBox.scrollToEnd();
        }
    } catch (err) {
        alert(err);
    }
};
kony.chatbots.bot.onClickAccountBalanceHandler = function(data, noOfRecordsDisplayed, index) {
    try {
        (new kony.chatbotPresentation()).onClickMoreAccountBalanceHandler(data, noOfRecordsDisplayed, index);
    } catch (err) {}
};
kony.chatbots.bot.addClickableMapFlex = function(data, index) {
    try {
        var widgets = frmChatBots.flexScrollChatBox.widgets();
        var length = 0;
        if (index) {
            length = index;
        } else {
            if (widgets) {
                length = widgets.length;
            }
        }
        var FLEX = new kony.ui.FlexContainer({
            "id": "FLEXATMClick" + kony.chatbots.util.getRandomString(),
            "top": "4dp",
            "left": "33dp",
            "width": "75%",
            "height": "80dp",
            "zIndex": 1,
            "skin": "sknBotAtmClickableFlex",
            "onClick": this.onClickMapHandler.bind(this, data, length),
            "isVisible": true,
            "clipBounds": true,
            "layoutType": kony.flex.FREE_FORM
        }, {}, {});
        FLEX.setDefaultUnit(kony.flex.DP);
        var displayMsg = "Go to maps" + "\n" + "for more options";
        var MESSAGE = new kony.ui.Label({
            "id": "Message" + kony.chatbots.util.getRandomString(),
            "text": displayMsg,
            "left": "5dp",
            "top": "0dp",
            "width": "70%",
            "height": "100%",
            "isVisible": true,
            "skin": "sknBotAtmGotoMaps",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "zindex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var IMAGE = new kony.ui.Image2({
            "id": "imageMap",
            "isVisible": true,
            "src": "botmap.png",
            "left": "80%",
            "width": "16%",
            "height": "50dp",
            "centerY": "50%"
        }, {}, {});
        FLEX.add(MESSAGE, IMAGE);
        if (index) {
            frmChatBots.flexScrollChatBox.addAt(FLEX, index);
        } else {
            frmChatBots.flexScrollChatBox.add(FLEX);
            frmChatBots.flexScrollChatBox.scrollToEnd();
        }
    } catch (err) {
        kony.print(err);
    }
};
kony.chatbots.bot.onClickMapHandler = function(data, index) {
    try {
        var callback = data.callback;
        var locationData = data.locationData;
        var noOfRecordsDisplayed = data.noOfRecordsDisplayed;
        if (callback) {
            callback(locationData, noOfRecordsDisplayed, index);
        }
    } catch (err) {}
};
/**
 *this function adds the user message
 * @param {string} message- the text to be displayed
 */
kony.chatbots.user.addMessage = function(message) {
    try {
        kony.print("$$-- in adding Message for user --$$");
        var MESSAGE = new kony.ui.Label({
            "id": "userMessage" + kony.chatbots.util.getRandomString(),
            "text": message,
            "right": "3%",
            "top": "3%",
            "width": "preferred",
            "maxWidth": "225dp",
            "isVisible": true,
            "skin": "sknUserText",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "zindex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        frmChatBots.flexScrollChatBox.add(MESSAGE);
        frmChatBots.flexScrollChatBox.scrollToEnd();
    } catch (err) {
        kony.print("in catch of user addMessage with error message :" + err);
    }
};
/**
 *this function adds the time on user side
 */
kony.chatbots.user.addTime = function() {
    try {
        var currentTime = kony.chatbots.util.getTime();
        kony.chatbots.addTime("user", currentTime);
    } catch (err) {}
};
/**
 *this generic function to add time based on type
 * @param {string} type- user or bot
 * @param {string} currentTime - like "03:12 P.M"
 */
kony.chatbots.addTime = function(type, currentTime) {
    try {
        var left = "";
        var right = "";
        if (type === "bot") {
            left = "37dp";
            right = "default";
        }
        if (type === "user") {
            left = "default";
            right = "15dp";
        }
        var TIME = new kony.ui.Label({
            "id": "botOrUserTime" + kony.chatbots.util.getRandomString(),
            "text": currentTime,
            "left": left,
            "right": right,
            "top": "2dp",
            "width": "preferred",
            "height": "preferred",
            "maxWidth": "200dp",
            "isVisible": true,
            "skin": "sknBotUserTime",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        frmChatBots.flexScrollChatBox.add(TIME);
        frmChatBots.flexScrollChatBox.scrollToEnd();
    } catch (err) {}
};
/**
 *this function return the current time in required format
 * @output-param {string} format-"03:12 P.M"
 */
kony.chatbots.util.getTime = function() {
    try {
        var date = new Date();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var tailString = "A.M.";
        if (hours >= 0 && hours < 12) {
            tailString = "A.M.";
        }
        if (hours === 12) {
            tailString = "P.M.";
        }
        if (hours > 12 && hours < 24) {
            hours = hours - 12;
            tailString = "P.M.";
        }
        hours = (String(hours).length > 1) ? (hours) : ("0" + hours);
        minutes = (String(minutes).length > 1) ? (minutes) : ("0" + minutes);
        var timeString = hours + ":" + minutes + " " + tailString;
        return timeString;
    } catch (err) {}
};
kony.chatbots.util.getRandomString = function() {
    try {
        var random = Math.random();
        random = String(random);
        if (random) {
            random = random.split(".")[1];
        }
        return random;
    } catch (err) {}
};
kony.chatbots.user.onTextChangeOfUserTextField = function() {
    try {
        var userText = frmChatBots.txtBoxUserText.text;
        if (userText && userText.length > 0) {
            this.changeVoiceImageToEnter();
        } else {
            if (userText === null || userText.length === 0) {
                kony.chatbots.user.changeEnterImageToVoice();
            }
        }
    } catch (err) {
        kony.print(err + JSON.stringify(err));
    }
};
kony.chatbots.user.changeVoiceImageToEnter = function() {
    try {
        frmChatBots.imgUserAction.src = "send.png";
    } catch (err) {}
};
kony.chatbots.user.changeEnterImageToVoice = function() {
    try {
        frmChatBots.imgUserAction.src = "mic.png";
    } catch (err) {}
};
kony.chatbots.user.onClickClose = function() {
    try {
        var previousForm = kony.chatbots.bot.previousForm;
        if (previousForm !== null && previousForm !== undefined) {
            previousForm.show();
            kony.chatbots.bot.clean();
        }
    } catch (err) {}
};
kony.chatbots.user.onClickEnter = function() {
    try {
        var userText = frmChatBots.txtBoxUserText.text;
        if (userText !== null && userText !== undefined && userText.length > 0) {
            kony.chatbotPresentation.lastTyped = "user";
            frmChatBots.txtBoxUserText.text = "";
            this.changeEnterImageToVoice();
            (new kony.chatbotPresentation()).predictString(null, userText);
            kony.chatbots.user.addMessage(userText);
            kony.chatbots.user.addTime();
            kony.chatbots.bot.showLoading();
        } else {
            toastMsg.showToastMsg("This feature is not available", 3000);
        }
    } catch (err) {
        kony.print(err);
    }
};

function onClickButnBot() {
    var text = frmChatBots.txtBoxUserText.text;
    var text1 = text.split("-");
    text = text1[0];
    if (text === "bot") {
        kony.chatbots.bot.addMessageWithIcon(text1[1]);
    }
    if (text === "user") {
        kony.chatbots.user.addMessage(text1[1]);
    }
    if (text === "ut") {
        kony.chatbots.user.addTime();
    }
    if (text === "bt") {
        kony.chatbots.bot.addTime();
    }
    if (text === "btn") {
        var data = [{
            "text": "View Balance",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "Transfer Money",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "ATM",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "Apply for credit card",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "NewUser Onboarding",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "Block Card",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "Make Bill Payment",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "need Loan",
            "onClickCallback": "",
            "parameter": true
        }];
        kony.chatbots.bot.addButtons(data);
    }
    if (text === "load") {
        kony.chatbots.bot.showLoading();
    }
    if (text === "dis") {
        kony.chatbots.bot.dismissLoading();
    }
}
kony.chatbots.user.onSwipeCancelTransaction = function(widget, gestureInfo, context) {
    try {
        frmChatBots.flxCancelTransaction.left = "-75%";
        alert("done");
    } catch (err) {}
};
flag = 0;
var start = 0;

function onTouchStart1(eventobject, x, y) {
    flag = 1;
    start = x;
    frmChatBots.txtBoxUserText.setEnabled(false);
}

function getValue(val) {
    return Number(val.substring(0, val.length - 1));
}
var dummy = -100;
var previousLeft = -100;

function onTouchMove1(eventobject, x, y) {
    var width = kony.os.deviceInfo().screenWidth;
    var per = ((x - start) / width) * 100;
    previousLeft = getValue(frmChatBots.flxUserInput.left);
    if (Number(per) > 10) {
        frmChatBots.flxUserInput.left = (previousLeft + per) + "%";
        start = x;
        frmChatBots.flxUserInput.forceLayout();
    }
}

function onTouchEnd1(widget, x, y) {
    flag = 0;
    var width = kony.os.deviceInfo().screenWidth;
    var per = ((x - start) / width) * 100;
    var val = getValue(frmChatBots.flxUserInput.left);
    if (val + 60 >= 0) {
        frmChatBots.flxUserInput.left = "-100%";
        frmChatBots.flxUserInput.forceLayout();
        (new kony.chatbotPresentation()).predictString(null, "cancel");
        kony.chatbots.user.addMessage("Cancel");
        kony.chatbots.user.addTime();
        kony.chatbots.bot.showLoading();
        kony.chatbotPresentation.lastTyped = "user";
    } else {
        var dum1 = val + 100;
        if (dum1 < 10) {
            frmChatBots.flxUserInput.left = "-100%";
            frmChatBots.flxUserInput.forceLayout();
            frmChatBots.txtBoxUserText.setEnabled(true);
            frmChatBots.txtBoxUserText.setFocus(true);
        } else {
            frmChatBots.flxUserInput.left = "-100%";
            frmChatBots.flxUserInput.forceLayout();
        }
    }
}