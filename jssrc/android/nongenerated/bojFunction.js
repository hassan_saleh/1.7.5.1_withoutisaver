kony = kony || {};
kony.boj = kony.boj || {};
//Red ,Orange,skyblue,,Blue,Green,purple
kony.boj.populateSuccessScreen = function(imgSrc, title, desc, btnText, btnClick, btnSecText, btnSecClick, btnThirdText, btnThirdClick, refNum) {
    frmCongratulations.imgCongratulations.src = imgSrc;
    frmCongratulations.lblCongratulations.text = title;
    frmCongratulations.lblMessage.text = desc;
    frmCongratulations.btnScreen.text = btnText;
    frmCongratulations.btnScreen.onClick = function() {
        gotoPage(btnClick);
    };
    if (btnSecText === undefined || btnSecText === "") frmCongratulations.btnSecondary.isVisible = false;
    else {
        frmCongratulations.btnSecondary.isVisible = true;
        frmCongratulations.btnSecondary.text = btnSecText;
        frmCongratulations.btnSecondary.onClick = function() {
            gotoPage(btnSecClick);
        };
    }
    if (btnThirdText === undefined || btnThirdText === "") frmCongratulations.btnThree.isVisible = false;
    else {
        frmCongratulations.btnThree.isVisible = true;
        frmCongratulations.btnThree.text = btnThirdText;
        frmCongratulations.btnThree.onClick = function() {
            gotoPage(btnThirdClick);
        };
    }
    if (refNum === undefined || refNum === "") frmCongratulations.lblRefNumber.isVisible = false;
    else {
        frmCongratulations.lblRefNumber.isVisible = true;
        frmCongratulations.lblRefNumber.text = refNum;
    }
    frmCongratulations.show();
};

function gotoPage(page) {
    gblsubaccBranchList = false;
    gblrequestStatementBranch = false;
    gblRequestNewPinBranchList = false;
    gblRequestStatementCrediCards = false;
    switch (page) {
        case "Login":
            if (isLoggedIn) kony.sdk.mvvm.LogoutAction();
            else frmLoginKA.show();
            break;
        case "Beneficiary":
            kony.boj.detailsForBene.flag = false;
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var listController = INSTANCE.getFormController("frmAddExternalAccountKA");
            var navObject = new kony.sdk.mvvm.NavigationObject();
            navObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
            navObject.setRequestOptions("form", {
                "headers": {}
            });
            listController.performAction("navigateTo", ["frmAddExternalAccountKA", navObject]);
            break;
        case "SendMoney":
            kony.timer.cancel("mytime21");
            check_numberOfAccounts();
            frmNewTransferKA.show();
            break;
        case "payapplink":
            if (isLoggedIn) kony.sdk.mvvm.LogoutAction();
            else {
                frmLoginKA.show();
            }
            gblPayAppFlow = false;
            gblLaunchModeOBJ.lauchMode = false;
            break;
        case "CliqDashboard":
            ResetFormIPSData();
            frmIPSHome.show();
            break;
        case "CliqDashboardHome":
            frmIPSHome.show();
            frmIPSRegistration.destroy();
            break;
        case "CliqDashboardManage":
            frmIPSManageBene.show();
            frmIPSRegistration.destroy();
            break;
        case "PaymentDashboard": //getAllAccounts();
            removeDatafrmTransfersform();
            //       getAllAccounts();
            frmPaymentDashboard.show();
            break;
        case "MakeAnotherTransfer":
            removeDatafrmTransfersform();
            makeInlineErrorsTransfersInvisible();
            getAllAccounts();
            break;
        case geti18nkey("i18n.common.AccountDashboard"):
            accountDashboardDataCall();
            break;
        case "JoMo":
            frmJoMoPay.destroy();
            frmJOMOPayAddBiller.destroy();
            //             frmJoMoPayLanding.show();
            //       getJomoPayTransactionsList();
            gblTModule = "jomo";
            glbJmpFlow = "dash";
            call_serv_getjomoppayregdata();
            break;
        case "CardsLanding":
            gblToOTPFrom = "";
            gblFromModule = "";
            onClickCardsMenu();
            break;
        case "Cards":
            if (is_CARDPAYMENTSUCCESS) {
                service_fetch_cardListAndDetils({
                    "custID": custid,
                    "form": "frmCardsLandingKA"
                });
            } else {
                //frmManageCardsKA.show();
                frmCardsLandingKA.show();
                frmWebCharge.destroy();
                frmWebCharge.txtFieldAmount.text = "";
                frmCreditCardPayment.destroy();
                frmCreditCardPayment.txtFieldAmount.text = "";
            }
            break;
        case "ManagePayee":
            if (kony.sdk.isNetworkAvailable()) {
                INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                controller = INSTANCE.getFormController("frmManagePayeeKA");
                navObject = new kony.sdk.mvvm.NavigationObject();
                navObject.setRequestOptions("managepayeesegment", {
                    "headers": {},
                    "queryParams": {
                        "custId": custid,
                        "P_BENE_TYPE": "PRE"
                    }
                });
                controller.loadDataAndShowForm(navObject);
                frmAddNewPrePayeeKA.destroy(); //  hassan prePaid
            } else {
                customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
            }
            break;
        case "PayBill":
            ShowLoadingScreen();
            refreshAllAccounts();
            var prevform = kony.application.getPreviousForm();
            if (prevform.id !== "frmEditPayeeKA") {
                kony.boj.selectedPayee = {
                    "ServiceType": prevform.tbxServiceType.text,
                    "BillingNumber": prevform.tbxBillerNumber.text,
                    "BillerCode": prevform.tbxBillerName.text,
                    "NickName": prevform.tbxNickName.text
                };
                kony.boj.AllPayeeList.push(kony.boj.selectedPayee);
            }
            loadBillerDetails = true;
            billType = "POSTPAID";
            getBillDetailsPostpaidBills();
            break;
        case "PayPrePaidBill": //  hassan prePaid
            ShowLoadingScreen();
            refreshAllAccounts();
            loadBillerDetails = true;
            billType = "PREPAID";
            var denoCode = "";
            var denoDesc = "";
            if (frmAddNewPrePayeeKA.tbxDenomination.text === null && frmAddNewPrePayeeKA.tbxDenomination.text === undefined && frmAddNewPrePayeeKA.tbxDenomination.text === "") {
                denoCode = "";
                denoDesc = "";
            } else {
                denoCode = frmAddNewPrePayeeKA.tbxDenomination.text;
                denoDesc = frmAddNewPrePayeeKA.lblDenominationConf.text;
            }
            kony.print("hassan selectedPayee-->" + JSON.stringify(kony.boj.selectedPayee));
            if (gblToOTPFrom === "UpdateBiller") {
                selectedPrePaidPayee = {
                    "nickName": kony.boj.selectedPayee.NickName,
                    "custId": custid,
                    "bene_address1": kony.boj.selectedPayee.p_billerdesc,
                    "bene_city": kony.boj.selectedPayee.lblServiceType,
                    "bankName": kony.boj.selectedPayee.p_denocode, //frmAddNewPrePayeeKA.lblDenomination.text,
                    "bene_address2": kony.boj.selectedPayee.p_billercode,
                    "bene_email": kony.boj.selectedPayee.p_servicetype,
                    "bene_bank_city": kony.boj.selectedPayee.p_denodesc, //frmAddNewPrePayeeKA.tbxDenomination.text,
                    "benAcctNo": kony.boj.selectedPayee.tbxBillerNumber,
                    "benBranchNo": kony.boj.selectedPayee.tbxAmount,
                    "p_servicetype": kony.boj.selectedPayee.p_servicetype,
                    "p_billerdesc": kony.boj.selectedPayee.p_billerdesc,
                    "p_billercode": kony.boj.selectedPayee.p_billercode,
                    "BillingNumber": kony.boj.selectedPayee.p_billingno,
                    "p_dueamount": kony.boj.selectedPayee.p_dueamount,
                    "p_denocode": kony.boj.selectedPayee.p_denocode,
                    "p_denodesc": kony.boj.selectedPayee.p_denodesc
                };
                frmEditPayeeKA.destroy();
            } else {
                selectedPrePaidPayee = {
                    "nickName": frmAddNewPrePayeeKA.tbxNickName.text,
                    "custId": custid,
                    "bene_address1": frmAddNewPrePayeeKA.lblBillerName.text,
                    "bene_city": frmAddNewPrePayeeKA.lblServiceType.text,
                    "bankName": denoDesc, //frmAddNewPrePayeeKA.lblDenomination.text,
                    "bene_address2": frmAddNewPrePayeeKA.tbxBillerName.text,
                    "bene_email": frmAddNewPrePayeeKA.tbxServiceType.text,
                    "bene_bank_city": denoCode, //frmAddNewPrePayeeKA.tbxDenomination.text,
                    "benAcctNo": frmAddNewPrePayeeKA.tbxBillerNumber.text,
                    "benBranchNo": frmAddNewPrePayeeKA.tbxAmount.text,
                    "p_servicetype": frmAddNewPrePayeeKA.tbxServiceType.text,
                    "p_billerdesc": frmAddNewPrePayeeKA.lblBillerName.text,
                    "p_billercode": frmAddNewPrePayeeKA.tbxBillerName.text,
                    "BillingNumber": frmAddNewPrePayeeKA.tbxBillerNumber.text,
                    "p_dueamount": frmAddNewPrePayeeKA.tbxAmount.text,
                    "p_denocode": denoDesc,
                    "p_denodesc": denoCode
                };
                frmAddNewPrePayeeKA.destroy();
            }
            kony.print("hassan selectedPayee111111-->" + JSON.stringify(selectedPrePaidPayee));
            serv_getPrePaidBeneData(selectedPrePaidPayee);
            break;
        case "MyBillers":
            ShowLoadingScreen();
            refreshAllAccounts();
            if (kony.sdk.isNetworkAvailable()) {
                var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                var controller = INSTANCE.getFormController("frmManagePayeeKA");
                var navObject = new kony.sdk.mvvm.NavigationObject();
                navObject.setRequestOptions("managepayeesegment", {
                    "headers": {},
                    "queryParams": {
                        "custId": custid,
                        "P_BENE_TYPE": "PRE"
                    }
                });
                controller.loadDataAndShowForm(navObject);
            } else {
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
            }
            break;
        case "AddNewBiller": //frmAddNewPrePayeeKA
            ShowLoadingScreen();
            refreshAllAccounts();
            kony.boj.BillerResetFlag = false;
            kony.boj.addBillerFromQuickPay = true;
            if (gblQuickFlow === "pre") {
                frmAddNewPrePayeeKA.show();
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            } else {
                var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                var listController = INSTANCE.getFormController("frmAddNewPayeeKA");
                var navigationObject = new kony.sdk.mvvm.NavigationObject();
                navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
                navigationObject.setRequestOptions("form", {
                    "headers": {}
                });
                listController.performAction("navigateTo", ["frmAddNewPayeeKA", navigationObject]);
            }
            break;
        case "StandingInstructions":
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var controller = INSTANCE.getFormController("frmStandingInstructions");
            var navObject = new kony.sdk.mvvm.NavigationObject();
            navObject.setRequestOptions("segSIList", {
                "headers": {},
                "queryParams": {}
            });
            controller.loadDataAndShowForm(navObject);
            break;
        case "gotofrmAuthorizationAlternatives":
            gotofrmAuthorizationAlternatives();
            break;
        case "gotoSettings":
            gotoSettings();
            break;
        case "LogoutAction":
            kony.sdk.mvvm.LogoutAction();
            break;
        case "sendMoneyJomo":
            gblTModule = "jomoBiller";
            getAllAccounts();
            break;
        case "BeneficiaryJomo":
            navigate_JOMOPAYADDBILLER();
            break;
        case "addBeneJOMOPAY":
            add_AS_BENEFICIARY_JOMOPAYBILLER();
            break;
        case geti18nkey("i18n.Cardless.ManageCardless"):
            /*hassan Cardless*/ kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            gblTModule = "";
            getAllAccounts(); //when go to cardless after confirm account balance didnt change
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            serv_cardlessTransactions();
            frmManageCardLess.show();
            previous_FORM = null;
            frmCardlessTransaction.destroy();
            break;
        case "openDeposite":
            /*hassan open Deposite*/ kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            frmAccountsLandingKA.flxNoDeposit.setEnabled(false);
            frmAccountsLandingKA.segAccountsKADeals.setEnabled(true);
            frmAccountsLandingKA.btnNewDeposit.setEnabled(true);
            frmOpenTermDeposit.destroy();
            frmAccountsLandingKA.flxAccountsMain.setVisibility(false);
            frmAccountsLandingKA.flxLoans.setVisibility(false);
            frmAccountsLandingKA.flxDeals.setVisibility(true);
            frmAccountsLandingKA.show();
            break;
        default:
            function none() {}
    }
}

function getFormattedDate(date) {
    var currDate = date.day;
    var currMonth = date.month;
    var currYear = date.year;
    if (parseInt(currMonth) < 10) {
        currMonth = "0" + currMonth;
    }
    if (parseInt(currDate) < 10) {
        currDate = "0" + currDate;
    }
    return currYear + "-" + currMonth + "-" + currDate;
}

function geti18Value(key) {
    return kony.i18n.getLocalizedString(key);
}

function getServiceErrorMessage(errorCode, invokedFunction) {
    if (invokedFunction === null || invokedFunction === undefined || invokedFunction === "") {
        invokedFunction = "Not sent";
    }
    kony.print("getServiceErrorMessage" + JSON.stringify(errorCode));
    try {
        var errorMessage = "";
        switch (errorCode) {
            case 1010:
                errorMessage = geti18nkey("i18n.Generic.InternalServerError");
                break;
            case 1011:
                errorMessage = geti18nkey("i18n.errorMsg.deviceConnectionError");
                break;
            case 1012:
                errorMessage = geti18nkey("i18n.Generic.InternalServerError");
                break;
            case 1013:
                errorMessage = geti18nkey("i18n.Generic.InternalServerError");
                break;
            case 1014:
                errorMessage = geti18nkey("i18n.Generic.InternalServerError");
                break;
            case 1015:
                errorMessage = geti18nkey("i18n.Generic.InternalServerError");
                break;
            case 1016:
                errorMessage = geti18nkey("i18n.Generic.InternalServerError");
                break;
            case 90004:
                errorMessage = geti18nkey("i18n.Generic.InternalServerError");
                break;
            case "00054":
                errorMessage = geti18nkey("i18n.errorCode.54");
                break;
            case "00052":
                errorMessage = geti18nkey("i18n.errorCode.52");
                break;
            case "00051":
                errorMessage = geti18nkey("i18n.errorCode.51");
                break;
            case "00060":
                errorMessage = geti18nkey("i18n.errorCode.60");
                break;
            case "S0038":
                errorMessage = geti18nkey("i18n.transfers.incorrectOTP");
                break;
            case "S0034":
                errorMessage = geti18nkey("i18n.transfers.expiredOTP");
                break;
            case "00444":
                errorMessage = geti18nkey("i18n.AddBene.BeneExist");
                break;
            case "00500":
                errorMessage = geti18nkey("i18n.AddBene.BeneExist");
                break;
            case "2034":
                errorMessage = geti18nkey("i18n.addr.NeedEngAdrr");
                errorMessage = errorMessage + " " + bojcontactnumber;
                break;
            case 2034:
                errorMessage = geti18nkey("i18n.addr.NeedEngAdrr");
                errorMessage = errorMessage + " " + bojcontactnumber;
                break;
            default:
                errorMessage = getErrorMessage(errorCode); //geti18nkey("i18n.common.somethingwentwrong");
                exceptionLogCall("getServiceErrorMessage", "UI ERROR", "UI", errorCode + ":: From :: " + invokedFunction + "  : code not found in the list of error");
                break;
        }
        kony.print("errorMessage::" + errorMessage);
        return errorMessage;
    } catch (e) {
        kony.print("Exception_ErrorCode ::" + e);
        exceptionLogCall("getServiceErrorMessage", "UI ERROR", "UI", e + ":: From :: " + invokedFunction);
        var errorMessage = geti18nkey("i18n.common.somethingwentwrong");
        return errorMessage;
    }
}

function customAlertPopup(Header, Content, AcceptanceCallback, CancelCallback, OkText, CancelText) {
    try {
        kony.print("customAlertPopup::::" + "Title" + Header + "Content" + Content);
        if (CancelCallback === null || CancelCallback === "") {
            kony.print("Single button");
            popupCommonAlert.hbxSingleOk.isVisible = true;
            popupCommonAlert.hbxTwoButtons.isVisible = false;
            popupCommonAlert.btnSingleOkay.onClick = AcceptanceCallback;
            if (OkText === null || OkText === undefined || OkText === "") popupCommonAlert.btnSingleOkay.text = geti18Value("i18n.NUO.OKay");
            else popupCommonAlert.btnSingleOkay.text = OkText;
        }
        /* else if(CancelCallback == "noButton"){
           kony.print("No buttons");
           popupCommonAlert.hbxSingleOk.isVisible = false;
           popupCommonAlert.hbxTwoButtons.isVisible = false;
         }*/
        else if (CancelCallback !== null || CancelCallback !== "") {
            kony.print("Two buttons");
            popupCommonAlert.hbxSingleOk.isVisible = false;
            popupCommonAlert.hbxTwoButtons.isVisible = true;
            popupCommonAlert.btnCancel.onClick = CancelCallback;
            if (CancelText === null || CancelText === undefined || CancelText === "") popupCommonAlert.btnCancel.text = geti18Value("i18n.common.cancel");
            else popupCommonAlert.btnCancel.text = CancelText;
            popupCommonAlert.btnOkay.onClick = AcceptanceCallback;
            if (OkText === null || OkText === undefined || OkText === "") popupCommonAlert.btnSingleOkay.text = geti18Value("i18n.NUO.OKay");
            else popupCommonAlert.btnOkay.text = OkText;
        }
        if (Header === null || Header === "") {
            //popupCommonAlert.hbxAlertHeader.isVisible = false;
            popupCommonAlert.lineSortpopup.isVisible = false;
        } else {
            popupCommonAlert.lineSortpopup.isVisible = true;
            // popupCommonAlert.hbxAlertHeader.isVisible = true;
        }
        popupCommonAlert.lblAlertHeader.text = Header;
        popupCommonAlert.lblDescription.text = Content;
        var langSelected1 = kony.store.getItem("langPrefObj");
        if (!isEmpty(langSelected1)) {
            if (langSelected1 === "en" || langSelected1 === "EN") {
                popupCommonAlert.lblAlertHeader.contentAlignment = constants.CONTENT_ALIGN_CENTER;
                popupCommonAlert.lblDescription.contentAlignment = constants.CONTENT_ALIGN_CENTER;
            } else {
                //popupCommonAlert.lblAlertHeader.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;// hassan 28/12/2020
                popupCommonAlert.lblAlertHeader.contentAlignment = constants.CONTENT_ALIGN_CENTER; // hassan 28/12/2020
                popupCommonAlert.lblDescription.contentAlignment = constants.CONTENT_ALIGN_CENTER; // hassan 28/12/2020
                // popupCommonAlert.lblDescription.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
            }
        } else {
            var deviceLocale = kony.i18n.getCurrentDeviceLocale();
            //var langInDevice = deviceLocale.language;
            var langInDevice;
            if (kony.os.deviceInfo().name === "Android") {
                langInDevice = deviceLocale.language;
            }
            if (kony.os.deviceInfo().name === "iPhone") {
                langInDevice = deviceLocale;
                if (langInDevice.indexOf("en") > -1) {
                    langInDevice = "en";
                } else if (langInDevice.indexOf("ar") > -1) {
                    langInDevice = "ar";
                } else {
                    langInDevice = "en";
                }
            }
            if (langInDevice === "en") {
                popupCommonAlert.lblAlertHeader.contentAlignment = constants.CONTENT_ALIGN_CENTER;
                popupCommonAlert.lblDescription.contentAlignment = constants.CONTENT_ALIGN_CENTER;
            } else {
                popupCommonAlert.lblAlertHeader.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
                popupCommonAlert.lblDescription.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
            }
        }
        if (Header !== undefined && Header !== "undefined" && Content !== undefined && Content !== "undefined") popupCommonAlert.show();
    } catch (e) {
        kony.print("Exception_customAlert ::" + e);
        exceptionLogCall("customAlertPopup", "UI ERROR", "UI", e);
    }
}

function popupCommonAlertDimiss() {
    var headerText = popupCommonAlert.lblAlertHeader.text;
    if (headerText == geti18nkey("i18n.common.appusageTimeout")) {
        kony.sdk.mvvm.LogoutAction();
        popupCommonAlert.dismiss();
    } else {
        popupCommonAlert.dismiss();
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }
}
kony.boj.popupDeviceRegistration = function(title, message, okText, okCallback, cancelText, cancelCallback) {
    PopupDeviceRegistration.lblTitle.text = title;
    PopupDeviceRegistration.lblMessage.text = message;
    PopupDeviceRegistration.btnYes.text = okText;
    PopupDeviceRegistration.btnNo.text = cancelText;
    PopupDeviceRegistration.btnYes.onClick = okCallback;
    PopupDeviceRegistration.btnNo.onClick = cancelCallback;
    PopupDeviceRegistration.show();
};
kony.boj.dismissPopup = function() {
    PopupDeviceRegistration.dismiss();
};
kony.boj.getBackGroundColour = function(val) {
    var arrColour = ["EA4335", "FC8102", "2ECCFA", "3c34a8", "167047", "c027e0"];
    if (!isEmpty(val)) {
        if (val.length != 2) return arrColour[val.charCodeAt() % arrColour.length];
        return arrColour[(val[0].charCodeAt() + val[1].charCodeAt()) % arrColour.length];
    } else {
        return "052c49";
    }
};
kony.boj.validateEmailBoj = function(userName) {
    var userNamePattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (userNamePattern.test(userName)) {
        return true;
    } else {
        return false;
    }
};
kony.boj.maintainenceCHECK = function() {
    try {
        var isTouchSupported = kony.store.getItem("isTouchSupported");
        kony.print("kony.boj.retailBanking.globals.Maintenance_Flag :: " + kony.boj.retailBanking.globals.Maintenance_Flag);
        if (kony.boj.retailBanking.globals.Maintenance_Flag == "Y") {
            var CHeader = geti18nkey("i18n.app,MaintenanceHeader");
            var CContent = kony.i18n.getLocalizedString("i18n.app.Maintenancetext");
            customAlertPopup(CHeader, CContent, exitAppOk, "", kony.i18n.getLocalizedString("i18n.NUO.OKay"), "");
            kony.application.dismissLoadingScreen();
        } else {
            var appVersion1 = appConfig.appVersion.split(".");
            var version = "";
            for (var i in appVersion1) {
                if (i == 0) version = appVersion1[i] + ".";
                else version = version + "" + appVersion1[i];
            }
            var currentAppVersion = parseFloat(version);
            kony.print("currentAppVersion:: " + currentAppVersion + " ::serverAppVersion:: " + serverAppVersion);
            kony.application.dismissLoadingScreen();
            if (!isEmpty(kony.store.getItem("isAppUpdated"))) {
                var stored_APP_VERSION = kony.store.getItem("isAppUpdated").split(".");
                for (var i in stored_APP_VERSION) {
                    if (i == 0) version = stored_APP_VERSION[i] + ".";
                    else version = version + "" + stored_APP_VERSION[i];
                }
                if (parseFloat(version) < serverAppVersion) kony.store.setItem("isDeviceRegisteredMandatory", true);
            }
            if (currentAppVersion < serverAppVersion) {
                kony.store.setItem("isDeviceRegisteredMandatory", true);
                customAlertPopup(geti18nkey("i18n.appUpgrade.upgradeOfAnApp"), kony.i18n.getLocalizedString("i18n.appUgrademsg.text"), navtoPlayStore, exitAppOk, kony.i18n.getLocalizedString("i18n.common.Text"), kony.i18n.getLocalizedString("i18n.common.cancelC"));
                return;
            } else if (isTouchSupported !== null && isTouchSupported !== undefined && isTouchSupported !== "" && isTouchSupported == "Y") {
                serverAppVersion = currentAppVersion;
                if (kony.sdk.isNetworkAvailable()) {
                    if (autoTouchShowFlag === undefined || autoTouchShowFlag === null || autoTouchShowFlag === false) {
                        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
                        isAlternateLoginEnabled("T");
                        autoTouchShowFlag = true;
                    }
                }
            } else {
                serverAppVersion = currentAppVersion;
                kony.application.dismissLoadingScreen();
            }
        }
    } catch (e) {
        kony.print("Exception_maintainenceCHECK ::" + e);
        kony.application.dismissLoadingScreen();
    }
};

function checkAccount(accnbr) {
    try {
        if (!isEmpty(accnbr) && !isEmpty(custid) && !isEmpty(accnbr.substring(6, 13))) {
            kony.print("checkAccount :: " + accnbr.substring(6, 13));
            var res = parseInt(accnbr.substring(6, 13));
            var custidt = parseInt(custid);
            if (res == custidt) return true;
            else return false;
        } else {
            return false;
        }
    } catch (err) {
        kony.print("in checkAccount :: " + err);
        return false;
    }
}

function logoutpp() {
    popupCommonAlertDimiss();
    kony.sdk.mvvm.LogoutAction();
}

function flip_ANIMATION_SEGMENT_WIDGET(eventobject, context, segment, widgetToAnim) {
    try {
        var animationObject = kony.ui.createAnimation({
            "0": {
                "width": "100%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            },
            "50": {
                "width": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            },
            "100": {
                "width": "100%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            }
        });
        var animationConfig = {
            duration: 0.5,
            fillMode: kony.anim.FILL_MODE_FORWARDS
        };
        var animationCallbacks = {
            "animationEnd": function() {}
        };
        var animationDefObject = {
            definition: animationObject,
            config: animationConfig,
            callbacks: animationCallbacks
        };
        var row1 = {
            sectionIndex: context.sectionIndex,
            rowIndex: context.rowIndex
        };
        var rowList = [row1];
        segment.animateRows({
            rows: rowList,
            widgets: [widgetToAnim],
            animation: animationDefObject
        });
    } catch (e) {
        kony.print("Exception_onClick_animate_LABEL ::" + e);
    }
}

function openBOJPAYAppLink() {
    kony.print("openBOJPAYAppLink");
    var KonyMain = java.import('com.konylabs.android.KonyMain');
    var context = KonyMain.getActivityContext();
    var Intent = java.import('android.content.Intent');
    var bojpayIntent = new Intent("com.bankofjordan.bojpay.START");
    bojpayIntent.putExtra("CONTEXT", "CARD_CONTROL");
    bojpayIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
    //activity.startActivity(bojpayIntent);
    var PackageManager = java.import('android.content.pm.PackageManager');
    var bojpayCheckIntent = context.getPackageManager().getLaunchIntentForPackage("com.bankofjordan.bojpay");
    if (bojpayCheckIntent === null) {
        kony.application.openURL("http://play.google.com/store/apps/details?id=com.bankofjordan.bojpay");
        kony.print("App not installed, Navigating to playstore");
    } else {
        kony.print("App opened");
        context.startActivity(bojpayIntent);
    }
    gblPayAppFlow = false;
    gblLaunchModeOBJ.appFlow = "";
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmEnrolluserLandingKA.lblFlag.text = "F";
    frmLoginKA.show();
}