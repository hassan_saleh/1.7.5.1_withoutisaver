//Type your code here
//addGesture
kony.boj.setAnimationToBenfSegment = function() {
    /*
    kony.print("setAnimationToBenfSegment");
    var lang = kony.store.getItem("langPrefObj");
    if(lang === "ar"){   
      frmShowAllBeneficiary.segAllBeneficiary.rowTemplate = flxSeg;
      var gesture = frmShowAllBeneficiary.segAllBeneficiary.rowTemplate.flxAnimateBenf.addGestureRecognizer(constants.GESTURE_TYPE_SWIPE, {
      fingers: 1,swipedistance: 55,
          swipevelocity: 60
    }, kony.boj.animateRowOnSwipeBenfAr);
    }
    else{
      frmShowAllBeneficiary.segAllBeneficiary.rowTemplate = flxSeg;
      var gesture = frmShowAllBeneficiary.segAllBeneficiary.rowTemplate.flxAnimateBenf.addGestureRecognizer(constants.GESTURE_TYPE_SWIPE, {
      fingers: 1,swipedistance: 55,
          swipevelocity: 60
    }, kony.boj.animateRowOnSwipeBenf);
    }
    */
};
kony.boj.animateRowOnSwipeBenf = function(widgetRef, gestureInfo, context) {
    kony.print("rowdata::" + JSON.stringify(context));
    kony.boj.selectedBenf = context.widgetInfo.selectedRowItems[0];
    try {
        /*if(gestureInfo.swipeDirection === 0){
          var deviceInfo = kony.os.deviceInfo();
          if(deviceInfo.name !== "iPhone"){
            kony.boj.deleteFlagBenf = false;
            gblSelectedBene = context.widgetInfo.selectedRowItems[0];
            check_numberOfAccounts();
           }
        }
        else */
        if (gestureInfo.swipeDirection === 1) {
            //code for delete
            if (kony.boj.deleteFlagBenf === true) return;
            kony.boj.deleteFlagBenf = true;
            animationdef = {
                100: {
                    left: "-20%"
                }
            };
            animDef = kony.ui.createAnimation(animationdef);
            animationConfig = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS,
                "iterationCount": 1
            };
            callBack = {
                animationStart: function() {},
                animationEnd: function() {}
            };
            widgetRef.animate(animDef, animationConfig, callBack);
        } else if (gestureInfo.swipeDirection === 2) {
            //code for hide delete
            kony.boj.deleteFlagBenf = false;
            kony.boj.selectedBenf = {};
            animationdef = {
                100: {
                    left: "0%"
                }
            };
            animDef = kony.ui.createAnimation(animationdef);
            animationConfig = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS,
                "iterationCount": 1
            };
            callBack = {
                animationStart: function() {},
                animationEnd: function() {}
            };
            widgetRef.animate(animDef, animationConfig, callBack);
        }
    } catch (err) {
        kony.print("Error in Ben Swipe:" + err);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen("");
    }
};
kony.boj.animateRowOnSwipeBenfAr = function(widgetRef, gestureInfo, context) {
    //kony.print("rowdata::"+JSON.stringify(context.widgetInfo.selectedRowItems[0]));
    kony.boj.selectedBenf = context.widgetInfo.selectedRowItems[0];
    try {
        /* if(gestureInfo.swipeDirection === 0){
           var deviceInfo = kony.os.deviceInfo();
           if(deviceInfo.name !== "iPhone"){
             kony.boj.deleteFlagBenf = false;
             gblSelectedBene = context.widgetInfo.selectedRowItems[0];
             check_numberOfAccounts();
            }
         }
         else*/
        if (gestureInfo.swipeDirection === 1) {
            //code for hide delete
            kony.boj.deleteFlagBenf = false;
            animationdef = {
                100: {
                    right: "0%"
                }
            };
            animDef = kony.ui.createAnimation(animationdef);
            animationConfig = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS,
                "iterationCount": 1
            };
            callBack = {
                animationStart: function() {},
                animationEnd: function() {}
            };
            widgetRef.animate(animDef, animationConfig, callBack);
        } else if (gestureInfo.swipeDirection === 2) {
            //code for delete
            if (kony.boj.deleteFlagBenf === true) return;
            kony.boj.deleteFlagBenf = true;
            animationdef = {
                100: {
                    right: "-20%"
                }
            };
            animDef = kony.ui.createAnimation(animationdef);
            animationConfig = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS,
                "iterationCount": 1
            };
            callBack = {
                animationStart: function() {},
                animationEnd: function() {}
            };
            widgetRef.animate(animDef, animationConfig, callBack);
        }
    } catch (err) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen("");
        kony.print("Error in benf Swipe:" + err);
    }
};
kony.boj.deleteBenfFromSegmentPrePaid = function(eventobject, context) {
    kony.print("deleteBenfFromSegment:" + JSON.stringify(eventobject));
    kony.print("deleteBenfFromSegment:" + JSON.stringify(context));
    kony.boj.selectedBenf = context.widgetInfo.selectedRowItems[0];
    customAlertPopup(kony.i18n.getLocalizedString("i18n.common.delBenConfirm"), kony.i18n.getLocalizedString("i18n.common.deleteBen"), callDeleteSelectedPrePaidBenf, popupCommonAlertDimiss, kony.i18n.getLocalizedString("i18n.common.YES"), kony.i18n.getLocalizedString("i18n.common.NO"));
};

function callDeleteSelectedPrePaidBenf() {
    popupCommonAlertDimiss();
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        kony.print("callDeleteSelectedPrePaidBenf:" + JSON.stringify(kony.boj.selectedBenf));
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("ExternalAccounts", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("ExternalAccounts");
        dataObject.addField("custId", custid);
        dataObject.addField("P_BENEF_ID", kony.boj.selectedBenf.benef_id);
        dataObject.addField("P_BENEF_NAME", kony.boj.selectedBenf.beneficiaryName);
        dataObject.addField("P_BENE_ACCOUNT", kony.boj.selectedBenf.benAcctNo);
        dataObject.addField("P_BENE_TYPE", "PRE");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("benf dataObject::" + JSON.stringify(dataObject));
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("deleteBenificiary", serviceOptions, deletePrePaidBenfSuccess, deletePrePaidBenfError);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function deletePrePaidBenfSuccess(res) {
    kony.print("deleteBenfSuccess::" + JSON.stringify(res));
    if (res.opstatus == "0" && !isEmpty(res.result) && res.result === "00000") {
        frmManagePayeeKA.btnAll.skin = "slButtonWhiteTabDisabled";
        frmManagePayeeKA.btnPrePaid.skin = "slButtonWhiteTab";
        frmManagePayeeKA.btnPostPaid.skin = "slButtonWhiteTabDisabled";
        kony.boj.selectedBillerType = "PrePaid";
        serv_getPrePaidBillerData();
    } else {
        customAlertPopup(geti18nkey("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
        kony.application.dismissLoadingScreen();
    }
}

function deletePrePaidBenfError(err) {
    kony.print("deleteBenfError::" + JSON.stringify(err));
    customAlertPopup(geti18nkey("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    kony.application.dismissLoadingScreen();
}