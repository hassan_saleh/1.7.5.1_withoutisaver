kony = kony || {};
kony.rb = kony.rb || {};
kony.rb.transactionBusinessController = function(mfObjSvc) {
    this.mfObjectServiceHandler = mfObjSvc;
    this.cardlessTransactionType = "Cardless";
};
kony.rb.transactionBusinessController.prototype.createCardlessTransaction = function(createTxnRecordDetails, presentationSuccessCallback, presentationErrorCallback) {
    this.mfObjectServiceHandler.create("Transactions", createTxnRecordDetails, {}, success, error);

    function success(res) {
        presentationSuccessCallback(res);
    }

    function error(err) {
        presentationErrorCallback(err);
    }
};
kony.rb.transactionBusinessController.prototype.createCardlessQRCodeTransaction = function(createTxnRecordDetails, presentationSuccessCallback, presentationErrorCallback) {
    this.mfObjectServiceHandler.create("QrCode", createTxnRecordDetails, {}, success, error);

    function success(res) {
        presentationSuccessCallback(res);
    }

    function error(err) {
        presentationErrorCallback(err);
    }
};
kony.rb.transactionBusinessController.prototype.deleteCardlessTransaction = function(deleteTxnRecordDetails, presentationSuccessCallback, presentationErrorCallback) {
    this.mfObjectServiceHandler.delete("Transactions", deleteTxnRecordDetails, {}, success, error);

    function success(res) {
        presentationSuccessCallback(res);
    }

    function error(err) {
        presentationErrorCallback(err);
    }
};
kony.rb.transactionBusinessController.prototype.faceIDCall = function(recordsDataMap, presentationSuccessCallback, presentationErrorCallback) {
    var amount = recordsDataMap[0].amount;
    recordsDataMap[0].amount = parseFloat(amount.replace(/[^0-9-.]/g, ''));
    var utilHandler = applicationManager.getUtilityHandler();
    var internalTransactionRecord = utilHandler.convertJSonToTransactionsObject(recordsDataMap[0]);
    this.mfObjectServiceHandler.customPost("isSecondFactorAuthenticationRequired", "Transactions", internalTransactionRecord, {}, success, error);

    function success(res) {
        presentationSuccessCallback(res);
    }

    function error(err) {
        presentationErrorCallback(err);
    }
};
kony.rb.transactionBusinessController.prototype.faceIDBillPayCall = function(presentationSuccessCallback, presentationErrorCallback) {
    var fromAccNumber = frmConfirmPayBill.fromAccNumberKA.text;
    var amountLabel = frmConfirmPayBill.MapedAmountLabel.text;
    var fromAccountNumber = frmConfirmPayBill.fromAccountNumberKA.text;
    var mapedData = frmConfirmPayBill.mapedDateKA.text;
    var fromAccountName = frmConfirmPayBill.fromAccountNameKA.text;
    var transactionType = frmConfirmPayBill.transactionType.text;
    var transactionId = frmConfirmPayBill.transactionId.text;
    var feilds = {
        "fromAccountNumber": fromAccNumber,
        "amount": amountLabel,
        "payeeId": fromAccountNumber,
        "scheduledDate": mapedData,
        "payeeNickName": fromAccountName,
        "transactionType": transactionType,
        "transactionId": transactionId
    };
    this.mfObjectServiceHandler.customPost("isSecondFactorAuthenticationRequired", "Transactions", feilds, {}, success, error);

    function success(res) {
        presentationSuccessCallback(res);
    }

    function error(err) {
        presentationErrorCallback(err);
    }
};