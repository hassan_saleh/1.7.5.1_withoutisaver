 // var email = "konyRB@gmail.com";
 var senderID = "178769882464"; //"218701134386"; //"619396663770";
 var client;

 function onClickAlertsCHKBox() {
     try {
         client = kony.sdk.getCurrentInstance();
         setupPushCallbacks();
         if (frmAlertsKA.infoImg.src == "checkbox_off.png") {
             frmAlertsKA.infoImg.src = "checkbox_on.png";
             frmAlertsKA.alertsTypes.isVisible = true;
             registerPush();
             updateFlags("alerts", true);
         } else {
             frmAlertsKA.infoImg.src = "checkbox_off.png";
             frmAlertsKA.alertsTypes.isVisible = false;
             deregisterPush();
             updateFlags("alerts", false);
         }
     } catch (err) {
         //alert("something went wrong got unexpected err " + err);
         exceptionLogCall("onClickAlertsCHKBox", "UI ERROR", "UI", err);
     }
 }

 function appintKPNSCallback() {
     var settingsData = kony.store.getItem("settingsflagsObject");
     kony.print("appintKPNSCallback::::");
     var platFormName = kony.sdk.mvvm.Utils.getPlatformName();
     if (platFormName === kony.sdk.mvvm.Platforms["IPHONE"] || platFormName === kony.sdk.mvvm.Platforms["IPAD"]) {
         callbackiPhoneSetCallbacks();
     } else {
         setupPushCallbacks();
     }
 }

 function onClickAlertsSwitchOption() {
     client = kony.sdk.getCurrentInstance();
     callbackiPhoneSetCallbacks();
     if (frmAlertsKA.SwitchAlert.selectedIndex === 1) {
         frmAlertsKA.alertsTypes.isVisible = false;
         deregisterPush();
         updateFlags("alerts", false);
     } else {
         frmAlertsKA.alertsTypes.isVisible = true;
         registerPush();
         updateFlags("alerts", true);
     }
 }
 // Function to register the device/app/user with push notification service provider (GCM)
 function registerPush() {
     kony.timer.cancel("pushRegister");
     kony.boj.dismissPopup();
     ShowLoadingScreen();
     var configInfo;
     kony.print("\n\n\n<--------in registerPush--------->\n\n\n");
     kony.print("registration started.....");
     kony.print("sender ID: " + senderID);
     // Create local configInfo object where we pass the senderid
     if (kony.retailBanking.globalData.deviceInfo.isIphone()) {
         configInfo = [0, 1, 2];
     } else {
         configInfo = {
             senderid: senderID
         };
     }
     // Call the push API to register the device with GCM
     kony.push.register(configInfo);
     gblPushRegisterTimerFlag = false;
     kony.timer.schedule("pushRegister", pushRegisterTimeCallback, 10, false);
 }
 //pushRegisterTimeCallback
 function pushRegisterTimeCallback() {
     kony.application.dismissLoadingScreen();
     kony.timer.cancel("pushRegister");
     if (!gblPushRegisterTimerFlag && (kony.store.getItem("isPushMandatory") === false)) {
         customAlertPopup(geti18Value("i18n.Bene.Failed"), kony.i18n.getLocalizedString("i18n.pushRegisterFailedMsg"), popupCommonAlertDimiss, "");
     }
     gblPushRegisterTimerFlag = false;
 }
 // Function to deregister the device/app/user with push notification service provider (GCM)
 function deregisterPush() {
     var configInfo = {};
     kony.push.deRegister(configInfo);
     kony.print("deregisterPush: " + JSON.stringify(configInfo));
 }
 // Function to setup all the callbacks for all push events
 // This function is called on the preshow of the form
 function setupPushCallbacks() {
     kony.print("\n\n\n<--------in setupPushCallbacks--------->\n\n\n");
     // Create local object 'callbacks'
     var callbacks = {
         onsuccessfulregistration: regSuccess,
         onfailureregistration: regFailureCallback,
         onlinenotification: gotOnlinePushMessage,
         offlinenotification: gotOfflinePushMessage,
         onsuccessfulderegistration: unsubscribeMFMessaging,
         onfailurederegistration: deregFail
     };
     // Call the push API to register callbacks
     kony.push.setCallbacks(callbacks);
 }

 function callbackiPhoneSetCallbacks() {
     var callbacksTable = {
         onsuccessfulregistration: regSuccessiPhoneCallback,
         onfailureregistration: regFailureCallback,
         onlinenotification: onlinePushNotificationiPhoneCallback,
         offlinenotification: offlinePushNotificationiPhoneCallback,
         onsuccessfulderegistration: unsubscribeMFMessaging,
         onfailurederegistration: deregFail
     };
     kony.push.setCallbacks(callbacksTable);
     //alert("setCallBack Done !!!");
 }

 function regSuccess(regId) {
     kony.print("\n\n\n<--------in regSuccess()--------->\n\n\n");
     kony.print("\nRegistration Id-->" + JSON.stringify(regId));
     gblPushRegisterTimerFlag = true;
     kony.store.setItem("isPushMandatory", false);
     // 	frmPush.lblResult.text = frmPush.lblResult.text + "\n" +  "Successful registration: " + JSON.stringify(regId);
     subscribeMFMessaging(regId, "android");
     exceptionLogCall("RegisterPushNotification", "00000", "Service", "regSuccess -> Push Notification Registration for Android is succeed " + regId);
 }

 function regSuccessiPhoneCallback(regId) {
     kony.print(" Registerd to iPhone push server : " + regId);
     gblPushRegisterTimerFlag = true;
     if (kony.application.getCurrentForm().id === "frmDeviceRegistration" || kony.application.getCurrentForm().id === "frmDeviceRegistrationOptionsKA" || kony.store.getItem("isPushMandatory") === false) {
         subscribeMFMessaging(regId, "iphone");
         exceptionLogCall("RegisterPushNotification", "00000", "Service", "regSuccessiPhoneCallback -> Push Notification Registration for IOS is succeed " + regId);
     }
     kony.store.setItem("isPushMandatory", false);
 }

 function regFailureCallback(error) {
     kony.print("\n Error  --------->" + JSON.stringify(error));
     gblPushRegisterTimerFlag = true;
     kony.store.setItem("isPushMandatory", false);
     callRegisterDeviceBOJ();
     //customAlertPopup(geti18Value("i18n.Bene.Failed"), kony.i18n.getLocalizedString("i18n.registration.failed"), popupCommonAlertDimiss, "");
     exceptionLogCall("RegisterPushNotification", "ERROR", "Service", "regFailureCallback -> Register for Push Notification failed " + error);
     kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
 }
 // Function to be invoked when the deregistration is successful. Once the deregistration is sucessful,
 // we need to unsubscribe to KMS.
 // So this function contains the code to invoke service 'unregisterSubscriberPush'
 function deregSuccess() {
     kony.print("\n\n\n<--------in deregSuccess--------->\n\n\n");
     // Displaying the log on the screen along with previous log
     // 	frmPush.lblResult.text = frmPush.lblResult.text + "\n" + "successful deregistration";
     // Check if userKsID is not null
     //if (userKsID!=null){
     //}
     exceptionLogCall("DeRegisterPushNotification", "SUCCESS", "Service", "deregSuccess -> DeRegister for Push Notification succeed");
     //unsubscribeMFMessaging();
 }
 // Function to be invoked when the deregistration is unsuccessful
 function deregFail(error) {
     kony.print("\n\n\n<--------in deregFail--------->\n\n\n");
     // Displaying the log on the screen along with previous log
     // 	frmPush.lblResult.text = frmPush.lblResult.text + "\n" + "Unsuccessful deregistration: " + JSON.stringify(error);
     exceptionLogCall("DeRegisterPushNotification", "ERROR", "Service", "deregFail -> DeRegister for Push Notification failed " + error);
 }
 // Function to be invoked when the device receives push notification message when the application is running
 function gotOnlinePushMessage(pushMsg) {
     kony.print("Online push Notification ::" + JSON.stringify(pushMsg));
     try {
         popupNotificationInfo.lblAlertHeader.text = pushMsg.title;
         popupNotificationInfo.lblDescription.text = pushMsg.content;
         popupNotificationInfo.show();
         //   customAlertPopup("", errorMessage,popupCommonAlertDimiss,"");
         var pushObj = new kony.retailbanking.pushNotificationsHandler();
         pushObj.onlinePushHandlerAndroid(pushMsg);
     } catch (e) {
         kony.print("Exception_OnlinePushNotification ::" + e);
     }
 }
 // Function to be invoked when the device receives push notification message when the application is not running
 function gotOfflinePushMessage(pushMsg) {
     //kony.print("Offline Push notification ::"+JSON.stringify(pushMsg));
     try {
         popupNotificationInfo.lblAlertHeader.text = pushMsg.title;
         popupNotificationInfo.lblDescription.text = pushMsg.content;
         popupNotificationInfo.show();
     } catch (e) {
         //kony.print("Exception_gotOfflinePushMessage ::"+e);
     }
 }

 function onlinePushNotificationiPhoneCallback(response) {
     //   alert("Online Push message Iphone: " + JSON.stringify(msg));
     if (response !== null && response !== undefined) {
         var pushTitle = response.alert.title;
         var pushMessage = response.alert.body;
         //customAlertPopup(pushTitle, pushMessage,popupCommonAlertDimiss,"");
         popupNotificationInfo.lblAlertHeader.text = pushTitle;
         popupNotificationInfo.lblDescription.text = pushMessage;
         popupNotificationInfo.show();
         var pushObj = new kony.retailbanking.pushNotificationsHandler();
     }
     //pushObj.onlinePushHandlerIphone(msg);
 }

 function offlinePushNotificationiPhoneCallback(response) {
     //   alert("Offline Push message Iphone: " + JSON.stringify(msg));
     if (response !== null && response !== undefined) {
         var pushTitle = response.alert.title;
         var pushMessage = response.alert.body;
         //customAlertPopup(pushTitle, pushMessage,popupCommonAlertDimiss,"");
         popupNotificationInfo.lblAlertHeader.text = pushTitle;
         popupNotificationInfo.lblDescription.text = pushMessage;
         popupNotificationInfo.show();
         //obj.offlinePushHandlerIphone(msg);
     } else {
         kony.retailbanking.pushNotificationsHandler.isAppInBackGround = true;
     }
 }
 var messagingClient;

 function subscribeMFMessaging(regId, ostype) {
     kony.print("\n\n<----------in subscribeMFMessaging()-------->\n\n");
     var deviceID = deviceid;
     var savedCredential = kony.sdk.mvvm.KonyApplicationContext.getRecentlyLoggedUserDetails();
     var ufID = user_id; //kony.retailBanking.globalData.globals.userObj.userName || frmLoginKA.tbxusernameTextField.text || savedCredential["username"]  || frmLoginKA.lblChangeUserName.text || DecryptValue(kony.store.getItem("userName"));
     kony.print("deviceID:::" + deviceID + "::ufID::" + ufID);
     try {
         // 			messagingClient = kony.sdk.prototype.getMessagingService();
         messagingClient = kony.sdk.getCurrentInstance().getMessagingService();
     } catch (exception) {
         kony.print("\n<----------Exception-------->" + exception.message);
     }
     if (messagingClient != null && messagingClient != "" && messagingClient != undefined) {
         kony.print("messagingClient :: " + JSON.stringify(messagingClient));
         if (ostype === "android") {
             messagingClient.register("androidgcm", deviceID, regId, ufID, function(response) {
                 KSID = response.id;
                 kony.print("\n<----------Subscription Success--------> " + JSON.stringify(response));
                 var curForm = kony.application.getCurrentForm().id;
                 if (curForm === "frmDeviceRegistration") onClickYesDeviceRegister();
                 else callRegisterDeviceBOJ();
             }, function(error) {
                 kony.application.dismissLoadingScreen();
                 customAlertPopup(geti18Value("i18n.Bene.Failed"), kony.i18n.getLocalizedString("i18n.registration.failed"), popupCommonAlertDimiss, "");
                 kony.print("\n<----------Subscription Failure--------> " + JSON.stringify(error));
             });
         } else {
             messagingClient.register("iphone", deviceID, regId, ufID, function(response) {
                 KSID = response.id;
                 kony.print("\n<----------Subscription Success--------> " + JSON.stringify(response));
                 var curForm = kony.application.getCurrentForm().id;
                 if (curForm === "frmDeviceRegistration") onClickYesDeviceRegister();
                 else callRegisterDeviceBOJ();
             }, function(error) {
                 kony.application.dismissLoadingScreen();
                 customAlertPopup(geti18Value("i18n.Bene.Failed"), kony.i18n.getLocalizedString("i18n.registration.failed"), popupCommonAlertDimiss, "");
                 kony.print("\n<----------Subscription Failure--------> " + JSON.stringify(error));
             });
         }
     } else {
         kony.application.dismissLoadingScreen();
         customAlertPopup(geti18Value("i18n.Bene.Failed"), kony.i18n.getLocalizedString("i18n.registration.failed"), popupCommonAlertDimiss, "");
     }
 }

 function unsubscribeMFMessagingOLD() {
     kony.print("\n\n<----------in unsubscribeKMS-------->\n\n");
     try {
         messagingClient = kony.sdk.getCurrentInstance().getMessagingService();
     } catch (exception) {
         kony.print("\n<----------Exception-------->" + exception.message);
     }
     //kony.print("messagingClient: " + JSON.stringify(messagingClient));
     try {
         messagingClient.unregister(function(response) {
             KSID = response.id;
             alert("Unsubscription Success");
         }, function(error) {
             alert("Unsubscription Failed");
             //kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
         });
     } catch (e) {
         //toastMsg.showToastMsg("Unsubscription Failed", 7000);
     }
 }

 function unsubscribeMFMessaging() {
     try {
         var uri = konyRef.messagingsvc.url + "/subscribers";
         kony.print("gblUnsubscribe::" + JSON.stringify(gblUnsubscribe));
         var inp = {
             "subscriptionService": {
                 "unsubscribe": {
                     "ksid": gblUnsubscribe.ksid,
                     "appId": gblUnsubscribe.appId,
                     "deviceId": gblUnsubscribe.deviceId
                 }
             }
         };
         var networkProvider = new konyNetworkProvider();
         var headers = {
             "Content-Type": "application/json"
         };
         var payload = {
             postdata: JSON.stringify(inp)
         };
         kony.print("unsubscribe uri:" + uri);
         networkProvider.post(uri, payload, headers, function(data) {
             kony.print("unsubscribe success:::::" + JSON.stringify(data));
         }, function(data, status, error) {
             kony.print("ERROR: Failed to unregister device for KMS::" + error);
         });
     } catch (e) {
         kony.print("Exception::" + e);
     }
 }

 function unsubscribeCall() {
     kony.print("unsubscribeCall::");
     kony.push.deRegister({});
 }

 function securityDealsAlertsNavigation() {
     var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
     if (frmAlertsKA.generalAlerts.selectedRowIndex[1] === 0) {
         var controller = INSTANCE.getFormController("frmSecurityAlertsKA");
         var navigationObject = new kony.sdk.mvvm.NavigationObject();
         //navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
         navigationObject.setRequestOptions("form", {
             "headers": {
                 "session_token": kony.retailBanking.globalData.session_token
             }
         });
         controller.performAction("navigateTo", ["frmSecurityAlertsKA", navigationObject]);
         // frmSecurityAlertsKA.show();
     } else {
         var controller2 = INSTANCE.getFormController("frmDealsAlertKA");
         var navigationObject2 = new kony.sdk.mvvm.NavigationObject();
         //navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
         navigationObject2.setRequestOptions("form", {
             "headers": {
                 "session_token": kony.retailBanking.globalData.session_token
             }
         });
         controller2.performAction("navigateTo", ["frmDealsAlertKA", navigationObject2]);
         // frmDealsAlertKA.show();
     }
 }

 function updateSecurityAletStatus(alertType) {
     if (kony.retailBanking.globalData.deviceInfo.isIphone()) {
         if (alertType === "bankingIdChange") {
             if (frmSecurityAlertsKA.BankingIdChangeAlertSwitch.selectedIndex === 1) {
                 frmSecurityAlertsKA.lblBankingIdChange.text = "false";
             } else {
                 frmSecurityAlertsKA.lblBankingIdChange.text = "true";
             }
         } else if (alertType === "passwordChange") {
             if (frmSecurityAlertsKA.PasswordChangeAlertSwitch.selectedIndex === 1) {
                 frmSecurityAlertsKA.lblPasswordChange.text = "false";
             } else {
                 frmSecurityAlertsKA.lblPasswordChange.text = "true";
             }
         } else if (alertType === "passwordExpired") {
             if (frmSecurityAlertsKA.PasswordExpiredAlertSwitch.selectedIndex === 1) {
                 frmSecurityAlertsKA.lblPasswordExpired.text = "false";
             } else {
                 frmSecurityAlertsKA.lblPasswordExpired.text = "true";
             }
         } else if (alertType === "addressPhoneChanged") {
             if (frmSecurityAlertsKA.AddressPhoneChangeAlertSwitch.selectedIndex === 1) {
                 frmSecurityAlertsKA.lblAddressPhoneChange.text = "false";
             } else {
                 frmSecurityAlertsKA.lblAddressPhoneChange.text = "true";
             }
         } else if (alertType === "newPayeeAdded") {
             if (frmSecurityAlertsKA.NewPayeeAddedAlertSwitch.selectedIndex === 1) {
                 frmSecurityAlertsKA.lbNewPayeeAdded.text = "false";
             } else {
                 frmSecurityAlertsKA.lbNewPayeeAdded.text = "true";
             }
         } else {
             if (frmSecurityAlertsKA.PayeeDetailsUpdatedAlertSwitch.selectedIndex === 1) {
                 frmSecurityAlertsKA.lblPayeeDetailsUpdated.text = "false";
             } else {
                 frmSecurityAlertsKA.lblPayeeDetailsUpdated.text = "true";
             }
         }
     } else {
         if (alertType === "bankingIdChange") {
             if (frmSecurityAlertsKA.imgBankingIdChangeKA.src == "checkbox_off.png") {
                 frmSecurityAlertsKA.imgBankingIdChangeKA.src = "checkbox_on.png";
                 frmSecurityAlertsKA.lblBankingIdChange.text = "true";
             } else {
                 frmSecurityAlertsKA.imgBankingIdChangeKA.src = "checkbox_off.png";
                 frmSecurityAlertsKA.lblBankingIdChange.text = "false";
             }
         } else if (alertType === "passwordChange") {
             if (frmSecurityAlertsKA.imgPasswordChangeKA.src == "checkbox_off.png") {
                 frmSecurityAlertsKA.imgPasswordChangeKA.src = "checkbox_on.png";
                 frmSecurityAlertsKA.lblPasswordChange.text = "true";
             } else {
                 frmSecurityAlertsKA.imgPasswordChangeKA.src = "checkbox_off.png";
                 frmSecurityAlertsKA.lblPasswordChange.text = "false";
             }
         } else if (alertType === "passwordExpired") {
             if (frmSecurityAlertsKA.imgPasswordExpiredKA.src == "checkbox_off.png") {
                 frmSecurityAlertsKA.imgPasswordExpiredKA.src = "checkbox_on.png";
                 frmSecurityAlertsKA.lblPasswordExpired.text = "true";
             } else {
                 frmSecurityAlertsKA.imgPasswordExpiredKA.src = "checkbox_off.png";
                 frmSecurityAlertsKA.lblPasswordExpired.text = "false";
             }
         } else if (alertType === "addressPhoneChanged") {
             if (frmSecurityAlertsKA.imgAddressPhoneChangeKA.src == "checkbox_off.png") {
                 frmSecurityAlertsKA.imgAddressPhoneChangeKA.src = "checkbox_on.png";
                 frmSecurityAlertsKA.lblAddressPhoneChange.text = "true";
             } else {
                 frmSecurityAlertsKA.imgAddressPhoneChangeKA.src = "checkbox_off.png";
                 frmSecurityAlertsKA.lblAddressPhoneChange.text = "false";
             }
         } else if (alertType === "newPayeeAdded") {
             if (frmSecurityAlertsKA.imgNewPayeeAddedKA.src == "checkbox_off.png") {
                 frmSecurityAlertsKA.imgNewPayeeAddedKA.src = "checkbox_on.png";
                 frmSecurityAlertsKA.lbNewPayeeAdded.text = "true";
             } else {
                 frmSecurityAlertsKA.imgNewPayeeAddedKA.src = "checkbox_off.png";
                 frmSecurityAlertsKA.lbNewPayeeAdded.text = "false";
             }
         } else {
             if (frmSecurityAlertsKA.imgPayeeDetailsUpdatedKA.src == "checkbox_off.png") {
                 frmSecurityAlertsKA.imgPayeeDetailsUpdatedKA.src = "checkbox_on.png";
                 frmSecurityAlertsKA.lblPayeeDetailsUpdated.text = "true";
             } else {
                 frmSecurityAlertsKA.imgPayeeDetailsUpdatedKA.src = "checkbox_off.png";
                 frmSecurityAlertsKA.lblPayeeDetailsUpdated.text = "false";
             }
         }
     } //else //if android
     // logic to call update of alert
     var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
     var controller = INSTANCE.getFormController("frmSecurityAlertsKA");
     var navigationObject = new kony.sdk.mvvm.NavigationObject();
     navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
     navigationObject.setRequestOptions("form", {
         "headers": {
             "session_token": kony.retailBanking.globalData.session_token
         }
     });
     controller.performAction("saveData", [navigationObject]);
 }

 function updateDealsAlertStatus(alertType) {
     if (kony.retailBanking.globalData.deviceInfo.isIphone()) {
         if (alertType === "newDealsAvailable") {
             if (frmDealsAlertKA.NewDealsAlertSwitch.selectedIndex === 1) {
                 frmDealsAlertKA.lblNewDeals.text = "false";
             } else {
                 frmDealsAlertKA.lblNewDeals.text = "true";
             }
         } else {
             if (frmDealsAlertKA.DealsExpiringAlertSwitch.selectedIndex === 1) {
                 frmDealsAlertKA.lblDealsExpiring.text = "false";
             } else {
                 frmDealsAlertKA.lblDealsExpiring.text = "true";
             }
         }
     } else {
         if (alertType === "newDealsAvailable") {
             if (frmDealsAlertKA.imgNewDealsKA.src == "checkbox_off.png") {
                 frmDealsAlertKA.imgNewDealsKA.src = "checkbox_on.png";
                 frmDealsAlertKA.lblNewDeals.text = "true";
             } else {
                 frmDealsAlertKA.imgNewDealsKA.src = "checkbox_off.png";
                 frmDealsAlertKA.lblNewDeals.text = "false";
             }
         } else {
             if (frmDealsAlertKA.imgDealsExpiringKA.src == "checkbox_off.png") {
                 frmDealsAlertKA.imgDealsExpiringKA.src = "checkbox_on.png";
                 frmDealsAlertKA.lblDealsExpiring.text = "true";
             } else {
                 frmDealsAlertKA.imgDealsExpiringKA.src = "checkbox_off.png";
                 frmDealsAlertKA.lblDealsExpiring.text = "false";
             }
         }
     } //else //if android
     // logic to call update of alert
     var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
     var controller = INSTANCE.getFormController("frmDealsAlertKA");
     var navigationObject = new kony.sdk.mvvm.NavigationObject();
     navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
     navigationObject.setRequestOptions("form", {
         "headers": {
             "session_token": kony.retailBanking.globalData.session_token
         }
     });
     controller.performAction("saveData", [navigationObject]);
 }
 /*
   var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
 		  var controller = INSTANCE.getFormController("frmSecurityAlertsKA");
 		  var navigationObject = new kony.sdk.mvvm.NavigationObject();
     	  navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
 		  navigationObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
 		  controller.performAction("saveData",[navigationObject]);

            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
 		  var controller = INSTANCE.getFormController("frmSecurityAlertsKA");
 		  var navigationObject = new kony.sdk.mvvm.NavigationObject();
     	  navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
 		  navigationObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
 		  controller.performAction("saveData",[navigationObject]);
       	// frmAlertsKA.alertsTypes.isVisible=false;
      // deregisterPush();
        //  updateFlags("alerts",false);
 */
 function getAlertsHistoryList() {
     if (kony.sdk.isNetworkAvailable()) {
         try {
             kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
             var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
             var controller = INSTANCE.getFormController("frmAlertHistory");
             var navObject = new kony.sdk.mvvm.NavigationObject();
             var langSelected = kony.store.getItem("langPrefObj");
             var Language = langSelected.toUpperCase();
             navObject.setRequestOptions("segDetails", {
                 "headers": {
                     "session_token": kony.retailBanking.globalData.session_token
                 },
                 "queryParams": {
                     "custId": custid,
                     "Language": Language
                 }
             });
             kony.print("getAlertsHistoryList ::::" + JSON.stringify(navObject));
             controller.loadDataAndShowForm(navObject);
         } catch (err) {
             kony.print("error in getAlertsHistoryList :: " + err);
         }
     } else {
         kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
         customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
     }
 }

 function serv_DELETEALERTHISTORY() {
     try {
         popupCommonAlertDimiss();
         var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
         var options = {
             "access": "online",
             "objectName": "RBObjects"
         };
         var headers = {};
         var serviceName = "RBObjects";
         var modelObj = INSTANCE.getModel("Messages", serviceName, options);
         var dataObject = new kony.sdk.dto.DataObject("Messages");
         dataObject.addField("custId", custid);
         dataObject.addField("language", kony.store.getItem("langPrefObj").toUpperCase());
         var serviceOptions = {
             "dataObject": dataObject,
             "headers": headers
         };
         if (kony.sdk.isNetworkAvailable()) {
             kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
             modelObj.customVerb("deleteHistData", serviceOptions, function(res) {
                 kony.print(JSON.stringify(res));
                 kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                 if (res.statusCode === "S0024" && res.statusType === "S") {
                     frmAlertHistory.segDetails.removeAll();
                     frmAlertHistory.forceLayout();
                     frmAlertHistory.btnDeleteAlertHistory.setVisibility(false);
                     customAlertPopup(geti18Value("i18n.common.success"), geti18Value("i18n.alerts.deletealertsuccess"), popupCommonAlertDimiss, "");
                 } else if (res.statusType === "F") {
                     frmAlertHistory.btnDeleteAlertHistory.setVisibility(true);
                     customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.alerts.deletealertfailed"), popupCommonAlertDimiss, "");
                 }
             }, function(err) {
                 kony.print(JSON.stringify(err));
             });
         }
     } catch (e) {
         kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
         kony.print("Exception_serv_DELETEALERTHISTORY ::" + e);
     }
 }