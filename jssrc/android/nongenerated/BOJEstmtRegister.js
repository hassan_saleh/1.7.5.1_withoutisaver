function callGetEstmt() {
    p_sec_email_id = "";
    p_primary_email_id = "";
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("User", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("User");
        dataObject.addField("custId", custid);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("callGetEstmt :: serviceOptions " + JSON.stringify(serviceOptions));
        modelObj.customVerb("GetRegisteredEstmt", serviceOptions, callGetEstmtSuccessCallback, callGetEstmtErrorCallback);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        kony.application.dismissLoadingScreen();
    }
}

function callGetEstmtSuccessCallback(response) {
    kony.print("callGetEstmtSuccessCallback :: " + JSON.stringify(response));
    p_sec_email_id = "";
    p_primary_email_id = "";
    if (response.ErrorCode != undefined && response.ErrorCode != null && response.ErrorCode != "" && response.ErrorCode == "00000") {
        if (response.p_primary_email_id != undefined && response.p_primary_email_id != null && response.p_primary_email_id != "") {
            frmEstatementLandingKA.txtEmail1.text = response.p_primary_email_id;
            frmEstatementLandingKA.flxSwitchOnTouchLogin.setVisibility(true);
            frmEstatementLandingKA.flxSwitchOffTouchLogin.setVisibility(false);
        } else {
            frmEstatementLandingKA.flxSwitchOnTouchLogin.setVisibility(false);
            frmEstatementLandingKA.flxSwitchOffTouchLogin.setVisibility(true);
            frmEstatementLandingKA.txtEmail1.text = "";
        }
        if (response.p_sec_email_id != undefined && response.p_sec_email_id != null && response.p_sec_email_id != "") {
            frmEstatementLandingKA.txtEmail2.text = response.p_sec_email_id;
        } else {
            frmEstatementLandingKA.txtEmail2.text = "";
        }
    }
    kony.application.dismissLoadingScreen();
    frmEstatementLandingKA.show();
}

function callGetEstmtErrorCallback(error) {
    p_sec_email_id = "";
    p_primary_email_id = "";
    kony.print("callGetEstmtErrorCallback :: " + JSON.stringify(error));
    kony.application.dismissLoadingScreen();
    frmEstatementLandingKA.show();
}

function onNextofEstmtRegister() {
    if (frmEstatementLandingKA.lblNext.skin == "sknLblNextEnabled" && frmEstatementLandingKA.lblTermsandConditionsCheckBox.text === "p") {
        var Email1 = frmEstatementLandingKA.txtEmail1.text;
        var Email2 = frmEstatementLandingKA.txtEmail2.text;
        kony.print(Email1 + "  :: " + Email2);
        if (Email1 !== null && Email1 !== "" && isValidEmaill(Email1) && frmEstatementLandingKA.flxSwitchOnTouchLogin.isVisible) {
            if (Email2 !== null && Email2 !== "") {
                if (!(isValidEmaill(Email2))) {
                    frmEstatementLandingKA.borderBottom2.skin = "skntextFieldDivider";
                    frmEstatementLandingKA.lblInvalidEmail2.text = geti18Value("i18n.Estmt.invalidEmail");
                    frmEstatementLandingKA.lblInvalidEmail2.setVisibility(true);
                    frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
                } else {
                    //make service call with two first email
                    frmEstatementConfirmKA.show();
                }
            } else {
                //make service call with only first email
                frmEstatementConfirmKA.show();
            }
        } else {
            frmEstatementLandingKA.borderBottom1.skin = "skntextFieldDivider";
            frmEstatementLandingKA.lblInvalidEmail1.text = geti18Value("i18n.Estmt.invalidEmail");
            frmEstatementLandingKA.lblInvalidEmail1.setVisibility(true);
            frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
        }
    } else {
        frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
    }
}

function frmEstatementConfirmpreshpw() {
    var Email1 = frmEstatementLandingKA.txtEmail1.text;
    var Email2 = frmEstatementLandingKA.txtEmail2.text;
    if (Email1 !== "") {
        frmEstatementConfirmKA.flxEmailData1.setVisibility(true);
        frmEstatementConfirmKA.lblEamail1.text = Email1;
    }
    if (Email2 !== null && Email2 !== "") {
        frmEstatementConfirmKA.flxEmailData2.setVisibility(true);
        frmEstatementConfirmKA.lblEamail2.text = Email2;
    } else {
        frmEstatementConfirmKA.flxEmailData2.setVisibility(false);
        frmEstatementConfirmKA.lblEamail2.text = "";
    }
}

function registerEstmt() {
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("User", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("User");
        dataObject.addField("custId", custid);
        dataObject.addField("p_fcdb_ref_no", " ");
        dataObject.addField("p_primary_email_id", frmEstatementConfirmKA.lblEamail1.text);
        if (frmEstatementConfirmKA.flxEmailData2.isVisible) dataObject.addField("p_sec_email_id", frmEstatementConfirmKA.lblEamail2.text);
        else dataObject.addField("p_sec_email_id", "");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("registerEstmt :: serviceOptions " + JSON.stringify(serviceOptions));
        modelObj.customVerb("confirmEmailEstmt", serviceOptions, registerEstmtSuccessCallback, registerEstmtErrorCallback);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        kony.application.dismissLoadingScreen();
    }
}

function registerEstmtSuccessCallback(resObj) {
    kony.print("In success of registerEstmtSuccessCallback ::  " + JSON.stringify(resObj));
    if (resObj != null && resObj != undefined && resObj != "" && resObj.ErrorCode == "00000") {
        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.thankyou"), geti18Value("i18n.Estmt.SuccessNote"), geti18nkey("i18n.common.CgtAD"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.gotoSet"), "gotoSettings");
        frmEstatementLandingKA.destroy();
    } else {
        customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.defaultErrorMsg"), gotoSettings, "");
        frmEstatementLandingKA.destroy();
    }
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function registerEstmtErrorCallback(errObj) {
    kony.print("In Error of registerEstmtErrorCallback ::  " + JSON.stringify(errObj));
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.defaultErrorMsg"), gotoSettings, "");
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    try {
        logObj[16] = errObj.ErrorCode; // reference number
        logObj[17] = "FAILURE"; //status,
        logObj[18] = getErrorMessage(errObj.ErrorCode); // statuscomments
        loggerCall();
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        //do noting
    }
}