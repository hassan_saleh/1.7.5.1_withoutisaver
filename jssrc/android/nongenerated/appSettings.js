function isNullorEmptyorUndefined(value) {
    kony.print("Inside isNullorEmptyorUndefined........");
    try {
        return ((value === null) || (value === "null") || (value === undefined) || (value === ""));
    } catch (e) {
        kony.print("Exception found in isNullorEmptyorUndefined ::" + JSON.stringify(e));
        exceptionLogCall("isNullorEmptyorUndefined :: " + value, "UI ERROR", "UI", e);
    }
}

function doNothing() {}

function onLoadSettings() {
    kony.print("@@@Inside onLoadSettings...." + gblQbalShowFlag);
    try {
        if (subMenuFlag) {
            SETTINGS_GLOBALS.PROFILE = GENERIC_COSNTATNTS.FALSE;
            SETTINGS_GLOBALS.ACCOUNT = GENERIC_COSNTATNTS.FALSE;
            SETTINGS_GLOBALS.LANGUAGE = GENERIC_COSNTATNTS.FALSE;
            SETTINGS_GLOBALS.ALERT = GENERIC_COSNTATNTS.FALSE;
            SETTINGS_GLOBALS.ALTERNATELOGINS = GENERIC_COSNTATNTS.FALSE;
            SETTINGS_GLOBALS.CARD = GENERIC_COSNTATNTS.FALSE;
            toggleMenuOptions(PROFILE_CONSTANTS.PROFILE, SETTINGS_GLOBALS.PROFILE, PROFILE_CONSTANTS.MENU[0]);
            toggleMenuOptions(PROFILE_CONSTANTS.ACCOUNTPREFERENCES, SETTINGS_GLOBALS.ACCOUNT, PROFILE_CONSTANTS.MENU[1]);
            toggleMenuOptions(PROFILE_CONSTANTS.CHANGELANGUAGE, SETTINGS_GLOBALS.LANGUAGE, PROFILE_CONSTANTS.MENU[2]);
            toggleMenuOptions(PROFILE_CONSTANTS.ALERTANDNOTIFICATIONS, SETTINGS_GLOBALS.ALERT, PROFILE_CONSTANTS.MENU[4]);
            toggleMenuOptions(PROFILE_CONSTANTS.ALTERNATELOGINS, SETTINGS_GLOBALS.ALTERNATELOGINS, PROFILE_CONSTANTS.MENU[3]);
            toggleMenuOptions(PROFILE_CONSTANTS.CARDPREFERENCES, SETTINGS_GLOBALS.CARD, PROFILE_CONSTANTS.MENU[5]);
        }
        if (gblQbalShowFlag === "Y") {
            frmSettingsKA.flxQuickBalSwitchOn.isVisible = true;
            frmSettingsKA.flxQuickBalSwitchOff.isVisible = false;
        } else {
            frmSettingsKA.flxQuickBalSwitchOn.isVisible = false;
            frmSettingsKA.flxQuickBalSwitchOff.isVisible = true;
        }
        kony.print("Language setted ::" + kony.store.getItem("langPrefObj"));
        if (kony.store.getItem("langPrefObj") == "ar") {
            onSelection_LanguageSettings({
                "flxEnglishSwitchOn": false,
                "flxEnglishSwitchOff": true,
                "flxArabicSwitchOff": false,
                "flxArabicSwitchOn": true
            });
        } else if (kony.store.getItem("langPrefObj") == "en") {
            onSelection_LanguageSettings({
                "flxEnglishSwitchOn": true,
                "flxEnglishSwitchOff": false,
                "flxArabicSwitchOff": true,
                "flxArabicSwitchOn": false
            });
        }
        if (isDevRegDone === "T") {
            var alertOption = kony.store.getItem("SMSALERT");
            frmSettingsKA.flxSMSSwitchOff.isVisible = alertOption.isSMSDisabled;
            frmSettingsKA.flxSMSSwitchOn.isVisible = !alertOption.isSMSDisabled;
            frmSettingsKA.flxAlertsSwitchOff.isVisible = alertOption.isALERTDisabled;
            frmSettingsKA.flxAlertsSwitchOn.isVisible = !alertOption.isALERTDisabled;
        } else {
            frmSettingsKA.flxSMSSwitchOff.isVisible = false;
            frmSettingsKA.flxSMSSwitchOn.isVisible = true;
            frmSettingsKA.flxAlertsSwitchOff.isVisible = true;
            frmSettingsKA.flxAlertsSwitchOn.isVisible = false;
        }
        if (isDevRegDone == "F") {
            kony.store.removeItem("isPinSupported");
            kony.store.removeItem("isTouchSupported");
        }
        subMenuFlag = false;
        frmSettingsKA.flxSiri.isVisible = false;
        frmSettingsKA.flxseperator4.isVisible = false;
        // Commented for Siri
        var deviceInfoThing = getDeviceInfo();
        kony.print("getDeviceInfo:" + JSON.stringify(getDeviceInfo));
        var versionCompare = (checkVersionCompare("" + deviceInfoThing.version, "13.3.1") >= 0) ? true : false;
        kony.print("versionCompare:" + versionCompare);
        diffFlowFlag = "";
        //
        set_UI_FOR_CASA_USER(frmSettingsKA);
        //
    } catch (e) {
        subMenuFlag = false;
        kony.print("@@ Exception found in onLoadSettings... " + JSON.stringify(e));
        exceptionLogCall("onLoadSettings", "UI ERROR", "UI", e);
    }
}

function toggleMenuOptions(subMenuArr, flag, mainMenuName) {
    kony.print("Inside toggleMenuOptions...subMenuArr:: " + JSON.stringify(subMenuArr) + ":: flag:: " + flag);
    changeMainMenuSkin(mainMenuName, flag);
    try {
        for (var i = 0; i < subMenuArr.length; i++) {
            if (frmSettingsKA[GENERIC_COSNTATNTS.FLX + subMenuArr[i]] !== null && frmSettingsKA[GENERIC_COSNTATNTS.FLX + subMenuArr[i]] !== undefined) {
                frmSettingsKA[GENERIC_COSNTATNTS.FLX + subMenuArr[i]].setVisibility(flag);
            }
        }
    } catch (e) {
        kony.print("Exception found in toggleMenuOptions::" + JSON.stringify(e));
        exceptionLogCall("toggleMenuOptions", "UI ERROR", "UI", e);
    }
}

function changeMainMenuSkin(menuName, flag) {
    kony.print("@@@Inside changeMainmenuSkin...menuName" + menuName + ",flag:: " + flag);
    try {
        if (flag) {
            kony.print("@@@Inside If...menuName" + menuName + ",flag:: " + flag);
            if (frmSettingsKA[GENERIC_COSNTATNTS.FLX + menuName] !== null && frmSettingsKA[GENERIC_COSNTATNTS.FLX + menuName] !== undefined) {
                frmSettingsKA[GENERIC_COSNTATNTS.FLX + menuName].skin = "sknDarkMenu";
            }
            if (frmSettingsKA[GENERIC_COSNTATNTS.LBL + menuName + GENERIC_COSNTATNTS.RIGHTICON] !== null && frmSettingsKA[GENERIC_COSNTATNTS.FLX + menuName + GENERIC_COSNTATNTS.RIGHTICON] !== undefined) {
                kony.print("##changeMainMenuSkin:: " + GENERIC_COSNTATNTS.LBL + menuName + GENERIC_COSNTATNTS.RIGHTICON);
                frmSettingsKA[GENERIC_COSNTATNTS.LBL + menuName + GENERIC_COSNTATNTS.RIGHTICON].text = "d";
            }
        } else {
            //text:"o"
            kony.print("@@@Inside else...menuName" + menuName + ",flag:: " + flag);
            if (frmSettingsKA[GENERIC_COSNTATNTS.FLX + menuName] !== null && frmSettingsKA[GENERIC_COSNTATNTS.FLX + menuName] !== undefined) {
                frmSettingsKA[GENERIC_COSNTATNTS.FLX + menuName].skin = "slFbox";
            }
            if (frmSettingsKA[GENERIC_COSNTATNTS.LBL + menuName + GENERIC_COSNTATNTS.RIGHTICON] !== null && frmSettingsKA[GENERIC_COSNTATNTS.FLX + menuName + GENERIC_COSNTATNTS.RIGHTICON] !== undefined) {
                kony.print("##changeMainMenuSkin:: " + GENERIC_COSNTATNTS.LBL + menuName + GENERIC_COSNTATNTS.RIGHTICON);
                frmSettingsKA[GENERIC_COSNTATNTS.LBL + menuName + GENERIC_COSNTATNTS.RIGHTICON].text = kony.i18n.getLocalizedString("i18n.appsettings.more");
            }
        }
    } catch (e) {
        kony.print("@@ Exception found in changeMainMenuSkin::" + JSON.stringify(e));
        exceptionLogCall("changeMainMenuSkin", "UI ERROR", "UI", e);
    }
}

function checkVersionCompare(a, b) {
    var i, diff;
    var regExStrip0 = /(\.0+)+$/;
    var segmentsA = a.replace(regExStrip0, '').split('.');
    var segmentsB = b.replace(regExStrip0, '').split('.');
    var l = Math.min(segmentsA.length, segmentsB.length);
    for (i = 0; i < l; i++) {
        diff = parseInt(segmentsA[i], 10) - parseInt(segmentsB[i], 10);
        if (diff) {
            return diff;
        }
    }
    return segmentsA.length - segmentsB.length;
}

function onClickOfAltLoginFromMenu() {
    try {
        kony.print("here 1");
        var isPinSupported = kony.store.getItem("isPinSupported");
        var isTouchSupported = kony.store.getItem("isTouchSupported");
        if (!kony.retailBanking.globalData.deviceInfo.isTouchIDSupported()) {
            frmSettingsKA.flxTouchId.setVisibility(false);
            frmSettingsKA.flxTouchIDSwitchOff.setVisibility(true);
            frmSettingsKA.flxTouchIDSwitchOn.setVisibility(false);
        } else {
            PROFILE_CONSTANTS.ALTERNATELOGINS = ["TouchId", "PINLogin"];
            frmSettingsKA.flxTouchId.setVisibility(true);
            if (checkFaceIdIphone()) {
                frmSettingsKA.lblTouchID.text = geti18nkey("i18n.appsettings.FaceID");
            } else {
                frmSettingsKA.lblTouchID.text = geti18nkey("i18n.appsettings.TouchID");
            }
            kony.print("here 2");
            if (isTouchSupported !== null && isTouchSupported !== undefined && isTouchSupported !== "" && isTouchSupported == "Y") {
                kony.print("onClickOfAltLoginFromMenu isTouchSupported : " + isTouchSupported);
                frmSettingsKA.flxTouchIDSwitchOff.setVisibility(false);
                frmSettingsKA.flxTouchIDSwitchOn.setVisibility(true);
            } else {
                frmSettingsKA.flxTouchIDSwitchOff.setVisibility(true);
                frmSettingsKA.flxTouchIDSwitchOn.setVisibility(false);
            }
        }
        frmSettingsKA.flxPINLogin.setVisibility(true);
        if (isPinSupported !== null && isPinSupported !== undefined && isPinSupported !== "" && isPinSupported == "Y") {
            kony.print("onClickOfAltLoginFromMenu isPinSupported   : " + isPinSupported);
            frmSettingsKA.flxPinLoginSwitchOn.setVisibility(true);
            frmSettingsKA.flxPinLoginSwitchOff.setVisibility(false);
        } else {
            frmSettingsKA.flxPinLoginSwitchOn.setVisibility(false);
            frmSettingsKA.flxPinLoginSwitchOff.setVisibility(true);
        }
        toggleMenuOptions(PROFILE_CONSTANTS.ALTERNATELOGINS, SETTINGS_GLOBALS.ALTERNATELOGINS, PROFILE_CONSTANTS.MENU[3]);
    } catch (e) {
        kony.print("errir " + e);
    }
}

function onClickTOuchIDSet() {
    if (isDevRegDone === "T") {
        var status = kony.localAuthentication.getStatusForAuthenticationMode(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID);
        kony.print("Status for the touch/faceid :: " + status);
        if (status == 5007 || status == 5005) {
            if (checkFaceIdIphone()) {
                customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.FaceID.notEnabledText"), popupCommonAlertDimiss, "");
            } else {
                customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.TOuch.notEnabledText"), popupCommonAlertDimiss, "");
            }
        } else {
            var isTouchSupported = kony.store.getItem("isTouchSupported");
            if (isTouchSupported !== null && isTouchSupported !== undefined && isTouchSupported !== "" && isTouchSupported == "Y") {
                frmUserSettingsTouchIdKA.flxSwitchOffTouchLogin.setVisibility(false);
                frmUserSettingsTouchIdKA.flxSwitchOnTouchLogin.setVisibility(true);
            } else {
                frmUserSettingsTouchIdKA.flxSwitchOnTouchLogin.setVisibility(false);
                frmUserSettingsTouchIdKA.flxSwitchOffTouchLogin.setVisibility(true);
            }
            frmUserSettingsTouchIdKA.flxtouchIdAndrd.setVisibility(false);
            var model = kony.os.deviceInfo().model;
            if (checkFaceIdIphone()) {
                frmUserSettingsTouchIdKA.lblTouchIdHeader.text = geti18nkey("i18n.common.FaceID");
                frmUserSettingsTouchIdKA.lblTouchBodytxt.text = geti18nkey("i18n.common.FaceIDText");
                frmUserSettingsTouchIdKA.lblAltText.text = geti18nkey("i18n.common.FaceIDEnable");
            } else {
                frmUserSettingsTouchIdKA.lblTouchIdHeader.text = geti18nkey("i18n.common.touchID");
                frmUserSettingsTouchIdKA.lblTouchBodytxt.text = geti18nkey("i18n.common.touchText");
                frmUserSettingsTouchIdKA.lblAltText.text = geti18nkey("i18n.common.touchIDEnable");
            }
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            frmUserSettingsTouchIdKA.show();
        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        //customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.deviceNotRegistered.QuickBal"),popupCommonAlertDimiss,"");
        //1933 fix
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.deviceNotRegistered.TouchID"), onClickRegister, onClickCancel, geti18Value("i18n.login.Register"), geti18Value("i18n.common.cancelC"));
        //end
    }
}

function onClickPinIDSet() {
    if (isDevRegDone === "T") {
        //go to change pin form
        frmUserSettingsPinLoginKA.show();
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        //customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.deviceNotRegistered.QuickBal"),popupCommonAlertDimiss,"");
        //1933 fix
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.deviceNotRegistered.PinID"), onClickRegister, onClickCancel, geti18Value("i18n.login.Register"), geti18Value("i18n.common.cancelC"));
        //end
    }
}
//added new method for 1933 fix
function onClickRegister() {
    getDeviceList();
    popupCommonAlertDimiss();
}

function onClickCancel() {
    popupCommonAlertDimiss();
}
//end
function backfromSettings() {
    kony.print("gblEntryFormForSettings :: " + gblEntryFormForSettings);
    switch (gblEntryFormForSettings) {
        case "Accounts":
            frmAccountsLandingKA.flxDeals.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
            frmAccountsLandingKA.flxAccountsMain.left = "0%";
            frmAccountsLandingKA.flxLoans.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
            frmAccountsLandingKA.flxDeals.zIndex = 1;
            frmAccountsLandingKA.flxLoans.zIndex = 1;
            frmAccountsLandingKA.forceLayout();
            accountDashboardDataCall();
            break;
        case "Cards":
            onClickCardsMenu();
            break;
        case "Paymentdashboard":
            frmPaymentDashboard.show();
            break;
        case "More":
            frmMore.show();
            break;
        default:
            frmPaymentDashboard.show();
    }
}

function onClickSiriSet() {
    if (isDevRegDone === "T") {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        frmUserSettingsSiri.show();
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.deviceNotRegistered.QuickBal"), popupCommonAlertDimiss, "");
    }
}