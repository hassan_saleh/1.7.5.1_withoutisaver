kony = kony || {};
kony.rb = kony.rb || {};
kony.rb.configurationManager = function() {
    this.amountValues = ['50', '100', '150', '300', '500', '1000'];
    this.maritalStatus = [
        ['Single', 'Single'],
        ['Married', 'Married'],
        ['Widowed', 'Widowed'],
        ['Divorced', 'Divorced']
    ];
    this.numberOfDependents = [
        ['3', '3'],
        ['2', '2'],
        ['1', '1']
    ];
    this.employmentType = [
        ['Salaried', 'Salaried'],
        ['Self Employed', 'Self Employed']
    ];
    this.experience = [
        ['3 years', '3 years'],
        ['2 years', '2 years'],
        ['1 year', '1 year']
    ];
    this.gender = ['Male', 'Female'];
    this.maritalStatusValues = ['Single', 'Married', 'Widowed', 'Divorced'];
    this.numberOfDependentsValues = ['00', '01', '02', '03', '04', '05', '06', '07', '08'];
    this.employmentInfoYears = [
        ['0year', '1year', 0],
        ['1year', '2years', 1],
        ['2years', '3years', 2],
        ['3years', '4years', 3],
        ['4years', '5years', 4],
        ['5years', '6years', 5],
        ['6years', '7years', 6],
        ['7years', '8years', 7],
        ['8years', '9years', 8],
        ['9years', '10years', 9],
        ['10years', '11years', 10]
    ];
    this.annualIncome = [
        ['0', '25000', 0],
        ['25001', '50000', 1],
        ['50001', '100000', 2],
        ['100001', '200000', 3],
        ['200001', '300000', 4],
        ['300001', '500000', 5],
        ['500001', '1200000', 6],
        ['1200001', '9999999', 7]
    ];
    this.assets = [
        ['0', '25000', 0],
        ['25001', '50000', 1],
        ['50001', '100000', 2],
        ['100001', '200000', 3],
        ['200001', '300000', 4],
        ['300001', '500000', 5],
        ['500001', '1200000', 6],
        ['1200001', '9999999', 7]
    ];
    this.monthlyExpenditure = [
        ['0', '25000', 0],
        ['25001', '50000', 1],
        ['50001', '100000', 2],
        ['100001', '200000', 3],
        ['200001', '300000', 4],
        ['300001', '500000', 5],
        ['500001', '1200000', 6],
        ['1200001', '9999999', 7]
    ];
    this.employmentTypeValues = ['Employed', 'Unemployed', 'Retired', 'Student'];
    this.countryCodes = {
        "AF": "+93",
        "AL": "+355",
        "DZ": "+213",
        "AS": "+1-684",
        "AD": "+376",
        "AO": "+244",
        "AI": "+1-264",
        "AQ": "+672",
        "AG": "+1-268",
        "AR": "+54",
        "AM": "+374",
        "AW": "+297",
        "AU": "+61",
        "AT": "+43",
        "AZ": "+994",
        "BS": "+1-242",
        "BH": "+973",
        "BD": "+880",
        "BB": "+1-246",
        "BY": "+375",
        "BE": "+32",
        "BZ": "+501",
        "BJ": "+229",
        "BM": "+1-441",
        "BT": "+975",
        "BO": "+591",
        "BA": "+387",
        "BW": "+267",
        "BV": "",
        "BR": "+55",
        "IO": "",
        "BN": "+673",
        "BG": "+359",
        "BF": "+226",
        "BI": "+257",
        "KH": "+855",
        "CM": "+237",
        "CA": "+1",
        "CV": "+238",
        "KY": "+1-345",
        "CF": "+236",
        "TD": "+235",
        "CL": "+56",
        "CN": "+86",
        "CX": "+53",
        "CC": "+61",
        "CO": "+57",
        "KM": "+269",
        "CD": "+243",
        "CG": "+242",
        "CK": "+682",
        "CR": "+506",
        "CI": "+225",
        "HR": "+385",
        "CU": "+53",
        "CY": "+357",
        "CZ": "+420",
        "CS": "",
        "DK": "+45",
        "DJ": "+253",
        "DM": "+1-767",
        "DO": "+1-809",
        "TP": "+670",
        "EC": "+593 ",
        "EG": "+20",
        "SV": "+503",
        "GQ": "+240",
        "ER": "+291",
        "EE": "+372",
        "ET": "+251",
        "FK": "+500",
        "FO": "+298",
        "FJ": "+679",
        "FI": "+358",
        "FR": "+33",
        "GF": "+594",
        "PF": "+689",
        "TF": "",
        "GA": "+241",
        "GM": "+220",
        "GE": "+995",
        "DE": "+49",
        "GH": "+233",
        "GI": "+350",
        "GB": "",
        "GR": "+30",
        "GL": "+299",
        "GD": "+1-473",
        "GP": "+590",
        "GU": "+1-671",
        "GT": "+502",
        "GN": "+224",
        "GW": "+245",
        "GY": "+592",
        "HT": "+509",
        "HM": "",
        "VA": "",
        "HN": "+504",
        "HK": "+852",
        "HU": "+36",
        "IS": "+354",
        "IN": "+91",
        "ID": "+62",
        "IR": "+98",
        "IQ": "+964",
        "IE": "+353",
        "IL": "+972",
        "IT": "+39",
        "JM": "+1-876",
        "JP": "+81",
        "JO": "+962",
        "KZ": "+7",
        "KE": "+254",
        "KI": "+686",
        "KP": "+850",
        "KR": "+82",
        "KW": "+965",
        "KG": "+996",
        "LA": "+856",
        "LV": "+371",
        "LB": "+961",
        "LS": "+266",
        "LR": "+231",
        "LY": "+218",
        "LI": "+423",
        "LT": "+370",
        "LU": "+352",
        "MO": "+853",
        "MK": "+389",
        "MG": "+261",
        "MW": "+265",
        "MY": "+60",
        "MV": "+960",
        "ML": "+223",
        "MT": "+356",
        "MH": "+692",
        "MQ": "+596",
        "MR": "+222",
        "MU": "+230",
        "YT": "+269",
        "MX": "+52",
        "FM": "+691",
        "MD": "+373",
        "MC": "+377",
        "MN": "+976",
        "MS": "+1-664",
        "MA": "+212",
        "MZ": "+258",
        "MM": "+95",
        "NA": "+264",
        "NR": "+674",
        "NP": "+977",
        "NL": "+31",
        "AN": "+599",
        "NC": "+687",
        "NZ": "+64",
        "NI": "+505",
        "NE": "+227",
        "NG": "+234",
        "NU": "+683",
        "NF": "+672",
        "MP": "+1-670",
        "NO": "+47",
        "OM": "+968",
        "PK": "+92",
        "PW": "+680",
        "PS": "+970",
        "PA": "+507",
        "PG": "+675",
        "PY": "+595",
        "PE": "+51",
        "PH": "+63",
        "PN": "",
        "PL": "+48",
        "PT": "+351",
        "PR": "+1-787",
        "QA": "+974 ",
        "RE": "+262",
        "RO": "+40",
        "SU": "",
        "RU": "+7",
        "RW": "+250",
        "SH": "+290",
        "KN": "+1-869",
        "LC": "+1-758",
        "PM": "+508",
        "VC": "+1-784",
        "WS": "+685",
        "SM": "+378",
        "ST": "+239",
        "SA": "+966",
        "RS": "",
        "SN": "+221",
        "SC": "+248",
        "SL": "+232",
        "SG": "+65",
        "SK": "+421",
        "SI": "+386",
        "SB": "+677",
        "SO": "+252",
        "ZA": "+27",
        "GS": "",
        "ES": "+34",
        "LK": "+94",
        "SD": "+249",
        "SR": "+597",
        "SJ": "",
        "SZ": "+268",
        "SE": "+46",
        "CH": "+41",
        "SY": "+963",
        "TW": "+886",
        "TJ": "+992",
        "TZ": "+255",
        "TH": "+66",
        "TG": "",
        "TK": "+690",
        "TO": "+676",
        "TT": "+1-868",
        "TE": "",
        "TN": "+216",
        "TR": "+90",
        "TM": "+993",
        "TC": "+1-649",
        "TV": "+688",
        "UG": "+256",
        "UA": "+380",
        "AE": "+971",
        "GB": "+44",
        "US": "+1",
        "UM": "",
        "UY": "+598",
        "UZ": "+998",
        "VU": "+678",
        "VA": "+418",
        "VE": "+58",
        "VN": "+84",
        "VI": "+1-284",
        "VQ": "+1-340",
        "WF": "+681",
        "EH": "",
        "YE": "+967",
        "YU": "",
        "ZR": "",
        "ZM": "+260",
        "ZW": "+263",
    };
};
kony.rb.configurationManager.prototype.getAmountValuesforAtm = function() {
    return this.amountValues;
};
kony.rb.configurationManager.prototype.getMaritalStatusForNewUser = function() {
    return this.maritalStatus;
};
kony.rb.configurationManager.prototype.getNumberOfDependentsForNewUser = function() {
    return this.numberOfDependents;
};
kony.rb.configurationManager.prototype.getEmploymentTypeForNewUser = function() {
    return this.employmentType;
};
kony.rb.configurationManager.prototype.getExperienceForNewUser = function() {
    return this.experience;
};
kony.rb.configurationManager.prototype.getAnnualIncomeForNewUser = function() {
    return this.annualIncome;
};
kony.rb.configurationManager.prototype.getAssestsForNewUser = function() {
    return this.assets;
};
kony.rb.configurationManager.prototype.getMonthlyExpenditureForNewUser = function() {
    return this.monthlyExpenditure;
};
kony.rb.configurationManager.prototype.getGenderForNewUser = function() {
    return this.gender;
};
kony.rb.configurationManager.prototype.getMaritalStatusValuesForNewUser = function() {
    return this.maritalStatusValues;
};
kony.rb.configurationManager.prototype.getNumberOfDependentsValuesForNewUser = function() {
    return this.numberOfDependentsValues;
};
kony.rb.configurationManager.prototype.getEmploymentTypeValuesForNewUser = function() {
    return this.employmentTypeValues;
};
kony.rb.configurationManager.prototype.getCountryCode = function(country) {
    return this.countryCodes[country];
};
kony.rb.configurationManager.prototype.getEmploymentInfoYearsForNewUser = function() {
    return this.employmentInfoYears;
};