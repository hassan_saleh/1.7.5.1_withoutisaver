var pin_count_login = 0;
var login_pass = "";
var first_login_pass = "";
var loginType;

function check_Login_Pin(i) {
    frmLoginKA.pinReTry.setVisibility(true);
    frmLoginKA.pinReTry.text = " ";
    frmLoginKA.PinEntryLabel.setVisibility(true);
    pin_count_login++;
    if (pin_count_login <= 6) {
        //frmLoginKA.btnCancel.setVisibility(false);
        frmLoginKA.clearLink.setVisibility(true);
        login_pass = login_pass + i;
        frmLoginKA["flxProgressButton" + pin_count_login].skin = "sknFlxProgressButtonFill";
    }
    if (pin_count_login === 6) {
        loginType = "pin";
        kony.sdk.mvvm.LoginAction("", "", "P", custid, login_pass, "");
        //kony.sdk.mvvm.pinLogin(login_pass);
    }
}

function clearBtnClickedFromLogin() { //on click of clear button
    if (pin_count_login > 0) {
        login_pass = login_pass.substring(0, login_pass.length - 1);
        frmLoginKA["flxProgressButton" + pin_count_login].skin = "sknFlxProgressButtonEmpty";
        pin_count_login--;
        if (pin_count_login === 0) {
            ////frmLoginKA.btnCancel.setVisibility(true);
            frmLoginKA.clearLink.setVisibility(true);
        }
    } else {
        // //frmLoginKA.btnCancel.setVisibility(true);
        frmLoginKA.clearLink.setVisibility(true);
    }
}

function clearProgressFlexLogin() { //changes all filled skin to empty skins
    for (var i = 6; i >= 1; i--) {
        frmLoginKA["flxProgressButton" + i].skin = "sknFlxProgressButtonEmpty";
    }
    pin_count_login = 0;
    frmLoginKA.clearLink.isVisible = true;
    //frmLoginKA.btnCancel.isVisible=true;
}

function backBtnClickedFromLogin() {
    try {
        login_pass = "";
        pin_count_login = 0;
        clearProgressFlexLogin();
    } catch (Error) {
        kony.print("Exception While getting exiting the application  : " + Error);
    }
}

function callback_ofanim() {
    frmLoginKA.flxProgressButtons.animate(kony.ui.createAnimation({
        "100": {
            "left": "35%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_BACKWARDS,
        "duration": 0.07
    }, {});
}

function animation_wrong_pin() {
    frmLoginKA.flxProgressButtons.animate(kony.ui.createAnimation({
        "100": {
            "left": "20%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_BACKWARDS,
        "duration": 0.12
    }, {
        "animationEnd": function() {}
    });
}