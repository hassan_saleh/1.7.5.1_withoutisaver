/*
Author : Charan Kadari
Date : 12-02-18
Desc : PreShow of own accounts
*/
var gblFeesFrom = "";

function gotoNewTransferScreen(val) {
    try {
        switch (val) {
            case 1:
                //       frmNewTransferKA.lblCahngeCurr.setEnabled(false);
                frmNewTransferKA.flxAcc2.setEnabled(false);
                frmNewTransferKA.flxAcc1.setEnabled(true);
                frmNewTransferKA.flxAcc1.onClick = onClickFromAcct;
                frmNewTransferKA.btnForward1.onClick = onClickFromAcct;
                frmNewTransferKA.lblIcon1.skin = sknLblFromIcon;
                frmConfirmTransferKA.transFlag.text = "T";
                var d = new Date();
                //gblFrequency = geti18Value("i18n.Transfers.Instant");
                frmNewTransferKA.lblFrom.text = geti18Value("i18n.cashWithdraw.from");
                frmNewTransferKA.lblTo.text = geti18Value("i18n.common.To");
                frmNewTransferKA.calFrom.date = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
                //frmNewTransferKA.calFrom.validEndDate = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
                frmNewTransferKA.calFrom.validStartDate = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
                frmNewTransferKA.calTo.validStartDate = ([d.getDate() + 1, d.getMonth() + 1, d.getFullYear()]);
                frmNewTransferKA.calFrom.setEnabled(true);
                frmNewTransferKA.flxRecurrence.setVisibility(false);
                frmNewTransferKA.flxFrequency.setVisibility(true);
                frmNewTransferKA.lblCurrency.setVisibility(false);
                frmNewTransferKA.calTo.date = ([d.getDate() + 1, d.getMonth() + 1, d.getFullYear()]);
                //       frmNewTransferKA.show();
                check_numberOfAccounts();
                break;
            case 2:
                break;
        }
    } catch (e) {
        exceptionLogCall("::gotoNewTransferScreen::", "values assigning to UI", "UI", e);
    }
}

function onClickFromAcct() {
    var amount = frmNewTransferKA.txtBox.text;
    if (amount === null || amount === "" || amount == "." || (Number(amount) < 0.01) || amount === 0.00) {
        kony.print("Amount is either null or empty");
    } else {
        frmNewTransferKA.txtBox.text = frmNewTransferKA.lblHiddenAmount.text;
        frmNewTransferKA.txtBox.text = formatAmountwithcomma(amount, frmNewTransferKA.lblDecimal.text);
    }
    gotoAccountDetailsScreen(1);
}
/*
Author : Vineeth Vishwanath
Date : 16-02-18
Desc : To set the "to" date based on recurrence val
*/
function onTextChangeRecurrence() {
    var frequency = gblFrequency;
    var currDate = frmNewTransferKA.calFrom.day;
    var currMonth = frmNewTransferKA.calFrom.month;
    var currYear = frmNewTransferKA.calFrom.year;
    if (frmNewTransferKA.txtNumRecurrences.text === "") {
        frmNewTransferKA.calTo.date = ([currDate + 1, currMonth, currYear]);
        frmNewTransferKA.calTo.validStartDate = ([currDate + 1, currMonth, currYear]);
        frmNewTransferKA.calTo.setEnabled(true);
        if (frequency === geti18Value("i18n.Transfers.OneTime")) {
            var currdate = new Date();
            if (parseInt(currMonth) == (currdate.getMonth() + 1)) {
                frmConfirmTransferKA.lblFreType.text = "0";
            } else {
                frmConfirmTransferKA.lblFreType.text = "";
            }
        }
        return;
    } else {
        frmNewTransferKA.calTo.setEnabled(false);
    }
    var num = frmNewTransferKA.txtNumRecurrences.text;
    num = parseInt(num);
    if (num == 0 && frequency !== geti18Value("i18n.Transfers.OneTime")) {
        frmNewTransferKA.lblInvalidRecurrence.setVisibility(true);
        frmNewTransferKA.CopylblLine0iff8accf05a247.skin = "sknFlxOrangeLine";
        return;
    } else {
        frmNewTransferKA.lblInvalidRecurrence.setVisibility(false);
        frmNewTransferKA.CopylblLine0iff8accf05a247.skin = "lblLine0h8b99d8a85f649";
    }
    //var d = new Date(currYear, currMonth, currDate);
    if (frequency == geti18Value("i18n.transfers.Daily")) {
        frmConfirmTransferKA.lblFreType.text = "0";
        var d = new Date(currYear, currMonth - 1, currDate);
        num = parseInt(num) - 1;
        d.setDate(d.getDate() + num);
        frmNewTransferKA.calTo.date = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
    } else if (frequency == geti18Value("i18n.transfers.MonthlyOnce")) {
        frmConfirmTransferKA.lblFreType.text = "1";
        var total = parseInt(currMonth) + parseInt(num) - 1;
        kony.print("total " + total);
        kony.print("num " + num);
        kony.print("currDate " + currDate);
        kony.print("currYear " + currYear);
        if (total > 12) {
            num = total % 12;
            var year = total / 12;
            // added for 1940
            kony.print(currDate);
            kony.print(num);
            kony.print(year);
            kony.print(parseInt(year));
            if ((num == parseInt(4) || num == parseInt(6) || num == parseInt(9) || num == parseInt(11)) && (currDate == parseInt(31))) {
                currDate = currDate - 1;
                frmNewTransferKA.calTo.date = ([currDate, num, currYear + parseInt(year)]);
            } else if (num == parseInt(2)) {
                var FYear = currYear + parseInt(year);
                if (currDate == parseInt(31)) {
                    if ((parseInt(0) == parseInt(FYear) % 4) && (parseInt(0) != parseInt(FYear) % 100) || (parseInt(0) == parseInt(FYear) % 400)) {
                        currDate = currDate - 2;
                    } else {
                        currDate = currDate - 3;
                    }
                    frmNewTransferKA.calTo.date = ([currDate, num, currYear + parseInt(year)]);
                } else if (currDate == parseInt(30)) {
                    if ((parseInt(0) == parseInt(FYear) % 4) && (parseInt(0) != parseInt(FYear) % 100) || (parseInt(0) == parseInt(FYear) % 400)) {
                        currDate = currDate - 1;
                    } else {
                        currDate = currDate - 2;
                    }
                    frmNewTransferKA.calTo.date = ([currDate, num, currYear + parseInt(year)]);
                } else if (currDate == parseInt(29)) {
                    if ((parseInt(0) == parseInt(FYear) % 4) && (parseInt(0) != parseInt(FYear) % 100) || (parseInt(0) == parseInt(FYear) % 400)) {
                        currDate = currDate;
                    } else {
                        currDate = currDate - 1;
                    }
                    frmNewTransferKA.calTo.date = ([currDate, num, currYear + parseInt(year)]);
                } else {
                    frmNewTransferKA.calTo.date = ([currDate, num, currYear + parseInt(year)]);
                }
            } else if (num == parseInt(0)) {
                num = 12;
                frmNewTransferKA.calTo.date = ([currDate, num, currYear + parseInt(year) - parseInt(1)]);
            }
            //end
            else {
                frmNewTransferKA.calTo.date = ([currDate, num, currYear + parseInt(year)]);
            }
            // frmNewTransferKA.calTo.date = ([currDate, num , currYear + parseInt(year)]);
        }
        //added for 1940
        else if (total == parseInt(2)) {
            if ((parseInt(0) == currYear % 4) && (parseInt(0) != currYear % 100) || (parseInt(0) == currYear % 400)) {
                if (currDate == parseInt(30)) {
                    currDate = currDate - 1;
                    frmNewTransferKA.calTo.date = ([currDate, total, currYear]);
                } else if (currDate == parseInt(31)) {
                    currDate = currDate - 2;
                    frmNewTransferKA.calTo.date = ([currDate, total, currYear]);
                } else {
                    frmNewTransferKA.calTo.date = ([currDate, total, currYear]);
                }
            } else {
                if (currDate == parseInt(29)) {
                    currDate = currDate - 1;
                    frmNewTransferKA.calTo.date = ([currDate, total, currYear]);
                } else if (currDate == parseInt(30)) {
                    currDate = currDate - 2;
                    frmNewTransferKA.calTo.date = ([currDate, total, currYear]);
                } else if (currDate == parseInt(31)) {
                    currDate = currDate - 3;
                    frmNewTransferKA.calTo.date = ([currDate, total, currYear]);
                } else {
                    frmNewTransferKA.calTo.date = ([currDate, total, currYear]);
                }
            }
        } else if ((total == parseInt(4) || total == parseInt(6) || total == parseInt(9) || total == parseInt(11)) && (currDate == parseInt(31))) {
            currDate = currDate - 1;
            frmNewTransferKA.calTo.date = ([currDate, total, currYear]);
        }
        //end
        else {
            frmNewTransferKA.calTo.date = ([currDate, total, currYear]);
        }
    } else if (frequency == geti18Value("i18n.transfers.WeeklyOnce")) {
        frmConfirmTransferKA.lblFreType.text = "2";
        var d = new Date(currYear, currMonth - 1, currDate);
        num = (num - 1) * 7;
        d.setDate(d.getDate() + num);
        frmNewTransferKA.calTo.date = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
    } else if (frequency == geti18Value("i18n.transfers.biweekly")) {
        frmConfirmTransferKA.lblFreType.text = "3";
        if (num != 1) {
            var d = new Date(currYear, currMonth - 1, currDate);
            num = (num - 1) * 15;
            d.setDate(d.getDate() + num);
            frmNewTransferKA.calTo.date = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
        } else {
            frmNewTransferKA.calTo.date = ([currDate, currMonth, currYear]);
        }
    } else if (frequency == geti18Value("i18n.transfers.Quarterly")) {
        frmConfirmTransferKA.lblFreType.text = "4";
        if (num == 1) {
            frmNewTransferKA.calTo.date = ([currDate, currMonth, currYear]);
            return;
        }
        num = (num - 1) * 3;
        var total = parseInt(currMonth) + parseInt(num);
        if (total > 12) {
            num = total % 12;
            var year = total / 12;
            frmNewTransferKA.calTo.date = ([currDate, num, currYear + parseInt(year)]);
        } else {
            frmNewTransferKA.calTo.date = ([currDate, total, currYear]);
        }
    } else if (frequency == geti18Value("i18n.transfers.semiannual")) {
        frmConfirmTransferKA.lblFreType.text = "5";
        if (num == 1) {
            frmNewTransferKA.calTo.date = ([currDate, currMonth, currYear]);
            return;
        }
        num = (num - 1) * 6;
        var total = parseInt(currMonth) + parseInt(num);
        if (total > 12) {
            num = total % 12;
            var year = total / 12;
            frmNewTransferKA.calTo.date = ([currDate, num, currYear + parseInt(year)]);
        } else {
            frmNewTransferKA.calTo.date = ([currDate, total, currYear]);
        }
    } else if (frequency === geti18Value("i18n.transfers.annual")) {
        frmConfirmTransferKA.lblFreType.text = "6";
        frmNewTransferKA.calTo.date = ([currDate, currMonth, currYear + num - 1]);
    }
    frmNewTransferKA.calTo.setEnabled(false);
}
/*
Author : Vineeth Vishwanath
Date : 12-02-18
Desc : preShow for Send money
*/
function preShowNewTransfer() {
    kony.print("gblSelectedBene ::" + JSON.stringify(gblSelectedBene));
    //   frmNewTransferKA.lblToAccCurr.text = "JOD";
    frmNewTransferKA.lblCurrency.setVisibility(true);
    frmNewTransferKA.lblFrom.text = geti18Value("i18n.common.To");
    frmNewTransferKA.lblTo.text = geti18Value("i18n.common.fromc");
    frmNewTransferKA.flxAcc1.onClick = doNothing;
    frmNewTransferKA.btnForward1.onClick = doNothing;
    frmNewTransferKA.lblSelectanAccount1.setVisibility(false);
    frmNewTransferKA.lblAccountName1.setVisibility(true);
    frmNewTransferKA.lblAccountNumber1.setVisibility(true);
    frmNewTransferKA.flxAcc2.setEnabled(true);
    frmNewTransferKA.flxIcon1.setVisibility(true);
    //frmNewTransferKA.CopyLabel0i74ab774c6354b.setVisibility(true);
    frmNewTransferKA.lblSelectanAccount2.setVisibility(true);
    frmNewTransferKA.lblAccountName1.text = gblSelectedBene.beneficiaryName;
    //gblSelectedBene.currency;//To account for send money
    if (!isEmpty(frmNewTransferKA.lblAccountName2.text)) {
        frmNewTransferKA.lblSelectanAccount2.setVisibility(false);
    }
    frmNewTransferKA.lblAccountNumber1.text = gblSelectedBene.benAcctNo;
    if (gblSelectedBene.benType === "IFB") {
        if (!isEmpty(gblDefCurr) && !frmNewTransferKA.lblSelectanAccount2.isVisible && (frmNewTransferKA.lblToAccCurr.text != gblDefCurr)) {
            frmNewTransferKA.lblCahngeCurr.setEnabled(true);
        } else {
            frmNewTransferKA.lblCahngeCurr.setEnabled(false);
        }
        frmConfirmTransferKA.transFlag.text = "T";
        frmNewTransferKA.flxPurpose.setVisibility(false);
        frmNewTransferKA.flxFrequency.setVisibility(true);
        frmNewTransferKA.Label0g6814cba9e7841.text = geti18Value("i18n.Bene.BOJ");
    }
    if (gblSelectedBene.benType === "ITB") {
        frmNewTransferKA.lblCahngeCurr.setEnabled(true);
        frmConfirmTransferKA.transFlag.text = "S";
        frmNewTransferKA.flxFrequency.setVisibility(true);
        frmNewTransferKA.flxPurpose.setVisibility(true);
        //frmNewTransferKA.flxRecurrence.setVisibility(false);
        frmNewTransferKA.Label0g6814cba9e7841.text = geti18Value("i18n.Transfers.InternationalPayment");
    }
    if (gblSelectedBene.benType === "DTB") {
        frmNewTransferKA.lblCahngeCurr.setEnabled(true);
        frmConfirmTransferKA.transFlag.text = "S";
        frmNewTransferKA.flxPurpose.setVisibility(true);
        frmNewTransferKA.flxFrequency.setVisibility(true);
        //frmNewTransferKA.flxRecurrence.setVisibility(false);
        frmNewTransferKA.Label0g6814cba9e7841.text = geti18Value("i18n.Transfers.DomesticPayment");
    }
    //frmNewTransferKA.Label0fc18f6b7f83d4d.setVisibility(false);
    if (gblSelectedBene.isFav == "Y") {
        //alert("Done");
        frmNewTransferKA.flxIcon1.skin = sknFlxFav;
        //     frmNewTransferKA.lblIcon1.onTouchEnd = onClickFav;
        frmNewTransferKA.lblIcon1.skin = sknLblToIcon;
        frmNewTransferKA.lblIcon1.text = "k";
    } else {
        frmNewTransferKA.flxIcon1.skin = sknFlxToIcon;
        //     frmNewTransferKA.lblIcon1.onTouchEnd = doNothing;
        frmNewTransferKA.flxIcon1.backgroundColor = kony.boj.getBackGroundColour(gblSelectedBene.initial);
        frmNewTransferKA.lblIcon1.skin = sknLblFromIcon;
        frmNewTransferKA.lblIcon1.text = gblSelectedBene.initial;
    }
    if (!isEmpty(gblLaunchModeOBJ.lauchMode) && gblLaunchModeOBJ.lauchMode) {
        if (!isEmpty(gblSelectedBene)) kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }
}

function onClickFav() {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.Transfer.rmFav"), onClickYesRemFav, onClickNoRemFav, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function onClickYesRemFav() {
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("ExternalAccounts", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("ExternalAccounts");
        //     dataObject.addField("usr", "icbsserv");
        //     dataObject.addField("pass", "icbsserv_1");
        dataObject.addField("custId", custid);
        dataObject.addField("accountNumber", gblSelectedBene.benAcctNo);
        dataObject.addField("isFav", (gblSelectedBene.isFav === "Y") ? "N" : "Y");
        dataObject.addField("P_BENEF_ID", gblSelectedBene.benef_id);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("serviceOptions ::" + JSON.stringify(serviceOptions));
        if (kony.sdk.isNetworkAvailable()) {
            //       kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb("update", serviceOptions, function(res) {
                kony.print("success ::" + JSON.stringify(res));
            }, function(err) {
                kony.print("Failed ::" + JSON.stringify(err));
            });
        }
        popupCommonAlertDimiss();
    }

    function onClickNoRemFav() {
        popupCommonAlertDimiss();
    }
}

function doNothing() {
    kony.print("Do Nothing");
}
/*
Author : Charan Kadari/Vineeth Vishwanath
Date : 12-02-18
Desc : setting data for Account details
*/
function gotoAccountDetailsScreen(val) {
    //1960 fix
    var fromAccounts = "";
    var toAccounts = "";
    var accountsData = "";
    kony.print("accountsData" + kony.retailBanking.globalData.accountsDashboardData.accountsData);
    if (kony.boj.siri === "fromSiri" || kony.boj.JoMoPayQR === "fromJoMoPayQR" || gblTModule === "IPSRegistration") {
        accountsData = kony.retailBanking.globalData.accountsDashboardData.accountsData;
    } else {
        fromAccounts = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
        toAccounts = kony.retailBanking.globalData.accountsDashboardData.toAccounts.slice(0);
    }
    kony.print("fromAccounts :: " + fromAccounts);
    kony.print("toAccounts :: " + toAccounts);
    if (val === 1) {
        if (kony.newCARD.applyCardsOption === true || kony.application.getCurrentForm().id === "frmCardLinkedAccounts" || kony.boj.selectedBillerType === "PrePaid" || kony.boj.siri === "fromSiri" || kony.boj.selectedBillerType === "PostPaid" || kony.boj.JoMoPayQR === "fromJoMoPayQR") {
            frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.common.accountNumber");
        } else {
            frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyFrom");
        }
        kony.store.setItem("toval", 1);
        if ((kony.application.getCurrentForm().id === "frmNewBillKA" && frmNewBillKA.btnBillsPayCards.text == "t") || (kony.application.getCurrentForm().id === "frmBills" && frmBills.btnBillsPayCards.text == "t") || (kony.application.getCurrentForm().id === "frmNewBillDetails" && frmNewBillDetails.btnBillsPayCards.text == "t") || (kony.application.getCurrentForm().id === "frmPrePaidPayeeDetailsKA" && frmPrePaidPayeeDetailsKA.btnBillsPayCards.text == "t")) {
            if (kony.retailBanking.globalData.prCreditCardPayList.length > 0) {
                var tempdata = [];
                var cardData = kony.retailBanking.globalData.prCreditCardPayList;
                var cc = false;
                for (var i = 0; i < cardData.length; i++) {
                    kony.print("cardTypeFlag ::" + cardData[i].cardTypeFlag);
                    if (cardData[i].cardTypeFlag === "C") {
                        cc = true;
                        tempdata.push(cardData[i]);
                    }
                }
                frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.creditcard.cardnumber");
                if (cc) {
                    accountsScreenPreshow(tempdata);
                    frmAccountDetailsScreen.show();
                } else customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.nocards"), popupCommonAlertDimiss, "");
            } else customVerb_CARDSDETAILS();
        } else {
            if (kony.application.getCurrentForm().id === "frmCardLinkedAccounts") {
                var data = frmCardLinkedAccounts.segCardLinkedAccounts.data;
                var temp = [];
                for (var i in data) {
                    if (data[i].flxLinkedAccountsEnable.isVisible === true) {
                        for (var j in fromAccounts) {
                            if (fromAccounts[j].accountID === data[i].lblAccountNumber.text) {
                                temp.push(fromAccounts[j]);
                                break;
                            }
                        }
                    }
                }
                accountsScreenPreshow(temp);
            }
            //added for 1960  and jomopay using QRCode changes
            else {
                if (kony.boj.siri === "fromSiri" || kony.boj.JoMoPayQR === "fromJoMoPayQR") {
                    kony.print("loding accounts for siri");
                    accountsScreenPreshow(accountsData);
                } else if (gblTModule === "CLIQ") {
                    accountsData = get_ACCOUNTS_FOR_CLIQ(fromAccounts);
                    if (!isEmpty(accountsData)) accountsScreenPreshow(accountsData);
                } else if (gblTModule === "IPSRegistration") {
                    accountsData = get_ACCOUNTS_FOR_CLIQ(fromAccounts);
                    if (!isEmpty(accountsData)) accountsScreenPreshow(accountsData);
                } else {
                    accountsScreenPreshow(fromAccounts);
                }
            }
            frmAccountDetailsScreen.show();
        }
        //frmNewTransferKA.flxAcc2.setEnabled(true);
    }
    if (val == 2) {
        kony.store.setItem("toval", 0);
        if (gblTModule !== "send") {
            frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyTo");
            var i = kony.store.getItem("toAccount");
            for (var j in toAccounts) {
                if (toAccounts[j].accountID === i.accountID) {
                    toAccounts.splice(j, 1);
                    break;
                }
            }
            accountsScreenPreshow(toAccounts);
            frmAccountDetailsScreen.show();
        } else {
            var data = [];
            frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyFrom");
            accountsScreenPreshow(fromAccounts);
            frmAccountDetailsScreen.show();
        }
        //alert(JSON.stringify("todata "+i+"index "+j+"data "+data));
    }
}
/*
Author : Charan Kadari/Vineeth Vishwanath
Date : 12-02-18
Desc : setting data for Account details screen
*/
function accountsScreenPreshow(data) {
    try {
        kony.print("data" + JSON.stringify(data));
        //alert("data" + JSON.stringify(data));
        var anum = "";
        var tempData = [];
        for (var i = 0; i < data.length; i++) {
            //kony.print("i ::"+i+" :: nameOnCard ::"+data[i].name_on_card);
            var accountNumber = data[i].accountID || data[i].card_num;
            anum = "";
            if (data[i].name_on_card !== undefined && data[i].name_on_card !== "undefined") {
                if (!isEmpty(data[i].nick_name)) anum = data[i].nick_name;
                else anum = data[i].name_on_card;
            } else {
                anum = data[i].accountName;
                if (data[i].AccNickName !== "" && data[i].AccNickName !== undefined) {
                    anum = data[i].AccNickName;
                }
            }
            kony.print("account name ::" + anum.length);
            if (anum.length > 28) data[i].anum = anum.substring(0, 28) + "...";
            else data[i].anum = anum;
            //data[i].hiddenAccountNumber =  data[i].accountName +"****"+ accountNumber.substring(accountNumber.length-3, accountNumber.length);
            if (data[i].card_num !== undefined || data[i].card_number !== undefined) {
                data[i].hiddenAccountNumber = mask_CreditCardNumberFirstSix(data[i].card_num);
                kony.print("data[i].hiddenAccountNumber::" + JSON.stringify(data[i].hiddenAccountNumber));
            } else data[i].hiddenAccountNumber = (data[i].AccNickName !== undefined && data[i].AccNickName !== "") ? "" : accountNumber; //"***"+ accountNumber.substring(accountNumber.length-3, accountNumber.length);    
            data[i].balance = formatAmountwithcomma((data[i].availableBalance || data[i].spending_limit), getDecimalNumForCurr(data[i].currencyCode || "JOD")) + " " + (data[i].currencyCode || "JOD");
            if (!isEmpty(data[i].accountType)) {
                if (data[i].accountType == "C") {
                    data[i].imgIcon = "current_account.png";
                    if (gblTModule === "AccountOrder" && data[i].producttype === "301") tempData.push(data[i]);
                } else if (data[i].accountType == "S") {
                    data[i].imgIcon = "savings_account.png";
                } else if (data[i].accountType == "D") {
                    data[i].imgIcon = "deposit.png";
                } else if (data[i].accountType == "L") {
                    data[i].imgIcon = "loan.png";
                }
            } else {
                data[i].imgIcon = data[i].card_image; //set_IMAGE_FOR_CARD(data[i].card_category,data[i].card_type)[0];
            }
            if ((gblTModule === "jomoReg" || gblTModule === "jomo") && data[i].currencyCode === "JOD") {
                tempData.push(data[i]);
            }
            if (gblTModule === "IPSRegistration" && data[i].currencyCode === "JOD") { /* Hassan Cardless */
                tempData.push(data[i]);
            }
            if ((gblTModule === "CardLessTransaction" && data[i].currencyCode === "JOD") && (data[i].accountType === "S" || data[i].accountType === "C")) { /* Hassan Cardless 02/04/2021*/
                tempData.push(data[i]);
            }
            if (gblTModule === "OpenTermDeposite" && (data[i].currencyCode === "JOD" || data[i].currencyCode === "USD")) {
                tempData.push(data[i]);
            } // hassan OpenTermDeposite
            if (gblTModule === "OpenTermDepositInterestAccount") {
                kony.print("Currency is::" + frmOpenTermDeposit.lblDepositCurrency.text);
                if (frmOpenTermDeposit.lblDepositCurrency.text === data[i].currencyCode) {
                    tempData.push(data[i]);
                }
            }
            //formatAmountwithcomma(frmNewTransferKA.lblHiddenAmount.text,frmNewTransferKA.lblDecimal.text);
        }
        frmAccountDetailsScreen.segFre.widgetDataMap = {
            lblAccountName: "anum",
            lblAccountNumber: "hiddenAccountNumber",
            lblAmount: "balance",
            imgIcon: "imgIcon"
        };
        kony.print("Account Data ::" + JSON.stringify(data));
        // hassan gblTModule ==="OpenTermDeposite"
        if (gblTModule === "AccountOrder" || gblTModule === "jomoReg" || gblTModule === "jomo" || gblTModule === "OpenTermDeposite" || gblTModule === "CardLessTransaction" || gblTModule === "OpenTermDepositInterestAccount" /* Hassan Cardless */ ) {
            frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.common.accountNumber");
            frmAccountDetailsScreen.segFre.setData(tempData);
        } else frmAccountDetailsScreen.segFre.setData(data);
    } catch (e) {
        exceptionLogCall("::accountsScreenPreshow::", "Exception while assiging values to form", "UI", e);
    }
}
/*
Author : Charan Kadari
Date : 12-02-18
Desc : On selection of an account from account details screen
*/
function onClickAccountDetailsSegment() {
    try {
        var AccDetailsSegdata = frmAccountDetailsScreen.segFre.data;
        var dataSelected = frmAccountDetailsScreen.segFre.selectedRowItems[0];
        kony.print(dataSelected.availableBalance);
        var text;
        var isNoFund = false;
        kony.print("Selected Account Data ::" + JSON.stringify(dataSelected));
        toval = kony.store.getItem("toval");
        if (toval === 1) {
            if (dataSelected.availableBalance <= 0) {
                isNoFund = true;
            }
            //     }else if(parseInt(dataSelected.availableBalance).toFixed() === 0 || parseInt(dataSelected.availableBalance).toFixed() === "0"){
            //     	isNoFund = true;
            if (isNoFund && kony.newCARD.applyCardsOption === false && kony.application.getPreviousForm().id !== "frmCardLinkedAccounts") {
                customAlertPopup(geti18Value("i18n.common.Information"), geti18Value("i18n.errorCode.54"), popupCommonAlertDimiss, "", geti18Value("i18n.NUO.OKay"), "");
                isNoFund = false;
                return;
            }
        }
        kony.print("gblTModule ::" + gblTModule);
        if (kony.newCARD.applyCardsOption) {
            selected_OPTIONS_APPLYNEWCARDS([{
                "lblJP": dataSelected.accountID,
                "branchNumber": dataSelected.branchNumber
            }], "AccountNumber");
        } else if (kony.application.getPreviousForm().id === "frmCardLinkedAccounts") {
            onSelection_DEFAULT_ACCOUNTS(dataSelected.accountID);
        } else if (gblTModule == "own") {
            set_formOwnTransferData(dataSelected);
        } else if (gblTModule == "send") {
            set_formTransferData(dataSelected);
        } else if (gblTModule == "jomoReg") {
            kony.print("dataSelected::" + JSON.stringify(dataSelected));
            //     frmJoMoPayRegistration.lblAccountType.text=dataSelected.accountName;
            frmJoMoPayRegistration.lblAccountCode.text = dataSelected.hiddenAccountNumber;
            if (dataSelected.AccNickName !== "" && dataSelected.AccNickName !== undefined) {
                frmJoMoPayRegistration.lblAccountType.text = dataSelected.AccNickName;
                frmJoMoPayRegistration.lblAccountCode.isVisible = false;
            } else {
                frmJoMoPayRegistration.lblAccountType.text = (dataSelected.accountName.length > 22) ? (dataSelected.accountName.substring(0, 22) + "...") : dataSelected.accountName;
                frmJoMoPayRegistration.lblAccountCode.isVisible = true;
            }
            var branchNum = dataSelected.branchNumber;
            if (branchNum.length === 1) {
                branchNum = "00" + branchNum;
            } else if (branchNum.length === 2) {
                branchNum = "0" + branchNum;
            }
            var appendedData = branchNum + "" + dataSelected.accountID;
            frmJoMoPayRegistration.lblFromAccountNumber.text = appendedData;
            frmJoMoPayRegistration.lblfromAccountCurrency.text = dataSelected.currencyCode; //getCurrencyCode_FROM_CURRENCY_ISO(dataSelected.currencyCode);
            frmJoMoPayRegistration.show();
        } else if (gblTModule === "BillPay") {
            set_formNewPayBill(dataSelected);
        } else if (gblTModule === "BillPayNewBillKA") {
            set_formNewPayBill(dataSelected);
            frmNewBillKA.show();
        } else if (gblTModule == "BillPrePayNewBillKA") { /*hassan prepaid*/
            set_formNewPayBill(dataSelected);
            frmPrePaidPayeeDetailsKA.show();
        } else if (gblTModule === "BulkPrePayPayment") { // hassan bulkPayment
            set_formNewPayBill(dataSelected);
            frmBulkPaymentKA.show();
            if (kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD") {
                var amount = frmBulkPaymentKA.lblSelBillsAmt.text;
                amount = amount.substring(0, amount.length - 4);
                serv_BILLSCOMISSIONCHARGE(amount.replace(/,/g, ""));
            } else {
                frmBulkPaymentKA.flxConversionAmt.setVisibility(false);
                if (parseFloat(frmBulkPaymentKA.lblHiddenSelBillsAmt.text) <= parseFloat(dataSelected.availableBalance)) {
                    frmBulkPaymentKA.lblNext.skin = "sknLblNextEnabled";
                } else {
                    customAlertPopup(geti18Value("i18n.common.Information"), geti18Value("i18n.errorCode.54"), popupCommonAlertDimiss, "", geti18Value("i18n.NUO.OKay"), "");
                    frmBulkPaymentKA.lblNext.skin = "sknLblNextDisabled";
                }
            }
        } else if (gblTModule === "CardLessTransaction") { /*Hassan Cardless*/
            set_formCardlessTransaction(dataSelected);
            frmCardlessTransaction.show();
        } /*Hassan Cardless*/
        else if (gblTModule === "OpenTermDeposite") {
            kony.print("hassan 161616 :: ");
            frmOpenTermDeposit.show()
        } // hassan OpenTermDeposite
        else if (gblTModule === "OpenTermDepositInterestAccount") {
            frmOpenTermDeposit.show()
        } //Nart OpenTermDepositInterestAccount
        else if (gblTModule == "jomo") {
            kony.print("dataSelected::" + JSON.stringify(dataSelected));
            frmJoMoPay.lblAccountType.text = dataSelected.accountName;
            frmJoMoPay.lblAccountCode.text = dataSelected.hiddenAccountNumber;
            if (dataSelected.AccNickName !== "" && dataSelected.AccNickName !== undefined) {
                frmJoMoPay.lblAccountType.text = dataSelected.AccNickName;
                frmJoMoPay.lblAccountCode.isVisible = false;
            } else {
                frmJoMoPay.lblAccountType.text = (dataSelected.accountName.length > 22) ? (dataSelected.accountName.substring(0, 22) + "...") : dataSelected.accountName;
                frmJoMoPay.lblAccountCode.isVisible = true;
            }
            var branchNum = dataSelected.branchNumber;
            if (branchNum.length === 1) {
                branchNum = "00" + branchNum;
            } else if (branchNum.length === 2) {
                branchNum = "0" + branchNum;
            }
            var appendedData = branchNum + "" + dataSelected.accountID;
            frmJoMoPayConfirmation.lblFromAccountNumber.text = appendedData;
            //     frmJoMoPayConfirmation.lblFromAccountNumber.text="0010013010000050005";
            frmJoMoPayConfirmation.lblSourceBranchCode.text = dataSelected.branchNumber;
            frmJoMoPayConfirmation.lblStaticJOD.text = dataSelected.currencyCode;
            frmJoMoPay.lblCurrency.text = dataSelected.currencyCode;
            kony.print("dataSelected.currencyCode::" + dataSelected.currencyCode);
            frmJoMoPayConfirmation.lblfromAccountCurrency.text = dataSelected.currencyCode;
            frmJoMoPay.show();
        } else if (gblTModule == "web") {
            set_formWebChargeTopUp(dataSelected);
            frmWebCharge.show();
        } else if (gblTModule === "BillPayBillers") {
            set_formRegisteredPayBill(dataSelected);
        } else if (gblTModule === "statistics") {}
        //1960 fix
        else if (gblTModule === "AccountsForPayments" || gblTModule === "AccountsForTransfer") {
            kony.print("gblTModule   from siri is " + gblTModule);
            set_formSiriData(dataSelected);
            kony.print("after setting ");
            frmUserSettingsSiri.show();
        }
        //end
        //jomopay using QRCode changes
        else if (gblTModule === "AccountsForJoMoPayQR") {
            kony.print("gblTModule   from Jomopay QRCode is " + gblTModule);
            set_frmJoMoPayQRConfirmData(dataSelected);
            kony.print("after setting ");
            frmJoMoPayQRConfirm.show();
        }
        //end
        else if (gblTModule === "IPSRegistration") {
            set_ACCOUNT_LIST_IPS_REGISTRATION(dataSelected);
        } else if (gblTModule === "CLIQ") {
            set_SELECTED_ACCOUNT_TO_CLIQ_TRANS(dataSelected);
        } else {
            set_formCreditCardPayment(dataSelected);
            frmCreditCardPayment.show();
        }
    } catch (e) {
        exceptionLogCall("::onClickAccountDetailsSegment::", "Exception while assigning values, on click of segment", "UI", e);
    }
}
//jomopay using QRCode changes
function set_frmJoMoPayQRConfirmData(dataSelected) {
    try {
        kony.print("Selected accounts for Jomopay QR::" + JSON.stringify(dataSelected));
        var name = "";
        var accountNumber = "";
        if (dataSelected.accountName !== null && dataSelected.accountName !== undefined) {
            if (dataSelected.AccNickName !== "" && dataSelected.AccNickName !== null) {
                name = dataSelected.AccNickName;
                accountNumber = dataSelected.accountNumber;
            } else {
                name = dataSelected.accountName;
                accountNumber = dataSelected.accountNumber;
            }
        } else {
            name = dataSelected.name_on_card;
            accountNumber = dataSelected.accountNumber;
        }
        kony.print("name from set_frmJoMoPayQRConfirmData " + name + "accountNumber " + accountNumber);
        if (gblTModule === "AccountsForJoMoPayQR") {
            frmJoMoPayQRConfirm.lblAccountType.text = accountNumber; //name;
            kony.store.setItem("JoMoPayQRCodefromAcc", dataSelected);
        }
        kony.print("frmJoMoPayQRConfirm.lblAccountType.text  " + frmJoMoPayQRConfirm.lblAccountType.text);
    } catch (e) {
        kony.print("Exception_set_frmJoMoPayQRConfirmData ::" + e);
        exceptionLogCall("::set_frmJoMoPayQRConfirmData::", "new bill payment account assigning", "UI", e);
    }
}
//end
//1960 fix
function set_formSiriData(dataSelected) {
    try {
        kony.print("Selected accounts for Siri::" + JSON.stringify(dataSelected));
        var name = "";
        var accountNumber = "";
        if (dataSelected.accountName !== null && dataSelected.accountName !== undefined) {
            if (dataSelected.AccNickName !== "" && dataSelected.AccNickName !== null) {
                name = dataSelected.AccNickName;
                accountNumber = dataSelected.accountNumber;
            } else {
                name = dataSelected.accountName;
                accountNumber = dataSelected.accountNumber;
            }
        } else {
            name = dataSelected.name_on_card;
            accountNumber = dataSelected.accountNumber;
        }
        kony.print("name from set_formSiriData " + name);
        if (gblTModule === "AccountsForTransfer") {
            frmUserSettingsSiri.lblAccountTransfer.text = name;
            gblDefaultAccTransfer = accountNumber;
            //gblAccTransferSiri=accountNumber;
        } else if (gblTModule === "AccountsForPayments") {
            frmUserSettingsSiri.labelAccountPayments.text = name;
            gblDefaultAccPayment = accountNumber;
            //gblAccPaymentSiri=accountNumber;
        }
        kony.print("frmUserSettingsSiri.lblAccountTransfer.text  " + frmUserSettingsSiri.lblAccountTransfer.text);
        kony.print("frmUserSettingsSiri.labelAccountPayments.text  " + frmUserSettingsSiri.labelAccountPayments.text);
        kony.print("selected accountNumber in set_formSiriData " + accountNumber);
    } catch (e) {
        kony.print("Exception_set_formSiriData ::" + e);
        exceptionLogCall("::set_formSiriData::", "new bill payment account assigning", "UI", e);
    }
}
//end
/*
Author : Charan Kadari
Date : 12-02-18
Desc : setting the account values
*/
var accFromCurr = "";

function setAccountDetailstoFlex(val, dataSelected) {
    if (val === 1) //Own transfer from account
    {
        var x = kony.store.getItem("frmAccount");
        if (frmNewTransferKA.lblAccountName2.text === "") frmNewTransferKA.lblSelectanAccount2.setVisibility(true);
        else frmNewTransferKA.lblSelectanAccount2.setVisibility(false);
        if (!(x === "nulldataselected" || x === null || x === undefined))
            if (dataSelected.accountID === x.accountID) {
                frmNewTransferKA.lblAccountName2.text = "";
                frmNewTransferKA.flxIcon2.setVisibility(false);
                frmNewTransferKA.lblSelectanAccount2.setVisibility(true);
                frmNewTransferKA.lblAccountNumber2.text = "";
                frmNewTransferKA.lblToAccCurr.text = "";
                frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false);
            }
        frmNewTransferKA.lblAccountName1.text = dataSelected.accountName;
        frmNewTransferKA.lblAccountNumber1.text = dataSelected.accountID;
        accFromCurr = dataSelected.currencyCode;
        if (dataSelected.AccNickName !== "" && dataSelected.AccNickName !== undefined) {
            frmNewTransferKA.lblAccountNumber1.setVisibility(false);
        } else {
            frmNewTransferKA.lblAccountNumber1.setVisibility(true);
        }
        gblFromAccCurrency = "";
        gblFromAccCurrency = dataSelected.currencyCode;
        if (gblTModule === "own" && (dataSelected.accountName !== "" && dataSelected.accountName !== null)) showLetterPicture(dataSelected.accountName, val);
        frmNewTransferKA.flxAcc2.setEnabled(true);
        frmNewTransferKA.lblSelectanAccount1.setVisibility(false);
        frmNewTransferKA.lblCurrency.text = dataSelected.currencyCode;
        if (frmNewTransferKA.lblCurrency.text === "USD" || frmNewTransferKA.lblCurrency.text === "EUR") {
            frmNewTransferKA.txtBox.placeholder = "0.00";
            if (kony.store.getItem("langPrefObj") !== "ar") {
                frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.01" + " " + frmNewTransferKA.lblCurrency.text;
            } else {
                frmNewTransferKA.flxInvalidAmountField.text = frmNewTransferKA.lblCurrency.text + " 0.01" + " " + geti18Value("i18n.transfer.minAmount");
            }
        } else {
            frmNewTransferKA.txtBox.placeholder = "0.000";
            if (kony.store.getItem("langPrefObj") !== "ar") {
                frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.001" + " " + frmNewTransferKA.lblCurrency.text;
            } else {
                frmNewTransferKA.flxInvalidAmountField.text = frmNewTransferKA.lblCurrency.text + " 0.001" + " " + geti18Value("i18n.transfer.minAmount");
            }
        }
        frmNewTransferKA.lblFromAccCurr.text = dataSelected.currencyCode;
        frmNewTransferKA.txtBox.text = formatAmountwithcomma(frmNewTransferKA.lblHiddenAmount.text, frmNewTransferKA.lblDecimal.text);
        checkPlaceholder(frmNewTransferKA.lblDecimal.text);
        frmNewTransferKA.lblCurrency.setVisibility(true);
        if (frmNewTransferKA.lblAccountName2.text !== null && frmNewTransferKA.lblAccountName2.text !== "") {
            gblFeesFrom = "fromAcc";
            getCommisionFee();
        }
    } else {
        //frmNewTransferKA.FlexContainer0b7dc31ecc5cf40
        frmNewTransferKA.lblAccountName2.text = dataSelected.accountName;
        frmNewTransferKA.lblAccountNumber2.text = dataSelected.accountID;
        if (dataSelected.AccNickName !== "" && dataSelected.AccNickName !== undefined) {
            frmNewTransferKA.lblAccountNumber2.isVisible = false;
        } else {
            frmNewTransferKA.lblAccountNumber2.isVisible = true;
        }
        frmNewTransferKA.lblToAccCurr.text = dataSelected.currencyCode;
        var fromAccount = kony.store.getItem("toAccount");
        frmNewTransferKA.lblCurrency.text = fromAccount.currencyCode;
        frmNewTransferKA.lblFromAccCurr.text = fromAccount.currencyCode;
        frmNewTransferKA.lblToAccCurr.text = kony.store.getItem("frmAccount").currencyCode;
        frmNewTransferKA.lblCurrency.setVisibility(true);
        if (frmNewTransferKA.lblCurrency.text === "USD" || frmNewTransferKA.lblCurrency.text === "EUR") {
            frmNewTransferKA.txtBox.placeholder = "0.00";
            if (kony.store.getItem("langPrefObj") !== "ar") {
                frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.01" + " " + frmNewTransferKA.lblCurrency.text;
            } else {
                frmNewTransferKA.flxInvalidAmountField.text = frmNewTransferKA.lblCurrency.text + " 0.01" + " " + geti18Value("i18n.transfer.minAmount");
            }
        } else {
            frmNewTransferKA.txtBox.placeholder = "0.000";
            if (kony.store.getItem("langPrefObj") !== "ar") {
                frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.001" + " " + frmNewTransferKA.lblCurrency.text;
            } else {
                frmNewTransferKA.flxInvalidAmountField.text = frmNewTransferKA.lblCurrency.text + " 0.001" + " " + geti18Value("i18n.transfer.minAmount");
            }
        }
        frmNewTransferKA.lblSelectanAccount2.setVisibility(false);
        if (gblTModule === "own" && (dataSelected.accountName !== "" && dataSelected.accountName !== null)) showLetterPicture(dataSelected.accountName, val);
        frmNewTransferKA.lblSelectanAccount2.setVisibility(false);
        gblFeesFrom = "fromAcc";
        getCommisionFee();
        if (kony.store.getItem("toAccount").currencyCode !== null & kony.store.getItem("frmAccount").currencyCode !== null) {
            if (kony.store.getItem("toAccount").currencyCode !== kony.store.getItem("frmAccount").currencyCode) {
                frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(true); //ahmad 26/11/2020
            } else {
                frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false); //ahmad 26/11/2020
            }
        }
    }
}
/*
Author : Charan Kadari
Date : 12-02-18
Desc : preshow for frequendcy screen
*/
function frequencyListPreshow() {
    var freqData = [{
            "Frequency": geti18Value("i18n.Transfers.Instant"),
        }, {
            "Frequency": geti18Value("i18n.Transfers.OneTime"),
        }, {
            "Frequency": geti18Value("i18n.transfers.Daily"),
        },
        //     {
        //       "Frequency" : geti18Value("i18n.transfers.WeeklyOnce"),
        //     }
        //     {
        //       "Frequency" : geti18Value("i18n.transfers.biweekly"),
        //     },
        {
            "Frequency": geti18Value("i18n.transfers.MonthlyOnce"),
        },
        //     {
        //       "Frequency" : geti18Value("i18n.transfers.Quarterly"),
        //     },
        //     {
        //       "Frequency" : geti18Value("i18n.transfers.semiannual"),
        //     },
        //     {
        //       "Frequency" : geti18Value("i18n.transfers.annual"),
        //     },*/
    ];
    frmFrequencyList.segFrequencyList.widgetDataMap = {
        lblfrequency: "Frequency"
    };
    frmFrequencyList.segFrequencyList.setData(freqData);
}
/*
Author : Charan Kadari
Date : 12-02-18
Desc : on selection of frequendcy
*/
function selectFrequency() {
    var x = frmFrequencyList.segFrequencyList.selectedRowItems[0].Frequency;
    frmNewTransferKA.lblFre.text = frmFrequencyList.segFrequencyList.selectedRowItems[0].Frequency;
    gblFrequency = x;
    kony.print("hassan frequecy " + frmNewTransferKA.lblFre.text);
    if (x === geti18Value("i18n.Transfers.Instant")) {
        frmConfirmTransferKA.lblFreType.text = "";
        frmNewTransferKA.txtNumRecurrences.text = "";
        frmConfirmTransferKA.payMode.text = "0";
        frmNewTransferKA.flxRecurrence.setVisibility(false);
        //frmNewTransferKA.flxFrequency.setVisibility(false);
    } else if (x === geti18Value("i18n.Transfers.OneTime")) {
        var d = new Date();
        frmNewTransferKA.txtNumRecurrences.text = "";
        frmNewTransferKA.calFrom.date = ([d.getDate() + 1, d.getMonth() + 1, d.getFullYear()]);
        frmNewTransferKA.calFrom.validStartDate = ([d.getDate() + 1, d.getMonth() + 1, d.getFullYear()]);
        frmConfirmTransferKA.payMode.text = "1";
        frmNewTransferKA.lblDateRangeBoj.text = geti18Value("i18n.transfers.Date");
        frmNewTransferKA.flxRecurrence.height = "13%";
        frmNewTransferKA.flxDates.height = "90%";
        //to do things invisible and visible on selecting One time
        frmNewTransferKA.lblInvalidRecurrence.setVisibility(false);
        frmNewTransferKA.lblRecurrence.setVisibility(false);
        frmNewTransferKA.CopyflxFrequency0d4dacf1e8f8646.setVisibility(false);
        frmNewTransferKA.CopylblLine0iff8accf05a247.setVisibility(false);
        frmNewTransferKA.flxRecurrence.setVisibility(true);
        frmNewTransferKA.flxDates.setVisibility(true);
        frmNewTransferKA.frmToDate.setVisibility(false);
        frmNewTransferKA.flxFrequency.setVisibility(true);
    } else {
        var d = new Date();
        frmNewTransferKA.calFrom.date = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
        frmNewTransferKA.calTo.date = ([d.getDate() + 1, d.getMonth() + 1, d.getFullYear()]);
        frmNewTransferKA.calFrom.validStartDate = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
        frmNewTransferKA.calTo.validStartDate = ([d.getDate() + 1, d.getMonth() + 1, d.getFullYear()]);
        frmConfirmTransferKA.payMode.text = "1";
        frmNewTransferKA.lblDateRangeBoj.text = geti18Value("i18n.transfers.DateRange");
        frmNewTransferKA.flxRecurrence.height = "43%";
        frmNewTransferKA.flxDates.height = "40%";
        frmNewTransferKA.lblRecurrence.setVisibility(true);
        frmNewTransferKA.CopyflxFrequency0d4dacf1e8f8646.setVisibility(true);
        frmNewTransferKA.CopylblLine0iff8accf05a247.setVisibility(true);
        frmNewTransferKA.flxRecurrence.setVisibility(true);
        frmNewTransferKA.flxDates.setVisibility(true);
        frmNewTransferKA.frmToDate.setVisibility(true);
    }
    onTextChangeRecurrence();
    frmNewTransferKA.show();
}
/*
Author : Charan Kadari
Date : 12-02-18
Desc : Formatting the amount
*/
function formatAmountwithcomma(x, limit) {
    var text = "";
    if (gblTModule == "web") {
        text = frmWebCharge.lblCurrencyCode.text;
    } else if (gblTModule == "own" || gblTModule == "send") {
        text = frmNewTransferKA.lblCurrency.text;
    } else {
        text = "JOD";
    }
    //   if(text !== null  && text !== ""){
    if (x === "" || x === null || x === undefined) {
        //alert("x null");
        return "";
    }
    if (x === ".") {
        return x;
    }
    if (limit === "" || limit === null || limit === undefined) {
        //alert("limit null");
        limit = 0;
    }
    var a = x + "";
    a = a.split(".");
    a[0] = a[0].replace(/\,/g, '');
    var val = a[0] + "." + a[1];
    val = parseFloat(val);
    var t = Number.parseFloat(val).toFixed(limit);
    var parts = t.toString().split(".");
    var y = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
    kony.print("y ::" + y);
    return y;
    //   }
}

function checkPlaceholder(num) {
    if (num == "3") {
        frmNewTransferKA.txtBox.placeholder = "0.000";
        if (kony.store.getItem("langPrefObj") !== "ar") {
            frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.001" + " " + frmNewTransferKA.lblCurrency.text;
        } else {
            frmNewTransferKA.flxInvalidAmountField.text = frmNewTransferKA.lblCurrency.text + " 0.001" + " " + geti18Value("i18n.transfer.minAmount");
        }
    } else if (num == "2") {
        frmNewTransferKA.txtBox.placeholder = "0.00";
        if (kony.store.getItem("langPrefObj") !== "ar") {
            frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.01" + " " + frmNewTransferKA.lblCurrency.text;
        } else {
            frmNewTransferKA.flxInvalidAmountField.text = frmNewTransferKA.lblCurrency.text + " 0.01" + " " + geti18Value("i18n.transfer.minAmount");
        }
    } else {
        frmNewTransferKA.txtBox.placeholder = "0.000";
        kony.print("hiii9");
        if (kony.store.getItem("langPrefObj") !== "ar") {
            frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.001" + " " + frmNewTransferKA.lblCurrency.text;
        } else {
            frmNewTransferKA.flxInvalidAmountField.text = frmNewTransferKA.lblCurrency.text + " 0.001" + " " + geti18Value("i18n.transfer.minAmount");
        }
    }
}

function showLetterPicture(Account, val) {
    var str1 = Account;
    str1 = str1.split(" ");
    var s1 = str1[0].toUpperCase();
    var s2;
    if (!isEmpty(str1[1])) s2 = str1[1].toUpperCase();
    else s2 = str1[0][1];
    if (val === 1) {
        frmNewTransferKA.lblAccountName1.setVisibility(true);
        //     frmNewTransferKA.lblAccountNumber1.setVisibility(true);
        frmNewTransferKA.flxIcon1.setVisibility(true);
        frmNewTransferKA.lblIcon1.text = s1[0] + s2[0];
    } else {
        frmNewTransferKA.lblAccountName2.setVisibility(true);
        //     frmNewTransferKA.lblAccountNumber2.setVisibility(true);
        frmNewTransferKA.flxIcon2.setVisibility(true);
        frmNewTransferKA.lblIcon2.text = s1[0] + s2[0];
    }
}
/*
Author : Charan Kadari
Date : 12-02-18
Desc : On click of next from confirm transfer
*/
function gotoConfirmTransfer() {
    loadAllDatatoConfirmTransfersKA();
}

function getNumberfromcommma(num) {
    //alert(JSON.stringify(num));
    if (num === "" || num === null || num === undefined) return "";
    else {
        // alert("abcdef");
        var a = num;
        a = a.split(".");
        a[0] = a[0].replace(/\,/g, '');
        var val = a[0] + "." + a[1];
        val = parseFloat(val);
        return val;
    }
}
/*
Author : Charan Kadari/Vineeth Vishwanath
Date : 12-02-18
Desc : Loading data in confirm transfer
*/
function loadAllDatatoConfirmTransfersKA() {
    kony.print("hassan aaa " + frmNewTransferKA.lblFre.text);
    if (kony.boj.addBeneVar.selectedType === "BOJ" || gblTModule === "own" || gblTModule === "web") {
        //Do not show holiday text
        frmConfirmTransferKA.flxNote.setVisibility(false);
        frmConfirmTransferKA.flxExchangeCommTra.setVisibility(false); // hassan 16/05/2019
        frmConfirmTransferKA.flxOtherComm.setVisibility(false); //hasan 16/05/2019
    } else {
        //Show holiday text
        frmConfirmTransferKA.flxNote.setVisibility(true);
    }
    if (gblTModule != "web") {
        frmConfirmTransferKA.flxTransfers.setVisibility(true);
        frmConfirmTransferKA.flxWebCharge.setVisibility(false);
        var lblto, lblfrom, acc = "";
        lblto = frmNewTransferKA.lblAccountName1.text;
        if (frmNewTransferKA.lblAccountNumber1.isVisible) acc = frmNewTransferKA.lblAccountNumber1.text;
        ///acc = "****" + acc.substring(acc.length-4, acc.length);
        if (frmNewTransferKA.lblToAccCurr.text != frmNewTransferKA.lblFromAccCurr.text) {
            frmConfirmTransferKA.LblConvertedAmt.setVisibility(true);
        } else {
            frmConfirmTransferKA.LblConvertedAmt.setVisibility(false);
        }
        if (frmNewTransferKA.flxPurpose.isVisible) {
            frmConfirmTransferKA.CopynotesWrapper0fa62da5ca9ef4b.setVisibility(true); //PurposeOfTrans
            frmConfirmTransferKA.lblPurposeofTransfer.text = frmNewTransferKA.lstPurposeOfTransfer.selectedKeyValue[1];
        } else {
            frmConfirmTransferKA.CopynotesWrapper0fa62da5ca9ef4b.setVisibility(false);
        }
        frmConfirmTransferKA.transactionNotes.text = frmNewTransferKA.txtDesc.text;
        var fromAcc = kony.store.getItem("frmAccount");
        frmConfirmTransferKA.frmAccBranchCode.text = fromAcc.branchNumber;
        if (gblTModule == "own") {
            var toAcc = kony.store.getItem("toAccount");
            frmConfirmTransferKA.frmAccBranchCode.text = toAcc.branchNumber;
            frmConfirmTransferKA.toAccBranchCode.text = fromAcc.branchNumber;
            frmConfirmTransferKA.transactionFrom.text = lblto;
            frmConfirmTransferKA.transactionFromNumber.text = acc;
            frmConfirmTransferKA.fromAccountNumberKA.text = addZerosForBenificisry(toAcc.branchNumber) + frmNewTransferKA.lblAccountNumber1.text;
            frmConfirmTransferKA.toAccountNumberKA.text = addZerosForBenificisry(fromAcc.branchNumber) + frmNewTransferKA.lblAccountNumber2.text;
            frmConfirmTransferKA.fromAccountNumberKA.text = frmNewTransferKA.lblAccountNumber1.text;
            frmConfirmTransferKA.toAccountNumberKA.text = frmNewTransferKA.lblAccountNumber2.text;
        } else {
            frmConfirmTransferKA.toAccBranchCode.text = gblSelectedBene.benBranchNo;
            frmConfirmTransferKA.toAcc.text = lblto;
            frmConfirmTransferKA.lblAccountNumber.text = acc;
            frmConfirmTransferKA.fromAccountNumberKA.text = frmNewTransferKA.lblAccountNumber2.text;
            frmConfirmTransferKA.toAccountNumberKA.text = frmNewTransferKA.lblAccountNumber1.text;
        }
        if (frmNewTransferKA.lblTo.text == geti18Value("i18n.common.To")) {
            frmConfirmTransferKA.lblheading.text = geti18Value("i18n.transfers.Transfer") + " " + geti18Value("i18n.Transfers.WithinAcc");
        } else {
            frmConfirmTransferKA.lblheading.text = geti18Value("i18n.transfers.Transfer") + " " + frmNewTransferKA.Label0g6814cba9e7841.text;
        }
        lblfrom = frmNewTransferKA.lblAccountName2.text;
        acc = "";
        if (frmNewTransferKA.lblAccountNumber2.isVisible) acc = frmNewTransferKA.lblAccountNumber2.text;
        //acc = "****" + acc.substring(acc.length-4, acc.length);
        if (gblTModule == "own") {
            frmConfirmTransferKA.toAcc.text = lblfrom;
            frmConfirmTransferKA.lblAccountNumber.text = acc;
        } else {
            frmConfirmTransferKA.transactionFrom.text = lblfrom;
            frmConfirmTransferKA.transactionFromNumber.text = acc;
        }
        var value = "";
        if (kony.store.getItem("toAccount") !== null && kony.store.getItem("toAccount") !== undefined) {
            if (kony.store.getItem("toAccount").currencyCode !== frmNewTransferKA.lblCurrency.text && (gblTModule == "own")) {
                value = frmNewTransferKA.lblVal.text;
                frmConfirmTransferKA.transactionAmount.text = (value.substring(0, value.length - 4)).replace(/,/g, "");
            } else {
                frmConfirmTransferKA.transactionAmount.text = frmNewTransferKA.lblHiddenAmount.text.replace(/,/g, "");
            }
        } else {
            frmConfirmTransferKA.transactionAmount.text = frmNewTransferKA.lblHiddenAmount.text.replace(/,/g, "");
        }
        if (gblTModule !== "send") {
            if (kony.store.getItem("toAccount").currencyCode === frmNewTransferKA.lblCurrency.text) {
                frmConfirmTransferKA.amountTransaction.text = frmNewTransferKA.txtBox.text + " " + frmNewTransferKA.lblCurrency.text;
            } else {
                frmConfirmTransferKA.amountTransaction.text = frmNewTransferKA.lblVal.text;
            }
        } else {
            if (kony.store.getItem("frmAccount").currencyCode !== frmNewTransferKA.lblCurrency.text && gblSelectedBene.benType === "IFB") {
                value = frmNewTransferKA.lblVal.text;
                frmConfirmTransferKA.amountTransaction.text = value;
                frmConfirmTransferKA.transactionAmount.text = (value.substring(0, value.length - 4)).replace(/,/g, "");
            } else {
                frmConfirmTransferKA.amountTransaction.text = frmNewTransferKA.txtBox.text + " " + frmNewTransferKA.lblCurrency.text;
            }
        }
        frmConfirmTransferKA.lblReccuureceFreq.text = frmNewTransferKA.txtNumRecurrences.text;
        if (frmNewTransferKA.lblFre.text !== geti18Value("i18n.Transfers.Instant")) {
            //frmNewTransferKA.txtNumRecurrences.text = "";
            frmConfirmTransferKA.flxFrequency.setVisibility(true);
            frmConfirmTransferKA.flxDateDurtarion.setVisibility(true);
            //frmConfirmTransferKA.flxSchedule.setVisibility(true);
            frmConfirmTransferKA.flxFrequency.setVisibility(true);
            frmConfirmTransferKA.lblToDateKA.setVisibility(true);
            frmConfirmTransferKA.flxReccurrence.setVisibility(true);
            frmConfirmTransferKA.lblEndDateIndicator.setVisibility(true);
            frmConfirmTransferKA.lblReccurrenceValue.text = frmNewTransferKA.lblFre.text;
            frmConfirmTransferKA.lblfromDateKA.text = frmNewTransferKA.calFrom.formattedDate;
            frmConfirmTransferKA.lblToDateKA.text = frmNewTransferKA.calTo.formattedDate;
            frmConfirmTransferKA.lblScheduledDate.text = frmNewTransferKA.calFrom.formattedDate;
            if (frmNewTransferKA.lblFre.text === geti18Value("i18n.Transfers.OneTime")) {
                frmConfirmTransferKA.hiddenLblFromDate.text = getFormattedDate(frmNewTransferKA.calFrom);
                frmConfirmTransferKA.hiddenLblToDate.text = getFormattedDate(frmNewTransferKA.calFrom);
                frmConfirmTransferKA.flxFrequency.setVisibility(false);
                frmConfirmTransferKA.lblToDateKA.setVisibility(false);
                frmConfirmTransferKA.lblEndDateIndicator.setVisibility(false);
                frmConfirmTransferKA.lblStartDateIndicator.text = geti18Value("i18n.accounts.date");
                frmConfirmTransferKA.lblReccuureceFreq.text = "1";
            } else {
                frmConfirmTransferKA.lblStartDateIndicator.text = geti18Value("i18n.Transfer.StartDate");
                frmConfirmTransferKA.hiddenLblFromDate.text = getFormattedDate(frmNewTransferKA.calFrom);
                frmConfirmTransferKA.hiddenLblToDate.text = getFormattedDate(frmNewTransferKA.calTo);
            }
        } else {
            frmConfirmTransferKA.lblReccuureceFreq.text = "";
            frmConfirmTransferKA.hiddenLblFromDate.text = "";
            frmConfirmTransferKA.hiddenLblToDate.text = "";
            frmConfirmTransferKA.lblReccurrenceValue.text = frmNewTransferKA.lblFre.text;
            frmConfirmTransferKA.flxReccurrence.setVisibility(false);
            frmConfirmTransferKA.lblToDateKA.setVisibility(true);
            frmConfirmTransferKA.flxFrequency.setVisibility(false);
            frmConfirmTransferKA.flxFrequency.setVisibility(false);
            frmConfirmTransferKA.flxDateDurtarion.setVisibility(false);
            frmConfirmTransferKA.payMode.text = "0";
            //frmConfirmTransferKA.flxSchedule.setVisibility(false);
        }
        frmConfirmTransferKA.lblPurposeofTransfer.text = frmNewTransferKA.lstPurposeOfTransfer.selectedKeyValue[1];
        if (frmNewTransferKA.txtDesc.text === "") frmConfirmTransferKA.CopynotesWrapper03de47f39b8bc46.setVisibility(false);
        else {
            frmConfirmTransferKA.CopynotesWrapper03de47f39b8bc46.setVisibility(true);
            frmConfirmTransferKA.transactionNotes.text = frmNewTransferKA.txtDesc.text;
        }
        if (frmNewTransferKA.txtNumRecurrences.text === "") {
            frmConfirmTransferKA.flxFrequency.setVisibility(false);
            //frmConfirmTransferKA.flxReccurrence.height = "25%";
        } else {
            //frmConfirmTransferKA.flxReccurrence.height = "30%";
            frmConfirmTransferKA.flxFrequency.setVisibility(true);
        }
        frmConfirmTransferKA.flxCommisionRate.setVisibility(true);
        frmConfirmTransferKA.flxExchangeRate.setVisibility(true);
        frmConfirmTransferKA.flxExchangeComm.setVisibility(true);
        frmConfirmTransferKA.flxCreditAmount.setVisibility(true);
        frmConfirmTransferKA.flxLocalAmount.setVisibility(true);
        frmConfirmTransferKA.LblConvertedAmt.setVisibility(true);
        frmConfirmTransferKA.flxExchangeCommTra.setVisibility(false); // hassan 16/05/2019
        frmConfirmTransferKA.flxOtherComm.setVisibility(false); // hassan 16/05/2019
        if (gblTModule != "own") {
            //alert("gblSelectedBene :: "+JSON.stringify(gblSelectedBene));
            if (gblSelectedBene.benType == "ITB" || gblSelectedBene.benType == "DTB") {
                frmConfirmTransferKA.flxCommisionRate.setVisibility(false);
                frmConfirmTransferKA.flxExchangeRate.setVisibility(false);
                frmConfirmTransferKA.flxExchangeComm.setVisibility(false);
                frmConfirmTransferKA.flxCreditAmount.setVisibility(false);
                frmConfirmTransferKA.flxLocalAmount.setVisibility(false);
                frmConfirmTransferKA.LblConvertedAmt.setVisibility(false);
                frmConfirmTransferKA.flxExchangeCommTra.setVisibility(true); // hassan 16/05/2019
                frmConfirmTransferKA.flxOtherComm.setVisibility(true); //hasan 16/05/2019
                //alert("gblSelectedBene :: "+JSON.stringify(gblSelectedBene));
                frmConfirmTransferKA.lblPurposeofTransfer.text = frmNewTransferKA.lblPOT.text;
                kony.print("gblSelectedBene :: " + JSON.stringify(gblSelectedBene));
                frmConfirmTransferKA.lblBenName.text = gblSelectedBene.beneficiaryFullName;
                frmConfirmTransferKA.lblBenAdr1.text = gblSelectedBene.bene_address1;
                frmConfirmTransferKA.lblBenAdr2.text = "";
                frmConfirmTransferKA.lblBenAdr3.text = "";
                frmConfirmTransferKA.lblBenBankName.text = gblSelectedBene.bankName;
                frmConfirmTransferKA.lblBenBankAdr1.text = "";
                frmConfirmTransferKA.lblBenBankAdr2.text = "";
                frmConfirmTransferKA.lblBenBankCity.text = gblSelectedBene.bene_bank_city;
                frmConfirmTransferKA.lblTransCur.text = frmNewTransferKA.lblCurrency.text;
                frmConfirmTransferKA.lblPmt1.text = frmConfirmTransferKA.lblRemmit.text;
                frmConfirmTransferKA.lblNarr.text = "";
                frmConfirmTransferKA.lblImbAdr1.text = "";
                frmConfirmTransferKA.lblCorCharges.text = "";
                frmConfirmTransferKA.lblImbAdr2.text = "";
                frmConfirmTransferKA.lblImbCity.text = "";
                frmConfirmTransferKA.lblCustomerID.text = "";
                frmConfirmTransferKA.lblBenBranchCode.text = "";
                frmConfirmTransferKA.lblIBAN.text = "";
                frmConfirmTransferKA.lblRefID.text = "";
                frmConfirmTransferKA.lblPmtDetails3.text = "";
                frmConfirmTransferKA.lblPmtDetails4.text = "";
                frmConfirmTransferKA.lblToAccType.text = "";
                frmConfirmTransferKA.lblImbName.text = "";
                frmConfirmTransferKA.lblImbCoun.text = "";
                frmConfirmTransferKA.lblImbSC.text = "";
                frmConfirmTransferKA.lblDealRefNo.text = "";
                if (gblSelectedBene.benType == "DTB") {
                    frmConfirmTransferKA.lblCrearCode.text = "";
                    frmConfirmTransferKA.lblCrearType.text = "";
                    frmConfirmTransferKA.lblBenCountry.text = "";
                    frmConfirmTransferKA.lblBenCity.text = "";
                    frmConfirmTransferKA.lblSwift.text = "";
                    frmConfirmTransferKA.lblBenBankCountry.text = "";
                    frmConfirmTransferKA.lblPmt2.text = "";
                } else {
                    frmConfirmTransferKA.lblCrearCode.text = "";
                    frmConfirmTransferKA.lblCrearType.text = "";
                    frmConfirmTransferKA.lblBenCountry.text = gblSelectedBene.bene_address2;
                    frmConfirmTransferKA.lblBenBankCity.text = gblSelectedBene.bene_city;
                    frmConfirmTransferKA.lblBenCity.text = gblSelectedBene.bene_bank_city;
                    frmConfirmTransferKA.lblSwift.text = gblSelectedBene.swiftCode;
                    frmConfirmTransferKA.lblBenBankCountry.text = get_COUNTRYCode(gblSelectedBene.countryName);
                    frmConfirmTransferKA.lblPmt2.text = frmNewTransferKA.lblPOT.text;
                    frmConfirmTransferKA.lblCorCharges.text = "SHA";
                }
                //         serv_getExternalTraComm(); // hassan 16/05/2019//vadivel
            }
        }
        gblFeesFrom = "fromNext";
        if (kony.application.getCurrentForm().id === "frmCreditCardPayment") {
            setgblcurrencyfullCountryListt();
        }
        if (gblSelectedBene.benType == "ITB" || gblSelectedBene.benType == "DTB") {
            if (kony.store.getItem("frmAccount").currencyCode === frmNewTransferKA.lblCurrency.text) {
                frmConfirmTransferKA.show();
            } else getCommisionFee();
        } else getCommisionFee();
    } else { //webcharge topup
        var fromAcc = kony.store.getItem("frmAccount");
        if (frmWebCharge.lblAmount.text != null && frmWebCharge.lblAmount.text != "") frmConfirmTransferKA.lblCurrBal.text = frmWebCharge.lblAmount.text;
        else frmConfirmTransferKA.lblCurrBal.text = "" + " JOD";
        frmConfirmTransferKA.transactionAmount.text = frmWebCharge.lblHiddenAmount.text.replace(/,/g, "");
        frmConfirmTransferKA.transactionNotes.text = "";
        frmConfirmTransferKA.fromAccountNumberKA.text = frmWebCharge.lblhiddenFrmAcc.text;
        frmConfirmTransferKA.toAccountNumberKA.text = frmWebCharge.lblhiddenToAcc.text;
        var acc = frmWebCharge.lblhiddenToAcc.text;
        frmConfirmTransferKA.CopyLabel0cf60348636d54e.text = frmWebCharge.lblCardnum.text;
        //frmConfirmTransferKA.toMaskNum.text=   "****" + acc.substring(acc.length-4, acc.length);
        for (var i in kony.retailBanking.globalData.accountsDashboardData.accountsData) {
            if (kony.retailBanking.globalData.accountsDashboardData.accountsData[i].accountID === acc) {
                frmConfirmTransferKA.toAccBranchCode.text = kony.retailBanking.globalData.accountsDashboardData.accountsData[i].branchNumber;
                break;
            }
        }
        kony.print("account details ::" + JSON.stringify(kony.store.getItem("frmAccount")));
        frmConfirmTransferKA.frmAccBranchCode.text = kony.store.getItem("frmAccount").branchNumber;
        //frmConfirmTransferKA.toAccBranchCode.text="1";
        frmConfirmTransferKA.hiddenlblCommRate.text = "";
        frmConfirmTransferKA.hiddenlblExchComm.text = "";
        frmConfirmTransferKA.hiddenlblCrAmt.text = "";
        frmConfirmTransferKA.hiddenlblLclAmt.text = "";
        frmConfirmTransferKA.payMode.text = "0";
        frmConfirmTransferKA.transFlag.text = "T";
        frmConfirmTransferKA.lblFreType.text = "";
        frmConfirmTransferKA.hiddenLblFromDate.text = "";
        frmConfirmTransferKA.hiddenLblToDate.text = "";
        frmConfirmTransferKA.lblReccuureceFreq.text = "";
        frmConfirmTransferKA.frmAccount.text = frmWebCharge.lblAccountType.text;
        //     var amountentered = Number.parseFloat(frmWebCharge.lblHiddenAmount.text).toFixed(3);
        //     frmConfirmTransferKA.lblWebAmt.text=amountentered;
        //     var amount=parseFloat(frmWebCharge.lblAmount.text)+parseFloat(frmWebCharge.lblHiddenAmount.text);
        //     frmConfirmTransferKA.lblAfterBal.text=amount+" JOD";
        frmConfirmTransferKA.flxWebCharge.setVisibility(true);
        frmConfirmTransferKA.flxTransfers.setVisibility(false);
        //     frmConfirmTransferKA.toAccBranchCode.text = fromAcc.branchNumber;
        frmConfirmTransferKA.transactionFrom.text = frmWebCharge.lblAccountType.text;
        frmConfirmTransferKA.transactionFromNumber.text = frmWebCharge.lblAccountCode.text;
        gblTModule = "web";
        gblSelectedBene = "";
        setgblcurrencyfullCountryListt();
        getCommisionFee();
    }
}
/*
function getFormattedDate(dateString) {
    var monthNames = ["zero", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    dateString = dateString.split("-");
    var month_name = monthNames[parseInt(dateString[1])];
    return month_name + " " + parseInt(dateString[2]) + ", " + dateString[0] + " - " + arr[1].substring(0, 5);
}

*/
function getUpcommingDateString() {
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    mm = tomorrow.getMonth() + 1;
    dd = tomorrow.getDate();
    if (mm / 10 < 1) mm = "0" + mm;
    if (dd / 10 < 1) dd = "0" + dd;
    return dd + "/" + mm + "/" + tomorrow.getFullYear();
}
/*
Author : Charan Kadari
Date : 12-02-18
Desc : Clearinf the transfer screen on click of back
*/
function removeDatafrmTransfersform() {
    frmConfirmTransferKA.lblFreType.text = "";
    frmNewTransferKA.lblDecimal.text = "";
    frmNewTransferKA.lblCurrencyCode.text = "";
    gblDefCurr = "";
    frmNewTransferKA.lblHiddenAmount.text = "";
    frmNewTransferKA.lblAvailableLimit.text = "";
    frmNewTransferKA.lblAvailableLimit.setVisibility(false);
    frmNewTransferKA.flxRecurrence.setVisibility(false);
    frmNewTransferKA.lblInvalidFromAccount.setVisibility(false);
    frmNewTransferKA.lblInvalidToAccount.setVisibility(false);
    frmNewTransferKA.flxInvalidAmountField.setVisibility(false);
    frmNewTransferKA.InvalidDatefieldsTransfer.setVisibility(false);
    frmNewTransferKA.lblInvalidRecurrence.setVisibility(false);
    frmNewTransferKA.CopylblLine0iff8accf05a247.skin = "lblLine0h8b99d8a85f649";
    frmNewTransferKA.lblLine5.skin = "lblLine0h8b99d8a85f649";
    frmNewTransferKA.lblLine10.skin = "lblLine0h8b99d8a85f649";
    frmNewTransferKA.lblTransferCategoryLine.skin = "lblLine0h8b99d8a85f649";
    frmNewTransferKA.lblLine3.skin = "lblLine0h8b99d8a85f649";
    frmNewTransferKA.lblCurrency.text = "JOD";
    frmNewTransferKA.lblToAccCurr.text = "";
    frmNewTransferKA.lblFromAccCurr.text = "";
    frmNewTransferKA.lblAccountName1.text = "";
    frmConfirmTransferKA.lblPurposeofTransfer.text = "";
    frmNewTransferKA.lblAccountNumber1.text = "";
    frmNewTransferKA.flxIcon1.setVisibility(false);
    frmNewTransferKA.flxIcon2.setVisibility(false);
    frmNewTransferKA.lblAccountName2.text = "";
    frmNewTransferKA.lblAccountNumber2.text = "";
    frmNewTransferKA.txtNumRecurrences.text = "";
    frmNewTransferKA.txtBox.text = "";
    frmNewTransferKA.lblFre.text = geti18Value("i18n.Transfers.Instant");
    frmNewTransferKA.calFrom.clearData();
    frmNewTransferKA.calTo.clearData();
    frmNewTransferKA.calTo.setEnabled(true);
    frmNewTransferKA.txtDesc.text = "";
    gblSelectedBene = "";
    var d = new Date();
    frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false);
    gblFrequency = geti18Value("i18n.Transfers.Instant");
    frmNewTransferKA.calFrom.date = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
    frmNewTransferKA.calTo.date = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
    frmNewTransferKA.calFrom.validStartDate = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
    frmNewTransferKA.calTo.validStartDate = ([d.getDate(), d.getMonth() + 1, d.getFullYear()]);
    frmNewTransferKA.calTo.setEnabled(true);
    frmNewTransferKA.lblPOT.text = geti18Value("i18n.settings.select");
    frmNewTransferKA.lblTransferCategory.text = geti18Value("i18n.settings.select");
    kony.retailBanking.globalData.globals.popTransferFlag = null;
}
/*
Author : Charan Kadari
Date : 12-02-18
Desc : On click of next from transfer for validation
*/
function onClickNextTransfers() {
    if (gblTModule === "own") {
        //frmConfirmTransferKA.flxCommisionRate.setVisibility(false);
        //frmConfirmTransferKA.flxExchangeRate.setVisibility(false);
    } else {
        frmConfirmTransferKA.flxCommisionRate.setVisibility(true);
        frmConfirmTransferKA.flxExchangeRate.setVisibility(true);
    }
    var x = validateFromAndToAccounts();
    var y = validateAmount();
    var z = null;
    if (x && y) {
        z = validate_SufficientBalance();
        if (!z) {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        }
    }
    //var z = validateFrequency();
    //var a = validatePoT();
    return x & y & z; //&z;//&a;
}
/*
Author : Charan Kadari
Date : 12-02-18
Desc : Validating the tranfer page
*/
function validateFromAndToAccounts() {
    var flag = 1;
    if (frmNewTransferKA.lblAccountName1.text === "" && frmNewTransferKA.lblAccountNumber1.text === "") {
        if (gblTModule === "own") {
            frmNewTransferKA.lblInvalidToAccount.text = geti18Value("i18.Transfer.enterFromAcc");
            frmNewTransferKA.CopylblLine0i25ba22a14b94a.skin = "sknFlxOrangeLine";
            frmNewTransferKA.CopylblLine0a56eccb1a64345.skin = "sknFlxOrangeLine";
            frmNewTransferKA.lblInvalidFromAccount.setVisibility(true);
        } else {
            frmNewTransferKA.lblInvalidToAccount.text = geti18Value("i18.Transfer.enterToAcc");
            frmNewTransferKA.CopylblLine0i25ba22a14b94a.skin = "sknFlxOrangeLine";
            frmNewTransferKA.CopylblLine0a56eccb1a64345.skin = "sknFlxOrangeLine";
            frmNewTransferKA.lblInvalidToAccount.setVisibility(true);
        }
        flag = 0;
    } else {
        if (gblTModule === "own") {
            frmNewTransferKA.lblInvalidFromAccount.setVisibility(false);
            frmNewTransferKA.CopylblLine0i25ba22a14b94a.skin = "lblsknToandFromAccLine";
            frmNewTransferKA.CopylblLine0a56eccb1a64345.skin = "lblsknToandFromAccLine";
        } else {
            frmNewTransferKA.CopylblLine0i25ba22a14b94a.skin = "lblsknToandFromAccLine";
            frmNewTransferKA.CopylblLine0a56eccb1a64345.skin = "lblsknToandFromAccLine";
            frmNewTransferKA.lblInvalidToAccount.setVisibility(false);
        }
    }
    if (frmNewTransferKA.lblAccountName2.text === "" && frmNewTransferKA.lblAccountNumber2.text === "") {
        if (gblTModule === "own") {
            frmNewTransferKA.CopylblLine0hd66bb44c34643.skin = "sknFlxOrangeLine";
            frmNewTransferKA.CopylblLine0b03cf015d52c4a.skin = "sknFlxOrangeLine";
            frmNewTransferKA.lblInvalidToAccount.text = geti18Value("i18.Transfer.enterToAcc");
            frmNewTransferKA.lblInvalidToAccount.setVisibility(true);
        } else {
            frmNewTransferKA.CopylblLine0hd66bb44c34643.skin = "sknFlxOrangeLine";
            frmNewTransferKA.CopylblLine0b03cf015d52c4a.skin = "sknFlxOrangeLine";
            frmNewTransferKA.lblInvalidToAccount.text = geti18Value("i18.Transfer.enterFromAcc");
            frmNewTransferKA.lblInvalidToAccount.setVisibility(true);
        }
        flag = 0;
    } else {
        frmNewTransferKA.CopylblLine0hd66bb44c34643.skin = "lblsknToandFromAccLine";
        frmNewTransferKA.CopylblLine0b03cf015d52c4a.skin = "lblsknToandFromAccLine";
        frmNewTransferKA.lblInvalidToAccount.setVisibility(false);
    }
    return flag;
}

function validateAmount() {
    var amount = frmNewTransferKA.txtBox.text;
    var currency = frmNewTransferKA.lblCurrency.text;
    amount = parseFloat(amount);
    if (amount === null || amount === "" || amount === "." || ((Number(amount) < 0.001) && currency === "JOD") || ((Number(amount) < 0.01) && currency !== "JOD") || amount == 0 || amount == "NaN") {
        frmNewTransferKA.flxInvalidAmountField.setVisibility(true);
        frmNewTransferKA.lblLine3.skin = "sknFlxOrangeLine";
        return 0;
    }
    frmNewTransferKA.flxInvalidAmountField.setVisibility(false);
    frmNewTransferKA.lblLine3.skin = "lblLine0h8b99d8a85f649";
    return 1;
}

function validate_SufficientBalance() {
    try {
        var bal = null,
            amount = null;
        if (gblTModule == "own") {
            bal = parseFloat(kony.store.getItem("toAccount").availableBalance);
            if (kony.store.getItem("toAccount").currencyCode === frmNewTransferKA.lblCurrency.text) amount = frmNewTransferKA.txtBox.text;
            else amount = frmNewTransferKA.lblVal.text.substring(0, frmNewTransferKA.lblVal.text.length - 4);
        } else if (gblTModule == "send") {
            bal = parseFloat(kony.store.getItem("frmAccount").availableBalance);
            if (kony.store.getItem("frmAccount").currencyCode === frmNewTransferKA.lblCurrency.text) amount = frmNewTransferKA.txtBox.text;
            else return true;
        } else if (gblTModule === "CLIQ") {
            bal = parseFloat(kony.store.getItem("frmAccount").availableBalance);
            if (frmEPS.btnAlias.skin === "sknOrangeBGRNDBOJ") {
                amount = frmEPS.txtAmountAlias.text;
            } else if (frmEPS.btnMob.skin === "sknOrangeBGRNDBOJ") {
                amount = frmEPS.txtAmountMob.text;
            } else if (frmEPS.btnIBAN.skin === "sknOrangeBGRNDBOJ") {
                amount = frmEPS.txtAmountIBAN.text;
            }
        } else {
            if (kony.application.getCurrentForm().btnBillsPayCards.text === "t") bal = parseFloat(kony.store.getItem("BillPayfromAcc").spending_limit.replace(/,/g, ""));
            else bal = parseFloat(kony.store.getItem("BillPayfromAcc").availableBalance);
            amount = kony.application.getCurrentForm().tbxAmount.text;
        }
        amount = amount.toString().replace(/,/g, "");
        kony.print("amount ::" + amount);
        if (parseFloat(amount) <= bal) return true;
        else return false;
    } catch (e) {
        kony.print("Exception_validate_SufficientBalance ::" + e);
    }
}

function validateFrequency() {
    var flag = 1;
    onTextChangeRecurrence();
    if (frmNewTransferKA.lblFre.text === geti18Value("i18n.Transfers.Instant")) {
        frmNewTransferKA.InvalidDatefieldsTransfer.setVisibility(false);
        frmNewTransferKA.lblLine4.skin = "lblLine0h8b99d8a85f649";
    } else {
        if (frmNewTransferKA.lblFre.text === geti18Value("i18n.Transfers.OneTime")) {
            if (frmNewTransferKA.calFrom.formattedDate === "") {
                frmNewTransferKA.InvalidDatefieldsTransfer.setVisibility(true);
                frmNewTransferKA.lblLine4.skin = "sknFlxOrangeLine";
            } else {
                if (frmNewTransferKA.calFrom.formattedDate === 0) {
                    frmNewTransferKA.InvalidDatefieldsTransfer.setVisibility(true);
                    frmNewTransferKA.lblLine4.skin = "sknFlxOrangeLine";
                    flag = 0;
                }
                frmNewTransferKA.InvalidDatefieldsTransfer.setVisibility(false);
                frmNewTransferKA.lblLine4.skin = "lblLine0h8b99d8a85f649";
            }
        } else {
            if (frmNewTransferKA.txtNumRecurrences.text !== "" && frmNewTransferKA.txtNumRecurrences.text == 0) {
                frmNewTransferKA.lblInvalidRecurrence.setVisibility(true);
                frmNewTransferKA.CopylblLine0iff8accf05a247.skin = "sknFlxOrangeLine";
                flag = 0;
            } else {
                frmNewTransferKA.lblInvalidRecurrence.setVisibility(false);
                frmNewTransferKA.CopylblLine0iff8accf05a247.skin = "lblLine0h8b99d8a85f649";
            }
            if (frmNewTransferKA.calFrom.formattedDate === "" && frmNewTransferKA.calTo.formattedDate === "") {
                frmNewTransferKA.InvalidDatefieldsTransfer.setVisibility(true);
                frmNewTransferKA.lblLine4.skin = "sknFlxOrangeLine";
                flag = 0;
            } else {
                frmNewTransferKA.InvalidDatefieldsTransfer.setVisibility(false);
                frmNewTransferKA.lblLine4.skin = "lblLine0h8b99d8a85f649";
            }
        }
    }
    return flag;
}

function addZeroes(num) {
    try {
        kony.print("Received amiount --> " + num);
        var value = Number(num);
        var res = num.split(".");
        if (num.indexOf('.') === -1) {
            value = value.toFixed(3);
            num = value.toString();
        } else if (res[1].length < 3) {
            value = value.toFixed(3);
            num = value.toString();
        } else {
            num = value.toFixed(3);
        }
        kony.print("sent amiount --> " + num);
        return num;
    } catch (e) {
        kony.print("sent excepion amiount --> " + num);
        return parseFloat(num);
    }
}

function makeInlineErrorsTransfersInvisible() {
    frmNewTransferKA.flxInvalidAmountField.setVisibility(false);
    frmNewTransferKA.lblLine3.skin = "lblLine0h8b99d8a85f649";
    frmNewTransferKA.InvalidDatefieldsTransfer.setVisibility(false);
    frmNewTransferKA.lblInvalidToAccount.setVisibility(false);
    frmNewTransferKA.lblInvalidFromAccount.setVisibility(false);
    frmNewTransferKA.flxInvalidAmountField.setVisibility(false);
    frmNewTransferKA.lblInvalidRecurrence.setVisibility(false);
    frmNewTransferKA.CopylblLine0iff8accf05a247.skin = "lblLine0h8b99d8a85f649";
    frmNewTransferKA.CopylblLine0hd66bb44c34643.skin = "lblsknToandFromAccLine";
    frmNewTransferKA.CopylblLine0b03cf015d52c4a.skin = "lblsknToandFromAccLine";
    frmNewTransferKA.CopylblLine0i25ba22a14b94a.skin = "lblsknToandFromAccLine";
    frmNewTransferKA.CopylblLine0a56eccb1a64345.skin = "lblsknToandFromAccLine";
    frmNewTransferKA.lblLine3.skin = "lblLine0h8b99d8a85f649";
    frmNewTransferKA.lblLine4.skin = "lblLine0h8b99d8a85f649";
}

function ownAccountPreshow() {
    frmNewTransferKA.lblSelectanAccount1.setVisibility(true);
    frmNewTransferKA.lblSelectanAccount2.setVisibility(true);
    frmNewTransferKA.flxRecurrence.setVisibility(false);
    frmNewTransferKA.flxPurpose.setVisibility(false);
    frmNewTransferKA.Label0g6814cba9e7841.text = geti18Value("i18n.Transfers.WithinAcc");
}

function getPropAmt(num) {
    return num.split(",").join("");
}
/*
Author : Vineeth Vishwanath
Date : 20-02-18
Desc : Getting the commision
*/
/* hassan 16/05/2019 */
function serv_getExternalTraComm() {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
        "access": "online",
        "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Transactions", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Transactions");
    var d = new Date();
    kony.print("fromAccountNumber ::" + frmNewTransferKA.lblAccountNumber2.text);
    kony.print("BenCity ::" + gblSelectedBene.bene_city);
    kony.print("BenCountry ::" + gblSelectedBene.countryName);
    kony.print("amount ::" + frmNewTransferKA.txtBox.text);
    kony.print("p_trans_amt_ccy ::" + frmNewTransferKA.lblCurrency.text);
    kony.print("transactionDate ::" + ([d.getDate(), d.getMonth() + 1, d.getFullYear()]));
    kony.print("frmAccBranchCode ::" + kony.store.getItem("frmAccount").branchNumber);
    dataObject.addField("fromAccountNumber", frmNewTransferKA.lblAccountNumber2.text);
    dataObject.addField("SourceBranchCode", kony.store.getItem("frmAccount").branchNumber);
    dataObject.addField("BenCity", gblSelectedBene.bene_city);
    dataObject.addField("BenCountry", gblSelectedBene.countryName);
    dataObject.addField("amount", frmNewTransferKA.lblHiddenAmount.text.replace(/,/g, ""));
    dataObject.addField("p_trans_amt_ccy", frmNewTransferKA.lblCurrency.text);
    dataObject.addField("p_pay_mode", 0);
    dataObject.addField("p_user_id", "BOJMOB");
    dataObject.addField("p_channel", "MOBILE");
    var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
    };
    kony.print("service input paramerters ::" + JSON.stringify(dataObject));
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("getExternalComm", serviceOptions, function(res) {
            kony.print("Success ::" + JSON.stringify(res));
            getExternalTraComm(res);
        }, function(err) {
            kony.print("failed ::" + JSON.stringify(err));
            /*if(operationName === "InternetMailorder"){
                  		customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.ecommerce.failed"), popupCommonAlertDimiss, "");
                      }else{*/
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
            //kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"),geti18nkey("i18n.cards.gotoCards"),"Cards","","");
            //}
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        });
    }
}

function getExternalTraComm(res) {
    try {
        if (res.ErrorCode === "" && res.swiftComm !== undefined && res.exchComm !== undefined && res.othComm !== undefined && res.bankComm !== undefined) {
            var totalComm = parseFloat(res.swiftComm) + parseFloat(res.othComm) + parseFloat(res.bankComm);
            if (frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.isVisible === false) frmConfirmTransferKA.lblOtherComm.text = formatamountwithCurrency(totalComm, frmNewTransferKA.lblFromAccCurr.text) + " " + frmNewTransferKA.lblFromAccCurr.text;
            else frmConfirmTransferKA.lblOtherComm.text = formatamountwithCurrency(totalComm, frmNewTransferKA.lblToAccCurr.text) + " " + frmNewTransferKA.lblToAccCurr.text;
            //             frmConfirmTransferKA.lblOtherComm.text = frmConfirmTransferKA.lblOtherComm.text + " " + frmNewTransferKA.lblFromAccCurr.text;
            frmConfirmTransferKA.lblExchangeCommTra.text = formatamountwithCurrency(res.exchComm, CURRENCY) + " " + CURRENCY; //frmNewTransferKA.lblFromAccCurr.text;
            gotoConfirmTransfer();
        } else {
            var Message = "";
            if (res.ErrorCode !== "00000" || response.ErrorCode === "0" || response.ErrorCode === 0) {
                Message = getErrorMessage(res.ErrorCode);
                customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
            }
        }
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_call_serv_getjomoppayregdata ::" + e);
    }
}
/* hassan 16/05/2019 */
function getCommisionFee() {
    kony.print("getcomfee gblTModule ::" + gblTModule);
    if (gblTModule == "web") {
        if (kony.sdk.isNetworkAvailable()) {
            if (frmWebCharge.lblHiddenAmount.text != 0 && frmWebCharge.lblHiddenAmount.text != ".") {
                kony.print("Comm From web charge module");
                var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                var options = {
                    "access": "online",
                    "objectName": "RBObjects"
                };
                var headers = {};
                var serviceName = "RBObjects";
                var modelObj = INSTANCE.getModel("Transactions", serviceName, options);
                var dataObject = new kony.sdk.dto.DataObject("Transactions");
                var billerDetails = kony.store.getItem("frmAccount");
                var currencyCode = "";
                if (billerDetails !== null && billerDetails !== "") {
                    for (var i in gblCurrList) {
                        if (gblCurrList[i].CURR_ISO === billerDetails.currencyCode) {
                            currencyCode = gblCurrList[i].CURR_CODE;
                        }
                    }
                }
                kony.print("frmWebCharge.lblhiddenFrmAcc.text " + frmWebCharge.lblhiddenFrmAcc.text);
                kony.print("frmWebCharge.lblhiddenToAcc.text " + frmWebCharge.lblhiddenToAcc.text);
                kony.print("frmConfirmTransferKA.lblWebAmt.text " + frmConfirmTransferKA.lblWebAmt.text);
                dataObject.addField("p_trnsf_cur", currencyCode || "1");
                dataObject.addField("accountNumber", frmWebCharge.lblhiddenFrmAcc.text);
                dataObject.addField("ExternalAccountNumber", "");
                dataObject.addField("amount", frmWebCharge.txtFieldAmount.text.replace(/,/g, ""));
                dataObject.addField("TransferFlag", "T");
                dataObject.addField("p_billId", "");
                dataObject.addField("p_billNo", "");
                kony.print("dataobject - commission :: " + JSON.stringify(dataObject));
                var serviceOptions = {
                    "dataObject": dataObject,
                    "headers": headers
                };
                kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
                modelObj.customVerb("getFeeData", serviceOptions, getFeesSuccess, getFeesFailure);
                return;
            }
        } else { //Network not available
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            alert("" + kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"));
            return;
        }
        return;
    }
    if (gblTModule != "own" && gblTModule != "web") {
        if (gblSelectedBene.benType == "ITB" || gblSelectedBene.benType == "DTB") {
            if (kony.store.getItem("frmAccount").currencyCode === frmNewTransferKA.lblCurrency.text) {
                frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false);
                return;
            } else {
                frmNewTransferKA.lblToAccCurr.text = kony.store.getItem("frmAccount").currencyCode;
            }
        }
        /*if(gblFeesFrom !== undefined){
            if(!isEmpty(gblFeesFrom) && gblFeesFrom == "fromNext"){
              frmConfirmTransferKA.show();
              return;
            }
            else
              return;
          }
          else
            return;
        }*/
    }
    if (frmNewTransferKA.txtBox.text != 0 && frmNewTransferKA.txtBox.text !== "NaN" && frmNewTransferKA.txtBox.text != "." && frmNewTransferKA.lblToAccCurr.text !== null && frmNewTransferKA.lblFromAccCurr.text !== null && frmNewTransferKA.lblToAccCurr.text !== "" && frmNewTransferKA.lblFromAccCurr.text !== "") { //Checking if accounts are selected
        //if(frmNewTransferKA.lblToAccCurr.text != frmNewTransferKA.lblFromAccCurr.text){//Checking if the source and dest currency are equal
        if (frmNewTransferKA.lblHiddenAmount.text !== null && frmNewTransferKA.lblHiddenAmount.text !== "") { //Caaling the service only when there an amount entered
            if (frmNewTransferKA.lblToAccCurr.text != frmNewTransferKA.lblFromAccCurr.text) {
                frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(true); //currency conversion true if they are not equal
                frmConfirmTransferKA.LblConvertedAmt.setVisibility(true);
            } else {
                frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false);
                frmConfirmTransferKA.LblConvertedAmt.setVisibility(false);
            }
            if (kony.sdk.isNetworkAvailable()) {
                var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                var options = {
                    "access": "online",
                    "objectName": "RBObjects"
                };
                var headers = {};
                var serviceName = "RBObjects";
                var modelObj = INSTANCE.getModel("Transactions", serviceName, options);
                var dataObject = new kony.sdk.dto.DataObject("Transactions");
                var currencyCode = "";
                for (var i in gblCurrList) {
                    if (frmNewTransferKA.lblCurrency.text === gblCurrList[i].CURR_ISO) {
                        currencyCode = gblCurrList[i].CURR_CODE;
                    }
                }
                if (gblTModule == "own") {
                    kony.print("Comm From Own acc module");
                    dataObject.addField("p_trnsf_cur", currencyCode);
                    dataObject.addField("accountNumber", frmNewTransferKA.lblAccountNumber1.text);
                    dataObject.addField("ExternalAccountNumber", frmNewTransferKA.lblAccountNumber2.text);
                    frmNewTransferKA.lblHiddenAmount.text = getPropAmt(frmNewTransferKA.lblHiddenAmount.text);
                    dataObject.addField("amount", frmNewTransferKA.lblHiddenAmount.text);
                } else {
                    kony.print("Comm From send module");
                    var toAccCCode = "";
                    if (gblSelectedBene.benType == "ITB" || gblSelectedBene.benType == "DTB") {
                        toAccCCode = getCurrencyCode_FROM_CURRENCY_ISO(frmNewTransferKA.lblCurrency.text);
                        if (isEmpty(toAccCCode)) return;
                        toAccCCode = (toAccCCode.length == 1) ? append_VALUE("00" + toAccCCode, "0", 13) : (toAccCCode.length == 2) ? append_VALUE("0" + toAccCCode, "0", 13) : append_VALUE(toAccCCode, "0", 13);
                    } else {
                        toAccCCode = frmNewTransferKA.lblAccountNumber1.text;
                    }
                    dataObject.addField("p_trnsf_cur", currencyCode);
                    dataObject.addField("accountNumber", frmNewTransferKA.lblAccountNumber2.text);
                    dataObject.addField("ExternalAccountNumber", toAccCCode); //frmNewTransferKA.lblAccountNumber1.text);
                    frmNewTransferKA.lblHiddenAmount.text = getPropAmt(frmNewTransferKA.lblHiddenAmount.text);
                    dataObject.addField("amount", frmNewTransferKA.lblHiddenAmount.text.replace(/,/g, ""));
                }
                dataObject.addField("TransferFlag", "T");
                dataObject.addField("p_billId", "");
                dataObject.addField("p_billNo", "");
                kony.print("dataobject - commission :: " + JSON.stringify(dataObject));
                var serviceOptions = {
                    "dataObject": dataObject,
                    "headers": headers
                };
                kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
                if (dataObject.amount !== "." && dataObject.amount !== "NaN") modelObj.customVerb("getFeeData", serviceOptions, getFeesSuccess, getFeesFailure);
            } else { //Network not available
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"));
            }
        } else { //if amount not yet entered
            frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false);
        }
        //}
        // else{//if both are same currencies
        //  frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false); 
        // }
    } else { //Accounts not selected
        frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false);
    }

    function getFeesSuccess(res) {
        kony.application.dismissLoadingScreen();
        kony.print("Result Commission :: " + JSON.stringify(res));
        if (!isEmpty(res)) {
            if (!isEmpty(res.errmsg)) {
                if (res.errmsg.length > 0) {
                    var errorMessage = geti18Value("i18n.common.somethingwentwrong");
                    customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
                    if (gblTModule != "web") frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false);
                    return;
                }
            }
            if (isEmpty(res.ref_code)) {
                var errorMessage = geti18Value("i18n.common.somethingwentwrong");
                customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
                if (gblTModule != "web") frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false);
                return;
            }
            if (gblTModule != "web") {
                frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(true);
                var exRate = res.transferResponse[0].cross_rate.substring(0, 6);
                kony.print("exRate ::" + exRate);
                frmNewTransferKA.lblVal.text = formatAmountwithcomma(res.transferResponse[0].cr_amt, getDecimalNumForCurr(frmNewTransferKA.lblToAccCurr.text)) + " " + frmNewTransferKA.lblToAccCurr.text;
                if (gblTModule === "own") {
                    frmNewTransferKA.lblFromCurr.text = "1 " + frmNewTransferKA.lblFromAccCurr.text;
                    if (kony.store.getItem("toAccount").currencyCode !== frmNewTransferKA.lblCurrency.text) {
                        exRate = (parseFloat(res.transferResponse[0].cr_amt) / parseFloat(frmNewTransferKA.txtBox.text.replace(/,/g, ""))).toFixed(4);
                    }
                    if (kony.store.getItem("toAccount").currencyCode !== kony.store.getItem("frmAccount").currencyCode) {
                        frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(true); //hassan 28/09/2020
                        //               alert("same");
                    } else frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false); //hassan 28/09/2020
                    //               alert("diffs");
                    frmNewTransferKA.lblToCurr.text = exRate + " " + frmNewTransferKA.lblToAccCurr.text;
                } else if (gblTModule === "send") {
                    frmNewTransferKA.lblFromCurr.text = "1 " + frmNewTransferKA.lblFromAccCurr.text;
                    if (kony.store.getItem("frmAccount").currencyCode !== frmNewTransferKA.lblCurrency.text) {
                        exRate = (parseFloat(res.transferResponse[0].cr_amt) / parseFloat(frmNewTransferKA.txtBox.text.replace(/,/g, ""))).toFixed(4);
                    }
                    frmNewTransferKA.lblToCurr.text = exRate + " " + frmNewTransferKA.lblToAccCurr.text;
                }
                if (res.transferResponse[0].comm_chrg !== "" && res.transferResponse[0].comm_chrg !== null) {
                    frmConfirmTransferKA.lblCommRate.text = formatAmountwithcomma(res.transferResponse[0].comm_chrg, getDecimalNumForCurr(frmNewTransferKA.lblFromAccCurr.text)); //+" "+frmNewTransferKA.lblFromAccCurr.text; //tentative
                }
                if (res.transferResponse[0].comm_chrg === null || res.transferResponse[0].comm_chrg === "") {
                    frmConfirmTransferKA.lblCommRate.text = "0 "; //+frmNewTransferKA.lblFromAccCurr.text; //tentative
                }
                var creditCurrency = "";
                if (kony.store.getItem("toAccount") !== null) creditCurrency = kony.store.getItem("toAccount").currencyCode;
                frmConfirmTransferKA.lblExchRate.text = formatAmountwithcomma(exRate, 4); //+" "+frmNewTransferKA.lblToAccCurr.text;
                frmConfirmTransferKA.lblExchComm.text = formatAmountwithcomma(res.transferResponse[0].exch_comm_chrg, getDecimalNumForCurr(CURRENCY)) + " " + CURRENCY; //tentative
                frmConfirmTransferKA.lblLclAmt.text = formatAmountwithcomma(res.transferResponse[0].fees, getDecimalNumForCurr(CURRENCY)) + " " + CURRENCY; //frmNewTransferKA.lblFromAccCurr.text;//Local Amount
                if (gblTModule !== "send") {
                    if (kony.store.getItem("toAccount").currencyCode === frmNewTransferKA.lblCurrency.text) {
                        frmConfirmTransferKA.LblConvertedAmt.text = formatAmountwithcomma(res.transferResponse[0].cr_amt, getDecimalNumForCurr(frmNewTransferKA.lblToAccCurr.text)) + " " + frmNewTransferKA.lblToAccCurr.text;
                        frmConfirmTransferKA.lblCrAmt.text = formatAmountwithcomma(res.transferResponse[0].cr_amt, getDecimalNumForCurr(frmNewTransferKA.lblToAccCurr.text)) + " " + frmNewTransferKA.lblToAccCurr.text; //Credit amount
                    } else {
                        frmConfirmTransferKA.LblConvertedAmt.text = formatAmountwithcomma(frmNewTransferKA.txtBox.text, getDecimalNumForCurr(frmNewTransferKA.lblCurrency.text)) + " " + frmNewTransferKA.lblCurrency.text;
                        frmConfirmTransferKA.lblCrAmt.text = formatAmountwithcomma(frmNewTransferKA.txtBox.text, getDecimalNumForCurr(frmNewTransferKA.lblCurrency.text)) + " " + frmNewTransferKA.lblCurrency.text; //Credit amount
                    }
                } else {
                    if (kony.store.getItem("frmAccount").currencyCode !== frmNewTransferKA.lblCurrency.text && gblSelectedBene.benType === "IFB") {
                        frmConfirmTransferKA.LblConvertedAmt.text = formatAmountwithcomma(frmNewTransferKA.txtBox.text, getDecimalNumForCurr(frmNewTransferKA.lblCurrency.text)) + " " + frmNewTransferKA.lblCurrency.text;
                        frmConfirmTransferKA.lblCrAmt.text = formatAmountwithcomma(frmNewTransferKA.txtBox.text, getDecimalNumForCurr(frmNewTransferKA.lblCurrency.text)) + " " + frmNewTransferKA.lblCurrency.text; //Credit amount
                    } else {
                        frmConfirmTransferKA.LblConvertedAmt.text = formatAmountwithcomma(res.transferResponse[0].cr_amt, getDecimalNumForCurr(frmNewTransferKA.lblToAccCurr.text)) + " " + frmNewTransferKA.lblToAccCurr.text;
                        frmConfirmTransferKA.lblCrAmt.text = formatAmountwithcomma(res.transferResponse[0].cr_amt, getDecimalNumForCurr(frmNewTransferKA.lblToAccCurr.text)) + " " + frmNewTransferKA.lblToAccCurr.text; //Credit amount
                    }
                }
                frmConfirmTransferKA.hiddenlblCommRate.text = res.transferResponse[0].comm_chrg;
                frmConfirmTransferKA.hiddenlblExchComm.text = res.transferResponse[0].exch_comm_chrg;
                if (creditCurrency !== frmNewTransferKA.lblCurrency.text && gblTModule === "own") frmConfirmTransferKA.hiddenlblCrAmt.text = frmNewTransferKA.txtBox.text;
                else {
                    if ((frmNewTransferKA.lblCurrency.text !== kony.store.getItem("frmAccount").currencyCode) && gblSelectedBene.benType === "IFB") {
                        frmConfirmTransferKA.amountTransaction.text = formatAmountwithcomma(res.transferResponse[0].cr_amt, getDecimalNumForCurr(frmNewTransferKA.lblToAccCurr.text)) + " " + frmNewTransferKA.lblToAccCurr.text;
                        frmConfirmTransferKA.transactionAmount.text = parseFloat(res.transferResponse[0].cr_amt).toFixed(3);
                        frmConfirmTransferKA.hiddenlblLclAmt.text = res.transferResponse[0].cr_amt;
                        frmConfirmTransferKA.hiddenlblCrAmt.text = res.transferResponse[0].fees;
                    } else {
                        frmConfirmTransferKA.hiddenlblLclAmt.text = res.transferResponse[0].fees;
                        frmConfirmTransferKA.hiddenlblCrAmt.text = res.transferResponse[0].cr_amt;
                    }
                }
                frmConfirmTransferKA.lblTransCur.text = frmNewTransferKA.lblCurrency.text;
                kony.application.dismissLoadingScreen();
                kony.print("Successfully recieceved the fees ::" + JSON.stringify(res));
                if (gblFeesFrom !== undefined) {
                    if (!isEmpty(gblFeesFrom) && gblFeesFrom == "fromNext") {
                        gblFeesFrom = "";
                        frmConfirmTransferKA.show();
                    } else frmNewTransferKA.forceLayout();
                } else frmNewTransferKA.forceLayout();
            } else {
                kony.print("Successfully recieceved the fees2 ::" + JSON.stringify(res));
                //         if(parseFloat(res.transferResponse[0].comm_chrg) > 0)
                //            frmConfirmTransferKA.lblWebFees.text= setDecimal(parseFloat(res.transferResponse[0].comm_chrg||0),3)+" JOD";
                //         else
                frmConfirmTransferKA.lblWebFees.text = setDecimal(3, 3) + " JOD"; //geti18Value("i18n.cards.webchargeTopUpFeeHint");
                frmConfirmTransferKA.lblWebAmt.text = setDecimal(parseFloat(res.transferResponse[0].lc_amt), 3) + " JOD";
                frmConfirmTransferKA.lblAfterBal.text = setDecimal((parseFloat((frmConfirmTransferKA.lblCurrBal.text.replace(" ", "").replace("JOD", "").replace(",", "")) || 0) + parseFloat(res.transferResponse[0].lc_amt)), 3) + " JOD";
                //frmConfirmTransferKA.lblAfterBal.text = setDecimal((parseFloat(res.transferResponse[0].comm_chrg||0)+parseFloat(res.transferResponse[0].lc_amt)),3)+" JOD";
                frmConfirmTransferKA.hiddenlblCommRate.text = res.transferResponse[0].comm_chrg;
                frmConfirmTransferKA.hiddenlblExchComm.text = res.transferResponse[0].exch_comm_chrg;
                frmConfirmTransferKA.hiddenlblCrAmt.text = res.transferResponse[0].cr_amt;
                frmConfirmTransferKA.hiddenlblLclAmt.text = res.transferResponse[0].fees;
                frmConfirmTransferKA.show();
            }
        } else {
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
            kony.print("Empty result - commission");
            kony.application.dismissLoadingScreen();
        }
    }

    function getFeesFailure(err) {
        kony.application.dismissLoadingScreen();
        if (err === undefined || err === null || err === "") {
            kony.print("getfees err found undefined");
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
        } else {
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
        }
        //alert("Not Success recieceved the fees ::" + JSON.stringify(err));
    }
}
/*
Author : Vineeth Vishwanath
Date : 20-02-18
Desc : Checking if the user has enough accounts i.e more than one account to check if he can go to own transfers page
*/
function hasEnoughAccounts() {
    var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
    var toAccounts = kony.retailBanking.globalData.accountsDashboardData.toAccounts;
    //   alert("To :: " + JSON.stringify(toAccounts));
    //   alert("fromAccounts :: "+JSON.stringify(fromAccounts));
    if (isEmpty(toAccounts) || isEmpty(fromAccounts)) {
        return false;
    } else if (toAccounts.length == 1 && fromAccounts.length == 1) {
        if (toAccounts[0].accountID == fromAccounts[0].accountID) {
            return false;
        } else {
            return true;
        }
    } else {
        return true;
    }
}
/*
Author : Vineeth Vishwanath
Date : 20-02-18
Desc : Calling the transfer service 
*/
function performOwnTransfer() {
    if (kony.sdk.isNetworkAvailable()) {
        if (gblTModule == "web") {
            frmNewTransferKA.lblHiddenAmount.text = getPropAmt(frmConfirmTransferKA.lblWebAmt.text)
        } else {
            frmNewTransferKA.lblHiddenAmount.text = getPropAmt(frmNewTransferKA.lblHiddenAmount.text);
        }
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var controller = INSTANCE.getFormController("frmConfirmTransferKA");
        var controllerContextData = controller.getContextData() || new kony.sdk.mvvm.NavigationObject();
        controllerContextData.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
        controller.setContextData(controllerContextData);
        controller.performAction("saveData");
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        alert("" + kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"));
    }
}

function validateSelectedDay() {
    if (frmNewTransferKA.flxRecurrence.isVisible === true) {
        var currDate = frmNewTransferKA.calFrom.day;
        var currMonth = frmNewTransferKA.calFrom.month;
        var currYear = frmNewTransferKA.calFrom.year;
        var Obj = new Date(currMonth + "/" + currDate + "/" + currYear);
        kony.print("ObjDate::" + Obj);
        var currDay = Obj.getDay();
        var x = frmFrequencyList.segFrequencyList.selectedRowItems[0].Frequency;
        kony.print("currDayyyyy1::" + currDay);
        if (currDay === "5" || currDay === 5 || currDay === "6" || currDay === 6) {
            if (x !== geti18Value("i18n.Transfers.Instant")) {
                customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.onlyInWorkingDays"), popupCommonAlertDimiss, "");
                return false;
            }
        }
    }
    return true;
}
/*
Author : Vineeth Vishwanath
Date : 25-02-18
Desc : Enabling next after all teh required parameters are entered
*/
/*
Author : Vineeth Vishwanath
Date : 25-02-18
Desc : Enabling next after all teh required parameters are entered
*/
function enableNext() {
    if (frmNewTransferKA.txtBox.text !== "" && frmNewTransferKA.txtBox.text !== "." && frmNewTransferKA.txtBox.text != 0 && frmNewTransferKA.lblSelectanAccount1.isVisible === false && frmNewTransferKA.lblSelectanAccount2.isVisible === false) {
        if (frmNewTransferKA.flxRecurrence.isVisible === true && frmNewTransferKA.lblFre.text !== geti18Value("i18n.Transfers.OneTime")) {
            //added for 1532
            var freq = frmNewTransferKA.lblFre.text;
            var currToDate = frmNewTransferKA.calTo.day;
            var currToMonth = frmNewTransferKA.calTo.month;
            var currToYear = frmNewTransferKA.calTo.year;
            var Obj = new Date(currToMonth + "/" + currToDate + "/" + currToYear);
            var currDay = Obj.getDay();
            if ((frmNewTransferKA.txtNumRecurrences.text === "" || frmNewTransferKA.txtNumRecurrences.text == 0) && (currDay === "" || currDay === null)) {
                frmNewTransferKA.lblNext.skin = sknLblNextDisabled;
                frmNewTransferKA.lblNext.setEnabled(false);
                frmNewTransferKA.lblInvalidRecurrence.setVisibility(true);
                frmNewTransferKA.CopylblLine0iff8accf05a247.skin = "sknFlxOrangeLine";
            } else {
                if ((gblTModule === "send") && (gblSelectedBene.benType === "ITB" || gblSelectedBene.benType === "DTB")) {
                    if (frmNewTransferKA.txtDesc.text !== null && frmNewTransferKA.txtDesc.text !== "" && frmNewTransferKA.lblPOT.text !== geti18Value("i18n.settings.select")) {
                        frmNewTransferKA.lblNext.skin = sknLblNextEnabled;
                        frmNewTransferKA.lblNext.setEnabled(true);
                    } else {
                        frmNewTransferKA.lblNext.skin = sknLblNextDisabled;
                        frmNewTransferKA.lblNext.setEnabled(false);
                    }
                    if (frmNewTransferKA.txtDesc.text !== null && frmNewTransferKA.txtDesc.text !== "") {
                        frmNewTransferKA.lblLine5.skin = "lblLine0h8b99d8a85f649";
                    } else {
                        frmNewTransferKA.lblLine5.skin = "sknFlxOrangeLine";
                    }
                    if (frmNewTransferKA.lblPOT.text !== geti18Value("i18n.settings.select")) {
                        frmNewTransferKA.lblLine10.skin = "lblLine0h8b99d8a85f649";
                    } else {
                        frmNewTransferKA.lblLine10.skin = "sknFlxOrangeLine";
                    }
                    if (frmNewTransferKA.lblTransferCategory.text !== geti18Value("i18n.settings.select")) {
                        frmNewTransferKA.lblTransferCategoryLine.skin = "lblLine0h8b99d8a85f649";
                    } else {
                        frmNewTransferKA.lblTransferCategoryLine.skin = "sknFlxOrangeLine";
                    }
                } else {
                    frmNewTransferKA.lblNext.skin = sknLblNextEnabled;
                    frmNewTransferKA.lblNext.setEnabled(true);
                    frmNewTransferKA.lblInvalidRecurrence.setVisibility(false);
                    frmNewTransferKA.CopylblLine0iff8accf05a247.skin = "lblLine0h8b99d8a85f649";
                }
            }
        } else if ((gblTModule === "send") && (gblSelectedBene.benType === "ITB" || gblSelectedBene.benType === "DTB")) {
            if (frmNewTransferKA.txtDesc.text !== null && frmNewTransferKA.txtDesc.text !== "" && frmNewTransferKA.lblPOT.text !== geti18Value("i18n.settings.select")) {
                frmNewTransferKA.lblNext.skin = sknLblNextEnabled;
                frmNewTransferKA.lblNext.setEnabled(true);
                //frmNewTransferKA.lblLine5.skin = "lblLine0h8b99d8a85f649"; 
            } else {
                frmNewTransferKA.lblNext.skin = sknLblNextDisabled;
                frmNewTransferKA.lblNext.setEnabled(false);
                //frmNewTransferKA.lblLine5.skin = "sknFlxOrangeLine";
            }
            if (frmNewTransferKA.lblTransferCategory.text !== geti18Value("i18n.settings.select")) {
                frmNewTransferKA.lblTransferCategoryLine.skin = "lblLine0h8b99d8a85f649";
            } else {
                frmNewTransferKA.lblTransferCategoryLine.skin = "sknFlxOrangeLine";
            }
            if (frmNewTransferKA.lblPOT.text !== geti18Value("i18n.settings.select")) {
                frmNewTransferKA.lblLine10.skin = "lblLine0h8b99d8a85f649";
            } else {
                frmNewTransferKA.lblLine10.skin = "sknFlxOrangeLine";
            }
            if (frmNewTransferKA.txtDesc.text !== null && frmNewTransferKA.txtDesc.text !== "") {
                frmNewTransferKA.lblLine5.skin = "lblLine0h8b99d8a85f649";
            } else {
                frmNewTransferKA.lblLine5.skin = "sknFlxOrangeLine";
            }
        } else {
            frmNewTransferKA.lblNext.skin = sknLblNextEnabled;
            frmNewTransferKA.lblNext.setEnabled(true);
        }
    } else {
        frmNewTransferKA.lblNext.skin = sknLblNextDisabled;
        frmNewTransferKA.lblNext.setEnabled(false);
    }
    if ((frmNewTransferKA.flxRecurrence.isVisible === true) && isEmpty(frmNewTransferKA.txtNumRecurrences.text) && frmNewTransferKA.lblFre.text !== geti18Value("i18n.Transfers.OneTime")) {
        frmNewTransferKA.lblNext.skin = sknLblNextDisabled;
        frmNewTransferKA.lblNext.setEnabled(false);
    }
}

function newTransferDecimalCheck() {
    if (frmNewTransferKA.txtBox.text !== null && frmNewTransferKA.txtBox.text !== "") {
        var amount = frmNewTransferKA.txtBox.text;
        if (amount.indexOf(".") > -1) {
            amount = amount.substring(0, amount.indexOf(".") + 4);
        }
        frmNewTransferKA.txtBox.text = amount;
        enableNext();
    }
}

function fixDecimal(numText, numDec) {
    //alert("numText "+ numText + "numDec "+numDec);
    if (numText !== null && numText !== "") {
        try {
            if (numDec != 0) {
                numDec = parseInt(numDec) + 1;
            } else {
                if (numText.indexOf(".") !== -1 && numText.substring(numText.indexOf(".") + 1, numText.length).length >= numDec) {
                    return numText.substring(0, numText.indexOf("."));
                }
                return numText;
            }
            if (numText.indexOf(".") !== -1 && numText.substring(numText.indexOf(".") + 1, numText.length).length >= numDec) {
                numText = numText.substring(0, numText.indexOf(".")) + numText.split('').splice(numText.indexOf("."), numDec).join('');
            }
        } catch (e) {
            // DO NOTHING
        }
        return numText;
    } else {
        return "";
    }
}
/*
Author : Vineeth Vishwanath
Date : 07-03-18
Desc : Refreshiing the accounts after completeing a transfer and going to payment dashboard screen(To update the balances)
*/
function getAllAccounts() {
    if (kony.sdk.mvvm.isNetworkAvailabile()) {
        if (kony.sdk.mvvm.KonyApplicationContext.getAppInstance()) {
            ShowLoadingScreen();
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var options = {
                "access": "online",
                "objectName": "RBObjects"
            };
            var serviceName = "RBObjects";
            var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
            var dataObject = new kony.sdk.dto.DataObject("Accounts");
            var serviceOptions = {
                "dataObject": dataObject,
                "queryParams": {
                    "custId": custid,
                    "lang": kony.store.getItem("langPrefObj") === "en" ? "eng" : "ara"
                }
            };
            modelObj.fetch(serviceOptions, refreshSuc, refreshFail);
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function refreshSuc(response) {
    kony.print("refresh accounts:::" + JSON.stringify(response));
    try {
        if (isEmpty(response)) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup(geti18Value("i18n.common.Attention"), geti18Value("i18n.alerts.NoAccountsMsg"), popupCommonAlertDimiss, "");
            exceptionLogCall("::Getallaccounts Success::", "service response null", "service", response);
        } else {
            var segAccountListData = response;
            var fromAccounts = [],
                toAccounts = [],
                loansData = [];
            var sectionAccData = [],
                segDealsData = [],
                segLoansData = [];
            if ((!isEmpty(segAccountListData)) && segAccountListData.length > 0) {
                for (var i in segAccountListData) {
                    if (checkAccount(segAccountListData[i]["accountID"])) {
                        if (!isEmpty(segAccountListData[i]["AccNickName"])) segAccountListData[i]["accountName"] = segAccountListData[i]["AccNickName"];
                        if (segAccountListData[i]["accountType"] != "T" && segAccountListData[i]["accountType"] != "L" && segAccountListData[i]["dr_ind"] == "Y" && segAccountListData[i]["productCode"] != "320") {
                            fromAccounts.push(segAccountListData[i]);
                        }
                        if (segAccountListData[i]["accountType"] != "T" && segAccountListData[i]["accountType"] != "L" && segAccountListData[i]["cr_ind"] == "Y" && segAccountListData[i]["productCode"] != "320") {
                            toAccounts.push(segAccountListData[i]);
                        }
                    } else {
                        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                        customAlertPopup(geti18Value("i18n.common.Attention"), geti18nkey("i18n.security.loginagain"), logoutpp, "");
                        return;
                    }
                }
                kony.print("Refreshed fromAccounts:::" + JSON.stringify(fromAccounts));
                kony.print("Refreshed toAccounts:::" + JSON.stringify(toAccounts));
                kony.retailBanking.globalData.accountsDashboardData.fromAccounts = isEmpty(fromAccounts) ? {} : fromAccounts;
                kony.retailBanking.globalData.accountsDashboardData.toAccounts = isEmpty(toAccounts) ? {} : toAccounts;
                if (kony.newCARD.applyCardsOption === true || kony.application.getCurrentForm().id === "frmCardLinkedAccounts") {
                    gotoAccountDetailsScreen(1);
                    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                } else if (gblTModule === "AccNumberStatement" || gblTModule === "AccountOrder") {
                    accountsScreenPreshow(fromAccounts);
                    if (gblTModule === "AccountOrder") {
                        frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.my_money.accounts");
                    } else {
                        frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.my_money.accounts");
                    }
                    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                    frmAccountDetailsScreen.show();
                } else if (gblTModule == "own") {
                    if (hasEnoughAccounts()) {
                        //           frmNewTransferKA.lblCurrency.setVisibility(false);
                        ownAccountPreshow();
                        removeDatafrmTransfersform();
                        makeInlineErrorsTransfersInvisible();
                        if (kony.store.getItem("langPrefObj") !== "ar") {
                            frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.001" + " " + "JOD";
                        } else {
                            frmNewTransferKA.flxInvalidAmountField.text = "JOD" + " 0.001" + " " + geti18Value("i18n.transfer.minAmount");
                        }
                        gotoNewTransferScreen(1);
                        frmNewTransferKA.flxAcc1.setEnabled(true);
                    } else {
                        customAlertPopup(geti18Value("i18n.common.Attention"), geti18Value("i18n.Transfers.InsufficientAcc"), popupCommonAlertDimiss, "");
                    }
                    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                } else if (gblTModule == "send") {
                    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                    var controller = INSTANCE.getFormController("frmShowAllBeneficiary");
                    var navObject = new kony.sdk.mvvm.NavigationObject();
                    navObject.setRequestOptions("segAllBeneficiary", {
                        "headers": {},
                        "queryParams": {
                            "P_BENE_TYPE": "",
                            "custId": custid
                        }
                    });
                    controller.loadDataAndShowForm(navObject);
                } else if (gblTModule === "jomoDashBoard") {
                    call_serv_getjomoppayregdata();
                } else if (gblTModule == "jomo") {
                    //         frmJoMoPayLanding.show();
                    //         getJomoPayTransactionsList();
                    //      	if(glbJmpFlow === "dash"){
                    //        	call_serv_getjomoppayregdata();
                    //        }else{
                    preshowjomopay();
                    navigate_JOMOPAY_SENDMONEYWITHOUTBILLER();
                    //        }
                } else if (gblTModule === "jomoBiller") {
                    preshowjomopay();
                    var jmpType = "";
                    if (kony.application.getCurrentForm().id === "frmJoMoPayLanding") {
                        jmpType = frmJoMoPayLanding.segJomopayBeneficiary.selectedRowItems[0].bene_address1;
                        if (jmpType === "A") {
                            frmJoMoPay.lblType.text = geti18Value("i18n.jomopay.aliastype");
                            frmJoMoPay.lblBeneficiaryStaticText.text = geti18Value("i18n.jomopay.benificiaryalias");
                            //               frmJoMoPay.lblBeneficiaryStaticText.width = "41%";
                            frmJoMoPay.btnAliasDropDown.isVisible = false;
                            frmJoMoPay.txtPhoneNo.isVisible = false;
                            frmJoMoPay.lblMobileIDHint.setVisibility(false);
                            frmJoMoPay.txtAliasType.isVisible = true;
                            frmJoMoPay.txtAliasType.setEnabled(false);
                            frmJoMoPay.txtAliasType.text = frmJoMoPayLanding.segJomopayBeneficiary.selectedRowItems[0].benAcctNo; /* hassan */
                            frmJoMoPayConfirmation.lblJomopayType.text = "A";
                        } else {
                            frmJoMoPay.lblType.text = geti18Value("i18n.jomopay.mobiletype");
                            frmJoMoPay.lblBeneficiaryStaticText.text = geti18Value("i18n.jomopay.mobile");
                            //               frmJoMoPay.lblBeneficiaryStaticText.width = "37%";
                            frmJoMoPay.btnAliasDropDown.isVisible = false;
                            frmJoMoPay.txtAliasType.isVisible = false;
                            frmJoMoPay.txtPhoneNo.isVisible = true;
                            frmJoMoPay.lblMobileIDHint.setVisibility(true);
                            frmJoMoPay.lblMobileIDHint.setVisibility(true);
                            frmJoMoPay.txtPhoneNo.setEnabled(false);
                            frmJoMoPay.txtPhoneNo.text = frmJoMoPayLanding.segJomopayBeneficiary.selectedRowItems[0].benAcctNo;
                            frmJoMoPayConfirmation.lblJomopayType.text = "M";
                        }
                    } else {
                        if (frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== "" && frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== null && frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== undefined) {
                            frmJoMoPay.lblType.text = geti18Value("i18n.jomopay.mobiletype");
                            frmJoMoPay.lblBeneficiaryStaticText.text = geti18Value("i18n.jomopay.mobile");
                            //             	frmJoMoPay.lblBeneficiaryStaticText.width = "37%";
                            frmJoMoPay.btnAliasDropDown.isVisible = false;
                            frmJoMoPay.txtAliasType.isVisible = false;
                            frmJoMoPay.txtPhoneNo.isVisible = true;
                            frmJoMoPay.lblMobileIDHint.setVisibility(true);
                            frmJoMoPay.lblMobileIDHint.setVisibility(true);
                            frmJoMoPay.txtPhoneNo.setEnabled(false);
                            frmJoMoPay.txtPhoneNo.text = frmJOMOPayAddBiller.txtBeneficiaryMobile.text;
                            frmJoMoPayConfirmation.lblJomopayType.text = "M";
                            frmJoMoPay.txtAliasType.text = "";
                        } else {
                            frmJoMoPay.lblType.text = geti18Value("i18n.jomopay.aliastype");
                            frmJoMoPay.lblBeneficiaryStaticText.text = geti18Value("i18n.jomopay.benificiaryalias");
                            //             	frmJoMoPay.lblBeneficiaryStaticText.width = "41%";
                            frmJoMoPay.btnAliasDropDown.isVisible = false;
                            frmJoMoPay.txtPhoneNo.isVisible = false;
                            frmJoMoPay.lblMobileIDHint.setVisibility(false);
                            frmJoMoPay.txtAliasType.isVisible = true;
                            frmJoMoPay.txtAliasType.setEnabled(false);
                            frmJoMoPay.txtAliasType.text = frmJOMOPayAddBiller.txtBeneficiaryAlias.text;
                            frmJoMoPayConfirmation.lblJomopayType.text = "A";
                            frmJoMoPay.txtPhoneNo.text = "";
                        }
                        frmJOMOPayAddBiller.destroy();
                    }
                    frmJoMoPay.txtPhoneNo.setEnabled(false);
                    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                    frmJoMoPay.show();
                } else if (gblTModule == "web") {
                    var webAcc = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex];
                    frmWebCharge.lblhiddenToAcc.text = webAcc.card_acc_num;
                    frmConfirmTransferKA.toAccBranchCode.text = webAcc.card_acc_branch;
                    var acc = webAcc.card_acc_num;
                    //frmWebCharge.lblAccountCode.text = acc;//"****" + acc.substring(acc.length-4, acc.length);
                    frmWebCharge.lblCardnum.text = geti18Value("i18n.webcharge.currentbalance") + "\n" + webAcc.card_num.substring(0, 6) + "******" + webAcc.card_num.substring(webAcc.card_num.length - 4, webAcc.card_num.length);
                    /* if(kony.store.getItem("langPrefObj") === "en"){
        	frmWebCharge.lblCardnum.text = geti18Value("i18n.webcharge.currentbalance")+"  "+webAcc.card_num.substring(0,6)+"******" + webAcc.card_num.substring(webAcc.card_num.length-4, webAcc.card_num.length);
        }else{
        	frmWebCharge.lblCardnum.text = geti18Value("i18n.webcharge.currentbalance")+"  "+webAcc.card_num.substring(webAcc.card_num.length-4, webAcc.card_num.length)+"******"+webAcc.card_num.substring(0,6);
        }*/
                    var webChargeAccount = kony.retailBanking.globalData.accountsDashboardData.accountsData;
                    frmWebCharge.lblAmount.text = isEmpty(webAcc.card_balance) ? "0.000 JOD" : (formatamountwithCurrency(webAcc.card_balance, "JOD") + " JOD");
                    var storeData = isEmpty(webChargeAccount[0]) ? "" : webChargeAccount[0];
                    kony.store.setItem("frmAccount", storeData);
                    frmWebCharge.txtFieldAmount.text = "0.000";
                    check_numberOfAccounts();
                    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                    frmWebCharge.show();
                    frmWebCharge.forceLayout();
                } else if (gblTModule == "cards") {
                    var card = "";
                    kony.print("inside cards option :::");
                    //if(!isEmpty(kony.retailBanking.globalData.prCardLandinList)){
                    if (kony.application.getCurrentForm().id === "frmPaymentDashboard") {
                        if (!isEmpty(kony.retailBanking.globalData.prCreditCardPayList)) card = kony.retailBanking.globalData.prCreditCardPayList[0];
                        kony.print("hassan prCreditCardPayList::" + JSON.stringify(card));
                        /*if(isEmpty(card)){
                          customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.nocards"),popupCommonAlertDimiss,"");
                          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                          return;
                        }*/
                    } else card = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex];
                    //             kony.print("Selected card :: " + JSON.stringify(card));
                    if (!isEmpty(card)) {
                        if (!isEmpty(card.card_num)) card.hiddenCardNum = card.card_num.substring(0, 6) + "****" + card.card_num.substring(card.card_num.length - 4, card.card_num.length);
                    }
                    //             frmCreditCardPayment.lblCardNumber.text = card.card_num.substring(0, 6) + "****"+ card.card_num.substring(card.card_num.length-4, card.card_num.length);;
                    //             frmCreditCardPayment.hiddenCardNumber.text = card.card_num;
                    //             frmCreditCardPayment.lblTUAmount.text = formatAmountwithcomma(card.total_due_amt,3);
                    //             frmCreditCardPayment.lblPaymentValue.text = (card.min_amount_due == "")? "0.000":formatAmountwithcomma(card.min_amount_due,3);
                    //             frmCreditCardPayment.lblDate.text = formatDateMMDDYY(card.payment_due_date);
                    //         check_numberOfAccounts();
                    //}
                    var fromAccount = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
                    var accountNumber = fromAccount[0].accountID;
                    fromAccount[0].hiddenAccountNumber = accountNumber;
                    set_formCreditCardPayment(fromAccount[0]);
                    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                    frmCreditCardPayment.lblCardNumber.text = geti18Value("i18n.cards.selectCard");
                    frmCreditCardPayment.hiddenCardNumber.text = "";
                    //frmCreditCardPayment.txtFieldAmount.text = "";
                    // frmCreditCardPayment.txtBoxOtherCard.text = "";
                    kony.print("hassan card::" + JSON.stringify(card));
                    var icon_POSITION = "left";
                    if (kony.store.getItem("langPrefObj") == "ar") icon_POSITION = "right";
                    if (!isEmpty(card)) {
                        kony.print("hassan1111");
                        frmCreditCardPayment.lblOtherCard.setVisibility(true); // hassan other cards visible
                        frmCreditCardPayment.btnCard.setVisibility(true);
                        frmCreditCardPayment.lblBOJCard.setVisibility(true);
                        //             	frmCreditCardPayment.btnOtherCard.left = "5%";
                        //             	frmCreditCardPayment.lblOtherCard.left = "15%";
                        frmCreditCardPayment.btnOtherCard[icon_POSITION] = "53%";
                        frmCreditCardPayment.lblOtherCard[icon_POSITION] = "64%";
                        onRowClickCC(card);
                    } else {
                        kony.print("hassan");
                        frmCreditCardPayment.btnOtherCard[icon_POSITION] = "5%";
                        frmCreditCardPayment.lblOtherCard[icon_POSITION] = "15%";
                        frmCreditCardPayment.btnCard.setVisibility(false);
                        frmCreditCardPayment.lblBOJCard.setVisibility(false);
                        onclickothercardbtn();
                        frmCreditCardPayment.show();
                    }
                    //         frmCreditCardPayment.show();
                    //         var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                    //         var listController = INSTANCE.getFormController("frmCreditCardPayment");
                    //         var navigationObject = new kony.sdk.mvvm.NavigationObject();
                    //         navigationObject.setDataModel(null,kony.sdk.mvvm.OperationType.ADD, "form");
                    // //         navigationObject.setRequestOptions("form",{"headers":{}});
                    //         listController.performAction("navigateTo",["frmCreditCardPayment",navigationObject]);
                    //}
                } else if (gblTModule == "billspay") {
                    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                    var controller = INSTANCE.getFormController("frmManagePayeeKA");
                    var navObject = new kony.sdk.mvvm.NavigationObject();
                    navObject.setRequestOptions("managepayeesegment", {
                        "headers": {},
                        "queryParams": {
                            "custId": custid,
                            "P_BENE_TYPE": "PRE",
                            "lang": (kony.store.getItem("langPrefObj") === "en") ? "eng" : "ara"
                        }
                    });
                    controller.loadDataAndShowForm(navObject);
                } else if (gblTModule === "CLIQ") {
                    serv_IPS_REGISTRATION_STATUS();
                } else if (gblTModule === "OpenTermDeposite") {
                    gotoAccountDetailsScreen(1);
                    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                } else if (gblTModule === "OpenTermDepositInterestAccount") {
                    gotoAccountDetailsScreen(1);
                    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                }
                //frmPaymentDashboard.show();
                if (gblTModule === "") kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            } else {
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                customAlertPopup(geti18Value("i18n.common.Attention"), geti18Value("i18n.alerts.NoAccountsMsg"), popupCommonAlertDimiss, "");
                exceptionLogCall("::Getallaccounts Success::", "service response null", "service", response);
            }
        }
        startChatbotAfterLogin(custid, false);
    } catch (e) {
        exceptionLogCall("::Getallaccounts Success::", "Exception while handling success callback", "UI", e);
    }
}

function refreshFail(err) {
    kony.application.dismissLoadingScreen();
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("::Getallaccounts Failed::", "service failed to respond", "service", err);
}

function check_numberOfAccounts() {
    try {
        kony.print("gblDefaultAccPayment ::" + gblDefaultAccPayment + " :: gblDefaultAccTransfer ::" + gblDefaultAccTransfer);
        var accountsData = [];
        var data = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
        if (gblTModule === "BillPayBillers" || gblTModule === "BillPay" || gblTModule === "BillPayNewBillKA" || gblTModule === "BulkPrePayPayment" || gblTModule === "BillPrePayNewBillKA") {
            if (!isEmpty(gblDefaultAccPayment) && accountsData.length !== 1) {
                for (var j in data) {
                    if ((data[j].accountID === gblDefaultAccPayment)) {
                        accountsData.push(data[j]);
                        break;
                    }
                }
            }
        } else if (gblTModule === "send") {
            if (!isEmpty(gblDefaultAccTransfer) && accountsData.length !== 1) {
                for (var i in data) {
                    if ((data[i].accountID === gblDefaultAccTransfer)) {
                        accountsData.push(data[i]);
                        break;
                    }
                }
            }
        } else accountsData = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
        kony.print("gblTModule ::" + gblTModule + " :: length :: " + accountsData.length);
        if (accountsData.length === 1) {
            if (gblTModule === "send") {
                kony.store.setItem("toval", 0);
                set_formTransferData(accountsData[0]);
            } else if (gblTModule === "own") {
                kony.store.setItem("toval", 1);
                set_formOwnTransferData(accountsData[0]);
            } else if (gblTModule === "BillPayBillers") {
                set_formRegisteredPayBill(accountsData[0]);
            } else if (gblTModule === "cards" || gblTModule === "web") {
                var accountNumber = accountsData[0].accountID;
                accountsData[0].hiddenAccountNumber = "***" + accountNumber.substring(accountNumber.length - 5, accountNumber.length);
                accountsData[0].balance = accountsData[0].availableBalance + " " + accountsData[0].currencyCode;
                if (gblTModule === "cards") set_formCreditCardPayment(accountsData[0]);
                else set_formWebChargeTopUp(accountsData[0]);
            } else if (gblTModule === "BillPay" || gblTModule === "BillPayNewBillKA" || gblTModule === "BulkPrePayPayment" || gblTModule === "BillPrePayNewBillKA") {
                set_formNewPayBill(accountsData[0]);
            }
        } else {
            if (gblTModule === "BillPay") {
                frmNewBillDetails.show();
            } else if (gblTModule === "BillPayBillers") {
                frmBills.show();
            } else if (gblTModule !== "web" && gblTModule !== "cards" && gblTModule !== "BillPayNewBillKA" && gblTModule !== "BulkPrePayPayment" && gblTModule !== "BillPrePayNewBillKA") {
                frmNewTransferKA.show();
            }
        }
    } catch (e) {
        kony.print("Exception_check_numberOfAccounts ::" + e);
        exceptionLogCall("::check_numberOfAccounts::", "default account handling", "UI", e);
    }
}

function set_formTransferData(dataSelected) {
    try {
        getDecimalNumForCurr(dataSelected.currencyCode);
        frmNewTransferKA.lblAccountName2.text = dataSelected.accountName;
        frmNewTransferKA.lblAccountNumber2.text = dataSelected.accountID;
        if (dataSelected.AccNickName !== "" && dataSelected.AccNickName !== undefined) {
            frmNewTransferKA.lblAccountNumber2.isVisible = false;
        } else {
            frmNewTransferKA.lblAccountNumber2.isVisible = true;
        }
        if (dataSelected.accountName !== "" && dataSelected.accountName !== null) showLetterPicture(dataSelected.accountName, 2);
        var storeData = isEmpty(dataSelected) ? "" : dataSelected;
        kony.store.setItem("frmAccount", storeData);
        frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyFrom");
        frmNewTransferKA.lblCurrency.text = "";
        if (gblSelectedBene.benType === "DTB" || gblSelectedBene.benType === "ITB") {
            get_NATIONAL_INTERNATIONAL_CURRENCYLIST();
            for (var j in gblCurrList) {
                if (dataSelected.currencyCode === gblCurrList[j].CURR_ISO) {
                    kony.print("benType ::" + gblSelectedBene.benType);
                    kony.print("Check ::" + dataSelected.currencyCode + " :: CURRENCY ::" + CURRENCY);
                    switch (dataSelected.currencyCode) {
                        case "JOD":
                            if ((get_COUNTRY_DETAILS("2").CTRY_CODE != gblSelectedBene.countryName) && (gblSelectedBene.benType === "ITB")) {} else {
                                frmNewTransferKA.lblCurrency.text = dataSelected.currencyCode;
                                getDecimalNumForCurr(dataSelected.currencyCode);
                            }
                            break;
                        default:
                            kony.print("Switch case default");
                            frmNewTransferKA.lblCurrency.text = dataSelected.currencyCode;
                            getDecimalNumForCurr(dataSelected.currencyCode);
                            break;
                    }
                    break;
                }
            }
        } else {
            frmNewTransferKA.lblCurrency.text = dataSelected.currencyCode;
            frmNewTransferKA.lblToAccCurr.text = getCurrency_FROM_ACCOUNT_NUMBER(gblSelectedBene.benAcctNo);
        }
        frmNewTransferKA.lblFromAccCurr.text = dataSelected.currencyCode;
        //frmNewTransferKA.lblAvailableLimit.setVisibility(true);
        var limite = 3;
        if (kony.string.equalsIgnoreCase(dataSelected.currencyCode, "JOD") || kony.string.equalsIgnoreCase(dataSelected.currencyCode, "BHD")) {
            limite = 3;
        } else {
            limite = 2;
        }
        var valuee = formatAmountwithcomma(dataSelected.availableBalance, limite);
        if (kony.store.getItem("langPrefObj") === "ar")
            if (new RegExp(/\-/g).test(valuee)) {
                valuee = valuee.replace("-", "") + "-"
            }
            //frmNewTransferKA.lblAvailableLimit.text = geti18Value("i18n.common.AvailableLimit")+" "+valuee+" "+dataSelected.currencyCode;
        if (frmNewTransferKA.lblCurrency.text === "JOD") {
            if (kony.store.getItem("langPrefObj") !== "ar") {
                frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.001" + " " + frmNewTransferKA.lblCurrency.text;
            } else {
                frmNewTransferKA.flxInvalidAmountField.text = frmNewTransferKA.lblCurrency.text + " 0.001" + " " + geti18Value("i18n.transfer.minAmount");
            }
        } else {
            if (kony.store.getItem("langPrefObj") !== "ar") {
                frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.01" + " " + frmNewTransferKA.lblCurrency.text;
            } else {
                frmNewTransferKA.flxInvalidAmountField.text = frmNewTransferKA.lblCurrency.text + " 0.01" + " " + geti18Value("i18n.transfer.minAmount");
            }
        }
        gblDefCurr = dataSelected.currencyCode;
        frmNewTransferKA.txtBox.text = formatAmountwithcomma(frmNewTransferKA.lblHiddenAmount.text, frmNewTransferKA.lblDecimal.text);
        frmNewTransferKA.lblCurrency.setVisibility(true);
        frmNewTransferKA.show();
        if (gblLaunchModeOBJ.lauchMode) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }
        frmNewTransferKA.txtBox.text = fixDecimal(frmNewTransferKA.txtBox.text, getDecimalNumForCurr(frmNewTransferKA.lblCurrency.text));
        checkPlaceholder(frmNewTransferKA.lblDecimal.text);
        gblFeesFrom = "fromAcc";
        getCommisionFee();
        //serv_FETCH_TRANSACTION_LIMIT();  available limit
    } catch (e) {
        kony.print("Exception_set_formTransferData ::" + e);
        exceptionLogCall("::set_formTransferData::", "send money account assigning", "UI", e);
    }
}

function set_formOwnTransferData(dataSelected) {
    try {
        var flg = 0;
        var abc = dataSelected.accountID;
        frmNewTransferKA.lblSelectanAccount2.setVisibility(false);
        toval = kony.store.getItem("toval");
        if (toval === 1) {
            text = geti18Value("i18n.Transfers.SendMoneyTo");
            var storeData = isEmpty(dataSelected) ? "" : dataSelected;
            kony.store.setItem("toAccount", storeData);
            gblDefCurr = dataSelected.currencyCode;
        } else {
            text = geti18Value("i18n.Transfers.SendMoneyFrom");
            var storeData = isEmpty(dataSelected) ? "" : dataSelected;
            kony.store.setItem("frmAccount", storeData);
        }
        getDecimalNumForCurr(dataSelected.currencyCode);
        checkPlaceholder(frmNewTransferKA.lblDecimal.text);
        setAccountDetailstoFlex(toval, dataSelected);
        frmNewTransferKA.show();
        frmAccountDetailsScreen.lblHead.text = text;
        frmAccountDetailsScreen.segFre.widgetDataMap = {
            lblAccountName: "accountName",
            lblAccountNumber: "hiddenAccountNumber",
            lblAmount: "availableBalance"
        };
        frmAccountDetailsScreen.segFre.setData(AccDetailsSegdata);
    } catch (e) {
        kony.print("Exception_set_formOwnTransferData ::" + e);
        exceptionLogCall("::set_formOwnTransferData::", "own transfer account assigning", "UI", e);
    }
}

function set_formNewPayBill(dataSelected) {
    try {
        kony.print("Data Selected ::" + JSON.stringify(dataSelected));
        var name = "";
        if (dataSelected.accountName !== null && dataSelected.accountName !== undefined) {
            if (dataSelected.AccNickName !== "" && dataSelected.AccNickName !== null) name = dataSelected.AccNickName;
            else name = dataSelected.accountName;
        } else {
            name = dataSelected.name_on_card;
        }
        if (gblTModule === "BulkPrePayPayment") { // hassan bulkpayment
            frmBulkPaymentKA.lblPaymentMode.text = name;
            frmBulkPaymentKA.lblPaymentMode.skin = "sknLblBack";
            frmBulkPaymentKA.flxUnderlinePaymentModeBulk.skin = "sknFlxGreenLine";
            frmBulkPaymentKA.lblBranchCode.text = dataSelected.branchNumber;
            frmBulkPaymentKA.lblNext.setEnabled(true);
            var storeData = isEmpty(dataSelected) ? "" : dataSelected;
            kony.store.setItem("BillPayfromAcc", storeData);
        } else if (kony.boj.selectedBillerType === "PostPaid") {
            frmNewBillDetails.lblPaymentMode.text = name;
            frmNewBillDetails.lblPaymentMode.skin = "sknLblBack";
            var storeData = isEmpty(dataSelected) ? "" : dataSelected;
            kony.store.setItem("BillPayfromAcc", storeData);
            var balance = isEmpty(kony.store.getItem("BillPayfromAcc").availableBalance) ? kony.store.getItem("BillPayfromAcc").spending_limit.replace(/,/g, "") : kony.store.getItem("BillPayfromAcc").availableBalance;
            frmNewBillDetails.flxConversionAmt.isVisible = false;
            if (kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD" && frmNewBillDetails.btnBillsPayCards.isVisible === false) {
                gblQuickFlow = "postBillAcc";
                serv_BILLSCOMISSIONCHARGE(frmNewBillDetails.tbxAmount.text.replace(/,/g, ""));
            } else if (!check_SUFFICIENT_BALANCE(frmNewBillDetails.tbxAmount.text.replace(/,/g, ""), balance)) {
                if (frmNewBillDetails.btnBillsPayCards.isVisible === true) customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInCard"), popupCommonAlertDimiss, "");
                else customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
                frmNewBillDetails.flxConversionAmt.setVisibility(false);
                frmNewBillDetails.lblNext.skin = "sknLblNextDisabled";
            }
            frmNewBillDetails.show();
        } else if (gblTModule === "BillPrePayNewBillKA") { /* hassan */
            frmPrePaidPayeeDetailsKA.lblPaymentMode.text = name;
            frmPrePaidPayeeDetailsKA.lblPaymentMode.skin = "sknLblBack";
            frmPrePaidPayeeDetailsKA.lblNext.skin = "sknLblNextEnabled";
            kony.store.setItem("BillPayfromAcc", dataSelected);
            /*hasssan pay from credit card */
            var bal = 0;
            if (frmPrePaidPayeeDetailsKA.btnBillsPayCards.text === "t") bal = parseFloat(kony.store.getItem("BillPayfromAcc").spending_limit.replace(/,/g, ""));
            else bal = kony.store.getItem("BillPayfromAcc").availableBalance;
            /*hasssan pay from credit card */
            frmPrePaidPayeeDetailsKA.flxConversionAmt.isVisible = false;
            /*hasssan pay from credit card */
            //if(!check_SUFFICIENT_BALANCE(0, kony.store.getItem("BillPayfromAcc").availableBalance) &&
            if (!check_SUFFICIENT_BALANCE(0, bal) && frmPrePaidPayeeDetailsKA.flxAmount.isVisible === false) {
                if (frmPrePaidPayeeDetailsKA.btnBillsPayCards.text === "t") // hassan pay from credit card
                    customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInCard"), popupCommonAlertDimiss, "");
                else customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
                frmPrePaidPayeeDetailsKA.flxConversionAmt.setVisibility(false);
                frmPrePaidPayeeDetailsKA.lblNext.skin = "sknLblNextDisabled";
                frmPrePaidPayeeDetailsKA.flxUnderlinePaymentModeBulk.skin = "sknFlxGreyLine";
            } else if (kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD" && frmPrePaidPayeeDetailsKA.flxAmount.isVisible === true && frmPrePaidPayeeDetailsKA.btnBillsPayCards.text === false) {
                //           gblQuickFlow = "postBillAcc";
                /*hasssan pay from credit card */
                //serv_BILLSCOMISSIONCHARGE(frmPrePaidPayeeDetailsKA.txtAmount.text.replace(/,/g,""));
                //}else if(!check_SUFFICIENT_BALANCE(frmPrePaidPayeeDetailsKA.txtAmount.text.replace(/,/g,""), kony.store.getItem("BillPayfromAcc").availableBalance) &&
                serv_BILLSCOMISSIONCHARGE(frmPrePaidPayeeDetailsKA.txtAmount.text.replace(/,/g, ""));
            } else if (!check_SUFFICIENT_BALANCE(frmPrePaidPayeeDetailsKA.txtAmount.text.replace(/,/g, ""), bal) && frmPrePaidPayeeDetailsKA.flxAmount.isVisible === true) {
                if (frmPrePaidPayeeDetailsKA.btnBillsPayCards.text === "t") // hassan pay from credit card
                    customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInCard"), popupCommonAlertDimiss, "");
                else customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
                frmPrePaidPayeeDetailsKA.flxConversionAmt.setVisibility(false);
                frmPrePaidPayeeDetailsKA.lblNext.skin = "sknLblNextDisabled";
                frmPrePaidPayeeDetailsKA.flxUnderlinePaymentModeBulk.skin = "sknFlxGreyLine";
            }
        } else if (kony.boj.selectedBillerType === "PrePaid") {
            frmNewBillKA.lblPaymentMode.text = name;
            frmNewBillKA.lblPaymentMode.skin = "sknLblBack";
            var storeData = isEmpty(dataSelected) ? "" : dataSelected;
            kony.store.setItem("BillPayfromAcc", storeData);
            var balance = isEmpty(dataSelected.spending_limit) ? dataSelected.availableLimit : dataSelected.spending_limit;
            if ((parseInt(frmNewBillKA.tbxAmount.text.replace(/,/g, "")) > 0) && ((dataSelected.currencyCode === "JOD") || (frmNewBillKA.btnBillsPayCards.text === "t"))) {
                if (!check_SUFFICIENT_BALANCE(frmNewBillKA.tbxAmount.text.replace(/,/g, ""), balance) && frmNewBillKA.flxAmount.isVisible === true) {
                    if (frmNewBillKA.btnBillsPayCards.text === "t") customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInCard"), popupCommonAlertDimiss, "");
                    else customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
                }
            }
        }
    } catch (e) {
        kony.print("Exception_set_formNewPayBill ::" + e);
        exceptionLogCall("::set_formNewPayBill::", "new bill payment account assigning", "UI", e);
    }
}

function set_formRegisteredPayBill(dataSelected) {
    try {
        if (dataSelected.accountName !== null && dataSelected.accountName !== undefined) {
            if (dataSelected.AccNickName !== "" && dataSelected.AccNickName !== null) frmBills.lblPaymentMode.text = dataSelected.AccNickName;
            else frmBills.lblPaymentMode.text = dataSelected.accountName;
        } else {
            frmBills.lblPaymentMode.text = dataSelected.name_on_card;
        }
        frmBills.flxUnderlinePaymentMode.skin = "sknFlxGreyLine";
        kony.print("dataSelected ::" + JSON.stringify(dataSelected));
        frmBills.lblPaymentMode.skin = "sknLblBack";
        var storeData = isEmpty(dataSelected) ? "" : dataSelected;
        kony.store.setItem("BillPayfromAcc", storeData);
        // hassan 
        frmBills.flxConversionAmt.isVisible = false;
        if (frmBills.btnBillsPayAccounts.text === "t")
            if (kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD") {
                gblQuickFlow = "postBillAcc";
                serv_BILLSCOMISSIONCHARGE(frmBills.tbxAmount.text.replace(/,/g, ""));
            }
        frmBills.show();
    } catch (e) {
        kony.print("Exception_set_formRegisteredPayBill ::" + e);
        exceptionLogCall("::set_formRegisteredPayBill::", "registered bill account assigning", "UI", e);
    }
}

function set_formCreditCardPayment(dataSelected) {
    try {
        kony.print("In Credit card Payment Selected::" + JSON.stringify(dataSelected));
        var storeData = isEmpty(dataSelected) ? "" : dataSelected;
        kony.store.setItem("BillPayfromAcc", storeData);
        frmCreditCardPayment.lblAccountType.text = dataSelected.accountName + " " + dataSelected.hiddenAccountNumber;
        frmCreditCardPayment.lblAccountCode.text = dataSelected.hiddenAccountNumber;
        frmCreditCardPayment.hiddenLblAccountNum.text = dataSelected.accountID;
        frmCreditCardPayment.lblBranchNum.text = dataSelected.branchNumber;
        frmCreditCardPayment.lblFromCurr.text = dataSelected.currencyCode;
        frmCreditCardPayment.lblCurrency.text = dataSelected.currencyCode;
        if (frmCreditCardPayment.lblCurrency.text === "JOD") {
            kony.print("JOD currency " + frmCreditCardPayment.lblCurrency.text);
            frmCreditCardPayment.txtFieldAmount.placeholder = "0.000";
            frmCreditCardPayment.txtFieldAmount.text = "";
        } else {
            kony.print("Other currency " + frmCreditCardPayment.lblCurrency.text);
            frmCreditCardPayment.txtFieldAmount.placeholder = "0.00";
            frmCreditCardPayment.txtFieldAmount.text = "";
        }
    } catch (e) {
        kony.print("Exception_set_formCreditCardPayment ::" + e);
        exceptionLogCall("::set_formCreditCardPayment::", "credit card payment account assigning", "UI", e);
    }
}

function set_formWebChargeTopUp(dataSelected) {
    try {
        frmWebCharge.lblAccountType.text = dataSelected.accountName;
        frmWebCharge.lblAccountCode.text = dataSelected.accountID;
        frmWebCharge.lblCurrencyCode.text = dataSelected.currencyCode;
        frmWebCharge.lblhiddenFrmAcc.text = dataSelected.accountID;
        //frmWebCharge.lblAmount.text=dataSelected.availableBalance;
        var storeData = isEmpty(dataSelected) ? "" : dataSelected;
        kony.store.setItem("frmAccount", storeData);
        if (frmWebCharge.lblCurrencyCode.text === "JOD") {
            frmWebCharge.txtFieldAmount.placeholder = "0.000";
        } else {
            frmWebCharge.txtFieldAmount.placeholder = "0.00";
        }
    } catch (e) {
        kony.print("Exception_set_formWebChargeTopUp ::" + e);
        exceptionLogCall("::set_formWebChargeTopUp::", "webcharge card payment account assigning", "UI", e);
    }
}
/* Hassan CardLess*/
function set_formCardlessTransaction(dataSelected) {
    try {
        kony.print("Data Selected ::" + JSON.stringify(dataSelected));
        var name = "";
        if (!isEmpty(dataSelected.accountName)) {
            if (!isEmpty(dataSelected.AccNickName)) name = dataSelected.AccNickName;
            else name = dataSelected.accountName;
        } else {
            name = dataSelected.name_on_card;
        }
        kony.print("name ::" + name + " :: gblTModule ::" + gblTModule);
        if (gblTModule === "CardLessTransaction") {
            frmCardlessTransaction.lblPaymentMode.text = name;
            frmCardlessTransaction.lblAccountNumber.text = dataSelected.accountID;
            frmCardlessTransaction.lblPaymentMode.skin = "sknLblBack";
            frmCardlessTransaction.flxUnderlinePaymentModeBulk.skin = "sknFlxGreenLine";
            frmCardlessTransaction.lblBranchCode.text = dataSelected.branchNumber;
            var storeData = isEmpty(dataSelected) ? "" : dataSelected;
            kony.store.setItem("BillPayfromAcc", storeData);
        }
        checkNextCardless();
    } catch (e) {
        kony.print("Exception_set_formCardlessTransaction ::" + e);
        exceptionLogCall("set_formCardlessTransaction", "UI ERROR", "UI", e);
    }
}
/* Hassan CardLess*/
function customVerb_CARDSDETAILS() {
    //try{
    var queryParams = {
        "custId": custid,
        "Cardflag": "A"
    };
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetCrCardDetails");
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        kony.print("queryParams ::" + JSON.stringify(queryParams));
        appMFConfiguration.invokeOperation("prGetCrCardDetails", {}, queryParams, function(res) {
            kony.print("success response ::" + JSON.stringify(res));
            if (res.CardDetails !== undefined && res.CardDetails !== null && !isEmpty(res.CardDetails)) {
                kony.retailBanking.globalData.prCreditCardPayList = [];
                for (var i in res.CardDetails) {
                    kony.retailBanking.globalData.prCreditCardPayList.push(res.CardDetails[i]);
                }
                for (var i in kony.retailBanking.globalData.prCreditCardPayList) {
                    kony.retailBanking.globalData.prCreditCardPayList[i].cardTypeFlag = "C";
                }
            } else {
                kony.retailBanking.globalData.prCreditCardPayList = [];
                if (kony.application.getCurrentForm().id !== "frmPaymentDashboard") customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.nocards"), popupCommonAlertDimiss, "");
            }
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            if (kony.application.getCurrentForm().id === "frmPaymentDashboard") {
                gblTModule = "cards";
                kony.boj.creditcardOption = "fromCreditCardPayments";
                kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
                getAllAccounts();
            }
        }, function(err) {
            kony.print("error in service ::" + JSON.stringify(err));
            if (kony.application.getCurrentForm().id === "frmPaymentDashboard") {
                customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.cards.unabletoretrivecreditcard"), popupCommonAlertDimiss, "");
            };
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        });
    }
    /***
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Cards", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Cards");
//     dataObject.addField("usr", "icbsserv");
//     dataObject.addField("pass", "icbsserv_1");
    dataObject.addField("custId", custid);
    dataObject.addField("CardNum", "");
    dataObject.addField("Cardflag", "A");
    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };

    kony.print("dataObject::"+JSON.stringify(dataObject));
    if (kony.sdk.isNetworkAvailable()) {
      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      modelObj.customVerb("get", serviceOptions, function(res){
        kony.print("Transfer Cards Success ::"+JSON.stringify(res));
          if(res.CardDetails !== undefined && res.CardDetails !== null){
            for(var i in res.CardDetails){
              kony.retailBanking.globalData.prCardLandinList.push(res.records[0].CardDetails[i]);
            }
            for(var i in kony.retailBanking.globalData.prCardLandinList){
              kony.retailBanking.globalData.prCardLandinList[i].cardTypeFlag = "C";
            }
          }
        }

        var queryParams = {
          "custId": custid+":"+custid+":",
          "loop_count": parseInt(2),
          "loop_seperator": ":",
          "CardNum":"",
          "Cardflag" : "D:W:"

        };

        var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("DebitCardList");
        appMFConfiguration.invokeOperation("getDebitCardList", {},queryParams,loopingSuccessCallback,loopingErrorCallback);
      }, function(err){kony.print("Transfer Cards Failure ::"+JSON.stringify(err));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
    }
  }catch(e){
    kony.print("Exception_customVerb_CARDSDETAILS ::"+e);
  }
  function loopingSuccessCallback(response){
    kony.print("looping success callback ::"+JSON.stringify(response));
    for(var i in response.LoopDataset[0].DWCardList){
      response.LoopDataset[0].DWCardList[i].cardTypeFlag = "D";
      kony.retailBanking.globalData.prCardLandinList.push(response.LoopDataset[0].DWCardList[i]);
    }
    for(i in response.LoopDataset[1].DWCardList){
      response.LoopDataset[1].DWCardList[i].cardTypeFlag = "W";
      kony.retailBanking.globalData.prCardLandinList.push(response.LoopDataset[1].DWCardList[i]);
    }
    kony.print("Cards List ::"+JSON.stringify(kony.retailBanking.globalData.prCardLandinList));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    if(kony.retailBanking.globalData.prCardLandinList.length === 0){
      customAlertPopup("", geti18Value("i18n.common.nocards"), popupCommonAlertDimiss, "");
      var currForm = kony.application.getCurrentForm();
      currForm["btnBillsPayAccounts"].text = "t";
      currForm["btnBillsPayCards"].text = "s";
      currForm["lblPaymentMode"].text = geti18nkey("i18n.billsPay.Accounts");
      return;
    }
  }
  function loopingErrorCallback(error){
    kony.print("looping error callback ::"+JSON.stringify(error));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }****/
}

function addZerosForBenificisry(str) {
    var n = str.length;
    switch (n) {
        case 1:
            str = "00" + str;
            break;
        case 2:
            str = "0" + str;
            break;
        case 3:
            str = str;
            break;
        default:
            str = str;
            break;
    }
    kony.print("str :: " + str);
    return str;
}

function get_COUNTRYNAME(code) {
    try {
        for (var i in kony.boj.detailsForBene.Country) {
            if (kony.boj.detailsForBene.Country[i].CTRY_CODE == code) {
                return kony.boj.detailsForBene.Country[i].CTRY_S_DESC;
            }
        }
        return code;
    } catch (e) {
        kony.print("Exception _get_COUNTRYNAME ::" + e);
    }
}

function get_COUNTRYCode(code) {
    try {
        for (var i in kony.boj.detailsForBene.Country) {
            if (kony.boj.detailsForBene.Country[i].CTRY_CODE == code || kony.boj.detailsForBene.Country[i].CTRY_S_DESC == code) {
                return kony.boj.detailsForBene.Country[i].CTRY_CODE;
            }
        }
        return code;
    } catch (e) {
        kony.print("Exception _get_COUNTRYNAME ::" + e);
    }
}

function append_VALUE(data, appendText, count) {
    try {
        for (var i = 0; i < count; i++) data = data + appendText;
        return data;
    } catch (e) {
        kony.print("Exception_append_VALUE ::" + e);
    }
}

function navtoAddbeneforSendMoney() {
    kony.boj.detailsForBene.flag = false;
    kony.boj.addBeneVar.selectedType = "BOJ";
    if (!isEmpty(gblLaunchModeOBJ.accno)) {
        kony.boj.addBeneVar.selectedType = "BOJ";
    } else if (!isEmpty(gblLaunchModeOBJ.iban)) {
        if (gblLaunchModeOBJ.iban.startsWith("JO") || gblLaunchModeOBJ.iban.startsWith("jo")) kony.boj.addBeneVar.selectedType = "DOM";
        else kony.boj.addBeneVar.selectedType = "INT";
    }
    kony.boj.getBranchList();
}

function recurrence_DATE_SELECTION(type) {
    try {
        if (type === geti18Value("i18n.transfers.Daily")) {
            //         	alert(JSON.stringify(frmNewTransferKA.calTo.date));
            var date1 = frmNewTransferKA.calTo.dateComponents[1] + "/" + frmNewTransferKA.calTo.dateComponents[0] + "/" + frmNewTransferKA.calTo.dateComponents[2];
            var date2 = frmNewTransferKA.calFrom.dateComponents[1] + "/" + frmNewTransferKA.calFrom.dateComponents[0] + "/" + frmNewTransferKA.calFrom.dateComponents[2];
            var recurrence = (new Date(date1) - new Date(date2));
            recurrence = recurrence / (1000 * 60 * 60 * 24);
            frmNewTransferKA.txtNumRecurrences.text = (recurrence + 1).toString().replace(/\./g, '');
            frmNewTransferKA.lblInvalidRecurrence.setVisibility(false);
            frmNewTransferKA.CopylblLine0iff8accf05a247.skin = "lblLine0h8b99d8a85f649";
            //end
            enableNext();
        }
    } catch (e) {
        kony.print("Exception_recurrence_DATE_SELECTION ::" + e);
    }
}

function showAddBenfiWithPreFilledData() {
    kony.print("Showwwww111111111111");
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmAddExternalAccountKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
    navObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
    navObject.setRequestOptions("form", {
        "headers": {}
    });
    listController.performAction("navigateTo", ["frmAddExternalAccountKA", navObject]);
}

function SetupTxtBoxKeypad() {
    frmNewTransferKA.txtBox.keyboardActionLabel = constants.TEXTBOX_KEYBOARD_LABEL_DONE;
    frmNewTransferKA.txtDesc.keyboardActionLabel = constants.TEXTBOX_KEYBOARD_LABEL_DONE;
}