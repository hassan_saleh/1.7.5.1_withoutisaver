/*
 * Controller Extension class for frmDeviceRegistration
 * Developer can edit the existing methods or can add new methods if required
 *
 */
kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};
/**
 * Creates a new Form Controller Extension.
 * @class frmDeviceRegistrationControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmDeviceRegistrationControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmDeviceRegistrationControllerExtension#
     */
    fetchData: function() {
        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
            kony.sdk.mvvm.log.info("success fetching data ", response);
            scopeObj.getController().processData(response);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmDeviceRegistrationControllerExtension#
     * @returns {Object} - processed data
     */
    processData: function(data) {
        try {
            var scopeObj = this;
            var processedData = this.$class.$superp.processData.call(this, data);
            this.getController().bindData(processedData);
            return processedData;
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        };
    },
    /** 
     * This method binds the processed data to the form. Developer can edit.
     * Default implementation binds the data to widgets in the form.
     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmDeviceRegistrationControllerExtension#
     */
    bindData: function(data) {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.clear();
            this.$class.$superp.bindData.call(this, data);
            this.getController().getFormModel().formatUI();
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            this.getController().showForm();
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmDeviceRegistrationControllerExtension#
     */
    saveData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.saveData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully created record
            kony.boj.dismissPopup();
            if (!isEmpty(res.statusCode) && res.statusCode === "S0009") {
                customAlertPopup(geti18Value("i18n.Bene.Success"), geti18Value("i18n.common.registerMessage"), popupCommonAlertDimiss, "");
                isDevRegDone = "T";
                kony.print("Json : " + JSON.stringify(res));
                var res1 = devlistSelectedObj;
                var deviceName = res1.devInfo;
                if (deviceName === kony.i18n.getLocalizedString("i18n.common.ThisDevice")) {
                    if (res.soft_token !== null && res.soft_token !== "") {
                        kony.print("response.soft_token--> " + res.soft_token);
                        try {
                            var reSoftToken = EVfn(res.soft_token);
                            var storeData = isEmpty(reSoftToken) ? "" : reSoftToken;
                            kony.store.setItem("soft_token", storeData);
                            kony.print("soft token : " + res.soft_token);
                        } catch (err) {
                            kony.print(err);
                            exceptionLogCall("EVfn", "UI ERROR res.soft_token", "UI", err);
                        }
                        siriObject.softToken = res.soft_token;
                        siriObject.deviceID = deviceid;
                    } else {
                        //remove the token
                        siriObject.softToken = "";
                        siriObject.deviceID = deviceid;
                        kony.store.removeItem("soft_token");
                    }
                } else {
                    //do not save SoftT.
                    if (res.soft_token !== null && res.soft_token !== "") {
                        kony.print("response.soft_token--> " + res.soft_token);
                        try {
                            var reSoftToken1 = EVfn(res.soft_token);
                            var storeData1 = isEmpty(reSoftToken1) ? "" : reSoftToken1;
                            kony.store.setItem("soft_token", storeData1);
                            kony.print("soft token : " + res.soft_token);
                        } catch (err) {
                            kony.print(err);
                            exceptionLogCall("EVfn", "UI ERROR res.soft_token else", "UI", err);
                        }
                        siriObject.softToken = res.soft_token;
                        siriObject.deviceID = deviceid;
                    }
                }
                kony.store.setItem("isDeviceRegisteredMandatory", false);
                enableRegisteredDevice();
            } else {
                if (!isEmpty(res.statusCode) && res.statusCode === "S0043") {
                    customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.deviceReg.S0043"), popupCommonAlertDimiss, "");
                } else if (!isEmpty(res.statusCode) && res.statusCode === "S0046") {
                    //on this for BMA-412 
                    customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.deviceReg.S0046"), deRegAndRegOnCurrentUserList, popupCommonAlertDimiss, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));
                }
            }
            getDeviceList();
            kony.sdk.mvvm.log.info("success saving record ", res);
        }

        function error(err) {
            //Handle error case
            kony.boj.dismissPopup();
            frmDeviceRegistration.lblLanguage.text = "";
            frmDeviceRegistration.lblDeviceInfo.text = "";
            frmDeviceRegistration.lblCustId.text = "";
            frmDeviceRegistration.lblDeviceId.text = "";
            var Message = kony.i18n.getLocalizedString("18n.common.devRegFail");
            customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
            kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function deRegAndRegOnCurrentUserList() {
            popupCommonAlert.dismiss();
            if (kony.sdk.isNetworkAvailable()) {
                ShowLoadingScreen();
                if (kony.boj.imeiPermissionFlag) imei = kony.os.deviceInfo().uid;
                else imei = "";
                var deviceName = kony.os.deviceInfo().model;
                var Language = kony.store.getItem("langPrefObj");
                Language = Language.toUpperCase();
                var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                var options = {
                    "access": "online",
                    "objectName": "RBObjects"
                };
                var headers = {};
                var serviceName = "RBObjects";
                var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
                var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
                dataObject.addField("Language", Language);
                dataObject.addField("user_id", user_id);
                dataObject.addField("custId", custid);
                dataObject.addField("deviceId", deviceid);
                dataObject.addField("devInfo", deviceName);
                dataObject.addField("Imei", imei);
                dataObject.addField("ksid", isEmpty(KSID) ? "" : KSID);
                dataObject.addField("appid", konyRef.mainRef.appId);
                var serviceOptions = {
                    "dataObject": dataObject,
                    "headers": headers
                };
                kony.print("Input params verify otp-->" + JSON.stringify(serviceOptions));
                modelObj.customVerb("deAndReReg", serviceOptions, success, error);
            } else {
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
            }
        }
    },
    /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmDeviceRegistrationControllerExtension#
     */
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            kony.boj.dismissPopup();
            disableRegisteredDevice();
            getDeviceList();
            unsubscribeCall();
            customAlertPopup(geti18Value("i18n.Bene.Success"), geti18Value("i18n.common.deregistermsg"), popupCommonAlertDimiss, "");
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.boj.dismissPopup();
            frmDeviceRegistration.lblLanguage.text = "";
            frmDeviceRegistration.lblDeviceInfo.text = "";
            frmDeviceRegistration.lblCustId.text = "";
            frmDeviceRegistration.lblDeviceId.text = "";
            var Message = "Device registration/de-registration failed. Please try again later";
            customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method shows form.
     * @memberof frmDeviceRegistrationControllerExtension#
     */
    showForm: function() {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.showView();
        } catch (e) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    }
});