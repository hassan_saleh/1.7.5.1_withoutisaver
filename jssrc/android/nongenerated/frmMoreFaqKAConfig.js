var frmMoreFaqKAConfig = {
    "formid": "frmMoreFaqKA",
    "frmMoreFaqKA": {
        "entity": "Informationcontent",
        "objectServiceName": "RBObjects",
        "objectServiceOptions": {
            "access": "online"
        },
    },
    "txtFaqKA": {
        "fieldprops": {
            "entity": "Informationcontent",
            "widgettype": "TextArea",
            "field": "infoContent"
        }
    }
};