function geti18Value(key) {
    return kony.i18n.getLocalizedString(key);
}
kony.boj.accounttype = function(data, flag) {
    frmAccountDetailsScreen.lblHead.text = "Accounts"; {
        for (var i = 0; i < data.length; i++) {
            var accountNumber = data[i].accountID;
            //data[i].hiddenAccountNumber =  data[i].accountName +"****"+ accountNumber.substring(accountNumber.length-3, accountNumber.length);
            data[i].hiddenAccountNumber = "***" + accountNumber.substring(accountNumber.length - 5, accountNumber.length);
            data[i].balance = data[i].availableBalance + " " + data[i].currencyCode;
        }
        frmAccountDetailsScreen.segFre.widgetDataMap = {
            lblAccountName: "accountName",
            lblAccountNumber: "hiddenAccountNumber",
            lblAmount: "balance"
        };
        frmAccountDetailsScreen.segFre.setData(data);
        frmAccountDetailsScreen.show();
    }
};

function searchJomopayBene(srcTxt, data) {
    try {
        var temp = [];
        for (var i in data) {
            if (data[i].nickName.indexOf(srcTxt) > -1) {
                temp.push(data[i]);
            }
        }
        isSearchUpdate = true;
        updateJomopayBeneList(temp);
    } catch (e) {
        kony.print(e);
    }
}
kony.boj.JomoPaytransferType = function(inputObj, flag) {
    //alert(flag);
    var dataTransferType = [];
    var len = inputObj.length;
    var contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
    //   if(flag != 1 && flag !== "ID" && flag !== "regJP"){
    //   	contentAlignment = constants.CONTENT_ALIGN_CENTER;
    //   }
    for (i = 0; i < len; i++) {
        rowData = {
            "lblJP": inputObj[i]
        };
        dataTransferType.push(rowData);
    }
    //{"lblJPTransfer":"Person to Person"},{"lblJPTransfer":"Government Organisation"},{"lblJPTransfer":"Charity Fund"}];
    frmJomoPayAccountList.segPopup.widgetDataMap = {
        lblJPTransfer: "lblJP"
    };
    frmJomoPayAccountList.segPopup.setData(dataTransferType);
    flagtype = flag;
    if (flagtype == 1) {
        frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.jomopay.transfertype");
    } else if (flagtype == "ID") {
        frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.jomopay.idtype");
    } else if (flagtype == "regJP") {
        frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.jomopay.jomopaytype");
    } else if (flagtype == "addBen") /* hassan */ {
        frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.jomopay.jomopaytype");
    } else if (kony.newCARD.applyCardsOption === true) {
        if (flagtype === "RelationShip") {
            frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.common.relationship");
        } else if (flagtype === "Reason") {
            frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.newCard.reason");
        } else if (flagtype === "SuplementryCardType") {
            if (kony.newCARD.cardTYPE === "UpgradeCreditCard") {
                frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.applycards.cardtype");
            } else {
                frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.newCard.supplementrycardtype");
            }
        } else if (flagtype === "LimitType") {
            frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.newCard.limittype");
        } else if (flagtype === "EmploymentStatus") {
            frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.newCard.employmentstatus");
        } else if (flagtype === "Color") {
            frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.common.color");
        } else if (flagtype === "UpgradeCard") {
            frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.cards.upgradetype");
        }
        //  	frmJomoPayAccountList.lblJoMoPay.text= "";
    } else {
        frmJomoPayAccountList.lblJoMoPay.text = geti18Value("i18n.jomopay.jomopaytype");
    }
    frmJomoPayAccountList.show();
};
kony.boj.Idtype = function() {
    //var inputtype=["Mobile","Alias","Corporate"];
    var inputtype = [geti18Value("i18n.common.ID")];
    var flag = "ID";
    kony.boj.JomoPaytransferType(inputtype, flag);
};
// kony.boj.jomopaytype=function()
// {
//   var inputtype=["Mobile Number"];
//   var flag="type";
//   kony.boj.JomoPaytransferType(inputtype,flag);
// };
kony.boj.jomopaytype = function(flagI) {
    //var inputtype=["Mobile","Alias","Corporate"];
    //   var inputtype=[geti18Value("i18n.jomopay.mobiletype"),geti18Value("i18n.jomopay.aliastype"),geti18Value("i18n.jomopay.corporatetype")];
    var inputtype = [geti18Value("i18n.jomopay.mobiletype"), geti18Value("i18n.jomopay.aliastype")];
    var flag = flagI;
    kony.boj.JomoPaytransferType(inputtype, flag);
};
// var jpdata=frmJoMoPayLanding.segJomopayDetails.data;
// kony.print("frmJoMoPayLanding.segJomopayDetails.data::"+JSON.stringify(jpdata));
var jppersonname = ["Virat Kohli", "Rohit Sharma", "Ajinkya Rahane", "Rahul Dravid", "Bhuvneshwar Kumar", "Mohammad Shami"];
var transactiondate = ["Today, 4:00 PM", "26 Jan 2018", "1 Jan 2018", "16 Dec 2017", "4 Dec 2017", "1 Dec 2017"];
var amount = ["140.341 JOD", "140.341 JOD", "140.341 JOD", "140.341 JOD", "140.341 JOD", "240.341 JOD"];

function addData() {
    var accounts = [];
    for (var i = 0; i < jppersonname.length; i++) {
        var Name = jppersonname[i];
        splittedName = Name.split(" ");
        shortName = splittedName[0][0] + splittedName[1][0];
        shortName = shortName.toUpperCase();
        color = ((1 << 24) * Math.random() | 0).toString(16);
        profileButton = {
            text: shortName,
            backgroundColor: color
        };
        accounts.push({
            "PName": jppersonname[i],
            "Date": transactiondate[i],
            "Amount": amount[i],
            "btn": profileButton
        });
    }
    frmJoMoPayLanding.lblNoHistory.setVisibility(false);
    kony.print("Accounts(addData):-------> " + JSON.stringify(accounts));
    frmJoMoPayLanding.segJomopayDetails.widgetDataMap = {
        lblPayeeName: "PName",
        lblTransactiondate: "Date",
        lblAmount: "Amount",
        btnPic: "btn"
    };
    frmJoMoPayLanding.segJomopayDetails.setData(accounts);
}
//var jpdata=frmJoMoPayLanding.segJomopayDetails.data;
function searchString(strin, data) {
    if (strin === "" || strin === null) {
        if (!isEmpty(data)) {
            frmJoMoPayLanding.segJomopayDetails.widgetDataMap = {
                lblPayeeName: "jomopaytypeval",
                lblTransactiondate: "paymentdate",
                lblAmount: "paymentamt"
            };
            frmJoMoPayLanding.lblNoHistory.setVisibility(false);
            kony.print("Empty String::" + JSON.stringify(data));
            frmJoMoPayLanding.segJomopayDetails.removeAll();
            frmJoMoPayLanding.segJomopayDetails.setData(data);
            frmJoMoPayLanding.forceLayout();
        } else {
            frmJoMoPayLanding.lblNoHistory.setVisibility(true);
        }
    } else if (!isEmpty(data)) {
        var accounts = [];
        var sear = strin;
        for (var i = 0; i < data.length; i++) {
            var t = data[i].jomopaytypeval.search(sear);
            if (t == -1) {} else {
                accounts.push({
                    "PName": data[i].jomopaytypeval,
                    "Date": data[i].paymentdate,
                    "Amount": data[i].paymentamt,
                });
            }
        }
        if (accounts.length === 0) {
            frmJoMoPayLanding.lblNoHistory.setVisibility(true);
            frmJoMoPayLanding.lblNoHistory.text = geti18Value("i18n.accounts.noTransaction");
            frmJoMoPayLanding.segJomopayDetails.removeAll();
        } else {
            frmJoMoPayLanding.lblNoHistory.setVisibility(false);
            kony.print("Accounts:-------> " + JSON.stringify(accounts));
            frmJoMoPayLanding.segJomopayDetails.widgetDataMap = {
                lblPayeeName: "PName",
                lblTransactiondate: "Date",
                lblAmount: "Amount"
            };
            frmJoMoPayLanding.segJomopayDetails.removeAll();
            frmJoMoPayLanding.segJomopayDetails.setData(accounts);
            frmJoMoPayLanding.segJomopayDetails.setVisibility(true);
        }
    } else {
        kony.print("No Dataa1");
    }
}
var testArray = [];
var data = [];

function getDataOfContats() {
    testArray = [];
    kony.application.showLoadingScreen(null, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    testArray = kony.contact.find("*", true);
    kony.print("Direct Data testArray : - " + JSON.stringify(testArray));
    kony.application.dismissLoadingScreen();
    if (testArray !== undefined && testArray !== null && testArray !== "") {
        setthis();
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.noContacts"), popupCommonAlertDimiss, "");
    }
}

function setthis() {
    try {
        kony.application.dismissLoadingScreen();
        var i = 0;
        var len = testArray.length;
        //     alert(len);
        //     alert(testArray);
        var processedRowObj;
        data = [];
        var isValid = false;
        if (len !== 0) {
            while (i < len) {
                data.push(form_JOMOPAY_CONTACT_LIST_DATA(testArray[i]));
                i++;
            }
            data = sort_ALPHABETICALLY(data, "lblcname");
            frmJomopayContacts.segJomopayList.widgetDataMap = {
                lblName: "lblcname",
                lblPhoneNum: "lblpno",
                btnPic: "btnProfilePic"
            };
            frmJomopayContacts.segJomopayList.removeAll();
            frmJomopayContacts.segJomopayList.setData(data);
            kony.application.dismissLoadingScreen();
            frmJomopayContacts.txtSearch.text = "";
            frmJomopayContacts.show();
        } else {
            customAlertPopup(geti18nkey("i18n.common.jmpnocontacts"), geti18nkey("i18n.common.nocontactsteext"), popupCommonAlertDimiss, "");
        }
    } catch (err) {
        kony.application.dismissLoadingScreen();
        kony.print("JOMOPAY CONTACTS::  setthis" + JSON.stringify(err));
        exceptionLogCall("JOMOPAY CONTACTS::  setthis", "UI ERROR", "UI", err);
    }
}

function aliasselected() {
    frmJoMoPay.lblBeneficiaryStaticText.text = geti18Value("i18n.jomopay.benificiaryalias");
    //   frmJoMoPay.lblBeneficiaryStaticText.width = "41%";
    frmJoMoPay.btnContactBook.setVisibility(false);
    frmJoMoPay.txtPhoneNo.setVisibility(false);
    frmJoMoPay.lblMobileIDHint.setVisibility(false);
    frmJoMoPay.lblMobileIDHint.setVisibility(false);
    frmJoMoPay.txtAliasType.setVisibility(true);
    frmJoMoPay.txtAliasType.text = "";
    frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
    frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
    frmJoMoPay.txtPhoneNo.text = "123";
    frmJoMoPayConfirmation.lblJomopayType.text = "A";
}

function mobileselected() {
    frmJoMoPay.lblBeneficiaryStaticText.text = geti18Value("i18n.jomopay.mobile");
    //frmJoMoPay.lblBeneficiaryStaticText.width = "37%";
    frmJoMoPay.btnContactBook.setVisibility(true);
    //frmJoMoPay.txtAliasDetail.textInputMode=constant.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD;
    frmJoMoPay.txtAliasType.setVisibility(false);
    frmJoMoPay.txtPhoneNo.setVisibility(true);
    frmJoMoPay.lblMobileIDHint.setVisibility(true);
    frmJoMoPay.txtAliasType.text = "";
    frmJoMoPay.txtPhoneNo.text = "";
    frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
    frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
    frmJoMoPayConfirmation.lblTransactionType.text = 10;
    frmJoMoPayConfirmation.lblJomopayType.text = "M";
}

function corporateselected() {
    frmJoMoPay.lblBeneficiaryStaticText.text = geti18Value("i18n.jomopay.id");
    frmJoMoPay.btnContactBook.setVisibility(false);
    frmJoMoPay.txtPhoneNo.setVisibility(false);
    frmJoMoPay.lblMobileIDHint.setVisibility(false);
    frmJoMoPay.txtAliasDetail.setVisibility(true);
    frmJoMoPay.txtAliasDetail.text = "";
    frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
    frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
    frmJoMoPay.txtPhoneNo.text = "123";
    frmJoMoPayConfirmation.lblJomopayType.text = "C";
}
kony.boj.getJomopayFee = function() {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
        "access": "online",
        "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Transactions", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Transactions");
    dataObject.addField("transactionType", "10");
    dataObject.addField("fromAccountNumber", frmJoMoPay.lblAccountCode.text);
    dataObject.addField("amount", frmJoMoPay.txtFieldAmount.text);
    dataObject.addField("jomopayType", frmJoMoPayConfirmation.lblJomopayType.text);
    dataObject.addField("payeeName", frmJoMoPay.txtPhoneNo.text);
    dataObject.addField("TransferFlag", "J");
    var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
    };
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    modelObj.customVerb("getFeeData", serviceOptions, getFeesSuccess, getFeesFailure);

    function getFeesSuccess(res) {
        kony.application.dismissLoadingScreen();
        //alert("Successfully recieceved the fees ::" + JSON.stringify(res));
        kony.print("Successfully recieceved the fees ::" + JSON.stringify(res));
        if (res.ref_code !== null && res.ref_code !== "") {
            var fee = res.fees;
            var amount = Number.parseFloat(fee).toFixed(3);
            frmJoMoPayConfirmation.lblFee.text = amount;
            frmJoMoPayConfirmation.show();
        } else {
            //       alert("Something Went Wrong Please Try again Later");
        }
        //    alert("Successfully recieceved the fees ::" + JSON.stringify(res));
    }

    function getFeesFailure(err) {
        kony.application.dismissLoadingScreen();
        //alert("Something Went Wrong Please Try again Later");
        //alert("Successfully recieceved the fees ::" + JSON.stringify(err));
    }
};

function navigate_StandingInstructions(flow) {
    try {
        if (flow == "Standing Instruction") {
            frmJoMoPayLanding.btnAdd.isVisible = false;
            frmJoMoPayLanding.flxPaymentHistory.isVisible = false;
            frmJoMoPayLanding.flxJomoPayList.isVisible = true;
            frmJoMoPayLanding.flxJoMoPayHeader.isVisible = true;
            frmJoMoPayLanding.lblJoMoPay.text = "Standing Instruction";
            frmJoMoPayLanding.segJomopayDetails.removeAll();
            frmJoMoPayLanding.segJomopayDetails.onRowClick = function(context) {
                kony.print("context onRowClick ::" + JSON.stringify(context));
                frmTransactionDetails.show();
            };
            frmJoMoPayLanding.segJomopayDetails.widgetDataMap = {
                "transactionDate": "transactionDate",
                "transactionName": "transactionName",
                "transactionAmountSI": "transactionAmountSI",
                "chevron": "chevron"
            };
            frmJoMoPayLanding.segJomopayDetails.setData([{
                "transactionDate": {
                    "isVisible": true,
                    text: "07/03/2018"
                },
                "transactionName": {
                    "isVisible": true,
                    text: "Jordan Water"
                },
                "transactionAmountSI": {
                    "isVisible": true,
                    text: "130.201 JOD"
                },
                template: CopyFlexContainer011baa9bcad7141
            }, {
                "transactionDate": {
                    "isVisible": true,
                    text: "07/03/2018"
                },
                "transactionName": {
                    "isVisible": true,
                    text: "Jordan gas"
                },
                "transactionAmountSI": {
                    "isVisible": true,
                    text: "52.871 JOD"
                },
                template: CopyFlexContainer011baa9bcad7141
            }, {
                "transactionDate": {
                    "isVisible": true,
                    text: "07/03/2018"
                },
                "transactionName": {
                    "isVisible": true,
                    text: "sayegh group"
                },
                "transactionAmountSI": {
                    "isVisible": true,
                    text: "3,600.000 JOD"
                },
                template: CopyFlexContainer011baa9bcad7141
            }, {
                "transactionDate": {
                    "isVisible": true,
                    text: "07/03/2018"
                },
                "transactionName": {
                    "isVisible": true,
                    text: "Bath & Body Works"
                },
                "transactionAmountSI": {
                    "isVisible": true,
                    text: "48.417 JOD"
                },
                template: CopyFlexContainer011baa9bcad7141
            }, {
                "transactionDate": {
                    "isVisible": true,
                    text: "07/03/2018"
                },
                "transactionName": {
                    "isVisible": true,
                    text: "Ryan Portman"
                },
                "transactionAmountSI": {
                    "isVisible": true,
                    text: "278.882 JOD"
                },
                template: CopyFlexContainer011baa9bcad7141
            }]);
            frmJoMoPayLanding.show();
        } else if (flow == "JoMoPay") {
            frmJoMoPayLanding.segJomopayDetails.removeAll();
            frmJoMoPayLanding.btnAdd.isVisible = true;
            frmJoMoPayLanding.flxPaymentHistory.isVisible = false;
            frmJoMoPayLanding.flxJomoPayList.isVisible = true;
            frmJoMoPayLanding.flxJoMoPayHeader.isVisible = true;
            frmJoMoPayLanding.lblJoMoPay.text = geti18Value("i18n.jomopay.jomopay");;
            frmJoMoPayLanding.show();
        } else if (flow == "Payment History") {
            frmJoMoPayLanding.lblBoj.text = geti18Value("i18n.PaymentDash.Paymenthistory");
            frmJoMoPayLanding.flxPaymentHistory.isVisible = true;
            frmJoMoPayLanding.flxJomopayBeneficiary.setVisibility(false);
            frmJoMoPayLanding.flxAddBene.setVisibility(false);
            kony.print("hassan 2");
            frmJoMoPayLanding.flxScanMerchantQR.setVisibility(false);
            frmJoMoPayLanding.flxJomoPayList.isVisible = false;
            frmJoMoPayLanding.flxJoMoPayHeader.isVisible = false;
            frmJoMoPayLanding.segJomopayBeneficiary.isVisible = false;
            frmJoMoPayLanding.flxJomopayBeneficiary.isVisible = false;
            frmJoMoPayLanding.flxScanMerchantQR.bottom = "10%";
            frmJoMoPayLanding.flxCheckRegJomopay.isVisible = false;
            getDatatoPaymentHistory();
        } else if (flow == "Card Statement") {
            frmJoMoPayLanding.lblBoj.text = "Card Statement";
            frmJoMoPayLanding.flxPaymentHistory.isVisible = true;
            frmJoMoPayLanding.flxJomoPayList.isVisible = false;
            frmJoMoPayLanding.flxJoMoPayHeader.isVisible = false;
            serv_cardStatement();
            frmJoMoPayLanding.show();
        }
    } catch (e) {
        kony.print("Exception_navigate_StandingInstructions ::" + e);
        exceptionLogCall("navigate_StandingInstructions", "UI ERROR", "UI", e);
    }
}

function initjomopay() {
    var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
    frmJoMoPay.lblAccountType.text = fromAccounts[0].accountName;
    var accountNumber = fromAccounts[0].accountID;
    fromAccounts[0].hiddenAccountNumber = accountNumber;
    frmJoMoPay.lblAccountCode.text = fromAccounts[0].hiddenAccountNumber;
    if (fromAccounts[0].AccNickName !== "") {
        frmJoMoPay.lblAccountType.text = fromAccounts[0].AccNickName;
        frmJoMoPay.lblAccountCode.isVisible = false;
    } else {
        frmJoMoPay.lblAccountType.text = (fromAccounts[0].accountName.length > 22) ? (fromAccounts[0].accountName.substring(0, 22) + "...") : fromAccounts[0].accountName;
        frmJoMoPay.lblAccountCode.isVisible = true;
    }
}

function preshowjomopay() {
    //   frmJoMoPay.CopylblBack0i5985fbe3b634e.setFocus();
    var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
    inputObj1 = [geti18Value("i18n.jomopay.type")];
    inputObj3 = [geti18Value("i18n.jomopay.mobiletype"), geti18Value("i18n.jomopay.aliastype"), geti18Value("i18n.jomopay.corporatetype")];
    var len1 = inputObj1.length;
    var len2 = fromAccounts.length;
    var len3 = inputObj3.length;
    var branchNum = fromAccounts[0].branchNumber;
    if (branchNum.length === 1) {
        branchNum = "00" + branchNum;
    } else if (branchNum.length === 2) {
        branchNum = "0" + branchNum;
    }
    var appendedData = branchNum + "" + fromAccounts[0].accountID;
    frmJoMoPayConfirmation.lblFromAccountNumber.text = appendedData;
    //   frmJoMoPayConfirmation.lblFromAccountNumber.text="0010013010000050005";
    frmJoMoPayConfirmation.lblSourceBranchCode.text = fromAccounts[0].branchNumber;
    frmJoMoPayConfirmation.lblStaticJOD.text = fromAccounts[0].currencyCode;
    frmJoMoPayConfirmation.lblfromAccountCurrency.text = fromAccounts[0].currencyCode;
    frmJoMoPayConfirmation.lblJomopayType.text = "M";
    if (len1 < 2) {
        frmJoMoPay.lblTransferType.text = inputObj1[0];
        frmJoMoPayConfirmation.lblTransactionType.text = 10;
        frmJoMoPay.btnDropDown.setVisibility(false);
    }
    //if (len2 < 2) {
    //     frmJoMoPay.lblAccountType.text=fromAccounts[0].accountName;
    /* var accountNumber = fromAccounts[0].accountID;
    fromAccounts[0].hiddenAccountNumber =  accountNumber;
    frmJoMoPay.lblAccountCode.text=fromAccounts[0].hiddenAccountNumber;
    if(fromAccounts[0].AccNickName!==""){
      frmJoMoPay.lblAccountType.text=fromAccounts[0].AccNickName;
      frmJoMoPay.lblAccountCode.isVisible = false;
    }else{
      frmJoMoPay.lblAccountType.text=(fromAccounts[0].accountName.length>30)?(fromAccounts[0].accountName.substring(0,22)+"..."):fromAccounts[0].accountName;
      frmJoMoPay.lblAccountCode.isVisible = true;
    }
    hassan 
    */
    /*	frmJoMoPay.lblCurrency.text=fromAccounts[0].currencyCode;
      if(frmJoMoPay.lblCurrency.text==="JOD")
  {
frmJoMoPay.txtFieldAmount.placeholder="00.000";
  }
else
  {
    frmJoMoPay.txtFieldAmount.placeholder="00.00";
  }*/
    // frmJoMoPay.btnDropDownAccount.setVisibility(false);
    //}
    if (len3 < 2) {
        frmJoMoPay.lblType.text = inputObj3[0];
        frmJoMoPayConfirmation.lblJomopayType.text = "M";
        frmJoMoPay.btnAliasDropDown.setVisibility(false);
    }
    //Changes of adding alias to view
    var mobile = geti18Value("i18n.jomopay.mobiletype");
    var alias = geti18Value("i18n.jomopay.aliastype");
    if (gbllblTypeJomoPay) {
        frmJoMoPay.lblType.text = frmJomoPayAccountList.segPopup.selectedItems[0].lblJP;
        if (frmJoMoPay.lblType.text === alias) {
            kony.print("In on click of aliasselected::");
            aliasselected();
        } else if (frmJoMoPay.lblType.text === mobile) {
            kony.print("In on click of mobileselected::");
            mobileselected();
        }
        gbllblTypeJomoPay = false;
    }
}

function nextjomopay() {
    if (frmJoMoPay.btnNext.skin == "jomopaynextEnabled") {
        if (kony.sdk.isNetworkAvailable()) {
            kony.application.showLoadingScreen(null, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
            frmJoMoPayConfirmation.lblTransferType.text = frmJoMoPay.lblTransferType.text;
            frmJoMoPayConfirmation.lblAccountType.text = frmJoMoPay.lblAccountType.text + " " + frmJoMoPay.lblAccountCode.text;
            frmJoMoPayConfirmation.lblType.text = frmJoMoPay.lblType.text;
            frmJoMoPayConfirmation.lblDescription.text = frmJoMoPay.txtDescription.text;
            // var amount = Number.parseFloat(frmJoMoPay.txtFieldAmount.text).toFixed(3);
            frmJoMoPayConfirmation.lblAmount.text = frmJoMoPay.txtFieldAmount.text;
            frmJoMoPayConfirmation.lblStaticJOD.text = frmJoMoPay.lblCurrency.text;
            frmJoMoPayConfirmation.lblfromAccountCurrency.text = frmJoMoPay.lblCurrency.text;
            if (frmJoMoPay.lblType.text === geti18Value("i18n.jomopay.mobiletype")) {
                frmJoMoPayConfirmation.btnProfilePhoto.skin = "sknbtnProfileMobile";
                frmJoMoPayConfirmation.btnProfilePhoto.focusSkin = "sknbtnProfileMobile";
                frmJoMoPayConfirmation.btnProfilePhoto.text = "F";
                frmJoMoPayConfirmation.lblPhoneNo.text = frmJoMoPay.txtPhoneNo.text;
            } else {
                var Name = frmJoMoPay.txtAliasType.text;
                var splittedName = Name.split(" ");
                var shortName = "";
                if (splittedName.length == 2) {
                    shortName = splittedName[0][0] + splittedName[1][0];
                } else {
                    if (splittedName[0].length > 1) shortName = splittedName[0][0] + splittedName[0][1];
                    else shortName = splittedName[0][0];
                }
                shortName = shortName.toUpperCase();
                frmJoMoPayConfirmation.lblPhoneNo.text = frmJoMoPay.txtAliasType.text;
                frmJoMoPayConfirmation.btnProfilePhoto.skin = "sknbtnProfileAlias";
                frmJoMoPayConfirmation.btnProfilePhoto.focusSkin = "sknbtnProfileAlias";
                frmJoMoPayConfirmation.btnProfilePhoto.text = shortName;
            }
            kony.boj.getJomopayFee();
        } else {
            alert("" + kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"));
        }
    } else {
        kony.print("All details are not filled");
    }
}

function ontextChangeMobile() {
    if (frmJoMoPay.txtPhoneNo.isVisible === true) {
        if (!/[0-9]/.test(frmJoMoPay.txtPhoneNo.text)) frmJoMoPay.txtPhoneNo.text = frmJoMoPay.txtPhoneNo.text.substring(0, frmJoMoPay.txtPhoneNo.text.length - 1);
        if (frmJoMoPay.txtFieldAmount.text !== null && frmJoMoPay.txtFieldAmount.text !== "" && frmJoMoPay.txtFieldAmount.text !== "0" && frmJoMoPay.txtPhoneNo.text !== null) {
            kony.print("------1-------");
            frmJoMoPay.btnNext.skin = "jomopaynextEnabled";
            frmJoMoPay.btnNext.focusSkin = "jomopaynextEnabled";
        } else {
            kony.print("------2-------");
            frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
            frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
        }
    } else if (frmJoMoPay.txtAliasType.isVisible === true) {
        if (frmJoMoPay.txtFieldAmount.text !== null && frmJoMoPay.txtFieldAmount.text !== "" && frmJoMoPay.txtFieldAmount.text !== "0" && frmJoMoPay.txtAliasType.text !== null && frmJoMoPay.txtAliasType.text !== "") {
            kony.print("------3-------");
            frmJoMoPay.btnNext.skin = "jomopaynextEnabled";
            frmJoMoPay.btnNext.focusSkin = "jomopaynextEnabled";
        } else {
            kony.print("------4-------");
            frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
            frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
        }
    } else {
        frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
        frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
    }
}

function ontextchangeamount() {
    if (frmJoMoPay.txtFieldAmount.text !== null && frmJoMoPay.txtFieldAmount.text !== "") {
        var amount = frmJoMoPay.txtFieldAmount.text;
        if (amount.indexOf(".") > -1) {
            amount = amount.substring(0, amount.indexOf(".") + 4);
        }
        if (parseFloat(amount) > 0) {
            frmJoMoPay.btnNext.skin = "jomopaynextEnabled";
            frmJoMoPay.btnNext.focusSkin = "jomopaynextEnabled";
        } else {
            frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
            frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
            return;
        }
        frmJoMoPay.txtFieldAmount.text = amount;
    } else {
        frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
        frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
        return;
    }
    if (frmJoMoPay.txtPhoneNo.isVisible === true) {
        if (frmJoMoPay.txtPhoneNo.text !== null) {
            frmJoMoPay.btnNext.skin = "jomopaynextEnabled";
            frmJoMoPay.btnNext.focusSkin = "jomopaynextEnabled";
        } else {
            frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
            frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
            return;
        }
    } else if (frmJoMoPay.txtAliasType.isVisible === true) {
        if (frmJoMoPay.txtAliasType.text !== null && frmJoMoPay.txtAliasType.text !== "") {
            frmJoMoPay.btnNext.skin = "jomopaynextEnabled";
            frmJoMoPay.btnNext.focusSkin = "jomopaynextEnabled";
        } else {
            frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
            frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
            return;
        }
    } else {
        frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
        frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
    }
}

function onendediting() {
    frmJoMoPay.flxBorderAmount.skin = "skntextFieldDividerGreen";
    /*if(frmJoMoPay.lblCurrency.text==="JOD")
{
if (frmJoMoPay.txtFieldAmount.text !== "" && frmJoMoPay.txtFieldAmount.text !== "0" && frmJoMoPay.txtFieldAmount.text !== 0) 
	{
        var amount = Number.parseFloat(frmJoMoPay.txtFieldAmount.text).toFixed(3);
        frmJoMoPay.txtFieldAmount.text = amount;
  	}
}
else
  {*/
    if (frmJoMoPay.txtFieldAmount.text !== null && frmJoMoPay.txtFieldAmount.text !== "" && frmJoMoPay.txtFieldAmount.text !== "." && frmJoMoPay.txtFieldAmount.text !== "0" && frmJoMoPay.txtFieldAmount.text !== 0) {
        var amount = Number.parseFloat(frmJoMoPay.txtFieldAmount.text).toFixed(3);
        frmJoMoPay.txtFieldAmount.text = amount;
    }
    if (frmJoMoPay.txtFieldAmount.text === ".") {
        frmJoMoPay.txtFieldAmount.text = 0;
    }
    // }
}

function backfromaccount() {
    if (flagtype === "type") {
        frmJoMoPayRegistration.show();
    } else if (flagtype === "ID") {
        frmJoMoPayRegistration.show();
    } else {
        frmJoMoPay.show();
    }
}

function onclickofaccountseg() {
    kony.print("In on click of Segment::");
    var mobile = geti18Value("i18n.jomopay.mobiletype");
    var alias = geti18Value("i18n.jomopay.aliastype");
    kony.print("flagtype:::" + flagtype);
    kony.print(frmJomoPayAccountList.segPopup.selectedItems);
    if (kony.newCARD.applyCardsOption === true) {
        selected_OPTIONS_APPLYNEWCARDS(frmJomoPayAccountList.segPopup.selectedItems, flagtype);
    } else if (flagtype === "type") {
        frmJoMoPayRegistration.lblType.text = frmJomoPayAccountList.segPopup.selectedItems[0].lblJP;
        frmJoMoPayRegistration.show();
    } else if (flagtype === "ID") {
        frmJoMoPayRegistration.lblIDType.text = frmJomoPayAccountList.segPopup.selectedItems[0].lblJP;
        frmJoMoPayRegistration.show();
    } else if (flagtype === "jomopaydummy") {
        frmJoMoPay.lblTransferType.text = frmJomoPayAccountList.segPopup.selectedItems[0].lblJP;
        frmJoMoPay.show();
    } else if (flagtype === "jmp") {
        frmJoMoPay.lblType.text = frmJomoPayAccountList.segPopup.selectedItems[0].lblJP;
        gbllblTypeJomoPay = true;
        if (frmJoMoPay.lblType.text === alias) {
            kony.print("In on click of aliasselected::");
            aliasselected();
        } else if (frmJoMoPay.lblType.text === mobile) {
            kony.print("In on click of mobileselected::");
            mobileselected();
        } else {
            kony.print("In on click of corporateselected::");
            corporateselected();
        }
        frmJoMoPay.show();
    } else if (flagtype === "regJP") {
        var jomoPayType = frmJomoPayAccountList.segPopup.selectedItems[0].lblJP;
        frmJoMoPayRegistration.lblType.text = jomoPayType;
        if (jomoPayType === geti18Value("i18n.jomopay.corporatetype")) {
            frmJoMoPayRegistration.lblPayType.text = "C";
        }
        if (jomoPayType === alias) {
            frmJoMoPayRegistration.lblPayType.text = "A";
        } else {
            frmJoMoPayRegistration.lblPayType.text = "M";
        }
        frmJoMoPayRegistration.show();
    } else if (flagtype === "addBen") /* hassan jomopay type */ {
        var jomoPayType = frmJomoPayAccountList.segPopup.selectedItems[0].lblJP;
        frmJOMOPayAddBiller.lblType.text = jomoPayType;
        if (jomoPayType === geti18Value("i18n.jomopay.mobiletype")) {
            frmJOMOPayAddBiller.flxBeneficiaryMobile.isVisible = true;
            frmJOMOPayAddBiller.lblMobileIDHint.setVisibility(true);
            frmJOMOPayAddBiller.flxBeneficiaryAlias.isVisible = false;
            if (jomopayFlag === "addBene") /*hassa edit jomopay bene*/ frmJOMOPayAddBiller.txtBeneficiaryMobile.text = "";
            frmJOMOPayAddBiller.flxBorderBenificiary.skin = "skntextFieldDividerJomoPay";
            frmJOMOPayAddBiller.flxBorderAlias.skin = "skntextFieldDividerJomoPay"; // hassan
        } else if (jomoPayType === geti18Value("i18n.jomopay.aliastype")) {
            frmJOMOPayAddBiller.flxBeneficiaryAlias.isVisible = true;
            frmJOMOPayAddBiller.flxBeneficiaryMobile.isVisible = false;
            frmJOMOPayAddBiller.lblMobileIDHint.setVisibility(false);
            if (jomopayFlag === "addBene") /*hassa edit jomopay bene*/ frmJOMOPayAddBiller.txtBeneficiaryAlias.text = "";
            frmJOMOPayAddBiller.flxBorderBenificiary.skin = "skntextFieldDividerJomoPay";
            frmJOMOPayAddBiller.flxBorderAlias.skin = "skntextFieldDividerJomoPay"; // hassan
        }
        frmJOMOPayAddBiller.show();
    }
}

function contactselection() {
    if (kony.application.getPreviousForm().id === "frmJoMoPay") {
        frmJoMoPay.txtPhoneNo.text = frmJomopayContacts.segJomopayList.selectedItems[0].lblpno.replace(" ", "");
        if (frmJoMoPay.txtAliasDetail !== null && frmJoMoPay.txtAliasDetail !== "" && frmJoMoPay.txtFieldAmount.text !== null && frmJoMoPay.txtFieldAmount.text !== "" && frmJoMoPay.txtFieldAmount.text !== "0" && frmJoMoPay.txtPhoneNo.text !== null && frmJoMoPay.txtPhoneNo.text !== "") {
            frmJoMoPay.btnNext.skin = "jomopaynextEnabled";
        } else {
            frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
        }
        frmJoMoPay.show();
    } else {
        frmJOMOPayAddBiller.txtBeneficiaryMobile.text = frmJomopayContacts.segJomopayList.selectedItems[0].lblpno.replace(/\s/g, "");
        frmJOMOPayAddBiller.flxBorderBenificiary.skin = "skntextFieldDividerGreen";
        frmJOMOPayAddBiller.show();
    }
    animateLabel("UP", "lblBeneficiaryStaticText", frmJoMoPay.txtPhoneNo.text);
}

function confirmjomopay() {
    if (kony.sdk.isNetworkAvailable()) {
        kony.print("lblTransactionType------>" + frmJoMoPayConfirmation.lblTransactionType.text);
        kony.print("lblFromAccountNumber------>" + frmJoMoPayConfirmation.lblFromAccountNumber.text);
        kony.print("lblSourceBranchCode------>" + frmJoMoPayConfirmation.lblSourceBranchCode.text);
        kony.print("lblJomopayType------>" + frmJoMoPayConfirmation.lblJomopayType.text);
        kony.print("lblTransferFlag------>" + frmJoMoPayConfirmation.lblTransferFlag.text);
        gblToOTPFrom = "jomo";
        //     frmJoMoPay.destroy();
        ShowLoadingScreen();
        if (parseFloat(frmJoMoPayConfirmation.lblAmount.text) > parseFloat(bojJMP_Limit)) {
            callRequestOTP();
        } else {
            serv_PAYMENT_JOMOPAY();
        }
    } else {
        alert("" + kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"));
    }
    //frmNewUserOnboardVerificationKA.show();
}

function confirmjomopayreg() {
    if (kony.sdk.isNetworkAvailable()) {
        gblToOTPFrom = "regjp";
        ShowLoadingScreen();
        callRequestOTP();
    } else {
        alert("" + kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"));
    }
    //frmNewUserOnboardVerificationKA.show();
}
kony = kony || {};
kony.boj = kony.boj || {};
kony.boj.selectedDetailsList = [];
kony.boj.selectedDetailsType = "";
kony.boj.showCountryorCity = function(detailType, lblName, countryCode) {
    frmJomopayRegCountry.tbxSearch.text = "";
    kony.print("showCountryorCity");
    kony.boj.detailsForBene.lblToUpdate = lblName;
    kony.boj.selectedDetailsType = detailType;
    frmJomopayRegCountry.segDetails.setData([]);
    var ctrydesc = "CTRY_S_DESC";
    if (kony.store.getItem("langPrefObj") === "en" || kony.store.getItem("langPrefObj") === "EN") ctrydesc = "CTRY_S_DESC";
    else ctrydesc = "CTRY_B_DESC";
    if (detailType === "Country") {
        frmJomopayRegCountry.lblTitle.text = kony.i18n.getLocalizedString("i18n.jomopay.selectcountry");
        frmJomopayRegCountry.segDetails.widgetDataMap = {
            lblData: ctrydesc,
            lblInitial: "INITIAL",
            //       flxIcon1: "backImg"
        };
        kony.boj.selectedDetailsList = kony.boj.detailsForBene.Country;
        for (var i in kony.boj.selectedDetailsList) {
            if (kony.boj.selectedDetailsList[i][ctrydesc] !== undefined && kony.boj.selectedDetailsList[i][ctrydesc] !== "") {
                kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i][ctrydesc].substring(0, 2).toUpperCase();
            } else {
                kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CTRY_S_DESC.substring(0, 2).toUpperCase();
            }
        }
    } else if (detailType === "City") {
        ctrydesc = "CITY_B_DESC";
        if (kony.store.getItem("langPrefObj") === "en" || kony.store.getItem("langPrefObj") === "EN") ctrydesc = "CITY_S_DESC";
        else ctrydesc = "CITY_B_DESC";
        frmJomopayRegCountry.lblTitle.text = kony.i18n.getLocalizedString("i18n.jomopay.selectcity");
        frmJomopayRegCountry.segDetails.widgetDataMap = {
            lblData: ctrydesc,
            lblInitial: "INITIAL",
            //       flxIcon1: "backImg"
        };
        kony.boj.selectedDetailsList = kony.boj.detailsForBene[detailType][countryCode];
        for (var i in kony.boj.selectedDetailsList) {
            if (kony.boj.selectedDetailsList[i][ctrydesc] !== undefined && kony.boj.selectedDetailsList[i][ctrydesc] !== "") {
                kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i][ctrydesc].substring(0, 2).toUpperCase();
            } else {
                kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CITY_B_DESC.substring(0, 2).toUpperCase();
            }
        }
    }
    frmJomopayRegCountry.segDetails.setData(kony.boj.selectedDetailsList);
    frmJomopayRegCountry.show();
};
kony.boj.updateLabelJP = function(selectedItem) {
    var ctrydesc = "CTRY_S_DESC";
    if (kony.boj.detailsForBene.lblToUpdate === "lblCountry") {
        frmJoMoPayRegistration.lblCity.text = kony.i18n.getLocalizedString("i18n.jomopay.selectcity");
        frmJoMoPayRegistration.lblCity.skin = "sknLblNextDisabled";
        frmJoMoPayRegistration.flxBorderCountry.skin = "sknFlxGreenLine";
        frmJoMoPayRegistration.lblCountryCode.text = selectedItem.CTRY_CODE;
        if (kony.store.getItem("langPrefObj") === "en" || kony.store.getItem("langPrefObj") === "EN") ctrydesc = "CTRY_S_DESC";
        else ctrydesc = "CTRY_B_DESC";
        frmJoMoPayRegistration[kony.boj.detailsForBene.lblToUpdate].text = selectedItem[ctrydesc];
        frmJoMoPayRegistration[kony.boj.detailsForBene.lblToUpdate].skin = "sknLblBack";
    } else if (kony.boj.detailsForBene.lblToUpdate === "lblCity") {
        ctrydesc = "CITY_B_DESC";
        if (kony.store.getItem("langPrefObj") === "en" || kony.store.getItem("langPrefObj") === "EN") ctrydesc = "CITY_S_DESC";
        else ctrydesc = "CITY_B_DESC";
        frmJoMoPayRegistration[kony.boj.detailsForBene.lblToUpdate].text = selectedItem[ctrydesc];
        frmJoMoPayRegistration[kony.boj.detailsForBene.lblToUpdate].skin = "sknLblBack";
        frmJoMoPayRegistration.lblCityCode.text = selectedItem.CITY_CODE;
        frmJoMoPayRegistration.flxBordercity.skin = "sknFlxGreenLine";
    }
    nextCheck();
    frmJoMoPayRegistration.show();
};
kony.boj.searchDetailsJP = function(searchText) {
    frmJomopayRegCountry.segDetails.setData([]);
    searchText = searchText.toLowerCase();
    var filteredData = [];
    var ctrydesc = "CTRY_S_DESC";
    if (kony.store.getItem("langPrefObj") === "en" || kony.store.getItem("langPrefObj") === "EN") ctrydesc = "CTRY_S_DESC";
    else ctrydesc = "CTRY_B_DESC";
    if (kony.boj.selectedDetailsType === "Country") {
        frmJomopayRegCountry.segDetails.widgetDataMap = {
            lblData: ctrydesc,
            lblInitial: "CTRY_ISO",
            //       flxIcon1: "backImg"
        };
        for (var i in kony.boj.selectedDetailsList) {
            if (kony.boj.selectedDetailsList[i][ctrydesc].toLowerCase().indexOf(searchText) !== -1 || kony.boj.selectedDetailsList[i].CTRY_ISO.toLowerCase().indexOf(searchText) !== -1) filteredData.push(kony.boj.selectedDetailsList[i]);
        }
        frmJomopayRegCountry.segDetails.setData(filteredData);
    } else if (kony.boj.selectedDetailsType === "City") {
        frmJomopayRegCountry.segDetails.widgetDataMap = {
            lblData: "CITY_B_DESC",
            lblInitial: "INITIAL",
            //       flxIcon1: "backImg"
        };
        for (var i in kony.boj.selectedDetailsList) {
            if (kony.boj.selectedDetailsList[i].CITY_B_DESC.toLowerCase().indexOf(searchText) !== -1) filteredData.push(kony.boj.selectedDetailsList[i]);
        }
        frmJomopayRegCountry.segDetails.setData(filteredData);
    }
};

function nextCheck() {
    var emailValid = false;
    if (!isEmpty(frmJoMoPayRegistration.txtEmail.text)) {
        if (kony.boj.validateEmailBoj(frmJoMoPayRegistration.txtEmail.text)) {
            frmJoMoPayRegistration.lblInvalidCredentialsKA.isVisible = false;
            frmJoMoPayRegistration.flxBorderEmail.skin = "skntextFieldDividerGreen";
            emailValid = true;
        } else {
            frmJoMoPayRegistration.flxBorderEmail.skin = "sknFlxOrangeLine";
            frmJoMoPayRegistration.lblInvalidCredentialsKA.isVisible = true;
            emailValid = false;
        }
    }
    if (frmJoMoPayRegistration.txtNationalID.text !== null && frmJoMoPayRegistration.txtPhoneNo.text !== null && frmJoMoPayRegistration.txtEmail.text !== null && frmJoMoPayRegistration.txtJomopayID.text !== null && frmJoMoPayRegistration.txtNationalID.text !== "" && frmJoMoPayRegistration.txtPhoneNo.text !== "" && frmJoMoPayRegistration.txtEmail.text !== "" && frmJoMoPayRegistration.txtAlias.text !== "" && frmJoMoPayRegistration.CopyflxBorderjomopayID0b6963ff7a40e46.skin == "skntextFieldDividerGreen" && //Nart Rawashdeh
        frmJoMoPayRegistration.lblDateOfBirth.text !== "" && emailValid && frmJoMoPayRegistration.txtJomopayID.text !== "" && frmJoMoPayRegistration.lblCountry.text !== kony.i18n.getLocalizedString("i18n.jomopay.selectcountry") && frmJoMoPayRegistration.lblCity.text !== kony.i18n.getLocalizedString("i18n.jomopay.selectcountry")) {
        frmJoMoPayRegistration.btnNext.skin = "jomopaynextEnabled";
        frmJoMoPayRegistration.btnNext.focusSkin = "jomopaynextEnabled";
    } else {
        frmJoMoPayRegistration.btnNext.skin = "jomopaynextDisabled";
        frmJoMoPayRegistration.btnNext.focusSkin = "jomopaynextDisabled";
    }
}

function onnextRegJP() {
    if (frmJoMoPayRegistration.btnNext.skin === "jomopaynextEnabled") {
        var Language = getLangBojServices();
        frmJoMoPayRegistration.flxBodyReg.setVisibility(false);
        frmJoMoPayRegistration.flxConfirmPopup.setVisibility(true);
        frmJoMoPayRegistration.lblNationalIDConfirm.text = frmJoMoPayRegistration.txtNationalID.text;
        frmJoMoPayRegistration.lblIDTypeConfirm.text = "777";
        frmJoMoPayRegistration.lblPhoneConfirm.text = frmJoMoPayRegistration.txtPhoneNo.text;
        frmJoMoPayRegistration.lblEmailConfirm.text = frmJoMoPayRegistration.txtEmail.text;
        frmJoMoPayRegistration.lblJomopayIDConfirm.text = frmJoMoPayRegistration.txtJomopayID.text;
        frmJoMoPayRegistration.lblJomopayTypeConfirm.text = isEmpty(frmJoMoPayRegistration.lblPayType.text) ? "M" : frmJoMoPayRegistration.lblPayType.text;
        frmJoMoPayRegistration.lblCountryConfirm.text = frmJoMoPayRegistration.lblCountry.text;
        frmJoMoPayRegistration.lblCityConfirm.text = frmJoMoPayRegistration.lblCity.text;
        frmJoMoPayRegistration.lblCountryCodeConfirm.text = "400";
        frmJoMoPayRegistration.lblCityCodeConfirm.text = frmJoMoPayRegistration.lblCityCode.text;
        frmJoMoPayRegistration.lblLanguage.text = Language;
        frmJoMoPayRegistration.lblJMPAlias.text = frmJoMoPayRegistration.txtAlias.text;
        frmJoMoPayRegistration.lblDOBDummy.text = frmJoMoPayRegistration.lblDateOfBirth.text;
        var dob = frmJoMoPayRegistration.lblDateOfBirth.text.split("/");
        frmJoMoPayRegistration.lblDOB.text = dob[2] + "-" + dob[1] + "-" + dob[0];
        frmJoMoPayRegistration.forceLayout();
    } else {
        kony.print("Please Fill all the data");
    }
}

function getJomoPayTransactionsList() {
    try {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        kony.print("In getJomoPayTransactionsList");
        kony.application.showLoadingScreen(null, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
        frmJoMoPayLanding.segJomopayDetails.removeAll();
        if (glbJmpFlow == "dash") {
            /*frmJoMoPayLanding.btnAdd.isVisible = true;
            frmJoMoPayLanding.flxPaymentHistory.isVisible = false;
            frmJoMoPayLanding.flxJomoPayList.isVisible = true;
            frmJoMoPayLanding.flxJoMoPayHeader.isVisible = true;
            frmJoMoPayLanding.flxCheckRegJomopay.isVisible = false;
            frmJoMoPayLanding.flxSearch.isVisible = true;
            frmJoMoPayLanding.btnReg.isVisible = true;*/
            glbJmpFlow = "";
            update_JOMOPAY_BENLIST = false;
        }
        /* frmJoMoPayLanding.btnAdd.isVisible = true;
         frmJoMoPayLanding.flxPaymentHistory.isVisible = false;
         frmJoMoPayLanding.flxJomoPayList.isVisible = true;
         frmJoMoPayLanding.flxJoMoPayHeader.isVisible = true;*/
        frmJoMoPayLanding.lblJoMoPay.text = "JoMoPay";
        frmJoMoPayLanding.forceLayout();
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var controller = INSTANCE.getFormController("frmJoMoPayLanding");
        var navObject = new kony.sdk.mvvm.NavigationObject();
        navObject.setRequestOptions("segJomopayDetails", {
            "headers": {
                "session_token": kony.retailBanking.globalData.session_token
            },
            "queryParams": {
                "custId": custid,
                "lang": "EN"
            }
        });
        controller.loadDataAndShowForm(navObject);
    } catch (e) {
        exceptionLogCall("getjomoPayTransactionsList", "UI ERROR", "UI", e);
        kony.print("In Exception of getjomoPayTransactionsList::" + e);
    }
}

function getDatatoPaymentHistory() {
    kony.print("getDatatoPaymentHistory");
    kony.application.showLoadingScreen(null, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
    var langselected = (kony.store.getItem("langPrefObj") === "en" ? "eng" : "ara");
    kony.print("Total Accounts11::" + JSON.stringify(fromAccounts));

    function loopingSuccessCallback(response) {
        kony.print("getDatatoPaymentHistory response Looping:::" + JSON.stringify(response));
        var segData = [];
        var segRowData = [];
        var aHeader = {};
        var sectionAccData = [];
        frmJoMoPayLanding.transactionSegment.widgetDataMap = {
            "transactionDate": "transactionDate",
            "transactionName": "transactionName",
            "transactionAmount": "transactionAmount",
            "lbltmpTitle": "lbltmpTitle",
            "lblNoData": "lblNoData",
            "lblTransType": "lblTransType",
            "lblTransRef": "lblTransRef"
        };
        for (var i in fromAccounts) {
            segData = [];
            segRowData = [];
            aHeader = {};
            aHeader.lbltmpTitle = {
                "text": fromAccounts[i].accountID,
                "isVisible": true
            };
            segData.push(aHeader);
            if (response.LoopDataset !== null && response.LoopDataset !== undefined && response.LoopDataset.length > 0) {
                if (response.LoopDataset[i].StatsCollection !== undefined) {
                    var acctransaction = response.LoopDataset[i].StatsCollection;
                    for (var j in acctransaction) {
                        var dateFormat = isEmpty(acctransaction[j].transactiondate) ? "" : getShortMonthDateFormat(acctransaction[j].transactiondate);
                        var transDesc = "";
                        //             var tranSym = (acctransaction[j].creditdebitflag === "D") ? "+ " : "- ";
                        if (acctransaction[j].currencydesc !== null && acctransaction[j].currencydesc !== "") {
                            transDesc = acctransaction[j].currencydesc;
                            if (!isEmpty(transDesc)) {
                                if (transDesc.length > 60) transDesc = transDesc.substring(0, 60) + "  .  .  .";
                            }
                        }
                        segRowData.push({
                            transactionDate: {
                                "text": dateFormat,
                                "isVisible": true
                            },
                            transactionName: {
                                "text": transDesc, //acctransaction[j].description,
                                "isVisible": true
                            },
                            transactionAmount: {
                                "text": formatamountwithCurrency(acctransaction[j].transactionamount, acctransaction[j].currencycode) + " " + acctransaction[j].currencycode,
                                "isVisible": true
                            },
                            lblNoData: {
                                "text": geti18Value("i18n.accounts.noTransaction"),
                                "isVisible": false
                            },
                            lblTransType: {
                                "text": gettransdesc(acctransaction[j].txnmodule),
                                "isVisible": true
                            },
                            lblTransRef: {
                                "text": acctransaction[j].txnrefnumber,
                                "isVisible": true
                            }
                        });
                    }
                } else {
                    kony.print(" No Transactios:::");
                    segRowData.push({
                        transactionDate: {
                            "isVisible": false
                        },
                        transactionName: {
                            "isVisible": false
                        },
                        transactionAmount: {
                            "isVisible": false
                        },
                        lblNoData: {
                            "text": geti18Value("i18n.accounts.noTransaction"),
                            "isVisible": true
                        },
                        lblTransType: {
                            "isVisible": false
                        },
                        lblTransRef: {
                            "isVisible": false
                        }
                    });
                }
            } else {
                kony.print(" No Transactios:::");
                segRowData.push({
                    transactionDate: {
                        "isVisible": false
                    },
                    transactionName: {
                        "isVisible": false
                    },
                    transactionAmount: {
                        "isVisible": false
                    },
                    lblNoData: {
                        "text": geti18Value("i18n.accounts.noTransaction"),
                        "isVisible": true
                    },
                    lblTransType: {
                        "isVisible": false
                    },
                    lblTransRef: {
                        "isVisible": false
                    }
                });
            }
            segData.push(segRowData);
            sectionAccData.push(segData);
        }
        kony.print("sectionAccData1:::" + JSON.stringify(sectionAccData));
        frmJoMoPayLanding.transactionSegment.setData(sectionAccData);
        frmJoMoPayLanding.show();
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }

    function loopingErrorCallback(error) {
        kony.print("error looping" + JSON.stringify(error));
        var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
    }
    if (fromAccounts !== undefined && fromAccounts !== null && fromAccounts.length > 0) {
        var date = new Date();
        var day = date.getDate();
        if (day < 10) {
            day = "0" + day;
        }
        var mon = date.getMonth() + 1;
        if (mon < 10) {
            mon = "0" + mon;
        }
        var year = date.getFullYear();
        var currDate = year + "-" + mon + "-" + day;
        var queryParams = {
            "custid": "",
            "loop_count": "" + parseInt(fromAccounts.length),
            "loop_seperator": ":",
            "p_Account": "",
            "p_Branch": "",
            "p_toDate": "",
            "lang": ""
        };
        var i = 0;
        for (i = 0; i < fromAccounts.length; i++) {
            queryParams.custid = queryParams.custid + (i == fromAccounts.length - 1 ? custid : (custid + ":"));
            queryParams.p_Account = queryParams.p_Account + (i == fromAccounts.length - 1 ? fromAccounts[i].accountID : (fromAccounts[i].accountID + ":"));
            queryParams.p_Branch = queryParams.p_Branch + (i == fromAccounts.length - 1 ? fromAccounts[i].branchNumber : (fromAccounts[i].branchNumber + ":"));
            queryParams.lang = queryParams.lang + (i == fromAccounts.length - 1 ? langselected : (langselected + ":"));
            queryParams.p_toDate = queryParams.p_toDate + (i == fromAccounts.length - 1 ? currDate : (currDate + ":"));
        }
        kony.print("Query Params:: " + JSON.stringify(queryParams));
        var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("LoopStatistics");
        if (kony.sdk.isNetworkAvailable()) {
            appMFConfiguration.invokeOperation("LoopStatsService", {}, queryParams, loopingSuccessCallback, loopingErrorCallback);
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("No data::::");
        var errorMessage = geti18nkey("i18n.alerts.NoAccounts");
        customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
    }
}

function maskFirstLastFourChars(accnumber) {
    var maskedNum = accnumber;
    if (!isEmpty(accnumber)) {
        var first4 = accnumber.substring(0, 4);
        var last4 = accnumber.substring(accnumber.length - 4);
        mask = accnumber.substring(4, accnumber.length - 4).replace(/\d/g, "*");
        maskedNum = first4 + mask + last4;
    }
    return maskedNum;
}

function getLangBojServices() {
    kony.print("getLangBojServices:::");
    var langSelected = kony.store.getItem("langPrefObj");
    var Language = langSelected.toUpperCase();
    var lan = "eng";
    if (Language == "EN") {
        lan = "eng";
    } else {
        lan = "ara";
    }
    return lan;
}

function resetJomoPayRegistationSaveData() {
    /******************ENABLING FIELDS**********************/
    frmJoMoPayRegistration.txtNationalID.setEnabled(true);
    frmJoMoPayRegistration.btnCountryDropDown.setVisibility(true);
    frmJoMoPayRegistration.btnCityDropDown.setVisibility(true);
    frmJoMoPayRegistration.calDateDOB.setVisibility(true);
    /********************************************************/
    frmJoMoPayRegistration.lblNationalIDConfirm.text = "";
    frmJoMoPayRegistration.lblIDTypeConfirm.text = "";
    frmJoMoPayRegistration.lblPhoneConfirm.text = "";
    frmJoMoPayRegistration.lblInvalidCredentialsKA.isVisible = false;
    frmJoMoPayRegistration.flxBorderEmail.skin = "skntextFieldDividerJomoPay";
    frmJoMoPayRegistration.lblEmailConfirm.text = "";
    frmJoMoPayRegistration.lblJomopayIDConfirm.text = "";
    frmJoMoPayRegistration.lblJomopayTypeConfirm.text = "";
    frmJoMoPayRegistration.lblCountryConfirm.text = "";
    frmJoMoPayRegistration.lblCityConfirm.text = "";
    frmJoMoPayRegistration.lblCountryCodeConfirm.text = "";
    frmJoMoPayRegistration.lblCityCodeConfirm.text = "";
    frmJoMoPayRegistration.lblLanguage.text = "";
    frmJoMoPayRegistration.btnNext.skin = "jomopaynextDisabled";
    frmJoMoPayRegistration.btnNext.focusSkin = "jomopaynextDisabled";
    frmJoMoPayRegistration.txtNationalID.text = "";
    //frmJoMoPayRegistration.lblIDType.text="";
    frmJoMoPayRegistration.txtPhoneNo.text = "";
    frmJoMoPayRegistration.txtEmail.text = "";
    frmJoMoPayRegistration.txtJomopayID.text = "";
    frmJoMoPayRegistration.lblCountry.text = geti18nkey("i18n.jomopay.selectcountry");
    frmJoMoPayRegistration.lblCity.text = geti18nkey("i18n.jomopay.selectcity");
    frmJoMoPayRegistration.lblCityCode.text = "";
    frmJoMoPayRegistration.lblfromAccountCurrency.text = "";
    frmJoMoPayRegistration.lblFromAccountNumber.text = "";
    frmJoMoPayRegistration.txtAlias.text = "";
    frmJoMoPayRegistration.lblJMPAlias.text = "";
    frmJoMoPayRegistration.lblDateOfBirth.text = "";
    frmJoMoPayRegistration.lblDOB.text = "";
    frmJoMoPayRegistration.lblTypeTrn.text = "";
    frmJoMoPayRegistration.flxBodyReg.setVisibility(true);
    frmJoMoPayRegistration.flxConfirmPopup.setVisibility(false);
    var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
    if (!isEmpty(fromAccounts)) {
        var dataSelected = fromAccounts[0];
        var accountNumber = dataSelected.accountID;
        frmJoMoPayRegistration.lblAccountCode.text = accountNumber;
        if (dataSelected.AccNickName !== "") {
            frmJoMoPayRegistration.lblAccountType.text = dataSelected.AccNickName;
            frmJoMoPayRegistration.lblAccountCode.isVisible = false;
        } else {
            frmJoMoPayRegistration.lblAccountType.text = (dataSelected.accountName.length > 22) ? (dataSelected.accountName.substring(0, 22) + "...") : dataSelected.accountName;
            frmJoMoPayRegistration.lblAccountCode.isVisible = true;
        }
        var branchNum = dataSelected.branchNumber;
        if (branchNum.length === 1) {
            branchNum = "00" + branchNum;
        } else if (branchNum.length === 2) {
            branchNum = "0" + branchNum;
        }
        var appendedData = branchNum + "" + dataSelected.accountID;
        frmJoMoPayRegistration.lblFromAccountNumber.text = appendedData;
        frmJoMoPayRegistration.lblfromAccountCurrency.text = dataSelected.currencyCode; //getCurrencyCode_FROM_CURRENCY_ISO(dataSelected.currencyCode);
    }
    frmJoMoPayRegistration.calDateDOB.validStartDate = [1, 1, 1960];
    frmJoMoPayRegistration.calDateDOB.validEndDate = [new Date().getDate(), (new Date().getMonth()) + 1, new Date().getFullYear()];
    frmJoMoPayRegistration.show();
}

function call_serv_getJomopayBeneList() {
    try {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("ExternalAccounts", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("ExternalAccounts");
        dataObject.addField("custId", custid);
        dataObject.addField("P_BENE_TYPE", "JMP");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("service input paramerters ::" + JSON.stringify(dataObject));
        if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb("get", serviceOptions, function(res) {
                kony.print("Success ::" + JSON.stringify(res));
                getJomopayBeneList(res);
            }, function(err) {
                kony.print("failed ::" + JSON.stringify(err));
                /*if(operationName === "InternetMailorder"){
//             		customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.ecommerce.failed"), popupCommonAlertDimiss, "");
                }else{*/
                //             		kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"),geti18nkey("i18n.cards.gotoCards"),"Cards","","");
                //}
                customAlertPopup(geti18Value("i18n.NUO.Error"), geti18Value("i18n.common.defaultErrorMsg"), popupCommonAlertDimiss, "");
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            });
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_call_serv_getJomopayBeneList ::" + e);
        getJomoPayTransactionsList();
    }
}

function showJomopayTransBene() {
    if (frmJoMoPayLanding.btnTrans.skin === "slButtonWhiteTab") {
        frmJoMoPayLanding.segJomopayBeneficiary.isVisible = false;
        frmJoMoPayLanding.flxJomopayBeneficiary.isVisible = false;
        frmJoMoPayLanding.flxPaymentHistory.isVisible = false;
        frmJoMoPayLanding.flxScanMerchantQR.bottom = "10%";
        frmJoMoPayLanding.flxCheckRegJomopay.isVisible = false;
        frmJoMoPayLanding.flxJomoPayList.isVisible = true;
        frmJoMoPayLanding.flxAddBene.setVisibility(false);
        frmJoMoPayLanding.flxScanMerchantQR.setVisibility(false);
        frmJoMoPayLanding.btnAdd.isVisible = true;
        frmJoMoPayLanding.flxSearch.isVisible = true;
        frmJoMoPayLanding.btnReg.isVisible = true;
    } else if (frmJoMoPayLanding.btnBene.skin === "slButtonWhiteTab") {
        if (frmJoMoPayLanding.segJomopayBeneficiary.data !== null && frmJoMoPayLanding.segJomopayBeneficiary.data.length > 0) {
            frmJoMoPayLanding.segJomopayBeneficiary.isVisible = true;
            frmJoMoPayLanding.flxJomopayBeneficiary.isVisible = true;
            frmJoMoPayLanding.btnAdd.isVisible = true;
            frmJoMoPayLanding.flxSearch.isVisible = true;
            frmJoMoPayLanding.btnReg.isVisible = true;
            frmJoMoPayLanding.flxScanMerchantQR.bottom = "10%";
            frmJoMoPayLanding.flxCheckRegJomopay.isVisible = false;
            frmJoMoPayLanding.flxAddBene.setVisibility(true);
            frmJoMoPayLanding.flxScanMerchantQR.setVisibility(true);
        } else {
            frmJoMoPayLanding.flxJomopayBeneficiary.setVisibility(false);
            frmJoMoPayLanding.FlxJomopayTab.setVisibility(true);
            frmJoMoPayLanding.flxSearch.setVisibility(false);
            frmJoMoPayLanding.btnAdd.setVisibility(true);
            frmJoMoPayLanding.btnRege.setVisibility(false);
            frmJoMoPayLanding.lblMessageTitle.text = geti18Value("i18n.jomopay.nobeneficiarytitle");
            frmJoMoPayLanding.lblMessage.text = geti18Value("i18n.jomopay.nobeneficiarymsg");
            frmJoMoPayLanding.flxCheckRegJomopay.top = "16%";
            frmJoMoPayLanding.flxCheckRegJomopay.height = "84%";
            //                 frmJoMoPayLanding.btnAddBene.setVisibility(true);
            frmJoMoPayLanding.flxAddBene.setVisibility(true);
            kony.print("hassan 4");
            //               frmJoMoPayLanding.flxScanMerchantQR.bottom = "15%";
            frmJoMoPayLanding.flxScanMerchantQR.setVisibility(true); //hassan jomopay vis
            //frmJoMoPayLanding.flxScanMerchantQR.setVisibility(true);//hassan jomopay vis ch
            frmJoMoPayLanding.flxCheckRegJomopay.setVisibility(true);
        }
        frmJoMoPayLanding.flxPaymentHistory.isVisible = false;
        frmJoMoPayLanding.flxJomoPayList.isVisible = false;
    }
}
kony.boj.beneJmpList = {
    "countBene": 0
};
kony.boj.resetJmpBeneList = function() {
    kony.boj.beneJmpList = {
        "countBene": 0,
        "JMP": {
            "fav": [],
            "norm": []
        }
    };
};

function getJomopayBeneList(res) {
    //if(res.beneList !== undefined && res.beneList !== null){
    kony.retailBanking.globalData.jmpBeneDetails = res.records[0].beneList;
    try {
        isSearchUpdate = false;
        updateJomopayBeneList(kony.retailBanking.globalData.jmpBeneDetails);
        //         frmJoMoPayLanding.segJomopayBeneficiary.isVisible = true;
        //         frmJoMoPayLanding.flxJomopayBeneficiary.isVisible = true;
        frmJoMoPayLanding.flxPaymentHistory.isVisible = false;
        //         frmJoMoPayLanding.flxCheckRegJomopay.isVisible = false;
        frmJoMoPayLanding.flxJomoPayList.isVisible = false;
        frmJoMoPayLanding.btnAdd.isVisible = true;
        frmJoMoPayLanding.flxPaymentHistory.isVisible = false;
        //         frmJoMoPayLanding.flxJomoPayList.isVisible = true;
        frmJoMoPayLanding.flxJoMoPayHeader.isVisible = true;
        //         frmJoMoPayLanding.flxSearch.isVisible = true;
        frmJoMoPayLanding.btnReg.isVisible = true;
        //      	frmJoMoPayLanding.FlxJomopayTab.setVisibility(true);
        //     	frmJoMoPayLanding.flxJomopayBeneficiary.setVisibility(true);
        getJomoPayTransactionsList();
        //kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        //frmJoMoPayLanding.show();
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_getJomopayBeneList ::" + e);
    }
    //}
}

function updateJomopayBeneList(data) {
    kony.boj.resetBeneList();
    /*******************UI HANDLING**************************/
    frmJoMoPayLanding.btnAddBene.setVisibility(false);
    frmJoMoPayLanding.flxAddBene.setVisibility(false);
    kony.print("hassan 5");
    //frmJoMoPayLanding.flxScanMerchantQR.setVisibility(false); hassan jomopay vis
    frmJoMoPayLanding.flxScanMerchantQR.bottom = "10%";
    frmJoMoPayLanding.flxCheckRegJomopay.setVisibility(false);
    frmJoMoPayLanding.FlxJomopayTab.setVisibility(true);
    frmJoMoPayLanding.flxSearch.setVisibility(true);
    frmJoMoPayLanding.btnAdd.setVisibility(true);
    frmJoMoPayLanding.lblMessageTitle.text = geti18Value("i18n.externalAccount.Sendmoneytobeneficiary");
    frmJoMoPayLanding.lblMessage.text = geti18Value("i18n.jomopay.regestrationMsg");
    frmJoMoPayLanding.flxCheckRegJomopay.top = "12%";
    frmJoMoPayLanding.flxCheckRegJomopay.height = "88%";
    frmJoMoPayLanding.btnRege.setVisibility(true);
    frmJoMoPayLanding.flxJomopayBeneficiary.setVisibility(true);
    /*********************************************/
    try {
        frmJoMoPayLanding.segJomopayBeneficiary.removeAll();
        for (var i in data) {
            data[i].initial = data[i].nickName.substring(0, 2).toUpperCase();
            data[i].icon = {
                backgroundColor: kony.boj.getBackGroundColour(data[i].initial)
            };
            if (data[i].isFav == "Y") {
                data[i].btnSetting = {
                    "text": ","
                };
                kony.boj.beneList.JMP.fav.push(data[i]);
            } else {
                data[i].btnSetting = {
                    "text": "k"
                };
                kony.boj.beneList.JMP.norm.push(data[i]);
            }
            kony.boj.beneList.countBene++;
        }
        frmJoMoPayLanding.segJomopayBeneficiary.widgetDataMap = {
            "lbltmpTitle": "lbltmpTitle",
            "BenificiaryName": "nickName",
            "accountNumber": "benAcctNo",
            "lblInitial": "initial",
            "flxIcon1": "icon",
            "btnFav": "btnSetting"
        };
        frmJoMoPayLanding.lblNoBeneficiary.setVisibility(false);
        frmJoMoPayLanding.segJomopayBeneficiary.setVisibility(true);
        var dataFav = [],
            dataNorm = [];
        dataFav = kony.boj.beneList.JMP.fav;
        dataNorm = kony.boj.beneList.JMP.norm;
        if (dataFav.length !== 0 && dataNorm.length !== 0) {
            frmJoMoPayLanding.flxAddBene.setVisibility(true);
            frmJoMoPayLanding.flxScanMerchantQR.setVisibility(true);
            frmJoMoPayLanding.segJomopayBeneficiary.setData([
                [{
                        lbltmpTitle: geti18Value("i18n.common.Favourite")
                    },
                    dataFav
                ],
                [{
                        lbltmpTitle: geti18Value("i18n.Bene.AllContacts")
                    },
                    dataNorm
                ],
            ]);
        } else if (dataFav.length !== 0) {
            frmJoMoPayLanding.flxAddBene.setVisibility(true);
            frmJoMoPayLanding.flxScanMerchantQR.setVisibility(true);
            frmJoMoPayLanding.segJomopayBeneficiary.setData([
                [{
                        lbltmpTitle: geti18Value("i18n.common.Favourite")
                    },
                    dataFav
                ]
            ]);
        } else if (dataNorm.length !== 0) {
            frmJoMoPayLanding.flxAddBene.setVisibility(true);
            frmJoMoPayLanding.flxScanMerchantQR.setVisibility(true);
            frmJoMoPayLanding.segJomopayBeneficiary.setData([
                [{
                        lbltmpTitle: geti18Value("i18n.Bene.AllContacts")
                    },
                    dataNorm
                ]
            ]);
        } else {
            if (isSearchUpdate === true) {
                frmJoMoPayLanding.segJomopayBeneficiary.setVisibility(false);
                frmJoMoPayLanding.lblNoBeneficiary.setVisibility(true);
            } else {
                frmJoMoPayLanding.flxJomopayBeneficiary.setVisibility(false);
                frmJoMoPayLanding.FlxJomopayTab.setVisibility(true);
                frmJoMoPayLanding.flxSearch.setVisibility(false);
                frmJoMoPayLanding.btnAdd.setVisibility(true);
                frmJoMoPayLanding.btnRege.setVisibility(false);
                frmJoMoPayLanding.lblMessageTitle.text = geti18Value("i18n.jomopay.nobeneficiarytitle");
                frmJoMoPayLanding.lblMessage.text = geti18Value("i18n.jomopay.nobeneficiarymsg");
                frmJoMoPayLanding.flxCheckRegJomopay.top = "16%";
                frmJoMoPayLanding.flxCheckRegJomopay.height = "84%";
                //                 frmJoMoPayLanding.btnAddBene.setVisibility(true);
                frmJoMoPayLanding.flxAddBene.setVisibility(true);
                kony.print("hassan 5");
                //                frmJoMoPayLanding.flxScanMerchantQR.bottom = "15%";
                frmJoMoPayLanding.flxScanMerchantQR.setVisibility(true); //hassan jomopay vis
                //frmJoMoPayLanding.flxScanMerchantQR.setVisibility(false); hassan jomopay vis
                frmJoMoPayLanding.flxCheckRegJomopay.setVisibility(true);
            }
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        frmJoMoPayLanding.segJomopayBeneficiary.setVisibility(false);
        kony.print("Exception_getJomopayBeneList ::" + e);
    }
}
kony.boj.setJomoBeneDataList = function(selectedBene) {
    kony.print("Selected Bene fav ::" + JSON.stringify(selectedBene));
    if (selectedBene.isFav === "Y") customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.Transfer.rmFav"), onClickYesRemFav, onClickNoRemFav, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
    else customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.Transfer.addFav"), onClickYesRemFav, onClickNoRemFav, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function onClickYesRemFav() {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen();
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("ExternalAccounts", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("ExternalAccounts");
        //     dataObject.addField("usr", "icbsserv");
        //     dataObject.addField("pass", "icbsserv_1");
        dataObject.addField("custId", custid);
        dataObject.addField("accountNumber", selectedBene.benAcctNo);
        dataObject.addField("isFav", (selectedBene.isFav === "Y") ? "N" : "Y");
        dataObject.addField("P_BENEF_ID", selectedBene.benef_id);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("serviceOptions ::" + JSON.stringify(serviceOptions));
        if (kony.sdk.isNetworkAvailable()) {
            //       kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb("update", serviceOptions, function(res) {
                for (var i in kony.retailBanking.globalData.jmpBeneDetails) {
                    if (selectedBene.benef_id === kony.retailBanking.globalData.jmpBeneDetails[i].benef_id) {
                        if (selectedBene.isFav === "Y") {
                            kony.retailBanking.globalData.jmpBeneDetails[i].isFav = "N";
                            kony.retailBanking.globalData.jmpBeneDetails[i].btnSetting = {
                                "text": "k"
                            };
                        } else {
                            kony.retailBanking.globalData.jmpBeneDetails[i].isFav = "Y";
                            kony.retailBanking.globalData.jmpBeneDetails[i].btnSetting = {
                                "text": ","
                            };
                        }
                    }
                }
                kony.print("serviceOptions ::" + JSON.stringify(kony.retailBanking.globalData.jmpBeneDetails));
                isSearchUpdate = false;
                updateJomopayBeneList(kony.retailBanking.globalData.jmpBeneDetails);
            }, function(err) {
                kony.print("Failed in change fav tag ::" + JSON.stringify(err));
            });
        }
        popupCommonAlertDimiss();
    }

    function onClickNoRemFav() {
        popupCommonAlertDimiss();
    }
}

function serv_getjomoppayregdata() {
    try {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("p2pregistration", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("p2pregistration");
        dataObject.addField("custId", custid);
        dataObject.addField("lang", kony.store.getItem("langPrefObj"));
        dataObject.addField("p_user_id", "BOJMOB");
        dataObject.addField("p_channel", "MOBILE");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("service input paramerters ::" + JSON.stringify(dataObject));
        if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb("RegesterdUsers", serviceOptions, function(res) {
                kony.print("Success ::" + JSON.stringify(res));
                getjomoppayregdata(res);
            }, function(err) {
                kony.print("failed ::" + JSON.stringify(err));
                /*if(operationName === "InternetMailorder"){
            		customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.ecommerce.failed"), popupCommonAlertDimiss, "");
                }else{*/
                customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
                //kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"),geti18nkey("i18n.cards.gotoCards"),"Cards","","");
                //}
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            });
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_serv_getjomoppayregdata ::" + e);
    }
}

function call_serv_getjomoppayregdata() {
    try {
        var servData = [];
        serv_getjomoppayregdata();
    } catch (e) {
        kony.print("Exception_call_serv_getjomoppayregdata ::" + e);
    }
}

function getjomoppayregdata(res) {
    if (res.jmpRegistrationDetails !== "" && res.jmpRegistrationDetails !== null && res.jmpRegistrationDetails !== undefined) {
        if (glbJmpFlow == "dash") {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            //getJomoPayTransactionsList();
            frmJoMoPayLanding.btnBene.skin = "slButtonWhiteTab";
            frmJoMoPayLanding.flxScanMerchantQR.setVisibility(true); //hassan jomopay QR visible
            frmJoMoPayLanding.btnTrans.skin = "slButtonWhiteTabDisabled";
            if (res.jmpRegistrationDetails[1] !== undefined && res.jmpRegistrationDetails[1].jomopayid !== "") REG_JMP_MOB = res.jmpRegistrationDetails[1].jomopayid || "";
            getJomoPayTransactionsList();
            call_serv_getJomopayBeneList();
        } else {
            frmJoMoPayRegistration.lblJomoPayReg.text = geti18Value("i18n.jomopay.createaccount");
            if (glbJmpFlow === "Update") {
                frmJoMoPayRegistration.lblJomoPayReg.text = geti18Value("i18n.jomopay.updateprofile");
            }
            if (res.jmpRegistrationDetails[1] !== "" && res.jmpRegistrationDetails[1] !== null && res.jmpRegistrationDetails[1] !== undefined) {
                frmJoMoPayRegistration.txtAlias.text = res.jmpRegistrationDetails[1].alias;
                frmJoMoPayRegistration.txtJomopayID.text = res.jmpRegistrationDetails[1].jomopayid;
                if (res.jmpRegistrationDetails[1].jomopaytype == "M") frmJoMoPayRegistration.lblType.text = geti18Value("i18n.jomopay.mobiletype");
                else frmJoMoPayRegistration.lblType.text = geti18Value("i18n.jomopay.aliastype");
                frmJoMoPayRegistration.lblInvalidCredentialsKA.isVisible = false;
                frmJoMoPayRegistration.flxBorderEmail.skin = "skntextFieldDividerJomoPay";
                frmJoMoPayRegistration.lblLanguage.text = res.jmpRegistrationDetails[0].lang;
                frmJoMoPayRegistration.btnNext.skin = "jomopaynextEnabled";
                frmJoMoPayRegistration.btnNext.focusSkin = "jomopaynextEnabled";
                frmJoMoPayRegistration.txtNationalID.text = res.jmpRegistrationDetails[0].national_id;
                //frmJoMoPayRegistration.lblIDType.text="";
                frmJoMoPayRegistration.txtPhoneNo.text = res.jmpRegistrationDetails[0].custmobno;
                frmJoMoPayRegistration.txtEmail.text = res.jmpRegistrationDetails[0].email;
                //frmJoMoPayRegistration.lblCountry.text=  geti18nkey("i18n.jomopay.selectcountry");
                //frmJoMoPayRegistration.lblCity.text= geti18nkey("i18n.jomopay.selectcity");
                frmJoMoPayRegistration.lblCityCode.text = res.jmpRegistrationDetails[0].citycode;
                frmJoMoPayRegistration.lblfromAccountCurrency.text = res.jmpRegistrationDetails[0].currcode;
                frmJoMoPayRegistration.lblFromAccountNumber.text = res.jmpRegistrationDetails[0].accno;
                frmJoMoPayRegistration.lblJMPAlias.text = "";
                var dob = res.jmpRegistrationDetails[0].dob.split("-");
                frmJoMoPayRegistration.lblDateOfBirth.text = dob[2] + "/" + dob[1] + "/" + dob[0];
                frmJoMoPayRegistration.calDateDOB.validStartDate = [1, 1, 1960];
                frmJoMoPayRegistration.calDateDOB.validEndDate = [new Date().getDate(), (new Date().getMonth()) + 1, new Date().getFullYear()];
                frmJoMoPayRegistration.lblDOB.text = res.jmpRegistrationDetails[0].dob;
                frmJoMoPayRegistration.flxBodyReg.setVisibility(true);
                frmJoMoPayRegistration.flxConfirmPopup.setVisibility(false);
                frmJoMoPayRegistration.lblAccountCode.setVisibility(false);
                frmJoMoPayRegistration.lblAccountCode.text = res.jmpRegistrationDetails[0].accno;
                frmJoMoPayRegistration.lblAccountType.text = res.jmpRegistrationDetails[0].accno;
                frmJoMoPayRegistration.lblTypeTrn.text = "U";
                for (var i in kony.boj.detailsForBene.Country) {
                    if (kony.boj.detailsForBene.Country[i].CTRY_CODE == "1") {
                        frmJoMoPayRegistration.lblCountry.text = kony.boj.detailsForBene.Country[i].CTRY_S_DESC;
                        var countryCode = kony.boj.detailsForBene.Country[i].CTRY_CODE;
                        for (var j in kony.boj.detailsForBene.City[countryCode]) {
                            if (kony.boj.detailsForBene.City[countryCode][j].CITY_CODE == res.jmpRegistrationDetails[0].citycode) {
                                frmJoMoPayRegistration.lblCity.text = kony.boj.detailsForBene.City[countryCode][j].CITY_B_DESC;
                            }
                        }
                    }
                }
                /******************DISABLING FIELDS AS NON EDITIBALE**********************/
                frmJoMoPayRegistration.txtNationalID.setEnabled(false);
                frmJoMoPayRegistration.btnCountryDropDown.setVisibility(false);
                frmJoMoPayRegistration.btnCityDropDown.setVisibility(false);
                frmJoMoPayRegistration.calDateDOB.setVisibility(false);
                /********************************************************/
                nextCheck();
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                frmJoMoPayRegistration.show();
            } else {
                resetJomoPayRegistationSaveData();
                frmJoMoPayRegistration.lblTypeTrn.text = "R";
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            }
        }
    } else {
        if (glbJmpFlow == "dash") {
            kony.print("hassan 1");
            frmJoMoPayLanding.btnAdd.isVisible = false;
            frmJoMoPayLanding.flxPaymentHistory.isVisible = false;
            frmJoMoPayLanding.flxJomoPayList.isVisible = false;
            frmJoMoPayLanding.flxJoMoPayHeader.isVisible = true;
            frmJoMoPayLanding.lblMessageTitle.text = geti18Value("i18n.externalAccount.Sendmoneytobeneficiary");
            frmJoMoPayLanding.lblMessage.text = geti18Value("i18n.jomopay.regestrationMsg");
            frmJoMoPayLanding.btnRege.setVisibility(true);
            frmJoMoPayLanding.flxCheckRegJomopay.top = "12%";
            frmJoMoPayLanding.flxCheckRegJomopay.height = "88%";
            frmJoMoPayLanding.btnAddBene.setVisibility(false);
            frmJoMoPayLanding.flxAddBene.setVisibility(false);
            frmJoMoPayLanding.flxScanMerchantQR.setVisibility(false); //hassan jomopay vis
            //frmJoMoPayLanding.flxScanMerchantQR.bottom = "15%";
            frmJoMoPayLanding.flxCheckRegJomopay.isVisible = true;
            frmJoMoPayLanding.flxSearch.isVisible = false;
            frmJoMoPayLanding.btnReg.isVisible = false;
            frmJoMoPayLanding.flxSearch.setVisibility(false);
            frmJoMoPayLanding.FlxJomopayTab.setVisibility(false);
            frmJoMoPayLanding.flxJomopayBeneficiary.setVisibility(false);
            glbJmpFlow = "";
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            frmJoMoPayLanding.show();
        } else {
            resetJomoPayRegistationSaveData();
            frmJoMoPayRegistration.lblTypeTrn.text = "R";
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }
    }
}

function gettransdesc(DescType) {
    try {
        if (DescType != null && DescType != "" && DescType != undefined) {
            kony.print("DescType ::" + DescType);
            switch (DescType) {
                case "1":
                    return "ATM in & out";
                    break;
                case "2":
                    return "EFwatercoom";
                    break;
                case "3":
                    return "Transfers";
                    break;
                case "4":
                    return "Teller in & out";
                    break;
                case "5":
                    return "Credit card payment";
                    break;
                default:
                    return "Others";
                    break;
            }
        } else {
            return "Others";
        }
    } catch (err) {
        return "Others";
    }
}

function serv_PAYMENT_JOMOPAY() {
    try {
        if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var controller = INSTANCE.getFormController("frmJoMoPayConfirmation");
            var controllerContextData = controller.getContextData() || new kony.sdk.mvvm.NavigationObject();
            controllerContextData.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
            controller.setContextData(controllerContextData);
            controller.performAction("saveData");
            kony.print("SAVE DATA END");
        } else {
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } catch (e) {
        kony.print("Exception_serv_PAYMENT_JOMOPAY ::" + e);
    }
}

function search_JOMOPAY_CONTACTS(searchText) {
    try {
        searchText = searchText.trim();
        var len = testArray.length,
            i = 0,
            data = [],
            isSearchFound = false;
        if (len > 0) {
            while (i < len) {
                if ((testArray[i].hasOwnProperty('firstname')) && testArray[i].firstname !== undefined && testArray[i].firstname !== null) {
                    if (testArray[i].firstname.toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                        isSearchFound = true;
                    }
                }
                if ((testArray[i].hasOwnProperty('lastname')) && testArray[i].lastname !== undefined && testArray[i].lastname !== null) {
                    if (testArray[i].lastname.toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                        isSearchFound = true;
                    }
                }
                if (isSearchFound) {
                    data.push(form_JOMOPAY_CONTACT_LIST_DATA(testArray[i]));
                }
                isSearchFound = false;
                i++;
            }
            data = sort_ALPHABETICALLY(data, "lblcname");
            frmJomopayContacts.segJomopayList.widgetDataMap = {
                lblName: "lblcname",
                lblPhoneNum: "lblpno",
                btnPic: "btnProfilePic"
            };
            frmJomopayContacts.segJomopayList.removeAll();
            frmJomopayContacts.segJomopayList.setData(data);
        }
    } catch (e) {
        kony.print("Exception_search_JOMOPAY_CONTACTS ::" + e);
    }
}

function form_JOMOPAY_CONTACT_LIST_DATA(contactDATA) {
    var name = "";
    var number12 = "";
    var shortName = "",
        isValid = false;
    var processedRowObj, profileButton = null;
    processedRowObj = {};
    if ((contactDATA.phone) !== undefined && contactDATA.phone[0].number !== null && contactDATA.phone[0].number !== undefined && contactDATA.phone[0].number !== "") {
        isValid = true;
    }
    if (isValid) {
        if ((contactDATA.hasOwnProperty('firstname')) && contactDATA.firstname !== undefined && contactDATA.firstname !== null) {
            name = contactDATA.firstname;
            shortName = name.charAt(0);
        }
        if ((contactDATA.hasOwnProperty('lastname')) && contactDATA.lastname !== undefined && contactDATA.lastname !== null) {
            var Ln = contactDATA.lastname;
            name = name + " " + Ln;
            name = name.trim();
            shortName = shortName + Ln.charAt(0);
        }
        if (name !== null && name !== "") {
            shortName = shortName.toUpperCase();
        }
        number12 = contactDATA.phone[0].number;
        var color = ((1 << 24) * Math.random() | 0).toString(16);
        profileButton = {
            enable: true,
            text: shortName,
            backgroundColor: color,
            visible: true
        };
    }
    return {
        "lblcname": name,
        "lblpno": number12,
        "btnProfilePic": profileButton
    };
}
//Nart Rawashdeh 28/10/2020 <<<<<
function validateAliasField(val, widget) {
    if (val.match(/^[A-Z0-9]+@+([A-Z0-9]+)/g) && (val.length >= 6) && (val.length <= 15)) {
        widget.text = val;
        frmJoMoPayRegistration.CopyflxBorderjomopayID0b6963ff7a40e46.skin = "skntextFieldDividerGreen";
        return true;
    } else {
        frmJoMoPayRegistration.CopyflxBorderjomopayID0b6963ff7a40e46.skin = "skntextFieldDividerOrange";
        return false;
    }
}