//Type your code here
function navigateToExchangeRates() {
    kony.print("navigateToExchangeRates");
    frmFxRate.btnExchangeRates.skin = "slButtonWhiteTab";
    frmFxRate.btnExchangeRates.focusSkin = "slButtonWhiteTabFocus";
    frmFxRate.btnCalculateFx.skin = "slButtonWhiteTabDisabled";
    frmFxRate.flxScrlCalculateFx.setVisibility(false);
    frmFxRate.flxScrlExchangeRates.setVisibility(true);
    if (kony.sdk.mvvm.isNetworkAvailabile()) {
        ShowLoadingScreen();
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("ExchangeRates", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("ExchangeRates");
        dataObject.addField("user_id", user_id);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        modelObj.customVerb("getFXRates", serviceOptions, navigateToExchangeRatesSuccess, navigateToExchangeRatesError);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function navigateToExchangeRatesSuccess(response) {
    kony.print("navigateToExchangeRatesSuccess:::" + JSON.stringify(response));
    if (!isEmpty(response.CurrencyMain) && response.CurrencyMain.length > 0) {
        gblFxRateModifiedArray = [];
        gblFxRateUniqueCountryList = [];
        var Curr = response.CurrencyMain.map(function(obj) {
            return obj.FromCurrency;
        });
        gblFxRateUniqueCountryList = Curr.filter(function(v, i) {
            return Curr.indexOf(v) == i;
        });
        kony.print("gblFxRateUniqueCountryList:::" + JSON.stringify(gblFxRateUniqueCountryList));
        for (var i in gblFxRateUniqueCountryList) {
            var dataObj = [];
            for (var j in response.CurrencyMain) {
                var row = response.CurrencyMain[j];
                if (row.FromCurrency === gblFxRateUniqueCountryList[i]) {
                    var flag1 = getFlagforCurrencyy(row.ToCurrency);
                    row.IMG_FLAG = flag1[1];
                    row.ENGLISH_DESC = flag1[0];
                    row.ARABIC_DESC = flag1[2];
                    dataObj.push(row);
                }
            }
            var flag = getFlagforCurrencyy(gblFxRateUniqueCountryList[i]);
            gblFxRateModifiedArray.push({
                "from": gblFxRateUniqueCountryList[i],
                "to": dataObj,
                "IMG_FLAG": flag[1],
                "ENGLISH_DESC": flag[0],
                "ARABIC_DESC": flag[2]
            });
        }
        kony.print("gblFxRateModifiedArray:::" + JSON.stringify(gblFxRateModifiedArray));
        loadSegDataforExchangeRates(gblFxRateModifiedArray[0].from);
        frmFxRate.imgFlag.src = gblFxRateModifiedArray[0].IMG_FLAG;
        if (kony.store.getItem("langPrefObj") == "ar") frmFxRate.lblCountryName.text = gblFxRateModifiedArray[0].ARABIC_DESC;
        else frmFxRate.lblCountryName.text = gblFxRateModifiedArray[0].ENGLISH_DESC;
        frmFxRate.lblCurrency.text = gblFxRateModifiedArray[0].from;
        frmFxRate.imgLeftFlag.src = gblFxRateModifiedArray[0].IMG_FLAG;
        frmFxRate.lblLeftValue.text = gblFxRateModifiedArray[0].from;
        frmFxRate.imgRightFlag.src = gblFxRateModifiedArray[0].to[0].IMG_FLAG;
        frmFxRate.lblRightValue.text = gblFxRateModifiedArray[0].to[0].ToCurrency;
        frmFxRate.txtEnteredValue.text = "";
        frmFxRate.txtConvertedValue.text = "";
        frmFxRate.lblActualValue.text = 1 + gblFxRateModifiedArray[0].from;
        frmFxRate.lblEqualentValue.text = formatamountwithCurrency(gblFxRateModifiedArray[0].to[0].SellRate, gblFxRateModifiedArray[0].to[0].ToCurrency) + " " + gblFxRateModifiedArray[0].to[0].ToCurrency;
        frmFxRate.txtConvertedValue.isReadOnly = true;
        frmFxRate.txtConvertedValue.setEnabled(false);
        frmFxRate.segCalculateFX.removeAll();
        frmFxRate.forceLayout();
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        frmFxRate.show();
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
        customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
    }
}

function navigateToExchangeRatesError(err) {
    kony.print("navigateToExchangeRatesError:::" + JSON.stringify(err));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
}

function ExchangeFlagsViceVersa() {
    kony.print("ExchangeFlagsViceVersa::");
    var fromFlag = false;
    var toFlag = false;
    var data = "";
    var toData = "";
    for (var i in gblFxRateModifiedArray) {
        if (gblFxRateModifiedArray[i].from === frmFxRate.lblRightValue.text) {
            data = gblFxRateModifiedArray[i];
            fromFlag = true;
            break;
        }
    }
    if (fromFlag) {
        for (var k in data.to) {
            if (data.to[k].ToCurrency === frmFxRate.lblLeftValue.text) {
                toData = data.to[k];
                toFlag = true;
                break;
            }
        }
    }
    if (fromFlag && toFlag) {
        gblExchangeTempObject = {
            "CURR_CODE": "",
            "ARABIC_DESC": "",
            "ENGLISH_DESC": "",
            "CURR_ISO": frmFxRate.lblRightValue.text,
            "DECIMAL_PLACES": "",
            "IMG_FLAG": frmFxRate.imgRightFlag.src
        };
        frmFxRate.imgRightFlag.src = frmFxRate.imgLeftFlag.src;
        frmFxRate.lblRightValue.text = frmFxRate.lblLeftValue.text;
        frmFxRate.imgLeftFlag.src = gblExchangeTempObject.IMG_FLAG;
        frmFxRate.lblLeftValue.text = gblExchangeTempObject.CURR_ISO;
        var value = "";
        if (!isEmpty(frmFxRate.txtEnteredValue.text)) {
            value = parseFloat(frmFxRate.txtEnteredValue.text);
        }
        frmFxRate.txtConvertedValue.text = (!isEmpty(value)) ? (formatamountwithCurrency(value * (parseFloat(toData.SellRate)), toData.ToCurrency)) : "";
        frmFxRate.lblActualValue.text = 1 + gblExchangeTempObject.CURR_ISO;
        frmFxRate.lblEqualentValue.text = formatamountwithCurrency(toData.SellRate, toData.ToCurrency) + " " + toData.ToCurrency;;
        loadDatatoSegCalculateFX(gblExchangeTempObject.CURR_ISO);
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.fxrates.noequivalentcurrency"), popupCommonAlertDimiss, "");
    }
}

function loadSegDataforExchangeRates(from) {
    kony.print("loadSegDataforExchangeRates:::" + from);
    var data = "";
    var segData = [];
    for (var i in gblFxRateModifiedArray) {
        if (gblFxRateModifiedArray[i].from === from) {
            data = gblFxRateModifiedArray[i];
            break;
        }
    }
    /**********
    formatamountwithCurrency(todata.SellRate,todata.ToCurrency)
    formatamountwithCurrency(todata.BuyRate,todata.ToCurrency)
    **********/
    if (!isEmpty(data.to) && data.to.length > 0) {
        for (var j in data.to) {
            var todata = data.to[j];
            segData.push({
                "imgFlag": todata.IMG_FLAG,
                "lblCountry": (kony.store.getItem("langPrefObj") === "ar") ? todata.ARABIC_DESC : todata.ENGLISH_DESC,
                "lblCurrency": todata.ToCurrency,
                "lblSellRate": setDecimal(todata.SellRate, 6),
                "lblBuyRate": setDecimal(todata.BuyRate, 6)
            });
        }
        kony.print("segDatasegData::" + JSON.stringify(segData));
        frmFxRate.segExchangeRates.setData(segData);
    } else {
        frmFxRate.segExchangeRates.removeAll();
    }
    frmFxRate.show();
}

function SetDatatoConvertValue(from, to) {
    kony.print("SetDatatoConvertValue:::" + from);
    var data = "";
    var segData = [];
    for (var i in gblFxRateModifiedArray) {
        if (gblFxRateModifiedArray[i].from === from) {
            data = gblFxRateModifiedArray[i];
            break;
        }
    }
    if (!isEmpty(data.to) && data.to.length >= 0) {
        var todata = "";
        if (gblCalculateFXLeft) {
            todata = data.to[0];
        } else {
            for (var j = 0; j < data.to.length; j++) {
                if (data.to[j].ToCurrency === to) {
                    todata = data.to[j];
                    break;
                }
            }
        }
        frmFxRate.imgRightFlag.src = todata.IMG_FLAG;
        frmFxRate.lblRightValue.text = todata.ToCurrency;
        frmFxRate.txtEnteredValue.text = "";
        frmFxRate.txtConvertedValue.text = "";
        frmFxRate.lblEqualentValue.text = formatamountwithCurrency(todata.SellRate, todata.ToCurrency) + " " + todata.ToCurrency;
    }
    frmFxRate.segCalculateFX.removeAll();
    frmFxRate.show();
}

function loadDatatoSegCalculateFX(from) {
    kony.print("loadDatatoSegCalculateFX:::" + from);
    var data = "";
    var segData = [];
    for (var i in gblFxRateModifiedArray) {
        if (gblFxRateModifiedArray[i].from === from) {
            data = gblFxRateModifiedArray[i];
            break;
        }
    }
    if (!isEmpty(data.to) && data.to.length > 0) {
        for (var j in data.to) {
            var todata = data.to[j];
            var value = "";
            if (!isEmpty(frmFxRate.txtEnteredValue.text)) {
                value = parseFloat(frmFxRate.txtEnteredValue.text);
            }
            if (todata.ToCurrency === frmFxRate.lblRightValue.text) {
                frmFxRate.txtConvertedValue.text = (!isEmpty(value)) ? formatamountwithCurrency(value * (parseFloat(todata.SellRate)), todata.ToCurrency) : "";
            } else {
                segData.push({
                    "imgFlag": todata.IMG_FLAG,
                    "lblConvertedValue": (!isEmpty(value)) ? formatamountwithCurrency(value * (parseFloat(todata.SellRate)), todata.ToCurrency) : formatamountwithCurrency(todata.SellRate, todata.ToCurrency) + " " + todata.ToCurrency,
                    "lblCurrency": todata.ToCurrency,
                    "lblEqualentValue": formatamountwithCurrency(todata.SellRate, todata.ToCurrency) + " " + todata.ToCurrency,
                    "lblActualValue": frmFxRate.lblActualValue.text
                });
            }
        }
        kony.print("segDatasegData::" + JSON.stringify(segData));
        //frmFxRate.segCalculateFX.setData(segData);
    } else {
        frmFxRate.segCalculateFX.removeAll();
    }
    //frmFxRate.show();
}

function getFlagforCurrencyy(curr) {
    kony.print("getFlagforCurrencyy:::" + curr);
    var ENGLISH_DESC = "",
        IMG_FLAG = "",
        ARABIC_DESC = "";
    for (var j in gblCurrList) {
        var row = gblCurrList[j];
        if (row.CURR_ISO === curr) {
            ENGLISH_DESC = row.ENGLISH_DESC;
            ARABIC_DESC = row.ARABIC_DESC;
            IMG_FLAG = row.IMG_FLAG;
            break;
        }
    }
    return [ENGLISH_DESC, IMG_FLAG, ARABIC_DESC];
}