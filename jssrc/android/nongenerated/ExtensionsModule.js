kony = kony || {};
kony.retailbankingapp = kony.retailbankingapp || {};
kony.retailbankingapp.sdk = kony.retailbankingapp.sdk || {};
kony.retailbankingapp.extensions = kony.retailbankingapp.extensions || {};
//Using name space kony.retailbankingapp.sdk in a view of Exposing this functions as sdk API's 
// overriding functions should provide a return object(with two keys "status" and "msg") as specified in Documentation
// Idea is to call a register function as soon as a new function is defined
//API to register for Extensions
kony.retailbankingapp.sdk.registerFunction = function(existingFunc, newFunc) {
    if (typeof(newFunc) === "function") {
        var jsn = {};
        jsn[existingFunc] = newFunc;
        kony.sdk.ClassExtensionUtility.updateMember(jsn);
    } else {
        alert("specified functions are incorrect");
    }
};

function secondFactorAuth(scopeObj, func) {
    //alert("In second factor Auth");
    func.call(scopeObj, {
        "status": "success",
        "msg": "Successfuly authunticated"
    });
    //return {"status":"error","msg":"No Internet Connection"}
}

function serviceCharges(scopeObj, func) {
    //alert("In Service Charges Hook");
    func.call(scopeObj, {
        "status": "success",
        "msg": "Successfuly authunticated"
    });
    //return {"status":"error","msg":"No Internet Connection"}
}

function tncAfterLogin(scopeObj, func) {
    //alert("In T&C");
    func.call(scopeObj, {
        "status": "success",
        "msg": "T&C"
    });
    //return {"status":"error","msg":"No Internet Connection"}
}

function additionalValidationRdc(scopeObj, func) {
    //alert("In T&C");
    func.call(scopeObj, {
        "status": "success",
        "msg": "Additional Validation For Rdc"
    });
    //return {"status":"error","msg":"No Internet Connection"}
}

function registerExtentions() {}