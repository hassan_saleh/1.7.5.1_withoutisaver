//Created by Arpan
kony = kony || {};
kony.boj = kony.boj || {};
kony.boj.BillerTabSwitch = false;
kony.boj.addBillerFromQuickPay = false;
kony.boj.storedBillerDetail = {};
kony.boj.initialiseStoredDetails = function() {
    kony.boj.storedBillerDetail = {
        "PostPaid": {
            "BillerNumber": "",
            "BillerID": "",
            "Address": "",
            "NickName": ""
        },
        "PrePaid": {
            "BillerNumber": "",
            "BillerID": "",
            "Address": "",
            "NickName": ""
        }
    };
};
kony.boj.onClickNextAddBiller = function() {
    frmAddNewPayeeKA.lblLanguage.text = kony.boj.lang;
    frmAddNewPayeeKA.lblType.text = "A";
    //1967 fix
    var nicknm = frmAddNewPayeeKA.tbxNickName.text;
    if (nicknm !== undefined && nicknm !== "" && nicknm !== null && nicknm != "0") {
        frmAddNewPayeeKA.lblBillerNamePopup.text = nicknm;
        frmAddNewPayeeKA.lblInitial.text = nicknm.substring(0, 2).toUpperCase();
    } else {
        frmAddNewPayeeKA.lblBillerNamePopup.text = frmAddNewPayeeKA.lblBillerName.text;
        frmAddNewPayeeKA.lblInitial.text = frmAddNewPayeeKA.lblBillerName.text.substring(0, 2).toUpperCase();
    }
    //end
    //frmAddNewPayeeKA.lblBillerNamePopup.text = frmAddNewPayeeKA.lblBillerName.text;
    //frmAddNewPayeeKA.lblInitial.text = frmAddNewPayeeKA.lblBillerName.text.substring(0, 2).toUpperCase();
    frmAddNewPayeeKA.flxIcon1.backgroundColor = kony.boj.getBackGroundColour(frmAddNewPayeeKA.lblInitial.text);
    frmAddNewPayeeKA.lblBillerNumPopup.text = frmAddNewPayeeKA.tbxBillerNumber.text;
    frmAddNewPayeeKA.flxBillerCategoryPopup.isVisible = true;
    frmAddNewPayeeKA.lblBillerNamePopup.isVisible = true;
    frmAddNewPayeeKA.lblConfirmBillerCategory.text = frmAddNewPayeeKA.lblBillerCategory.text;
    frmAddNewPayeeKA.lblConfirmServiceType.text = frmAddNewPayeeKA.lblServiceType.text;
    frmAddNewPayeeKA.lblConfirmIDType.text = ""; //frmAddNewPayeeKA.lblIDType.text;
    frmAddNewPayeeKA.lblConfirmIDNumber.text = ""; //frmAddNewPayeeKA.tbxBillerID.text;
    frmAddNewPayeeKA.lblConfirmNickName.text = frmAddNewPayeeKA.tbxNickName.text;
    frmAddNewPayeeKA.lblConfirmAddress.text = ""; //frmAddNewPayeeKA.tbxAddress.text;
    frmAddNewPayeeKA.lblConfirmNationality.text = ""; //frmAddNewPayeeKA.lblNationality.text;
    frmAddNewPayeeKA.tbxPhoneNo.text = "";
    frmAddNewPayeeKA.flxConfirmPopUp.isVisible = true;
    frmAddNewPayeeKA.flxFormMain.isVisible = false;
};
kony.boj.resetDropdownBiller = function(name) {
    var tbx = "tbx" + name;
    var lbl = "lbl" + name;
    var line = "flxUnderline" + name;
    var form = null;
    if (gblQuickFlow === "pre") form = frmAddNewPrePayeeKA;
    else form = frmAddNewPayeeKA;
    form[tbx].text = "";
    form[lbl].skin = "sknLblNextDisabled";
    form[line].skin = "sknFlxGreyLine";
    switch (name) {
        case "BillerCategory":
            form[lbl].text = kony.i18n.getLocalizedString("i18n.billsPay.BillerCategory");
            form.flxBillerCategory.isVisible = true;
            break;
        case "BillerName":
            form[lbl].text = kony.i18n.getLocalizedString("i18n.billsPay.billerName");
            form.flxBillerName.isVisible = true;
            break;
        case "ServiceType":
            form[lbl].text = kony.i18n.getLocalizedString("i18n.billsPay.ServiceType");
            form.lblserviceTypeHint.isVisible = false;
            break;
        case "IDType":
            form[lbl].text = kony.i18n.getLocalizedString("i18n.billsPay.IDType");
            break;
        case "Nationality":
            form[lbl].text = kony.i18n.getLocalizedString("i18n.billsPay.Nationality");
            break;
    }
};
kony.boj.partialResetBiller = function() {
    frmAddNewPayeeKA.tbxBillerNumber.text = "";
    animateLabel("DOWN", "lblBillerNumber", "");
    frmAddNewPayeeKA.flxUnderlineBillerNumber.skin = "sknFlxGreyLine";
    frmAddNewPayeeKA.tbxBillerID.text = "";
    animateLabel("DOWN", "lblBillerID", "");
    frmAddNewPayeeKA.flxUnderlineBillerID.skin = "sknFlxGreyLine";
    frmAddNewPayeeKA.tbxAddress.text = "";
    animateLabel("DOWN", "lblAddress", "");
    frmAddNewPayeeKA.flxUnderlineAddress.skin = "sknFlxGreyLine";
    frmAddNewPayeeKA.tbxPhoneNo.text = "";
    animateLabel("DOWN", "lblPhoneNo", "");
    frmAddNewPayeeKA.flxUnderlinePhoneNo.skin = "sknFlxGreyLine";
    frmAddNewPayeeKA.tbxNickName.text = "";
    animateLabel("DOWN", "lblNickName", "");
    frmAddNewPayeeKA.flxUnderlineNickName.skin = "sknFlxGreyLine";
    frmAddNewPayeeKA.lblNext.skin = "sknLblNextDisabled";
    kony.boj.initialiseStoredDetails();
}
kony.boj.resetBiller = function() {
    kony.boj.partialResetBiller();
    kony.boj.resetDropdownBiller("BillerCategory");
    kony.boj.resetDropdownBiller("BillerName");
    kony.boj.resetDropdownBiller("ServiceType");
    kony.boj.resetDropdownBiller("IDType");
    kony.boj.resetDropdownBiller("Nationality");
    //   if(kony.store.getItem("langPrefObj") === "en")
    //     kony.boj.lang = "eng";
    //   else
    //     kony.boj.lang = "ara";
};
kony.boj.isNextEnabledBiller = function() {
    if ((frmAddNewPayeeKA.tbxBillerCategory.text === "" || frmAddNewPayeeKA.tbxBillerName.text === "" || frmAddNewPayeeKA.tbxServiceType.text === "" || frmAddNewPayeeKA.tbxBillerNumber.text === "" ||
            //frmAddNewPayeeKA.tbxBillerID.text === "" || frmAddNewPayeeKA.tbxAddress.text === "" || 
            //frmAddNewPayeeKA.tbxIDType.text === "" || frmAddNewPayeeKA.tbxNationality.text === "" || 
            frmAddNewPayeeKA.tbxNickName.text === "")) return false;
    return true;
};
kony.boj.isBillerRegistered = function() {
    for (var i in kony.boj.AllPayeeList) {
        if (frmAddNewPayeeKA.tbxBillerNumber.text === kony.boj.AllPayeeList[i].BillingNumber && frmAddNewPayeeKA.tbxBillerName.text === kony.boj.AllPayeeList[i].BillerCode && frmAddNewPayeeKA.tbxServiceType.text === kony.boj.AllPayeeList[i].ServiceType) return true;
    }
    return false;
};
kony.boj.restoreBillerTabDetails = function() {
    var categoryList = [
        ["GetEfwBillerCategory", "selectedBillerCategory", "BillerCategory"],
        ["GetEfwBillerList", "selectedBiller", "BillerName"]
    ];
    //["Nationality", "selectedNationality", "Nationality"],
    //["IDType", "selectedIDType", "IDType"]];
    if (kony.boj.selectedBillerType === "PostPaid") categoryList.push(["GetEfwServiceType", "selectedType", "ServiceType"]);
    else if (kony.boj.selectedBillerType === "PrePaid") {
        categoryList.push(["GetPPServiceType", "selectedType", "ServiceType"]);
        categoryList.push(["Denomination", "selectedDenoType", "Denomination"]);
    }
    for (var i in categoryList) {
        //alert(JSON.stringify());
        if (JSON.stringify(kony.boj.Biller[kony.boj.selectedBillerType][categoryList[i][1]]) != "{}") {
            kony.boj.BillerTabSwitch = true;
            kony.boj.Biller[kony.boj.selectedBillerType].Operation = categoryList[i][0];
            kony.print("---- categoryList ::" + JSON.stringify(kony.boj.Biller[kony.boj.selectedBillerType][categoryList[i][1]]));
            kony.boj.onSegmentClickBiller(kony.boj.Biller[kony.boj.selectedBillerType][categoryList[i][1]]);
        } else kony.boj.resetDropdownBiller(categoryList[i][2]);
    }
};
kony.boj.restoreTextboxAddBiller = function() {
    var details = kony.boj.storedBillerDetail[kony.boj.selectedBillerType];
    for (var i in details) {
        var tbx = "tbx" + i;
        var flx = "flxUnderline" + i;
        var lbl = "lbl" + i;
        frmAddNewPayeeKA[tbx].text = details[i];
        if (details[i] === "") {
            frmAddNewPayeeKA[flx].skin = "sknFlxGreyLine";
            animateLabel("DOWN", lbl, details[i]);
        } else {
            frmAddNewPayeeKA[flx].skin = "sknFlxGreenLine";
            animateLabel("UP", lbl, details[i]);
        }
    }
};
kony.boj.storeDetailsAddBiller = function(name, text) {
    var flx = "flxUnderline" + name;
    kony.boj.storedBillerDetail[kony.boj.selectedBillerType][name] = text;
    if (text === "") frmAddNewPayeeKA[flx].skin = "sknFlxOrangeLine";
    else frmAddNewPayeeKA[flx].skin = "sknFlxGreenLine";
};
kony.boj.editBillerDetails = function(eventobject, context) {
    try {
        //     	gblTModule = "editBiller";
        kony.print("editBillerDetails ::" + JSON.stringify(context.widgetInfo.selectedRowItems[0]));
        kony.boj.selectedPayee = context.widgetInfo.selectedRowItems[0];
        if (kony.boj.selectedPayee.category === "prepaid") gblToOTPFrom = "UpdateBiller"; // hassan edit prepaid biller
        else gblToOTPFrom = "";
        //Clearing Denomination Value
        frmEditPayeeKA.lblDenominationDESC.text = "";
        frmEditPayeeKA.lblDenomination.text = "";
        frmEditPayeeKA.btnEditBillerConfirm.centerY = "25%";
        frmEditPayeeKA.flxDenomination.setVisibility(false);
        //
        frmEditPayeeKA.lblBillerNumber.text = kony.boj.selectedPayee.BillingNumber;
        frmEditPayeeKA.lblServiceType.text = (kony.boj.selectedPayee.category === "prepaid") ? kony.boj.selectedPayee.p_servicetype : kony.boj.selectedPayee.ServiceType;
        frmEditPayeeKA.lblBillerCode.text = kony.boj.selectedPayee.BillerCode;
        frmEditPayeeKA.txtBillerName.text = kony.boj.selectedPayee.NickName;
        frmEditPayeeKA.lblBillingName.text = kony.boj.selectedPayee.billerName;
        frmEditPayeeKA.initialsCategory.text = getInitials(kony.boj.selectedPayee.NickName);
        if (kony.boj.selectedPayee.category === "prepaid" && !isEmpty(kony.boj.selectedPayee.p_denodesc) && !isEmpty(kony.boj.selectedPayee.p_denocode)) {
            frmEditPayeeKA.lblDenominationDESC.text = kony.boj.selectedPayee.p_denodesc;
            frmEditPayeeKA.lblDenomination.text = kony.boj.selectedPayeep_denocode;
            frmEditPayeeKA.btnEditBillerConfirm.centerY = "15%";
            frmEditPayeeKA.flxDenomination.setVisibility(true);
        }
        frmEditPayeeKA.show();
    } catch (e) {
        kony.print("Exception_editBillerDetails ::" + e);
    }
};
kony.boj.confirmEditBillerDetails = function() {
    try {
        var newname = frmEditPayeeKA.txtBillerName.text;
        if (!isEmpty(newname)) {
            var uNN = newname.toUpperCase();
            for (var i in kony.boj.AllPayeeList) {
                if (!isEmpty(kony.boj.AllPayeeList[i].NickName)) {
                    if (uNN == (kony.boj.AllPayeeList[i].NickName).toUpperCase()) {
                        var Message = geti18Value("i18n.common.samebillername");
                        customAlertPopup(geti18Value("i18n.common.alert"), Message, popupCommonAlertDimiss, "", geti18nkey("i18n.settings.ok"), "");
                        return;
                    }
                }
            }
            if (gblToOTPFrom === "UpdateBiller") {
                serv_addNewPrePaidBene();
            } else {
                var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                var options = {
                    "access": "online",
                    "objectName": "RBObjects"
                };
                var headers = {};
                var serviceName = "RBObjects";
                var modelObj = INSTANCE.getModel("Payee", serviceName, options);
                var dataObject = new kony.sdk.dto.DataObject("Payee");
                dataObject.addField("custId", custid);
                dataObject.addField("lang", (kony.store.getItem("langPrefObj") === "en") ? "eng" : "ara");
                dataObject.addField("p_txn_type", "A");
                dataObject.addField("p_BillerCode", frmEditPayeeKA.lblBillerCode.text);
                dataObject.addField("p_ServiceType", frmEditPayeeKA.lblServiceType.text);
                dataObject.addField("p_BillingNo", frmEditPayeeKA.lblBillerNumber.text);
                dataObject.addField("p_NickName", frmEditPayeeKA.txtBillerName.text);
                var serviceOptions = {
                    "dataObject": dataObject,
                    "headers": headers
                };
                if (kony.sdk.isNetworkAvailable()) {
                    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
                    modelObj.customVerb("create", serviceOptions, success_EDITBILLERDETAILS, failure_EDITBILLERDETAILS);
                } else {
                    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
                }
            } // else gblToOTPFrom
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_confirmEditBillerDetails ::" + e);
    }
};

function success_EDITBILLERDETAILS(res) {
    try {
        kony.print("Edit Biller success ::" + JSON.stringify(res));
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        if (res.referenceId === "0" && res.ErrorCode === "00000") {
            kony.boj.selectedPayee.NickName = frmEditPayeeKA.txtBillerName.text;
            kony.boj.populateSuccessScreen("success.png", geti18nkey("i18n.common.billereditsuccess"), "", geti18nkey("i18n.common.CgtAD"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.CgtBL"), "ManagePayee", geti18Value("i18n.common.paynow"), "PayBill");
        } else {
            var Message = getErrorMessage(res.ErrorCode);
            if (res.ErrorCode === "72018" || res.ErrorCode === "72004") {
                Message = geti18Value("i18n.common.somethingwentwrong");
            }
            customAlertPopup(geti18Value("i18n.common.alert"), Message, popupCommonAlertDimiss, "", geti18nkey("i18n.settings.ok"), "");
        }
        //     	gblTModule = "";
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_success_EDITBILLERDETAILS ::" + e);
        //     	gblTModule = "";
    }
}

function failure_EDITBILLERDETAILS(err) {
    try {
        //     	gblTModule = "";
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
        kony.sdk.mvvm.log.error(err.toString());
        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.CgtBL"), "ManagePayee");
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_failure_EDITBILLERDETAILS ::" + e);
        //     	gblTModule = "";
    }
}

function serv_ADD_BILLER() {
    try {
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Payee", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Payee");
        dataObject.addField("lang", frmAddNewPayeeKA.lblLanguage.text);
        dataObject.addField("p_txn_type", frmAddNewPayeeKA.lblType.text);
        dataObject.addField("p_BillerCode", frmAddNewPayeeKA.tbxBillerName.text);
        dataObject.addField("p_ServiceType", frmAddNewPayeeKA.tbxServiceType.text);
        dataObject.addField("p_BillingNo", frmAddNewPayeeKA.tbxBillerNumber.text);
        dataObject.addField("p_IdType", frmAddNewPayeeKA.tbxIDType.text);
        dataObject.addField("p_ID", frmAddNewPayeeKA.tbxBillerID.text);
        dataObject.addField("p_Nation", frmAddNewPayeeKA.tbxNationality.text);
        dataObject.addField("p_Phone", frmAddNewPayeeKA.tbxPhoneNo.text);
        dataObject.addField("p_Address1", frmAddNewPayeeKA.tbxAddress.text);
        dataObject.addField("p_NickName", frmAddNewPayeeKA.tbxNickName.text);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        if (kony.sdk.isNetworkAvailable()) {
            modelObj.customVerb("create", serviceOptions, success_serv_ADD_BILLER, failed_serv_ADD_BILLER);
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_serv_ADD_BILLER ::" + e);
    }
}

function success_serv_ADD_BILLER(res) {
    try {
        kony.print("Add biller success ::" + JSON.stringify(res));
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        if (res.referenceId === "0" && res.ErrorCode === "00000") { //geti18nkey("i18n.common.ReferenceNumber")+" " + res.p_EfawateercomID
            kony.boj.populateSuccessScreen("success.png", geti18nkey("i18n.common.billeraddsucc"), "", geti18nkey("i18n.common.CgtAD"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.CgtBL"), "ManagePayee", geti18Value("i18n.common.paynow"), "PayBill");
        } else if (res.ErrorCode === "00444") {
            kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.AddBene.BeneExist"), geti18nkey("i18n.common.CgtAD"), geti18nkey("i18n.common.AccountDashboard"), "ManagePayee", geti18Value("i18n.common.paynow"), "PayBill");
        } else {
            var Message = getErrorMessage(res.ErrorCode);
            if (res.ErrorCode === "72018" || res.ErrorCode === "72004") {
                Message = geti18Value("i18n.common.somethingwentwrong");
            }
            customAlertPopup(geti18Value("i18n.common.alert"), Message, popupCommonAlertDimiss, "", geti18nkey("i18n.settings.ok"), "");
            frmAddNewPayeeKA.flxConfirmPopUp.isVisible = false;
            frmAddNewPayeeKA.flxFormMain.isVisible = true;
            frmAddNewPayeeKA.show();
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_success_serv_ADD_BILLER ::" + e);
    }
}

function failed_serv_ADD_BILLER(err) {
    try {
        kony.print("error in AddBiller\n" + JSON.stringify(err));
        gblTModule = "";
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
        var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
        kony.sdk.mvvm.log.error(exception.toString());
        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.CgtBL"), "ManagePayee");
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_failed_serv_ADD_BILLER ::" + e);
    }
}