function getstatistics() {
    try {
        if (kony.sdk.isNetworkAvailable()) {
            kony.application.showLoadingScreen(null, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
            var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
            var date = new Date();
            var day = date.getDate();
            var langselected = (kony.store.getItem("langPrefObj") === "en" ? "eng" : "ara");
            if (day < 10) {
                day = "0" + day;
            }
            var mon = date.getMonth() + 1;
            if (mon < 10) {
                mon = "0" + mon;
            }
            var year = date.getFullYear();
            var currDate = year + "-" + mon + "-" + day;
            //day = "01";
            var prevDate = year + "-" + mon + "-" + day;
            var queryParams = {
                "custid": "",
                "loop_count": "" + parseInt(fromAccounts.length),
                "loop_seperator": ":",
                "p_Account": "",
                "p_Branch": "",
                "p_toDate": "",
                "lang": ""
            };
            for (var i = 0; i < fromAccounts.length; i++) {
                queryParams.custid = queryParams.custid + (i == fromAccounts.length - 1 ? custid : (custid + ":"));
                queryParams.p_Account = queryParams.p_Account + (i == fromAccounts.length - 1 ? fromAccounts[i].accountID : (fromAccounts[i].accountID + ":"));
                queryParams.p_Branch = queryParams.p_Branch + (i == fromAccounts.length - 1 ? fromAccounts[i].branchNumber : (fromAccounts[i].branchNumber + ":"));
                queryParams.lang = queryParams.lang + (i == fromAccounts.length - 1 ? langselected : (langselected + ":"));
                queryParams.p_toDate = queryParams.p_toDate + (i == fromAccounts.length - 1 ? prevDate : (prevDate + ":"));
            }
            kony.print("getstatistics Query Params:: " + JSON.stringify(queryParams));
            var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("LoopStatistics");
            appMFConfiguration.invokeOperation("LoopStatsService", {}, queryParams, loopingSuccessCallback, loopingErrorCallback);
        } else {
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("In Exception of Get Statistics::" + e);
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function loopingErrorCallback(err) {
    frmMyMoneyListKA.lblNoChart.setVisibility(true);
    kony.application.dismissLoadingScreen();
}

function loopingSuccessCallback(response) {
    try {
        kony.print("loopingSuccessCallback getstatistics ::" + JSON.stringify(response));
        var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
        var len = response.LoopDataset[0].StatsCollection.length; //Total Transactions
        var lineCount = 0; //Lines on  graph
        var i = 0,
            j = 0,
            k = 0,
            l = 0,
            m = 0;
        var index = [];
        var accountType = [];
        var chartData = {
            "amount": []
        };
        var tranDate = {
            "date": []
        };
        var date = new Date();
        var day = date.getDate();
        if (day < 10) {
            day = "0" + day;
        }
        for (i = 0; i < len; i++) {
            var amount = [];
            var trdate = [];
            var count = 0;
            if (response.LoopDataset[0].StatsCollection[i] !== undefined) {
                index.push(i);
                for (j in response.LoopDataset[0].StatsCollection[i]) {
                    if ((count > 0) && (response.LoopDataset[0].StatsCollection[i].transactiondate === trdate[count - 1])) {
                        amount[count - 1] = parseInt(amount[count - 1]) + parseInt(response.LoopDataset[0].StatsCollection[i].transactionamount);
                    } else {
                        amount[count] = parseInt(response.LoopDataset[0].StatsCollection[i].transactionamount);
                        trdate[count] = response.LoopDataset[0].StatsCollection[i].transactiondate;
                        count++;
                    }
                }
                var splitteddate = [];
                var trdatecount = trdate.length;
                var tempamount = [];
                m = 0;
                for (l = 0; l < trdatecount; l++) {
                    var splitdate = trdate[l].split("-");
                    splitteddate.push(parseInt(splitdate[2]));
                    kony.print("Here in BHu splitdate:: " + splitdate);
                }
                for (k = 1; k <= parseInt(day); k++) {
                    if ((splitteddate[m]) !== k) {
                        tempamount.push(0);
                    } else {
                        tempamount.push(parseInt(amount[m]));
                        m++;
                    }
                    kony.print("Here in BHu tempamount:: " + tempamount);
                }
                kony.print("Amount::" + amount);
                kony.print("Temp Amount::" + tempamount);
                kony.print("Date::" + splitteddate);
                kony.print("Amount Length::" + tempamount.length);
                chartData.amount[lineCount] = tempamount;
                tranDate.date[lineCount] = trdate;
                lineCount++;
            }
        }
        var indexlen = index.length;
        if (index.length !== 0 && index !== null && index !== "") {
            j = 0;
            for (i = 0; i < fromAccounts.length; i++) {
                if (i === index[j] && j < indexlen) {
                    accountType.push(fromAccounts[i].accountName);
                    j++;
                }
            }
            kony.print("accountType.length" + accountType.length);
            var rowData = {};
            // rowData[0]=accountType[0]+":"+JSON.stringify(chartData.amount[0]);
            for (i = 0; i < accountType.length; i++) {
                //rowData[i]=accountType[i] +": "+JSON.stringify(chartData.amount[i]);
                rowData[accountType[i]] = chartData.amount[i];
            }
            kony.print("ROW DATA::" + rowData);
            var data = {
                "rowNames": {
                    "values": ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
                },
                "columnNames": {
                    "values": accountType
                        //["Deposit", "Cash", "Credit", "Target", "Achieved"]
                },
                "data": rowData
                    //    {     
                    //                     "Current Account": ["234", 236, 224, 244, 240, 218, 256,234, 236, 224, 244, 240, 218, 256,234, 236, 224, 244, 240, 218, 256,234, 236, 224, 244, 240, 218, 256,234, 236, 224],
                    //                    "Current Account": [700, 512, 514, 526, 534, 536, 522],
                    //                    "Current Account": [344, 335, 336, 334, 332, 324, 822]
                    //                                     }
            };
            if (index.length !== undefined && index !== null && index !== "") {
                frmMyMoneyListKA.lblNoChart.setVisibility(false);
                var linedata = {};
                frmMyMoneyListKA.flxLineChart.removeAll();
                linedata = line_createChartWidget(data, accountType);
                kony.print("Amount Data::" + JSON.stringify(chartData));
                kony.print("Transaction Date::" + JSON.stringify(tranDate));
                kony.print("Line Count::" + lineCount);
                kony.print("Indexex::" + index);
                kony.print("accountType::" + accountType);
                kony.application.dismissLoadingScreen();
                //frmMyMoneyListKA.flxLineChart.remove(linedata);
                frmMyMoneyListKA.flxLineChart.add(linedata);
                //frmMyMoneyListKA.show();
            } else {
                frmMyMoneyListKA.flxLineChart.removeAll();
                frmMyMoneyListKA.lblNoChart.setVisibility(true);
            }
        } else {
            kony.application.dismissLoadingScreen();
            frmMyMoneyListKA.flxLineChart.removeAll();
            frmMyMoneyListKA.lblNoChart.setVisibility(true);
        }
    } catch (e) {
        frmMyMoneyListKA.lblNoChart.setVisibility(true);
        kony.application.dismissLoadingScreen();
        kony.print("In Exception get statistics success::" + e);
    }
}

function gesture_pieChartSwipe() {
    frmMyMoneyListKA.flxPieChart.removeGestureRecognizer(2);
    var setupTblTap = {
        fingers: 1,
        swipedistance: 60,
        swipevelocity: 60
    };
    frmMyMoneyListKA.flxPieChart.setGestureRecognizer(2, setupTblTap, gesture_pieChart);

    function gesture_pieChart(myWidget, gestureInfo) {
        if (gestureInfo.swipeDirection === 1) {
            if (customerAccountDetails.currentIndex < kony.retailBanking.globalData.accountsDashboardData.accountsData.length - 1) {
                customerAccountDetails.currentIndex = customerAccountDetails.currentIndex + 1;
                serv_getStatistics();
            }
        } else if (gestureInfo.swipeDirection === 2) {
            if (customerAccountDetails.currentIndex > 0) {
                customerAccountDetails.currentIndex = customerAccountDetails.currentIndex - 1;
                serv_getStatistics();
            }
        }
    }
}