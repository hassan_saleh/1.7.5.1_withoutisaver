/*
 * Controller Extension class for frmAccountsLandingKA
 * Developer can edit the existing methods or can add new methods if required
 *
 */
kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};
kony.sdk.mvvm.frmAccountsLandingKAControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    fetchData: function() {
        try {
            var scopeObj = this;
            scopeObjgbl = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            /* if(kony.retailBanking.globalData.globals.DoReloadEditAccountInfo){
                       kony.retailBanking.globalData.globals.DoReloadEditAccountInfo = false;
                       this.$class.$superp.fetchData.call(this, success, error);
                   }else{
                      var formmodel = this.getController().getFormModel();
                      formmodel.showView();
                   }*/
            kony.retailBanking.globalData.globals.DoReloadEditAccountInfo = false;
            kony.print("Perf Log: Account fetch service call - start");
            this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            exceptionLogCall("frmAccountsLandingKAControllerExtension:FetchData", "UI ERROR", "UI", err);
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
            kony.print("Perf Log: Account fetch service call - End");
            kony.sdk.mvvm.log.info("success fetching data ", response);
            kony.print("before responseresponse:::::::" + JSON.stringify(response));
            var resData = JSON.stringify(response._raw_response_.segAccountsKA.records);
            response = {};
            response = {
                segAccountsKA: {}
            };
            response.segAccountsKA = JSON.parse(resData);
            kony.print("responseresponse:::::::" + JSON.stringify(response));
            //fetchAllCompanies();
            var currentdate = new Date();
            var time = currentdate.timeNow();
            kony.retailBanking.globalData.globals.refreshTimeLabel = i18n_asOfToday + " - " + time;
            var navigationObject = this.getController() && this.getController().getContextData();
            navigationObject.setCustomInfo("segAccountsKA", response.segAccountsKA);
            var Cash = 0,
                CreditDebt = 0;
            var tempResponse = response.segAccountsKA;
            for (var i = 0; i < tempResponse.length; i++) {
                if ((!isEmpty(tempResponse)) && tempResponse.length > 0) {
                    //kony.print("Account number  :: " + tempResponse[i]["accountID"]);
                    if (checkAccount(tempResponse[i]["accountID"])) {} else {
                        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                        customAlertPopup(geti18Value("i18n.common.Attention"), geti18nkey("i18n.security.loginagain"), logoutpp, "");
                        return;
                    }
                }
                if (tempResponse[i]["accountType"] == kony.retailBanking.globalData.globals.Savings || tempResponse[i]["accountType"] == kony.retailBanking.globalData.globals.Checking) {
                    Cash = Cash + parseFloat(tempResponse[i]["availableBalance"]);
                } else if (tempResponse[i]["accountType"] == kony.retailBanking.globalData.globals.CreditCard /*||tempResponse[i]["accountType"]==kony.retailBanking.globalData.globals.Mortgage*/ ) {
                    if (tempResponse[i]["currentBalance"]) CreditDebt = CreditDebt + parseFloat(tempResponse[i]["currentBalance"]);
                }
            }
            Cash = Cash.toFixed(2);
            CreditDebt = CreditDebt.toFixed(2);
            frmAccountsLandingKA.cashOverviewLabel.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(Cash);
            frmAccountsLandingKA.creditDebtOverviewLabel.text = "-" + kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(CreditDebt);
            kony.retailBanking.globalData.accounts.setAccountsData(response.segAccountsKA);
            var accountsRawData = response.segAccountsKA;
            var accountsFilteredData = [];
            if (accountsRawData !== undefined && accountsRawData !== null && accountsRawData.length > 0) {
                for (var z in accountsRawData) {
                    if ((accountsRawData[z]["accountType"] == "S" || accountsRawData[z]["accountType"] == "C" || accountsRawData[z]["accountType"] == "Y" || accountsRawData[z]["accountType"] == "CC" || accountsRawData[z]["accountType"] == "B") && accountsRawData[z]["productCode"] != "320") {
                        if (isEmpty(accountsRawData[z].AccHideShowFlag)) {
                            accountsFilteredData.push(accountsRawData[z]);
                        } else {
                            if (accountsRawData[z].AccHideShowFlag === "T" || accountsRawData[z].AccHideShowFlag === "Y") accountsFilteredData.push(accountsRawData[z]);
                        }
                    }
                }
                if (accountsFilteredData !== undefined && accountsFilteredData !== null && accountsFilteredData.length > 0) {
                    var requestObj = {
                        "custId": "",
                        "loop_count": parseInt(accountsFilteredData.length),
                        "loop_seperator": ":",
                        "accountNumber": "",
                        "p_Branch": "",
                        "p_no_Of_Txn": "",
                        "lang": ""
                    };
                    var language_SELECTED = kony.store.getItem("langPrefObj") === "en" ? "eng" : "ara";
                    for (var j in accountsFilteredData) {
                        requestObj.custId = requestObj.custId + (j === accountsFilteredData.length - 1 ? custid : (custid + ":"));
                        requestObj.accountNumber = requestObj.accountNumber + (j === accountsFilteredData.length - 1 ? accountsFilteredData[j].accountID : (accountsFilteredData[j].accountID + ":"));
                        requestObj.p_Branch = requestObj.p_Branch + (j === accountsFilteredData.length - 1 ? accountsFilteredData[j].branchNumber : (accountsFilteredData[j].branchNumber + ":"));
                        requestObj.lang = requestObj.lang + (j === accountsFilteredData.length - 1 ? language_SELECTED : (language_SELECTED + ":"));
                        requestObj.p_no_Of_Txn = requestObj.p_no_Of_Txn + (j === accountsFilteredData.length - 1 ? "2" : ("2" + ":"));
                    }
                    scopeObjgblResponse = response;
                    getAccountTransactions(requestObj, this, response);
                    //scopeObj.getController().processData(response);
                } else {
                    kony.print("No data11::::");
                    scopeObj.getController().processData(response);
                }
            } else {
                kony.print("No data22::::");
                scopeObj.getController().processData(response);
            }
            //scopeObj.bindData(formattedResponse);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.print("errerr ::" + JSON.stringify(err));
            var errorCode = "";
            if (err.code !== null) {
                errorCode = err.code;
            } else if (err.opstatus !== null) {
                errorCode = err.opstatus;
            } else if (err.errorCode !== null) {
                errorCode = err.errorCode;
            }
            var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
            customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    processData: function(data) {
        try {
            kony.print("Inside processData start::::");
            var processedAccountsData = this.getController().performAction("processAccounts", [data, "segAccountsKA"]);
            kony.print("processedAccountsData ::" + JSON.stringify(processedAccountsData));
            data.segAccountsKA = processedAccountsData[0] || [];
            data.segAccountsKADeals = processedAccountsData[1] || [];
            data.segAccountsKALoans = processedAccountsData[2] || [];
            var navigationObject = this.getController() && this.getController().getContextData();
            navigationObject.setCustomInfo("segAccountsKA", processedAccountsData[0] || []);
            navigationObject.setCustomInfo("segAccountsKADeals", processedAccountsData[1] || []);
            navigationObject.setCustomInfo("segAccountsKALoans", processedAccountsData[2] || []);
            var processedData = this.$class.$superp.processData.call(this, data);
            this.getController().bindData(processedData);
            return processedData;
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            exceptionLogCall("frmAccountsLandingKAControllerExtension:ProcessData", "UI ERROR", "UI", err);
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    bindData: function(data) {
        try {
            kony.print("bindData:::::::" + JSON.stringify(data));
            if (!kony.retailBanking.AreaChartGenereted) {
                //fetchAllTransactions();   Commentes
            }
            var formmodel = this.getController().getFormModel();
            formmodel.clear();
            formmodel.setViewAttributeByProperty("flxAccountsMain", "isVisible", true);
            formmodel.setViewAttributeByProperty("flxDeals", "isVisible", false);
            formmodel.setViewAttributeByProperty("flxLoans", "isVisible", false);
            if (data["segAccountsKA"]["segAccountsKA"].getData() && data["segAccountsKA"]["segAccountsKA"].getData().length > 0) {
                kony.print("Inside Data:")
                formmodel.setViewAttributeByProperty("segAccountsKA", "isVisible", true);
                formmodel.setViewAttributeByProperty("LabelNoRecordsKA", "isVisible", false);
                if (gblNoThanksClickSaveDatatoQB) {
                    callSaveDataToQuickAccessAccountListDB();
                    gblNoThanksClickSaveDatatoQB = false;
                }
            } else {
                formmodel.setViewAttributeByProperty("segAccountsKA", "isVisible", false);
                formmodel.setViewAttributeByProperty("LabelNoRecordsKA", "isVisible", true);
                formmodel.setViewAttributeByProperty('LabelNoRecordsKA', "text", geti18nkey("i18n.alerts.NoAccountsMsg"));
            }
            if (data["segAccountsKADeals"]["segAccountsKADeals"].getData() && data["segAccountsKADeals"]["segAccountsKADeals"].getData().length > 0) {
                formmodel.setViewAttributeByProperty("segAccountsKADeals", "isVisible", true);
                //  formmodel.setViewAttributeByProperty("btnNewDeposit", "isVisible", true);// hassan open term deposit
                // formmodel.setViewAttributeByProperty("LabelNoRecordsKADeals", "isVisible", false);/ hassan open term deposit
                //    formmodel.setViewAttributeByProperty("flxNoDeposit", "isVisible", false);// hassan open term deposit
            } else {
                formmodel.setViewAttributeByProperty("segAccountsKADeals", "isVisible", false);
                //   formmodel.setViewAttributeByProperty("btnNewDeposit", "isVisible", false);// hassan open term deposit
                //formmodel.setViewAttributeByProperty("LabelNoRecordsKADeals", "isVisible", true);// hassan open term deposit
                formmodel.setViewAttributeByProperty('LabelNoRecordsKADeals', "text", geti18nkey("i18n.alerts.NoDepositsMsg"));
                //    formmodel.setViewAttributeByProperty("flxNoDeposit", "isVisible", true);// hassan open term deposit
            }
            if (data["segAccountsKALoans"]["segAccountsKALoans"].getData() && data["segAccountsKALoans"]["segAccountsKALoans"].getData().length > 0) {
                formmodel.setViewAttributeByProperty("segAccountsKALoans", "isVisible", true);
                formmodel.setViewAttributeByProperty("LabelNoRecordsKALoans", "isVisible", false);
            } else {
                formmodel.setViewAttributeByProperty("segAccountsKALoans", "isVisible", false);
                formmodel.setViewAttributeByProperty("LabelNoRecordsKALoans", "isVisible", true);
                formmodel.setViewAttributeByProperty('LabelNoRecordsKALoans', "text", geti18nkey("i18n.alerts.NoLoansMsg"));
            }
            if ((isEmpty(data["segAccountsKA"]["segAccountsKA"].getData())) && (isEmpty(data["segAccountsKADeals"]["segAccountsKADeals"].getData())) && (isEmpty(data["segAccountsKALoans"]["segAccountsKALoans"].getData()))) {
                set_CASA_ACCOUNT_LOGIN();
                return;
            }
            var navigationObject = this.getController() && this.getController().getContextData();
            var accData = navigationObject.getCustomInfo("segAccountsKA");
            kony.print("accData segAccountsKA:::::::" + JSON.stringify(accData));
            data["segAccountsKA"]["segAccountsKA"].setData(accData);
            var widgetSetData = formmodel.getViewAttributeByProperty("segAccountsKA", "setData");
            widgetSetData.setData = accData;
            formmodel.setViewAttributeByProperty('segAccountsKA', "setData", widgetSetData);
            formmodel.setViewAttributeByProperty('btnAccounts', "text", geti18nkey("i18n.accounts.accounts"));
            formmodel.setViewAttributeByProperty('btnDeals', "text", geti18nkey("i18n.accounts.deals"));
            formmodel.setViewAttributeByProperty('btnLoans', "text", geti18nkey("i18n.accounts.loans"));
            formmodel.setViewAttributeByProperty('btnAccounts1', "text", geti18nkey("i18n.accounts.accounts"));
            formmodel.setViewAttributeByProperty('btnDeals1', "text", geti18nkey("i18n.accounts.deals"));
            formmodel.setViewAttributeByProperty('btnLoans1', "text", geti18nkey("i18n.accounts.loans"));
            formmodel.setViewAttributeByProperty('btnAccounts2', "text", geti18nkey("i18n.accounts.accounts"));
            formmodel.setViewAttributeByProperty('btnDeals2', "text", geti18nkey("i18n.accounts.deals"));
            formmodel.setViewAttributeByProperty('btnLoans2', "text", geti18nkey("i18n.accounts.loans"));
            var widgetDataMap = formmodel.getViewAttributeByProperty("segAccountsKA", "widgetDataMap");
            widgetDataMap.amountAccount1 = "availableBalance";
            widgetDataMap.nameAccount1 = "nickName";
            widgetDataMap.lblTransDate = "lblTransDate";
            widgetDataMap.lblTransName = "lblTransName";
            widgetDataMap.lblTransAmount = "lblTransAmount";
            widgetDataMap.lblTransId = "lblTransId";
            widgetDataMap.lblNoData = "lblNoData";
            widgetDataMap.lblMobile = "lblMobile";
            widgetDataMap.flxCardless = "flxCardless";
            widgetDataMap.flxISaver = "flxISaver";
            widgetDataMap.nameContainer = "nameContainer";
            hideSortpopupinAccountsDetailsScreen();
            formmodel.setViewAttributeByProperty('segAccountsKA', "widgetDataMap", widgetDataMap);
            this.$class.$superp.bindData.call(this, data);
            this.getController().getFormModel().formatUI();
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            if (data["segAccountsKA"]["segAccountsKA"].getData() && data["segAccountsKA"]["segAccountsKA"].getData().length === 1) {
                kony.print("Only one account:");
                var context = {
                    "sectionIndex": 0,
                    "rowIndex": 0,
                    "widgetInfo": {
                        "id": "segAccountsKA"
                    }
                };
                kony.print("accountDashboardFlowFlag" + kony.retailBanking.globalData.globals.accountDashboardFlowFlag);
                if (kony.retailBanking.globalData.globals.accountDashboardFlowFlag) {
                    onheaderDashboard(frmAccountsLandingKA.segAccountsKA.data, context);
                } else {
                    kony.print("showView account:");
                    formmodel.showView();
                }
            } else {
                formmodel.showView();
                kony.print("all accounts");
            }
            // Start InfeedAds
            //var accountsLandingVC = new kony.rb.frmAccountsLandingKAViewController();	
            /*accountsLandingVC = new kony.rb.frmAccountsLandingKAViewController();
                		accountsLandingVC.bindInFeedAds();  */ //Commented
            // End Infeed Ads
            kony.print("Perf Log: account prview dashboard- End");
            if (kony.retailBanking.globalData.deviceInfo.isIphone()) {
                // registerForPeekAndPop();  // commented
            }
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            exceptionLogCall("frmAccountsLandingKAControllerExtension:BindData", "UI ERROR", "UI", err);
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    saveData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.saveData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully created record
            kony.sdk.mvvm.log.info("success saving record ", res);
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    navigateTo: function(formId, navObject) {
        this.$class.$superp.navigateTo.call(this, formId, navObject);
    },
    navigateToAccountsDetails: function() {
        navigateToDetails("frmAccountsLandingKA", "frmAccountDetailKA", "segAccountsKA");
    },
    processAccounts: function(data, segment) {
        kony.print("processAccounts:::::11" + JSON.stringify(data[segment]));
        kony.retailBanking.globalData.acccountsUnfilteredData = [];
        kony.print("transaction processAccounts-----------------dtat" + JSON.stringify(kony.retailBanking.globalData.accountsTransactionData));
        try {
            var segAccountListData = data[segment];
            var accountsData = [],
                dealsData = [],
                loansData = [];
            var accountPartitionData = {};
            var sectionAccData = [],
                segDealsData = [],
                segLoansData = [];
            var jomoImg = "";
            frmAccountInfoKA.flxOrderCheckBook.setVisibility(false);
            frmOrderChequeBookConfirm.lblAccountNumber.text = "";
            if ((!isEmpty(segAccountListData)) && segAccountListData.length > 0) {
                for (var i in segAccountListData) {
                    if ((segAccountListData[i]["accountType"] == "S" || segAccountListData[i]["accountType"] == "C" || segAccountListData[i]["accountType"] == "Y" || segAccountListData[i]["accountType"] == "CC" || segAccountListData[i]["accountType"] == "B") && segAccountListData[i]["productCode"] != "320") {
                        segAccountListData[i]["availableBalance"] = formatamountwithCurrency(segAccountListData[i]["availableBalance"], segAccountListData[i]["currencyCode"]);
                        segAccountListData[i]["currentBalance"] = formatamountwithCurrency(segAccountListData[i]["currentBalance"], segAccountListData[i]["currencyCode"]);
                        segAccountListData[i]["outstandingBalance"] = formatamountwithCurrency(segAccountListData[i]["outstandingBalance"], segAccountListData[i]["currencyCode"]);
                        if (segAccountListData[i]["accountType"] == "C") {
                            frmAccountInfoKA.flxOrderCheckBook.setVisibility(true);
                        }
                        kony.retailBanking.globalData.acccountsUnfilteredData.push(segAccountListData[i]);
                        if (isEmpty(segAccountListData[i].AccHideShowFlag)) {
                            accountsData.push(segAccountListData[i]);
                        } else {
                            if (segAccountListData[i].AccHideShowFlag === "T" || segAccountListData[i].AccHideShowFlag === "Y") accountsData.push(segAccountListData[i]);
                        }
                    }
                    if (segAccountListData[i]["accountType"] == "T") {
                        segAccountListData[i]["availableBalance"] = formatamountwithCurrency(segAccountListData[i]["availableBalance"], segAccountListData[i]["currencyCode"]);
                        segAccountListData[i]["currentBalance"] = formatamountwithCurrency(segAccountListData[i]["currentBalance"], segAccountListData[i]["currencyCode"]);
                        segAccountListData[i]["outstandingBalance"] = formatamountwithCurrency(segAccountListData[i]["outstandingBalance"], segAccountListData[i]["currencyCode"]);
                        dealsData.push(segAccountListData[i]);
                    }
                    if (segAccountListData[i]["accountType"] == "L") {
                        segAccountListData[i]["availableBalance"] = formatamountwithCurrency(segAccountListData[i]["availableBalance"], segAccountListData[i]["currencyCode"]);
                        segAccountListData[i]["currentBalance"] = formatamountwithCurrency(segAccountListData[i]["currentBalance"], segAccountListData[i]["currencyCode"]);
                        segAccountListData[i]["outstandingBalance"] = formatamountwithCurrency(segAccountListData[i]["outstandingBalance"], segAccountListData[i]["currencyCode"]);
                        loansData.push(segAccountListData[i]);
                    }
                }
                kony.print("accountsData:::" + JSON.stringify(accountsData));
                kony.print("dealsData:::" + JSON.stringify(dealsData));
                kony.print("loansData:::" + JSON.stringify(loansData));
                accountPartitionData.accountsData = isEmpty(accountsData) ? {} : accountsData;
                accountPartitionData.dealsData = isEmpty(dealsData) ? {} : dealsData;
                accountPartitionData.loansData = isEmpty(loansData) ? {} : loansData;
                kony.retailBanking.globalData.accountsDashboardData = accountPartitionData;
                var langSelected1 = kony.store.getItem("langPrefObj");
                //Accounts-----------------------------------------------------------------------------------------------
                if (accountsData && accountsData.length > 0) {
                    kony.print("Inside accounts:::");
                    for (var i in accountsData) {
                        var segformat = [];
                        var segRowData = [];
                        var aHeader = {};
                        var availableBal, currBal, outstandingBal, accType;
                        var accountNumber = accountsData[i]["accountID"];
                        var ttt = "";
                        if (isEmpty(accountsData[i]["AccNickName"])) {
                            aHeader["nickName"] = accountsData[i]["accountName"];
                            if (aHeader["nickName"] !== null && aHeader["nickName"] !== "") {
                                if (aHeader["nickName"].length > 25) {
                                    aHeader["nickName"] = aHeader["nickName"].substring(0, 20) + "  ...";
                                }
                                ttt = aHeader["nickName"] + " " + accountNumber;
                                kony.print("qqqqqq:" + ttt);
                            }
                        } else {
                            aHeader["nickName"] = accountsData[i]["AccNickName"];
                            if (aHeader["nickName"] !== null && aHeader["nickName"] !== "") {
                                if (aHeader["nickName"].length > 35) aHeader["nickName"] = aHeader["nickName"].substring(0, 33) + "  ...";
                                ttt = aHeader["nickName"];
                            }
                        }
                        if (accountsData[i]["availableBalance"]) {
                            availableBal = accountsData[i]["availableBalance"];
                        }
                        if (accountsData[i]["currentBalance"]) {
                            currBal = accountsData[i]["currentBalance"];
                        }
                        if (accountsData[i]["outstandingBalance"]) {
                            outstandingBal = accountsData[i]["outstandingBalance"];
                        }
                        if (accountsData[i]["accountType"] == "C") {
                            currBal = "-" + currBal;
                        }
                        if (accountsData[i]["isjomopayacc"] == "N") {
                            jomoImg = "";
                        } else if (accountsData[i]["isjomopayacc"] == "Y") {
                            jomoImg = '<img align = "left" src="mobicon.png" />';
                        }
                        aHeader["currentBalance"] = currBal;
                        aHeader["currencyCode"] = accountsData[i]["currencyCode"];
                        if (langSelected1 === "ar" || langSelected1 === "ara") {
                            var taxtData = jomoImg + "  " + '<label>' + ttt.toString() + '</label>';
                            aHeader["nickName"] = {
                                "text": taxtData.toString()
                            }
                            aHeader["availableBalance"] = aHeader["currencyCode"] + " " + availableBal;
                        } else {
                            var taxtData = '<label>' + ttt.toString() + '</label>' + "  " + jomoImg;
                            aHeader["nickName"] = {
                                "text": taxtData.toString()
                            };
                            aHeader["availableBalance"] = availableBal + " " + aHeader["currencyCode"];
                        }
                        //Omar ALnajjar  27/1/2021
                        //Formatting cardless icon in accounts landing screen
                        aHeader.flxCardless = {
                            isVisible: false
                        };
                        // aHeader.flxISaver = {isVisible:false};
                        aHeader.nameContainer = {
                            width: "85%"
                        };
                        if (accountsData[i]["currencyCode"] === CURRENCY) {
                            aHeader.flxCardless = {
                                isVisible: true
                            };
                            aHeader.nameContainer = {
                                width: "71%"
                            };
                        }
                        //                       if(accountsData[i]["producttype"] === "441"){
                        //                         	 aHeader.flxISaver = {isVisible:true};
                        //                              aHeader.nameContainer = {width:"57%"};
                        //                       }
                        //
                        aHeader["outstandingBalance"] = outstandingBal;
                        if (accountsData[i]["accountType"] == "C") {
                            accType = "Current account";
                        } else if (accountsData[i]["accountType"] == "S") {
                            accType = "Saving account";
                        } else if (accountsData[i]["accountType"] == "Y") {
                            accType = "Corporate account";
                        }
                        aHeader["accountType"] = accType;
                        segformat.push(aHeader);
                        kony.print("i " + i);
                        if (kony.retailBanking.globalData.accountsTransactionData !== null && kony.retailBanking.globalData.accountsTransactionData !== undefined && kony.retailBanking.globalData.accountsTransactionData.length > 0) {
                            if (kony.retailBanking.globalData.accountsTransactionData[i].Transactions !== null && kony.retailBanking.globalData.accountsTransactionData[i].Transactions !== undefined) {
                                var acctransaction = kony.retailBanking.globalData.accountsTransactionData[i].Transactions;
                                kony.print("acctransaction::::::" + acctransaction);
                                for (var j = 0; j < acctransaction.length; j++) {
                                    var tranSym = (acctransaction[j].category === "C") ? "+ " : "- "
                                    var dateFormat = isEmpty(acctransaction[j].transactionDate) ? "" : getShortMonthDateFormat(acctransaction[j].transactionDate);
                                    var transDesc = "";
                                    if (acctransaction[j].description !== null && acctransaction[j].description !== "") {
                                        transDesc = acctransaction[j].description;
                                        transDesc = transDesc.substring(0, 30) + "  .  .  .";
                                    }
                                    var tranAmoount = formatamountwithCurrency(acctransaction[j].amount, acctransaction[j]["currencycode"]);
                                    if (langSelected1 === "ar" || langSelected1 === "ara") {
                                        tranAmoount = acctransaction[j].currencycode + " " + tranAmoount + tranSym;
                                    } else {
                                        tranAmoount = tranSym + tranAmoount + " " + acctransaction[j].currencycode;
                                    }
                                    segRowData.push({
                                        lblTransId: {
                                            "text": acctransaction[j].transactionId,
                                            "isVisible": false
                                        },
                                        lblTransDate: {
                                            "text": dateFormat,
                                            "isVisible": true
                                        },
                                        lblTransName: {
                                            "text": transDesc, //acctransaction[j].description,
                                            "isVisible": true
                                        },
                                        lblTransAmount: {
                                            "text": tranAmoount,
                                            "isVisible": true
                                        },
                                        lblNoData: {
                                            "text": geti18Value("i18n.accounts.noTransaction"),
                                            "isVisible": false
                                        },
                                    });
                                }
                            } else {
                                kony.print("No Transaction ----->")
                                segRowData.push({
                                    lblTransId: {
                                        "isVisible": false
                                    },
                                    lblTransDate: {
                                        "isVisible": false
                                    },
                                    lblTransName: {
                                        "isVisible": false
                                    },
                                    lblTransAmount: {
                                        "isVisible": false
                                    },
                                    lblNoData: {
                                        "text": geti18Value("i18n.accounts.noTransaction"),
                                        "left": "15Dp",
                                        "isVisible": true
                                    },
                                });
                            }
                        } else {
                            kony.print("No Transaction -----temp")
                            segRowData.push({
                                lblTransId: {
                                    "isVisible": false
                                },
                                lblTransDate: {
                                    "isVisible": false
                                },
                                lblTransName: {
                                    "isVisible": false
                                },
                                lblTransAmount: {
                                    "isVisible": false
                                },
                                lblNoData: {
                                    "text": geti18Value("i18n.accounts.noTransaction"),
                                    "left": "15Dp",
                                    "isVisible": true
                                },
                            });
                        }
                        segformat.push(segRowData);
                        sectionAccData.push(segformat);
                    }
                    kony.print("sectionAccData:::" + JSON.stringify(sectionAccData));
                }
                //Deals-----------------------------------------------------------------------------------------------
                if ((!isEmpty(dealsData)) && dealsData.length > 0) {
                    kony.print("Inside Deals section:::");
                    for (var i in dealsData) {
                        var segformat = [];
                        var segRowData = [];
                        var aHeader = {};
                        var availableBal, currBal, outstandingBal, accType;
                        var accountNumber = dealsData[i]["accountID"];
                        aHeader["nickName"] = dealsData[i]["accountName"];
                        if (aHeader["nickName"] !== null && aHeader["nickName"] !== "") {
                            if (aHeader["nickName"].length > 20) aHeader["nickName"] = aHeader["nickName"].substring(0, 20) + "  ...";
                        }
                        //                       var ttt = aHeader["nickName"] +" "+ accountNumber.substring(accountNumber.length-3, accountNumber.length);
                        var ttt = aHeader["nickName"] + " " + accountNumber;
                        if (!isEmpty(dealsData[i]["availableBalance"])) {
                            availableBal = dealsData[i]["availableBalance"];
                        }
                        if (!isEmpty(dealsData[i]["currentBalance"])) {
                            currBal = dealsData[i]["currentBalance"];
                        }
                        if (!isEmpty(dealsData[i]["outstandingBalance"])) {
                            outstandingBal = dealsData[i]["outstandingBalance"];
                        }
                        if (dealsData[i]["accountType"] == "C") {
                            currBal = "-" + currBal;
                        }
                        //aHeader["availableBalance"] = availableBal;
                        aHeader["currentBalance"] = currBal;
                        aHeader["outstandingBalance"] = outstandingBal;
                        aHeader["currencyCode"] = dealsData[i]["currencyCode"];
                        aHeader["accountNumber"] = accountNumber;
                        aHeader["reference_no"] = dealsData[i]["reference_no"];
                        if (langSelected1 === "ar" || langSelected1 === "ara") {
                            var taxtData = '<label>' + ttt + '</label>';
                            aHeader["nickName"] = {
                                "text": taxtData.toString()
                            }
                            aHeader["availableBalance"] = aHeader["currencyCode"] + " " + availableBal;
                        } else {
                            var taxtData = '<label>' + ttt + '</label>';
                            aHeader["nickName"] = {
                                "text": taxtData.toString()
                            }
                            aHeader["availableBalance"] = availableBal + " " + aHeader["currencyCode"];
                        }
                        if (dealsData[i]["accountType"] == "T") {
                            accType = "Term Deposite";
                        }
                        aHeader["accountType"] = accType;
                        segDealsData.push(aHeader);
                    }
                    kony.print("segDealsData:::" + JSON.stringify(segDealsData));
                }
                //Loans-----------------------------------------------------------------------------------------------
                if ((!isEmpty(loansData)) && loansData.length > 0) {
                    kony.print("Inside Loans section:::");
                    for (var i in loansData) {
                        var segformat = [];
                        var segRowData = [];
                        var aHeader = {};
                        var availableBal, currBal, outstandingBal, accType;
                        var accountNumber = loansData[i]["accountID"];
                        aHeader["nickName"] = loansData[i]["accountName"];
                        if (aHeader["nickName"] !== null && aHeader["nickName"] !== "") {
                            if (aHeader["nickName"].length > 20) aHeader["nickName"] = aHeader["nickName"].substring(0, 20) + "  ...";
                        }
                        var ttt = aHeader["nickName"] + " " + accountNumber;
                        if (!isEmpty(loansData[i]["availableBalance"])) {
                            availableBal = loansData[i]["availableBalance"];
                        }
                        if (!isEmpty(loansData[i]["currentBalance"])) {
                            currBal = loansData[i]["currentBalance"];
                        }
                        if (!isEmpty(loansData[i]["outstandingBalance"])) {
                            outstandingBal = loansData[i]["outstandingBalance"];
                        }
                        if (loansData[i]["accountType"] == null && loansData[i]["accountType"] == "C") {
                            currBal = "-" + currBal;
                        }
                        //aHeader["availableBalance"] = availableBal;
                        aHeader["currentBalance"] = currBal;
                        aHeader["outstandingBalance"] = outstandingBal;
                        aHeader["currencyCode"] = loansData[i]["currencyCode"];
                        aHeader["accountNumber"] = accountNumber;
                        aHeader["reference_no"] = loansData[i]["reference_no"];
                        if (langSelected1 === "ar" || langSelected1 === "ara") {
                            var taxtData = '<label>' + ttt + '</label>';
                            aHeader["nickName"] = {
                                "text": taxtData.toString()
                            }
                            aHeader["availableBalance"] = aHeader["currencyCode"] + " " + availableBal;
                        } else {
                            var taxtData = '<label>' + ttt + '</label>';
                            aHeader["nickName"] = {
                                "text": taxtData.toString()
                            }
                            aHeader["availableBalance"] = availableBal + " " + aHeader["currencyCode"];
                        }
                        if (loansData[i]["accountType"] == "L") {
                            accType = "Loans";
                        }
                        aHeader["accountType"] = accType;
                        segLoansData.push(aHeader);
                    }
                    kony.print("segLoansData:::" + JSON.stringify(segLoansData));
                }
                /* data = {"segAccountsKADeals" :{"segAccountsKADeals":{}},
                        "segAccountsKALoans" :{"segAccountsKALoans":{}},
                        "segAccountsKA" :{"segAccountsKA":{}}
                       } 
                 data[segment][segment].setData(sectionAccData);
                data["segAccountsKADeals"]["segAccountsKADeals"].setData(segDealsData);
                data["segAccountsKALoans"]["segAccountsKALoans"].setData(segLoansData); */
                kony.print("sectionAccData  Data::::::" + JSON.stringify(data));
            }
            return [sectionAccData, segDealsData, segLoansData];
        } catch (e) {
            kony.print("exception in frmAccountsLandingControllerExtension processAccounts - " + JSON.stringify(e));
        }
    }
});
Date.prototype.timeNow = function() {
    return ((this.getHours() < 10) ? "0" : "") + ((this.getHours() > 12) ? (this.getHours() - 12) : this.getHours()) + ":" + ((this.getMinutes() < 10) ? "0" : "") + this.getMinutes() + ((this.getHours() > 12) ? (' PM') : ' AM');
};