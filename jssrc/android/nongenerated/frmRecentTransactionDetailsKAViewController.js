kony = kony || {};
kony.rb = kony.rb || {};
kony.rb.frmRecentTransactionDetailsKAViewController = function() {};
kony.rb.frmRecentTransactionDetailsKAViewController.prototype.onClickDeleteCardlessTransaction = function() {
    ShowLoadingScreen();
    var navManager = applicationManager.getNavManager();
    var cardlessPC = applicationManager.getCardlessWithdrawalPresentationController();
    var cardlessTxnObj = navManager.getCustomInfo("frmRecentTransactionDetailsKA");
    var cardlessTxnObjRecord = cardlessTxnObj.cancelCardlessTxn;
    cardlessPC.showTransferPayLandingScreen(cardlessTxnObjRecord);
};
kony.rb.frmRecentTransactionDetailsKAViewController.prototype.frmRecentTransactionDetailsKApostshow = function(txnDetails) {
    var navManager = applicationManager.getNavManager();
    var cardlessTxnObj = navManager.getCustomInfo("frmRecentTransactionDetailsKA");
    var cardlessDetails = cardlessTxnObj.cancelCardlessTxn;
    navManager.setCustomInfo("frmRecentTransactionDetailsKA", {
        "cancelCardlessTxn": cardlessDetails,
        "txnDetails": txnDetails
    });
};
kony.rb.frmRecentTransactionDetailsKAViewController.prototype.frmRecentTransactionDetailsKApreshow = function(cardlessDetails) {
    var navManager = applicationManager.getNavManager();
    navManager.setCustomInfo("frmRecentTransactionDetailsKA", {
        "cancelCardlessTxn": cardlessDetails
    });
};
kony.rb.frmRecentTransactionDetailsKAViewController.prototype.onClickMail = function() {
    var navManager = applicationManager.getNavManager();
    var cardlessPC = applicationManager.getCardlessWithdrawalPresentationController();
    var cardlessTxnObj = navManager.getCustomInfo("frmRecentTransactionDetailsKA");
    var cardlessTxnObjRecord = cardlessTxnObj.cancelCardlessTxn;
    var contact;
    if (cardlessTxnObjRecord.cashlessPhone) {
        contact = cardlessTxnObjRecord.cashlessPhone;
    } else {
        contact = cardlessTxnObjRecord.cashlessEmail;
    }
    var txnObj = navManager.getCustomInfo("frmRecentTransactionDetailsKA");
    var txnObjRecord = txnObj.txnDetails;
    var passCode = txnObjRecord.lblCashlessOTP;
    var toContactName = txnObjRecord.cashlessPersonName;
    var amount = txnObjRecord.amount;
    var number = kony.retailBanking.globalData.globals.userObj.phone;
    var email = kony.retailBanking.globalData.globals.userObj.email;
    if (number) {
        kony.phone.openEmail([contact], [], [], "Cardless Cash Withdrawal", "Dear " + toContactName + ", You have received " + amount + " from mobile " + number + ". To withdraw cash, please enter the Withdrawal Code - " + passCode + " and 4-digit Secure Code shared by the sender at the ATM.");
    } else if (email) {
        kony.phone.openEmail([contact], [], [], "Cardless Cash Withdrawal", "Dear " + toContactName + ", You have received " + amount + " from email " + email + ". To withdraw cash, please enter the Withdrawal Code - " + passCode + " and 4-digit Secure Code shared by the sender at the ATM.");
    } else {
        kony.phone.openEmail([contact], [], [], "Cardless Cash Withdrawal", "Dear " + toContactName + ", You have received " + amount + ". To withdraw cash, please enter the Withdrawal Code - " + passCode + " and 4-digit Secure Code shared by the sender at the ATM.");
    }
};
kony.rb.frmRecentTransactionDetailsKAViewController.prototype.onClickMessage = function() {
    var navManager = applicationManager.getNavManager();
    var cardlessPC = applicationManager.getCardlessWithdrawalPresentationController();
    var cardlessTxnObj = navManager.getCustomInfo("frmRecentTransactionDetailsKA");
    var cardlessTxnObjRecord = cardlessTxnObj.cancelCardlessTxn;
    var contact;
    if (cardlessTxnObjRecord.cashlessPhone) {
        contact = cardlessTxnObjRecord.cashlessPhone;
    } else {
        contact = cardlessTxnObjRecord.cashlessEmail;
    }
    var txnObj = navManager.getCustomInfo("frmRecentTransactionDetailsKA");
    var txnObjRecord = txnObj.txnDetails;
    var passCode = txnObjRecord.lblCashlessOTP;
    var toContactName = txnObjRecord.cashlessPersonName;
    var amount = txnObjRecord.amount;
    var number = kony.retailBanking.globalData.globals.userObj.phone;
    var email = kony.retailBanking.globalData.globals.userObj.email;
    if (number) {
        kony.phone.sendSMS(contact, "Dear " + toContactName + ", You have received " + amount + " from mobile " + number + ". To withdraw cash, please enter the Withdrawal Code - " + passCode + " and 4-digit Secure Code shared by the sender at the ATM.");
    } else if (email) {
        kony.phone.sendSMS(contact, "Dear " + toContactName + ", You have received " + amount + " from email " + email + ". To withdraw cash, please enter the Withdrawal Code - " + passCode + " and 4-digit Secure Code shared by the sender at the ATM.");
    } else {
        kony.phone.sendSMS(contact, "Dear " + toContactName + ", You have received " + amount + ". To withdraw cash, please enter the Withdrawal Code - " + passCode + " and 4-digit Secure Code shared by the sender at the ATM.");
    }
};
kony.rb.frmRecentTransactionDetailsKAViewController.prototype.onClickWatchVideo = function() {
    // kony.application.openURL("https://youtu.be/UGJMk5_ZNrk");
};