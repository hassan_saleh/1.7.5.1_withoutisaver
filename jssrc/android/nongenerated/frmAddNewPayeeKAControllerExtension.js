/*
 * Controller Extension class for frmAddNewPayeeKA
 * Developer can edit the existing methods or can add new methods if required
 *
 */
kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};
/**
 * Creates a new Form Controller Extension.
 * @class frmAddNewPayeeKAControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmAddNewPayeeKAControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmAddNewPayeeKAControllerExtension#
     */
    fetchData: function() {
        try {
            var scopeObj = this;
            //       fetchAllCompanies();
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            exceptionLogCall("frmAddNewPayeeKAControllerExtension: fetchData", "UI ERROR", "UI", err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
            kony.print("success for newbiller ::" + JSON.stringify(response));
            kony.sdk.mvvm.log.info("success fetching data ", response);
            scopeObj.getController().processData(response);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmAddNewPayeeKAControllerExtension#
     * @returns {Object} - processed data
     */
    processData: function(data) {
        try {
            var scopeObj = this;
            var processedData = this.$class.$superp.processData.call(this, data);
            this.getController().bindData(processedData);
            return processedData;
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            exceptionLogCall("frmAddNewPayeeKAControllerExtension: ProcessData", "UI ERROR", "UI", err);
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        };
    },
    /** 
     * This method binds the processed data to the form. Developer can edit.
     * Default implementation binds the data to widgets in the form.
     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmAddNewPayeeKAControllerExtension#
     */
    bindData: function(data) {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.clear();
            this.$class.$superp.bindData.call(this, data);
            this.clearSkinsOffrmAddNewPayeeForm();
            this.getController().getFormModel().formatUI();
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            this.getController().showForm();
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            exceptionLogCall("frmAddNewPayeeKAControllerExtension: BindData", "UI ERROR", "UI", err);
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmAddNewPayeeKAControllerExtension#
     */
    saveData: function() {
        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            this.$class.$superp.saveData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            exceptionLogCall("frmAddNewPayeeKAControllerExtension: Savedata", "UI ERROR", "UI", err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully created record 
            kony.print("Add biller success ::" + JSON.stringify(res));
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            if (res.referenceId === "0" && res.ErrorCode === "00000") { //geti18nkey("i18n.common.ReferenceNumber")+" " + res.p_EfawateercomID
                kony.boj.populateSuccessScreen("success.png", geti18nkey("i18n.common.billeraddsucc"), "", geti18nkey("i18n.common.CgtAD"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.CgtBL"), "ManagePayee", geti18Value("i18n.common.paynow"), "PayBill");
            } else if (res.ErrorCode === "00444") {
                kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.AddBene.BeneExist"), geti18nkey("i18n.common.CgtAD"), geti18nkey("i18n.common.AccountDashboard"), "ManagePayee", geti18Value("i18n.common.paynow"), "PayBill");
            } else {
                var Message = getErrorMessage(res.ErrorCode);
                if (res.ErrorCode === "72018" || res.ErrorCode === "72004") {
                    Message = geti18Value("i18n.common.somethingwentwrong");
                }
                customAlertPopup(geti18Value("i18n.common.alert"), Message, popupCommonAlertDimiss, "", geti18nkey("i18n.settings.ok"), "");
                frmAddNewPayeeKA.flxConfirmPopUp.isVisible = false;
                frmAddNewPayeeKA.flxFormMain.isVisible = true;
                frmAddNewPayeeKA.show();
            }
        }

        function error(err) {
            //Handle error case
            kony.print("error in AddBiller\n" + JSON.stringify(err));
            gblTModule = "";
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
            kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.CgtBL"), "ManagePayee");
        }
    },
    /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmAddNewPayeeKAControllerExtension#
     */
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method shows form.
     * @memberof frmAddNewPayeeKAControllerExtension#
     */
    validateNewAccountMobile: function() {
        var formmodel = this.getController().getFormModel();
        if (kony.retailBanking.util.validation.validateTextArea(formmodel.getWidgetData("newPayeeNameTextfield").getData())) {
            frmAddNewPayeeKA.CopyLabel0b353ede7efad4c.skin = "sknsectionHeaderLabel";
            frmAddNewPayeeKA.flxDivider1.skin = "skntextFieldDivider";
        } else {
            frmAddNewPayeeKA.CopyLabel0b353ede7efad4c.skin = "sknD0021BLatoSemiBold";
            frmAddNewPayeeKA.flxDivider1.skin = "sknFlxBGe2e2e2B1pxD0021B";
        }
        if (kony.retailBanking.util.validation.validateTextArea(formmodel.getWidgetData("nickName").getData())) {
            frmAddNewPayeeKA.CopyLabel0927085edf80e42.skin = "sknsectionHeaderLabel";
            frmAddNewPayeeKA.CopyflxDivider0b294261c43344c.skin = "skntextFieldDivider";
        } else {
            frmAddNewPayeeKA.CopyLabel0927085edf80e42.skin = "sknD0021BLatoSemiBold";
            frmAddNewPayeeKA.CopyflxDivider0b294261c43344c.skin = "sknFlxBGe2e2e2B1pxD0021B";
        }
        if (kony.retailBanking.util.validation.validateTextArea(formmodel.getWidgetData("newPayeeAccountNumberTextField").getData()) && formmodel.getWidgetData("newPayeeAccountNumberTextField").getData() == frmAddNewPayeeKA.tboxReenterAccountNumberKA.text) {
            frmAddNewPayeeKA.CopyLabel0200c64cb34cb44.skin = "sknsectionHeaderLabel";
            frmAddNewPayeeKA.CopyflxDivider07de2109b1c0a4d.skin = "skntextFieldDivider";
            frmAddNewPayeeKA.CopyflxDivider0a70939b9027043.skin = "skntextFieldDivider";
        } else {
            frmAddNewPayeeKA.CopyLabel0200c64cb34cb44.skin = "sknD0021BLatoSemiBold";
            frmAddNewPayeeKA.CopyflxDivider07de2109b1c0a4d.skin = "sknFlxBGe2e2e2B1pxD0021B";
            frmAddNewPayeeKA.CopyflxDivider0a70939b9027043.skin = "sknFlxBGe2e2e2B1pxD0021B";
        }
        if (kony.retailBanking.util.validation.validateTextArea(formmodel.getWidgetData("zipcode").getData())) {
            frmAddNewPayeeKA.CopyLabel0b1ac08ce375a4c.skin = "sknsectionHeaderLabel";
            frmAddNewPayeeKA.CopyFlexContainer02077f851ad8c46.skin = "skntextFieldDivider";
        } else {
            frmAddNewPayeeKA.CopyLabel0b1ac08ce375a4c.skin = "sknD0021BLatoSemiBold";
            frmAddNewPayeeKA.CopyFlexContainer02077f851ad8c46.skin = "sknFlxBGe2e2e2B1pxD0021B";
        }
        if (kony.retailBanking.util.validation.validateTextArea(formmodel.getWidgetData("newPayeeNameTextfield").getData()) && kony.retailBanking.util.validation.validateTextArea(formmodel.getWidgetData("nickName").getData()) && kony.retailBanking.util.validation.validateTextArea(formmodel.getWidgetData("newPayeeAccountNumberTextField").getData()) && kony.retailBanking.util.validation.validateTextArea(formmodel.getWidgetData("zipcode").getData()) && formmodel.getWidgetData("newPayeeAccountNumberTextField").getData() == frmAddNewPayeeKA.tboxReenterAccountNumberKA.text) {
            this.saveData();
        }
    },
    clearSkinsOffrmAddNewPayeeForm: function() {
        frmAddNewPayeeKA.CopyLabel0b353ede7efad4c.skin = "sknsectionHeaderLabel";
        frmAddNewPayeeKA.flxDivider1.skin = "skntextFieldDivider";
        frmAddNewPayeeKA.CopyLabel0927085edf80e42.skin = "sknsectionHeaderLabel";
        frmAddNewPayeeKA.CopyflxDivider0b294261c43344c.skin = "skntextFieldDivider";
        frmAddNewPayeeKA.CopyLabel0200c64cb34cb44.skin = "sknsectionHeaderLabel";
        frmAddNewPayeeKA.CopyflxDivider07de2109b1c0a4d.skin = "skntextFieldDivider";
        frmAddNewPayeeKA.CopyLabel0b1ac08ce375a4c.skin = "sknsectionHeaderLabel";
        frmAddNewPayeeKA.CopyFlexContainer02077f851ad8c46.skin = "skntextFieldDivider";
        frmAddNewPayeeKA.CopyflxDivider0a70939b9027043.skin = "skntextFieldDivider";
    },
    showForm: function() {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.showView();
        } catch (e) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    }
});