kony = kony || {};
kony.chatbot = kony.chatbots || {};
kony.chatbot.command = function() {
    kony.print("in constructor");
};
kony.chatbot.command.botFormNavigationData = {};
kony.chatbot.command.prototype.predict = function(type, str, callback) {
    try {
        var httpClient = new kony.net.HttpRequest();
        httpClient.timeout = 10000;
        httpClient.onReadyStateChange = onReadyStateChanged;
        httpClient.open(constants.HTTP_METHOD_POST, "http://182.73.145.20:443/bot");
        httpClient.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        var postdata = new kony.net.FormData();
        if (type === "showATMS") {
            postdata.append("message", str.str);
            postdata.append("lat", str.lat);
            postdata.append("lon", str.lon);
            postdata.append("type", str.type);
        } else {
            postdata.append("message", str);
        }
        postdata.append("auth_token", kony.sdk.getCurrentInstance().currentClaimToken);
        postdata.append("session_token", kony.retailBanking.globalData.session_token);
        httpClient.send(postdata);

        function onReadyStateChanged() {
            var readyState = Number(this.readyState.toString());
            var status = Number(this.status.toString());
            if (readyState === 2) {
                kony.print("in ready state 2");
            }
            if (readyState === 3) {
                kony.print("in ready state 3");
            }
            if (readyState === 4) {
                kony.print("in ready state 4");
                if (status === 200) {
                    kony.print("in status 200");
                    callback(this.response);
                } else {
                    (new kony.chatbotPresentation()).handleBotServerConncetionFailure(callback);
                }
            }
        }
    } catch (err) {
        kony.print("err " + err);
    }
};
kony.chatbot.command.prototype.payCreditCard = function(amount) {
    try {
        kony.retailBanking.globalData.fromChat = true;
        fetchExternalAccountData();
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var listController = INSTANCE.getFormController("frmNewTransferKA");
        var navObject = new kony.sdk.mvvm.NavigationObject();
        navObject.setRequestOptions("segInternalFromAccountsKA", {
            "headers": {
                "session_token": kony.retailBanking.globalData.session_token
            }
        });
        listController.performAction("navigateTo", ["frmNewTransferKA", navObject]);
        newTransferPreShow();
        kony.retailBanking.globalData.selectedIndex = 0;
        kony.retailBanking.globalData.amount = amount;
    } catch (err) {}
};
kony.chatbot.command.prototype.showTransactions = function(AccountType) {
    try {
        kony.retailBanking.globalData.fromChat = true;
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var controller = INSTANCE.getFormController("frmAccountsLandingKA");
        var viewModel = controller.getFormModel();
        if (AccountType) {
            var accountType = AccountType;
            // checking , savings,deposit,creditcard
            if (accountType == "checking") {
                viewModel.setViewAttributeByProperty("segAccountsKA", "selectedRowIndex", [0, 2]);
            } else if (accountType == "savings") {
                viewModel.setViewAttributeByProperty("segAccountsKA", "selectedRowIndex", [0, 3]);
            } else if (accountType == "creditcard") {
                viewModel.setViewAttributeByProperty("segAccountsKA", "selectedRowIndex", [0, 0]);
            } else {
                // all other cases show deposit 
                viewModel.setViewAttributeByProperty("segAccountsKA", "selectedRowIndex", [0, 1]);
            }
            //frmAccountDetailKA.show();
            controller.performAction("navigateToAccountsDetails");
        }
    } catch (err) {
        kony.print(err);
    }
};
kony.chatbots.command.prototype.payPerson = function(reply, fromAccount, amount) {
    try {
        kony.retailBanking.globalData.fromAccount = fromAccount;
        kony.retailBanking.globalData.amount = amount;
        kony.retailBanking.globalData.fromChat = true;
        navigateToNewPayPerson("InitialLanding", null);
    } catch (err) {
        kony.print(err);
    }
};