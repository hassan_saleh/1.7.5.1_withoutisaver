var frmAlertsKAConfig = {
    "formid": "frmAlertsKA",
    "frmAlertsKA": {
        "entity": "Notifications",
        "objectServiceName": "RBObjects",
        "objectServiceOptions": {
            "access": "online"
        }
    },
    "lblAccountNumber": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Notifications",
            "field": "AccountNumber"
        }
    },
    "lblCustID": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Notifications",
            "field": "custId"
        }
    },
    "lblLanguage": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Notifications",
            "field": "language"
        }
    },
    "lblWithdrawalFlag": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Notifications",
            "field": "WithDwrlFlag"
        }
    },
    "lblDepositFlag": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Notifications",
            "field": "DepositFlag"
        }
    },
    "lblDailyBalanceFlag": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Notifications",
            "field": "DailyBalFlag"
        }
    },
    "lblMorningBalanceFlag": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Notifications",
            "field": "MornBalFlag"
        }
    },
    "lblChequeClearedFlag": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Notifications",
            "field": "ChkClrdFlag"
        }
    },
    "lblAlertDisable": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Notifications",
            "field": "AlertFlag"
        }
    }
};