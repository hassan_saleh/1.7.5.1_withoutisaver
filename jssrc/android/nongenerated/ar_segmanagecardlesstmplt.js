//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializesegmanagecardlesstmpltAr() {
    flxManageCardlessNormalAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "flxManageCardlessNormal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    flxManageCardlessNormal.setDefaultUnit(kony.flex.DP);
    var flxToAnimate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxToAnimate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxToAnimate.setDefaultUnit(kony.flex.DP);
    var flxAnimate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxAnimate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "85%",
        "zIndex": 1
    }, {}, {});
    flxAnimate.setDefaultUnit(kony.flex.DP);
    var flxDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "2%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDetails.setDefaultUnit(kony.flex.DP);
    var transactionDateLbl = new kony.ui.Label({
        "centerY": "20%",
        "height": "32%",
        "id": "transactionDateLbl",
        "isVisible": true,
        "right": "1%",
        "skin": "sknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.accounts.date"),
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var amountLbl = new kony.ui.Label({
        "centerY": "50%",
        "height": "30%",
        "id": "amountLbl",
        "isVisible": true,
        "right": "1%",
        "skin": "sknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionDate = new kony.ui.Label({
        "centerY": "20%",
        "height": "32%",
        "id": "transactionDate",
        "isVisible": true,
        "right": "50%",
        "skin": "sknLblNextDisabled",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var amount = new kony.ui.Label({
        "centerY": "50%",
        "height": "30%",
        "id": "amount",
        "isVisible": true,
        "right": "50%",
        "skin": "sknLblAccNumBiller",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var accountNumber = new kony.ui.Label({
        "centerY": "80%",
        "height": "32%",
        "id": "accountNumber",
        "isVisible": true,
        "right": "1%",
        "skin": "sknLblAccNumBiller",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStatus = new kony.ui.Label({
        "centerY": "80%",
        "height": "30%",
        "id": "lblStatus",
        "isVisible": true,
        "right": "50%",
        "skin": "sknLblAccNumBiller",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDetails.add(transactionDateLbl, amountLbl, transactionDate, amount, accountNumber, lblStatus);
    flxAnimate.add(flxDetails);
    var flxButtonHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxButtonHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "86%",
        "skin": "slFbox",
        "top": "0%",
        "width": "13%",
        "zIndex": 1
    }, {}, {});
    flxButtonHolder.setDefaultUnit(kony.flex.DP);
    var btnDelete = new kony.ui.Button({
        "focusSkin": "CopysknSegmentDelete0h225a870b5144b",
        "height": "100%",
        "id": "btnDelete",
        "isVisible": true,
        "right": "0%",
        "onClick": AS_Button_c1338012c1a347de81b7319a137d63ac,
        "skin": "CopysknSegmentDelete0h225a870b5144b",
        "text": "w",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxButtonHolder.add(btnDelete);
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknsegmentDivider",
        "top": "99%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    flxToAnimate.add(flxAnimate, flxButtonHolder, contactListDivider);
    flxManageCardlessNormalAr.add(flxToAnimate);
}