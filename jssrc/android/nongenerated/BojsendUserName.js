function callRequestUsrname(MSG) {
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var curDate = new Date();
        var month = (curDate.getMonth() + 1) < 10 ? "0" + (curDate.getMonth() + 1) : (curDate.getMonth() + 1);
        var date = (curDate.getDate()) < 10 ? "0" + (curDate.getDate()) : (curDate.getDate());
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("User", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("User");
        dataObject.addField("P_IS_SILENT", "0");
        dataObject.addField("P_ALERT_ID", "OTP");
        dataObject.addField("P_ALERT_INS_TIME", curDate.getFullYear() + "-" + month + "-" + date);
        dataObject.addField("otp", MSG);
        dataObject.addField("P_ALERT_PRIORITY", "1");
        dataObject.addField("P_COUNTRY_CODE", "1");
        dataObject.addField("P_SILENT_FROM", "0:00:00");
        dataObject.addField("P_LANGUAGE", "1");
        dataObject.addField("custId", custid);
        dataObject.addField("P_SOURCE", "MOB");
        dataObject.addField("P_SILENT_TO", "0:00:00");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("Input params-->" + JSON.stringify(serviceOptions));
        modelObj.customVerb("BOJsendUserName", serviceOptions, BOJsendUserNameSuccessCallback, BOJsendUserNameErrorCallback);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function BOJsendUserNameSuccessCallback(response) {
    kony.application.dismissLoadingScreen();
    if (!isEmpty(response) && response.opstatus === 0 && response.statusCodeSMS === "00000") {
        //Change SMS,Mobile number -------------------
        if (gblFromModule == "UpdateSMSNumber") {
            if (!gblBeforeUpdateSMSNumber) {
                gblToOTPFrom = "UpdateSMSNumber";
                callConfirmUpdateMobileNumber(geti18Value("i18n.SMS.SMSSure"), callRequestOTP);
            } else {
                customerAccountDetails.profileDetails = null;
                kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.SMS.mobileSuccess"), geti18Value("i18n.common.gotoSet"), "gotoSettings", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"));
                gblBeforeUpdateSMSNumber = false;
                gblToOTPFrom = "";
                gblFromModule = "";
                var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetProfileData");
                appMFConfiguration.invokeOperation("prGetCustDetails", {}, {
                    "custId": custid
                }, function(response) {
                    if (response !== undefined && response.custData !== undefined && response.custData[0] !== null) {
                        frmUserSettingsMyProfileKA.lblSMSNumber.text = "";
                        response.custData[0].maskedSMSno = mask_MobileNumber(response.custData[0].smsno);
                        customerAccountDetails.profileDetails = response.custData[0];
                    }
                }, function(error) {});
            }
        } else if (gblFromModule == "UpdateMobileNumber") {
            if (!gblBeforeUpdateSMSNumber) {
                gblToOTPFrom = "UpdateSMSNumber";
                callConfirmUpdateMobileNumber(geti18Value("i18n.SMS.MobileSure"), callRequestOTP);
            } else {
                kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.SMS.SMSSuccess"), geti18nkey("i18n.cards.gotoCards"), "CardsLanding", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"));
                gblBeforeUpdateSMSNumber = false;
                gblToOTPFrom = "";
                gblFromModule = "";
            }
            //Change SMS,Mobile number -------------------
        } else {
            //customAlertPopup("",geti18Value("i18n.common.sendyourname"),showlogin, "");
            kony.boj.populateSuccessScreen("success.png", "", geti18nkey("i18n.common.sendyourname"), geti18nkey("i18n.common.GoToLogin"), "Login");
        }
    } else {
        customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
        exceptionLogCall("BOJsendUserNameSuccessCallback", "UI ERROR", "UI", "opstatus othr than 0");
        kony.application.dismissLoadingScreen();
    }
}

function showlogin() {
    popupCommonAlertDimiss();
    frmLoginKA.show();
}

function BOJsendUserNameErrorCallback(response) {
    kony.print("BOJsendUserNameErrorCallback -----" + JSON.stringify(response));
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.defaultErrorMsg"), popupCommonAlertDimiss, "");
    kony.application.dismissLoadingScreen();
    if (gblBeforeUpdateSMSNumber) {
        gblBeforeUpdateSMSNumber = false;
    }
}