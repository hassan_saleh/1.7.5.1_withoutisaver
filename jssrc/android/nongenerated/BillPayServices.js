function getBillDetailsPostpaid() {
    if (kony.sdk.isNetworkAvailable()) {
        ShowLoadingScreen();
        var langSelected = kony.store.getItem("langPrefObj");
        var Language = langSelected.toUpperCase();
        var billingNumber = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g, "");
        billingNumber = billingNumber.replace(/\s/g, "");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Payee", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Payee");
        dataObject.addField("custId", custid);
        dataObject.addField("lang", Language);
        dataObject.addField("p_BillerCode", kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code);
        dataObject.addField("p_ServiceType", kony.boj.Biller[kony.boj.selectedBillerType].selectedType.code);
        //alert("Data in svc type "+kony.boj.Biller[kony.boj.selectedBillerType].selectedType.code);
        dataObject.addField("p_BillingNo", billingNumber);
        dataObject.addField("p_BillNo", billingNumber);
        dataObject.addField("p_DateFlag", "N");
        dataObject.addField("p_CustInfoFlag", "Y");
        //     dataObject.addField("p_IdType","NAT");
        //     dataObject.addField("p_ID","9861033442");
        //     dataObject.addField("p_Nation","JO");
        dataObject.addField("p_IncPayments", "N");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("Input params getBillDetailsPostpaid-->" + serviceOptions);
        kony.print("Input params getBillDetailsPostpaid-->" + JSON.stringify(serviceOptions));
        modelObj.customVerb("GetBillDetails", serviceOptions, getBillDetailsPostpaidSuccess, getBillDetailsPostpaidError);
    } else {
        var Message = getErrorMessage("abcdef");
        customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
    }
}

function getBillDetailsPostpaidSuccess(response) {
    //   alert(JSON.stringify(response));
    //   response =  demoResponsePostpaid;
    //   alert(JSON.stringify(response));
    kony.application.dismissLoadingScreen();
    if (response.ErrorCode === "00000" && response.BillerDetails != undefined) {
        //alert(JSON.stringify(response));
        kony.print("Success getBillDetailsPostpaid-->" + JSON.stringify(response));
        //alert("Success!!");
        if (parseFloat(response.BillerDetails[0].dueamount) > 0) {
            populateDatatopostpaidNewBillDetails(response);
            postpaidNewBillDetails();
        } else {
            var Message = "";
            if (response.ErrorCode === "00000" || response.ErrorCode === "0" || response.ErrorCode === 0) {
                Message = geti18Value("i18n.NoDueBilss")
            } else {
                Message = getErrorMessage(response.ErrorCode);
            }
            customAlertPopup(geti18Value("i18n.maps.Info"), Message, popupCommonAlertDimiss, "");
        }
    } else {
        kony.print("Success getBillDetailsPostpaid-->" + JSON.stringify(response));
        var Message = getErrorMessage(response.ErrorCode);
        customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
        //kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.transactionFailed"), geti18Value("i18n.Transfer.AnotherTransaction"), "PaymentDashboard",geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard"));
        //frmCongratulations.show();
    }
}

function getBillDetailsPostpaidError(response) {
    kony.application.dismissLoadingScreen();
    //alert(JSON.stringify(response));
    kony.print("Failure getBillDetailsPostpaid-->" + JSON.stringify(response));
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18nkey("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
}

function getBillDetailsPrepaid() {
    if (kony.sdk.isNetworkAvailable()) {
        ShowLoadingScreen();
        var langSelected = kony.store.getItem("langPrefObj");
        var Language = langSelected.toUpperCase();
        var billingNumber = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g, "");
        billingNumber = billingNumber.replace(/\s/g, "");
        kony.print("billing Number ::" + billingNumber);
        var acc = kony.store.getItem("BillPayfromAcc");
        var inputs = kony.store.getItem("PrepaidInputs");
        var deno_desc = kony.store.getItem("deno_desc");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Payee", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Payee");
        dataObject.addField("custId", custid);
        dataObject.addField("lang", Language);
        dataObject.addField("p_biller_code", kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code);
        dataObject.addField("p_serv_type_code", kony.boj.Biller[kony.boj.selectedBillerType].selectedType.serv_type_code);
        //alert(kony.boj.Biller[kony.boj.selectedBillerType].selectedType.serv_type_code);
        if (inputs.billing_no_flag == "true") dataObject.addField("p_billing_no", billingNumber);
        else dataObject.addField("p_billing_no", "");
        if (inputs.deno_flag == "true") dataObject.addField("P_deno", kony.boj.Biller[kony.boj.selectedBillerType].selectedDenoType.deno_code);
        else dataObject.addField("P_deno", "");
        dataObject.addField("p_fcdb_ref_no", "");
        if (inputs.deno_flag == "false") dataObject.addField("amount", frmNewBillKA.tbxAmount.text.replace(/,/g, ""));
        else dataObject.addField("amount", "0"); //deno_desc); amount field should not contains charactes
        if (frmNewBillKA.btnBillsPayAccounts.text === "t") {
            dataObject.addField("p_acc_or_cr", acc.accountID);
            dataObject.addField("p_account_br", acc.branchNumber);
        } else {
            dataObject.addField("p_acc_or_cr", acc.card_num);
            dataObject.addField("p_account_br", "");
        }
        dataObject.addField("fees", "");
        dataObject.addField("p_paymt_type", "BillNew");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("Input params getBillDetailsPostpaid-->" + serviceOptions);
        kony.print("Input params getBillDetailsPostpaid-->" + JSON.stringify(serviceOptions));
        modelObj.customVerb("PrePaidValDebit", serviceOptions, getBillDetailsPrepaidSuccess, getBillDetailsPrepaidError);
    } else {
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function getBillDetailsPrepaidSuccess(response) {
    kony.application.dismissLoadingScreen();
    if (response.ErrorCode === "00000" && response.FeeOutput !== undefined && response.FeeOutput[0].ErrorCode === "00000") {
        //alert(JSON.stringify(response));
        kony.print("Success getBillDetailsPrepaid-->" + JSON.stringify(response));
        // alert("Success!!");
        var storeData = isEmpty(response) ? "" : response;
        kony.store.setItem("PrepaidResponse", storeData);
        //     prePaidNewBillDetails(response);
        if (frmNewBillKA.flxAmount.isVisible === true) {
            frmConfirmPayBill.lblVal.text = formatamountwithCurrency(frmNewBillKA.tbxAmount.text, "JOD") + " JOD";
            serv_BILLSCOMISSIONCHARGE(frmNewBillKA.tbxAmount.text.replace(/,/g, ""));
        } else if (response.FeeOutput !== undefined) {
            frmConfirmPayBill.lblVal.text = formatamountwithCurrency(response.FeeOutput[0].DueAmount, "JOD") + " JOD";
            serv_BILLSCOMISSIONCHARGE(response.FeeOutput[0].DueAmount);
        }
        assignDatatoConfirmPrepaid();
        QuickFlowprev = "now";
    } else {
        kony.print("Success getBillDetailsPrepaid-->" + JSON.stringify(response));
        var Message = getServiceErrorMessage(response.FeeOutput[0].ErrorCode, "getBillDetailsPrepaidSuccess");
        customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
        //kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.transactionFailed"), geti18Value("i18n.Transfer.AnotherTransaction"), "PaymentDashboard",geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard"));
        //frmCongratulations.show();
    }
}

function getBillDetailsPrepaidError(response) {
    kony.application.dismissLoadingScreen();
    //alert(JSON.stringify(response));
    kony.print("Failure getBillDetailsPostpaid-->" + JSON.stringify(response));
    alert("Failure!!");
}

function performBillPayPostpaid() {
    if (kony.sdk.isNetworkAvailable()) {
        if (frmNewBillKA.btnBillsPayAccounts.text === "t") // hassan pay from credit card
        {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var controller = INSTANCE.getFormController("frmConfirmPayBill");
            var controllerContextData = controller.getContextData() || new kony.sdk.mvvm.NavigationObject();
            controllerContextData.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
            controller.setContextData(controllerContextData);
            controller.performAction("saveData");
        } else {
            CallServicePrePaidNewBillPayCC(); // hassan pay from credit card
        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        alert("" + kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"));
    }
}

function refreshAllAccounts() {
    if (kony.sdk.mvvm.isNetworkAvailabile()) {
        if (kony.sdk.mvvm.KonyApplicationContext.getAppInstance()) {
            //       ShowLoadingScreen();
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var options = {
                "access": "online",
                "objectName": "RBObjects"
            };
            var serviceName = "RBObjects";
            var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
            var dataObject = new kony.sdk.dto.DataObject("Accounts");
            var serviceOptions = {
                "dataObject": dataObject,
                "queryParams": {
                    "custId": custid,
                    "lang": kony.store.getItem("langPrefObj") === "en" ? "eng" : "ara"
                }
            };
            modelObj.fetch(serviceOptions, refreshSuc1, refreshFail1);
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function refreshSuc1(response) {
    kony.print("refresh accounts:::" + JSON.stringify(response));
    if (isEmpty(response)) {
        //     kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        //customAlertPopup("", geti18nkey("i18n.alerts.NoAccountsMsg"),popupCommonAlertDimiss,"");
    } else {
        var segAccountListData = response;
        var fromAccounts = [],
            toAccounts = [],
            loansData = [];
        var sectionAccData = [],
            segDealsData = [],
            segLoansData = [];
        if ((!isEmpty(segAccountListData)) && segAccountListData.length > 0) {
            for (var i in segAccountListData) {
                if (checkAccount(segAccountListData[i]["accountID"])) {
                    if (segAccountListData[i]["accountType"] != "T" && segAccountListData[i]["accountType"] != "L" && segAccountListData[i]["dr_ind"] == "Y" && segAccountListData[i]["productCode"] != "320") {
                        fromAccounts.push(segAccountListData[i]);
                    }
                    if (segAccountListData[i]["accountType"] != "T" && segAccountListData[i]["accountType"] != "L" && segAccountListData[i]["cr_ind"] == "Y" && segAccountListData[i]["productCode"] != "320") {
                        toAccounts.push(segAccountListData[i]);
                    }
                }
                kony.print("Refreshed fromAccounts:::" + JSON.stringify(fromAccounts));
                kony.print("Refreshed toAccounts:::" + JSON.stringify(toAccounts));
                kony.retailBanking.globalData.accountsDashboardData.fromAccounts = isEmpty(fromAccounts) ? {} : fromAccounts;
                kony.retailBanking.globalData.accountsDashboardData.toAccounts = isEmpty(toAccounts) ? {} : toAccounts;
                //       kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            }
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup(geti18Value("i18n.common.Attention"), geti18nkey("i18n.security.loginagain"), logoutpp, "");
            return;
        }
    }
}

function refreshFail1(err) {
    customAlertPopup(geti18nkey("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    kony.application.dismissLoadingScreen();
}
/*var demoResponsePostpaid = {
  "httpStatusCode": 200,
  "referenceId": "0",
  "opstatus": 0,
  "ErrorCode": "00000",
  "BillerDetails": 
  [      
    {
      "upper": "200",
      "billtype": "",
      "lower": "200",
      "servicetype": "Electricity",
      "feesonbiller": "false",
      "feesamt": "0",
      "issuedate": "2018-02-18",
      "duedate": "2018-02-18",
      "billstatus": "BillNew",
      "billno": "20180201",
      "dueamount": "200"
    }
  ],
  "httpresponse": 
  {
    "headers": 
    {
      "X-Android-Received-Millis": "1520406439228",
      "Access-Control-Allow-Methods": "GET, HEAD, POST, TRACE, OPTIONS",
      "X-Android-Selected-Protocol": "http/1.1",
      "Server": "Apache/2.4.6 (CentOS) OpenSSL/1.0.2k-fips",
      "X-Android-Sent-Millis": "1520406438166",
      "X-Kony-RequestId": "af2ded42-3e77-477c-908e-2656770fb1ed",
      "Date": "Wed, 07 Mar 2018 08:43:27 GMT",
      "Connection": "Keep-Alive",
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json; charset=UTF-8",
      "X-Android-Response-Source": "NETWORK 200",
      "Keep-Alive": "timeout=5, max=100",
      "Content-Length": "309"
    },
    "url": "http://boj.konylabs.net/services/data/v1/RBObjects/operations/Payee/GetBillDetails",
    "responsecode": 200
  }
}*/