//Type your code here
function bojgetDeviceLocale() {
    var langPrefDevice = kony.store.getItem("langPrefObj");
    if (langPrefDevice === null || langPrefDevice === "") {
        var deviceLocale = kony.i18n.getCurrentDeviceLocale();
        //var langInDevice = deviceLocale.language;
        var langInDevice;
        if (kony.os.deviceInfo().name === "Android") {
            langInDevice = deviceLocale.language;
        }
        if (kony.os.deviceInfo().name === "iPhone" || kony.os.deviceInfo().name === "iPad") {
            langInDevice = deviceLocale;
            if (langInDevice.indexOf("en") > -1) {
                langInDevice = "en";
            } else if (langInDevice.indexOf("ar") > -1) {
                langInDevice = "ar";
            } else {
                langInDevice = "en";
            }
        }
        kony.i18n.setCurrentLocaleAsync(langInDevice, onDevSuccesscallback, onDevFailurecallback);
    } else {
        kony.print("Nothing");
    }
}

function onDevSuccesscallback() {
    try {
        kony.print(" --- Inside onDevSuccesscallback ---");
        frmLanguageChange.btnEnglish.text = kony.i18n.getLocalizedString("i18n.common.English.text");
        frmLanguageChange.btnArabic.text = kony.i18n.getLocalizedString("i18n.common.Arabic.text");
    } catch (err) {
        kony.print(" --- Error ---" + JSON.stringify(err));
    }
    kony.print(" --- Exit of onDevSuccesscallback ---");
}

function onDevFailurecallback() {
    kony.print(" --- Inside onDevFailurecallback ---");
}

function onClickEnglish(waflag) {
    try {
        kony.print(" --- Inside onClickArabic ---");
        var engLocale = "en";
        if (isEmpty(waflag)) {
            kony.i18n.setCurrentLocaleAsync(engLocale, onEngSuccesscallback, onEngFailurecallback);
        } else {
            //Added for workaround for arabic issue
            kony.i18n.setCurrentLocaleAsync(engLocale, doNothing, onEngFailurecallback);
            kony.print(" --- Inside onEngSuccesscallback ---");
            kony.store.setItem("langPrefObj", "en");
            switchToDefaultLayout(rightToLeftCallback);
            //
        }
    } catch (err) {
        kony.print(" --- Error ---" + JSON.stringify(err));
    }
}

function onEngSuccesscallback() {
    try {
        kony.print(" --- Inside onEngSuccesscallback ---");
        frmLanguageChange.flxRememberLang.setVisibility(true);
        frmLanguageChange.lblRememChoice.text = kony.i18n.getLocalizedString("i18n.LangSelect.rememChoice.text");
        frmLanguageChange.lblLangChangeTxt.text = kony.i18n.getLocalizedString("i18n.LangSelect.langSettings.text");
        frmLanguageChange.btnDone.text = kony.i18n.getLocalizedString("i18n.LangSelect.done.text");
        kony.store.setItem("langPrefObj", "en");
        switchLangLayoutKA("en");
        switchToDefaultLayout(rightToLeftCallback);
    } catch (err) {
        kony.print(" --- Error ---" + JSON.stringify(err));
    }
    kony.print(" --- Exit of onEngSuccesscallback ---");
}

function onEngFailurecallback() {
    kony.print(" --- Inside onEngFailurecallback ---");
}

function onClickArabic(waflag) {
    try {
        kony.print(" --- Inside onClickArabic ---");
        var arabicLocale = "ar";
        if (isEmpty(waflag)) {
            kony.i18n.setCurrentLocaleAsync(arabicLocale, onArabicSuccesscallback, onArabicFailurecallback);
        } else {
            //Added for workaround for arabic issue
            kony.i18n.setCurrentLocaleAsync(arabicLocale, doNothing, onArabicFailurecallback);
            kony.print(" --- Inside onArabicSuccesscallback ---");
            kony.store.setItem("langPrefObj", "ar");
            switchToArabicLayout(rightToLeftCallback);
            //
        }
    } catch (err) {
        kony.print(" --- Error ---" + JSON.stringify(err));
    }
    kony.print(" ---  Exit of onClickArabic ---");
}

function onArabicSuccesscallback() {
    try {
        kony.print(" --- Inside onArabicSuccesscallback ---");
        frmLanguageChange.flxRememberLang.setVisibility(true);
        frmLanguageChange.lblRememChoice.text = kony.i18n.getLocalizedString("i18n.LangSelect.rememChoice.text");
        frmLanguageChange.lblLangChangeTxt.text = kony.i18n.getLocalizedString("i18n.LangSelect.langSettings.text");
        frmLanguageChange.btnDone.text = kony.i18n.getLocalizedString("i18n.LangSelect.done.text");
        //frmLanguageChange.btnDone.setEnabled(false);
        kony.store.setItem("langPrefObj", "ar");
        var langFlag = kony.store.getItem("langFlagSettingObj");
        if (!(langFlag)) {
            switchLangLayoutKA("ar");
        } else if ((kony.application.getCurrentForm().id === "frmLanguageChange") && langFlag === true) {
            switchLangLayoutKA("ar");
        }
        switchToArabicLayout(rightToLeftCallback);
    } catch (err) {
        kony.print(" --- Error ---" + JSON.stringify(err));
    }
    kony.print(" --- Exit of onArabicSuccesscallback ---");
}

function onArabicFailurecallback() {
    kony.print(" --- Inside onArabicFailurecallback ---");
}

function onClickDone() {
    if (kony.sdk.isNetworkAvailable()) {
        try {
            var langFlagData = kony.store.getItem("langFlagSettingObj");
            var langPrefOption = kony.store.getItem("langPrefObj");
            if (langPrefOption !== null || langPrefOption !== "") {
                if (frmLanguageChange.lblRememCheckBox.text === "q") {
                    kony.store.setItem("langFlagSettingObj", false);
                }
                if (gblPayAppFlow && !isEmpty(gblLaunchModeOBJ.appFlow)) {
                    kony.print("languagetologin::");
                    gblFromModule = "ForgotPassword";
                    gblReqField = "Username";
                    initialiseAndCallEnroll();
                    frmLoginKA.passwordTextField.text = "";
                } else {
                    frmLoginKA.show();
                }
            }
        } catch (err) {
            if (gblPayAppFlow && !isEmpty(gblLaunchModeOBJ.appFlow)) {
                kony.print("languagetologin::");
                gblFromModule = "ForgotPassword";
                gblReqField = "Username";
                initialiseAndCallEnroll();
                frmLoginKA.passwordTextField.text = "";
            } else {
                frmLoginKA.show();
            }
        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.common.appalaunchnointernet"), popupCommonAlertDimissclose, "");
    }
}

function rememberChoice() {
    try {
        kony.print("--- Inside rememberChoice---");
        var rememChoiceTxt = frmLanguageChange.lblRememCheckBox.text;
        if (rememChoiceTxt === "q") {
            kony.store.setItem("langFlagSettingObj", true);
            frmLanguageChange.lblRememCheckBox.text = "p";
            // frmLanguageChange.btnDone.setEnabled(true);
        } else {
            kony.store.setItem("langFlagSettingObj", false);
            frmLanguageChange.lblRememCheckBox.text = "q";
            // frmLanguageChange.btnDone.setEnabled(true);
        }
    } catch (err) {
        kony.print("--- Error ---" + JSON.stringify(err));
    }
    kony.print("--- Exit of rememberChoice---");
}

function rightToLeftCallback() {
    kony.print("@@rightToLeftCallback");
}

function switchLangLayoutKA(lang) {
    kony.print("switchLangLayoutKA:::");
    var leftCheck = "10%";
    var leftRem = "25%";
    if (lang === "en") {
        leftCheck = "10%";
        leftRem = "25%";
        frmLanguageChange.lblLangChangeTxt.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
        frmLanguageChange.lblRememChoice.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
    } else {
        frmLanguageChange.lblLangChangeTxt.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
        frmLanguageChange.lblRememChoice.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
        leftCheck = "75%";
        leftRem = "5%";
    }
    frmLanguageChange.lblRememCheckBox.animate(kony.ui.createAnimation({
        "100": {
            "left": leftCheck,
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.00
    }, {
        "animationStart": null,
        "animationEnd": function() {}
    });
    frmLanguageChange.lblLangChangeTxt.animate(kony.ui.createAnimation({
        "100": {
            "left": leftRem,
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.00
    }, {
        "animationStart": null,
        "animationEnd": function() {}
    });
    frmLanguageChange.lblRememChoice.animate(kony.ui.createAnimation({
        "100": {
            "left": leftRem,
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.00
    }, {
        "animationStart": null,
        "animationEnd": function() {}
    });
}