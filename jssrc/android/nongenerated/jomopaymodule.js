//Type your code here
var jomopayFlag;
var glb_jomopayBene = ""; //edit jomopay beneficiary
var glb_jomopayBeneName = ""; //edit jomopay beneficiary
var v_benef_ID = ""; //edit jomopay beneficiary
function reset_JOMOPAY_SENDMONEY() {
    try {
        frmJoMoPay.lblTransferType.text = geti18Value("i18n.jomopay.type");
        frmJoMoPay.lblAccountType.text = "";
        frmJoMoPay.lblAccountCode.text = "";
        frmJoMoPay.lblType.text = geti18Value("i18n.jomopay.mobiletype");
        frmJoMoPay.txtPhoneNo.text = null;
        frmJoMoPay.txtAliasType.text = "";
        frmJoMoPay.btnContactBook.setVisibility(false);
        frmJoMoPay.txtFieldAmount.text = "";
        frmJoMoPay.lblIncorrect.setVisibility(false);
        frmJoMoPay.txtDescription.text = "";
        frmJoMoPay.txtPhoneNo.setEnabled(true);
        /* hassan */
        frmJoMoPay.btnAliasDropDown.isVisible = true;
        frmJoMoPay.txtAliasType.setVisibility(false);
        frmJoMoPay.txtPhoneNo.isVisible = true;
        frmJoMoPay.lblMobileIDHint.setVisibility(true);
        frmJoMoPay.lblBeneficiaryStaticText.text = geti18Value("i18n.jomopay.mobile");
        // 		frmJoMoPay.lblBeneficiaryStaticText.width = "37%";
        var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
        frmJoMoPay.lblAccountType.text = fromAccounts[0].accountName;
        var accountNumber = fromAccounts[0].accountID;
        fromAccounts[0].hiddenAccountNumber = accountNumber;
        frmJoMoPay.lblAccountCode.text = fromAccounts[0].hiddenAccountNumber;
        if (fromAccounts[0].AccNickName !== "") {
            frmJoMoPay.lblAccountType.text = fromAccounts[0].AccNickName;
            frmJoMoPay.lblAccountCode.isVisible = false;
        } else {
            frmJoMoPay.lblAccountType.text = (fromAccounts[0].accountName.length > 22) ? (fromAccounts[0].accountName.substring(0, 22) + "...") : fromAccounts[0].accountName;
            frmJoMoPay.lblAccountCode.isVisible = true;
        }
    } catch (e) {
        kony.print("Exception_reset_JOMOPAY_SENDMONEY ::" + e);
    }
}

function navigate_JOMOPAY_SENDMONEYWITHOUTBILLER() {
    try {
        reset_JOMOPAY_SENDMONEY();
        frmJoMoPay.btnContactBook.setVisibility(true);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        frmJoMoPay.show();
    } catch (e) {
        kony.print("Exception_navigate_JOMOPAY_SENDMONEYWITHOUTBILLER ::" + e);
    }
}

function reset_JOMOPAYADDBILLER() {
    try {
        frmJOMOPayAddBiller.txtName.text = "";
        frmJOMOPayAddBiller.txtNickName.text = "";
        frmJOMOPayAddBiller.txtBeneficiaryMobile.text = "";
        frmJOMOPayAddBiller.txtBeneficiaryAlias.text = "";
        frmJOMOPayAddBiller.lblType.text = geti18Value("i18n.settings.select");
        frmJOMOPayAddBiller.btnFavCheckBox.text = "q";
        frmJOMOPayAddBiller.flxBorderName.skin = "skntextFieldDividerJomoPay";
        frmJOMOPayAddBiller.flxBorderNickName.skin = "skntextFieldDividerJomoPay";
        frmJOMOPayAddBiller.flxBorderBenificiary.skin = "skntextFieldDividerJomoPay";
        frmJOMOPayAddBiller.flxBorderAlias.skin = "skntextFieldDividerJomoPay"; // hassan
        frmJOMOPayAddBiller.lblNameTitle.top = "40%";
        frmJOMOPayAddBiller.lblNameTitle.width = "50%";
        frmJOMOPayAddBiller.lblNameTitle.skin = "lblAccountStaticText";
        frmJOMOPayAddBiller.lblNickNameTitle.top = "40%";
        frmJOMOPayAddBiller.lblNickNameTitle.width = "50%";
        frmJOMOPayAddBiller.lblNickNameTitle.skin = "lblAccountStaticText";
        frmJOMOPayAddBiller.lblBeneficiaryMobileTitle.top = "40%";
        frmJOMOPayAddBiller.lblBeneficiaryMobileTitle.width = "50%";
        frmJOMOPayAddBiller.lblBeneficiaryMobileTitle.skin = "lblAccountStaticText";
        frmJOMOPayAddBiller.lblBeneficiaryAlias.top = "40%"; /* hassan */
        frmJOMOPayAddBiller.lblBeneficiaryAlias.width = "50%"; /* hassan */
        frmJOMOPayAddBiller.lblBeneficiaryAlias.skin = "lblAccountStaticText"; /* hassan */
        frmJOMOPayAddBiller.flxBeneficiaryMobile.isVisible = false; /* hassan */
        frmJOMOPayAddBiller.lblMobileIDHint.setVisibility(false);
        frmJOMOPayAddBiller.flxBeneficiaryAlias.isVisible = false; /* hassan */
    } catch (e) {
        kony.print("Exception_reset_JOMOPAYADDBILLER ::" + e);
    }
}

function navigate_JOMOPAYADDBILLER() {
    try {
        jomopayFlag = "addBene"; //hassan edit jomopay bene
        reset_JOMOPAYADDBILLER();
        frmJOMOPayAddBiller.show();
    } catch (e) {
        kony.print("Exception_navigate_JOMOPAYADDBILLER ::" + e);
    }
}

function validate_JOMOPAYBILLER() {
    try {
        if (frmJOMOPayAddBiller.txtName.text !== "" && frmJOMOPayAddBiller.txtNickName.text !== "" && ((frmJOMOPayAddBiller.flxBeneficiaryMobile.isVisible === true && frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== "") || (frmJOMOPayAddBiller.flxBeneficiaryAlias.isVisible === true && frmJOMOPayAddBiller.txtBeneficiaryAlias.text !== ""))) {
            if (frmJOMOPayAddBiller.flxBeneficiaryMobile.isVisible === true) {
                if (REG_JMP_MOB !== "" && frmJOMOPayAddBiller.txtBeneficiaryMobile.text.length >= 9) {
                    var BEN_JMP_MOB = frmJOMOPayAddBiller.txtBeneficiaryMobile.text;
                    BEN_JMP_MOB = BEN_JMP_MOB.substring(BEN_JMP_MOB.length - 9, BEN_JMP_MOB.length);
                    var JMP_MOB = REG_JMP_MOB.substring(REG_JMP_MOB.length - 9, REG_JMP_MOB.length);
                    if (JMP_MOB === BEN_JMP_MOB) {
                        customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.benf.cantaddyouraccount"), popupCommonAlertDimiss, "");
                        return false;
                    }
                }
            }
            return true;
        }
        if (frmJOMOPayAddBiller.txtName.text === null || frmJOMOPayAddBiller.txtName.text === "") {
            frmJOMOPayAddBiller.flxBorderName.skin = "skntextFieldDividerOrange";
        }
        if (frmJOMOPayAddBiller.txtNickName.text === null || frmJOMOPayAddBiller.txtNickName.text === "") {
            frmJOMOPayAddBiller.flxBorderNickName.skin = "skntextFieldDividerOrange";
        }
        if ((frmJOMOPayAddBiller.txtBeneficiaryMobile.text === null || frmJOMOPayAddBiller.txtBeneficiaryMobile.text === "") && frmJOMOPayAddBiller.flxBeneficiaryMobile.isVisible === true) {
            frmJOMOPayAddBiller.flxBorderBenificiary.skin = "skntextFieldDividerOrange";
        }
        /* hassan */
        if ((frmJOMOPayAddBiller.txtBeneficiaryAlias.text === null || frmJOMOPayAddBiller.txtBeneficiaryAlias.text === "") && frmJOMOPayAddBiller.flxBeneficiaryAlias.isVisible === true) {
            frmJOMOPayAddBiller.flxBorderAlias.skin = "skntextFieldDividerOrange";
        }
        return false;
    } catch (e) {
        kony.print("Exception_validate_JOMOPAYBILLER ::" + e);
    }
}

function serv_ADD_JOMOPAYBILLER() {
    try {
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("ExternalAccounts", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("ExternalAccounts");
        dataObject.addField("custId", custid);
        dataObject.addField("beneficiaryName", frmJOMOPayAddBiller.txtName.text);
        dataObject.addField("nickName", frmJOMOPayAddBiller.txtNickName.text);
        /* hassan */
        if (frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== "" && frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== null && frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== undefined) {
            dataObject.addField("benAcctNo", frmJOMOPayAddBiller.txtBeneficiaryMobile.text);
            dataObject.addField("bene_address1", "M");
        } else {
            dataObject.addField("benAcctNo", frmJOMOPayAddBiller.txtBeneficiaryAlias.text);
            dataObject.addField("bene_address1", "A");
        }
        dataObject.addField("Fav_flag", (frmJOMOPayAddBiller.btnFavCheckBox.text === "q") ? "N" : "Y");
        dataObject.addField("P_BENE_TYPE", "JMP");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("dataObject::" + JSON.stringify(dataObject));
        if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb("create", serviceOptions, success_serv_ADD_JOMOPAYBILLER, failed_serv_ADD_JOMOPAYBILLER);
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } catch (e) {
        kony.print("Exception_serv_ADD_JOMOPAYBILLER :: " + e);
    }
}

function success_serv_ADD_JOMOPAYBILLER(res) {
    try {
        kony.print("Jomopay biller added ::" + JSON.stringify(res));
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        if (res.ErrorCode !== undefined && res.ErrorCode === "00000") {
            var msg = geti18Value("i18n.jomopay.successmsg").split("name");
            update_JOMOPAY_BENLIST = true;
            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), msg[0] + frmJOMOPayAddBiller.txtNickName.text + msg[1], geti18Value("i18n.jomopay.sendmoneyto"), "sendMoneyJomo", geti18Value("i18n.Bene.Addanewbeneficiary"), "BeneficiaryJomo", geti18Value("i18n.common.gotojomopay"), "JoMo");
        } else if (res.ErrorCode === "00444") {
            kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.AddBene.BeneExist"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.gotojomopay"), "JoMo", "", "");
        } else {
            kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), getErrorMessage(res.ErrorCode), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.gotojomopay"), "JoMo", "", "");
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_success_serv_ADD_JOMOPAYBILLER ::" + e);
    }
}

function failed_serv_ADD_JOMOPAYBILLER(err) {
    try {
        kony.print("Jomopay biller add failed ::" + JSON.stringify(err));
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), getErrorMessage(res.ErrorCode), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.gotojomopay"), "JoMo", "", "");
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_failed_serv_ADD_JOMOPAYBILLER ::" + e);
    }
}

function check_JOMOPAYBENEFICIARY_EXIST() {
    try {
        var checkFAV = false,
            checkNORM = false;
        var check_BENEFICIARY_EXISTANCE = function(beneData) {
            if (data !== null && data !== undefined && data.length > 0) {
                for (var i in data) {
                    if (data.benAcctNo.replaceAll("-", "") === frmJoMoPayConfirmation.lblPhoneNo.text) {
                        isAvailable = true;
                        return;
                    }
                }
            }
            return false;
        };
        checkFAV = check_BENEFICIARY_EXISTANCE(kony.boj.beneList.JMP.fav);
        checkNORM = check_BENEFICIARY_EXISTANCE(kony.boj.beneList.JMP.norm);
        return checkFAV || checkNORM;
    } catch (e) {
        kony.print("Exception_check_JOMOPAYBENEFICIARY_EXIST ::" + e);
    }
}

function add_AS_BENEFICIARY_JOMOPAYBILLER() {
    try {
        frmJOMOPayAddBiller.txtBeneficiaryMobile.text = frmJoMoPayConfirmation.lblPhoneNo.text;
        frmJOMOPayAddBiller.show();
    } catch (e) {
        kony.print("Exception_add_AS_BENEFICIARY_JOMOPAYBILLER ::" + e);
    }
}

function QRCodeScanner_callback(crypto_TEXT) {
    kony.print("crypto_TEXT ::" + JSON.stringify(crypto_TEXT));
    frmJoMoPayQRConfirm.lblCypherText.text = "";
    if (kony.sdk.isNetworkAvailable() && !isEmpty(crypto_TEXT)) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        var inputPrams = "";
        var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJEMVParse");
        var encodedSring = encodeURIComponent(crypto_TEXT);
        inputPrams = {
            "emvstring": encodedSring
        };
        EMVString = encodedSring + "";
        kony.print("inputPrams for android " + EMVString);
        kony.print("inputPrams for QRCode " + JSON.stringify(inputPrams));
        appMFConfiguration.invokeOperation("getEVMParse", {}, inputPrams, function(res) {
            assign_PARSED_DATA_TO_FORM(res, crypto_TEXT);
        }, function(err) {
            kony.print("error ::" + JSON.stringify(err));
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        });
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function initiate_QRCodeScanner() {
    try {
        QRCodeScanner.scanQRCode(QRCodeScanner_callback);
    } catch (e) {
        kony.print("Exception_initiate_QRCodeScanner ::" + e);
    }
}

function assign_PARSED_DATA_TO_FORM(data, cypherText) {
    try {
        kony.print("Parsed data ::" + JSON.stringify(data));
        if (data.opstatus === 0 || data.opstatus === "0") {
            if (isEmpty(data.JomoPayData)) {
                customAlertPopup("", kony.i18n.getLocalizedString("i18n.invalidJomopay"), popupCommonAlertDimiss, "");
                return;
            }
            var JomoPayParsedData = data.JomoPayData;
            if (!isEmpty(JomoPayParsedData)) {
                kony.print("JomoPayParsedData " + JSON.stringify(JomoPayParsedData[0]));
                JoMoPayQRCodeAmount = JomoPayParsedData[0].transactionAmount;
                JoMoPayQRCodeMerchantName = JomoPayParsedData[0].merchantName;
                JoMoPayQRCodeMerchantCity = JomoPayParsedData[0].merchantCity;
                JoMoPayQRCodebillingNum = JomoPayParsedData[0].billingNum;
                JoMoPayQRCoderefId = JomoPayParsedData[0].refId;
                JoMoPayQRCodeDateTime = JomoPayParsedData[0].datetime;
                frmJoMoPayQRConfirm.lblCypherText.text = cypherText;
                kony.print("JoMoPayQRCodeAmount " + JoMoPayQRCodeAmount);
                kony.print("JoMoPayQRCodeMerchantCity " + JoMoPayQRCodeMerchantCity);
                kony.print("JoMoPayQRCodeMerchantName " + JoMoPayQRCodeMerchantName);
                kony.print("JoMoPayQRCodebillingNum " + JoMoPayQRCodebillingNum);
                kony.print("JoMoPayQRCoderefId " + JoMoPayQRCoderefId);
                kony.print("JoMoPayQRCodeDateTime " + JoMoPayQRCodeDateTime);
                if (!isEmpty(JomoPayParsedData[0].validation1) && !isEmpty(JomoPayParsedData[0].validation2) && !isEmpty(JomoPayParsedData[0].validation3)) {
                    if (kony.string.equalsIgnoreCase(JomoPayParsedData[0].validation1, "12") && kony.string.equalsIgnoreCase(JomoPayParsedData[0].validation2, "JOQR") && kony.string.equalsIgnoreCase(JomoPayParsedData[0].validation3, "JOMOP")) {
                        kony.print("valid:::::");
                        callJmpRegisterDetailsService();
                    } else {
                        customAlertPopup("", kony.i18n.getLocalizedString("i18n.invalidJomopay"), popupCommonAlertDimiss, "");
                        return;
                    }
                } else {
                    customAlertPopup("", kony.i18n.getLocalizedString("i18n.invalidJomopay"), popupCommonAlertDimiss, "");
                    return;
                }
            } else {
                customAlertPopup("", kony.i18n.getLocalizedString("i18n.invalidJomopay"), popupCommonAlertDimiss, "");
                return;
            }
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_assign_PARSED_DATA_TO_FORM ::" + e);
    }
}