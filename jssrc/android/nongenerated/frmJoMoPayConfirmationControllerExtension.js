/*
 * Controller Extension class for frmJoMoPayConfirmation
 * Developer can edit the existing methods or can add new methods if required
 *
 */
kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};
/**
 * Creates a new Form Controller Extension.
 * @class frmJoMoPayConfirmationControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmJoMoPayConfirmationControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmJoMoPayConfirmationControllerExtension#
     */
    fetchData: function() {
        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
            kony.sdk.mvvm.log.info("success fetching data ", response);
            scopeObj.getController().processData(response);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmJoMoPayConfirmationControllerExtension#
     * @returns {Object} - processed data
     */
    processData: function(data) {
        try {
            var scopeObj = this;
            var processedData = this.$class.$superp.processData.call(this, data);
            this.getController().bindData(processedData);
            return processedData;
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        };
    },
    /** 
     * This method binds the processed data to the form. Developer can edit.
     * Default implementation binds the data to widgets in the form.
     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmJoMoPayConfirmationControllerExtension#
     */
    bindData: function(data) {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.clear();
            this.$class.$superp.bindData.call(this, data);
            this.getController().getFormModel().formatUI();
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            this.getController().showForm();
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmJoMoPayConfirmationControllerExtension#
     */
    saveData: function() {
        try {
            var scopeObj = this;
            kony.print("In Save Data::");
            logObj[0] = frmJoMoPayConfirmation.lblFromAccountNumber.text; //frmAccount,
            logObj[1] = frmJoMoPayConfirmation.lblSourceBranchCode.text; //frmAccountBr,
            kony.print("frmJoMoPayConfirmation.lblfromAccountCurrency.text::" + frmJoMoPayConfirmation.lblfromAccountCurrency.text);
            logObj[2] = frmJoMoPayConfirmation.lblfromAccountCurrency.text; //frmAccountCurr,
            logObj[9] = frmJoMoPayConfirmation.lblJomopayType.text; //jomopaytype,
            logObj[10] = frmJoMoPayConfirmation.lblPhoneNo.text; //jomopaytypeval, 
            // logObj[11] =frmJoMoPayConfirmation.lblfromAccountCurrency.text; //transfercurr,
            logObj[12] = frmJoMoPayConfirmation.lblAmount.text; //transferamt,
            logObj[13] = "JOMOPAY"; //transferType,
            kony.print(" frmJoMoPayConfirmation.lblDescription.text::" + frmJoMoPayConfirmation.lblDescription.text);
            if (frmJoMoPayConfirmation.lblDescription.text !== null && frmJoMoPayConfirmation.lblDescription.text !== "") {
                logObj[19] = frmJoMoPayConfirmation.lblDescription.text; //description
            } else {
                logObj[19] = " "; //description
            }
            this.$class.$superp.saveData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully created record
            kony.sdk.mvvm.log.info("success saving record ", res);
            kony.print("Save Data FRMJOMOPAYCONFIRMATION SUCCESS ::" + JSON.stringify(res));
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            if ((res.opstatus === 0 || res.opstatus == "0")) {
                if ((res.referenceId !== undefined) && (res.referenceId !== null) && (res.referenceId !== "") && (res.ErrorCode === "70000" || res.ErrorCode === "00000")) {
                    logObj[16] = res.referenceId; // reference number
                    logObj[17] = "SUCCESS"; //status
                    logObj[18] = "JoMoPay Transfer Successful";
                    loggerCall();
                    if (check_JOMOPAYBENEFICIARY_EXIST()) {
                        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.jomopay.transfersuccess"), geti18Value("i18n.common.gotodashboard"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.gotojomopay"), "JoMo", geti18Value("i18n.jomopay.addasbene"), "addBeneJOMOPAY");
                    } else {
                        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.jomopay.transfersuccess"), geti18Value("i18n.common.gotodashboard"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.gotojomopay"), "JoMo");
                    }
                    frmCongratulations.show();
                } else {
                    var errorCode = res.ErrorCode || res.code;
                    logObj[16] = errorCode; // reference number
                    logObj[17] = "FAILURE"; //status,
                    kony.print("getErrorMessage(res.ErrorCode)" + getErrorMessage(errorCode));
                    var message = getErrorMessage(errorCode);
                    if (errorCode === "70003" || errorCode == 7003) {
                        message = geti18Value("i18n.jomopay.invalidmobilenumber");
                    }
                    if (isEmpty(message)) {
                        logObj[18] = message;
                    } else if (message.length > 200) {
                        logObj[18] = message.substring(0, 200); // statuscomments
                    } else {
                        logObj[18] = message;
                    }
                    loggerCall();
                    kony.boj.populateSuccessScreen("failure.png", getErrorMessage(errorCode), geti18Value("i18n.jomopay.transferfailed"), geti18Value("i18n.common.gotodashboard"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.gotojomopay"), "JoMo");
                    //                   customAlertPopup("", message, popupCommonAlertDimiss, "");	
                }
            } else if (res.ErrorCode !== null && res.ErrorCode !== undefined && res.ErrorCode !== "" && (res.ErrorCode !== "70000" || res.ErrorCode !== "00000")) {
                kony.print("Having Error Code res.ErrorCode::" + res.ErrorCode);
                logObj[16] = res.ErrorCode; // reference number
                logObj[17] = "FAILURE"; //status,
                kony.print("getErrorMessage(res.ErrorCode)" + getErrorMessage(res.ErrorCode));
                var message = getErrorMessage(res.ErrorCode);
                if (res.ErrorCode === "70003" || res.ErrorCode == 7003) {
                    message = geti18Value("i18n.jomopay.invalidmobilenumber");
                }
                if (isEmpty(message)) {
                    logObj[18] = message;
                } else if (message.length > 200) {
                    logObj[18] = message.substring(0, 200); // statuscomments
                } else {
                    logObj[18] = message;
                }
                loggerCall();
                kony.boj.populateSuccessScreen("failure.png", getErrorMessage(res.ErrorCode), geti18Value("i18n.jomopay.transferfailed"), geti18Value("i18n.common.gotodashboard"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.gotojomopay"), "JoMo");
                //               customAlertPopup("", message, popupCommonAlertDimiss, "");	
            }
            frmJoMoPay.destroy();
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
            kony.print("Save Data FRMJOMOPAYCONFIRMATION ERROR ::" + JSON.stringify(err));
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            var errorCode = err.ErrorCode || err.code;
            logObj[16] = errorCode; // reference number
            logObj[17] = "FAILURE"; //status,
            kony.print("getErrorMessage(err.ErrorCode)" + getErrorMessage(errorCode));
            var message = getErrorMessage(errorCode);
            if (isEmpty(message)) {
                logObj[18] = message;
            } else if (message.length > 200) {
                logObj[18] = message.substring(0, 200); // statuscomments
            } else {
                logObj[18] = message;
            }
            loggerCall();
            kony.boj.populateSuccessScreen("failure.png", message, geti18Value("i18n.jomopay.transferfailed"), geti18Value("i18n.common.gotodashboard"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.gotojomopay"), "JoMo");
            frmJoMoPay.destroy();
            //           customAlertPopup("", message, popupCommonAlertDimiss, "");	
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmJoMoPayConfirmationControllerExtension#
     */
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method shows form.
     * @memberof frmJoMoPayConfirmationControllerExtension#
     */
    showForm: function() {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.showView();
        } catch (e) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    }
});