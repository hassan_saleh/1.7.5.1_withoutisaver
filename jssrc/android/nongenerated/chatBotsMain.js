kony = kony || {};
/**
 *chatbots namespace
 */
kony.chatbots = kony.chatbots || {};
/**
 *bot namespace in chatbots
 */
kony.chatbots.bot = kony.chatbots.bot || {};
/**
 *user namespace in chatbots
 */
kony.chatbots.user = kony.chatbots.user || {};
/**
 *util namespace in chatbots
 */
kony.chatbots.util = kony.chatbots.util || {};
//variable declaration
kony.chatbots.bot.icon = "bot.png";
/**
 *this function adds the bot icon and message
 * @param {string} message- the text to be displayed
 */
kony.chatbots.bot.addMessageWithIcon = function(message) {
    try {
        kony.print("$$-- start bot.addMessageWithIcon function --$$");
        if (message !== null && message !== undefined && kony.chatbots.bot.icon) {
            kony.print("$$-- got the message " + JSON.stringify(message) + "--$$");
            this.addIcon();
            var isIconAdded = true;
            this.addMessage(isIconAdded, message);
        }
        kony.print("$$-- end bot.addMessageWithIcon function --$$");
    } catch (err) {
        kony.print("$$-- in catch of bot addMessageWithIcon. error message is :" + err + "--$$");
    }
};
/**
 *this function adds the bot icon
 */
kony.chatbots.bot.addIcon = function() {
    try {
        kony.print("$$-- start bot addIcon function --$$");
        var IMAGE = new kony.ui.Image2({
            "id": "botIcon",
            "left": "0%",
            "top": "3%",
            "height": "30dp",
            "width": "30dp",
            "isVisible": true,
            "src": kony.chatbots.bot.icon
        }, {}, {});
        frmChatBots.flexScrollChatBox.add(IMAGE);
        frmChatBots.flexScrollChatBox.scrollToEnd();
        kony.print("$$-- end bot addIcon function --$$");
    } catch (err) {
        kony.print("in catch of bot addIcon with error message : " + err);
    }
};
/**
 *this function adds the bot message
 * @param {string} message- the text to be displayed
 * @param {boolean} isIconAdded - this is useful to adjust the height of the bot text.
 */
kony.chatbots.bot.addMessage = function(isIconAdded, message) {
    try {
        kony.print("$$--start bot addMessage function --$$");
        var msgTop = "";
        kony.print("$$-- isIconAdded value " + isIconAdded + "--$$");
        if (isIconAdded === true) {
            msgTop = "-29dp";
        } else {
            msgTop = "1%";
        }
        var MESSAGE = new kony.ui.Label({
            "id": "botMessage",
            "text": message,
            "left": "32dp",
            "top": msgTop,
            "width": "preferred",
            "maxWidth": "240dp",
            "isVisible": true,
            "skin": "sknBotText",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "zindex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        frmChatBots.flexScrollChatBox.add(MESSAGE);
        frmChatBots.flexScrollChatBox.scrollToEnd();
        kony.print("$$--end bot addMessage function --$$");
    } catch (err) {
        kony.print("$$-- in catch of bot addeMessage with error message " + err);
    }
};
/**
 *this function adds the time on bot side
 */
kony.chatbots.bot.addTime = function() {
    try {
        var currentTime = kony.chatbots.util.getTime();
        kony.chatbots.addTime("bot", currentTime);
    } catch (err) {}
};
/**
 *this function adds action button on bot side
 * @param {array} data
 * data must be in format of
   data = [
   {
   "text" : "the text for button",
   "onClickCallback" : "function to call after clickingperticulat button (OPTIONAL)
   "parameter" : "parameter to pass to the above callback(OPTIONAL)
   }
   ]
*/
kony.chatbots.bot.addButtons = function(data) {
    try {
        var length = data.length;
        var maxLength = 30;
        var currentLength = 0;
        var temp = [];
        for (var i = 0; i < data.length; i++) {
            currentLength += data[i].text.length + 2;
            if (currentLength <= maxLength) {
                temp.push(data[i]);
            } else {
                if (temp.length > 0) {
                    kony.chatbots.bot.addButtonsHelper(temp);
                    currentLength = data[i].text.length + 2;
                    temp = [];
                    temp.push(data[i]);
                } else {
                    temp.push(data[i]);
                }
            }
        }
        if (temp.length > 0) {
            kony.chatbots.bot.addButtonsHelper(temp);
        }
    } catch (err) {
        alert(err + JSON.stringify(err));
    }
};
kony.chatbots.bot.addButtonsHelper = function(data) {
    try {
        var buttonLayOut = {
            "margin": [2, 2, 2, 2],
            "contentAlignment": constants.CONTENT_ALIGN_CENTER
        };
        var HorizontalFlex = new kony.ui.FlexContainer({
            "id": "botHorizontalFlex",
            "left": "30dp",
            "top": "6dp",
            "wigth": "90%",
            "height": "40dp",
            "zIndex": 1,
            "isVisible": true,
            "clipBounds": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL
        }, {
            "vExpand": false
        }, {});
        HorizontalFlex.setDefaultUnit(kony.flex.DP);
        for (var i = 0; i < data.length; i++) {
            var BUTTON = new kony.ui.Button({
                "id": "botButton" + i,
                "text": data[i].text,
                "left": "4dp",
                "height": "40dp",
                "width": "preferred",
                "top": "0%",
                "skin": "sknBotButton",
                "focusSkin": "sknBotButtonFocus",
                "onClick": kony.chatbots.bot.buttonClickHandler.bind(this, data[i])
            }, buttonLayOut, {});
            HorizontalFlex.add(BUTTON);
        }
        frmChatBots.flexScrollChatBox.add(HorizontalFlex);
        frmChatBots.flexScrollChatBox.scrollToEnd();
    } catch (err) {}
};
kony.chatbots.bot.buttonClickHandler = function(data, object) {
    var callback = data.onClickCallback;
    if (callback) {
        callback(data.parameter);
    }
};
kony.chatbots.bot.showLoading = function() {
    try {
        this.addIcon();
        var IMAGE = new kony.ui.Image2({
            "id": "botLoading",
            "isVisible": true,
            "src": "botload.gif",
            "left": "33dp",
            "top": "-29dp",
            "height": "40dp",
            "width": "100dp"
        }, {
            imageScaleMode: constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS
        }, {});
        frmChatBots.flexScrollChatBox.add(IMAGE);
        frmChatBots.flexScrollChatBox.scrollToEnd();
    } catch (err) {
        alert(err + JSON.stringify(err));
    }
};
kony.chatbots.bot.dismissLoading = function() {
    try {
        var widgets = frmChatBots.flexScrollChatBox.widgets();
        var widgetsLength = widgets.length;
        frmChatBots.flexScrollChatBox.removeAt(widgetsLength - 1);
        frmChatBots.flexScrollChatBox.removeAt(widgetsLength - 2);
    } catch (err) {}
};
/**
 *this function adds the user message
 * @param {string} message- the text to be displayed
 */
kony.chatbots.user.addMessage = function(message) {
    try {
        kony.print("$$-- in adding Message for user --$$");
        var MESSAGE = new kony.ui.Label({
            "id": "userMessage",
            "text": message,
            "right": "3%",
            "top": "3%",
            "width": "preferred",
            "maxWidth": "225dp",
            "isVisible": true,
            "skin": "sknUserText",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "zindex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        frmChatBots.flexScrollChatBox.add(MESSAGE);
        frmChatBots.flexScrollChatBox.scrollToEnd();
    } catch (err) {
        kony.print("in catch of user addMessage with error message :" + err);
    }
};
/**
 *this function adds the time on user side
 */
kony.chatbots.user.addTime = function() {
    try {
        var currentTime = kony.chatbots.util.getTime();
        kony.chatbots.addTime("user", currentTime);
    } catch (err) {}
};
/**
 *this generic function to add time based on type
 * @param {string} type- user or bot
 * @param {string} currentTime - like "03:12 P.M"
 */
kony.chatbots.addTime = function(type, currentTime) {
    try {
        var left = "";
        var right = "";
        if (type === "bot") {
            left = "36dp";
            right = "default";
        }
        if (type === "user") {
            left = "default";
            right = "15dp";
        }
        var TIME = new kony.ui.Label({
            "id": "botOrUserTime",
            "text": currentTime,
            "left": left,
            "right": right,
            "top": "2dp",
            "width": "preferred",
            "height": "preferred",
            "maxWidth": "200dp",
            "isVisible": true,
            "skin": "sknBotUserTime",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        frmChatBots.flexScrollChatBox.add(TIME);
        frmChatBots.flexScrollChatBox.scrollToEnd();
    } catch (err) {}
};
/**
 *this function return the current time in required format
 * @output-param {string} format-"03:12 P.M"
 */
kony.chatbots.util.getTime = function() {
    try {
        var date = new Date();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var tailString = "A.M";
        if (hours >= 0 && hours < 12) {
            tailString = "A.M";
        }
        if (hours === 12) {
            tailString = "P.M";
        }
        if (hours > 12 && hours < 24) {
            hours = hours - 12;
            tailString = "P.M";
        }
        hours = (String(hours).length > 1) ? (hours) : ("0" + hours);
        minutes = (String(minutes).length > 1) ? (minutes) : ("0" + minutes);
        var timeString = hours + ":" + minutes + " " + tailString;
        return timeString;
    } catch (err) {}
};

function onClickButnBot() {
    var text = frmChatBots.txtBoxUserText.text;
    var text1 = text.split("-");
    text = text1[0];
    if (text === "bot") {
        kony.chatbots.bot.addMessageWithIcon(text1[1]);
    }
    if (text === "user") {
        kony.chatbots.user.addMessage(text1[1]);
    }
    if (text === "ut") {
        kony.chatbots.user.addTime();
    }
    if (text === "bt") {
        kony.chatbots.bot.addTime();
    }
    if (text === "btn") {
        var data = [{
            "text": "View Balance",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "Transfer Money",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "ATM",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "Apply for credit card",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "NewUser Onboarding",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "Block Card",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "Make Bill Payment",
            "onClickCallback": "",
            "parameter": true
        }, {
            "text": "need Loan",
            "onClickCallback": "",
            "parameter": true
        }];
        kony.chatbots.bot.addButtons(data);
    }
    if (text === "load") {
        kony.chatbots.bot.showLoading();
    }
    if (text === "dis") {
        kony.chatbots.bot.dismissLoading();
    }
}