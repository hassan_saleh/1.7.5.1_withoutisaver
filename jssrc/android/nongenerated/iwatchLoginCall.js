function loginCallforWatch(flow) {
    kony.print("Iwatch :: Inside loginCallforWatch call :: Flow" + flow);
    if (kony.sdk.isNetworkAvailable()) {
        var Tokenn = (kony.store.getItem("soft_token"));
        if (Tokenn !== undefined && Tokenn !== null && Tokenn !== "") {
            var DecToken = Dvfn(Tokenn);
            if (DecToken !== undefined && DecToken !== null && DecToken !== "") {
                var devID = getCurrentDeviceId();
                var langSelected = kony.store.getItem("langPrefObj");
                var Language = langSelected.toUpperCase();
                var scopeObj = this;
                var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                var options = {
                    "access": "online",
                    "objectName": "RBObjects"
                };
                var headers = {};
                objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects", options);
                var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
                dataObject.addField("Language", Language);
                dataObject.addField("deviceId", devID);
                dataObject.addField("Token", DecToken);
                var serviceOptions = {
                    "dataObject": dataObject,
                    "headers": headers
                };
                kony.print("Iwatch :: serviceOptions for loginCallforWatch : " + JSON.stringify(serviceOptions));
                objectService.customVerb("getLoginMethod", serviceOptions, function(response) {
                    iwatchtypeAltLoginSuccessCallback(response, flow);
                }, function(err) {
                    typeAltLoginErrorCallback(err, flow);
                });
            } else {
                return_watchrequest({
                    rService: [],
                    "isUserLoggedIn": true,
                    "isNetworkAvailable": true,
                    "isRetry": false
                });
            }
        } else {
            return_watchrequest({
                rService: [],
                "isUserLoggedIn": false,
                "isNetworkAvailable": true,
                "isRetry": true
            });
        }
    } else {
        return_watchrequest({
            rService: [],
            "isUserLoggedIn": true,
            "isNetworkAvailable": false,
            "isRetry": true
        });
    }
}

function iwatchtypeAltLoginSuccessCallback(response, flowFlag) {
    try {
        kony.print("Iwatch :: typeAltLoginSuccessCallback-->" + JSON.stringify(response));
        kony.print("Iwatch :: flow is : " + flowFlag);
        if (response.statusCode !== null && response.statusCode !== "" && response.statusCode == "S0024") {
            kony.print("Iwatch :: status Code : " + response.statusCode);
            iWatchcustid = response.custId;
            decideNextFlow(flowFlag);
        } else {
            //Device is not registerd
            kony.print("Iwatch :: Device is not registered, hence replying as isUserLoggedIn as false");
            return_watchrequest({
                rService: [],
                "isUserLoggedIn": false,
                "isNetworkAvailable": true,
                "isRetry": true
            });
        }
    } catch (err) {
        kony.print("Iwatch :: Catch of Error in iwatchtypeAltLoginSuccessCallback" + err);
        return_watchrequest({
            rService: [],
            "isUserLoggedIn": true,
            "isNetworkAvailable": true,
            "isRetry": false
        });
    }
}

function typeAltLoginErrorCallback(err, flowFlag) {
    kony.print("Iwatch :: Inside typeAltLoginErrorCallback--> :: " + JSON.stringify(err));
    if (isLoggedIn) {
        if (!isEmpty(custid)) {
            iWatchcustid = custid;
            decideNextFlow(flowFlag);
        } else {
            //Please try again
            kony.print("Iwatch :: Inside typeAltLoginErrorCallback Please try again, hence replying as isRetry as false");
            return_watchrequest({
                rService: [],
                "isUserLoggedIn": true,
                "isNetworkAvailable": true,
                "isRetry": false
            });
        }
    } else {
        //Device is not registerd
        kony.print("Iwatch :: Inside typeAltLoginErrorCallback Please try again, hence replying as isRetry as false");
        return_watchrequest({
            rService: [],
            "isUserLoggedIn": true,
            "isNetworkAvailable": true,
            "isRetry": false
        });
    }
}

function decideNextFlow(flowFlag) {
    if (flowFlag == "Login") {
        return_watchrequest({
            "isUserLoggedIn": true,
            "isNetworkAvailable": true,
            "isRetry": true
        });
    } else if (flowFlag == "accounts") {
        iWatchAccountsCall("accounts");
    } else if (flowFlag == "loans") {
        iWatchAccountsCall("loans");
    } else if (flowFlag == "deposits") {
        iWatchAccountsCall("deposits");
    } else if (flowFlag == "loanInfo") {
        var serv_DATA = iwatchdictobj.selectedInfo;
        iWatchLoanAndDepositDetails(serv_DATA, "loanInfo");
    } else if (flowFlag == "depositInfo") {
        var serv_DATAA = iwatchdictobj.selectedInfo;
        iWatchLoanAndDepositDetails(serv_DATAA, "depositInfo");
    } else if (flowFlag == "transactions") {
        iwatch_ACCOUNT_DETAILS = iwatchdictobj.selectedAcc;
        iWatchAccTransactionCall(iwatchdictobj.selectedAcc, {
            "toDATE": "",
            "fromDATE": "",
            "no_TNX": "5"
        });
    } else if (flowFlag == "cards") {
        iWatchCardsCall();
    } else if (flowFlag == "cardTransactions") {
        var cardNo = iwatchdictobj.cardNo;
        var cardFlag = iwatchdictobj.cardFlag;
        var branch = iwatchdictobj.branch;
        var accountNo = iwatchdictobj.accountNo;
        if (cardFlag === "C") {
            iWatchserv_getCrCardTransactions({
                "card_num": cardNo
            }, {
                "toDATE": "",
                "fromDATE": ""
            });
        } else if (cardFlag === "D") {
            iWatchserv_FETCHDEBITCARDTRANSACTION({
                "card_number": cardNo,
                "card_acc_num": accountNo,
                "card_acc_branch": branch
            }, {
                "toDATE": "",
                "fromDATE": ""
            });
        } else if (cardFlag === "W") {
            iWatchserv_getCardTransactions({
                "card_number": cardNo,
                "card_acc_num": accountNo,
                "card_acc_branch": branch
            }, {
                "toDATE": "",
                "fromDATE": ""
            });
        }
    }
}