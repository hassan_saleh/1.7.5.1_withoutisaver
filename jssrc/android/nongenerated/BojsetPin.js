function fnauthorizationAlternativePreShow(prevForm) {
    kony.print("Inside authorizationAlternativePreShow");
    //var prevForm = kony.application.getCurrentForm().id;
    kony.print("prevForm:: curr :: " + prevForm);
    if (prevForm == "frmSettingsKA") {
        frmAuthorizationAlternatives.lblAuthorization.setVisibility(false);
        frmAuthorizationAlternatives.lblAuthBody.setVisibility(false);
        frmAuthorizationAlternatives.flxAlterAuthTouchID.setVisibility(false);
        frmAuthorizationAlternatives.flxAltAuthPIN.setVisibility(false);
    } else {
        frmAuthorizationAlternatives.lblAuthorization.setVisibility(true);
        frmAuthorizationAlternatives.lblAuthBody.setVisibility(true);
        frmAuthorizationAlternatives.flxAlterAuthTouchID.setVisibility(true);
        frmAuthorizationAlternatives.flxAltAuthPIN.setVisibility(true);
        var isPinSupported = kony.store.getItem("isPinSupported");
        var isTouchSupported = kony.store.getItem("isTouchSupported");
        if (isPinSupported != null && isPinSupported != undefined && isPinSupported != "" && isPinSupported == "Y") {
            frmAuthorizationAlternatives.flxSwitchOffPinLogin.setVisibility(false);
            frmAuthorizationAlternatives.flxSwitchOnPinLogin.setVisibility(true);
        } else {
            frmAuthorizationAlternatives.flxSwitchOffPinLogin.setVisibility(true);
            frmAuthorizationAlternatives.flxSwitchOnPinLogin.setVisibility(false);
        }
        if (kony.retailBanking.globalData.deviceInfo.isTouchIDSupported()) {
            var model = kony.os.deviceInfo().model;
            if (checkFaceIdIphone()) {
                frmAuthorizationAlternatives.lblAlternateMethodtext.text = geti18nkey("i18n.common.FaceID");
            } else {
                frmAuthorizationAlternatives.lblAlternateMethodtext.text = geti18nkey("i18n.common.touchID");
            }
            if (isTouchSupported != null && isTouchSupported != undefined && isTouchSupported != "" && isTouchSupported == "Y") {
                frmAuthorizationAlternatives.flxSwitchOffTouchLogin.setVisibility(false);
                frmAuthorizationAlternatives.flxSwitchOnTouchLogin.setVisibility(true);
            } else {
                frmAuthorizationAlternatives.flxSwitchOffTouchLogin.setVisibility(true);
                frmAuthorizationAlternatives.flxSwitchOnTouchLogin.setVisibility(false);
            }
        } else {
            frmAuthorizationAlternatives.flxAlterAuthTouchID.setVisibility(false);
        }
    }
}

function onclickPinSet() {
    frmUserSettingsPinLoginKA.show();
}

function ShowUserSettigsTouchPage() {
    var currentform = kony.application.getCurrentForm().id;
    if (currentform == "frmSettingsKA") {
        // do nothing
    } else {
        var status = kony.localAuthentication.getStatusForAuthenticationMode(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID);
        kony.print("Status for the touch/faceid :: " + status);
        if (status == 5007 || status == 5005) {
            var model = kony.os.deviceInfo().model;
            if (checkFaceIdIphone()) {
                customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.FaceID.notEnabledText"), popupCommonAlertDimiss, "");
            } else {
                customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.TOuch.notEnabledText"), popupCommonAlertDimiss, "");
            }
        } else {
            frmUserSettingsTouchIdKA.flxtouchIdAndrd.setVisibility(false);
            var isTouchSupported = kony.store.getItem("isTouchSupported");
            if (isTouchSupported != null && isTouchSupported != undefined && isTouchSupported != "" && isTouchSupported == "Y") {
                frmUserSettingsTouchIdKA.flxSwitchOffTouchLogin.setVisibility(false);
                frmUserSettingsTouchIdKA.flxSwitchOnTouchLogin.setVisibility(true);
            } else {
                frmUserSettingsTouchIdKA.flxSwitchOffTouchLogin.setVisibility(true);
                frmUserSettingsTouchIdKA.flxSwitchOnTouchLogin.setVisibility(false);
            }
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            var model = kony.os.deviceInfo().model;
            if (checkFaceIdIphone()) {
                frmUserSettingsTouchIdKA.lblTouchIdHeader.text = geti18nkey("i18n.common.FaceID");
                frmUserSettingsTouchIdKA.lblTouchBodytxt.text = geti18nkey("i18n.common.FaceIDText");
                frmUserSettingsTouchIdKA.lblAltText.text = geti18nkey("i18n.common.FaceIDEnable");
            } else {
                frmUserSettingsTouchIdKA.lblTouchIdHeader.text = geti18nkey("i18n.common.touchID");
                frmUserSettingsTouchIdKA.lblTouchBodytxt.text = geti18nkey("i18n.common.touchText");
                frmUserSettingsTouchIdKA.lblAltText.text = geti18nkey("i18n.common.touchIDEnable");
            }
            frmUserSettingsTouchIdKA.show();
        }
    }
}
//TOuchID enableMent
function showTouchEnableFlex() {
    var model = kony.os.deviceInfo().model;
    if (checkFaceIdIphone()) {
        customAlertPopup(geti18Value("i18n.common.alert"), kony.i18n.getLocalizedString("i18n.common.FaceIDAlertMsg"), touchShowOrNot, popupCommonAlertDimiss, geti18Value("i18n.common.acceptAndActivate"), geti18Value("i18n.common.cancelC"));
    } else {
        customAlertPopup(geti18Value("i18n.common.alert"), kony.i18n.getLocalizedString("i18n.common.TouchAlertMsg"), touchShowOrNot, popupCommonAlertDimiss, geti18Value("i18n.common.acceptAndActivate"), geti18Value("i18n.common.cancelC"));
    }
}

function touchShowOrNot() {
    kony.print("touchShowOrNot");
    var config = {};
    popupCommonAlertDimiss();
    if (kony.retailBanking.globalData.deviceInfo.isIphone()) {
        kony.print("touchShowOrNot + isIphone");
        config = {
            "promptMessage": kony.i18n.getLocalizedString("i18n.touchIdMsg"),
            "fallbackTitle": ""
        };
        kony.localAuthentication.authenticate(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID, statusCB12, config);
    } else {
        kony.print("touchShowOrNot + not Iphone");
        frmUserSettingsTouchIdKA.flxtouchIdAndrd.setVisibility(true);
        kony.localAuthentication.authenticate(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID, authCallBack12, config);
    }
}

function authCallBack12(resStatus, msg) {
    if (resStatus == 5000) {
        kony.print("Success touch id");
        //Call to enable TOuch ID for this user
        EnableDisableFingerPrintLogin();
    } else {
        kony.print("failed touch id--. do nothing");
        kony.localAuthentication.cancelAuthentication();
        frmUserSettingsTouchIdKA.flxtouchIdAndrd.setVisibility(false);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }
}

function statusCB12(status, message) {
    if (status == 5000) {
        kony.print("Success touch id");
        //Call to enable TOuch ID for this user
        EnableDisableFingerPrintLogin();
    } else {
        kony.print("failed touch id--. do nothing");
        ///kony.localAuthentication.cancelAuthentication();
        frmUserSettingsTouchIdKA.flxtouchIdAndrd.setVisibility(false);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }
}

function EnableDisableFingerPrintLogin() {
    try {
        if (kony.sdk.isNetworkAvailable()) {
            kony.print("Inside rhi");
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            var devID = deviceid;
            var langSelected = kony.store.getItem("langPrefObj");
            var Language = langSelected.toUpperCase();
            var scopeObj = this;
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var options = {
                "access": "online",
                "objectName": "RBObjects"
            };
            var headers = {};
            var serviceName = "RBObjects";
            var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
            var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
            var Token = Dvfn(kony.store.getItem("soft_token"));
            kony.print("Perf Log: Token - start->" + Token);
            var FlowFlag = "N";
            if (frmUserSettingsTouchIdKA.flxSwitchOffTouchLogin.isVisible) {
                FlowFlag = "Y";
            }
            dataObject.addField("Language", Language);
            dataObject.addField("custId", custid);
            dataObject.addField("FlowFlag", FlowFlag);
            dataObject.addField("deviceId", devID);
            dataObject.addField("Token", Token);
            var serviceOptions = {
                "dataObject": dataObject,
                "headers": headers
            };
            kony.print("serviceOptions for EnableDisableFingerPrintLogin  : " + JSON.stringify(serviceOptions));
            modelObj.customVerb("SetTouchMethod", serviceOptions, TouchSuccessCallBack, TouchErrorCallback);
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } catch (e) {
        exceptionLogCall("kony.boj.updateBeneSegment", "UI ERROR", "UI", e);
    }
}

function TouchSuccessCallBack(response) {
    kony.print("TouchSuccessCallBack-->" + JSON.stringify(response));
    if (response.statusCode !== null && response.statusCode !== "" && response.statusCode == "S0024" && response.statusType == "S") {
        if (frmUserSettingsTouchIdKA.flxSwitchOffTouchLogin.isVisible) {
            frmUserSettingsTouchIdKA.flxSwitchOffTouchLogin.setVisibility(false);
            frmUserSettingsTouchIdKA.flxSwitchOnTouchLogin.setVisibility(true);
            kony.store.setItem("isTouchSupported", "Y");
            frmAuthorizationAlternatives.flxSwitchOffTouchLogin.setVisibility(false);
            frmAuthorizationAlternatives.flxSwitchOnTouchLogin.setVisibility(true);
            if (diffFlowFlag == "LOGINFLOW") {
                if (checkFaceIdIphone()) kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.thankyou"), geti18Value("i18n.faceid.successmessage"), geti18Value("i18n.common.gotoPre"), "gotofrmAuthorizationAlternatives");
                else kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.thankyou"), geti18Value("i18n.touchid.successmessage"), geti18Value("i18n.common.gotoPre"), "gotofrmAuthorizationAlternatives");
            } else {}
        } else {
            frmUserSettingsTouchIdKA.flxSwitchOffTouchLogin.setVisibility(true);
            frmUserSettingsTouchIdKA.flxSwitchOnTouchLogin.setVisibility(false);
            kony.store.removeItem("isTouchSupported");
            frmAuthorizationAlternatives.flxSwitchOffTouchLogin.setVisibility(true);
            frmAuthorizationAlternatives.flxSwitchOnTouchLogin.setVisibility(false);
            decideFlow();
        }
    } else {
        frmUserSettingsTouchIdKA.flxSwitchOffTouchLogin.setVisibility(true);
        frmUserSettingsTouchIdKA.flxSwitchOnTouchLogin.setVisibility(false);
        frmUserSettingsTouchIdKA.flxtouchIdAndrd.setVisibility(false);
        customAlertPopup(geti18Value("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    }
    frmUserSettingsTouchIdKA.flxtouchIdAndrd.setVisibility(false);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    //     popupCommonAlertDimiss();
}

function TouchErrorCallback(response) {
    kony.print("TouchErrorCallback-->" + JSON.stringify(response));
    frmUserSettingsTouchIdKA.flxSwitchOffTouchLogin.setVisibility(true);
    frmUserSettingsTouchIdKA.flxSwitchOnTouchLogin.setVisibility(false);
    frmUserSettingsTouchIdKA.flxtouchIdAndrd.setVisibility(false);
    //     popupCommonAlertDimiss();
    customAlertPopup(geti18Value("i18n.NUO.Error"), geti18Value("i18n.common.defaultErrorMsg"), popupCommonAlertDimiss, "");
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function frmUSPinLoginPreShow() {
    kony.print("--> Inside frmUserSettingsPinLoginKAPreShow->1 @@@");
    flagClicked = "";
    enter_count = 0;
    pass = "";
    firstPass = "";
    kony.print("--> @@@Inside frmUserSettingsPinLoginKAPreShow 2 ");
    kony.print("frmUserSettingsPinLoginKAPreShow isPinSupported  3 : ");
    var isPinSupported = kony.store.getItem("isPinSupported");
    kony.print("frmUserSettingsPinLoginKAPreShow isPinSupported   : 4");
    if (isPinSupported != null && isPinSupported != undefined && isPinSupported != "" && isPinSupported == "Y") {
        kony.print("frmUserSettingsPinLoginKAPreShow isPinSupported   : " + isPinSupported);
        frmUserSettingsPinLoginKA.flxSwitchOffTouchLogin.setVisibility(false);
        frmUserSettingsPinLoginKA.flxSwitchOnTouchLogin.setVisibility(true);
    } else {
        frmUserSettingsPinLoginKA.flxSwitchOffTouchLogin.setVisibility(true);
        frmUserSettingsPinLoginKA.flxSwitchOnTouchLogin.setVisibility(false);
    }
    if (frmUserSettingsPinLoginKA.flxSwitchOnTouchLogin.isVisible) {
        frmUserSettingsPinLoginKA.btnSetResetPin.setVisibility(true);
        frmUserSettingsPinLoginKA.btnSetResetPin.text = geti18Value("i18n.common.ResetPin");
    } else {
        frmUserSettingsPinLoginKA.btnSetResetPin.text = geti18Value("i18n.common.setPin");
        frmUserSettingsPinLoginKA.btnSetResetPin.setVisibility(false);
    }
}

function frmsetPinSwitchOff() {
    frmUserSettingsPinLoginKA.flxSwitchOffTouchLogin.setVisibility(false);
    frmUserSettingsPinLoginKA.flxSwitchOnTouchLogin.setVisibility(true);
    frmUserSettingsPinLoginKA.btnSetResetPin.setVisibility(true);
    frmUserSettingsPinLoginKA.btnSetResetPin.text = geti18Value("i18n.common.setPin");
}

function frmsetPinSwitchOnfn() {
    EnableDisablePinLogin();
}

function EnableDisablePinLogin() {
    if (kony.sdk.isNetworkAvailable()) {
        kony.print("Inside EnableDisablePinLogin");
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        var devID = deviceid;
        var langSelected = kony.store.getItem("langPrefObj");
        var Language = langSelected.toUpperCase();
        var scopeObj = this;
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
        var Token = Dvfn(kony.store.getItem("soft_token"));
        kony.print("Perf Log: Token - start->" + Token);
        var pin_pass = "";
        var confirm_pin_pass = "";
        var FlowFlag = "N";
        if (frmUserSettingsPinLoginKA.flxSwitchOffTouchLogin.isVisible || flagClicked) {
            FlowFlag = "Y";
            pin_pass = firstPass;
            confirm_pin_pass = firstPass;
        }
        dataObject.addField("Language", Language);
        dataObject.addField("custId", custid);
        dataObject.addField("FlowFlag", FlowFlag);
        dataObject.addField("deviceId", devID);
        dataObject.addField("Token", Token);
        dataObject.addField("pin_pass", pin_pass);
        dataObject.addField("confirm_pin_pass", confirm_pin_pass);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("serviceOptions for EnableDisableFingerPrintLogin  : " + JSON.stringify(serviceOptions));
        modelObj.customVerb("setResetPin", serviceOptions, PinSuccessCallBack, PinErrorCallback);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function PinSuccessCallBack(response) {
    kony.print("PinSuccessCallBack-->" + JSON.stringify(response));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    enter_count = 0;
    pass = "";
    firstPass = "";
    if (response.statusCode != null && response.statusCode != "" && response.statusCode == "S0024" && response.statusType == "S") {
        kony.print(frmUserSettingsPinLoginKA.flxSwitchOffTouchLogin.isVisible);
        if (frmUserSettingsPinLoginKA.flxSwitchOffTouchLogin.isVisible) {
            kony.store.setItem("isPinSupported", "Y");
            kony.print("diffFlowFlag :: " + diffFlowFlag);
            if (diffFlowFlag == "LOGINFLOW") {
                kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.thankyou"), geti18Value("i18n.pin.successmessage"), geti18Value("i18n.common.gotoPre"), "gotofrmAuthorizationAlternatives");
            } else {
                kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.thankyou"), geti18Value("i18n.pin.successmessage"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.gotoSet"), "gotoSettings");
            }
        } else {
            kony.print("btn thing: " + frmUserSettingsPinLoginKA.btnSetResetPin.isVisible)
            if (frmUserSettingsPinLoginKA.btnSetResetPin.isVisible && flagClicked) {
                kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.registration.congratulations"), geti18Value("i18n.common.succmsgLog"), geti18Value("i18n.common.GoToLogin"), "LogoutAction", "", "");
            } else {
                kony.store.removeItem("isPinSupported");
                flagClicked = false;
                frmUserSettingsPinLoginKA.flxSwitchOffTouchLogin.setVisibility(true);
                frmUserSettingsPinLoginKA.flxSwitchOnTouchLogin.setVisibility(false);
                frmUserSettingsPinLoginKA.btnSetResetPin.isVisible = false;
            }
        }
    } else {
        customAlertPopup(geti18Value("i18n.NUO.Error"), kony.i18n.getLocalizedString("i18n.pinerror.text"), decideFlow, "");
    }
}

function PinErrorCallback(response) {
    kony.print("PinErrorCallback-->" + JSON.stringify(response));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    enter_count = 0;
    pass = "";
    firstPass = "";
    customAlertPopup(geti18Value("i18n.NUO.Error"), kony.i18n.getLocalizedString("i18n.pinerror.text"), decideFlow, "");
}

function btnSetResetPinonClick() {
    //set pin flow
    frmPinEntryStep1.show();
}

function decideFlow() {
    kony.print("diffFlowFlag :: " + diffFlowFlag);
    if (diffFlowFlag == "LOGINFLOW") {
        frmAuthorizationAlternatives.show();
    } else {
        frmSettingsKA.show();
    }
    popupCommonAlertDimiss();
}

function gotofrmAuthorizationAlternatives() {
    frmAuthorizationAlternatives.show();
}

function gotoSettings() {
    popupCommonAlert.dismiss();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmSettingsKA.show();
}