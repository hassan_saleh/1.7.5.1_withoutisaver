function accountListPreshow() {
    var freqData = [];
    if (gblsubacclist == "Acc") {
        frmAccountType.lblHead.text = geti18Value("i18n.common.accountType");
        freqData = [{
            "Frequency": geti18Value("i18n.common.savingsAccount"),
            "imgIcon": "savings_account.png",
            template: flxFrequency
        }, {
            "Frequency": geti18Value("i18n.accounts.currentAccount"),
            "imgIcon": "current_account.png",
            template: flxFrequency
        }, {
            "Frequency": geti18Value("i18n.common.isavingsAccount"), //mai 19/1/2021
            "imgIcon": "savings_account.png",
            template: flxFrequency
        }];
    } else if (gblsubacclist == "AccountOrder") {
        frmAccountType.lblHead.text = geti18Value("i18n.my_money.accounts");
        var accountsData = kony.retailBanking.globalData.accountsDashboardData.accountsData;
        if (!isEmpty(accountsData)) {
            for (var i in accountsData) {
                if (accountsData[i]["accountType"] == "C") {
                    freqData.push({
                        "Frequency": accountsData[i]["accountID"],
                    });
                }
            }
        }
    } else if (gblsubacclist == "AccNumberStatement") {
        frmAccountType.lblHead.text = geti18Value("i18n.my_money.accounts");
        var accountsData = kony.retailBanking.globalData.accountsDashboardData.accountsData;
        if (!isEmpty(accountsData)) {
            for (var i in accountsData) {
                freqData.push({
                    "Frequency": accountsData[i]["accountID"],
                });
            }
        }
    } else if (gblsubacclist == "Curr") {
        frmAccountType.lblHead.text = geti18Value("i18n.common.currencyType");
        var temp = null;
        setgblcurrencyfullCountryListt();
        for (var i in gblCurrList) {
            if (gblCurrList[i].CURR_ISO === CURRENCY) {
                temp = gblCurrList[i];
            }
        }
        //mai 19/1/2021 add isaver sub account with JOD only
        kony.print("Previous form:: " + prev_FORM);
        kony.print(JSON.stringify(frmNewSubAccountLandingNew.lblFre));
        if (kony.application.getPreviousForm().id === "frmNewSubAccountLandingNew" && frmNewSubAccountLandingNew.lblFre !== null && frmNewSubAccountLandingNew.lblFre.text === geti18Value("i18n.common.isavingsAccount")) {
            freqData = [{
                "lblCurrency": temp.CURR_ISO, //"JOD",
                "imgCountryIcon": temp.IMG_FLAG, //"jordanflag.png",
                "lblDescriptionEn": (kony.store.getItem("langPrefObj") === "en") ? temp.ENGLISH_DESC : "",
                "lblDescriptionAr": (kony.store.getItem("langPrefObj") === "en") ? "" : temp.ARABIC_DESC,
                template: tmpFlxCurrency
            }];
        } else {
            //USD, JOD, UKPound, Euro
            freqData = [{
                "lblCurrency": "USD",
                "imgCountryIcon": "unitedstatesflag.png",
                "lblDescriptionEn": (kony.store.getItem("langPrefObj") === "en") ? "U.S.Dollars" : "",
                "lblDescriptionAr": (kony.store.getItem("langPrefObj") === "en") ? "" : "دولار أمريكي",
                template: tmpFlxCurrency
            }, {
                "lblCurrency": "GBP",
                "imgCountryIcon": "unitedkingdomflag.png",
                "lblDescriptionEn": (kony.store.getItem("langPrefObj") === "en") ? "Sterling Pound" : "",
                "lblDescriptionAr": (kony.store.getItem("langPrefObj") === "en") ? "" : "جنيه اسـترليني",
                template: tmpFlxCurrency
            }, {
                "lblCurrency": "EUR",
                "imgCountryIcon": "europeanunionflag.png",
                "lblDescriptionEn": (kony.store.getItem("langPrefObj") === "en") ? "Euro Currency Unit" : "",
                "lblDescriptionAr": (kony.store.getItem("langPrefObj") === "en") ? "" : "يورو",
                template: tmpFlxCurrency
            }, {
                "lblCurrency": temp.CURR_ISO, //"JOD",
                "imgCountryIcon": temp.IMG_FLAG, //"jordanflag.png",
                "lblDescriptionEn": (kony.store.getItem("langPrefObj") === "en") ? temp.ENGLISH_DESC : "",
                "lblDescriptionAr": (kony.store.getItem("langPrefObj") === "en") ? "" : temp.ARABIC_DESC,
                template: tmpFlxCurrency
            }];
        }
    } else if (gblsubacclist == "Leaves") {
        frmAccountType.lblHead.text = geti18Value("i18n.ordercheck.numberofLeaves");
        freqData = [{
            "Frequency": "10",
        }, {
            "Frequency": "20",
        }, {
            "Frequency": "40",
        }];
    } else if (gblsubacclist == "Book") {
        frmAccountType.lblHead.text = geti18Value("i18n.ordercheck.numberofbook");
        freqData = [{
            "Frequency": "1",
        }, {
            "Frequency": "2",
        }, {
            "Frequency": "3",
        }, {
            "Frequency": "4",
        }, {
            "Frequency": "5",
        }, {
            "Frequency": "6",
        }, {
            "Frequency": "7",
        }, {
            "Frequency": "8",
        }, {
            "Frequency": "9",
        }, {
            "Frequency": "10",
        }];
    } else if (gblsubacclist == "fromYear") {
        frmAccountType.lblHead.text = geti18Value("i18n.filtertransaction.year");
        freqData = loadDateDataforFromToRequestStatement();
    } else if (gblsubacclist == "toYear") {
        frmAccountType.lblHead.text = geti18Value("i18n.filtertransaction.year");
        freqData = loadDateDataforFromToRequestStatement();
    } else if (gblsubacclist == "fromMonth") {
        frmAccountType.lblHead.text = geti18Value("i18n.common.month");
        freqData = loadDateDataforFromToRequestStatement();
    } else if (gblsubacclist == "toMonth") {
        frmAccountType.lblHead.text = geti18Value("i18n.common.month");
        freqData = loadDateDataforFromToRequestStatement();
    }
    if (gblsubacclist == "Curr") {
        frmAccountType.segFrequencyList.widgetDataMap = {
            lblCurrency: "lblCurrency",
            imgCountryIcon: "imgCountryIcon",
            lblDescriptionEn: "lblDescriptionEn",
            lblDescriptionAr: "lblDescriptionAr"
        };
    } else if (gblsubacclist == "Acc") {
        frmAccountType.segFrequencyList.widgetDataMap = {
            lblfrequency: "Frequency",
            imgIcon: "imgIcon"
        };
    } else {
        frmAccountType.segFrequencyList.widgetDataMap = {
            lblfrequency: "Frequency"
        };
    }
    kony.print("freqData::" + JSON.stringify(freqData));
    frmAccountType.segFrequencyList.setData(freqData);
}

function selectAccounttype() {
    var x = "";
    if (gblsubacclist == "Curr") {
        x = frmAccountType.segFrequencyList.selectedRowItems[0].lblCurrency;
    } else {
        x = frmAccountType.segFrequencyList.selectedRowItems[0].Frequency;
    }
    if (gblsubacclist == "Acc") {
        frmNewSubAccountLandingNew.lblFre.text = x;
        frmNewSubAccountLandingNew.lblFrequency.setVisibility(true);
        frmNewSubAccountLandingNew.lblLine4.skin = "sknFlxGreenLine";
        //Omar Alnajjar 09/02/2021 Smart saver
        frmNewSubAccountLandingNew.lblCurrHead.setVisibility(false);
        frmNewSubAccountLandingNew.lblCurr.text = geti18Value("i18n.common.currencyType");
        frmNewSubAccountLandingNew.CopylblLine0ib1721fa6b7140.skin = "sknFlxGreyLine";
        frmNewSubAccountConfirm.lblCheck1.text = "q";
        if (frmNewSubAccountLandingNew.lblFre.text === geti18Value("i18n.accounts.currentAccount")) //mai 21/1/2021 Account Description
        {
            frmNewSubAccountLandingNew.flxAccDesc.setVisibility(true);
            frmNewSubAccountLandingNew.flxISaverSub.setVisibility(false);
            //       frmNewSubAccountLandingNew.flxAccDesc.height="10%";
            frmNewSubAccountLandingNew.lblAccDesc.text = geti18Value("i18n.account.subCurDesc");
        } else if (frmNewSubAccountLandingNew.lblFre.text === geti18Value("i18n.common.savingsAccount")) {
            frmNewSubAccountLandingNew.flxAccDesc.setVisibility(true);
            frmNewSubAccountLandingNew.flxISaverSub.setVisibility(false);
            //       frmNewSubAccountLandingNew.flxAccDesc.height="10%";
            frmNewSubAccountLandingNew.lblAccDesc.text = geti18Value("i18n.account.subSavDesc");
        } else if (frmNewSubAccountLandingNew.lblFre.text === geti18Value("i18n.common.isavingsAccount")) {
            frmNewSubAccountLandingNew.flxAccDesc.setVisibility(true);
            //       frmNewSubAccountLandingNew.flxAccDesc.height="40%";
            frmNewSubAccountLandingNew.lblAccDesc.text = geti18Value("i18n.account.subSmartSavDesc");
            frmNewSubAccountLandingNew.flxISaverSub.setVisibility(true);
            //Omar ALnajjar 09/02/2021 Smart Saver default cuurency
            frmNewSubAccountLandingNew.lblCurr.text = "JOD";
            frmNewSubAccountLandingNew.lblCurrHead.setVisibility(true);
            frmNewSubAccountLandingNew.CopylblLine0ib1721fa6b7140.skin = "sknFlxGreenLine";
        } else {
            frmNewSubAccountLandingNew.flxAccDesc.setVisibility(false);
            frmNewSubAccountLandingNew.flxISaverSub.setVisibility(false);
            frmNewSubAccountLandingNew.lblAccDesc.text = "";
        }
        nextSkinCheckForNewAccount();
        frmNewSubAccountLandingNew.show();
    } else if (gblsubacclist == "Curr") {
        frmNewSubAccountLandingNew.lblCurr.text = x;
        frmNewSubAccountLandingNew.lblCurrHead.setVisibility(true);
        frmNewSubAccountLandingNew.CopylblLine0ib1721fa6b7140.skin = "sknFlxGreenLine";
        nextSkinCheckForNewAccount();
        frmNewSubAccountLandingNew.show();
    } else if (gblsubacclist == "AccountOrder") {
        frmOrderCheckBook.lblFre.text = x;
        frmOrderCheckBook.lblFrequency.setVisibility(true);
        frmOrderCheckBook.lblLine4.skin = "sknFlxGreenLine";
        nextSkinCheckForOrderCheckBook();
        frmOrderCheckBook.show();
    } else if (gblsubacclist == "Leaves") {
        frmOrderCheckBook.lblLeaves.text = x;
        frmOrderCheckBook.CopylblFrequency0i1313ad52a414f.setVisibility(true);
        frmOrderCheckBook.CopylblLine0fb169aada68946.skin = "sknFlxGreenLine";
        nextSkinCheckForOrderCheckBook();
        frmOrderCheckBook.show();
    } else if (gblsubacclist == "Book") {
        frmOrderCheckBook.lblBook.text = x;
        frmOrderCheckBook.lblBookHeader.setVisibility(true);
        frmOrderCheckBook.CopylblLine0a150866e36e549.skin = "sknFlxGreenLine";
        nextSkinCheckForOrderCheckBook();
        frmOrderCheckBook.show();
    } else if (gblsubacclist == "AccNumberStatement") {
        frmRequestStatementAccounts.lblNumber.text = x;
        frmRequestStatementAccounts.lblAccountNumber.setVisibility(true);
        frmRequestStatementAccounts.lblLine4.skin = "sknFlxGreenLine";
        nextSkinCheckForRequestStaement();
        frmRequestStatementAccounts.show();
    } else if (gblsubacclist == "fromYear") {
        frmRequestStatementAccounts.lblYearFromValue.text = x;
        frmRequestStatementAccounts.lblYearFrom.setVisibility(true);
        frmRequestStatementAccounts.lblLineFY.skin = "sknFlxGreenLine";
        frmRequestStatementAccounts.lblFromService.text = x;
        frmRequestStatementAccounts.lblMonthFromValue.text = geti18Value("i18n.common.month");
        frmRequestStatementAccounts.lblMonthFrom.setVisibility(false);
        frmRequestStatementAccounts.lblLineFM.skin = "sknFlxOrangeLine";
        if (frmRequestStatementAccounts.lblYearToValue.text != geti18Value("i18n.filtertransaction.year") && frmRequestStatementAccounts.lblYearFromValue.text != geti18Value("i18n.filtertransaction.year")) {
            if (parseInt(x) >= parseInt(frmRequestStatementAccounts.lblYearToValue.text)) {
                frmRequestStatementAccounts.lblYearToValue.text = geti18Value("i18n.filtertransaction.year");
                frmRequestStatementAccounts.lblYearTo.setVisibility(false);
                frmRequestStatementAccounts.lblLineTY.skin = "sknFlxOrangeLine";
                frmRequestStatementAccounts.lblMonthToValue.text = geti18Value("i18n.common.month");
                frmRequestStatementAccounts.lblMonthTo.setVisibility(false);
                frmRequestStatementAccounts.lblLineTM.skin = "sknFlxOrangeLine";
            }
        }
        nextSkinCheckForRequestStaement();
        frmRequestStatementAccounts.show();
    } else if (gblsubacclist == "toYear") {
        frmRequestStatementAccounts.lblYearToValue.text = x;
        frmRequestStatementAccounts.lblYearTo.setVisibility(true);
        frmRequestStatementAccounts.lblLineTY.skin = "sknFlxGreenLine";
        frmRequestStatementAccounts.lblToService.text = x;
        frmRequestStatementAccounts.lblMonthToValue.text = geti18Value("i18n.common.month");
        frmRequestStatementAccounts.lblMonthTo.setVisibility(false);
        frmRequestStatementAccounts.lblLineTM.skin = "sknFlxOrangeLine";
        nextSkinCheckForRequestStaement();
        frmRequestStatementAccounts.show();
    } else if (gblsubacclist == "fromMonth") {
        frmRequestStatementAccounts.lblMonthFromValue.text = x;
        frmRequestStatementAccounts.lblFromService.text = x + "/" + frmRequestStatementAccounts.lblFromService.text;
        frmRequestStatementAccounts.lblMonthFrom.setVisibility(true);
        frmRequestStatementAccounts.lblLineFM.skin = "sknFlxGreenLine";
        if (frmRequestStatementAccounts.lblMonthToValue.text != geti18Value("i18n.common.month") && frmRequestStatementAccounts.lblMonthFromValue.text != geti18Value("i18n.common.month")) {
            if (parseInt(x) > parseInt(frmRequestStatementAccounts.lblMonthToValue.text) && parseInt(frmRequestStatementAccounts.lblYearFromValue.text) >= parseInt(frmRequestStatementAccounts.lblYearToValue.text)) {
                frmRequestStatementAccounts.lblMonthToValue.text = geti18Value("i18n.common.month");
                frmRequestStatementAccounts.lblMonthTo.setVisibility(false);
                frmRequestStatementAccounts.lblLineTM.skin = "sknFlxOrangeLine";
            }
        }
        nextSkinCheckForRequestStaement();
        frmRequestStatementAccounts.show();
    } else if (gblsubacclist == "toMonth") {
        frmRequestStatementAccounts.lblMonthToValue.text = x;
        frmRequestStatementAccounts.lblToService.text = x + "/" + frmRequestStatementAccounts.lblToService.text;
        frmRequestStatementAccounts.lblMonthTo.setVisibility(true);
        frmRequestStatementAccounts.lblLineTM.skin = "sknFlxGreenLine";
        nextSkinCheckForRequestStaement();
        frmRequestStatementAccounts.show();
    }
}

function nextSkinCheckForNewAccount() {
    if (frmNewSubAccountLandingNew.lblFre.text !== geti18Value("i18n.common.accountType") && frmNewSubAccountLandingNew.lblCurr.text !== geti18Value("i18n.common.currencyType")) {
        frmNewSubAccountLandingNew.lblNext.skin = "sknLblNextEnabled";
    } else {
        frmNewSubAccountLandingNew.lblNext.skin = "sknLblNextDisabled";
    }
}

function nextToConfirmofSubAccount() {
    if (frmNewSubAccountLandingNew.lblNext.skin === "sknLblNextEnabled") {
        frmNewSubAccountConfirm.lblFre.text = frmNewSubAccountLandingNew.lblFre.text;
        frmNewSubAccountConfirm.lblCurr.text = frmNewSubAccountLandingNew.lblCurr.text;
        frmNewSubAccountConfirm.lblCheck1.text = "q";
        frmNewSubAccountConfirm.richtxtTermsandConditions.text = "<U>" + geti18Value("i18n.account.newSubCheck") + "</U>";
        frmNewSubAccountConfirm.show();
    }
}

function callConfirmCreateNewSubAccount() {
    kony.print("callConfirmCreateNewSubAccount::");
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Accounts");
        dataObject.addField("custId", custid);
        var p_acc_type = frmNewSubAccountConfirm.lblFre.text;
        var p_ccy = frmNewSubAccountConfirm.lblCurr.text;
        if (p_acc_type == geti18Value("i18n.common.savingsAccount")) {
            p_acc_type = "305";
        } else if (p_acc_type == geti18Value("i18n.common.isavingsAccount")) { //mai 19/1/2021
            p_acc_type = "441";
            kony.print("iSaving:: " + p_acc_type);
        } else {
            p_acc_type = "301";
        }
        if (p_ccy == kony.i18n.getLocalizedString("i18n.EUroCurrency")) {
            p_ccy = "EUR";
        } else if (p_ccy == kony.i18n.getLocalizedString("i18n.GBPCurrency")) {
            p_ccy = "GBP";
        } else if (p_ccy == kony.i18n.getLocalizedString("i18n.USDCurrency")) {
            p_ccy = "USD";
        } else if (p_ccy == kony.i18n.getLocalizedString("i18n.JODCurrency")) {
            p_ccy = "JOD";
        }
        dataObject.addField("p_acc_type", p_acc_type);
        dataObject.addField("p_ccy", p_ccy);
        dataObject.addField("p_has_chk_book_facility", "");
        dataObject.addField("p_fcdb_ref_no", "");
        dataObject.addField("p_has_ovrdraft_facility", "");
        dataObject.addField("p_min_bal", "");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("callConfirmCreateNewSubAccountInput params-->" + JSON.stringify(serviceOptions));
        modelObj.customVerb("OpenNewSubAcc", serviceOptions, callConfirmCreateNewSubAccountSuccessCallback, callConfirmCreateNewSubAccountErrorCallback);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
    frmNewSubAccountLandingNew.flxAccDesc.setVisibility(false);
    frmNewSubAccountLandingNew.flxISaverSub.setVisibility(false);
    frmNewSubAccountLandingNew.lblAccDesc.text = "";
}

function callConfirmCreateNewSubAccountSuccessCallback(response) {
    kony.application.dismissLoadingScreen();
    if (!isEmpty(response) && response.opstatus === 0 && response.ErrorCode === "00000") {
        var reference = "";
        if (!isEmpty(response.ReferenceNum)) reference = geti18nkey("i18n.common.accountNumber") + " " + response.ReferenceNum;
        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.registration.congratulations"), geti18Value("i18n.account.subAccCongrats"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), "", "", "", "", reference);
    } else {
        customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
        exceptionLogCall("callConfirmCreateNewSubAccountSuccessCallback", "Service ERROR", "Service", "opstatus other than 0");
        kony.application.dismissLoadingScreen();
    }
}

function callConfirmCreateNewSubAccountErrorCallback(response) {
    kony.print("callConfirmCreateNewSubAccountErrorCallback -----" + JSON.stringify(response));
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.defaultErrorMsg"), popupCommonAlertDimiss, "");
    kony.application.dismissLoadingScreen();
}

function backToOrignalFormFromFrmAccountType() {
    if (gblsubacclist == "Acc") {
        frmNewSubAccountLandingNew.show();
    } else if (gblsubacclist == "Curr") {
        frmNewSubAccountLandingNew.show();
    } else if (gblsubacclist == "AccountOrder") {
        frmOrderCheckBook.show();
    } else if (gblsubacclist == "Leaves") {
        frmOrderCheckBook.show();
    } else if (gblsubacclist == "Book") {
        frmOrderCheckBook.show();
    } else if (gblsubacclist == "AccNumberStatement" || gblsubacclist == "fromYear" || gblsubacclist == "toYear" || gblsubacclist == "fromMonth" || gblsubacclist == "toMonth") {
        frmRequestStatementAccounts.show();
    }
}

function loadDateDataforFromToRequestStatement() {
    var data = [];
    var curDate = new Date();
    var month = (curDate.getMonth() + 1) < 10 ? "0" + (curDate.getMonth() + 1) : (curDate.getMonth() + 1);
    var year = parseInt(curDate.getFullYear());
    if (gblsubacclist == "fromYear") {
        data = [{
            "Frequency": (parseInt(year) - 2) + "",
        }, {
            "Frequency": (parseInt(year) - 1) + "",
        }, {
            "Frequency": parseInt(year) + ""
        }];
    } else if (gblsubacclist == "toYear") {
        var fromDate = frmRequestStatementAccounts.lblYearFromValue.text;
        data.push({
            "Frequency": (parseInt(fromDate)) + ""
        });
        if ((parseInt(fromDate)) != year) {
            for (var i = (parseInt(fromDate) + 1); i <= year; i++) {
                data.push({
                    "Frequency": parseInt(i) < 10 ? ("0" + i) : i + ""
                });
            }
        }
    } else if (gblsubacclist == "fromMonth") {
        var fromDate1 = frmRequestStatementAccounts.lblYearFromValue.text;
        if (parseInt(fromDate1) == year) {
            for (var k = 1; k <= parseInt(month); k++) {
                data.push({
                    "Frequency": parseInt(k) < 10 ? ("0" + k) : k + ""
                });
            }
        } else {
            for (var j = 1; j <= 12; j++) {
                data.push({
                    "Frequency": parseInt(j) < 10 ? ("0" + j) : j + ""
                });
            }
        }
    } else if (gblsubacclist == "toMonth") {
        var toDate2 = frmRequestStatementAccounts.lblYearToValue.text;
        var fromDate2 = frmRequestStatementAccounts.lblYearFromValue.text;
        var fromMonth = parseInt(frmRequestStatementAccounts.lblMonthFromValue.text);
        if ((parseInt(toDate2) == year) && (parseInt(toDate2) != parseInt(fromDate2))) {
            for (var z = 1; z <= parseInt(month); z++) {
                data.push({
                    "Frequency": parseInt(z) < 10 ? ("0" + z) : z + ""
                });
            }
        } else if ((parseInt(toDate2) == parseInt(fromDate2)) && (parseInt(year) == parseInt(fromDate2))) {
            for (var q = fromMonth; q <= parseInt(month); q++) {
                data.push({
                    "Frequency": parseInt(q) < 10 ? ("0" + q) : q + ""
                });
            }
        } else if ((parseInt(toDate2) == parseInt(fromDate2))) {
            for (var a = fromMonth; a <= 12; a++) {
                data.push({
                    "Frequency": parseInt(a) < 10 ? ("0" + a) : a + ""
                });
            }
        } else {
            for (var x = 1; x <= 12; x++) {
                data.push({
                    "Frequency": parseInt(x) < 10 ? ("0" + x) : x + ""
                });
            }
        }
    }
    return data;
}