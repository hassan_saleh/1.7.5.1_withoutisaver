function getBojBankList() {
    if (kony.sdk.isNetworkAvailable()) {
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Country", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Country");
        dataObject.addField("flow", "MOBILE");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("getBojBankList :: serviceOptions " + JSON.stringify(serviceOptions));
        modelObj.customVerb("getBanknameList", serviceOptions, getBanknameListSuccessCallback, getBanknameListErrorCallback);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        kony.application.dismissLoadingScreen();
    }
}

function getBanknameListSuccessCallback(resObj) {
    kony.print("In success of getBanknameListSuccessCallback ::  " + JSON.stringify(resObj));
    if (resObj.bankList != undefined && resObj.bankList != null && resObj.bankList != "") {
        try {
            var bList = resObj.bankList;
            kony.print("BojBankList From service :: " + JSON.stringify(bList));
            var len = bList.length;
            for (var i = 0; i < len; i++) {
                var BankName = bList[i].BankName;
                var INITIAL = getInitials(BankName);
                bList[i].INITIAL = INITIAL;
            }
            kony.boj.detailsForBene.BankDetail = bList;
            kony.print("BojBankList after Loop :: " + JSON.stringify(kony.boj.detailsForBene.BankDetail));
        } catch (e) {
            kony.print("in exception of getBanknameListSuccessCallback :; " + e);
            exceptionLogCall("getBanknameListSuccessCallback", "UI ERROR", "UI", e);
        }
    }
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function getBanknameListErrorCallback(errObj) {
    kony.print("In Error of getBanknameListErrorCallback ::  " + JSON.stringify(errObj));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}