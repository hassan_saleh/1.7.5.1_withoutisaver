kony = kony || {};
kony.rb = kony.rb || {};
kony.rb.frmConfirmCashWithDrawQRCodeKAViewController = function() {};
kony.rb.frmConfirmCashWithDrawQRCodeKAViewController.prototype.frmConfirmCashWithDrawQRCodeKApreshow = function() {
    var navManager = applicationManager.getNavManager();
    var txnData = navManager.getCustomInfo("frmConfirmCashWithDrawQRCodeKA");
    var withDrawDetails = txnData.withdraw;
    frmConfirmCashWithDrawQRCodeKA.withdrawAmount.text = kony.retailBanking.util.currency_symbols[kony.retailBanking.globalData.globals.CurrencyCode] + " " + withDrawDetails.amount;
    frmConfirmCashWithDrawQRCodeKA.collectorName.text = withDrawDetails.toAccountName;
    frmConfirmCashWithDrawQRCodeKA.withdrawFrom.text = withDrawDetails.fromAccountName;
    frmConfirmCashWithDrawQRCodeKA.transactionNotes.text = withDrawDetails.transactionsNotes;
};
kony.rb.frmConfirmCashWithDrawQRCodeKAViewController.prototype.onClickConfirm = function() {
    ShowLoadingScreen();
    var navManager = applicationManager.getNavManager();
    var txnData = navManager.getCustomInfo("frmConfirmCashWithDrawQRCodeKA");
    var withDrawDetails = txnData.withdraw;
    var cardlessPC = applicationManager.getCardlessWithdrawalPresentationController();
};
kony.rb.frmConfirmCashWithDrawQRCodeKAViewController.prototype.goBackToNewCashWithdraw = function() {
    var navManager = applicationManager.getNavManager();
    var txnData = navManager.getCustomInfo("frmConfirmCashWithDrawQRCodeKA");
    var curData = txnData.withdraw;
    curData.from = "EditCardlessWithdraw";
    curData.fromAccountBalance = kony.retailBanking.util.currency_symbols[kony.retailBanking.globalData.globals.CurrencyCode] + " " + curData.fromAccountBalance;
    navManager.goBack(curData);
};