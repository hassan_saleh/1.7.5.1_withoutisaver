//startup.js
var globalhttpheaders = {};
var appConfig = {
    appId: "BOJMobileBanking",
    appName: "BOJ Mobile",
    appVersion: "1.7.5.2",
    isturlbase: "https://mobileuat.bankofjordan.com/services",
    isDebug: true,
    isMFApp: true,
    appKey: "de868f68e0db476e369ce1d2ec311877",
    appSecret: "3e42682a543882869eaf937bbd29cf84",
    serviceUrl: "https://mobileuat.bankofjordan.com/authService/100000044/appconfig",
    svcDoc: {
        "selflink": "https://mobileuat.bankofjordan.com/authService/100000044/appconfig",
        "messagingsvc": {
            "appId": "82819621-0b00-4c62-aa90-55a6cc97f58a",
            "url": "https://mobileuat.bankofjordan.com/kpns/api/v1"
        },
        "integsvc": {
            "testregestered": "https://mobileuat.bankofjordan.com/services/testregestered",
            "prAddBeneficiary": "https://mobileuat.bankofjordan.com/services/prAddBeneficiary",
            "BOJGetPpValidation": "https://mobileuat.bankofjordan.com/services/BOJGetPpValidation",
            "BOJPrfChngSms": "https://mobileuat.bankofjordan.com/services/BOJPrfChngSms",
            "OTPController": "https://mobileuat.bankofjordan.com/services/OTPController",
            "BOJPrExternalTransfer": "https://mobileuat.bankofjordan.com/services/BOJPrExternalTransfer",
            "BOJGetCrCardDetails": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardDetails",
            "getTncProfileData": "https://mobileuat.bankofjordan.com/services/getTncProfileData",
            "BOJGetEfwListBillers": "https://mobileuat.bankofjordan.com/services/BOJGetEfwListBillers",
            "BOJApplySuppCcNew": "https://mobileuat.bankofjordan.com/services/BOJApplySuppCcNew",
            "LoopOutRemitDetails": "https://mobileuat.bankofjordan.com/services/LoopOutRemitDetails",
            "BOJBeneBranchList": "https://mobileuat.bankofjordan.com/services/BOJBeneBranchList",
            "BOJAliasIPS": "https://mobileuat.bankofjordan.com/services/BOJAliasIPS",
            "BOJApplyPrimCcReplace": "https://mobileuat.bankofjordan.com/services/BOJApplyPrimCcReplace",
            "BOJApplyPrimaryCcNew": "https://mobileuat.bankofjordan.com/services/BOJApplyPrimaryCcNew",
            "BOJprApplyLoanPostpone": "https://mobileuat.bankofjordan.com/services/BOJprApplyLoanPostpone",
            "BOJPpPayment": "https://mobileuat.bankofjordan.com/services/BOJPpPayment",
            "LoopBulkPrepaidPaymentServ": "https://mobileuat.bankofjordan.com/services/LoopBulkPrepaidPaymentServ",
            "BOJSendSMSMobile": "https://mobileuat.bankofjordan.com/services/BOJSendSMSMobile",
            "ApplyCards": "https://mobileuat.bankofjordan.com/services/ApplyCards",
            "BojUserReg": "https://mobileuat.bankofjordan.com/services/BojUserReg",
            "AllCardList": "https://mobileuat.bankofjordan.com/services/AllCardList",
            "BOJprDcReqNewPin": "https://mobileuat.bankofjordan.com/services/BOJprDcReqNewPin",
            "pushNotificationsAuthService": "https://mobileuat.bankofjordan.com/services/pushNotificationsAuthService",
            "BOJGetDbCardDetails": "https://mobileuat.bankofjordan.com/services/BOJGetDbCardDetails",
            "BOJprChqBookOrder": "https://mobileuat.bankofjordan.com/services/BOJprChqBookOrder",
            "BOJBitlyShortenURL": "https://mobileuat.bankofjordan.com/services/BOJBitlyShortenURL",
            "loopCardLinkAccount": "https://mobileuat.bankofjordan.com/services/loopCardLinkAccount",
            "BOJCnfSiCancellation": "https://mobileuat.bankofjordan.com/services/BOJCnfSiCancellation",
            "BOJPrGetTdDetails": "https://mobileuat.bankofjordan.com/services/BOJPrGetTdDetails",
            "BOJGetStandingInstructions": "https://mobileuat.bankofjordan.com/services/BOJGetStandingInstructions",
            "updateBeneficiary": "https://mobileuat.bankofjordan.com/services/updateBeneficiary",
            "ChangeUnamePwd": "https://mobileuat.bankofjordan.com/services/ChangeUnamePwd",
            "BOJprCreateAddFtd": "https://mobileuat.bankofjordan.com/services/BOJprCreateAddFtd",
            "BOJChangeSMSNo": "https://mobileuat.bankofjordan.com/services/BOJChangeSMSNo",
            "GetTransactionAndMaster": "https://mobileuat.bankofjordan.com/services/GetTransactionAndMaster",
            "updateJMPBene": "https://mobileuat.bankofjordan.com/services/updateJMPBene",
            "LoopExample": "https://mobileuat.bankofjordan.com/services/LoopExample",
            "BOJEstmtConfirm": "https://mobileuat.bankofjordan.com/services/BOJEstmtConfirm",
            "TestSondos": "https://mobileuat.bankofjordan.com/services/TestSondos",
            "ATMPOSLimit": "https://mobileuat.bankofjordan.com/services/ATMPOSLimit",
            "BOJprGetAccTxnMaster": "https://mobileuat.bankofjordan.com/services/BOJprGetAccTxnMaster",
            "GetCommission": "https://mobileuat.bankofjordan.com/services/GetCommission",
            "BOJprAccTransfer": "https://mobileuat.bankofjordan.com/services/BOJprAccTransfer",
            "DebitCardList": "https://mobileuat.bankofjordan.com/services/DebitCardList",
            "BOJGetProfileData": "https://mobileuat.bankofjordan.com/services/BOJGetProfileData",
            "BOJIPS": "https://mobileuat.bankofjordan.com/services/BOJIPS",
            "GetDetails": "https://mobileuat.bankofjordan.com/services/GetDetails",
            "BOJGetAccountDetails": "https://mobileuat.bankofjordan.com/services/BOJGetAccountDetails",
            "BOJprCcUnblockPin": "https://mobileuat.bankofjordan.com/services/BOJprCcUnblockPin",
            "IPSCustomer": "https://mobileuat.bankofjordan.com/services/IPSCustomer",
            "BOJPrJmpGetFees": "https://mobileuat.bankofjordan.com/services/BOJPrJmpGetFees",
            "BOJGetStatisticsDetails": "https://mobileuat.bankofjordan.com/services/BOJGetStatisticsDetails",
            "BOJGetPpServiceTypes": "https://mobileuat.bankofjordan.com/services/BOJGetPpServiceTypes",
            "BOJCardlessTransList": "https://mobileuat.bankofjordan.com/services/BOJCardlessTransList",
            "BOJJavaServices": "https://mobileuat.bankofjordan.com/services/BOJJavaServices",
            "BOJUnClearTxn": "https://mobileuat.bankofjordan.com/services/BOJUnClearTxn",
            "BOJApplyWcCardNew": "https://mobileuat.bankofjordan.com/services/BOJApplyWcCardNew",
            "BOJGetCrCardStmt": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardStmt",
            "BOJprCcPinReminder": "https://mobileuat.bankofjordan.com/services/BOJprCcPinReminder",
            "BOJGetBillerList": "https://mobileuat.bankofjordan.com/services/BOJGetBillerList",
            "testips": "https://mobileuat.bankofjordan.com/services/testips",
            "BOJCheckCardPin": "https://mobileuat.bankofjordan.com/services/BOJCheckCardPin",
            "BOJprGetInRemitDetails": "https://mobileuat.bankofjordan.com/services/BOJprGetInRemitDetails",
            "BOJGetEfwServtypes": "https://mobileuat.bankofjordan.com/services/BOJGetEfwServtypes",
            "BOJPOSprCcSpendingControl": "https://mobileuat.bankofjordan.com/services/BOJPOSprCcSpendingControl",
            "BOJGetWebChrgCardDetails": "https://mobileuat.bankofjordan.com/services/BOJGetWebChrgCardDetails",
            "EFWateercomBillerList": "https://mobileuat.bankofjordan.com/services/EFWateercomBillerList",
            "BOJprGetTransferLimit": "https://mobileuat.bankofjordan.com/services/BOJprGetTransferLimit",
            "BOJGetExchRates": "https://mobileuat.bankofjordan.com/services/BOJGetExchRates",
            "BOJDeleteBeneficiary": "https://mobileuat.bankofjordan.com/services/BOJDeleteBeneficiary",
            "BOJSendOTP": "https://mobileuat.bankofjordan.com/services/BOJSendOTP",
            "SendPushMessage": "https://mobileuat.bankofjordan.com/services/SendPushMessage",
            "TransferMoneySS": "https://mobileuat.bankofjordan.com/services/TransferMoneySS",
            "BOJGetPalpayMappingCodes": "https://mobileuat.bankofjordan.com/services/BOJGetPalpayMappingCodes",
            "BOJDcAddAccLink": "https://mobileuat.bankofjordan.com/services/BOJDcAddAccLink",
            "BOJGetAccDetailsNode": "https://mobileuat.bankofjordan.com/services/BOJGetAccDetailsNode",
            "BranchATMLocator": "https://mobileuat.bankofjordan.com/services/BranchATMLocator",
            "ATMLocator": "https://mobileuat.bankofjordan.com/services/ATMLocator",
            "BOJBillerAccounts": "https://mobileuat.bankofjordan.com/services/BOJBillerAccounts",
            "BOJDebitCrdTxnList": "https://mobileuat.bankofjordan.com/services/BOJDebitCrdTxnList",
            "BOJCancelCreditCard": "https://mobileuat.bankofjordan.com/services/BOJCancelCreditCard",
            "BOJJmpRegister": "https://mobileuat.bankofjordan.com/services/BOJJmpRegister",
            "GetAccountListService": "https://mobileuat.bankofjordan.com/services/GetAccountListService",
            "BOJsendUserName": "https://mobileuat.bankofjordan.com/services/BOJsendUserName",
            "iWatchBOJPrGetTdDetails": "https://mobileuat.bankofjordan.com/services/iWatchBOJPrGetTdDetails",
            "CancelCard": "https://mobileuat.bankofjordan.com/services/CancelCard",
            "BojprDcActivation": "https://mobileuat.bankofjordan.com/services/BojprDcActivation",
            "BOJGetEfwDebitAmt": "https://mobileuat.bankofjordan.com/services/BOJGetEfwDebitAmt",
            "BOJRestServices": "https://mobileuat.bankofjordan.com/services/BOJRestServices",
            "BOJprCcActivation": "https://mobileuat.bankofjordan.com/services/BOJprCcActivation",
            "LoopBulkPayment": "https://mobileuat.bankofjordan.com/services/LoopBulkPayment",
            "BOJPrGetIPSAcceptedDetail": "https://mobileuat.bankofjordan.com/services/BOJPrGetIPSAcceptedDetail",
            "BOJprJmpGetRegisterDetails": "https://mobileuat.bankofjordan.com/services/BOJprJmpGetRegisterDetails",
            "LoopBulkPaymentPrePaid": "https://mobileuat.bankofjordan.com/services/LoopBulkPaymentPrePaid",
            "BOJCnfOpenNewAcc": "https://mobileuat.bankofjordan.com/services/BOJCnfOpenNewAcc",
            "BOJGetIPSCreditStatus": "https://mobileuat.bankofjordan.com/services/BOJGetIPSCreditStatus",
            "BOJATMList": "https://mobileuat.bankofjordan.com/services/BOJATMList",
            "BOJprAccStmtOrder": "https://mobileuat.bankofjordan.com/services/BOJprAccStmtOrder",
            "BOJDcDelAccLink": "https://mobileuat.bankofjordan.com/services/BOJDcDelAccLink",
            "iWatchBOJPrGetLoanAccDetails": "https://mobileuat.bankofjordan.com/services/iWatchBOJPrGetLoanAccDetails",
            "InformationSupport": "https://mobileuat.bankofjordan.com/services/InformationSupport",
            "BOJAutoBillsPay": "https://mobileuat.bankofjordan.com/services/BOJAutoBillsPay",
            "BOJEfwPaymentConfirm": "https://mobileuat.bankofjordan.com/services/BOJEfwPaymentConfirm",
            "BOJprCheckExistIPSCust": "https://mobileuat.bankofjordan.com/services/BOJprCheckExistIPSCust",
            "BOJCXLead": "https://mobileuat.bankofjordan.com/services/BOJCXLead",
            "BOJprDcLinkedAccount": "https://mobileuat.bankofjordan.com/services/BOJprDcLinkedAccount",
            "LoopBulkPaymentPostpaid": "https://mobileuat.bankofjordan.com/services/LoopBulkPaymentPostpaid",
            "BOJSendIbanToSSC": "https://mobileuat.bankofjordan.com/services/BOJSendIbanToSSC",
            "prGetBeneficiaryDetails": "https://mobileuat.bankofjordan.com/services/prGetBeneficiaryDetails",
            "BOJrApplySuppDcNew": "https://mobileuat.bankofjordan.com/services/BOJrApplySuppDcNew",
            "BOJGetBillerDetails": "https://mobileuat.bankofjordan.com/services/BOJGetBillerDetails",
            "BOJECMsendimage": "https://mobileuat.bankofjordan.com/services/BOJECMsendimage",
            "BOJprCcSpendingControl": "https://mobileuat.bankofjordan.com/services/BOJprCcSpendingControl",
            "LoopInRemitDetails": "https://mobileuat.bankofjordan.com/services/LoopInRemitDetails",
            "addPrePaidBene": "https://mobileuat.bankofjordan.com/services/addPrePaidBene",
            "OTPSMSController": "https://mobileuat.bankofjordan.com/services/OTPSMSController",
            "PushNotification": "https://mobileuat.bankofjordan.com/services/PushNotification",
            "BOJprGetOutRemitDetails": "https://mobileuat.bankofjordan.com/services/BOJprGetOutRemitDetails",
            "BOJPPBillerAccounts": "https://mobileuat.bankofjordan.com/services/BOJPPBillerAccounts",
            "RetailBankingBEServices": "https://mobileuat.bankofjordan.com/services/RetailBankingBEServices",
            "BOJGetRegisteredEstmt": "https://mobileuat.bankofjordan.com/services/BOJGetRegisteredEstmt",
            "BOJCreditCardPayment": "https://mobileuat.bankofjordan.com/services/BOJCreditCardPayment",
            "GetNotificationController": "https://mobileuat.bankofjordan.com/services/GetNotificationController",
            "BOJUpdateFavorBeneficiary": "https://mobileuat.bankofjordan.com/services/BOJUpdateFavorBeneficiary",
            "PrePaidValnDebitAmount": "https://mobileuat.bankofjordan.com/services/PrePaidValnDebitAmount",
            "EFWateercomBillerList1": "https://mobileuat.bankofjordan.com/services/EFWateercomBillerList1",
            "BOJCancelDebitCard": "https://mobileuat.bankofjordan.com/services/BOJCancelDebitCard",
            "BOJCardlessAuth": "https://mobileuat.bankofjordan.com/services/BOJCardlessAuth",
            "BOJCancelCardlessRequest": "https://mobileuat.bankofjordan.com/services/BOJCancelCardlessRequest",
            "BOJApplyDebitCard": "https://mobileuat.bankofjordan.com/services/BOJApplyDebitCard",
            "AccountTransactions": "https://mobileuat.bankofjordan.com/services/AccountTransactions",
            "BOJBankList": "https://mobileuat.bankofjordan.com/services/BOJBankList",
            "prGetExternalTransferComm": "https://mobileuat.bankofjordan.com/services/prGetExternalTransferComm",
            "BOJGetCrCardHistTrans": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardHistTrans",
            "GoogleAPIs": "https://mobileuat.bankofjordan.com/services/GoogleAPIs",
            "ManualUserCreation": "https://mobileuat.bankofjordan.com/services/ManualUserCreation",
            "BOJIPSReturnTransfer": "https://mobileuat.bankofjordan.com/services/BOJIPSReturnTransfer",
            "BOJDcChangeDefaultAcc": "https://mobileuat.bankofjordan.com/services/BOJDcChangeDefaultAcc",
            "BOJprCcPpPayment": "https://mobileuat.bankofjordan.com/services/BOJprCcPpPayment",
            "BojGetPpRegisteredBilling": "https://mobileuat.bankofjordan.com/services/BojGetPpRegisteredBilling",
            "BOJGetEfwMappingCodes": "https://mobileuat.bankofjordan.com/services/BOJGetEfwMappingCodes",
            "loopCardUnLinkAccount": "https://mobileuat.bankofjordan.com/services/loopCardUnLinkAccount",
            "BOJprGetFtdIntRate": "https://mobileuat.bankofjordan.com/services/BOJprGetFtdIntRate",
            "BOJCrdtCrdChngSms": "https://mobileuat.bankofjordan.com/services/BOJCrdtCrdChngSms",
            "BOJPrConfirmJmpPayments": "https://mobileuat.bankofjordan.com/services/BOJPrConfirmJmpPayments",
            "BOJPushNotification": "https://mobileuat.bankofjordan.com/services/BOJPushNotification",
            "BOJprCcInstantCashback": "https://mobileuat.bankofjordan.com/services/BOJprCcInstantCashback",
            "BOJNationalID": "https://mobileuat.bankofjordan.com/services/BOJNationalID",
            "LoopStatistics": "https://mobileuat.bankofjordan.com/services/LoopStatistics",
            "BOJEMVParse": "https://mobileuat.bankofjordan.com/services/BOJEMVParse",
            "BOJprCcEfwPaymentConfirm": "https://mobileuat.bankofjordan.com/services/BOJprCcEfwPaymentConfirm",
            "BOJBranchList": "https://mobileuat.bankofjordan.com/services/BOJBranchList",
            "BOJPrGetLoanAccDetails": "https://mobileuat.bankofjordan.com/services/BOJPrGetLoanAccDetails",
            "BOJprIPSRegister": "https://mobileuat.bankofjordan.com/services/BOJprIPSRegister",
            "BOJGetTransactionDetails": "https://mobileuat.bankofjordan.com/services/BOJGetTransactionDetails",
            "BOJGetTransferFee": "https://mobileuat.bankofjordan.com/services/BOJGetTransferFee",
            "NotificationController": "https://mobileuat.bankofjordan.com/services/NotificationController",
            "prCcInternetMailorder": "https://mobileuat.bankofjordan.com/services/prCcInternetMailorder"
        },
        "appId": "82819621-0b00-4c62-aa90-55a6cc97f58a",
        "name": "RetailBankingServices",
        "reportingsvc": {
            "session": "https://mobileuat.bankofjordan.com/services/IST",
            "custom": "https://mobileuat.bankofjordan.com/services/CMS"
        },
        "baseId": "19f965b1-605e-4773-aed9-deeb6ffb80ac",
        "login": [{
            "alias": "LinkedIn",
            "type": "oauth2",
            "prov": "LinkedIn",
            "url": "https://mobileuat.bankofjordan.com/authService/100000044"
        }, {
            "mandatory_fields": [],
            "alias": "CustomLogin",
            "type": "basic",
            "prov": "CustomLogin",
            "url": "https://mobileuat.bankofjordan.com/authService/100000044"
        }],
        "services_meta": {
            "testregestered": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/testregestered"
            },
            "prAddBeneficiary": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/prAddBeneficiary"
            },
            "BOJGetPpValidation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetPpValidation"
            },
            "BOJPrfChngSms": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrfChngSms"
            },
            "OTPController": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/OTPController"
            },
            "BOJPrExternalTransfer": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrExternalTransfer"
            },
            "BOJGetCrCardDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardDetails"
            },
            "getTncProfileData": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/getTncProfileData"
            },
            "BOJGetEfwListBillers": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetEfwListBillers"
            },
            "BOJApplySuppCcNew": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJApplySuppCcNew"
            },
            "LoopOutRemitDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopOutRemitDetails"
            },
            "BOJBeneBranchList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJBeneBranchList"
            },
            "BOJAliasIPS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJAliasIPS"
            },
            "BOJApplyPrimCcReplace": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJApplyPrimCcReplace"
            },
            "BOJApplyPrimaryCcNew": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJApplyPrimaryCcNew"
            },
            "BOJprApplyLoanPostpone": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprApplyLoanPostpone"
            },
            "BOJPpPayment": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPpPayment"
            },
            "LoopBulkPrepaidPaymentServ": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopBulkPrepaidPaymentServ"
            },
            "BOJSendSMSMobile": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJSendSMSMobile"
            },
            "ApplyCards": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/ApplyCards"
            },
            "BojUserReg": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BojUserReg"
            },
            "AllCardList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/AllCardList"
            },
            "BOJprDcReqNewPin": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprDcReqNewPin"
            },
            "pushNotificationsAuthService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/pushNotificationsAuthService"
            },
            "BOJGetDbCardDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetDbCardDetails"
            },
            "BOJprChqBookOrder": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprChqBookOrder"
            },
            "BOJBitlyShortenURL": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJBitlyShortenURL"
            },
            "loopCardLinkAccount": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/loopCardLinkAccount"
            },
            "BOJCnfSiCancellation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCnfSiCancellation"
            },
            "BOJPrGetTdDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrGetTdDetails"
            },
            "BOJGetStandingInstructions": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetStandingInstructions"
            },
            "updateBeneficiary": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/updateBeneficiary"
            },
            "ChangeUnamePwd": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/ChangeUnamePwd"
            },
            "BOJprCreateAddFtd": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCreateAddFtd"
            },
            "BOJChangeSMSNo": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJChangeSMSNo"
            },
            "GetTransactionAndMaster": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GetTransactionAndMaster"
            },
            "updateJMPBene": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/updateJMPBene"
            },
            "LoopExample": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopExample"
            },
            "BOJEstmtConfirm": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJEstmtConfirm"
            },
            "TestSondos": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/TestSondos"
            },
            "ATMPOSLimit": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/ATMPOSLimit"
            },
            "BOJprGetAccTxnMaster": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprGetAccTxnMaster"
            },
            "GetCommission": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GetCommission"
            },
            "BOJprAccTransfer": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprAccTransfer"
            },
            "DebitCardList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/DebitCardList"
            },
            "BOJGetProfileData": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetProfileData"
            },
            "BOJIPS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJIPS"
            },
            "GetDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GetDetails"
            },
            "BOJGetAccountDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetAccountDetails"
            },
            "BOJprCcUnblockPin": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcUnblockPin"
            },
            "IPSCustomer": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/IPSCustomer"
            },
            "BOJPrJmpGetFees": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrJmpGetFees"
            },
            "BOJGetStatisticsDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetStatisticsDetails"
            },
            "BOJGetPpServiceTypes": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetPpServiceTypes"
            },
            "BOJCardlessTransList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCardlessTransList"
            },
            "BOJJavaServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJJavaServices"
            },
            "BOJUnClearTxn": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJUnClearTxn"
            },
            "BOJApplyWcCardNew": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJApplyWcCardNew"
            },
            "BOJGetCrCardStmt": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardStmt"
            },
            "BOJprCcPinReminder": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcPinReminder"
            },
            "BOJGetBillerList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetBillerList"
            },
            "testips": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/testips"
            },
            "BOJCheckCardPin": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCheckCardPin"
            },
            "BOJprGetInRemitDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprGetInRemitDetails"
            },
            "BOJGetEfwServtypes": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetEfwServtypes"
            },
            "BOJPOSprCcSpendingControl": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPOSprCcSpendingControl"
            },
            "BOJGetWebChrgCardDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetWebChrgCardDetails"
            },
            "EFWateercomBillerList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/EFWateercomBillerList"
            },
            "BOJprGetTransferLimit": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprGetTransferLimit"
            },
            "BOJGetExchRates": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetExchRates"
            },
            "BOJDeleteBeneficiary": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJDeleteBeneficiary"
            },
            "BOJSendOTP": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJSendOTP"
            },
            "SendPushMessage": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/SendPushMessage"
            },
            "TransferMoneySS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/TransferMoneySS"
            },
            "BOJGetPalpayMappingCodes": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetPalpayMappingCodes"
            },
            "BOJDcAddAccLink": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJDcAddAccLink"
            },
            "BOJGetAccDetailsNode": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetAccDetailsNode"
            },
            "RBObjects": {
                "metadata_url": "https://mobileuat.bankofjordan.com/services/metadata/v1/RBObjects",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/data/v1/RBObjects"
            },
            "BranchATMLocator": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BranchATMLocator"
            },
            "ATMLocator": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/ATMLocator"
            },
            "BOJBillerAccounts": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJBillerAccounts"
            },
            "BOJDebitCrdTxnList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJDebitCrdTxnList"
            },
            "BOJCancelCreditCard": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCancelCreditCard"
            },
            "BOJJmpRegister": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJJmpRegister"
            },
            "GetAccountListService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GetAccountListService"
            },
            "BOJsendUserName": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJsendUserName"
            },
            "iWatchBOJPrGetTdDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/iWatchBOJPrGetTdDetails"
            },
            "CancelCard": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/CancelCard"
            },
            "BojprDcActivation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BojprDcActivation"
            },
            "BOJGetEfwDebitAmt": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetEfwDebitAmt"
            },
            "BOJRestServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJRestServices"
            },
            "BOJprCcActivation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcActivation"
            },
            "LoopBulkPayment": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopBulkPayment"
            },
            "BOJPrGetIPSAcceptedDetail": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrGetIPSAcceptedDetail"
            },
            "BOJprJmpGetRegisterDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprJmpGetRegisterDetails"
            },
            "LoopBulkPaymentPrePaid": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopBulkPaymentPrePaid"
            },
            "BOJCnfOpenNewAcc": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCnfOpenNewAcc"
            },
            "BOJGetIPSCreditStatus": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetIPSCreditStatus"
            },
            "BOJATMList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJATMList"
            },
            "BOJprAccStmtOrder": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprAccStmtOrder"
            },
            "BOJDcDelAccLink": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJDcDelAccLink"
            },
            "iWatchBOJPrGetLoanAccDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/iWatchBOJPrGetLoanAccDetails"
            },
            "InformationSupport": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/InformationSupport"
            },
            "BOJAutoBillsPay": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJAutoBillsPay"
            },
            "BOJEfwPaymentConfirm": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJEfwPaymentConfirm"
            },
            "BOJprCheckExistIPSCust": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCheckExistIPSCust"
            },
            "BOJCXLead": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCXLead"
            },
            "BOJprDcLinkedAccount": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprDcLinkedAccount"
            },
            "LoopBulkPaymentPostpaid": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopBulkPaymentPostpaid"
            },
            "BOJSendIbanToSSC": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJSendIbanToSSC"
            },
            "prGetBeneficiaryDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/prGetBeneficiaryDetails"
            },
            "BOJrApplySuppDcNew": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJrApplySuppDcNew"
            },
            "BOJGetBillerDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetBillerDetails"
            },
            "BOJECMsendimage": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJECMsendimage"
            },
            "BOJprCcSpendingControl": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcSpendingControl"
            },
            "LoopInRemitDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopInRemitDetails"
            },
            "addPrePaidBene": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/addPrePaidBene"
            },
            "OTPSMSController": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/OTPSMSController"
            },
            "PushNotification": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/PushNotification"
            },
            "BOJprGetOutRemitDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprGetOutRemitDetails"
            },
            "BOJPPBillerAccounts": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPPBillerAccounts"
            },
            "RetailBankingBEServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/RetailBankingBEServices"
            },
            "BOJGetRegisteredEstmt": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetRegisteredEstmt"
            },
            "BOJCreditCardPayment": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCreditCardPayment"
            },
            "GetNotificationController": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GetNotificationController"
            },
            "BOJUpdateFavorBeneficiary": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJUpdateFavorBeneficiary"
            },
            "PrePaidValnDebitAmount": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/PrePaidValnDebitAmount"
            },
            "EFWateercomBillerList1": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/EFWateercomBillerList1"
            },
            "BOJCancelDebitCard": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCancelDebitCard"
            },
            "BOJCardlessAuth": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCardlessAuth"
            },
            "BOJCancelCardlessRequest": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCancelCardlessRequest"
            },
            "BOJApplyDebitCard": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJApplyDebitCard"
            },
            "AccountTransactions": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/AccountTransactions"
            },
            "BOJBankList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJBankList"
            },
            "prGetExternalTransferComm": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/prGetExternalTransferComm"
            },
            "BOJGetCrCardHistTrans": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardHistTrans"
            },
            "GoogleAPIs": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GoogleAPIs"
            },
            "ManualUserCreation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/ManualUserCreation"
            },
            "BOJIPSReturnTransfer": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJIPSReturnTransfer"
            },
            "BOJDcChangeDefaultAcc": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJDcChangeDefaultAcc"
            },
            "BOJprCcPpPayment": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcPpPayment"
            },
            "BojGetPpRegisteredBilling": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BojGetPpRegisteredBilling"
            },
            "BOJGetEfwMappingCodes": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetEfwMappingCodes"
            },
            "loopCardUnLinkAccount": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/loopCardUnLinkAccount"
            },
            "BOJprGetFtdIntRate": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprGetFtdIntRate"
            },
            "BOJCrdtCrdChngSms": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCrdtCrdChngSms"
            },
            "BOJPrConfirmJmpPayments": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrConfirmJmpPayments"
            },
            "BOJPushNotification": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPushNotification"
            },
            "BOJprCcInstantCashback": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcInstantCashback"
            },
            "BOJNationalID": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJNationalID"
            },
            "LoopStatistics": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopStatistics"
            },
            "BOJEMVParse": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJEMVParse"
            },
            "BOJprCcEfwPaymentConfirm": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcEfwPaymentConfirm"
            },
            "BOJBranchList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJBranchList"
            },
            "BOJPrGetLoanAccDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrGetLoanAccDetails"
            },
            "BOJprIPSRegister": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprIPSRegister"
            },
            "BOJGetTransactionDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetTransactionDetails"
            },
            "BOJGetTransferFee": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetTransferFee"
            },
            "NotificationController": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/NotificationController"
            },
            "prCcInternetMailorder": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/prCcInternetMailorder"
            }
        },
        "Webapp": {
            "url": "https://mobileuat.bankofjordan.com/Share"
        }
    },
    runtimeAppVersion: "1.0",
    eventTypes: ["FormEntry", "Error", "Crash"],
};
sessionID = "";

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        isI18nLayoutConfigEnabled: true,
        APILevel: 7000
    })
};

function appInit(params) {
    skinsInit();
    initializeMVCTemplates();
    initializeUserWidgets();
    initializemainSegmentTemplate();
    initializesegacntstatementstmplt();
    initializesegaddnewpaye();
    initializesegBulkPaymentConfirmation();
    initializesegbulkpaymenttmp();
    initializesegCheckReOrderKA();
    initializesegDebitCardTrans();
    initializesegdirectionInfoKA();
    initializesegFAQRow();
    initializesegFAQSection();
    initializesegGetAllIPSAlias();
    initializesegInformation();
    initializesegJMPBene();
    initializesegListViewKA();
    initializesegmanagecardlesstmplt();
    initializesegmanagecardstmplt();
    initializesegmanagepayeetmplt();
    initializesegmentAndroidIcon();
    initializesegmentHeader();
    initializesegmentheadertmplt();
    initializesegmentPayPerson();
    initializesegmentWithChevron();
    initializesegmentWithDoubleLabel();
    initializesegmentWithIcon();
    initializesegmentWithLabel();
    initializesegmentWithoutIcon();
    initializesegmentWithSwitch();
    initializesegMessagesBoldTmplDraftKA();
    initializesegMessagesBoldTmplKA();
    initializesegMessagesTmplKA();
    initializesegphonecnct();
    initializesegSelectProductKA();
    initializesegShowAcc();
    initializesegTmpData();
    initializesegtmpltchecknocolor();
    initializesegtransactionchecktmplt();
    initializesegTransferAddExternal();
    initializetempCurrencySectionHeader();
    initializetempJomopayPopup();
    initializetempNUOClassificationTypesKA();
    initializetempP2PphonePayeeKA();
    initializetempP2PphonePayeeSectionsKA();
    initializetempPayPeopleKA();
    initializetempSectionHeaderP2P();
    initializetmpAccountDetailsScreen();
    initializetmpAccountLandingKA();
    initializetmpAccountSMSDisabled();
    initializetmpAccountSMSEnabled();
    initializetmpAccountsPayBill();
    initializetmpAccountType();
    initializetmpAccountTypes();
    initializetmpAccPrecheckNoTransactions();
    initializetmpAddressSearchResults();
    initializetmpAlert();
    initializetmpAllBeneHeader();
    initializetmpAtmServices();
    initializetmpBudgetListKA();
    initializetmpCalculateFx();
    initializetmpCardNumber();
    initializetmpCardsReorder();
    initializetmpChooseAccountsSettings();
    initializetmpCurrency();
    initializetmpDeviceRegstrationDisabled();
    initializetmpDeviceRegstrationEnabled();
    initializetmpExchangeRates();
    initializetmpFrequrncyData();
    initializetmpJomopaycontacts();
    initializetmpJomopayList();
    initializetmplateCompanyNameKA();
    initializetmpLinkedAccounts();
    initializetmpLocationAtmBranch();
    initializetmplsegLegendKA();
    initializetmpMonths();
    initializetmpNavigationOptKA();
    initializetmpPOT();
    initializetmpProductBundle();
    initializetmpQuickBalanceSeg();
    initializetmpSearchResultsKA();
    initializetmpSegAccountsTran();
    initializetmpSegAccTranNoData();
    initializetmpSegBotUserInput();
    initializetmpSegCardsLanding();
    initializetmpSegLoginMethods();
    initializetmpSegPaymentHistory();
    initializetmpSegRequestStatus();
    initializetmpsegSIList();
    initializetmpServiceTypeDataPrePaid();
    initializetmpTransactionBiller();
    initializetmpTransactionStatus();
    initializetmpTwoLabelsWithHyphen();
    initializetmpVersionKA();
    initializetmSegReleaseNote();
    initializetransactionTemplateColor();
    initializetransactionTemplateNoColor();
    initializetmpAuthHeader();
    initializeiphoneFooter();
    initializeiphoneFooterAr();
    initializetabBarAccounts();
    initializetabBarDeposits();
    initializetabBarMore();
    initializetabBarTransfer();
    initializeTemp0da2fb8b2bfdc47();
    initializemapAtm();
    initializemapATMBranch();
    initializemapATMBranchWithoutLocation();
    Form1Globals();
    frmAccountAlertsKAGlobals();
    frmAccountDetailKAGlobals();
    frmAccountDetailsScreenGlobals();
    frmAccountInfoKAGlobals();
    frmAccountsLandingKAGlobals();
    frmAccountsReorderKAGlobals();
    frmAccountTypeGlobals();
    frmAddExternalAccountHomeGlobals();
    frmAddExternalAccountKAGlobals();
    frmAddNewPayeeKAGlobals();
    frmAddNewPrePayeeKAGlobals();
    frmAlertHistoryGlobals();
    frmAlertsKAGlobals();
    frmApplyNewCardsConfirmKAGlobals();
    frmApplyNewCardsKAGlobals();
    frmATMPOSLimitGlobals();
    frmAuthorizationAlternativesGlobals();
    frmBillerListFormGlobals();
    frmBillsGlobals();
    frmBotSplashGlobals();
    frmBulkPaymentConfirmKAGlobals();
    frmBulkPaymentKAGlobals();
    frmCancelCardKAGlobals();
    frmCardlessSendMessageGlobals();
    frmCardlessTransactionGlobals();
    frmCardLinkedAccountsGlobals();
    frmCardLinkedAccountsConfirmGlobals();
    frmCardOperationsKAGlobals();
    frmCardsAddNicknameGlobals();
    frmCardsLandingKAGlobals();
    frmCardsListPreferencesGlobals();
    frmCardStatementKAGlobals();
    frmChatBotsGlobals();
    frmCheckingJointGlobals();
    frmCheckReOrderListKAGlobals();
    frmCheckReOrderSuccessKAGlobals();
    frmchequeimagesGlobals();
    frmChequeStatusGlobals();
    frmConfirmationCardKAGlobals();
    frmConfirmationPasswordKAGlobals();
    frmConfirmationPersonalDetailsKAGlobals();
    frmConfirmationUserNameKAGlobals();
    frmConfirmCashWithDrawGlobals();
    frmConfirmCashWithDrawQRCodeKAGlobals();
    frmConfirmDepositKAGlobals();
    frmConfirmNPPRegistrationGlobals();
    frmConfirmP2PRegistrationKAGlobals();
    frmConfirmPayBillGlobals();
    frmConfirmPrePayBillGlobals();
    frmConfirmStandingInstructionsGlobals();
    frmConfirmTransferKAGlobals();
    frmCongratulationsGlobals();
    frmContactUsKAGlobals();
    frmCreateCredentialsGlobals();
    frmCreditCardNumberGlobals();
    frmCreditCardPaymentGlobals();
    frmCreditCardsKAGlobals();
    frmCurrencyGlobals();
    frmDefaultLoginMethodKAGlobals();
    frmDepositPayLandingKAGlobals();
    frmDeviceDeRegistrationKAGlobals();
    frmDeviceRegisterationIncorrectPinActicvationKAGlobals();
    frmDeviceregistrarionSuccessKAGlobals();
    frmDeviceRegistrationGlobals();
    frmDeviceRegistrationOptionsKAGlobals();
    frmDirectionsKAGlobals();
    frmEditAccountSettingsGlobals();
    frmEditPayeeKAGlobals();
    frmEditPayeeSuccessBillPayKAGlobals();
    frmEditPayeeSuccessKAGlobals();
    frmEnableInternetTransactionKAGlobals();
    frmEnlargeAdKAGlobals();
    frmEnrolluserLandingKAGlobals();
    frmEnterLocationKAGlobals();
    frmEnterPersonalDetailsKAGlobals();
    frmEPSGlobals();
    frmEstatementConfirmKAGlobals();
    frmEstatementLandingKAGlobals();
    frmFaceIDCaptureGlobals();
    frmFacialAuthEnableGlobals();
    frmFilterSIGlobals();
    frmFilterTransactionGlobals();
    frmFrequencyListGlobals();
    frmFxRateGlobals();
    frmGetIPSAliasGlobals();
    frmInformationDetailsKAGlobals();
    frmInformationKAGlobals();
    frmInstantCashGlobals();
    frmIPSHomeGlobals();
    frmIPSManageBeneGlobals();
    frmIPSRegistrationGlobals();
    frmJoMoPayGlobals();
    frmJomoPayAccountListGlobals();
    frmJOMOPayAddBillerGlobals();
    frmJoMoPayConfirmationGlobals();
    frmJomopayContactsGlobals();
    frmJoMoPayLandingGlobals();
    frmJoMoPayQRConfirmGlobals();
    frmJomopayRegCountryGlobals();
    frmJoMoPayRegistrationGlobals();
    frmLanguageChangeGlobals();
    frmLoanPostponeGlobals();
    frmLocationDetailsKAGlobals();
    frmLocatorATMDetailsKAGlobals();
    frmLocatorBranchDetailsKAGlobals();
    frmLocatorKAGlobals();
    frmLoginAuthSuccessKAGlobals();
    frmLoginKAGlobals();
    frmManageCardLessGlobals();
    frmManageCardsKAGlobals();
    frmManagePayeeKAGlobals();
    frmMoreGlobals();
    frmMoreCheckReorderKAGlobals();
    frmMoreFaqKAGlobals();
    frmMoreForeignExchangeRatesKAGlobals();
    frmMoreInterestRatesKAGlobals();
    frmMoreLandingKAGlobals();
    frmMorePrivacyPolicyKAGlobals();
    frmMoreTermsAndConditionsKAGlobals();
    frmMyAccountSettingsKAGlobals();
    frmMyMoneyListKAGlobals();
    frmNewBillDetailsGlobals();
    frmNewBillKAGlobals();
    frmNewSubAccountConfirmGlobals();
    frmNewSubAccountLandingNewGlobals();
    frmNewTransferKAGlobals();
    frmNewUserOnboardVerificationKAGlobals();
    frmOpenTermDepositGlobals();
    frmOrderCheckBookGlobals();
    frmOrderChequeBookConfirmGlobals();
    frmPayBillHomeGlobals();
    frmPayeeDetailsKAGlobals();
    frmPayeeTransactionsKAGlobals();
    frmPaymentDashboardGlobals();
    frmPendingWithdrawSummaryGlobals();
    frmPickAProductKAGlobals();
    frmPinEntryStep1Globals();
    frmPinEntryStep2Globals();
    frmPinEntrySuccessGlobals();
    frmPostLoginAdvertisementKAGlobals();
    frmPOTGlobals();
    frmPreferredAccountsKAGlobals();
    frmPrePaidPayeeDetailsKAGlobals();
    frmProductBundleGlobals();
    frmReasonsListScreenGlobals();
    frmRecentDepositKAGlobals();
    frmRecentTransactionDetailsKAGlobals();
    frmRegisterUserGlobals();
    frmReleaseNotesKAGlobals();
    frmRequestPinCardGlobals();
    frmRequestStatementAccountsGlobals();
    frmRequestStatementAccountsConfirmGlobals();
    frmrequestStatusGlobals();
    frmScheduledTransactionDetailsKAGlobals();
    frmSearchResultsKAGlobals();
    frmSelectBankNameGlobals();
    frmSelectDetailsBeneGlobals();
    frmSetDefaultPageKAGlobals();
    frmSetLocatorDistanceFilterKAGlobals();
    frmSettingsKAGlobals();
    frmSetupFaceIdSettingsKAGlobals();
    frmSetUpPinSettingsGlobals();
    frmShowAllBeneficiaryGlobals();
    frmSmartInfoGlobals();
    frmSMSNotificationGlobals();
    frmSplashGlobals();
    frmStandingInstructionsGlobals();
    frmStopCardKAGlobals();
    frmSuccessFormKAGlobals();
    frmSuccessFormQRCodeKAGlobals();
    frmTermsAndConditionsKAGlobals();
    frmTermsAndCondtionsGlobals();
    frmtransactionChequeKAGlobals();
    frmTransactionDetailKAGlobals();
    frmTransactionDetailsGlobals();
    frmTransactionDetailsPFMKAGlobals();
    frmTransferStatusGlobals();
    frmUserSettingsMyProfileKAGlobals();
    frmUserSettingsPinLoginKAGlobals();
    frmUserSettingsSiriGlobals();
    frmUserSettingsSIRILIMITGlobals();
    frmUserSettingsTouchIdKAGlobals();
    frmWebChargeGlobals();
    PopupCancelAndReplaceCardGlobals();
    popupCommonAlertGlobals();
    PopupDeviceRegistrationGlobals();
    popupNotificationInfoGlobals();
    popupStopCardGlobals();
    setAppBehaviors();
};
kony.visualizer.actions.postAppInitCallBack = function(eventObj) {
    return AS_AppEvents_a1f7494243ba459d87b81e142176930a(eventObj);
};

function themeCallBack() {
    initializeGlobalVariables();
    callAppMenu();
    kony.application.setApplicationInitializationEvents({
        init: appInit,
        preappinit: AS_AppEvents_b70e24eaba84433d9d1808d9ee81b773,
        appservice: function(eventObject) {
            var value = AS_AppEvents_d7d33e4974bd4754b472728a81aff528(eventObject);
            return value;
        },
        postappinit: kony.visualizer.actions.postAppInitCallBack,
        showstartupform: function() {
            frmLanguageChange.show();
        }
    });
};

function onSuccess(oldlocalname, newlocalename, info) {
    loadResources();
};

function onFailure(errorcode, errormsg, info) {
    loadResources();
};

function loadResources() {
    globalhttpheaders = {};
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_LocationSettings"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_toastMsg"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.ND_com_kony"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.ND_com_ip"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_AutoReadSMS"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.NDD_com_kony_keychain"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_BojScan"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.ND_com_siriSetup"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_NSPDFViewer"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_PDFViewer"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_serviceCall"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_QRCodeScanner"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_onboardingConfig"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_onboardingNav"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_IOSonboardingConfig"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_LabibaConfig"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_LabibaConfigIOS"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_KonyLogger"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_binarydata"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.ND_binary_util"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_WindowsOfflineObjects"
    });
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
kony.i18n.setLocaleLayoutConfig({
    "ar": {
        "mirrorContentAlignment": true,
        "mirrorFlexPositionProperties": true,
        "mirrorFlowHorizontalAlignment": false
    },
    "en": {
        "mirrorContentAlignment": false,
        "mirrorFlexPositionProperties": false,
        "mirrorFlowHorizontalAlignment": false
    }
});
//This is the entry point for the application.When Locale comes,Local API call will be the entry point.
kony.i18n.setDefaultLocaleAsync("en", onSuccess, onFailure, null);
debugger;