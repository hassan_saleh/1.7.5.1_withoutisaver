function initializetmpsegSIList() {
    flxtmpSIList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxtmpSIList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxtmpSIList.setDefaultUnit(kony.flex.DP);
    var flxUserDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxUserDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "18%",
        "skin": "slFbox",
        "top": "0%",
        "width": "42%",
        "zIndex": 1
    }, {}, {});
    flxUserDetails.setDefaultUnit(kony.flex.DP);
    var lblTransactiondate = new kony.ui.Label({
        "centerY": "40%",
        "id": "lblTransactiondate",
        "isVisible": false,
        "left": "5%",
        "skin": "sknSIDate",
        "text": "10 Feb 2010",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransactionDesc = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblTransactionDesc",
        "isVisible": true,
        "left": "5%",
        "skin": "sknSIDesc",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxUserDetails.add(lblTransactiondate, lblTransactionDesc);
    var flxTAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxTAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "60%",
        "skin": "slFbox",
        "top": "0%",
        "width": "30%",
        "zIndex": 1
    }, {}, {});
    flxTAmount.setDefaultUnit(kony.flex.DP);
    var lblAmount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAmount",
        "isVisible": true,
        "right": "5%",
        "skin": "sknSIMoney",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTAmount.add(lblAmount);
    var CopyflxTAmount0bc17ffab355945 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "CopyflxTAmount0bc17ffab355945",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "90%",
        "skin": "slFbox",
        "top": "0%",
        "width": "10%",
        "zIndex": 1
    }, {}, {});
    CopyflxTAmount0bc17ffab355945.setDefaultUnit(kony.flex.DP);
    var CopylblAmount0a2751be25d5944 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblAmount0a2751be25d5944",
        "isVisible": true,
        "right": "40%",
        "skin": "sknArrowSI",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxTAmount0bc17ffab355945.add(CopylblAmount0a2751be25d5944);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "3%",
        "skin": "sknFlxRoundContainer",
        "top": "15%",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon1.add(lblInitial);
    flxtmpSIList.add(flxUserDetails, flxTAmount, CopyflxTAmount0bc17ffab355945, flxIcon1);
}