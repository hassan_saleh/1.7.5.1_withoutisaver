function addWidgetsfrmApplyNewCardsKA() {
    frmApplyNewCardsKA.setDefaultUnit(kony.flex.DP);
    var flxApplyNewCardHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxApplyNewCardHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 5
    }, {}, {});
    flxApplyNewCardHeader.setDefaultUnit(kony.flex.DP);
    var lblApplyCardTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "90%",
        "id": "lblApplyCardTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.cards.applynewcard"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnNext = new kony.ui.Button({
        "focusSkin": "jomopaynextDisabled",
        "height": "100%",
        "id": "btnNext",
        "isVisible": true,
        "onClick": AS_Button_e6281b68c4d14d4a9f9899c33f489d70,
        "right": "0.00%",
        "skin": "jomopaynextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "top": "0.18%",
        "width": "20%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onTouchStart": AS_FlexContainer_hd869cf49ea444e0ac0cf364ea170db6,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 2
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    flxApplyNewCardHeader.add(lblApplyCardTitle, btnNext, flxBack);
    var flxApplynewCardsFirst = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "90%",
        "id": "flxApplynewCardsFirst",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxApplynewCardsFirst.setDefaultUnit(kony.flex.DP);
    var lblAccountStaticText = new kony.ui.Label({
        "id": "lblAccountStaticText",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.cards.cardtype"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPrimaryDebitCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxPrimaryDebitCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPrimaryDebitCard.setDefaultUnit(kony.flex.DP);
    var btnPrimaryDebitCard = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnPrimaryDebitCard",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_c22fa069c670419fb18f77bd20430848,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblPrimaryDebitCardTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblPrimaryDebitCardTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": kony.i18n.getLocalizedString("i18n.cards.PrimaryDebitCard"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPrimaryDebitCard.add(btnPrimaryDebitCard, lblPrimaryDebitCardTitle);
    var flxSuplementryDebitCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxSuplementryDebitCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSuplementryDebitCard.setDefaultUnit(kony.flex.DP);
    var btnSuplementryDebitCard = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnSuplementryDebitCard",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_a44944b377084c7585f379ae4e07d837,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSuplementryDebitCardTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSuplementryDebitCardTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": kony.i18n.getLocalizedString("i18n.cards.SuplementryDebitCard"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSuplementryDebitCard.add(btnSuplementryDebitCard, lblSuplementryDebitCardTitle);
    var flxSticker = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxSticker",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSticker.setDefaultUnit(kony.flex.DP);
    var btnSticker = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnSticker",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_haa46aa00a9b4de5bbbe6be325b63eb2,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblStickerTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblStickerTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": kony.i18n.getLocalizedString("i18n.cards.Sticker"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSticker.add(btnSticker, lblStickerTitle);
    var flxReplaceDebitCards = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxReplaceDebitCards",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxReplaceDebitCards.setDefaultUnit(kony.flex.DP);
    var btnReplaceDebitCards = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnReplaceDebitCards",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_ab9ab0fe7884491589d7b06781b0118b,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblReplaceDebitCardsTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblReplaceDebitCardsTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": kony.i18n.getLocalizedString("i18n.cards.ReplaceDebitCards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxReplaceDebitCards.add(btnReplaceDebitCards, lblReplaceDebitCardsTitle);
    var flxChequeBook = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxChequeBook",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxChequeBook.setDefaultUnit(kony.flex.DP);
    var btnChequeBook = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnChequeBook",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_g6dd03ea303d447a8bc4fb6b128e460e,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblChequeBookTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblChequeBookTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": "Cheque Book",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxChequeBook.add(btnChequeBook, lblChequeBookTitle);
    var flxPrimaryCreditCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxPrimaryCreditCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPrimaryCreditCard.setDefaultUnit(kony.flex.DP);
    var btnPrimaryCreditCard = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnPrimaryCreditCard",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_d13641c68e3e4d19802cd96f1a37ccda,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblPrimaryCreditCardTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblPrimaryCreditCardTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": kony.i18n.getLocalizedString("i18n.cards.PrimaryCreditCard"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPrimaryCreditCard.add(btnPrimaryCreditCard, lblPrimaryCreditCardTitle);
    var flxSuplementryCreditCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxSuplementryCreditCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSuplementryCreditCard.setDefaultUnit(kony.flex.DP);
    var btnSuplementryCreditCard = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnSuplementryCreditCard",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_g61003af099b4e6c94e8b683300c5157,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSuplementryCreditCardTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSuplementryCreditCardTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": kony.i18n.getLocalizedString("i18n.cards.SuplementryCreditCard"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSuplementryCreditCard.add(btnSuplementryCreditCard, lblSuplementryCreditCardTitle);
    var flxPrimaryCreditCardReplace = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxPrimaryCreditCardReplace",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPrimaryCreditCardReplace.setDefaultUnit(kony.flex.DP);
    var btnPrimaryCreditCardReplace = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnPrimaryCreditCardReplace",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_j7981396e7464024af4878f028d30afb,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblPrimaryCreditCardReplaceTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblPrimaryCreditCardReplaceTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": kony.i18n.getLocalizedString("i18n.cards.PrimaryCreditCardReplace"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPrimaryCreditCardReplace.add(btnPrimaryCreditCardReplace, lblPrimaryCreditCardReplaceTitle);
    var flxUpgradeCreditCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxUpgradeCreditCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxUpgradeCreditCard.setDefaultUnit(kony.flex.DP);
    var btnUpgradeCreditCard = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnUpgradeCreditCard",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_d80742e86f584c558b5a3df3b7ea1d81,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUpgradeCreditCardTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblUpgradeCreditCardTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": kony.i18n.getLocalizedString("i18n.cards.UpgradeCreditCard"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxUpgradeCreditCard.add(btnUpgradeCreditCard, lblUpgradeCreditCardTitle);
    var flxWearables = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxWearables",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxWearables.setDefaultUnit(kony.flex.DP);
    var btnWearables = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnWearables",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_ef8316cd4391405c8de43abda17f0105,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblWearablesTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblWearablesTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": kony.i18n.getLocalizedString("i18n.cards.Wearable"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxWearables.add(btnWearables, lblWearablesTitle);
    var flxWebChargeCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxWebChargeCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxWebChargeCard.setDefaultUnit(kony.flex.DP);
    var btnWebChargeCard = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnWebChargeCard",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_c1b74c9dfe3749998145d9ce68687a24,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblWebChargeCardTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblWebChargeCardTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": kony.i18n.getLocalizedString("i18n.cards.WebchargeCard"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxWebChargeCard.add(btnWebChargeCard, lblWebChargeCardTitle);
    var flxReplacePrepaidCards = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxReplacePrepaidCards",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "-2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxReplacePrepaidCards.setDefaultUnit(kony.flex.DP);
    var btnReplacePrepaidCards = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "id": "btnReplacePrepaidCards",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_gc92e32951044dac996be50be4c3c0ef,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "s",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblReplacePrepaidCardsTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblReplacePrepaidCardsTitle",
        "isVisible": true,
        "left": "10%",
        "skin": "sknWhite50OPC150SIZE",
        "text": kony.i18n.getLocalizedString("i18n.cards.ReplacePrepaidCards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxReplacePrepaidCards.add(btnReplacePrepaidCards, lblReplacePrepaidCardsTitle);
    flxApplynewCardsFirst.add(lblAccountStaticText, flxPrimaryDebitCard, flxSuplementryDebitCard, flxSticker, flxReplaceDebitCards, flxChequeBook, flxPrimaryCreditCard, flxSuplementryCreditCard, flxPrimaryCreditCardReplace, flxUpgradeCreditCard, flxWearables, flxWebChargeCard, flxReplacePrepaidCards);
    var flxApplyNewCardsSecond = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": false,
        "enableScrolling": true,
        "height": "90%",
        "horizontalScrollIndicator": true,
        "id": "flxApplyNewCardsSecond",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "10%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxApplyNewCardsSecond.setDefaultUnit(kony.flex.DP);
    var flxPrimaryCardNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxPrimaryCardNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPrimaryCardNumber.setDefaultUnit(kony.flex.DP);
    var lblPrimaryCardNumberTitle = new kony.ui.Label({
        "id": "lblPrimaryCardNumberTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newCard.primarycreditcardnumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnPrimaryCardNumber = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "50%",
        "id": "btnPrimaryCardNumber",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_b556f1c31d1841f6be38c5d00ef3cd6b,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblPrimaryCardNumber = new kony.ui.Label({
        "height": "50%",
        "id": "lblPrimaryCardNumber",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderCreditCardNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderCreditCardNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderCreditCardNumber.setDefaultUnit(kony.flex.DP);
    flxBorderCreditCardNumber.add();
    flxPrimaryCardNumber.add(lblPrimaryCardNumberTitle, btnPrimaryCardNumber, lblPrimaryCardNumber, flxBorderCreditCardNumber);
    var flxUpgradeCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxUpgradeCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxUpgradeCard.setDefaultUnit(kony.flex.DP);
    var lblUpgradeCardTitle = new kony.ui.Label({
        "id": "lblUpgradeCardTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.cards.upgradetype"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnUpgradeCard = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "50%",
        "id": "btnUpgradeCard",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_de0624bb628d4b769328fe20d9627607,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUpgradeCard = new kony.ui.Label({
        "height": "50%",
        "id": "lblUpgradeCard",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderUpgradeCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderUpgradeCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderUpgradeCard.setDefaultUnit(kony.flex.DP);
    flxBorderUpgradeCard.add();
    flxUpgradeCard.add(lblUpgradeCardTitle, btnUpgradeCard, lblUpgradeCard, flxBorderUpgradeCard);
    var flxSuplementryCardType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxSuplementryCardType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSuplementryCardType.setDefaultUnit(kony.flex.DP);
    var lblSuplementryCardTypeTitle = new kony.ui.Label({
        "id": "lblSuplementryCardTypeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newCard.supplementrycardtype"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnSuplementryType = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "50%",
        "id": "btnSuplementryType",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_bd01a1585c0043d7aa328e33372dbb35,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSuplementryCardType = new kony.ui.Label({
        "height": "50%",
        "id": "lblSuplementryCardType",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderSuplementryCardType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderSuplementryCardType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderSuplementryCardType.setDefaultUnit(kony.flex.DP);
    flxBorderSuplementryCardType.add();
    flxSuplementryCardType.add(lblSuplementryCardTypeTitle, btnSuplementryType, lblSuplementryCardType, flxBorderSuplementryCardType);
    var flxCardHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxCardHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxCardHolder.setDefaultUnit(kony.flex.DP);
    var lblCardHolderTitle = new kony.ui.Label({
        "id": "lblCardHolderTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newCard.cardholdername"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtCardHolder = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "50%",
        "id": "txtCardHolder",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "onTextChange": AS_TextField_i0277a740492477aac6742914cc998ae,
        "onTouchEnd": AS_TextField_je871c74358d4235a4b0fd4360e1d04c,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderCardHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderCardHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderCardHolder.setDefaultUnit(kony.flex.DP);
    flxBorderCardHolder.add();
    flxCardHolder.add(lblCardHolderTitle, txtCardHolder, flxBorderCardHolder);
    var lblHintCardHolder = new kony.ui.Label({
        "bottom": "0%",
        "id": "lblHintCardHolder",
        "isVisible": false,
        "right": "7%",
        "skin": "sknInvalidCredentailsRegular70PER",
        "text": kony.i18n.getLocalizedString("i18n.common.englishonly"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxRelationshipType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxRelationshipType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRelationshipType.setDefaultUnit(kony.flex.DP);
    var lblRelationshipTypeTitle = new kony.ui.Label({
        "id": "lblRelationshipTypeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.applycards.realationship"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnRelationshipType = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "50%",
        "id": "btnRelationshipType",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_ad1a22517f8647b89a973648e1e7c0a7,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblRelationshipType = new kony.ui.Label({
        "height": "50%",
        "id": "lblRelationshipType",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderRelationShip = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderRelationShip",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderRelationShip.setDefaultUnit(kony.flex.DP);
    flxBorderRelationShip.add();
    flxRelationshipType.add(lblRelationshipTypeTitle, btnRelationshipType, lblRelationshipType, flxBorderRelationShip);
    var flxAge = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxAge",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAge.setDefaultUnit(kony.flex.DP);
    var lblAgeTitle = new kony.ui.Label({
        "id": "lblAgeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.common.age"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderAge = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderAge",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderAge.setDefaultUnit(kony.flex.DP);
    flxBorderAge.add();
    var txtAge = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "50%",
        "id": "txtAge",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "maxTextLength": 3,
        "onTextChange": AS_TextField_g3066e67b0554a08826c1524c5b8b3c8,
        "onTouchEnd": AS_TextField_e9c775f7d8374b8d87422d9d5f11c1cf,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxAge.add(lblAgeTitle, flxBorderAge, txtAge);
    var flxReason = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxReason",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxReason.setDefaultUnit(kony.flex.DP);
    var lblReasonTitle = new kony.ui.Label({
        "id": "lblReasonTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newCard.reason"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnReason = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "50%",
        "id": "btnReason",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_g49eb981ff5a4833a8b1dd5a9ead9062,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblReason = new kony.ui.Label({
        "height": "50%",
        "id": "lblReason",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderReason = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderReason",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderReason.setDefaultUnit(kony.flex.DP);
    flxBorderReason.add();
    flxReason.add(lblReasonTitle, btnReason, lblReason, flxBorderReason);
    var flxColor = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxColor",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxColor.setDefaultUnit(kony.flex.DP);
    var lblColorTitle = new kony.ui.Label({
        "id": "lblColorTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.common.color"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblColor = new kony.ui.Label({
        "height": "50%",
        "id": "lblColor",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnColor = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "50%",
        "id": "btnColor",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_be1caccc602b4e61b4972cc6802b1ff8,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBorderColor = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderColor",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderColor.setDefaultUnit(kony.flex.DP);
    flxBorderColor.add();
    flxColor.add(lblColorTitle, lblColor, btnColor, flxBorderColor);
    var flxLimitType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxLimitType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxLimitType.setDefaultUnit(kony.flex.DP);
    var flxLimitTypeTitle = new kony.ui.Label({
        "id": "flxLimitTypeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newCard.limittype"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnLimitType = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "50%",
        "id": "btnLimitType",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_f9b9284d862d4b7daeb12bdd79d2e18c,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLimitType = new kony.ui.Label({
        "height": "50%",
        "id": "lblLimitType",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderLimitType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderLimitType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderLimitType.setDefaultUnit(kony.flex.DP);
    flxBorderLimitType.add();
    flxLimitType.add(flxLimitTypeTitle, btnLimitType, lblLimitType, flxBorderLimitType);
    var flxSuplementryCardLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxSuplementryCardLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSuplementryCardLimit.setDefaultUnit(kony.flex.DP);
    var lblSuplementryCardLimitTitle = new kony.ui.Label({
        "id": "lblSuplementryCardLimitTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newCard.supplementrycardlimit"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtSuplementryCardLimit = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "50%",
        "id": "txtSuplementryCardLimit",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "onTextChange": AS_TextField_ddf88f993079431789c712f1ad2a85fb,
        "onTouchEnd": AS_TextField_d2229fa273bd413eba11f1095589221d,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_f40fbc2521034dda82d8462098326e51,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderSuplementryCardLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderSuplementryCardLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderSuplementryCardLimit.setDefaultUnit(kony.flex.DP);
    flxBorderSuplementryCardLimit.add();
    flxSuplementryCardLimit.add(lblSuplementryCardLimitTitle, txtSuplementryCardLimit, flxBorderSuplementryCardLimit);
    var flxEmploymentStatus = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxEmploymentStatus",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxEmploymentStatus.setDefaultUnit(kony.flex.DP);
    var lblEmploymentStatus = new kony.ui.Label({
        "height": "50%",
        "id": "lblEmploymentStatus",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "37%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEmploymentStatusTitle = new kony.ui.Label({
        "id": "lblEmploymentStatusTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newCard.employmentstatus"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderEmploymentStatus = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderEmploymentStatus",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderEmploymentStatus.setDefaultUnit(kony.flex.DP);
    flxBorderEmploymentStatus.add();
    var btnEmploymentStatus = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "50%",
        "id": "btnEmploymentStatus",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_efd7a2e2b78940c2b651b3503ada7e27,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxEmploymentStatus.add(lblEmploymentStatus, lblEmploymentStatusTitle, flxBorderEmploymentStatus, btnEmploymentStatus);
    var flxProfession = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxProfession",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxProfession.setDefaultUnit(kony.flex.DP);
    var lblProfessionTitle = new kony.ui.Label({
        "id": "lblProfessionTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newcards.profession"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderProfession = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderProfession",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderProfession.setDefaultUnit(kony.flex.DP);
    flxBorderProfession.add();
    var txtProfession = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "50%",
        "id": "txtProfession",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "onTextChange": AS_TextField_f81ccf7f4bac49c788da0b27992d6a2a,
        "onTouchEnd": AS_TextField_i0bcb5dbac8b4d87bbead7256c2619e1,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_acdce5469bee41b98cca02e58c8559e4,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxProfession.add(lblProfessionTitle, flxBorderProfession, txtProfession);
    var flxTransferedSalary = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxTransferedSalary",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTransferedSalary.setDefaultUnit(kony.flex.DP);
    var lblTransferedSalaryTitle = new kony.ui.Label({
        "id": "lblTransferedSalaryTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newCard.transferedsalary"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderTransferedSalary = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderTransferedSalary",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderTransferedSalary.setDefaultUnit(kony.flex.DP);
    flxBorderTransferedSalary.add();
    var txtTransferedSalary = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "50%",
        "id": "txtTransferedSalary",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "onTextChange": AS_TextField_dcb401986a3f4779b7a23aa125ed3fd5,
        "onTouchEnd": AS_TextField_fcf6f61e280d4771b3bfad83e746e186,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_add135b9c31e4e28af233cb744c20669,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxTransferedSalary.add(lblTransferedSalaryTitle, flxBorderTransferedSalary, txtTransferedSalary);
    var flxMonthlyIncome = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxMonthlyIncome",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxMonthlyIncome.setDefaultUnit(kony.flex.DP);
    var lblMonthlyIncomeTitle = new kony.ui.Label({
        "id": "lblMonthlyIncomeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newCard.monthlyincome"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderMonthlyIncome = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderMonthlyIncome",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderMonthlyIncome.setDefaultUnit(kony.flex.DP);
    flxBorderMonthlyIncome.add();
    var txtMonthlyIncome = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "50%",
        "id": "txtMonthlyIncome",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "onTextChange": AS_TextField_c65b51ea088a474ca03ccd78d16ecd64,
        "onTouchEnd": AS_TextField_a3f64cca64a14855a44d81e05cd95f47,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_f08d0dfd156046b3886696cf5d60188d,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxMonthlyIncome.add(lblMonthlyIncomeTitle, flxBorderMonthlyIncome, txtMonthlyIncome);
    var flxRequestLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxRequestLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRequestLimit.setDefaultUnit(kony.flex.DP);
    var lblRequestLimitTitle = new kony.ui.Label({
        "id": "lblRequestLimitTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newCard.requestlimit"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderRequestLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderRequestLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderRequestLimit.setDefaultUnit(kony.flex.DP);
    flxBorderRequestLimit.add();
    var txtRequestLimit = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "50%",
        "id": "txtRequestLimit",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "onTextChange": AS_TextField_e30846faa3e2449c9b8361ac6331f246,
        "onTouchEnd": AS_TextField_j95497a30be94c3f96a09b920c5a1d67,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_a37c84ca7f324d4f801daff4ddb7deff,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxRequestLimit.add(lblRequestLimitTitle, flxBorderRequestLimit, txtRequestLimit);
    var lblRequestLimitHint = new kony.ui.Label({
        "id": "lblRequestLimitHint",
        "isVisible": true,
        "right": "5%",
        "skin": "sknLblSmallWhite",
        "text": kony.i18n.getLocalizedString("i18n.applycards.cardlimit"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAccountNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxAccountNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "onClick": AS_Button_h469116c554740b485052ead96391b7d,
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAccountNumber.setDefaultUnit(kony.flex.DP);
    var lblAccountNumberTitle = new kony.ui.Label({
        "id": "lblAccountNumberTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnAccountNumber = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "50%",
        "id": "btnAccountNumber",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_h469116c554740b485052ead96391b7d,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblAccountNumber = new kony.ui.Label({
        "height": "50%",
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderAccountNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderAccountNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderAccountNumber.setDefaultUnit(kony.flex.DP);
    flxBorderAccountNumber.add();
    flxAccountNumber.add(lblAccountNumberTitle, btnAccountNumber, lblAccountNumber, flxBorderAccountNumber);
    var lblMobileNumberHint = new kony.ui.Label({
        "id": "lblMobileNumberHint",
        "isVisible": false,
        "left": "5%",
        "skin": "sknInvalidCredentailsRegular70PER",
        "text": kony.i18n.getLocalizedString("i18n.applycards.limitexceesmsg"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-4%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxDeliveryMode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxDeliveryMode",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDeliveryMode.setDefaultUnit(kony.flex.DP);
    var lblDeliveryModeTitle = new kony.ui.Label({
        "id": "lblDeliveryModeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.accounts.deliveryMode"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxDeliveryModeBranch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxDeliveryModeBranch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "onClick": AS_FlexContainer_cd2ada5379164cf9a63516e05945eaaf,
        "skin": "slFbox",
        "top": "50%",
        "width": "28%",
        "zIndex": 1
    }, {}, {});
    flxDeliveryModeBranch.setDefaultUnit(kony.flex.DP);
    var btnDeliveryModeBranch = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "60%",
        "id": "btnDeliveryModeBranch",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_FlexContainer_cd2ada5379164cf9a63516e05945eaaf,
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "t",
        "width": "29%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblDeliveryModeBranch = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblDeliveryModeBranch",
        "isVisible": true,
        "left": "35%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.locateus.branch"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDeliveryModeBranch.add(btnDeliveryModeBranch, lblDeliveryModeBranch);
    var flxDeliverModeMailAddress = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxDeliverModeMailAddress",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "35%",
        "onClick": AS_FlexContainer_cd2ada5379164cf9a63516e05945eaaf,
        "skin": "slFbox",
        "top": "50%",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flxDeliverModeMailAddress.setDefaultUnit(kony.flex.DP);
    var btnDeliverModeMailAddress = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "60%",
        "id": "btnDeliverModeMailAddress",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_FlexContainer_cd2ada5379164cf9a63516e05945eaaf,
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "s",
        "width": "18%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblDeliverModeMailAddress = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblDeliverModeMailAddress",
        "isVisible": true,
        "left": "20%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.newcards.courier"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDeliverModeMailAddress.add(btnDeliverModeMailAddress, lblDeliverModeMailAddress);
    flxDeliveryMode.add(lblDeliveryModeTitle, flxDeliveryModeBranch, flxDeliverModeMailAddress);
    var flxBranchName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxBranchName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBranchName.setDefaultUnit(kony.flex.DP);
    var lblBranchNameTitle = new kony.ui.Label({
        "id": "lblBranchNameTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Map.Branchname"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnBranchName = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "50%",
        "id": "btnBranchName",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_ba043f4abef044cbaecabba6ee0ec1eb,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBranchName = new kony.ui.Label({
        "height": "50%",
        "id": "lblBranchName",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBorderBranch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderBranch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderBranch.setDefaultUnit(kony.flex.DP);
    flxBorderBranch.add();
    flxBranchName.add(lblBranchNameTitle, btnBranchName, lblBranchName, flxBorderBranch);
    var flxAddress = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxAddress",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAddress.setDefaultUnit(kony.flex.DP);
    var lblAddressTitle = new kony.ui.Label({
        "id": "lblAddressTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.common.address"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtAddress = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "60%",
        "id": "txtAddress",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "onTextChange": AS_TextField_af1ec3ff1e524a47913a0b359223ba0b,
        "onTouchEnd": AS_TextField_a98377b5b43a4fce8c24d5c670e35bea,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "30%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_c866a1ca042c4bafa7bc64c557cbf678,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderAddress = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderAddress",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "90%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderAddress.setDefaultUnit(kony.flex.DP);
    flxBorderAddress.add();
    flxAddress.add(lblAddressTitle, txtAddress, flxBorderAddress);
    var flxNotes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxNotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNotes.setDefaultUnit(kony.flex.DP);
    var lblNotesTitle = new kony.ui.Label({
        "id": "lblNotesTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.newCard.notes"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtNotes = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "50%",
        "id": "txtNotes",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "onTextChange": AS_TextField_g8948cacdecc4b43beb9f1e8317b378b,
        "onTouchEnd": AS_TextField_ba5d0c76ac0c43baae2eb0cfd2e773f3,
        "secureTextEntry": false,
        "skin": "txtBox0b5a21c6c49d64b",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_j84e389876b24f8fb5b995203646875b,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderNotes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderNotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerGreen",
        "top": "86%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBorderNotes.setDefaultUnit(kony.flex.DP);
    flxBorderNotes.add();
    flxNotes.add(lblNotesTitle, txtNotes, flxBorderNotes);
    var lblReasonHint = new kony.ui.Label({
        "id": "lblReasonHint",
        "isVisible": false,
        "left": "5%",
        "skin": "sknTransferType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxApplyNewCardsSecond.add(flxPrimaryCardNumber, flxUpgradeCard, flxSuplementryCardType, flxCardHolder, lblHintCardHolder, flxRelationshipType, flxAge, flxReason, flxColor, flxLimitType, flxSuplementryCardLimit, flxEmploymentStatus, flxProfession, flxTransferedSalary, flxMonthlyIncome, flxRequestLimit, lblRequestLimitHint, flxAccountNumber, lblMobileNumberHint, flxDeliveryMode, flxBranchName, flxAddress, flxNotes, lblReasonHint);
    frmApplyNewCardsKA.add(flxApplyNewCardHeader, flxApplynewCardsFirst, flxApplyNewCardsSecond);
};

function frmApplyNewCardsKAGlobals() {
    frmApplyNewCardsKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmApplyNewCardsKA,
        "bounces": false,
        "enabledForIdleTimeout": true,
        "id": "frmApplyNewCardsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "sknmainGradient",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};