function addWidgetsfrmCardlessTransaction() {
    frmCardlessTransaction.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "CopyslFbox0cd4df9aea5a14b",
        "top": "0.00%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFlxHeaderImg",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxback = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxback",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_ja068a57b12441188611d9f8ba460ca7,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 2
    }, {}, {});
    flxback.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0%",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxback.add(lblBackIcon, lblBack);
    var lblCardlessTitle = new kony.ui.Label({
        "height": "90%",
        "id": "lblCardlessTitle",
        "isVisible": true,
        "left": "20%",
        "minHeight": "90%",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.Cardless.NewCardless"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnNextCardless = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "90%",
        "id": "btnNextCardless",
        "isVisible": true,
        "onClick": AS_Button_bc9c73dd782947a9a224ee682904a362,
        "right": "0%",
        "skin": "jomopaynextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "top": "0%",
        "width": "20%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeader.add(flxback, lblCardlessTitle, btnNextCardless);
    var flxBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "91%",
        "id": "flxBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBody.setDefaultUnit(kony.flex.DP);
    var flxAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90dp",
        "id": "flxAccount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxAccount.setDefaultUnit(kony.flex.DP);
    var flxPaymentModeBulk = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "65%",
        "id": "flxPaymentModeBulk",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "right": 0,
        "skin": "slFbox",
        "top": "0.00%",
        "width": "90%",
        "zIndex": 2
    }, {}, {});
    flxPaymentModeBulk.setDefaultUnit(kony.flex.DP);
    var tbxPaymentModeBulk = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxPaymentModeBulk",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": null,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlinePaymentModeBulk = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "3%",
        "id": "flxUnderlinePaymentModeBulk",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlinePaymentModeBulk.setDefaultUnit(kony.flex.DP);
    flxUnderlinePaymentModeBulk.add();
    var flxPaymentModeTypeHolderBulk = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxPaymentModeTypeHolderBulk",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_d7efbb940b094c7da970be41f1d10215,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxPaymentModeTypeHolderBulk.setDefaultUnit(kony.flex.DP);
    var lblPaymentMode = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblPaymentMode",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "88%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowPaymentMode = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowPaymentMode",
        "isVisible": true,
        "left": "93%",
        "skin": "sknBackIconDisabled",
        "text": kony.i18n.getLocalizedString("i18n.common.baclinfo"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBranchCode = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBranchCode",
        "isVisible": false,
        "left": "93%",
        "skin": "sknBackIconDisabled",
        "text": "1",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPaymentModeTypeHolderBulk.add(lblPaymentMode, lblArrowPaymentMode, lblBranchCode);
    flxPaymentModeBulk.add(tbxPaymentModeBulk, flxUnderlinePaymentModeBulk, flxPaymentModeTypeHolderBulk);
    flxAccount.add(flxPaymentModeBulk);
    var flxAmountDetail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "30%",
        "id": "flxAmountDetail",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxAmountDetail.setDefaultUnit(kony.flex.DP);
    var lblAmountStaticTexts = new kony.ui.Label({
        "id": "lblAmountStaticTexts",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Cardless.amountWithdraw"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxSelectAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "50%",
        "id": "flxSelectAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "25%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSelectAmount.setDefaultUnit(kony.flex.DP);
    var txtFieldAmount = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "focusSkin": "txtBox0b5a21c6c49d64b",
        "height": "80%",
        "id": "txtFieldAmount",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "left": "25%",
        "maxTextLength": 9,
        "onDone": AS_TextField_aab002d3b3ff4972814c0b74ff660db2,
        "onTextChange": AS_TextField_a13aceb3120a4f7e8074fff5bf7ee866,
        "placeholder": "0",
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": 0,
        "width": "30%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_bb5866a526ad4a92b5a1df88c0986ac5,
        "onEndEditing": AS_TextField_fa5aa4c816044572bc5296bc0dde018d,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxBorderAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "25%",
        "skin": "skntextFieldDivider",
        "top": "75%",
        "width": "45%",
        "zIndex": 1
    }, {}, {});
    flxBorderAmount.setDefaultUnit(kony.flex.DP);
    flxBorderAmount.add();
    var lblHiddenAmount = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblHiddenAmount",
        "isVisible": false,
        "skin": "lblAccountStaticText",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurrencyCode = new kony.ui.Label({
        "height": "60%",
        "id": "lblCurrencyCode",
        "isVisible": true,
        "left": "60%",
        "skin": "sknSIDesc",
        "text": "JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "23%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPlusContainer = new kony.ui.Button({
        "height": "30dp",
        "id": "flxPlusContainer",
        "isVisible": true,
        "onClick": AS_Button_b31529ae056e447382c1c0621ad769b4,
        "right": "10%",
        "skin": "plusMinusSkin",
        "text": "u",
        "top": "18dp",
        "width": "30dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxMinusContainer = new kony.ui.Button({
        "height": "30dp",
        "id": "flxMinusContainer",
        "isVisible": true,
        "left": "10%",
        "onClick": AS_Button_gc6b7d93b2e64165a9f21e25995406c5,
        "skin": "plusMinusSkin",
        "text": "-",
        "top": "18dp",
        "width": "30dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSelectAmount.add(txtFieldAmount, flxBorderAmount, lblHiddenAmount, lblCurrencyCode, flxPlusContainer, flxMinusContainer);
    var lblAmountFieldNote = new kony.ui.Label({
        "bottom": "22%",
        "centerX": "50%",
        "id": "lblAmountFieldNote",
        "isVisible": true,
        "left": "25%",
        "skin": "latoRegular24px",
        "text": kony.i18n.getLocalizedString("i18n.cardless.AmountFieldNote"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPreDefinedAmounts = new kony.ui.Label({
        "bottom": "0%",
        "id": "lblPreDefinedAmounts",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Cardless.preDefined"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAmountDetail.add(lblAmountStaticTexts, flxSelectAmount, lblAmountFieldNote, lblPreDefinedAmounts);
    var flxAmounts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "130dp",
        "id": "flxAmounts",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "4%",
        "skin": "flxBGKA",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAmounts.setDefaultUnit(kony.flex.DP);
    var flxFirtsRow = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45%",
        "id": "flxFirtsRow",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "3%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFirtsRow.setDefaultUnit(kony.flex.DP);
    var btnTenJOD = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "75%",
        "id": "btnTenJOD",
        "isVisible": true,
        "left": "2%",
        "onClick": AS_Button_d3376bfe66c940a8af596956bba6fa73,
        "skin": "slButtonBlueFocus",
        "text": "10 JOD",
        "top": "2%",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnFiJOD = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "75%",
        "id": "btnFiJOD",
        "isVisible": true,
        "left": "2%",
        "onClick": AS_Button_de71918370534d6d8e35f6734aa1cb1f,
        "skin": "slButtonBlueFocus",
        "text": "50 JOD",
        "top": "2%",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnHuJOD = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "75%",
        "id": "btnHuJOD",
        "isVisible": true,
        "left": "2%",
        "onClick": AS_Button_j8be8ba74f2246fc9e949bbc2f2b095a,
        "skin": "slButtonBlueFocus",
        "text": "100 JOD",
        "top": "2%",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxFirtsRow.add(btnTenJOD, btnFiJOD, btnHuJOD);
    var flxSecondRow = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45%",
        "id": "flxSecondRow",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSecondRow.setDefaultUnit(kony.flex.DP);
    var btnHuFiJOD = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "75%",
        "id": "btnHuFiJOD",
        "isVisible": true,
        "left": "2%",
        "onClick": AS_Button_bfba1d6aa0334c86a44a56e0b58b2b5d,
        "skin": "slButtonBlueFocus",
        "text": "150 JOD",
        "top": "2%",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnTwoHJOD = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "75%",
        "id": "btnTwoHJOD",
        "isVisible": true,
        "left": "2%",
        "onClick": AS_Button_dc65fca6585d4d87a64f1a1e41494abb,
        "skin": "slButtonBlueFocus",
        "text": "200 JOD",
        "top": "2%",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnTwoFiJOD = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknOrangeBGRNDBOJ",
        "height": "75%",
        "id": "btnTwoFiJOD",
        "isVisible": true,
        "left": "2%",
        "onClick": AS_Button_c6aa21f05d6e4a5cb7f60d381bf9ca81,
        "skin": "slButtonBlueFocus",
        "text": "500 JOD",
        "top": "2%",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSecondRow.add(btnHuFiJOD, btnTwoHJOD, btnTwoFiJOD);
    flxAmounts.add(flxFirtsRow, flxSecondRow);
    var flxPassword = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "120dp",
        "id": "flxPassword",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPassword.setDefaultUnit(kony.flex.DP);
    var lblPasswordNote2 = new kony.ui.Label({
        "id": "lblPasswordNote2",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.cardless.PasswordFieldNote2"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var tbxPassword = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "45%",
        "id": "tbxPassword",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "maxTextLength": 6,
        "onDone": AS_TextField_f17beaab32c2418384c9e2c50d1b592f,
        "onTextChange": AS_TextField_f5ce4076c1914ae1b76fe485b21b0d93,
        "onTouchEnd": AS_TextField_cefda326474e480aac594724c468c4d6,
        "placeholder": kony.i18n.getLocalizedString("i18n.cardless.PasswordField"),
        "secureTextEntry": true,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "38%",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onEndEditing": AS_TextField_j94675ad8b064115b551fd02d7116193,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlinePassword = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlinePassword",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "skin": "sknFlxGreyLine",
        "top": "75%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlinePassword.setDefaultUnit(kony.flex.DP);
    flxUnderlinePassword.add();
    var lblPassword = new kony.ui.Label({
        "id": "lblPassword",
        "isVisible": false,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.cardless.PasswordField"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPasswordNote = new kony.ui.Label({
        "id": "lblPasswordNote",
        "isVisible": true,
        "left": "2%",
        "skin": "latoRegular24px",
        "text": kony.i18n.getLocalizedString("i18n.cardless.PasswordFieldNote"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "85%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPassword.add(lblPasswordNote2, tbxPassword, flxUnderlinePassword, lblPassword, lblPasswordNote);
    var flxCondition1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "5%",
        "id": "flxCondition1",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxCondition1.setDefaultUnit(kony.flex.DP);
    var lblConditionCheck1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblConditionCheck1",
        "isVisible": true,
        "left": "0%",
        "skin": "lblTick",
        "text": "r",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCondition1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblCondition1",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCondition1.add(lblConditionCheck1, lblCondition1);
    var flxTermsandConditionCheck = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "5%",
        "id": "flxTermsandConditionCheck",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "skin": "slFbox",
        "top": "2%",
        "width": "92%",
        "zIndex": 1
    }, {}, {});
    flxTermsandConditionCheck.setDefaultUnit(kony.flex.DP);
    var lblTermsandConditionsCheckBox = new kony.ui.Label({
        "height": "100%",
        "id": "lblTermsandConditionsCheckBox",
        "isVisible": true,
        "left": "0%",
        "onTouchEnd": AS_Label_c46cec92cc1b4fe0a557d19f169c1a74,
        "skin": "sknLblRemCheckBox",
        "text": "q",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "8%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxTncBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxTncBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "8%",
        "onClick": AS_FlexContainer_i117a263066c4e1199dd6865ed037b78,
        "skin": "slFbox",
        "top": "2dp",
        "width": "86%",
        "zIndex": 1
    }, {}, {});
    flxTncBody.setDefaultUnit(kony.flex.DP);
    var richtxtTermsandConditions = new kony.ui.RichText({
        "id": "richtxtTermsandConditions",
        "isVisible": true,
        "left": "2%",
        "skin": "sknRichtextTnC",
        "text": kony.i18n.getLocalizedString("i18n.Cardless.TNC"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTncBody.add(richtxtTermsandConditions);
    flxTermsandConditionCheck.add(lblTermsandConditionsCheckBox, flxTncBody);
    flxBody.add(flxAccount, flxAmountDetail, flxAmounts, flxPassword, flxCondition1, flxTermsandConditionCheck);
    var flxConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "91%",
        "id": "flxConfirm",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirm.setDefaultUnit(kony.flex.DP);
    var flxConfirmAccountNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmAccountNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxConfirmAccountNumber.setDefaultUnit(kony.flex.DP);
    var lblAccountNumberTitle = new kony.ui.Label({
        "id": "lblAccountNumberTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmAccountNumber.add(lblAccountNumberTitle, lblAccountNumber);
    var flxConfirmAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxConfirmAmount.setDefaultUnit(kony.flex.DP);
    var lblConfirmAmountTitle = new kony.ui.Label({
        "id": "lblConfirmAmountTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmAmount = new kony.ui.Label({
        "id": "lblConfirmAmount",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurrencyCodeConfirm = new kony.ui.Label({
        "id": "lblCurrencyCodeConfirm",
        "isVisible": true,
        "left": "88%",
        "skin": "lblAccountStaticText",
        "text": "JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "42%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmAmount.add(lblConfirmAmountTitle, lblConfirmAmount, lblCurrencyCodeConfirm);
    var flxConfirmPass = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmPass",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxConfirmPass.setDefaultUnit(kony.flex.DP);
    var lblConfirmPassTitle = new kony.ui.Label({
        "id": "lblConfirmPassTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.login.password"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmPass = new kony.ui.Label({
        "id": "lblConfirmPass",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmPass.add(lblConfirmPassTitle, lblConfirmPass);
    var btnConfirm = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "50dp",
        "id": "btnConfirm",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_d8a715ae17274d2fb579bee99135ec8f,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
        "top": "20%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxConfirm.add(flxConfirmAccountNumber, flxConfirmAmount, flxConfirmPass, btnConfirm);
    var BrowserTCCardless = new kony.ui.Browser({
        "detectTelNumber": true,
        "enableZoom": false,
        "height": "91%",
        "id": "BrowserTCCardless",
        "isVisible": false,
        "left": "0dp",
        "requestURLConfig": {
            "URL": "",
            "requestMethod": constants.BROWSER_REQUEST_METHOD_POST
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.add(flxHeader, flxBody, flxConfirm, BrowserTCCardless);
    frmCardlessTransaction.add(flxMainContainer);
};

function frmCardlessTransactionGlobals() {
    frmCardlessTransaction = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCardlessTransaction,
        "enabledForIdleTimeout": true,
        "id": "frmCardlessTransaction",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "postShow": AS_Form_hc6edfb735aa452885967b5931319343,
        "skin": "slFormCommon"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_df62ef33d09644ffb89d27ae389f1ab4,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};