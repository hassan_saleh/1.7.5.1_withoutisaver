function initializesegmentWithDoubleLabel() {
    Copycontainer018037268d2ef46 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "Copycontainer018037268d2ef46",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    Copycontainer018037268d2ef46.setDefaultUnit(kony.flex.DP);
    var TopText = new kony.ui.Label({
        "centerY": "30%",
        "id": "TopText",
        "isVisible": true,
        "left": "5%",
        "skin": "skn",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var BottomText = new kony.ui.Label({
        "centerY": "70%",
        "id": "BottomText",
        "isVisible": true,
        "left": "5%",
        "skin": "sknSegSecondLabel",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var RightText = new kony.ui.Label({
        "centerY": "48.72%",
        "id": "RightText",
        "isVisible": true,
        "right": "35dp",
        "skin": "sknSegSecondLabel",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var InfoImg = new kony.ui.Image2({
        "centerY": "50%",
        "height": "20dp",
        "id": "InfoImg",
        "isVisible": true,
        "right": "4%",
        "skin": "sknslImage",
        "src": "right_chevron_icon.png",
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var hidden1 = new kony.ui.Label({
        "id": "hidden1",
        "isVisible": false,
        "left": "116dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "9dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    Copycontainer018037268d2ef46.add(TopText, BottomText, RightText, InfoImg, hidden1);
}