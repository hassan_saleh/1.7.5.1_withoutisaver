function initializetmpTwoLabelsWithHyphen() {
    flxWrapperKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxWrapperKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "skncontainerFAFAFAKA"
    }, {}, {});
    flxWrapperKA.setDefaultUnit(kony.flex.DP);
    var flxInnerRound = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "focusSkin": "sknFocusRoundedFlexf0f0f0KA",
        "height": "80.20%",
        "id": "flxInnerRound",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "skin": "sknRoundedFlexNormalfafafaKA",
        "top": "7dp",
        "zIndex": 1
    }, {}, {});
    flxInnerRound.setDefaultUnit(kony.flex.DP);
    var flxStrtKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxStrtKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "35%",
        "zIndex": 1
    }, {}, {});
    flxStrtKA.setDefaultUnit(kony.flex.DP);
    var lblStrtKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblStrtKA",
        "isVisible": true,
        "left": "30%",
        "skin": "sknLatoNUOccccccKA",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblExperience = new kony.ui.Label({
        "centerY": "60%",
        "id": "lblExperience",
        "isVisible": false,
        "left": "40%",
        "skin": "sknLatoNUOccccccKA",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStrtKA.add(lblStrtKA, lblExperience);
    var lblhyphenKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblhyphenKA",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLatoNUOccccccKA",
        "text": "-",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxEndKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxEndKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "35%",
        "zIndex": 1
    }, {}, {});
    flxEndKA.setDefaultUnit(kony.flex.DP);
    var lblEndKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblEndKA",
        "isVisible": true,
        "left": "10%",
        "right": "80%",
        "skin": "sknLatoNUOccccccKA",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "85%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEndKA.add(lblEndKA);
    flxInnerRound.add(flxStrtKA, lblhyphenKA, flxEndKA);
    flxWrapperKA.add(flxInnerRound);
}