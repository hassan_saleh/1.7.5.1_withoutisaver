function initializesegMessagesBoldTmplDraftKA() {
    flxSegMsgKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxSegMsgKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    flxSegMsgKA.setDefaultUnit(kony.flex.DP);
    var flxSegMsgSwipe = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxSegMsgSwipe",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "168%",
        "zIndex": 1
    }, {}, {});
    flxSegMsgSwipe.setDefaultUnit(kony.flex.DP);
    var flxmainkA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxmainkA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "59.50%",
        "zIndex": 1
    }, {}, {});
    flxmainkA.setDefaultUnit(kony.flex.DP);
    var lblTitleKA = new kony.ui.Label({
        "id": "lblTitleKA",
        "isVisible": true,
        "left": "5%",
        "maxNumberOfLines": 1,
        "skin": "sknMessageTitleBoldKA",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "2%",
        "width": "90%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxDescChevronContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxDescChevronContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0",
        "skin": "slFbox",
        "top": "25dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDescChevronContainer.setDefaultUnit(kony.flex.DP);
    var lblDescKA = new kony.ui.Label({
        "id": "lblDescKA",
        "isVisible": true,
        "left": "5.00%",
        "maxNumberOfLines": 1,
        "skin": "CopyslLabel031d27909a26c4a",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0dp",
        "width": "80%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDescChevronContainer.add(lblDescKA);
    var lblTimestampKA = new kony.ui.Label({
        "id": "lblTimestampKA",
        "isVisible": true,
        "left": "5.00%",
        "skin": "sknasOfTimeLabelBlack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "60dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMessageIdKA = new kony.ui.Label({
        "id": "lblMessageIdKA",
        "isVisible": false,
        "left": "204dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMessageStatusKA = new kony.ui.Label({
        "id": "lblMessageStatusKA",
        "isVisible": false,
        "left": "0dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxmainkA.add(lblTitleKA, flxDescChevronContainer, lblTimestampKA, lblMessageIdKA, lblMessageStatusKA);
    var btnDeleteKA = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknbtnDeleteKA",
        "height": "100dp",
        "id": "btnDeleteKA",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_e56b8a52ad1940a8b397872a6f9d88a1,
        "skin": "sknbtnDeleteKA",
        "text": "Delete",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSegMsgSwipe.add(flxmainkA, btnDeleteKA);
    flxSegMsgKA.add(flxSegMsgSwipe);
}