function initializetmpSegLoginMethods() {
    flxtmpMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxtmpMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxtmpMain.setDefaultUnit(kony.flex.DP);
    var imgIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "30dp",
        "id": "imgIcon",
        "isVisible": true,
        "left": "5%",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "0dp",
        "width": "30dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLoginMethod = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLoginMethod",
        "isVisible": true,
        "left": "25%",
        "skin": "sknTmplbl",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var tmpAuthMode = new kony.ui.Label({
        "centerY": "60%",
        "id": "tmpAuthMode",
        "isVisible": false,
        "left": "35%",
        "skin": "sknonboardingHeaderLatoBold",
        "text": "password",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgEnable = new kony.ui.Image2({
        "centerY": "50%",
        "height": "20dp",
        "id": "imgEnable",
        "isVisible": true,
        "right": "2%",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "0dp",
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxDivider1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxDivider1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDivider1.setDefaultUnit(kony.flex.DP);
    flxDivider1.add();
    flxtmpMain.add(imgIcon, lblLoginMethod, tmpAuthMode, imgEnable, flxDivider1);
}