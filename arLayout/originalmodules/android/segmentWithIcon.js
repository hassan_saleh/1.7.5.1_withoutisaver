function initializesegmentWithIcon() {
    CopyFlexContainer0b2b1c26ffbf74f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyFlexContainer0b2b1c26ffbf74f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    CopyFlexContainer0b2b1c26ffbf74f.setDefaultUnit(kony.flex.DP);
    var lblNameKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNameKA",
        "isVisible": true,
        "left": "5%",
        "skin": "skn",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "top": "49dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    var imgicontick = new kony.ui.Image2({
        "centerY": "50%",
        "height": "20dp",
        "id": "imgicontick",
        "isVisible": true,
        "right": "5%",
        "skin": "sknslImage",
        "src": "imagedrag.png",
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer0b2b1c26ffbf74f.add(lblNameKA, contactListDivider, imgicontick);
}