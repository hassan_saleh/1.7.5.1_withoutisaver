function addWidgetsfrmConfirmNPPRegistration() {
    frmConfirmNPPRegistration.setDefaultUnit(kony.flex.DP);
    var titleBarWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarWrapper.setDefaultUnit(kony.flex.DP);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var androidTitleLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "androidTitleLabel",
        "isVisible": true,
        "left": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.NPP.confirmReg"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    androidTitleBar.add(androidTitleLabel);
    var backButton = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "skntitleBarTextButtonFocus",
        "height": "50dp",
        "id": "backButton",
        "isVisible": true,
        "left": "0dp",
        "minWidth": "50dp",
        "onClick": AS_Button_f2c68f7ae8dd4752a0b1470f16be04b8,
        "skin": "skntitleBarTextButton",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.cancel"),
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    titleBarWrapper.add(androidTitleBar, backButton);
    var flexMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexMainContainer.setDefaultUnit(kony.flex.DP);
    var flxPhnDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "65dp",
        "id": "flxPhnDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxBackGroundWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPhnDetails.setDefaultUnit(kony.flex.DP);
    var phnLabel = new kony.ui.Label({
        "id": "phnLabel",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNonEditableFields",
        "text": kony.i18n.getLocalizedString("i18n.NPP.Phone"),
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPhnValue = new kony.ui.Label({
        "id": "lblPhnValue",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "text": "1234567890",
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var divider2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    divider2.setDefaultUnit(kony.flex.DP);
    divider2.add();
    flxPhnDetails.add(phnLabel, lblPhnValue, divider2);
    var flxDisplayName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "65dp",
        "id": "flxDisplayName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxBackGroundWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDisplayName.setDefaultUnit(kony.flex.DP);
    var lblDisplayName = new kony.ui.Label({
        "id": "lblDisplayName",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNonEditableFields",
        "text": kony.i18n.getLocalizedString("i18n.NPP.displayName"),
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDisplayNameValue = new kony.ui.Label({
        "id": "lblDisplayNameValue",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "text": "john.williams",
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Copydivider0a730d460c20541 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0a730d460c20541",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    Copydivider0a730d460c20541.setDefaultUnit(kony.flex.DP);
    Copydivider0a730d460c20541.add();
    flxDisplayName.add(lblDisplayName, lblDisplayNameValue, Copydivider0a730d460c20541);
    var flxAccDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "flxAccDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxBackGroundWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccDetails.setDefaultUnit(kony.flex.DP);
    var lblAccount = new kony.ui.Label({
        "id": "lblAccount",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNonEditableFields",
        "text": kony.i18n.getLocalizedString("i18n.NPP.account"),
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountType = new kony.ui.Label({
        "id": "lblAccountType",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.NPP.typeOfAccount"),
        "top": "45%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBSBNumberValue = new kony.ui.Label({
        "id": "lblBSBNumberValue",
        "isVisible": true,
        "right": "5%",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.NPP.accNum"),
        "top": "45%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBankName = new kony.ui.Label({
        "id": "lblBankName",
        "isVisible": true,
        "left": "5%",
        "right": "-5%",
        "skin": "sknLatoLight3d3d3d",
        "text": kony.i18n.getLocalizedString("i18n.NPP.bankName"),
        "top": "70%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBSBNumber = new kony.ui.Label({
        "id": "lblBSBNumber",
        "isVisible": true,
        "right": "5%",
        "skin": "sknLatoLight3d3d3d",
        "text": kony.i18n.getLocalizedString("i18n.p2p.BSBNumber"),
        "top": "70%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAccDetails.add(lblAccount, lblAccountType, lblBSBNumberValue, lblBankName, lblBSBNumber);
    var btnConfirm = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "btnConfirm",
        "isVisible": true,
        "onClick": AS_Button_d5275fc9a85e4466be88ee2c397aaabc,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.confirm"),
        "top": "37%",
        "width": "80%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnEditTranscation = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "35dp",
        "id": "btnEditTranscation",
        "isVisible": true,
        "onClick": AS_Button_e168e7b6f9aa4408b88acbfcd0cdf3ec,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.NPP.edit"),
        "top": "5dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexMainContainer.add(flxPhnDetails, flxDisplayName, flxAccDetails, btnConfirm, btnEditTranscation);
    frmConfirmNPPRegistration.add(titleBarWrapper, flexMainContainer);
};

function frmConfirmNPPRegistrationGlobals() {
    frmConfirmNPPRegistration = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmConfirmNPPRegistration,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmConfirmNPPRegistration",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "preShow": AS_Form_h551125c3d6040b69abd8e8587ad8a67,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_e43bbee145e041cbaa5d5cd8870de70f,
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};