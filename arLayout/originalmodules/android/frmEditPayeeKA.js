function addWidgetsfrmEditPayeeKA() {
    frmEditPayeeKA.setDefaultUnit(kony.flex.DP);
    var mainScrollContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "mainScrollContainer",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkgWhite",
        "top": "50dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainScrollContainer.setDefaultUnit(kony.flex.DP);
    var payeeNameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "payeeNameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    payeeNameContainer.setDefaultUnit(kony.flex.DP);
    var CopyLabel0b353ede7efad4c = new kony.ui.Label({
        "height": "32dp",
        "id": "CopyLabel0b353ede7efad4c",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNonEditableFields",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.companyName"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyusernameContainer0c123209f151c45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "CopyusernameContainer0c123209f151c45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "right": "5%",
        "skin": "sknslFbox",
        "top": "35dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer0c123209f151c45.setDefaultUnit(kony.flex.DP);
    var newPayeeNameTextfield = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_WORDS,
        "focusSkin": "skngeneralTextFieldFocus",
        "height": 38,
        "id": "newPayeeNameTextfield",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "placeholder": "Enter Company name here",
        "right": 0,
        "secureTextEntry": false,
        "skin": "sknTextBoxNonEditable",
        "text": "Power Corporation Ltd.",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxDivider1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "2dp",
        "id": "flxDivider1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDivider1.setDefaultUnit(kony.flex.DP);
    flxDivider1.add();
    CopyusernameContainer0c123209f151c45.add(newPayeeNameTextfield, flxDivider1);
    payeeNameContainer.add(CopyLabel0b353ede7efad4c, CopyusernameContainer0c123209f151c45);
    var flxPayeeNickNameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "flxPayeeNickNameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPayeeNickNameContainer.setDefaultUnit(kony.flex.DP);
    var CopyLabel0927085edf80e42 = new kony.ui.Label({
        "height": "32dp",
        "id": "CopyLabel0927085edf80e42",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsectionHeaderLabel",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeNickName"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyusernameContainer0b764e42673504b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "CopyusernameContainer0b764e42673504b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "right": "5%",
        "skin": "sknslFbox",
        "top": "35dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer0b764e42673504b.setDefaultUnit(kony.flex.DP);
    var CopynewPayeeNameTextfield07a3d8dc6f56e4f = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_WORDS,
        "focusSkin": "skngeneralTextFieldFocus",
        "height": 38,
        "id": "CopynewPayeeNameTextfield07a3d8dc6f56e4f",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "placeholder": "Enter Payee Nick Name here",
        "right": 0,
        "secureTextEntry": false,
        "skin": "skngeneralTextField",
        "text": "My shop's power bill",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyflxDivider0b294261c43344c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyflxDivider0b294261c43344c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxDivider0b294261c43344c.setDefaultUnit(kony.flex.DP);
    CopyflxDivider0b294261c43344c.add();
    CopyusernameContainer0b764e42673504b.add(CopynewPayeeNameTextfield07a3d8dc6f56e4f, CopyflxDivider0b294261c43344c);
    flxPayeeNickNameContainer.add(CopyLabel0927085edf80e42, CopyusernameContainer0b764e42673504b);
    var payeeAccountContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "82dp",
        "id": "payeeAccountContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    payeeAccountContainer.setDefaultUnit(kony.flex.DP);
    var CopyLabel0200c64cb34cb44 = new kony.ui.Label({
        "height": "32dp",
        "id": "CopyLabel0200c64cb34cb44",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNonEditableFields",
        "text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyusernameContainer09871c009dbc247 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "54dp",
        "id": "CopyusernameContainer09871c009dbc247",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "right": "5%",
        "skin": "sknslFbox",
        "top": "35dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer09871c009dbc247.setDefaultUnit(kony.flex.DP);
    var newPayeeAccountNumberTextField = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "skngeneralTextFieldFocus",
        "height": 38,
        "id": "newPayeeAccountNumberTextField",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "0dp",
        "placeholder": "Enter Account Number here",
        "right": 0,
        "secureTextEntry": false,
        "skin": "sknTextBoxNonEditable",
        "text": "123499484321",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyflxDivider07de2109b1c0a4d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "2dp",
        "id": "CopyflxDivider07de2109b1c0a4d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxDivider07de2109b1c0a4d.setDefaultUnit(kony.flex.DP);
    CopyflxDivider07de2109b1c0a4d.add();
    CopyusernameContainer09871c009dbc247.add(newPayeeAccountNumberTextField, CopyflxDivider07de2109b1c0a4d);
    payeeAccountContainer.add(CopyLabel0200c64cb34cb44, CopyusernameContainer09871c009dbc247);
    var payeeAddressContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "payeeAddressContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    payeeAddressContainer.setDefaultUnit(kony.flex.DP);
    var CopyLabel0b1ac08ce375a4c = new kony.ui.Label({
        "height": "32dp",
        "id": "CopyLabel0b1ac08ce375a4c",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsectionHeaderLabel",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeAddressC"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyusernameContainer012a8b92d34cd4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "36dp",
        "id": "CopyusernameContainer012a8b92d34cd4b",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "right": "5%",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer012a8b92d34cd4b.setDefaultUnit(kony.flex.DP);
    var CopyusernameTextField0f7d7f563446f42 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "skngeneralTextFieldFocus",
        "height": 32,
        "id": "CopyusernameTextField0f7d7f563446f42",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "placeholder": "Street",
        "right": 0,
        "secureTextEntry": false,
        "skin": "skngeneralTextField",
        "text": "Power street",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyFlexContainer09fbd523e9a6c43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer09fbd523e9a6c43",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer09fbd523e9a6c43.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer09fbd523e9a6c43.add();
    CopyusernameContainer012a8b92d34cd4b.add(CopyusernameTextField0f7d7f563446f42, CopyFlexContainer09fbd523e9a6c43);
    var CopyusernameContainer0eea4f72ba00743 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "38dp",
        "id": "CopyusernameContainer0eea4f72ba00743",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "right": "5%",
        "skin": "sknslFbox",
        "top": "6dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer0eea4f72ba00743.setDefaultUnit(kony.flex.DP);
    var CopyusernameTextField08388e41c88cd4a = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "skngeneralTextFieldFocus",
        "height": 38,
        "id": "CopyusernameTextField08388e41c88cd4a",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "placeholder": "Apt, P.O. Box",
        "right": 0,
        "secureTextEntry": false,
        "skin": "skngeneralTextField",
        "text": "P.O.B 54424",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var FlexContainer0da5c3167759a45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "FlexContainer0da5c3167759a45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0da5c3167759a45.setDefaultUnit(kony.flex.DP);
    FlexContainer0da5c3167759a45.add();
    CopyusernameContainer0eea4f72ba00743.add(CopyusernameTextField08388e41c88cd4a, FlexContainer0da5c3167759a45);
    var CopyusernameContainer0740d926171f149 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "38dp",
        "id": "CopyusernameContainer0740d926171f149",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "right": "5%",
        "skin": "sknslFbox",
        "top": "6dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer0740d926171f149.setDefaultUnit(kony.flex.DP);
    var CopyusernameTextField01fe47525d95442 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "skngeneralTextFieldFocus",
        "height": 38,
        "id": "CopyusernameTextField01fe47525d95442",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "placeholder": "City",
        "right": 0,
        "secureTextEntry": false,
        "skin": "skngeneralTextField",
        "text": "New York",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyFlexContainer0f3582dde3eac40 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0f3582dde3eac40",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0f3582dde3eac40.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0f3582dde3eac40.add();
    CopyusernameContainer0740d926171f149.add(CopyusernameTextField01fe47525d95442, CopyFlexContainer0f3582dde3eac40);
    var CopyusernameContainer083753b47661045 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "38dp",
        "id": "CopyusernameContainer083753b47661045",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "right": "5%",
        "skin": "sknslFbox",
        "top": "6dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer083753b47661045.setDefaultUnit(kony.flex.DP);
    var CopyusernameTextField0411be02f9f4548 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "skngeneralTextFieldFocus",
        "height": 38,
        "id": "CopyusernameTextField0411be02f9f4548",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "placeholder": "State",
        "right": 0,
        "secureTextEntry": false,
        "skin": "skngeneralTextField",
        "text": "New York",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyFlexContainer0e65f2830703a49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0e65f2830703a49",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0e65f2830703a49.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0e65f2830703a49.add();
    CopyusernameContainer083753b47661045.add(CopyusernameTextField0411be02f9f4548, CopyFlexContainer0e65f2830703a49);
    var CopyusernameContainer09da656c676db42 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "38dp",
        "id": "CopyusernameContainer09da656c676db42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "right": "5%",
        "skin": "sknslFbox",
        "top": "6dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyusernameContainer09da656c676db42.setDefaultUnit(kony.flex.DP);
    var CopyusernameTextField0cec050157b0f48 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "skngeneralTextFieldFocus",
        "height": 38,
        "id": "CopyusernameTextField0cec050157b0f48",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "0dp",
        "placeholder": "Zip Code",
        "right": 0,
        "secureTextEntry": false,
        "skin": "skngeneralTextField",
        "text": "500047",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyFlexContainer02077f851ad8c46 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "1dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer02077f851ad8c46",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntextFieldDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer02077f851ad8c46.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer02077f851ad8c46.add();
    CopyusernameContainer09da656c676db42.add(CopyusernameTextField0cec050157b0f48, CopyFlexContainer02077f851ad8c46);
    payeeAddressContainer.add(CopyLabel0b1ac08ce375a4c, CopyusernameContainer012a8b92d34cd4b, CopyusernameContainer0eea4f72ba00743, CopyusernameContainer0740d926171f149, CopyusernameContainer083753b47661045, CopyusernameContainer09da656c676db42);
    var buttonWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "350dp",
        "id": "buttonWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    buttonWrapper.setDefaultUnit(kony.flex.DP);
    var saveNewPayeeButton = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "saveNewPayeeButton",
        "isVisible": true,
        "onClick": AS_Button_7201621ca0b74eb781500638cffd1f74,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.cSAVE"),
        "top": "24dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnDeletePayeeKA = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "42dp",
        "id": "btnDeletePayeeKA",
        "isVisible": true,
        "onClick": AS_Button_a09ea0eef99d45bc9495aabdd528af4b,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.transfer.deletePayee"),
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    buttonWrapper.add(saveNewPayeeButton, btnDeletePayeeKA);
    mainScrollContainer.add(payeeNameContainer, flxPayeeNickNameContainer, payeeAccountContainer, payeeAddressContainer, buttonWrapper);
    var titleBarEditPayee = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarEditPayee",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarEditPayee.setDefaultUnit(kony.flex.DP);
    var lblTitleKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblTitleKA",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.editPayee"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnCancelKA = new kony.ui.Button({
        "focusSkin": "skntitleBarTextButtonFocus",
        "height": "50dp",
        "id": "btnCancelKA",
        "isVisible": true,
        "left": "0dp",
        "minWidth": "50dp",
        "skin": "skntitleBarTextButton",
        "text": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    titleBarEditPayee.add(lblTitleKA, btnCancelKA);
    var flxEditBillerContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxEditBillerContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxEditBillerContainer.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_f585b16e711d41c192208eac41647afd,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var CopylblHeaderBackIcon0e57343f2e2274c = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblHeaderBackIcon0e57343f2e2274c",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblHeaderBack0j7f57eb046594b = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblHeaderBack0j7f57eb046594b",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(CopylblHeaderBackIcon0e57343f2e2274c, CopylblHeaderBack0j7f57eb046594b);
    var CopylblHeaderTitle0de254f31c2b44e = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "CopylblHeaderTitle0de254f31c2b44e",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.billspay.editnickname"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblClose = new kony.ui.Label({
        "centerX": "94%",
        "height": "100%",
        "id": "lblClose",
        "isVisible": true,
        "onTouchEnd": AS_Label_d888c826e4a14fc180df1fcb18d1b18b,
        "skin": "sknCloseConfirm",
        "text": "O",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(flxBack, CopylblHeaderTitle0de254f31c2b44e, lblClose);
    var flxEditBillerBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxEditBillerBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxEditBillerBody.setDefaultUnit(kony.flex.DP);
    var flxImpDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "25%",
        "id": "flxImpDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxImpDetails.setDefaultUnit(kony.flex.DP);
    var CopyflxIcon0d4c897823fde4f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyflxIcon0d4c897823fde4f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxToIcon",
        "top": "20%",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    CopyflxIcon0d4c897823fde4f.setDefaultUnit(kony.flex.DP);
    var initialsCategory = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "initialsCategory",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "text": "AI",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxIcon0d4c897823fde4f.add(initialsCategory);
    var lblBillingName = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBillingName",
        "isVisible": true,
        "skin": "sknBeneTitle",
        "text": "Airtel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "3%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblBeneAccNum0d0906faa372149 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopylblBeneAccNum0d0906faa372149",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblNextDisabled",
        "text": "9845654568",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxImpDetails.add(CopyflxIcon0d4c897823fde4f, lblBillingName, CopylblBeneAccNum0d0906faa372149);
    var flxBillN = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxBillN",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBillN.setDefaultUnit(kony.flex.DP);
    var CopylblConfirmBNTitle0b7f4f5e29e9346 = new kony.ui.Label({
        "height": "50%",
        "id": "CopylblConfirmBNTitle0b7f4f5e29e9346",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtBillerName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknTxtBox",
        "height": "50%",
        "id": "txtBillerName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "8%",
        "maxTextLength": 35,
        "onDone": AS_TextField_e3244cdda11440d58b74d7110a589278,
        "onTextChange": AS_TextField_jd1d867de52a48858d5d062c92c79e48,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblUnderline = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderline",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "8%",
        "skin": "sknFlxGreyLine",
        "top": "-5%",
        "width": "84%",
        "zIndex": 1
    }, {}, {});
    lblUnderline.setDefaultUnit(kony.flex.DP);
    lblUnderline.add();
    flxBillN.add(CopylblConfirmBNTitle0b7f4f5e29e9346, txtBillerName, lblUnderline);
    var flxServiceType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxServiceType",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxServiceType.setDefaultUnit(kony.flex.DP);
    var CopylblConfirmBankNameTitle0gb348fcf67e64f = new kony.ui.Label({
        "height": "50%",
        "id": "CopylblConfirmBankNameTitle0gb348fcf67e64f",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblServiceType = new kony.ui.Label({
        "height": "50%",
        "id": "lblServiceType",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBillerCode = new kony.ui.Label({
        "id": "lblBillerCode",
        "isVisible": false,
        "left": "269dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxServiceType.add(CopylblConfirmBankNameTitle0gb348fcf67e64f, lblServiceType, lblBillerCode);
    var flxDenomination = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxDenomination",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDenomination.setDefaultUnit(kony.flex.DP);
    var lblDenominationTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblDenominationTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.prepaid.denomination"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDenominationDESC = new kony.ui.Label({
        "height": "50%",
        "id": "lblDenominationDESC",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDenomination = new kony.ui.Label({
        "id": "lblDenomination",
        "isVisible": false,
        "left": "269dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDenomination.add(lblDenominationTitle, lblDenominationDESC, lblDenomination);
    var flxBillerNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxBillerNumber",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBillerNumber.setDefaultUnit(kony.flex.DP);
    var CopylblConfirmCountryTitle0a095687a3dab49 = new kony.ui.Label({
        "height": "50%",
        "id": "CopylblConfirmCountryTitle0a095687a3dab49",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.BillerNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBillerNumber = new kony.ui.Label({
        "height": "50%",
        "id": "lblBillerNumber",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBillerNumber.add(CopylblConfirmCountryTitle0a095687a3dab49, lblBillerNumber);
    var btnEditBillerConfirm = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "25%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "10%",
        "id": "btnEditBillerConfirm",
        "isVisible": true,
        "onClick": AS_Button_b99278acb55d4137b6830bc9440739c3,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxEditBillerBody.add(flxImpDetails, flxBillN, flxServiceType, flxDenomination, flxBillerNumber, btnEditBillerConfirm);
    flxEditBillerContainer.add(flxHeader, flxEditBillerBody);
    frmEditPayeeKA.add(mainScrollContainer, titleBarEditPayee, flxEditBillerContainer);
};

function frmEditPayeeKAGlobals() {
    frmEditPayeeKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmEditPayeeKA,
        "enabledForIdleTimeout": true,
        "id": "frmEditPayeeKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};