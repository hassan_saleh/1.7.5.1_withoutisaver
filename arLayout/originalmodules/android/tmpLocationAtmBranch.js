function initializetmpLocationAtmBranch() {
    flxLocationDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "slFbox",
        "height": "15%",
        "id": "flxLocationDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxLocationDetails.setDefaultUnit(kony.flex.DP);
    var btnAlpha = new kony.ui.Button({
        "centerY": "50%",
        "height": "40dp",
        "id": "btnAlpha",
        "isVisible": true,
        "left": "3%",
        "skin": "CopyslButtonGlossBlue0gc9fc8149f6943",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLocationName = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLocationName",
        "isVisible": true,
        "left": "20%",
        "skin": "CopyslLabel0e2302468ce1241",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRightChevron = new kony.ui.Label({
        "centerY": "50%",
        "height": "65%",
        "id": "lblRightChevron",
        "isVisible": true,
        "right": "5%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLocationDetails.add(btnAlpha, lblLocationName, lblRightChevron);
}