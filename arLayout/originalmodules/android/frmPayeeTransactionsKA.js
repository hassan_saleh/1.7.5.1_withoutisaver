function addWidgetsfrmPayeeTransactionsKA() {
    frmPayeeTransactionsKA.setDefaultUnit(kony.flex.DP);
    var flxTopWrapperKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxTopWrapperKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopWrapperKA.setDefaultUnit(kony.flex.DP);
    var lblTitleKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblTitleKA",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeDetails"),
        "top": "15dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnCancelKA = new kony.ui.Button({
        "focusSkin": "skntitleBarTextButtonFocus",
        "height": "50dp",
        "id": "btnCancelKA",
        "isVisible": true,
        "minWidth": "50dp",
        "onClick": AS_Button_d16232ef24b340f5b2a1458e12cc15db,
        "right": 0,
        "skin": "skntitleBarTextButton",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.details"),
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxAndroidTitleBarKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxAndroidTitleBarKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAndroidTitleBarKA.setDefaultUnit(kony.flex.DP);
    var lblAndroidTitleLabelKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAndroidTitleLabelKA",
        "isVisible": false,
        "left": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeNickName"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var androidBack = new kony.ui.Button({
        "focusSkin": "sknandroidBackButtonFocus",
        "height": "50dp",
        "id": "androidBack",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS__01402cb350b74188b4770eff708ef64b,
        "skin": "sknandroidBackButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxAndroidTitleBarKA.add(lblAndroidTitleLabelKA, androidBack);
    var flxDetailsWrapperKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "70%",
        "id": "flxDetailsWrapperKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "skin": "sknslFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDetailsWrapperKA.setDefaultUnit(kony.flex.DP);
    var lblPayeeNameKA = new kony.ui.Label({
        "centerX": "50.00%",
        "id": "lblPayeeNameKA",
        "isVisible": true,
        "skin": "sknlblwhiteLatoRegular136KA",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeOrCompanyName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCardNumberKA = new kony.ui.Label({
        "centerX": "50.00%",
        "id": "lblCardNumberKA",
        "isVisible": true,
        "skin": "sknlblCardNumberKA",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.frmPayeeTransactionsKA.lblCardNumberKA"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxWhiteDividerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2dp",
        "id": "flxWhiteDividerKA",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "107dp",
        "skin": "sknWhiteDivider100",
        "top": "10dp",
        "width": "85%",
        "zIndex": 1
    }, {}, {});
    flxWhiteDividerKA.setDefaultUnit(kony.flex.DP);
    flxWhiteDividerKA.add();
    var btnPayBillKA = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknsecondaryActionReversed",
        "height": "40dp",
        "id": "btnPayBillKA",
        "isVisible": true,
        "onClick": AS_Button_dd5d5f0bf99c4d13be15d7aff6a28c94,
        "skin": "sknsecondaryActionReversed",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.btnPayBill"),
        "top": "10dp",
        "width": "240dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxDetailsWrapperKA.add(lblPayeeNameKA, lblCardNumberKA, flxWhiteDividerKA, btnPayBillKA);
    flxTopWrapperKA.add(lblTitleKA, btnCancelKA, flxAndroidTitleBarKA, flxDetailsWrapperKA);
    var flxBottomContainerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBottomContainerKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkg",
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomContainerKA.setDefaultUnit(kony.flex.DP);
    var flxTransactionListsContainerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "88%",
        "id": "flxTransactionListsContainerKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "12%",
        "width": "300%",
        "zIndex": 1
    }, {}, {});
    flxTransactionListsContainerKA.setDefaultUnit(kony.flex.DP);
    var flxCompleted = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCompleted",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "33.33%"
    }, {}, {});
    flxCompleted.setDefaultUnit(kony.flex.DP);
    var segCompletedPaymentsKA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "chevron": "right_chevron_icon.png",
            "lblSepKA": "Label",
            "transactionAmount": "$155.00",
            "transactionDate": "Oct 10, 2015 - Recurring Monthly",
            "transactionName": "Payment to City of Austin"
        }, {
            "chevron": "right_chevron_icon.png",
            "lblSepKA": "Label",
            "transactionAmount": "$155.00",
            "transactionDate": "Oct 10, 2015 - Recurring Monthly",
            "transactionName": "Payment to City of Austin"
        }, {
            "chevron": "right_chevron_icon.png",
            "lblSepKA": "Label",
            "transactionAmount": "$155.00",
            "transactionDate": "Oct 10, 2015 - Recurring Monthly",
            "transactionName": "Payment to City of Austin"
        }],
        "groupCells": false,
        "height": "100%",
        "id": "segCompletedPaymentsKA",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_d150a2d613cf4a7db389ae4bca2129a7,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowSkin": "seg2Normal",
        "rowTemplate": CopyFlexContainer0c9f1eddbc7f547,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff00",
        "separatorRequired": true,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "AccountTypeKA": "AccountTypeKA",
            "CopyFlexContainer0c9f1eddbc7f547": "CopyFlexContainer0c9f1eddbc7f547",
            "chevron": "chevron",
            "lblSepKA": "lblSepKA",
            "transactionAmount": "transactionAmount",
            "transactionDate": "transactionDate",
            "transactionName": "transactionName"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var LabelNoCompletedTransactionsKA = new kony.ui.Label({
        "centerY": "35.519999999999996%",
        "id": "LabelNoCompletedTransactionsKA",
        "isVisible": true,
        "left": "12%",
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.alerts.NoCompletedTransactions"),
        "top": 30,
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCompleted.add(segCompletedPaymentsKA, LabelNoCompletedTransactionsKA);
    var flxScheduled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxScheduled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "33.33%",
        "zIndex": 1
    }, {}, {});
    flxScheduled.setDefaultUnit(kony.flex.DP);
    var segScheduledTransactionsKA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "chevron": "right_chevron_icon.png",
            "lblSepKA": "Label",
            "transactionAmount": "$155.00",
            "transactionDate": "Oct 10, 2015 - Recurring Monthly",
            "transactionName": "Payment to City of Austin"
        }, {
            "chevron": "right_chevron_icon.png",
            "lblSepKA": "Label",
            "transactionAmount": "$155.00",
            "transactionDate": "Oct 10, 2015 - Recurring Monthly",
            "transactionName": "Payment to City of Austin"
        }, {
            "chevron": "right_chevron_icon.png",
            "lblSepKA": "Label",
            "transactionAmount": "$155.00",
            "transactionDate": "Oct 10, 2015 - Recurring Monthly",
            "transactionName": "Payment to City of Austin"
        }],
        "groupCells": false,
        "height": "100%",
        "id": "segScheduledTransactionsKA",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_4d6c16e7dd634a05bd8c0bf76bb49c33,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": CopyFlexContainer0c9f1eddbc7f547,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff00",
        "separatorRequired": true,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "AccountTypeKA": "AccountTypeKA",
            "CopyFlexContainer0c9f1eddbc7f547": "CopyFlexContainer0c9f1eddbc7f547",
            "chevron": "chevron",
            "lblSepKA": "lblSepKA",
            "transactionAmount": "transactionAmount",
            "transactionDate": "transactionDate",
            "transactionName": "transactionName"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var LabelNoScheduledTransactionsKA = new kony.ui.Label({
        "centerY": "35.52%",
        "id": "LabelNoScheduledTransactionsKA",
        "isVisible": true,
        "left": "12%",
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.alerts.NoScheduledTransactions"),
        "top": 30,
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxScheduled.add(segScheduledTransactionsKA, LabelNoScheduledTransactionsKA);
    var flxFailed = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxFailed",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "33.33%",
        "zIndex": 1
    }, {}, {});
    flxFailed.setDefaultUnit(kony.flex.DP);
    var segFailedTransactionsKA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "chevron": "right_chevron_icon.png",
            "lblSepKA": "Label",
            "transactionAmount": "$155.00",
            "transactionDate": "Oct 10, 2015 - Recurring Monthly",
            "transactionName": "Payment to City of Austin"
        }, {
            "chevron": "right_chevron_icon.png",
            "lblSepKA": "Label",
            "transactionAmount": "$155.00",
            "transactionDate": "Oct 10, 2015 - Recurring Monthly",
            "transactionName": "Payment to City of Austin"
        }, {
            "chevron": "right_chevron_icon.png",
            "lblSepKA": "Label",
            "transactionAmount": "$155.00",
            "transactionDate": "Oct 10, 2015 - Recurring Monthly",
            "transactionName": "Payment to City of Austin"
        }],
        "groupCells": false,
        "height": "100%",
        "id": "segFailedTransactionsKA",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": CopyFlexContainer0c9f1eddbc7f547,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff00",
        "separatorRequired": true,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "AccountTypeKA": "AccountTypeKA",
            "CopyFlexContainer0c9f1eddbc7f547": "CopyFlexContainer0c9f1eddbc7f547",
            "chevron": "chevron",
            "lblSepKA": "lblSepKA",
            "transactionAmount": "transactionAmount",
            "transactionDate": "transactionDate",
            "transactionName": "transactionName"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var LabelNoFailedTransactionsKA = new kony.ui.Label({
        "centerY": "35.52%",
        "id": "LabelNoFailedTransactionsKA",
        "isVisible": true,
        "left": "12%",
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.alerts.NoFailedTransactions"),
        "top": 30,
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxFailed.add(segFailedTransactionsKA, LabelNoFailedTransactionsKA);
    flxTransactionListsContainerKA.add(flxCompleted, flxScheduled, flxFailed);
    var tabsWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "12%",
        "id": "tabsWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    tabsWrapper.setDefaultUnit(kony.flex.DP);
    var btnCompletedKA = new kony.ui.Button({
        "focusSkin": "skntabSelected",
        "height": "100%",
        "id": "btnCompletedKA",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_8ed47e6bbb4f4d418932ae8329372141,
        "skin": "skntabSelected",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.btnCompleted"),
        "top": "0dp",
        "width": "33.33%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 1, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnScheduledKA = new kony.ui.Button({
        "focusSkin": "skntabSelected",
        "height": "100%",
        "id": "btnScheduledKA",
        "isVisible": true,
        "left": "33.33%",
        "onClick": AS_Button_4283e06a33e44f8181fb2dbd73d4c4ce,
        "skin": "skntabDeselected",
        "text": kony.i18n.getLocalizedString("i18n.common.cSCHEDULED"),
        "top": "0dp",
        "width": "33.33%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 1, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnFailedKA = new kony.ui.Button({
        "focusSkin": "skntabSelected",
        "height": "100%",
        "id": "btnFailedKA",
        "isVisible": true,
        "left": "66.66%",
        "onClick": AS_Button_b5296a4b03d849878f50c71ab20ff521,
        "skin": "skntabDeselected",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.btnFailed"),
        "top": "0dp",
        "width": "33.33%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 1, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxDeselectedIndicatorKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxDeselectedIndicatorKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknCopyslFbox00fe82e6f7cc84b",
        "top": "0dp",
        "width": "33.33%"
    }, {}, {});
    flxDeselectedIndicatorKA.setDefaultUnit(kony.flex.DP);
    flxDeselectedIndicatorKA.add();
    var flxDeselectedIndicator2KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxDeselectedIndicator2KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "33.33%",
        "skin": "sknCopyslFbox00fe82e6f7cc84b",
        "top": "0dp",
        "width": "33.33%"
    }, {}, {});
    flxDeselectedIndicator2KA.setDefaultUnit(kony.flex.DP);
    flxDeselectedIndicator2KA.add();
    var flxDeselectedIndicator3KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxDeselectedIndicator3KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "66.66%",
        "skin": "sknCopyslFbox00fe82e6f7cc84b",
        "top": "0dp",
        "width": "33.33%"
    }, {}, {});
    flxDeselectedIndicator3KA.setDefaultUnit(kony.flex.DP);
    flxDeselectedIndicator3KA.add();
    var flxSelectedIndicatorKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxSelectedIndicatorKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknCopyslFbox03fbc1653617542",
        "top": "0dp",
        "width": "33.33%"
    }, {}, {});
    flxSelectedIndicatorKA.setDefaultUnit(kony.flex.DP);
    flxSelectedIndicatorKA.add();
    var flxListDividerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxListDividerKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxListDividerKA.setDefaultUnit(kony.flex.DP);
    flxListDividerKA.add();
    tabsWrapper.add(btnCompletedKA, btnScheduledKA, btnFailedKA, flxDeselectedIndicatorKA, flxDeselectedIndicator2KA, flxDeselectedIndicator3KA, flxSelectedIndicatorKA, flxListDividerKA);
    flxBottomContainerKA.add(flxTransactionListsContainerKA, tabsWrapper);
    frmPayeeTransactionsKA.add(flxTopWrapperKA, flxBottomContainerKA);
};

function frmPayeeTransactionsKAGlobals() {
    frmPayeeTransactionsKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPayeeTransactionsKA,
        "bounces": false,
        "enabledForIdleTimeout": true,
        "id": "frmPayeeTransactionsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "inTransitionConfig": {
            "formAnimation": 3
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};