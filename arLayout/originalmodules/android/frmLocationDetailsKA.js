function addWidgetsfrmLocationDetailsKA() {
    frmLocationDetailsKA.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_c69a0702931f44f4928936f903a24de0,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 10
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblBack0j7bfe4f6b9a548 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblBack0j7bfe4f6b9a548",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, CopylblBack0j7bfe4f6b9a548);
    var lblTitle = new kony.ui.Label({
        "height": "100%",
        "id": "lblTitle",
        "isVisible": true,
        "left": "20%",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.details"),
        "top": "0%",
        "width": "60%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(flxBack, lblTitle);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "91%",
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknConfirmTransfer",
        "top": "9%",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var flxLocationDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxLocationDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "1%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLocationDetails.setDefaultUnit(kony.flex.DP);
    var lblNameTitle = new kony.ui.Label({
        "height": "35dp",
        "id": "lblNameTitle",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular130149164",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblName = new kony.ui.Label({
        "id": "lblName",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "xyz",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var segmentBorderBottomAndroid = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2px",
        "id": "segmentBorderBottomAndroid",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknBlueLineSeparator023356",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    segmentBorderBottomAndroid.setDefaultUnit(kony.flex.DP);
    segmentBorderBottomAndroid.add();
    var lblTypeTitle = new kony.ui.Label({
        "height": "35dp",
        "id": "lblTypeTitle",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular130149164",
        "text": kony.i18n.getLocalizedString("i18n.common.type"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8dp",
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblType = new kony.ui.Label({
        "id": "lblType",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "abc",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopysegmentBorderBottomAndroid0a046ebf4199d49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2px",
        "id": "CopysegmentBorderBottomAndroid0a046ebf4199d49",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknBlueLineSeparator023356",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopysegmentBorderBottomAndroid0a046ebf4199d49.setDefaultUnit(kony.flex.DP);
    CopysegmentBorderBottomAndroid0a046ebf4199d49.add();
    var lblMobile = new kony.ui.Label({
        "height": "35dp",
        "id": "lblMobile",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular130149164",
        "text": kony.i18n.getLocalizedString("i18n.map.TelephonNummber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8dp",
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMobileNumber = new kony.ui.Label({
        "id": "lblMobileNumber",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "9877665522",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxTeleBorder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2px",
        "id": "flxTeleBorder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknBlueLineSeparator023356",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTeleBorder.setDefaultUnit(kony.flex.DP);
    flxTeleBorder.add();
    var lblExt = new kony.ui.Label({
        "height": "35dp",
        "id": "lblExt",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular130149164",
        "text": kony.i18n.getLocalizedString("i18n.map.buildingNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8dp",
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblExtVal = new kony.ui.Label({
        "id": "lblExtVal",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "679 456 2341",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxextBorder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2px",
        "id": "flxextBorder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknBlueLineSeparator023356",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxextBorder.setDefaultUnit(kony.flex.DP);
    flxextBorder.add();
    var lblFax = new kony.ui.Label({
        "height": "35dp",
        "id": "lblFax",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular130149164",
        "text": kony.i18n.getLocalizedString("i18n.common.Fax"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8dp",
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFaxVal = new kony.ui.Label({
        "id": "lblFaxVal",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "john.doe@boj.com",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxFaxBorder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2px",
        "id": "flxFaxBorder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknBlueLineSeparator023356",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFaxBorder.setDefaultUnit(kony.flex.DP);
    flxFaxBorder.add();
    var lblAddress1Title = new kony.ui.Label({
        "height": "35dp",
        "id": "lblAddress1Title",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular130149164",
        "text": kony.i18n.getLocalizedString("i18n.myprofile.AddressLine1"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8dp",
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAddressLine1 = new kony.ui.Label({
        "id": "lblAddressLine1",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "7380 Madiha Appartments",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopysegmentBorderBottomAndroid0bab0b7b3cf6242 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2px",
        "id": "CopysegmentBorderBottomAndroid0bab0b7b3cf6242",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknBlueLineSeparator023356",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopysegmentBorderBottomAndroid0bab0b7b3cf6242.setDefaultUnit(kony.flex.DP);
    CopysegmentBorderBottomAndroid0bab0b7b3cf6242.add();
    var lblAddress2Title = new kony.ui.Label({
        "height": "35dp",
        "id": "lblAddress2Title",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular130149164",
        "text": kony.i18n.getLocalizedString("i18n.myprofile.AddressLine2"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8dp",
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAddressLine2 = new kony.ui.Label({
        "id": "lblAddressLine2",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "West Sand Lake Road",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopysegmentBorderBottomAndroid0d29b652b34754b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2px",
        "id": "CopysegmentBorderBottomAndroid0d29b652b34754b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknBlueLineSeparator023356",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopysegmentBorderBottomAndroid0d29b652b34754b.setDefaultUnit(kony.flex.DP);
    CopysegmentBorderBottomAndroid0d29b652b34754b.add();
    var lblCityTitle = new kony.ui.Label({
        "height": "35dp",
        "id": "lblCityTitle",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular130149164",
        "text": kony.i18n.getLocalizedString("i18n.profileedit.city"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8dp",
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCity = new kony.ui.Label({
        "id": "lblCity",
        "isVisible": true,
        "left": "7%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "Amman",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopysegmentBorderBottomAndroid0a1acc80b506f4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2px",
        "id": "CopysegmentBorderBottomAndroid0a1acc80b506f4b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknBlueLineSeparator023356",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopysegmentBorderBottomAndroid0a1acc80b506f4b.setDefaultUnit(kony.flex.DP);
    CopysegmentBorderBottomAndroid0a1acc80b506f4b.add();
    var btnGetDirection = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknprimaryAction",
        "height": "50dp",
        "id": "btnGetDirection",
        "isVisible": true,
        "onClick": AS_Button_j0559aa0337f4e4c8d38d052cc08e773,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.locateus.getDirections"),
        "top": "10dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopysegmentBorderBottomAndroid0g7be49665e5743 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2px",
        "id": "CopysegmentBorderBottomAndroid0g7be49665e5743",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopysegmentBorderBottomAndroid0g7be49665e5743.setDefaultUnit(kony.flex.DP);
    CopysegmentBorderBottomAndroid0g7be49665e5743.add();
    flxLocationDetails.add(lblNameTitle, lblName, segmentBorderBottomAndroid, lblTypeTitle, lblType, CopysegmentBorderBottomAndroid0a046ebf4199d49, lblMobile, lblMobileNumber, flxTeleBorder, lblExt, lblExtVal, flxextBorder, lblFax, lblFaxVal, flxFaxBorder, lblAddress1Title, lblAddressLine1, CopysegmentBorderBottomAndroid0bab0b7b3cf6242, lblAddress2Title, lblAddressLine2, CopysegmentBorderBottomAndroid0d29b652b34754b, lblCityTitle, lblCity, CopysegmentBorderBottomAndroid0a1acc80b506f4b, btnGetDirection, CopysegmentBorderBottomAndroid0g7be49665e5743);
    mainContent.add(flxLocationDetails);
    frmLocationDetailsKA.add(flxHeader, mainContent);
};

function frmLocationDetailsKAGlobals() {
    frmLocationDetailsKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmLocationDetailsKA,
        "enabledForIdleTimeout": false,
        "id": "frmLocationDetailsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "slFormCommon"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_fd77d4cbeb9d418f960977a7cd693719,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};