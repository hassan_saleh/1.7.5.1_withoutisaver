function addWidgetsfrmApplyNewCardsConfirmKA() {
    frmApplyNewCardsConfirmKA.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_e1bff9ec879642459df7f5d7f4db94eb,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblApplyCardTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "90%",
        "id": "lblApplyCardTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(flxBack, lblApplyCardTitle);
    var flxApplyNewCardsConfirmBody = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "70%",
        "horizontalScrollIndicator": true,
        "id": "flxApplyNewCardsConfirmBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "10%",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxApplyNewCardsConfirmBody.setDefaultUnit(kony.flex.DP);
    var flxCreditCardNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxCreditCardNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxCreditCardNumber.setDefaultUnit(kony.flex.DP);
    var lblCreditCardNumberTitle = new kony.ui.Label({
        "id": "lblCreditCardNumberTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.primarycreditcardnumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCreditCardNumber = new kony.ui.Label({
        "id": "lblCreditCardNumber",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCreditCardNumber.add(lblCreditCardNumberTitle, lblCreditCardNumber);
    var flxUpgradeCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxUpgradeCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxUpgradeCard.setDefaultUnit(kony.flex.DP);
    var lblUpgradeCardTitle = new kony.ui.Label({
        "id": "lblUpgradeCardTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.cards.upgradetype"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblUpgradeCard = new kony.ui.Label({
        "id": "lblUpgradeCard",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxUpgradeCard.add(lblUpgradeCardTitle, lblUpgradeCard);
    var flxSuplementryCardType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxSuplementryCardType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSuplementryCardType.setDefaultUnit(kony.flex.DP);
    var lblSuplementryCardTypeTitle = new kony.ui.Label({
        "id": "lblSuplementryCardTypeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.supplementrycardtype"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSuplementryCardType = new kony.ui.Label({
        "id": "lblSuplementryCardType",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSuplementryCardType.add(lblSuplementryCardTypeTitle, lblSuplementryCardType);
    var flxAccountNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxAccountNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAccountNumber.setDefaultUnit(kony.flex.DP);
    var lblAccountNumberTitle = new kony.ui.Label({
        "id": "lblAccountNumberTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAccountNumber.add(lblAccountNumberTitle, lblAccountNumber);
    var flxCardHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxCardHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxCardHolder.setDefaultUnit(kony.flex.DP);
    var lblCardHolderTitle = new kony.ui.Label({
        "id": "lblCardHolderTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.cardholdername"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCardHolder = new kony.ui.Label({
        "id": "lblCardHolder",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCardHolder.add(lblCardHolderTitle, lblCardHolder);
    var flxRelationShip = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxRelationShip",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRelationShip.setDefaultUnit(kony.flex.DP);
    var lblRelationShipTitle = new kony.ui.Label({
        "id": "lblRelationShipTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.applycards.realationship"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRelationShip = new kony.ui.Label({
        "id": "lblRelationShip",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRelationShip.add(lblRelationShipTitle, lblRelationShip);
    var flxAge = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxAge",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAge.setDefaultUnit(kony.flex.DP);
    var lblAgeTitle = new kony.ui.Label({
        "id": "lblAgeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.common.age"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAge = new kony.ui.Label({
        "id": "lblAge",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAge.add(lblAgeTitle, lblAge);
    var flxReason = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxReason",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxReason.setDefaultUnit(kony.flex.DP);
    var lblReasonTitle = new kony.ui.Label({
        "id": "lblReasonTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.reason"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblReason = new kony.ui.Label({
        "id": "lblReason",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxReason.add(lblReasonTitle, lblReason);
    var flxColor = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxColor",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxColor.setDefaultUnit(kony.flex.DP);
    var lblColorTitle = new kony.ui.Label({
        "id": "lblColorTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.common.color"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblColor = new kony.ui.Label({
        "id": "lblColor",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxColor.add(lblColorTitle, lblColor);
    var flxLimitType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxLimitType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxLimitType.setDefaultUnit(kony.flex.DP);
    var lblLimitTypeTitle = new kony.ui.Label({
        "id": "lblLimitTypeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.limittype"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLimitType = new kony.ui.Label({
        "id": "lblLimitType",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLimitType.add(lblLimitTypeTitle, lblLimitType);
    var flxSuplementryCardLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxSuplementryCardLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSuplementryCardLimit.setDefaultUnit(kony.flex.DP);
    var lblSuplementryCardLimitTitle = new kony.ui.Label({
        "id": "lblSuplementryCardLimitTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.supplementrycardlimit"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSuplementryCardLimit = new kony.ui.Label({
        "id": "lblSuplementryCardLimit",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSuplementryCardLimit.add(lblSuplementryCardLimitTitle, lblSuplementryCardLimit);
    var flxEmploymentStatus = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxEmploymentStatus",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxEmploymentStatus.setDefaultUnit(kony.flex.DP);
    var lblEmploymentStatusTitle = new kony.ui.Label({
        "id": "lblEmploymentStatusTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.employmentstatus"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEmploymentStatus = new kony.ui.Label({
        "id": "lblEmploymentStatus",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEmploymentStatus.add(lblEmploymentStatusTitle, lblEmploymentStatus);
    var flxProfession = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxProfession",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxProfession.setDefaultUnit(kony.flex.DP);
    var lblProfessionTitle = new kony.ui.Label({
        "id": "lblProfessionTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newcards.profession"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblProfession = new kony.ui.Label({
        "id": "lblProfession",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxProfession.add(lblProfessionTitle, lblProfession);
    var flxTransferedSalary = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxTransferedSalary",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTransferedSalary.setDefaultUnit(kony.flex.DP);
    var lblTransferedSalaryTitle = new kony.ui.Label({
        "id": "lblTransferedSalaryTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.transferedsalary"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransferedSalary = new kony.ui.Label({
        "id": "lblTransferedSalary",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTransferedSalary.add(lblTransferedSalaryTitle, lblTransferedSalary);
    var flxMonthlyIncome = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxMonthlyIncome",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxMonthlyIncome.setDefaultUnit(kony.flex.DP);
    var lblMonthlyIncomeTitle = new kony.ui.Label({
        "id": "lblMonthlyIncomeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.monthlyincome"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMonthlyIncome = new kony.ui.Label({
        "id": "lblMonthlyIncome",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMonthlyIncome.add(lblMonthlyIncomeTitle, lblMonthlyIncome);
    var flxRequestLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxRequestLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRequestLimit.setDefaultUnit(kony.flex.DP);
    var lblRequestLimitTitle = new kony.ui.Label({
        "id": "lblRequestLimitTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.requestlimit"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRequestLimit = new kony.ui.Label({
        "id": "lblRequestLimit",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRequestLimit.add(lblRequestLimitTitle, lblRequestLimit);
    var flxDeliveryMode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxDeliveryMode",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDeliveryMode.setDefaultUnit(kony.flex.DP);
    var lblDeliveryModeTitle = new kony.ui.Label({
        "id": "lblDeliveryModeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.accounts.deliveryMode"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDeliveryMode = new kony.ui.Label({
        "id": "lblDeliveryMode",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDeliveryMode.add(lblDeliveryModeTitle, lblDeliveryMode);
    var flxNotes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "18%",
        "id": "flxNotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNotes.setDefaultUnit(kony.flex.DP);
    var lblNotesTitle = new kony.ui.Label({
        "id": "lblNotesTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.notes"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNotes = new kony.ui.Label({
        "id": "lblNotes",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "26%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNotes.add(lblNotesTitle, lblNotes);
    flxApplyNewCardsConfirmBody.add(flxCreditCardNumber, flxUpgradeCard, flxSuplementryCardType, flxAccountNumber, flxCardHolder, flxRelationShip, flxAge, flxReason, flxColor, flxLimitType, flxSuplementryCardLimit, flxEmploymentStatus, flxProfession, flxTransferedSalary, flxMonthlyIncome, flxRequestLimit, flxDeliveryMode, flxNotes);
    var flxTermsandConditionCheck = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxTermsandConditionCheck",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "3%",
        "skin": "slFbox",
        "top": "82%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxTermsandConditionCheck.setDefaultUnit(kony.flex.DP);
    var lblTermsandConditionsCheckBox = new kony.ui.Label({
        "id": "lblTermsandConditionsCheckBox",
        "isVisible": true,
        "left": "1%",
        "onTouchEnd": AS_Label_fa984e91e5bb4b6d9c19e156b03e30a3,
        "skin": "sknBOJttfwhitee150",
        "text": "q",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxTncBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxTncBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "10%",
        "onClick": AS_Label_idf25a9034c64002b77400650c0e0f3b,
        "skin": "slFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTncBody.setDefaultUnit(kony.flex.DP);
    var richtxtTermsandConditions = new kony.ui.RichText({
        "id": "richtxtTermsandConditions",
        "isVisible": true,
        "left": "0%",
        "skin": "sknrichTxtWhite100",
        "text": "RichText",
        "top": "0dp",
        "width": "95%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTncBody.add(richtxtTermsandConditions);
    flxTermsandConditionCheck.add(lblTermsandConditionsCheckBox, flxTncBody);
    var btnconfirm = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "94%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "8%",
        "id": "btnconfirm",
        "isVisible": true,
        "onClick": AS_Button_iee0fe973fe9429d9d58a8dd99374807,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    frmApplyNewCardsConfirmKA.add(flxHeader, flxApplyNewCardsConfirmBody, flxTermsandConditionCheck, btnconfirm);
};

function frmApplyNewCardsConfirmKAGlobals() {
    frmApplyNewCardsConfirmKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmApplyNewCardsConfirmKA,
        "bounces": false,
        "enabledForIdleTimeout": false,
        "id": "frmApplyNewCardsConfirmKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "sknmainGradient",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};