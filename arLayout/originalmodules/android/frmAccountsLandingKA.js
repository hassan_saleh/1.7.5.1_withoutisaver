function addWidgetsfrmAccountsLandingKA() {
    frmAccountsLandingKA.setDefaultUnit(kony.flex.DP);
    var flxAccountsMain = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": false,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxAccountsMain",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknflxAccounts",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccountsMain.setDefaultUnit(kony.flex.DP);
    var accountsOuterScroll = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "id": "accountsOuterScroll",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onScrolling": AS_FlexScrollContainer_f331ad382d6d40c885274dde245b1220,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknslFSbox",
        "top": "19%",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    accountsOuterScroll.setDefaultUnit(kony.flex.DP);
    var accountsWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": false,
        "id": "accountsWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    accountsWrapper.setDefaultUnit(kony.flex.DP);
    var accountsInnerScroll = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "id": "accountsInnerScroll",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "onScrollEnd": AS_FlexScrollContainer_f768cdf40c0a48beb3a7fb99b34a7e89,
        "onScrolling": AS_FlexScrollContainer_ga6c96abdbc54655b30cced3b3638a56,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%"
    }, {}, {});
    accountsInnerScroll.setDefaultUnit(kony.flex.DP);
    var segAccountsKA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0%",
        "centerX": "50%",
        "data": [
            [{
                    "CopylblIncommingRing0ac2c9e9c52d147": kony.i18n.getLocalizedString("i18n.common.reverseback"),
                    "Label0bed5640cdb4b46": "Label",
                    "amountAccount1": "00.00",
                    "amountcurrBal": "$00.00",
                    "amtOutsatndingBal": "$00.00",
                    "btnNav": "L",
                    "dummyAccountName": "",
                    "dummyAccountNumber": "",
                    "isPFMLabel": "",
                    "lblAccNumber": "",
                    "lblAccountID": "Label",
                    "lblBankName": "",
                    "lblColorKA": "",
                    "lblCurrency": "JOD",
                    "lblIncommingRing": "s",
                    "lblMobile": "",
                    "lblRowSeparator": "",
                    "nameAccount1": "<p>tqwwqwqeqweqweqwewe qweqweqweqweqweqwe qweqweqwe</p><img align = \"left\" src=\"mapblue.png\" />\n                        ",
                    "nameAccount12": "Account Name",
                    "typeAccount": "Available Balance",
                    "typeKA": ""
                },
                [{
                    "Button0hf394843f2a54f": "",
                    "lblNoData": "",
                    "lblTransAmount": "",
                    "lblTransDate": "",
                    "lblTransId": "",
                    "lblTransName": ""
                }]
            ],
            [{
                    "CopylblIncommingRing0ac2c9e9c52d147": kony.i18n.getLocalizedString("i18n.common.reverseback"),
                    "Label0bed5640cdb4b46": "Label",
                    "amountAccount1": "00.00",
                    "amountcurrBal": "$00.00",
                    "amtOutsatndingBal": "$00.00",
                    "btnNav": "L",
                    "dummyAccountName": "",
                    "dummyAccountNumber": "",
                    "isPFMLabel": "",
                    "lblAccNumber": "",
                    "lblAccountID": "Label",
                    "lblBankName": "",
                    "lblColorKA": "",
                    "lblCurrency": "JOD",
                    "lblIncommingRing": "s",
                    "lblMobile": "",
                    "lblRowSeparator": "",
                    "nameAccount1": "<p>t</p><img align = \"left\" src=\"mapblue.png\" />\n                        ",
                    "nameAccount12": "Account Name",
                    "typeAccount": "Available Balance",
                    "typeKA": ""
                },
                [{
                    "Button0hf394843f2a54f": "",
                    "lblNoData": "",
                    "lblTransAmount": "",
                    "lblTransDate": "",
                    "lblTransId": "",
                    "lblTransName": ""
                }]
            ]
        ],
        "groupCells": false,
        "id": "segAccountsKA",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowSkin": "seg2Normal",
        "rowTemplate": flxTrans,
        "scrollingEvents": {},
        "sectionHeaderTemplate": yourAccount1,
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "072c4800",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": true,
        "top": "0%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "Button0hf394843f2a54f": "Button0hf394843f2a54f",
            "CopylblIncommingRing0ac2c9e9c52d147": "CopylblIncommingRing0ac2c9e9c52d147",
            "Label0bed5640cdb4b46": "Label0bed5640cdb4b46",
            "amountAccount1": "amountAccount1",
            "amountcurrBal": "amountcurrBal",
            "amtOutsatndingBal": "amtOutsatndingBal",
            "btnNav": "btnNav",
            "btnNav1": "btnNav1",
            "dummyAccountName": "dummyAccountName",
            "dummyAccountNumber": "dummyAccountNumber",
            "flxLine": "flxLine",
            "flxSet": "flxSet",
            "flxTrans": "flxTrans",
            "flxamount": "flxamount",
            "isPFMLabel": "isPFMLabel",
            "lblAccNumber": "lblAccNumber",
            "lblAccountID": "lblAccountID",
            "lblBankName": "lblBankName",
            "lblColorKA": "lblColorKA",
            "lblCurrency": "lblCurrency",
            "lblIncommingRing": "lblIncommingRing",
            "lblMobile": "lblMobile",
            "lblNoData": "lblNoData",
            "lblRowSeparator": "lblRowSeparator",
            "lblTransAmount": "lblTransAmount",
            "lblTransDate": "lblTransDate",
            "lblTransId": "lblTransId",
            "lblTransName": "lblTransName",
            "nameAccount1": "nameAccount1",
            "nameAccount12": "nameAccount12",
            "nameContainer": "nameContainer",
            "typeAccount": "typeAccount",
            "typeKA": "typeKA",
            "yourAccount1": "yourAccount1"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var LabelNoRecordsKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "15.50%",
        "id": "LabelNoRecordsKA",
        "isVisible": false,
        "left": "17.52%",
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.alerts.NoAccountsMsg"),
        "top": "20dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    accountsInnerScroll.add(segAccountsKA, LabelNoRecordsKA);
    accountsWrapper.add(accountsInnerScroll);
    var androidInsightsCover = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidInsightsCover",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntitleBarGradient",
        "top": "43dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidInsightsCover.setDefaultUnit(kony.flex.DP);
    androidInsightsCover.add();
    accountsOuterScroll.add(accountsWrapper, androidInsightsCover);
    var accountLabelScroll = new kony.ui.Label({
        "height": "50dp",
        "id": "accountLabelScroll",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknCopysecondaryHeader07cfb9398604f4f",
        "text": kony.i18n.getLocalizedString("i18n.overview.yourAccount"),
        "top": "136dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var insightsButton = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknsecondaryActionReversedFocus",
        "height": "42dp",
        "id": "insightsButton",
        "isVisible": false,
        "onClick": AS_Button_c4eb73a5da5c4141ba183eb14a92d3a8,
        "skin": "sknsecondaryActionReversed",
        "text": kony.i18n.getLocalizedString("i18n.overview.hideAccountInsights"),
        "top": "130dp",
        "width": "250dp",
        "zIndex": 999
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var cashCreditOverview = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "64dp",
        "id": "cashCreditOverview",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5.00%",
        "skin": "sknslFbox",
        "top": "15%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    cashCreditOverview.setDefaultUnit(kony.flex.DP);
    var FlexContainer04dfa3182c5f34e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35dp",
        "id": "FlexContainer04dfa3182c5f34e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer04dfa3182c5f34e.setDefaultUnit(kony.flex.DP);
    var Label01a9e582c581146 = new kony.ui.Label({
        "centerY": "50%",
        "id": "Label01a9e582c581146",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLatoRegularFFFFFF107KA",
        "text": kony.i18n.getLocalizedString("i18n.overview.cash"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var cashOverviewLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "cashOverviewLabel",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknCopyslLabel03fbcc1555fb34b",
        "text": "7568.54",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer04dfa3182c5f34e.add(Label01a9e582c581146, cashOverviewLabel);
    var CopyFlexContainer0c37fe8523d504d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35dp",
        "id": "CopyFlexContainer0c37fe8523d504d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": 0,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0c37fe8523d504d.setDefaultUnit(kony.flex.DP);
    var CopyLabel031e1abe8023247 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopyLabel031e1abe8023247",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLatoRegularFFFFFF107KA",
        "text": kony.i18n.getLocalizedString("i18n.overview.creditDebt"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var creditDebtOverviewLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "creditDebtOverviewLabel",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknCopyslLabel043dabd91ff4847",
        "text": "- 1529.54",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0c37fe8523d504d.add(CopyLabel031e1abe8023247, creditDebtOverviewLabel);
    var cashCreditOverviewBorderBottom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "50%",
        "clipBounds": true,
        "height": "1dp",
        "id": "cashCreditOverviewBorderBottom",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknwhiteDivider50",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    cashCreditOverviewBorderBottom.setDefaultUnit(kony.flex.DP);
    cashCreditOverviewBorderBottom.add();
    cashCreditOverview.add(FlexContainer04dfa3182c5f34e, CopyFlexContainer0c37fe8523d504d, cashCreditOverviewBorderBottom);
    var flxTabAccounts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxTabAccounts",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "7%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxTabAccounts.setDefaultUnit(kony.flex.DP);
    var btnAccounts = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnAccounts",
        "height": "100%",
        "id": "btnAccounts",
        "isVisible": true,
        "left": "0%",
        "skin": "btnAccounts",
        "text": kony.i18n.getLocalizedString("i18n.accounts.accounts"),
        "top": "0dp",
        "width": "33.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnDeals = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnLoans",
        "height": "100%",
        "id": "btnDeals",
        "isVisible": true,
        "left": "33.30%",
        "onClick": AS_Button_e4773561daab4cdeb9cf285d0b592b8e,
        "skin": "btnLoans",
        "text": kony.i18n.getLocalizedString("i18n.accounts.deals"),
        "top": "0dp",
        "width": "33.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnLoans = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnLoans",
        "height": "100%",
        "id": "btnLoans",
        "isVisible": true,
        "left": "66.60%",
        "onClick": AS_Button_d9d4d61dae1a423aa29b8e3fff52f073,
        "right": "0%",
        "skin": "btnLoans",
        "text": kony.i18n.getLocalizedString("i18n.accounts.loans"),
        "top": "0dp",
        "width": "33.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTabAccounts.add(btnAccounts, btnDeals, btnLoans);
    var btnNotification = new kony.ui.Button({
        "centerY": "90%",
        "focusSkin": "sknOrangeBGRNDFontBOJ2",
        "height": "60dp",
        "id": "btnNotification",
        "isVisible": false,
        "onClick": AS_Button_c0d38695780f4d85be603425d748fd93,
        "right": "5%",
        "skin": "sknOrangeBGRNDFontBOJ2",
        "text": "A",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxAccountsMain.add(accountsOuterScroll, accountLabelScroll, insightsButton, cashCreditOverview, flxTabAccounts, btnNotification);
    var flxAccntLandingNavOptKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxAccntLandingNavOptKA",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "onClick": AS_FlexContainer_h2d05ab6074e4740bfad620e73e2de15,
        "skin": "sknFlx20000000KA",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccntLandingNavOptKA.setDefaultUnit(kony.flex.DP);
    var flxNavSegment1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": 0,
        "centerX": "50%",
        "centerY": "75%",
        "clipBounds": true,
        "id": "flxNavSegment1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "skin": "sknRoundedCornerKA",
        "top": "50%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNavSegment1.setDefaultUnit(kony.flex.DP);
    var segAccountOverViewNav1KA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblMenuItem": "Search Transactions"
        }, {
            "lblMenuItem": "Make a Transfer/Pay a bill "
        }, {
            "lblMenuItem": "Open New Account  "
        }],
        "groupCells": false,
        "id": "segAccountOverViewNav1KA",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_e4424f0c684446d38abd218698609415,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowSkin": "seg2Normal",
        "rowTemplate": flxNavigationOptKA,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxLineDividerNavOptKA": "flxLineDividerNavOptKA",
            "flxNavigationOptKA": "flxNavigationOptKA",
            "lblMenuItem": "lblMenuItem"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnSignOutNavOptAccOverKA = new kony.ui.Button({
        "focusSkin": "sknBtnSignOutEB5000KA",
        "height": "50dp",
        "id": "btnSignOutNavOptAccOverKA",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_c42e3f7edb294409a22e7c07dbcf34b2,
        "skin": "sknBtnSignOutEB5000KA",
        "text": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxNavSegment1.add(segAccountOverViewNav1KA, btnSignOutNavOptAccOverKA);
    flxAccntLandingNavOptKA.add(flxNavSegment1);
    var showInsightsButton = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknsecondaryActionReversedFocus",
        "height": "42dp",
        "id": "showInsightsButton",
        "isVisible": false,
        "onClick": AS_Button_cb594805a41f4e3a9af31331a209079f,
        "skin": "sknsecondaryActionReversed",
        "text": kony.i18n.getLocalizedString("i18n.overview.showInsightsButton"),
        "top": "130dp",
        "width": "250dp",
        "zIndex": 100
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var overview = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "overview",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    overview.setDefaultUnit(kony.flex.DP);
    var navBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "navBar",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    navBar.setDefaultUnit(kony.flex.DP);
    var searchButton = new kony.ui.Button({
        "focusSkin": "skntitleBarSearchButtonFocus",
        "height": "50dp",
        "id": "searchButton",
        "isVisible": false,
        "left": "80%",
        "onClick": AS_Button_g693755ea47548d89e9880f7e59d7bf1,
        "right": "5%",
        "skin": "skntitleBarSearchButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var hamburgerButton = new kony.ui.Button({
        "focusSkin": "sknhamburgerButtonFocus",
        "height": "50dp",
        "id": "hamburgerButton",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_c9d4c9ba11c64454b420ab9ef744a4f4,
        "skin": "sknhamburgerButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    androidTitleBar.add(hamburgerButton);
    var flxUserGreetingAndName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxUserGreetingAndName",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "skin": "slFbox",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxUserGreetingAndName.setDefaultUnit(kony.flex.DP);
    var lblGreetingTimeTitleBarKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblGreetingTimeTitleBarKA",
        "isVisible": true,
        "skin": "sknLatoRegularFFFFFF107KA",
        "text": kony.i18n.getLocalizedString("i18n.accountPreview.goodMorning"),
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblUserNameTitleBarKA = new kony.ui.Label({
        "id": "lblUserNameTitleBarKA",
        "isVisible": false,
        "left": "0%",
        "skin": "sknLatoRegularFFFFFF107KA",
        "text": "John",
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxUserGreetingAndName.add(lblGreetingTimeTitleBarKA, lblUserNameTitleBarKA);
    var imgMoreRightSideKA = new kony.ui.Image2({
        "centerX": "95%",
        "height": "100%",
        "id": "imgMoreRightSideKA",
        "isVisible": true,
        "left": "90%",
        "skin": "slImage",
        "src": "more.png",
        "top": "2dp",
        "width": "44px",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flximgOnclickKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80%",
        "id": "flximgOnclickKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "90%",
        "onClick": AS_FlexContainer_d422c704ec0f41509f3270d6f0c32e6b,
        "skin": "slFbox",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {}, {});
    flximgOnclickKA.setDefaultUnit(kony.flex.DP);
    flximgOnclickKA.add();
    navBar.add(searchButton, androidTitleBar, flxUserGreetingAndName, imgMoreRightSideKA, flximgOnclickKA);
    var insightsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "250dp",
        "id": "insightsContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2dp",
        "skin": "sknslFbox",
        "top": "192dp",
        "width": "100%",
        "zIndex": 1000
    }, {}, {});
    insightsContainer.setDefaultUnit(kony.flex.DP);
    var insightsScroll = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": true,
        "allowVerticalBounce": false,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "220dp",
        "horizontalScrollIndicator": false,
        "id": "insightsScroll",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "onScrolling": AS_FlexScrollContainer_cbd353bd76da46bba3dc8702d15e14be,
        "pagingEnabled": true,
        "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
        "skin": "sknslFSbox",
        "top": "22dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    insightsScroll.setDefaultUnit(kony.flex.DP);
    var insightsGraph1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "205dp",
        "id": "insightsGraph1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1000
    }, {}, {});
    insightsGraph1.setDefaultUnit(kony.flex.DP);
    var FlexContainer0f4211087f1ff4d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "29dp",
        "id": "FlexContainer0f4211087f1ff4d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0f4211087f1ff4d.setDefaultUnit(kony.flex.DP);
    var CopyLabel02f15f208359549 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel02f15f208359549",
        "isVisible": true,
        "left": "12dp",
        "skin": "sknaccountOverviewSubheads",
        "text": kony.i18n.getLocalizedString("i18n.overview.allAccountBalances"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0f357c1f2ae0642 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0f357c1f2ae0642",
        "isVisible": true,
        "right": "36dp",
        "skin": "sknoverviewTypeLabel",
        "text": "October 2015",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0f4211087f1ff4d.add(CopyLabel02f15f208359549, CopyLabel0f357c1f2ae0642);
    var Image051433e8ccbf54b = new kony.ui.Image2({
        "height": "176dp",
        "id": "Image051433e8ccbf54b",
        "isVisible": false,
        "left": "-4dp",
        "skin": "sknslImage",
        "src": "all_account_balances.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    insightsGraph1.add(FlexContainer0f4211087f1ff4d, Image051433e8ccbf54b);
    var insightsGraph2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "205dp",
        "id": "insightsGraph2",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1000
    }, {}, {});
    insightsGraph2.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0955dd33b57494a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45dp",
        "id": "CopyFlexContainer0955dd33b57494a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0955dd33b57494a.setDefaultUnit(kony.flex.DP);
    var CopyLabel001fa898ae91849 = new kony.ui.Label({
        "centerX": "50%",
        "height": "22dp",
        "id": "CopyLabel001fa898ae91849",
        "isVisible": true,
        "left": "12dp",
        "skin": "sknaccountOverviewSubheads",
        "text": kony.i18n.getLocalizedString("i18n.overview.yourMonthlySpending"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var FlexContainer016540125bcf04e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "16dp",
        "id": "FlexContainer016540125bcf04e",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "25dp",
        "width": "100%",
        "zIndex": 9999
    }, {}, {});
    FlexContainer016540125bcf04e.setDefaultUnit(kony.flex.DP);
    var Label0d6f5a6f9a23f46 = new kony.ui.Label({
        "centerY": "50%",
        "height": "8dp",
        "id": "Label0d6f5a6f9a23f46",
        "isVisible": true,
        "left": "2dp",
        "skin": "sknYellowCircle",
        "text": "Label",
        "textStyle": {},
        "top": "0dp",
        "width": "8dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLastMonthKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLastMonthKA",
        "isVisible": true,
        "left": "2dp",
        "skin": "sknFFFFFF93LatoLightKA",
        "text": "Last Month: $81,950.00",
        "textStyle": {},
        "top": "0dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0e381c893448d4d = new kony.ui.Label({
        "centerY": "50%",
        "height": "8dp",
        "id": "CopyLabel0e381c893448d4d",
        "isVisible": true,
        "left": "2dp",
        "skin": "sknBlueCircle",
        "text": "Label",
        "textStyle": {},
        "top": "0dp",
        "width": "8dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblThismonthKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblThismonthKA",
        "isVisible": true,
        "left": "2dp",
        "skin": "sknFFFFFF93LatoLightKA",
        "text": "This Month: $84,400.00",
        "textStyle": {},
        "top": "0dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer016540125bcf04e.add(Label0d6f5a6f9a23f46, lblLastMonthKA, CopyLabel0e381c893448d4d, lblThismonthKA);
    CopyFlexContainer0955dd33b57494a.add(CopyLabel001fa898ae91849, FlexContainer016540125bcf04e);
    var CopyImage068d331806a3244 = new kony.ui.Image2({
        "height": "176dp",
        "id": "CopyImage068d331806a3244",
        "isVisible": false,
        "left": "-4dp",
        "skin": "sknslImage",
        "src": "graph_two.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    insightsGraph2.add(CopyFlexContainer0955dd33b57494a, CopyImage068d331806a3244);
    var cashflowContainerinSightScroll = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "220dp",
        "id": "cashflowContainerinSightScroll",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1000
    }, {}, {});
    cashflowContainerinSightScroll.setDefaultUnit(kony.flex.DP);
    var cashFlowContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "71dp",
        "id": "cashFlowContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    cashFlowContainer.setDefaultUnit(kony.flex.DP);
    var CopyLabel0041d3446730b49 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0041d3446730b49",
        "isVisible": true,
        "skin": "sknaccountOverviewSubheads",
        "text": "October Cash Flow",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var cashFlowGraphContainer = new kony.ui.FlexContainer({
        "clipBounds": false,
        "height": "220dp",
        "id": "cashFlowGraphContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "12dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    cashFlowGraphContainer.setDefaultUnit(kony.flex.DP);
    var cashFlowGraph = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "53dp",
        "id": "cashFlowGraph",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    cashFlowGraph.setDefaultUnit(kony.flex.DP);
    var incomeGraph = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "12dp",
        "id": "incomeGraph",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknCopyslFbox08373f9b4304a49",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    incomeGraph.setDefaultUnit(kony.flex.DP);
    incomeGraph.add();
    var spendGraph = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "12dp",
        "id": "spendGraph",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknCopyslFbox00bdff10a6e8e49",
        "top": "10dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    spendGraph.setDefaultUnit(kony.flex.DP);
    spendGraph.add();
    cashFlowGraph.add(incomeGraph, spendGraph);
    var cashFlowGraphResults = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "53dp",
        "id": "cashFlowGraphResults",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {}, {});
    cashFlowGraphResults.setDefaultUnit(kony.flex.DP);
    var positiveCashFlowLabel = new kony.ui.Label({
        "centerX": "50%",
        "height": "17dp",
        "id": "positiveCashFlowLabel",
        "isVisible": true,
        "skin": "sknoverviewTypeLabel",
        "text": "3268.54",
        "top": "-2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var negativeCashFlowLabel = new kony.ui.Label({
        "centerX": "50%",
        "height": "17dp",
        "id": "negativeCashFlowLabel",
        "isVisible": true,
        "skin": "sknoverviewTypeLabel",
        "text": "- 1542.09",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    cashFlowGraphResults.add(positiveCashFlowLabel, negativeCashFlowLabel);
    cashFlowGraphContainer.add(cashFlowGraph, cashFlowGraphResults);
    var CopycashCreditOverviewBorderBottom0dbc2a40016bb44 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "50%",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopycashCreditOverviewBorderBottom0dbc2a40016bb44",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknwhiteDivider50",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    CopycashCreditOverviewBorderBottom0dbc2a40016bb44.setDefaultUnit(kony.flex.DP);
    CopycashCreditOverviewBorderBottom0dbc2a40016bb44.add();
    cashFlowContainer.add(CopyLabel0041d3446730b49, cashFlowGraphContainer, CopycashCreditOverviewBorderBottom0dbc2a40016bb44);
    cashflowContainerinSightScroll.add(cashFlowContainer);
    insightsScroll.add(insightsGraph1, insightsGraph2, cashflowContainerinSightScroll);
    var insightsPull = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "42dp",
        "id": "insightsPull",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "175dp",
        "zIndex": 9999
    }, {}, {});
    insightsPull.setDefaultUnit(kony.flex.DP);
    var insightsPullLabel = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "insightsPullLabel",
        "isVisible": false,
        "onClick": AS_Button_c21faf739fa74b958dfaca3246379d02,
        "skin": "sknCopyprimaryAction0eb0149d262af4f",
        "text": kony.i18n.getLocalizedString("i18n.overview.pullForAccountInsights"),
        "top": "0dp",
        "width": "250dp",
        "zIndex": 9999
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var insightsLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "1dp",
        "id": "insightsLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox0fc825aa3b24848",
        "top": "32dp",
        "width": "1%",
        "zIndex": 9999
    }, {}, {});
    insightsLine.setDefaultUnit(kony.flex.DP);
    insightsLine.add();
    insightsPull.add(insightsPullLabel, insightsLine);
    var pagingContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "centerX": "50%",
        "clipBounds": false,
        "height": "10dp",
        "id": "pagingContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "width": "100%",
        "zIndex": 9999
    }, {}, {});
    pagingContainer.setDefaultUnit(kony.flex.DP);
    var FlexContainer08b16aea3d71b4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "45%",
        "clipBounds": true,
        "height": "10dp",
        "id": "FlexContainer08b16aea3d71b4e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknpagingIndicator",
        "top": "0dp",
        "width": "10dp"
    }, {}, {});
    FlexContainer08b16aea3d71b4e.setDefaultUnit(kony.flex.DP);
    FlexContainer08b16aea3d71b4e.add();
    var pagingActive = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "45%",
        "clipBounds": false,
        "height": "10dp",
        "id": "pagingActive",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknpagingIndicatorActive",
        "top": "0dp",
        "width": "10dp"
    }, {}, {});
    pagingActive.setDefaultUnit(kony.flex.DP);
    pagingActive.add();
    var CopyFlexContainer0a37b9e9af7134e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "10dp",
        "id": "CopyFlexContainer0a37b9e9af7134e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknpagingIndicator",
        "top": "0dp",
        "width": "10dp"
    }, {}, {});
    CopyFlexContainer0a37b9e9af7134e.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0a37b9e9af7134e.add();
    var CopyFlexContainer02173faf744554f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "55%",
        "clipBounds": false,
        "height": "10dp",
        "id": "CopyFlexContainer02173faf744554f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknpagingIndicator",
        "top": "0dp",
        "width": "10dp"
    }, {}, {});
    CopyFlexContainer02173faf744554f.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer02173faf744554f.add();
    pagingContainer.add(FlexContainer08b16aea3d71b4e, pagingActive, CopyFlexContainer0a37b9e9af7134e, CopyFlexContainer02173faf744554f);
    insightsContainer.add(insightsScroll, insightsPull, pagingContainer);
    overview.add(navBar, insightsContainer);
    var flxInFeedAds = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "44dp",
        "id": "flxInFeedAds",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknNoBanner",
        "top": "180dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxInFeedAds.setDefaultUnit(kony.flex.DP);
    var imgInFeedAd = new kony.ui.Image2({
        "height": "44dp",
        "id": "imgInFeedAd",
        "imageWhenFailed": "failedimagered.png",
        "imageWhileDownloading": "infeed_ph.png",
        "isVisible": true,
        "left": "0dp",
        "onDownloadComplete": AS_Image_a961263116d94e258e623369f0750d54,
        "skin": "slImage",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnInFeedAdClose = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknCloseBtndm",
        "height": "14dp",
        "id": "btnInFeedAdClose",
        "isVisible": true,
        "onClick": AS_Button_d06f9b340950432b9e8798cd1de95a43,
        "right": "0dp",
        "skin": "sknCloseBtndm",
        "width": "18dp",
        "zIndex": 100
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxAdClickArea = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxAdClickArea",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onTouchStart": AS_FlexContainer_ade9cd0537cc409d919956409fb91d77,
        "right": "20dp",
        "skin": "slFbox",
        "top": "0dp",
        "zIndex": 100
    }, {}, {});
    flxAdClickArea.setDefaultUnit(kony.flex.DP);
    flxAdClickArea.add();
    flxInFeedAds.add(imgInFeedAd, btnInFeedAdClose, flxAdClickArea);
    var flxDeals = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": false,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxDeals",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknflxDeals",
        "top": "0%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDeals.setDefaultUnit(kony.flex.DP);
    var dealsOuterScroll = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": false,
        "height": "68%",
        "horizontalScrollIndicator": true,
        "id": "dealsOuterScroll",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknslFSbox",
        "top": "19%",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    dealsOuterScroll.setDefaultUnit(kony.flex.DP);
    var dealsWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": false,
        "id": "dealsWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    dealsWrapper.setDefaultUnit(kony.flex.DP);
    var dealsInnerScroll = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "dealsInnerScroll",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%"
    }, {}, {});
    dealsInnerScroll.setDefaultUnit(kony.flex.DP);
    var segAccountsKADeals = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "data": [{
            "CopylblIncommingRing0ac2c9e9c52d147": kony.i18n.getLocalizedString("i18n.common.reverseback"),
            "Label0bed5640cdb4b46": "Label",
            "amountAccount1": "00.00",
            "amountcurrBal": "$00.00",
            "amtOutsatndingBal": "$00.00",
            "btnNav": "L",
            "dummyAccountName": "",
            "dummyAccountNumber": "",
            "isPFMLabel": "",
            "lblAccNumber": "",
            "lblAccountID": "Label",
            "lblBankName": "",
            "lblColorKA": "",
            "lblCurrency": "JOD",
            "lblIncommingRing": "s",
            "lblMobile": "",
            "lblRowSeparator": "",
            "nameAccount1": "<p>t</p><img align = \"left\" src=\"mapblue.png\" />\n                        ",
            "nameAccount12": "Account Name",
            "typeAccount": "Available Balance",
            "typeKA": ""
        }, {
            "CopylblIncommingRing0ac2c9e9c52d147": kony.i18n.getLocalizedString("i18n.common.reverseback"),
            "Label0bed5640cdb4b46": "Label",
            "amountAccount1": "00.00",
            "amountcurrBal": "$00.00",
            "amtOutsatndingBal": "$00.00",
            "btnNav": "L",
            "dummyAccountName": "",
            "dummyAccountNumber": "",
            "isPFMLabel": "",
            "lblAccNumber": "",
            "lblAccountID": "Label",
            "lblBankName": "",
            "lblColorKA": "",
            "lblCurrency": "JOD",
            "lblIncommingRing": "s",
            "lblMobile": "",
            "lblRowSeparator": "",
            "nameAccount1": "<p>t</p><img align = \"left\" src=\"mapblue.png\" />\n                        ",
            "nameAccount12": "Account Name",
            "typeAccount": "Available Balance",
            "typeKA": ""
        }],
        "groupCells": false,
        "id": "segAccountsKADeals",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_jd5e2be810884ca6a3cccb6777153212,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowSkin": "sknsegAcc",
        "rowTemplate": yourAccount1,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff4b",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": true,
        "top": "0%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopylblIncommingRing0ac2c9e9c52d147": "CopylblIncommingRing0ac2c9e9c52d147",
            "Label0bed5640cdb4b46": "Label0bed5640cdb4b46",
            "amountAccount1": "amountAccount1",
            "amountcurrBal": "amountcurrBal",
            "amtOutsatndingBal": "amtOutsatndingBal",
            "btnNav": "btnNav",
            "btnNav1": "btnNav1",
            "dummyAccountName": "dummyAccountName",
            "dummyAccountNumber": "dummyAccountNumber",
            "flxLine": "flxLine",
            "flxSet": "flxSet",
            "flxamount": "flxamount",
            "isPFMLabel": "isPFMLabel",
            "lblAccNumber": "lblAccNumber",
            "lblAccountID": "lblAccountID",
            "lblBankName": "lblBankName",
            "lblColorKA": "lblColorKA",
            "lblCurrency": "lblCurrency",
            "lblIncommingRing": "lblIncommingRing",
            "lblMobile": "lblMobile",
            "lblRowSeparator": "lblRowSeparator",
            "nameAccount1": "nameAccount1",
            "nameAccount12": "nameAccount12",
            "nameContainer": "nameContainer",
            "typeAccount": "typeAccount",
            "typeKA": "typeKA",
            "yourAccount1": "yourAccount1"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var LabelNoRecordsKADeals = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "15.50%",
        "id": "LabelNoRecordsKADeals",
        "isVisible": false,
        "left": "17.52%",
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.alerts.NoDepositsMsg"),
        "top": "20dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    dealsInnerScroll.add(segAccountsKADeals, LabelNoRecordsKADeals);
    dealsWrapper.add(dealsInnerScroll);
    var CopyandroidInsightsCover0ae6415e4664a40 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyandroidInsightsCover0ae6415e4664a40",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntitleBarGradient",
        "top": "43dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyandroidInsightsCover0ae6415e4664a40.setDefaultUnit(kony.flex.DP);
    CopyandroidInsightsCover0ae6415e4664a40.add();
    dealsOuterScroll.add(dealsWrapper, CopyandroidInsightsCover0ae6415e4664a40);
    var accountLabelScroll1 = new kony.ui.Label({
        "height": "50dp",
        "id": "accountLabelScroll1",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknCopysecondaryHeader07cfb9398604f4f",
        "text": kony.i18n.getLocalizedString("i18n.overview.yourAccount"),
        "top": "136dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var insightsButton1 = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknsecondaryActionReversedFocus",
        "height": "42dp",
        "id": "insightsButton1",
        "isVisible": false,
        "onClick": AS_Button_d999d741b4184e569d154ccaef2663fb,
        "skin": "sknsecondaryActionReversed",
        "text": kony.i18n.getLocalizedString("i18n.overview.hideAccountInsights"),
        "top": "130dp",
        "width": "250dp",
        "zIndex": 999
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var cashCreditOverview1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "64dp",
        "id": "cashCreditOverview1",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5.00%",
        "skin": "sknslFbox",
        "top": "15%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    cashCreditOverview1.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0c615cc90439543 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35dp",
        "id": "CopyFlexContainer0c615cc90439543",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0c615cc90439543.setDefaultUnit(kony.flex.DP);
    var CopyLabel0a62b37b061b54e = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopyLabel0a62b37b061b54e",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLatoRegularFFFFFF107KA",
        "text": kony.i18n.getLocalizedString("i18n.overview.cash"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopycashOverviewLabel0ebf9837fd2154c = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopycashOverviewLabel0ebf9837fd2154c",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknCopyslLabel03fbcc1555fb34b",
        "text": "7568.54",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0c615cc90439543.add(CopyLabel0a62b37b061b54e, CopycashOverviewLabel0ebf9837fd2154c);
    var CopyFlexContainer0c4b56a1cecb341 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35dp",
        "id": "CopyFlexContainer0c4b56a1cecb341",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": 0,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0c4b56a1cecb341.setDefaultUnit(kony.flex.DP);
    var CopyLabel0gf7b9e5b85a440 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopyLabel0gf7b9e5b85a440",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLatoRegularFFFFFF107KA",
        "text": kony.i18n.getLocalizedString("i18n.overview.creditDebt"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopycreditDebtOverviewLabel0a185ef0fc5874d = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopycreditDebtOverviewLabel0a185ef0fc5874d",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknCopyslLabel043dabd91ff4847",
        "text": "- 1529.54",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0c4b56a1cecb341.add(CopyLabel0gf7b9e5b85a440, CopycreditDebtOverviewLabel0a185ef0fc5874d);
    var CopycashCreditOverviewBorderBottom0a77a2b76af2b45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "50%",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopycashCreditOverviewBorderBottom0a77a2b76af2b45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknwhiteDivider50",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    CopycashCreditOverviewBorderBottom0a77a2b76af2b45.setDefaultUnit(kony.flex.DP);
    CopycashCreditOverviewBorderBottom0a77a2b76af2b45.add();
    cashCreditOverview1.add(CopyFlexContainer0c615cc90439543, CopyFlexContainer0c4b56a1cecb341, CopycashCreditOverviewBorderBottom0a77a2b76af2b45);
    var flxTabDeals = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxTabDeals",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "7%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxTabDeals.setDefaultUnit(kony.flex.DP);
    var btnAccounts1 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnLoans",
        "height": "100%",
        "id": "btnAccounts1",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_ed3ea6423c374f07aa96c2805f82658a,
        "skin": "btnLoans",
        "text": kony.i18n.getLocalizedString("i18n.accounts.accounts"),
        "top": "0dp",
        "width": "33.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnDeals1 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnAccounts",
        "height": "100%",
        "id": "btnDeals1",
        "isVisible": true,
        "left": "33.30%",
        "skin": "btnAccounts",
        "text": kony.i18n.getLocalizedString("i18n.accounts.deals"),
        "top": "0dp",
        "width": "33.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnLoans1 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnLoans",
        "height": "100%",
        "id": "btnLoans1",
        "isVisible": true,
        "left": "66.60%",
        "onClick": AS_Button_fc9a052b4171486e92240db2f2cbe8d6,
        "right": "0%",
        "skin": "btnLoans",
        "text": kony.i18n.getLocalizedString("i18n.accounts.loans"),
        "top": "0dp",
        "width": "33.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTabDeals.add(btnAccounts1, btnDeals1, btnLoans1);
    var flxNewDepositButton = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "13%",
        "id": "flxNewDepositButton",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "87%",
        "width": "100%",
        "zIndex": 999
    }, {}, {});
    flxNewDepositButton.setDefaultUnit(kony.flex.DP);
    var btnNewDeposit = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "sknprimaryActionFcs",
        "height": "80%",
        "id": "btnNewDeposit",
        "isVisible": true,
        "skin": "sknprimaryAction",
        "width": "80%",
        "zIndex": 999
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxNewDepositButton.add(btnNewDeposit);
    var Button0cff661d9151644 = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "50dp",
        "id": "Button0cff661d9151644",
        "isVisible": false,
        "left": "46dp",
        "onClick": AS_Button_bb7e3a6b233a404a97f8e15a06c5b6cb,
        "skin": "sknbtnwhiteBGBlue",
        "text": "Open Deposite",
        "top": "504dp",
        "width": "260dp",
        "zIndex": 999
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxDeals.add(dealsOuterScroll, accountLabelScroll1, insightsButton1, cashCreditOverview1, flxTabDeals, flxNewDepositButton, Button0cff661d9151644);
    var flxLoans = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": false,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxLoans",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknflxLoans",
        "top": "0%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLoans.setDefaultUnit(kony.flex.DP);
    var flxLoansOuterScroll = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "id": "flxLoansOuterScroll",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknslFSbox",
        "top": "19%",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLoansOuterScroll.setDefaultUnit(kony.flex.DP);
    var flxDealsWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": false,
        "id": "flxDealsWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxDealsWrapper.setDefaultUnit(kony.flex.DP);
    var LoansInnerScroll = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "LoansInnerScroll",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%"
    }, {}, {});
    LoansInnerScroll.setDefaultUnit(kony.flex.DP);
    var segAccountsKALoans = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "data": [{
            "CopylblIncommingRing0ac2c9e9c52d147": kony.i18n.getLocalizedString("i18n.common.reverseback"),
            "Label0bed5640cdb4b46": "Label",
            "amountAccount1": "00.00",
            "amountcurrBal": "$00.00",
            "amtOutsatndingBal": "$00.00",
            "btnNav": "L",
            "dummyAccountName": "",
            "dummyAccountNumber": "",
            "isPFMLabel": "",
            "lblAccNumber": "",
            "lblAccountID": "Label",
            "lblBankName": "",
            "lblColorKA": "",
            "lblCurrency": "JOD",
            "lblIncommingRing": "s",
            "lblMobile": "",
            "lblRowSeparator": "",
            "nameAccount1": "<p>t</p><img align = \"left\" src=\"mapblue.png\" />\n                        ",
            "nameAccount12": "Account Name",
            "typeAccount": "Available Balance",
            "typeKA": ""
        }, {
            "CopylblIncommingRing0ac2c9e9c52d147": kony.i18n.getLocalizedString("i18n.common.reverseback"),
            "Label0bed5640cdb4b46": "Label",
            "amountAccount1": "00.00",
            "amountcurrBal": "$00.00",
            "amtOutsatndingBal": "$00.00",
            "btnNav": "L",
            "dummyAccountName": "",
            "dummyAccountNumber": "",
            "isPFMLabel": "",
            "lblAccNumber": "",
            "lblAccountID": "Label",
            "lblBankName": "",
            "lblColorKA": "",
            "lblCurrency": "JOD",
            "lblIncommingRing": "s",
            "lblMobile": "",
            "lblRowSeparator": "",
            "nameAccount1": "<p>t</p><img align = \"left\" src=\"mapblue.png\" />\n                        ",
            "nameAccount12": "Account Name",
            "typeAccount": "Available Balance",
            "typeKA": ""
        }],
        "groupCells": false,
        "id": "segAccountsKALoans",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_b74d529298004272bb5ee301dcb70273,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowSkin": "sknsegAcc",
        "rowTemplate": yourAccount1,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff4b",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": true,
        "top": "0%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopylblIncommingRing0ac2c9e9c52d147": "CopylblIncommingRing0ac2c9e9c52d147",
            "Label0bed5640cdb4b46": "Label0bed5640cdb4b46",
            "amountAccount1": "amountAccount1",
            "amountcurrBal": "amountcurrBal",
            "amtOutsatndingBal": "amtOutsatndingBal",
            "btnNav": "btnNav",
            "btnNav1": "btnNav1",
            "dummyAccountName": "dummyAccountName",
            "dummyAccountNumber": "dummyAccountNumber",
            "flxLine": "flxLine",
            "flxSet": "flxSet",
            "flxamount": "flxamount",
            "isPFMLabel": "isPFMLabel",
            "lblAccNumber": "lblAccNumber",
            "lblAccountID": "lblAccountID",
            "lblBankName": "lblBankName",
            "lblColorKA": "lblColorKA",
            "lblCurrency": "lblCurrency",
            "lblIncommingRing": "lblIncommingRing",
            "lblMobile": "lblMobile",
            "lblRowSeparator": "lblRowSeparator",
            "nameAccount1": "nameAccount1",
            "nameAccount12": "nameAccount12",
            "nameContainer": "nameContainer",
            "typeAccount": "typeAccount",
            "typeKA": "typeKA",
            "yourAccount1": "yourAccount1"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var LabelNoRecordsKALoans = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "15.50%",
        "id": "LabelNoRecordsKALoans",
        "isVisible": false,
        "left": "17.52%",
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.alerts.NoLoansMsg"),
        "top": "20dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    LoansInnerScroll.add(segAccountsKALoans, LabelNoRecordsKALoans);
    flxDealsWrapper.add(LoansInnerScroll);
    var androidInsightsCoverLoans = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidInsightsCoverLoans",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntitleBarGradient",
        "top": "43dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidInsightsCoverLoans.setDefaultUnit(kony.flex.DP);
    androidInsightsCoverLoans.add();
    flxLoansOuterScroll.add(flxDealsWrapper, androidInsightsCoverLoans);
    var LoansLabelScroll = new kony.ui.Label({
        "height": "50dp",
        "id": "LoansLabelScroll",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknCopysecondaryHeader07cfb9398604f4f",
        "text": kony.i18n.getLocalizedString("i18n.overview.yourAccount"),
        "top": "136dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Loaninsights = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknsecondaryActionReversedFocus",
        "height": "42dp",
        "id": "Loaninsights",
        "isVisible": false,
        "onClick": AS_Button_hc68c00966a24298a3e4207f3d1b9882,
        "skin": "sknsecondaryActionReversed",
        "text": kony.i18n.getLocalizedString("i18n.overview.hideAccountInsights"),
        "top": "130dp",
        "width": "250dp",
        "zIndex": 999
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var cashCreditOverviewLoan = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "64dp",
        "id": "cashCreditOverviewLoan",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5.00%",
        "skin": "sknslFbox",
        "top": "15%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    cashCreditOverviewLoan.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0hdf9779a9ce947 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35dp",
        "id": "CopyFlexContainer0hdf9779a9ce947",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0hdf9779a9ce947.setDefaultUnit(kony.flex.DP);
    var CopyLabel0a71b3069d29844 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopyLabel0a71b3069d29844",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLatoRegularFFFFFF107KA",
        "text": kony.i18n.getLocalizedString("i18n.overview.cash"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopycashOverviewLabel0j62493d5a8fa4a = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopycashOverviewLabel0j62493d5a8fa4a",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknCopyslLabel03fbcc1555fb34b",
        "text": "7568.54",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0hdf9779a9ce947.add(CopyLabel0a71b3069d29844, CopycashOverviewLabel0j62493d5a8fa4a);
    var CopyFlexContainer0dc4825dbec1c4c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35dp",
        "id": "CopyFlexContainer0dc4825dbec1c4c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": 0,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0dc4825dbec1c4c.setDefaultUnit(kony.flex.DP);
    var CopyLabel0d455b4dcd25e4c = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopyLabel0d455b4dcd25e4c",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLatoRegularFFFFFF107KA",
        "text": kony.i18n.getLocalizedString("i18n.overview.creditDebt"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopycreditDebtOverviewLabel0ba87acc1551f41 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopycreditDebtOverviewLabel0ba87acc1551f41",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknCopyslLabel043dabd91ff4847",
        "text": "- 1529.54",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0dc4825dbec1c4c.add(CopyLabel0d455b4dcd25e4c, CopycreditDebtOverviewLabel0ba87acc1551f41);
    var CopycashCreditOverviewBorderBottom0e0599b6a0da347 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "50%",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopycashCreditOverviewBorderBottom0e0599b6a0da347",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknwhiteDivider50",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    CopycashCreditOverviewBorderBottom0e0599b6a0da347.setDefaultUnit(kony.flex.DP);
    CopycashCreditOverviewBorderBottom0e0599b6a0da347.add();
    cashCreditOverviewLoan.add(CopyFlexContainer0hdf9779a9ce947, CopyFlexContainer0dc4825dbec1c4c, CopycashCreditOverviewBorderBottom0e0599b6a0da347);
    var flxTabLoans = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxTabLoans",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "7%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    flxTabLoans.setDefaultUnit(kony.flex.DP);
    var btnAccounts2 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnLoans",
        "height": "100%",
        "id": "btnAccounts2",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_a8b0f7792173482981b93c0dad10f950,
        "skin": "btnLoans",
        "text": kony.i18n.getLocalizedString("i18n.accounts.accounts"),
        "top": "0dp",
        "width": "33.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnDeals2 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnLoans",
        "height": "100%",
        "id": "btnDeals2",
        "isVisible": true,
        "left": "33.30%",
        "onClick": AS_Button_c1b983d863754d019ac8a4b4569e790d,
        "skin": "btnLoans",
        "text": kony.i18n.getLocalizedString("i18n.accounts.deals"),
        "top": "0dp",
        "width": "33.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnLoans2 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnAccounts",
        "height": "100%",
        "id": "btnLoans2",
        "isVisible": true,
        "left": "66.60%",
        "right": "0%",
        "skin": "btnAccounts",
        "text": kony.i18n.getLocalizedString("i18n.accounts.loans"),
        "top": "0dp",
        "width": "33.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTabLoans.add(btnAccounts2, btnDeals2, btnLoans2);
    flxLoans.add(flxLoansOuterScroll, LoansLabelScroll, Loaninsights, cashCreditOverviewLoan, flxTabLoans);
    var flxHeaderDashboard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeaderDashboard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10000
    }, {}, {});
    flxHeaderDashboard.setDefaultUnit(kony.flex.DP);
    var FlexContainer0b9fde1e61d3c44 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlexContainer0b9fde1e61d3c44",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100dp",
        "zIndex": 1
    }, {}, {});
    FlexContainer0b9fde1e61d3c44.setDefaultUnit(kony.flex.DP);
    var btnenvelop = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "50dp",
        "id": "btnenvelop",
        "isVisible": false,
        "left": "2dp",
        "skin": "btnUser",
        "text": "Y",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnProfile = new kony.ui.Button({
        "focusSkin": "btnUser",
        "height": "50dp",
        "id": "btnProfile",
        "isVisible": true,
        "left": "54dp",
        "onClick": AS_Button_g32d485b04f74ff6951c395c3d1ae80d,
        "skin": "btnUser",
        "text": "F",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer0b9fde1e61d3c44.add(btnenvelop, btnProfile);
    var Image0c5157670502748 = new kony.ui.Image2({
        "height": "30dp",
        "id": "Image0c5157670502748",
        "isVisible": false,
        "left": "15dp",
        "skin": "slImage",
        "src": "logo03.png",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderDashboard.add(FlexContainer0b9fde1e61d3c44, Image0c5157670502748);
    var flxIconMove1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxIconMove1",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "43%",
        "onClick": AS_FlexContainer_i6486c54c9bb41d0a39b155deee1b96d,
        "skin": "slFbox",
        "top": "85%",
        "width": "60dp",
        "zIndex": 1000
    }, {}, {});
    flxIconMove1.setDefaultUnit(kony.flex.DP);
    var chatbotIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "150%",
        "id": "chatbotIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "chatbotico2.png",
        "width": "150%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxIconMove1.add(chatbotIcon);
    frmAccountsLandingKA.add(flxAccountsMain, flxAccntLandingNavOptKA, showInsightsButton, overview, flxInFeedAds, flxDeals, flxLoans, flxHeaderDashboard, flxIconMove1);
};

function frmAccountsLandingKAGlobals() {
    frmAccountsLandingKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmAccountsLandingKA,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "footers": [footerBack],
        "id": "frmAccountsLandingKA",
        "init": AS_Form_d86ca862bda14477aaba29a1f8d8cb5b,
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_cc5efc6acd8048749dd69ade1e35dc5e,
        "skin": "Copysknback0e58b5b4e61874e"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_b5f9875ded654d4cb142ec256a46a096,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};