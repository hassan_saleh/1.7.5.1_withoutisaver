function addWidgetsfrmEditPayeeSuccessKA() {
    frmEditPayeeSuccessKA.setDefaultUnit(kony.flex.DP);
    var flxContainerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxContainerKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "60dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContainerKA.setDefaultUnit(kony.flex.DP);
    var lblFormTitleKA = new kony.ui.Label({
        "id": "lblFormTitleKA",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknonboardingHeader",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.editPayee"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxSuccessIconContainerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "30dp",
        "id": "flxSuccessIconContainerKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknsuccessIcon",
        "top": "40dp",
        "width": "30dp",
        "zIndex": 1
    }, {}, {});
    flxSuccessIconContainerKA.setDefaultUnit(kony.flex.DP);
    var imgSuccessIconKA = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "50%",
        "id": "imgSuccessIconKA",
        "isVisible": true,
        "skin": "sknslImage",
        "src": "check.png",
        "width": "50%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSuccessIconContainerKA.add(imgSuccessIconKA);
    var lblSuccessTitleKA = new kony.ui.Label({
        "id": "lblSuccessTitleKA",
        "isVisible": true,
        "left": "8dp",
        "skin": "sknonboardingHeader",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeUpdatedSuccessfully"),
        "top": "7dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSuccessNotesKA = new kony.ui.Label({
        "centerX": "50.03%",
        "id": "lblSuccessNotesKA",
        "isVisible": true,
        "left": "20%",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.updateThePayeeSuccess"),
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnPayBIllKA = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "btnPayBIllKA",
        "isVisible": true,
        "onClick": AS_Button_609c697c73884085b7bd5236f879ae05,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.btnPayBill"),
        "top": "60dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnGoBackKA = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "42dp",
        "id": "btnGoBackKA",
        "isVisible": true,
        "onClick": AS__01402cb350b74188b4770eff708ef64b,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.noGoBackToPayeelist"),
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxContainerKA.add(lblFormTitleKA, flxSuccessIconContainerKA, lblSuccessTitleKA, lblSuccessNotesKA, btnPayBIllKA, btnGoBackKA);
    frmEditPayeeSuccessKA.add(flxContainerKA);
};

function frmEditPayeeSuccessKAGlobals() {
    frmEditPayeeSuccessKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmEditPayeeSuccessKA,
        "enabledForIdleTimeout": true,
        "id": "frmEditPayeeSuccessKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "inTransitionConfig": {
            "formAnimation": 3
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};