function addWidgetsfrmDeviceRegisterationIncorrectPinActicvationKA() {
    frmDeviceRegisterationIncorrectPinActicvationKA.setDefaultUnit(kony.flex.DP);
    var touchFeature = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "touchFeature",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    touchFeature.setDefaultUnit(kony.flex.DP);
    var FlexContainer00a3c07fadcbf4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer00a3c07fadcbf4b",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "60dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer00a3c07fadcbf4b.setDefaultUnit(kony.flex.DP);
    var Label07b268452752c4c = new kony.ui.Label({
        "id": "Label07b268452752c4c",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknonboardingHeader",
        "text": kony.i18n.getLocalizedString("i18n.login.deviceRegisteration"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0d00f815bcc7c41 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0d00f815bcc7c41",
        "isVisible": true,
        "left": "20%",
        "skin": "sknonboardingText",
        "text": kony.i18n.getLocalizedString("i18n.common.activationPinSms"),
        "top": "10dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0d45989a2f48a4c = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0d45989a2f48a4c",
        "isVisible": true,
        "left": "20%",
        "skin": "sknonboardingText",
        "text": kony.i18n.getLocalizedString("i18n.login.pleaseEnterItHere"),
        "top": "10dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0e43f2a78d82342 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0e43f2a78d82342",
        "isVisible": true,
        "left": "20%",
        "skin": "sknErrorMessageEC223BKA",
        "text": kony.i18n.getLocalizedString("i18n.login.incorrectPinEntered"),
        "top": "40dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var TextField068616d32749e45 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50.03%",
        "focusSkin": "sknPinActivationborderEDEDEDKA",
        "height": "40dp",
        "id": "TextField068616d32749e45",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.login.enterPINherePlh"),
        "secureTextEntry": false,
        "skin": "sknPinActivationborderEDEDEDKA",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknPlaceholderKA",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var rememberContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "40dp",
        "id": "rememberContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknslFbox",
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    rememberContainer.setDefaultUnit(kony.flex.DP);
    var androidRememberMe = new kony.ui.CheckBoxGroup({
        "centerX": "50%",
        "centerY": "50%",
        "height": "25dp",
        "id": "androidRememberMe",
        "isVisible": true,
        "left": "0dp",
        "masterData": [
            ["remember", kony.i18n.getLocalizedString("i18n.login.rememberMe")]
        ],
        "selectedKeyValues": [
            ["remember", "Remember Me", "i18n.login.rememberMe"]
        ],
        "selectedKeys": ["remember"],
        "skin": "sknCopyslCheckBoxGroup01d67e589138b44",
        "top": "7dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
        "padding": [4, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "tickedImage": "checkbox_selected.png",
        "untickedImage": "checkbox_unselected.png"
    });
    rememberContainer.add(androidRememberMe);
    var enableTouchID = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "enableTouchID",
        "isVisible": true,
        "onClick": AS_Button_76167cfe2b944c2fb37a7dc478960f6f,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.login.cRegister"),
        "top": "60dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var noThanks = new kony.ui.Button({
        "centerX": "50.03%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "35dp",
        "id": "noThanks",
        "isVisible": true,
        "onClick": AS_Button_78bed686e92749438c003ca03526e6aa,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.noThanks"),
        "top": "4dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer00a3c07fadcbf4b.add(Label07b268452752c4c, CopyLabel0d00f815bcc7c41, CopyLabel0d45989a2f48a4c, CopyLabel0e43f2a78d82342, TextField068616d32749e45, rememberContainer, enableTouchID, noThanks);
    touchFeature.add(FlexContainer00a3c07fadcbf4b);
    frmDeviceRegisterationIncorrectPinActicvationKA.add(touchFeature);
};

function frmDeviceRegisterationIncorrectPinActicvationKAGlobals() {
    frmDeviceRegisterationIncorrectPinActicvationKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmDeviceRegisterationIncorrectPinActicvationKA,
        "bounces": false,
        "enableScrolling": true,
        "enabledForIdleTimeout": true,
        "id": "frmDeviceRegisterationIncorrectPinActicvationKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "pagingEnabled": false,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_be847dc6fb8d44079c82807b48457756,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};