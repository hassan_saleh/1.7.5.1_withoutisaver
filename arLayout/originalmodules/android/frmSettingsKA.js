function addWidgetsfrmSettingsKA() {
    frmSettingsKA.setDefaultUnit(kony.flex.DP);
    var flxSettingsMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxSettingsMain",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSettingsMain.setDefaultUnit(kony.flex.DP);
    flxSettingsMain.add();
    var flxSettingsHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxSettingsHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSettingsHeader.setDefaultUnit(kony.flex.DP);
    var lblSettingsTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "45%",
        "id": "lblSettingsTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.settings.settings"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnBack = new kony.ui.Button({
        "centerY": "45%",
        "focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
        "height": "50%",
        "id": "btnBack",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_c4427b68dee74ca6acda42f8bc798d89,
        "skin": "CopyslButtonGlossBlue0e73a02c4810645",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0.00%",
        "width": "20%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopylblSettingsTitle0i1abba96b5a748 = new kony.ui.Label({
        "centerX": "12%",
        "centerY": "45%",
        "id": "CopylblSettingsTitle0i1abba96b5a748",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSettingsHeader.add(lblSettingsTitle, btnBack, CopylblSettingsTitle0i1abba96b5a748);
    var flxMenuContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": false,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "91%",
        "horizontalScrollIndicator": true,
        "id": "flxMenuContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "9%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMenuContainer.setDefaultUnit(kony.flex.DP);
    var flxProfile = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxProfile",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_bc936614b03b4938bebc5d52f232e8fe,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProfile.setDefaultUnit(kony.flex.DP);
    var lblProfile = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblProfile",
        "isVisible": true,
        "left": "15dp",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.Profile"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblProfileRightIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblProfileRightIcon",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj145",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxProfile.add(lblProfile, lblProfileRightIcon);
    var flxMyProfile = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxMyProfile",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_a58e6f56b73946a8895d63fb80aa38e4,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMyProfile.setDefaultUnit(kony.flex.DP);
    var lblMyProfile = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblMyProfile",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18,appsettings.myprofile"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMyProfileRightIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblMyProfileRightIcon",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMyProfile.add(lblMyProfile, lblMyProfileRightIcon);
    var flxChangeUsername = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxChangeUsername",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_eb3dd40af05f4cf4b990ea19f4782bf2,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxChangeUsername.setDefaultUnit(kony.flex.DP);
    var CopylblMyProfile0d9f2585ac02343 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyProfile0d9f2585ac02343",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18,appsettings.changeUsername"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblMyProfileRightIcon0faf1c6beb1964b = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblMyProfileRightIcon0faf1c6beb1964b",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxChangeUsername.add(CopylblMyProfile0d9f2585ac02343, CopylblMyProfileRightIcon0faf1c6beb1964b);
    var flxChangePassword = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxChangePassword",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_c17c8946ef2845008ba1bfaa51afc536,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxChangePassword.setDefaultUnit(kony.flex.DP);
    var CopylblMyProfile0bd2d2b07421d4f = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyProfile0bd2d2b07421d4f",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18,appsettings.changePassword"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblMyProfileRightIcon0j2445197354046 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblMyProfileRightIcon0j2445197354046",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxChangePassword.add(CopylblMyProfile0bd2d2b07421d4f, CopylblMyProfileRightIcon0j2445197354046);
    var flxMyRegisteredDevices = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxMyRegisteredDevices",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_ab9b2f393410413b90fd9e563c48b346,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMyRegisteredDevices.setDefaultUnit(kony.flex.DP);
    var CopylblMyProfile0i558bec5e7ec40 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyProfile0i558bec5e7ec40",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.RegisteredDevices"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblMyProfileRightIcon0b949b8200a1e46 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblMyProfileRightIcon0b949b8200a1e46",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMyRegisteredDevices.add(CopylblMyProfile0i558bec5e7ec40, CopylblMyProfileRightIcon0b949b8200a1e46);
    var flxTnC1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxTnC1",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_fbd5fcd82db44be38e4251d2b3d65bae,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxTnC1.setDefaultUnit(kony.flex.DP);
    var lblTnC = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblTnC",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.common.TncForAppmenu"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblMyProfileRightIcon0f4a4357cca2042 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblMyProfileRightIcon0f4a4357cca2042",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTnC1.add(lblTnC, CopylblMyProfileRightIcon0f4a4357cca2042);
    var flxseperator = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxseperator",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "CopyslFbox0ab42d5f22ae345",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxseperator.setDefaultUnit(kony.flex.DP);
    flxseperator.add();
    var flxAlternateLogins = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxAlternateLogins",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_b6998ef669d345dc9b459ff09b4f1be0,
        "skin": "slFbox",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAlternateLogins.setDefaultUnit(kony.flex.DP);
    var CopylblMyProfile0a15dcafb1a8d43 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyProfile0a15dcafb1a8d43",
        "isVisible": true,
        "left": "15dp",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.AlternateLogins"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAlternateLoginsRightIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblAlternateLoginsRightIcon",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj145",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAlternateLogins.add(CopylblMyProfile0a15dcafb1a8d43, lblAlternateLoginsRightIcon);
    var flxTouchId = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxTouchId",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_i62cbe6b9a234f538649fed7e8abb1bd,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTouchId.setDefaultUnit(kony.flex.DP);
    var lblTouchID = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblTouchID",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.TouchID"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblMyProfileRightIcon0c1b784f5a4dc4b = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblMyProfileRightIcon0c1b784f5a4dc4b",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxHide = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "0dp",
        "id": "flxHide",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "5dp",
        "width": "0%",
        "zIndex": 1
    }, {}, {});
    flxHide.setDefaultUnit(kony.flex.DP);
    var flxTouchIDSwitchOff = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxTouchIDSwitchOff",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknflxGrey",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxTouchIDSwitchOff.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0ed98162cb87345 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0ed98162cb87345",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlueOff0ed98162cb87345.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0ed98162cb87345.add();
    var CopyflxNaveenbhaiKiLakeer0b879f5d5aba342 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0b879f5d5aba342",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 10,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0b879f5d5aba342.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0b879f5d5aba342.add();
    flxTouchIDSwitchOff.add(CopyflxRoundDBlueOff0ed98162cb87345, CopyflxNaveenbhaiKiLakeer0b879f5d5aba342);
    var flxTouchIDSwitchOn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxTouchIDSwitchOn",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxTouchIDSwitchOn.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0j40e19dd87274a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0j40e19dd87274a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0j40e19dd87274a.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0j40e19dd87274a.add();
    var Copyflxlakeer0e8f83b92988f43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0e8f83b92988f43",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0e8f83b92988f43.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0e8f83b92988f43.add();
    flxTouchIDSwitchOn.add(CopyflxRoundDBlue0j40e19dd87274a, Copyflxlakeer0e8f83b92988f43);
    flxHide.add(flxTouchIDSwitchOff, flxTouchIDSwitchOn);
    flxTouchId.add(lblTouchID, CopylblMyProfileRightIcon0c1b784f5a4dc4b, flxHide);
    var flxPINLogin = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxPINLogin",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_ddfca28b81aa4d7cb1a0e00929684612,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPINLogin.setDefaultUnit(kony.flex.DP);
    var lblPinLogin = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblPinLogin",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.PINLogin"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblMyProfileRightIcon0cb3744bc73264e = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblMyProfileRightIcon0cb3744bc73264e",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxHide1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "0dp",
        "id": "flxHide1",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {}, {});
    flxHide1.setDefaultUnit(kony.flex.DP);
    var flxPinLoginSwitchOn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxPinLoginSwitchOn",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxPinLoginSwitchOn.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0c93f137513d940 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0c93f137513d940",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0c93f137513d940.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0c93f137513d940.add();
    var Copyflxlakeer0ae6ebd8f9e144f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0ae6ebd8f9e144f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0ae6ebd8f9e144f.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0ae6ebd8f9e144f.add();
    flxPinLoginSwitchOn.add(CopyflxRoundDBlue0c93f137513d940, Copyflxlakeer0ae6ebd8f9e144f);
    var flxPinLoginSwitchOff = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxPinLoginSwitchOff",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknflxGrey",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxPinLoginSwitchOff.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0ce259f0b209649 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0ce259f0b209649",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlueOff0ce259f0b209649.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0ce259f0b209649.add();
    var CopyflxNaveenbhaiKiLakeer0ff457b2dd88144 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0ff457b2dd88144",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 10,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0ff457b2dd88144.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0ff457b2dd88144.add();
    flxPinLoginSwitchOff.add(CopyflxRoundDBlueOff0ce259f0b209649, CopyflxNaveenbhaiKiLakeer0ff457b2dd88144);
    flxHide1.add(flxPinLoginSwitchOn, flxPinLoginSwitchOff);
    flxPINLogin.add(lblPinLogin, CopylblMyProfileRightIcon0cb3744bc73264e, flxHide1);
    var flxseperator1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxseperator1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "1dp",
        "skin": "CopyslFbox0ab42d5f22ae345",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxseperator1.setDefaultUnit(kony.flex.DP);
    flxseperator1.add();
    var flxAlertandNotifications = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxAlertandNotifications",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_je98a67a040a40129944dd78dce5751a,
        "skin": "slFbox",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAlertandNotifications.setDefaultUnit(kony.flex.DP);
    var CopylblMyProfile0a194b295fbf249 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyProfile0a194b295fbf249",
        "isVisible": true,
        "left": "15dp",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.AlertNotifications"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAlertandNotificationsRightIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblAlertandNotificationsRightIcon",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj145",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAlertandNotifications.add(CopylblMyProfile0a194b295fbf249, lblAlertandNotificationsRightIcon);
    var flxSMS = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxSMS",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_g872d41c807c4baab298bfb72c903762,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxSMS.setDefaultUnit(kony.flex.DP);
    var lblSMS = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSMS",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.SMS"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblMyProfileRightIcon0f3c776891a734c = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblMyProfileRightIcon0f3c776891a734c",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxHidesms = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "0dp",
        "id": "flxHidesms",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {}, {});
    flxHidesms.setDefaultUnit(kony.flex.DP);
    var flxSMSSwitchOff = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSMSSwitchOff",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknflxGrey",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSMSSwitchOff.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0a5e3eddfada24b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0a5e3eddfada24b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlueOff0a5e3eddfada24b.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0a5e3eddfada24b.add();
    var CopyflxNaveenbhaiKiLakeer0cb72155c61834c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0cb72155c61834c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 10,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0cb72155c61834c.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0cb72155c61834c.add();
    flxSMSSwitchOff.add(CopyflxRoundDBlueOff0a5e3eddfada24b, CopyflxNaveenbhaiKiLakeer0cb72155c61834c);
    var flxSMSSwitchOn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSMSSwitchOn",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSMSSwitchOn.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0bbc73e3c0fb041 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0bbc73e3c0fb041",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0bbc73e3c0fb041.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0bbc73e3c0fb041.add();
    var Copyflxlakeer0h4a3ac20cf6948 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0h4a3ac20cf6948",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0h4a3ac20cf6948.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0h4a3ac20cf6948.add();
    flxSMSSwitchOn.add(CopyflxRoundDBlue0bbc73e3c0fb041, Copyflxlakeer0h4a3ac20cf6948);
    flxHidesms.add(flxSMSSwitchOff, flxSMSSwitchOn);
    flxSMS.add(lblSMS, CopylblMyProfileRightIcon0f3c776891a734c, flxHidesms);
    var flxAlerts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxAlerts",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_fb0334d3d737463d97b6965e95a2bbcb,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAlerts.setDefaultUnit(kony.flex.DP);
    var lblAlerts = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAlerts",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.Alerts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblMyProfileRightIcon0c7868ebc005247 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblMyProfileRightIcon0c7868ebc005247",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxHideAlerts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "0dp",
        "id": "flxHideAlerts",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {}, {});
    flxHideAlerts.setDefaultUnit(kony.flex.DP);
    var flxAlertsSwitchOff = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxAlertsSwitchOff",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknflxGrey",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxAlertsSwitchOff.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0bddc695f59cd40 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0bddc695f59cd40",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlueOff0bddc695f59cd40.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0bddc695f59cd40.add();
    var CopyflxNaveenbhaiKiLakeer0c2362d11257f43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0c2362d11257f43",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 10,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0c2362d11257f43.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0c2362d11257f43.add();
    flxAlertsSwitchOff.add(CopyflxRoundDBlueOff0bddc695f59cd40, CopyflxNaveenbhaiKiLakeer0c2362d11257f43);
    var flxAlertsSwitchOn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxAlertsSwitchOn",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxAlertsSwitchOn.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0bcf250d976ec41 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0bcf250d976ec41",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0bcf250d976ec41.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0bcf250d976ec41.add();
    var Copyflxlakeer0d0788fba2af04c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0d0788fba2af04c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0d0788fba2af04c.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0d0788fba2af04c.add();
    flxAlertsSwitchOn.add(CopyflxRoundDBlue0bcf250d976ec41, Copyflxlakeer0d0788fba2af04c);
    flxHideAlerts.add(flxAlertsSwitchOff, flxAlertsSwitchOn);
    flxAlerts.add(lblAlerts, CopylblMyProfileRightIcon0c7868ebc005247, flxHideAlerts);
    var flxAlertHistory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxAlertHistory",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_j237f1abd6494c2ca284693a683c55d4,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAlertHistory.setDefaultUnit(kony.flex.DP);
    var CopylblMyProfile0bff902a9249c4b = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyProfile0bff902a9249c4b",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.AlertHistory"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblProfileRightIcon0a317786c1fc142 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblProfileRightIcon0a317786c1fc142",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAlertHistory.add(CopylblMyProfile0bff902a9249c4b, CopylblProfileRightIcon0a317786c1fc142);
    var flxseperator2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxseperator2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "CopyslFbox0ab42d5f22ae345",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxseperator2.setDefaultUnit(kony.flex.DP);
    flxseperator2.add();
    var flxAccountPreferences = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxAccountPreferences",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_h9df978e54a74b12b4abd1ec2bda1942,
        "skin": "slFbox",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccountPreferences.setDefaultUnit(kony.flex.DP);
    var lblAccountPreferences = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAccountPreferences",
        "isVisible": true,
        "left": "15dp",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.AccountPreferences"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountPreferencesRightIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblAccountPreferencesRightIcon",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj145",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAccountPreferences.add(lblAccountPreferences, lblAccountPreferencesRightIcon);
    var flxQuickBalance = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxQuickBalance",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_a48f8de72f8d43778d38b42590c24dda,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxQuickBalance.setDefaultUnit(kony.flex.DP);
    var lblQuickBalance = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblQuickBalance",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.QuickBalance"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblMyProfileRightIcon0dbe4d02cceae40 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblMyProfileRightIcon0dbe4d02cceae40",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxHideQUick = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "0dp",
        "id": "flxHideQUick",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {}, {});
    flxHideQUick.setDefaultUnit(kony.flex.DP);
    var flxQuickBalSwitchOn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxQuickBalSwitchOn",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxQuickBalSwitchOn.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0b8d972a39ebb4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0b8d972a39ebb4b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0b8d972a39ebb4b.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0b8d972a39ebb4b.add();
    var Copyflxlakeer0c1530db2a1094c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0c1530db2a1094c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0c1530db2a1094c.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0c1530db2a1094c.add();
    flxQuickBalSwitchOn.add(CopyflxRoundDBlue0b8d972a39ebb4b, Copyflxlakeer0c1530db2a1094c);
    var flxQuickBalSwitchOff = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxQuickBalSwitchOff",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknflxGrey",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxQuickBalSwitchOff.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0db45d0a10d494c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0db45d0a10d494c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlueOff0db45d0a10d494c.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0db45d0a10d494c.add();
    var CopyflxNaveenbhaiKiLakeer0acd2967e78374d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0acd2967e78374d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 10,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0acd2967e78374d.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0acd2967e78374d.add();
    flxQuickBalSwitchOff.add(CopyflxRoundDBlueOff0db45d0a10d494c, CopyflxNaveenbhaiKiLakeer0acd2967e78374d);
    flxHideQUick.add(flxQuickBalSwitchOn, flxQuickBalSwitchOff);
    flxQuickBalance.add(lblQuickBalance, CopylblMyProfileRightIcon0dbe4d02cceae40, flxHideQUick);
    var flxMyAccountSettings = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxMyAccountSettings",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_ab13365a755a4254b8d2ee2a539ed6a5,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMyAccountSettings.setDefaultUnit(kony.flex.DP);
    var CopylblMyProfile0a0fb56c006fd42 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyProfile0a0fb56c006fd42",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.MyAccountSettings"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblProfileRightIcon0c03b636d78c445 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblProfileRightIcon0c03b636d78c445",
        "isVisible": true,
        "right": "5%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMyAccountSettings.add(CopylblMyProfile0a0fb56c006fd42, CopylblProfileRightIcon0c03b636d78c445);
    var flxReorderMyAccountList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxReorderMyAccountList",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_cdf43a7658684e158f5fd25dd4aa7cd0,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxReorderMyAccountList.setDefaultUnit(kony.flex.DP);
    var CopylblMflxyProfile0b677b5ded60b42 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMflxyProfile0b677b5ded60b42",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.ReOrderMyAccountList"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblProfileRightIcon0hc08b56ab80148 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblProfileRightIcon0hc08b56ab80148",
        "isVisible": true,
        "right": "5%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxReorderMyAccountList.add(CopylblMflxyProfile0b677b5ded60b42, CopylblProfileRightIcon0hc08b56ab80148);
    var flxEstmtRegister = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxEstmtRegister",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_iff5da8f601d45a582d3a5f91f486faa,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxEstmtRegister.setDefaultUnit(kony.flex.DP);
    var lblestmtLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblestmtLabel",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.Estmt.settingstext"),
        "textStyle": {},
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblIcon",
        "isVisible": true,
        "right": "5%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEstmtRegister.add(lblestmtLabel, lblIcon);
    var flxNewSubAccounts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknflxDBlue6",
        "height": "10%",
        "id": "flxNewSubAccounts",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "onClick": AS_Button_c0d38695780f4d85be603425d748fd93,
        "skin": "sknflxDBlue6",
        "top": "0%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxNewSubAccounts.setDefaultUnit(kony.flex.DP);
    var lblNewSubAccountsTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNewSubAccountsTitle",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.common.newsubaccount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNewSubAccountsIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblNewSubAccountsIcon",
        "isVisible": true,
        "right": "5%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNewSubAccounts.add(lblNewSubAccountsTitle, lblNewSubAccountsIcon);
    var flxseperator3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxseperator3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "CopyslFbox0ab42d5f22ae345",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxseperator3.setDefaultUnit(kony.flex.DP);
    flxseperator3.add();
    var flxCardPreferences = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxCardPreferences",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_i4d000581cf04b1bac1a5e4ecf05bc13,
        "skin": "slFbox",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCardPreferences.setDefaultUnit(kony.flex.DP);
    var lblCardPreferences = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCardPreferences",
        "isVisible": true,
        "left": "15dp",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.cardsPreferences"),
        "textStyle": {},
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCardPreferencesRightIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblCardPreferencesRightIcon",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj145",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCardPreferences.add(lblCardPreferences, lblCardPreferencesRightIcon);
    var flxMyCardSettings = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxMyCardSettings",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_fccb24234e7546d18883a824e4129214,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMyCardSettings.setDefaultUnit(kony.flex.DP);
    var lblMyCardSettings = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblMyCardSettings",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.myCardsSettings"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblProfileRightIcon0b910d6dc6a0643 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblProfileRightIcon0b910d6dc6a0643",
        "isVisible": true,
        "right": "5%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMyCardSettings.add(lblMyCardSettings, CopylblProfileRightIcon0b910d6dc6a0643);
    var flxReorderMyCardList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxReorderMyCardList",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_cc4a9159a08746359fba9d334737c759,
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxReorderMyCardList.setDefaultUnit(kony.flex.DP);
    var CopylblMflxyProfile0g8455e2483fd4a = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMflxyProfile0g8455e2483fd4a",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.reordercardlist"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblProfileRightIcon0b626282bb61a4e = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblProfileRightIcon0b626282bb61a4e",
        "isVisible": true,
        "right": "5%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxReorderMyCardList.add(CopylblMflxyProfile0g8455e2483fd4a, CopylblProfileRightIcon0b626282bb61a4e);
    var Copyflxseperator0e28c9d7cee8f4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copyflxseperator0e28c9d7cee8f4b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "CopyslFbox0ab42d5f22ae345",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    Copyflxseperator0e28c9d7cee8f4b.setDefaultUnit(kony.flex.DP);
    Copyflxseperator0e28c9d7cee8f4b.add();
    var flxSiri = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxSiri",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_i3107ccad50343958f52ce52b5546d38,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSiri.setDefaultUnit(kony.flex.DP);
    var lblSiri = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSiri",
        "isVisible": true,
        "left": "15dp",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.Siri.enableDisable"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSiriRigh = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblSiriRigh",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj145",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSiri.add(lblSiri, lblSiriRigh);
    var flxseperator4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxseperator4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "CopyslFbox0ab42d5f22ae345",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxseperator4.setDefaultUnit(kony.flex.DP);
    flxseperator4.add();
    var flxChangeLanguage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxChangeLanguage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_c483defcef43434db5dcb6340fd2c16d,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxChangeLanguage.setDefaultUnit(kony.flex.DP);
    var lblChangeLanguage = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblChangeLanguage",
        "isVisible": true,
        "left": "15dp",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.ChangeLanguage"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblChangeLanguageRightIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblChangeLanguageRightIcon",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblBoj145",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxChangeLanguage.add(lblChangeLanguage, lblChangeLanguageRightIcon);
    var flxEnglish = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxEnglish",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxEnglish.setDefaultUnit(kony.flex.DP);
    var lblEnglish = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblEnglish",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": "English",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxEnglishSwitchOn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxEnglishSwitchOn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_b771b0c67bd94e5ea10e7834f5ef47d0,
        "right": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxEnglishSwitchOn.setDefaultUnit(kony.flex.DP);
    var flxRoundDBlue = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxRoundDBlue",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    flxRoundDBlue.setDefaultUnit(kony.flex.DP);
    flxRoundDBlue.add();
    var flxlakeer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "flxlakeer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    flxlakeer.setDefaultUnit(kony.flex.DP);
    flxlakeer.add();
    flxEnglishSwitchOn.add(flxRoundDBlue, flxlakeer);
    var flxEnglishSwitchOff = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxEnglishSwitchOff",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_d094b0e6223f4040a09552eea41509a3,
        "right": "10%",
        "skin": "sknflxGrey",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxEnglishSwitchOff.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0c3417d846f3d4a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0c3417d846f3d4a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlueOff0c3417d846f3d4a.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0c3417d846f3d4a.add();
    var CopyflxNaveenbhaiKiLakeer0db9c1b5eb5164c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0db9c1b5eb5164c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 10,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0db9c1b5eb5164c.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0db9c1b5eb5164c.add();
    flxEnglishSwitchOff.add(CopyflxRoundDBlueOff0c3417d846f3d4a, CopyflxNaveenbhaiKiLakeer0db9c1b5eb5164c);
    flxEnglish.add(lblEnglish, flxEnglishSwitchOn, flxEnglishSwitchOff);
    var flxArabic = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxArabic",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknflxDBlue6",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxArabic.setDefaultUnit(kony.flex.DP);
    var lblArabic = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArabic",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknlblwhiteCairoL110",
        "text": kony.i18n.getLocalizedString("i18n.common.Arabic.text"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxArabicSwitchOff = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxArabicSwitchOff",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_d42bb2ce97534efab5537e961e08bbfd,
        "right": "5%",
        "skin": "sknflxGrey",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxArabicSwitchOff.setDefaultUnit(kony.flex.DP);
    var flxRoundDBlueOff = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxRoundDBlueOff",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    flxRoundDBlueOff.setDefaultUnit(kony.flex.DP);
    flxRoundDBlueOff.add();
    var flxNaveenbhaiKiLakeer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "flxNaveenbhaiKiLakeer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 10,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    flxNaveenbhaiKiLakeer.setDefaultUnit(kony.flex.DP);
    flxNaveenbhaiKiLakeer.add();
    flxArabicSwitchOff.add(flxRoundDBlueOff, flxNaveenbhaiKiLakeer);
    var flxArabicSwitchOn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxArabicSwitchOn",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_ab19b8f42d6148b8a18bc4ef791f6ecb,
        "right": "10%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxArabicSwitchOn.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0caa529c92d414e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0caa529c92d414e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0caa529c92d414e.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0caa529c92d414e.add();
    var Copyflxlakeer0f86352a9433447 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0f86352a9433447",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0f86352a9433447.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0f86352a9433447.add();
    flxArabicSwitchOn.add(CopyflxRoundDBlue0caa529c92d414e, Copyflxlakeer0f86352a9433447);
    flxArabic.add(lblArabic, flxArabicSwitchOff, flxArabicSwitchOn);
    var flxseperator5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxseperator5",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "CopyslFbox0ab42d5f22ae345",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxseperator5.setDefaultUnit(kony.flex.DP);
    flxseperator5.add();
    var flxlogout = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxlogout",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_if32c2a3f2d94071b7ee9e4091e4f17b,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxlogout.setDefaultUnit(kony.flex.DP);
    var lblLogOut = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLogOut",
        "isVisible": true,
        "left": "15dp",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.common.Logout"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxlogout.add(lblLogOut);
    var lblVersion = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Register"
        },
        "bottom": "1%",
        "id": "lblVersion",
        "isVisible": false,
        "maxNumberOfLines": 1,
        "right": "0%",
        "skin": "CopysknRegisterMobileBank0h92a6e4066af49",
        "text": "V 3.1",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMenuContainer.add(flxProfile, flxMyProfile, flxChangeUsername, flxChangePassword, flxMyRegisteredDevices, flxTnC1, flxseperator, flxAlternateLogins, flxTouchId, flxPINLogin, flxseperator1, flxAlertandNotifications, flxSMS, flxAlerts, flxAlertHistory, flxseperator2, flxAccountPreferences, flxQuickBalance, flxMyAccountSettings, flxReorderMyAccountList, flxEstmtRegister, flxNewSubAccounts, flxseperator3, flxCardPreferences, flxMyCardSettings, flxReorderMyCardList, Copyflxseperator0e28c9d7cee8f4b, flxSiri, flxseperator4, flxChangeLanguage, flxEnglish, flxArabic, flxseperator5, flxlogout, lblVersion);
    frmSettingsKA.add(flxSettingsMain, flxSettingsHeader, flxMenuContainer);
};

function frmSettingsKAGlobals() {
    frmSettingsKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSettingsKA,
        "enabledForIdleTimeout": true,
        "id": "frmSettingsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_ac7adfc406da42b2a4a5889a4f8e6c3b,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_b1e8440f6626429aa2cd811d5d9eb16b,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};