function addWidgetsfrmRecentDepositKA() {
    frmRecentDepositKA.setDefaultUnit(kony.flex.DP);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "50dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var FlexContainer042b0725667e643 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "145dp",
        "id": "FlexContainer042b0725667e643",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer042b0725667e643.setDefaultUnit(kony.flex.DP);
    var FlexContainer00bb80511034544 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "FlexContainer00bb80511034544",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    FlexContainer00bb80511034544.setDefaultUnit(kony.flex.DP);
    var transactionAmount = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionAmount",
        "isVisible": true,
        "skin": "skndetailPageNumber",
        "text": kony.i18n.getLocalizedString("i18n.deposit.frmRecentDepositKA.transactionAmount"),
        "top": "30dp",
        "width": "80%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionName = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionName",
        "isVisible": true,
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.deposit.frmRecentDepositKA.transactionName"),
        "top": "5dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer00bb80511034544.add(transactionAmount, transactionName);
    var Copydivider0e38f136dbb7e4d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0e38f136dbb7e4d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider0e38f136dbb7e4d.setDefaultUnit(kony.flex.DP);
    Copydivider0e38f136dbb7e4d.add();
    FlexContainer042b0725667e643.add(FlexContainer00bb80511034544, Copydivider0e38f136dbb7e4d);
    var lblLine1KA = new kony.ui.Label({
        "height": "1dp",
        "id": "lblLine1KA",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLineEDEDEDKA",
        "text": kony.i18n.getLocalizedString("i18n.deposit.frmRecentDepositKA.lblLine1KA"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var additionalDetails1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75dp",
        "id": "additionalDetails1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    additionalDetails1.setDefaultUnit(kony.flex.DP);
    var lblToKA = new kony.ui.Label({
        "id": "lblToKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.common.To"),
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionFrom = new kony.ui.Label({
        "id": "transactionFrom",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.transfer.frmRecentTransactionDetailsKA.transactionFrom"),
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var divider2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    divider2.setDefaultUnit(kony.flex.DP);
    divider2.add();
    additionalDetails1.add(lblToKA, transactionFrom, divider2);
    var lblLine2KA = new kony.ui.Label({
        "height": "1dp",
        "id": "lblLine2KA",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLineEDEDEDKA",
        "text": kony.i18n.getLocalizedString("i18n.deposit.frmRecentDepositKA.lblLine2KA"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxSchedule = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75dp",
        "id": "flxSchedule",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSchedule.setDefaultUnit(kony.flex.DP);
    var lblTransactionDateKA = new kony.ui.Label({
        "id": "lblTransactionDateKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.deposit.DepositDateC"),
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransactionDateValueKA = new kony.ui.Label({
        "id": "lblTransactionDateValueKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "text": "05-20-2016",
        "top": "40dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Copydivider058b92c2528494d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider058b92c2528494d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider058b92c2528494d.setDefaultUnit(kony.flex.DP);
    Copydivider058b92c2528494d.add();
    flxSchedule.add(lblTransactionDateKA, lblTransactionDateValueKA, Copydivider058b92c2528494d);
    var flxNotesKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxNotesKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNotesKA.setDefaultUnit(kony.flex.DP);
    var lblNotesLabelKA = new kony.ui.Label({
        "id": "lblNotesLabelKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.common.notesc"),
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionNotes = new kony.ui.Label({
        "bottom": "20dp",
        "id": "transactionNotes",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "text": "Note details appear here. This can be a muliple line descripsion",
        "top": "10dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Copydivider06e184b7d586e4a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider06e184b7d586e4a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider06e184b7d586e4a.setDefaultUnit(kony.flex.DP);
    Copydivider06e184b7d586e4a.add();
    flxNotesKA.add(lblNotesLabelKA, transactionNotes, Copydivider06e184b7d586e4a);
    var CopyrepeatTransactionContainer0c3d5855c666549 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "CopyrepeatTransactionContainer0c3d5855c666549",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyrepeatTransactionContainer0c3d5855c666549.setDefaultUnit(kony.flex.DP);
    var CopydeleteScheduleTransactionButton0bc7ffa8e0c1841 = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "60dp",
        "id": "CopydeleteScheduleTransactionButton0bc7ffa8e0c1841",
        "isVisible": true,
        "onClick": AS_Button_6e39c52dd9aa47049afaade68a19fce2,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.viewCheckImages"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Copydivider0eb5e6a55fd264d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0eb5e6a55fd264d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    Copydivider0eb5e6a55fd264d.setDefaultUnit(kony.flex.DP);
    Copydivider0eb5e6a55fd264d.add();
    CopyrepeatTransactionContainer0c3d5855c666549.add(CopydeleteScheduleTransactionButton0bc7ffa8e0c1841, Copydivider0eb5e6a55fd264d);
    mainContent.add(FlexContainer042b0725667e643, lblLine1KA, additionalDetails1, lblLine2KA, flxSchedule, flxNotesKA, CopyrepeatTransactionContainer0c3d5855c666549);
    var titleBarWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarWrapper.setDefaultUnit(kony.flex.DP);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var androidTitleLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "androidTitleLabel",
        "isVisible": true,
        "left": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.deposit.recentDeposit"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var androidBack = new kony.ui.Button({
        "focusSkin": "sknandroidBackButtonFocus",
        "height": "50dp",
        "id": "androidBack",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_9cdf67afa7454f078f2fdc1ba2750dd4,
        "skin": "sknandroidBackButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    androidTitleBar.add(androidTitleLabel, androidBack);
    var btnEditKA = new kony.ui.Button({
        "focusSkin": "sknbtn",
        "height": "50dp",
        "id": "btnEditKA",
        "isVisible": false,
        "onClick": AS_Button_3875da5975144751887cda88fbbae13d,
        "right": "10dp",
        "skin": "sknbtn",
        "text": kony.i18n.getLocalizedString("i18n.common.edit"),
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    titleBarWrapper.add(androidTitleBar, btnEditKA);
    frmRecentDepositKA.add(mainContent, titleBarWrapper);
};

function frmRecentDepositKAGlobals() {
    frmRecentDepositKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmRecentDepositKA,
        "allowHorizontalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmRecentDepositKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_0de669819e564a39b19f4e11189b3a41,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 3
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_4118a828b11947c8841e23b149e468b0,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};