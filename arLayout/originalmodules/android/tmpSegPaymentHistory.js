function initializetmpSegPaymentHistory() {
    flxtmpPaymentHistoryNewKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknflxTransprnt",
        "height": "85dp",
        "id": "flxtmpPaymentHistoryNewKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknflxTransprnt"
    }, {}, {});
    flxtmpPaymentHistoryNewKA.setDefaultUnit(kony.flex.DP);
    var Imgcheck = new kony.ui.Image2({
        "height": "40%",
        "id": "Imgcheck",
        "isVisible": false,
        "left": "3%",
        "skin": "slImage",
        "src": "checkf.png",
        "top": "1%",
        "width": "5%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var transactionDate = new kony.ui.Label({
        "centerY": "25%",
        "id": "transactionDate",
        "isVisible": true,
        "left": "5%",
        "skin": "loansDealsTextSkin",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionAmount = new kony.ui.Label({
        "centerY": "50%",
        "id": "transactionAmount",
        "isVisible": true,
        "right": "8%",
        "skin": "sknNumber",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSepKA = new kony.ui.Label({
        "bottom": "0%",
        "height": "1%",
        "id": "lblSepKA",
        "isVisible": false,
        "right": "0%",
        "skin": "sknLineEDEDEDKA",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "95%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxImageandNameKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40%",
        "id": "flxImageandNameKA",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "40%",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    flxImageandNameKA.setDefaultUnit(kony.flex.DP);
    flxImageandNameKA.add();
    var ImgWithDraw = new kony.ui.Image2({
        "centerY": "25%",
        "id": "ImgWithDraw",
        "isVisible": false,
        "left": "2%",
        "skin": "slImage",
        "src": "pending.png",
        "top": "0%",
        "width": "5%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblExpiryTime = new kony.ui.Label({
        "id": "lblExpiryTime",
        "isVisible": false,
        "right": "25%",
        "skin": "sknErrorMessageEC223BKA",
        "text": "21h:22m",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLastTransaction = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLastTransaction",
        "isVisible": false,
        "left": "5%",
        "skin": "sknNumber",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionAmountSI = new kony.ui.Label({
        "centerY": "50%",
        "id": "transactionAmountSI",
        "isVisible": false,
        "right": "10%",
        "skin": "sknNumber",
        "text": "$155.00",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionName = new kony.ui.Label({
        "centerY": "77%",
        "id": "transactionName",
        "isVisible": true,
        "left": "5%",
        "skin": "sknNumber",
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNoData = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNoData",
        "isVisible": false,
        "left": "15dp",
        "skin": "lblSegName",
        "text": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransRef = new kony.ui.Label({
        "centerY": "50.00%",
        "id": "lblTransRef",
        "isVisible": true,
        "left": "5.05%",
        "skin": "sknNumber",
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransType = new kony.ui.Label({
        "centerY": "77%",
        "id": "lblTransType",
        "isVisible": true,
        "right": "8%",
        "skin": "sknNumber",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxtmpPaymentHistoryNewKA.add(Imgcheck, transactionDate, transactionAmount, lblSepKA, flxImageandNameKA, ImgWithDraw, lblExpiryTime, lblLastTransaction, transactionAmountSI, transactionName, lblNoData, lblTransRef, lblTransType);
}