function addWidgetsfrmRequestStatementAccountsConfirm() {
    frmRequestStatementAccountsConfirm.setDefaultUnit(kony.flex.DP);
    var flxnewSubAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxnewSubAccount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxnewSubAccount.setDefaultUnit(kony.flex.DP);
    var lblJoMoPay = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "45%",
        "id": "lblJoMoPay",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnBack = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
        "height": "100%",
        "id": "btnBack",
        "isVisible": false,
        "left": "0%",
        "onClick": AS_Button_fa0b99327ab54b6bbed5259b4753aa7c,
        "skin": "CopyslButtonGlossBlue0e73a02c4810645",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0%",
        "width": "15%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_be8f74d103db4482a72b4183b904b15b,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 2
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblBack0i5985fbe3b634e",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
    flxnewSubAccount.add(lblJoMoPay, btnBack, flxBack);
    var flxScrollMain = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "91%",
        "horizontalScrollIndicator": true,
        "id": "flxScrollMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "9%",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScrollMain.setDefaultUnit(kony.flex.DP);
    var flxAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxAccount",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "skin": "slFbox0b3edf457af3b43",
        "top": "4%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccount.setDefaultUnit(kony.flex.DP);
    var lblAccountStaticText = new kony.ui.Label({
        "height": "40%",
        "id": "lblAccountStaticText",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhike125",
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxAccount.add(lblAccountStaticText, lblAccountNumber);
    var CopyflxBranch0g43672f6da144d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "CopyflxBranch0g43672f6da144d",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "skin": "slFbox0b3edf457af3b43",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxBranch0g43672f6da144d.setDefaultUnit(kony.flex.DP);
    var CopylblAccountStaticText0f01130bdc50c40 = new kony.ui.Label({
        "height": "40%",
        "id": "CopylblAccountStaticText0f01130bdc50c40",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.common.fromc"),
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblFrom = new kony.ui.Label({
        "id": "lblFrom",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhike125",
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyflxBranch0g43672f6da144d.add(CopylblAccountStaticText0f01130bdc50c40, lblFrom);
    var CopyflxBranch0d0eb9ca7f9c94e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "CopyflxBranch0d0eb9ca7f9c94e",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "skin": "slFbox0b3edf457af3b43",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxBranch0d0eb9ca7f9c94e.setDefaultUnit(kony.flex.DP);
    var CopylblAccountStaticText0f8f8347f104b43 = new kony.ui.Label({
        "height": "40%",
        "id": "CopylblAccountStaticText0f8f8347f104b43",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.common.To"),
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTo = new kony.ui.Label({
        "id": "lblTo",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhike125",
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyflxBranch0d0eb9ca7f9c94e.add(CopylblAccountStaticText0f8f8347f104b43, lblTo);
    var flxMode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxMode",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "skin": "slFbox0b3edf457af3b43",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMode.setDefaultUnit(kony.flex.DP);
    var CopylblAccountStaticText0b2548347fbc840 = new kony.ui.Label({
        "height": "40%",
        "id": "CopylblAccountStaticText0b2548347fbc840",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.accounts.deliveryMode"),
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblMode = new kony.ui.Label({
        "id": "lblMode",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhike125",
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxMode.add(CopylblAccountStaticText0b2548347fbc840, lblMode);
    var flxBranch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxBranch",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "skin": "slFbox0b3edf457af3b43",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBranch.setDefaultUnit(kony.flex.DP);
    var lblModeSelected = new kony.ui.Label({
        "id": "lblModeSelected",
        "isVisible": true,
        "left": "5%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.Map.Branchname"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBranch = new kony.ui.Label({
        "id": "lblBranch",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhike125",
        "top": "2dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBranch.add(lblModeSelected, lblBranch);
    var lblHint = new kony.ui.Label({
        "id": "lblHint",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.accounts.requeststatementMSG"),
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnConfirm = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhite",
        "height": "8%",
        "id": "btnConfirm",
        "isVisible": true,
        "left": "10%",
        "onClick": AS_Button_beed30c0f93b4b5aa3f03f8d12b353cb,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.P2P.confirm"),
        "top": "4%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxScrollMain.add(flxAccount, CopyflxBranch0g43672f6da144d, CopyflxBranch0d0eb9ca7f9c94e, flxMode, flxBranch, lblHint, btnConfirm);
    frmRequestStatementAccountsConfirm.add(flxnewSubAccount, flxScrollMain);
};

function frmRequestStatementAccountsConfirmGlobals() {
    frmRequestStatementAccountsConfirm = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmRequestStatementAccountsConfirm,
        "enabledForIdleTimeout": true,
        "id": "frmRequestStatementAccountsConfirm",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};