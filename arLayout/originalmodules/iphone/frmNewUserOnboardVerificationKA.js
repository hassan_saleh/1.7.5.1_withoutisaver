function addWidgetsfrmNewUserOnboardVerificationKA() {
    frmNewUserOnboardVerificationKA.setDefaultUnit(kony.flex.DP);
    var FlxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxMain",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlxMain.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var btnCancel = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "skntitleBarTextButtonFocus",
        "height": "50dp",
        "id": "btnCancel",
        "isVisible": true,
        "left": "0dp",
        "minWidth": "50dp",
        "onClick": AS_Button_ec2995699abc47aa9218dac928e78320,
        "skin": "skntitleBarTextButton",
        "text": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "width": "70dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var lblSignupKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblSignupKA",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.NewUserOnboarding.SignUp"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxHeader.add(btnCancel, lblSignupKA);
    var flxMainKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "92%",
        "id": "flxMainKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "skncontainerFAFAFAKA",
        "top": "8%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainKA.setDefaultUnit(kony.flex.DP);
    var tabsWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "tabsWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    tabsWrapper.setDefaultUnit(kony.flex.DP);
    var tabSelectedIndicator1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50%",
        "id": "tabSelectedIndicator1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknflxFFA500KA",
        "width": "10.15%"
    }, {}, {});
    tabSelectedIndicator1.setDefaultUnit(kony.flex.DP);
    tabSelectedIndicator1.add();
    var tabSelectedIndicator2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50%",
        "id": "tabSelectedIndicator2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.65%",
        "skin": "sknflxc1c1c1KA",
        "width": "10.15%"
    }, {}, {});
    tabSelectedIndicator2.setDefaultUnit(kony.flex.DP);
    tabSelectedIndicator2.add();
    var tabSelectedIndicator3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50%",
        "id": "tabSelectedIndicator3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.65%",
        "skin": "sknflxc1c1c1KA",
        "width": "10.15%"
    }, {}, {});
    tabSelectedIndicator3.setDefaultUnit(kony.flex.DP);
    tabSelectedIndicator3.add();
    var tabSelectedIndicator4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50%",
        "id": "tabSelectedIndicator4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.65%",
        "skin": "sknflxc1c1c1KA",
        "width": "10.15%"
    }, {}, {});
    tabSelectedIndicator4.setDefaultUnit(kony.flex.DP);
    tabSelectedIndicator4.add();
    var tabSelectedIndicator5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50%",
        "id": "tabSelectedIndicator5",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.65%",
        "skin": "sknflxc1c1c1KA",
        "width": "10.15%"
    }, {}, {});
    tabSelectedIndicator5.setDefaultUnit(kony.flex.DP);
    tabSelectedIndicator5.add();
    var tabSelectedIndicator6 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50%",
        "id": "tabSelectedIndicator6",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.65%",
        "skin": "sknflxc1c1c1KA",
        "width": "10.15%"
    }, {}, {});
    tabSelectedIndicator6.setDefaultUnit(kony.flex.DP);
    tabSelectedIndicator6.add();
    var tabSelectedIndicator7 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50%",
        "id": "tabSelectedIndicator7",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.65%",
        "skin": "sknflxc1c1c1KA",
        "width": "10.15%"
    }, {}, {});
    tabSelectedIndicator7.setDefaultUnit(kony.flex.DP);
    tabSelectedIndicator7.add();
    var tabSelectedIndicator8 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50%",
        "id": "tabSelectedIndicator8",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.65%",
        "skin": "sknflxc1c1c1KA",
        "width": "10.15%"
    }, {}, {});
    tabSelectedIndicator8.setDefaultUnit(kony.flex.DP);
    tabSelectedIndicator8.add();
    tabsWrapper.add(tabSelectedIndicator1, tabSelectedIndicator2, tabSelectedIndicator3, tabSelectedIndicator4, tabSelectedIndicator5, tabSelectedIndicator6, tabSelectedIndicator7, tabSelectedIndicator8);
    var lblOTPKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblOTPKA",
        "isVisible": true,
        "skin": "sknLblF3545a2LatoSemiBold",
        "text": kony.i18n.getLocalizedString("i18n.NUO.SecurityCode"),
        "top": "3%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxOTPinBoxes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxOTPinBoxes",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0%",
        "skin": "slFbox",
        "top": "100dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxOTPinBoxes.setDefaultUnit(kony.flex.DP);
    var txtbox1KA = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "height": "40dp",
        "id": "txtbox1KA",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "10%",
        "secureTextEntry": false,
        "skin": "slTextBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "12%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txtbox2KA = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "height": "40dp",
        "id": "txtbox2KA",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "5%",
        "secureTextEntry": false,
        "skin": "slTextBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "8dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txtbox3KA = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "height": "40dp",
        "id": "txtbox3KA",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "5%",
        "secureTextEntry": false,
        "skin": "slTextBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "8dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txtbox4KA = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "height": "40dp",
        "id": "txtbox4KA",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "5%",
        "secureTextEntry": false,
        "skin": "slTextBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "8dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txtbox5KA = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "height": "40dp",
        "id": "txtbox5KA",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "5%",
        "secureTextEntry": false,
        "skin": "slTextBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "8dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxOTPinBoxes.add(txtbox1KA, txtbox2KA, txtbox3KA, txtbox4KA, txtbox5KA);
    var lblDescriptionKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblDescriptionKA",
        "isVisible": true,
        "left": "129dp",
        "skin": "sknlblF9c9c9c",
        "text": kony.i18n.getLocalizedString("i18n.NUO.OTPDesc"),
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDescriptionContinuationKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblDescriptionContinuationKA",
        "isVisible": true,
        "left": "129dp",
        "skin": "sknlblF9c9c9c",
        "text": kony.i18n.getLocalizedString("i18n.NUO.phone"),
        "top": "1%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var TextfieldenterpinKA = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "skngeneralTextFieldFocus",
        "height": "40dp",
        "id": "TextfieldenterpinKA",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": 5,
        "onTextChange": AS_TextField_ic6dca93b74646aeb036f044ff9c6f17,
        "placeholder": kony.i18n.getLocalizedString("i18n.NUO.XXXXX"),
        "secureTextEntry": false,
        "skin": "sknTbxNoBG",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "5%",
        "width": "50%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "pasteboardType": constants.TEXTBOX_PASTE_BOARD_TYPE_SYSTEM_LEVEL,
        "placeholderSkin": "sknPlaceholderKA",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblLineKA = new kony.ui.Label({
        "centerX": "50%",
        "height": "1dp",
        "id": "lblLineKA",
        "isVisible": true,
        "skin": "sknLineEDEDEDKA",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblInCorrectpinKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblInCorrectpinKA",
        "isVisible": false,
        "skin": "sknErrorMessageEC223BKA",
        "text": kony.i18n.getLocalizedString("i18n.NewUserOnboarding.Incorrect entry,try again"),
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxButtonsWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxButtonsWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "5%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxButtonsWrapper.setDefaultUnit(kony.flex.DP);
    var buttonResendotpKA = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryFAFAFAfocus",
        "height": "40dp",
        "id": "buttonResendotpKA",
        "isVisible": true,
        "onClick": AS_Button_d61984d35b724f1f8cf194a150ddbb53,
        "skin": "sknlatosemiboldFAFAFAKA",
        "text": kony.i18n.getLocalizedString("i18n.NewUserOnboarding.RESENDOTP"),
        "top": "0%",
        "width": "120dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnSubmitKA = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "btnSubmitKA",
        "isVisible": true,
        "onClick": AS_Button_b9ca2ebfb7f74239bd2d063a810038c1,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.btnContinue"),
        "top": "7%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxButtonsWrapper.add(buttonResendotpKA, btnSubmitKA);
    flxMainKA.add(tabsWrapper, lblOTPKA, flxOTPinBoxes, lblDescriptionKA, lblDescriptionContinuationKA, TextfieldenterpinKA, lblLineKA, lblInCorrectpinKA, flxButtonsWrapper);
    FlxMain.add(flxHeader, flxMainKA);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var imgBackground = new kony.ui.Image2({
        "height": "100%",
        "id": "imgBackground",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "bg04.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxHead = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "12%",
        "id": "flxHead",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "1%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHead.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_c5667325cd68430eb5b39a41c1600260,
        "skin": "slFbox",
        "top": "10%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0%",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "login screen"
        },
        "centerY": "51.84%",
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblTitle = new kony.ui.Label({
        "centerX": "51%",
        "id": "lblTitle",
        "isVisible": true,
        "left": "140dp",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.pinLogin.OTP"),
        "top": "8dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxNext = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "31dp",
        "id": "flxNext",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_d64ab074007848f6abcf6adebb95166f,
        "right": "2.04%",
        "skin": "slFbox",
        "top": "11.75%",
        "width": "17.33%",
        "zIndex": 1
    }, {}, {});
    flxNext.setDefaultUnit(kony.flex.DP);
    var lblNext = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Next Page"
        },
        "height": "100%",
        "id": "lblNext",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxNext.add(lblNext);
    var flxProgress1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "11%",
        "id": "flxProgress1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "35%",
        "skin": "CopysknFlxProgress0fd0051ac013643",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxProgress1.setDefaultUnit(kony.flex.DP);
    flxProgress1.add();
    var flxProgress2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "11%",
        "id": "flxProgress2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxProgress",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProgress2.setDefaultUnit(kony.flex.DP);
    flxProgress2.add();
    flxHead.add(flxBack, lblTitle, flxNext, flxProgress1, flxProgress2);
    var flxOTP = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "86%",
        "id": "flxOTP",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "14%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxOTP.setDefaultUnit(kony.flex.DP);
    var lblInstruction3 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblInstruction3",
        "isVisible": true,
        "left": "141dp",
        "skin": "sknLblWhite",
        "text": kony.i18n.getLocalizedString("i18.Transfer.sentOTP"),
        "top": "6%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblPhoneNum = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPhoneNum",
        "isVisible": true,
        "skin": "sknLblWhite",
        "text": "***** ****",
        "top": "25%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxEnterOTP = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50.00%",
        "clipBounds": true,
        "height": "8%",
        "id": "flxEnterOTP",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_a09b232801f74be3b044fa655fb40c4a,
        "skin": "slFbox",
        "top": "35%",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    flxEnterOTP.setDefaultUnit(kony.flex.DP);
    var txtOTP1 = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your One Time Password and click next"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknTxtBox",
        "height": "100%",
        "id": "txtOTP1",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "0dp",
        "onTextChange": AS_TextField_fbe9a7f7dc3248ad87ba6f652c6fc478,
        "secureTextEntry": false,
        "skin": "sknTbxTrans",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "0%",
        "zIndex": 10
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "pasteboardType": constants.TEXTBOX_PASTE_BOARD_TYPE_NO_PASTE_BOARD,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblTxt1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt1",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTxt2 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt2",
        "isVisible": true,
        "left": "19.20%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTxt3 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt3",
        "isVisible": true,
        "left": "36.40%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTxt4 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt4",
        "isVisible": true,
        "left": "53.60%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTxt5 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt5",
        "isVisible": true,
        "left": "70.80%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTxt6 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt6",
        "isVisible": true,
        "left": "88%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxLine1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine1.setDefaultUnit(kony.flex.DP);
    flxLine1.add();
    var flxLine2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "17.20%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine2.setDefaultUnit(kony.flex.DP);
    flxLine2.add();
    var flxLine3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "34.40%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine3.setDefaultUnit(kony.flex.DP);
    flxLine3.add();
    var flxLine4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "51.60%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine4.setDefaultUnit(kony.flex.DP);
    flxLine4.add();
    var flxLine5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": -0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine5",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "68.80%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine5.setDefaultUnit(kony.flex.DP);
    flxLine5.add();
    var flxLine6 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine6",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "86%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine6.setDefaultUnit(kony.flex.DP);
    flxLine6.add();
    var txtOTP = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "1px",
        "id": "txtOTP",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "0dp",
        "onTextChange": AS_TextField_h346553134234151b6ed3f745242bc3d,
        "secureTextEntry": false,
        "skin": "sknTbxTrans",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "1px",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "pasteboardType": constants.TEXTBOX_PASTE_BOARD_TYPE_NO_PASTE_BOARD,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxEnterOTP.add(txtOTP1, lblTxt1, lblTxt2, lblTxt3, lblTxt4, lblTxt5, lblTxt6, flxLine1, flxLine2, flxLine3, flxLine4, flxLine5, flxLine6, txtOTP);
    var flxIphoneCustomWid = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "8%",
        "id": "flxIphoneCustomWid",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "slFbox",
        "top": "35%",
        "width": "65%",
        "zIndex": 1
    }, {}, {});
    flxIphoneCustomWid.setDefaultUnit(kony.flex.DP);
    var loadCustomWidgetForm = new iPhoneAutoOTPRead.customTextWidget({
        "id": "loadCustomWidgetForm",
        "isVisible": true,
        "left": 0,
        "width": "100%",
        "height": "100%",
        "centerX": "50%",
        "centerY": "50%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "widgetName": "customTextWidget"
    });
    var iPhoneLblClose = new kony.ui.Label({
        "centerX": "95%",
        "height": "40dp",
        "id": "iPhoneLblClose",
        "isVisible": true,
        "onTouchStart": AS_Label_h839a419634c448bb461e4c371066f39,
        "skin": "sknClose",
        "text": "O",
        "top": "3dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxCustWidLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxCustWidLine",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxGreyLine",
        "top": "42dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCustWidLine.setDefaultUnit(kony.flex.DP);
    flxCustWidLine.add();
    var CopyflxEnterOTP0d6f18ec15b1843 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "5dp",
        "id": "CopyflxEnterOTP0d6f18ec15b1843",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "85%",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    CopyflxEnterOTP0d6f18ec15b1843.setDefaultUnit(kony.flex.DP);
    var CopytxtOTP0b4b452c99d2a4c = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your One Time Password and click next"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknTxtBox",
        "height": "100%",
        "id": "CopytxtOTP0b4b452c99d2a4c",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "0dp",
        "secureTextEntry": false,
        "skin": "sknTbxTrans",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "0%",
        "zIndex": 10
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "pasteboardType": constants.TEXTBOX_PASTE_BOARD_TYPE_NO_PASTE_BOARD,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopylblTxt0e55a33a1d3724c = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblTxt0e55a33a1d3724c",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblTxt0a6e4aff004a64a = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblTxt0a6e4aff004a64a",
        "isVisible": true,
        "left": "19.20%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblTxt0c0d96837cca54f = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblTxt0c0d96837cca54f",
        "isVisible": true,
        "left": "36.40%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblTxt0ca6dd394b1264c = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblTxt0ca6dd394b1264c",
        "isVisible": true,
        "left": "53.60%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblTxt0f822a2764e6e43 = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblTxt0f822a2764e6e43",
        "isVisible": true,
        "left": "70.80%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblTxt0b40ef011641e4a = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblTxt0b40ef011641e4a",
        "isVisible": true,
        "left": "88%",
        "skin": "sknLblOTP150",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyflxLine0ac6a986644c545 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "CopyflxLine0ac6a986644c545",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    CopyflxLine0ac6a986644c545.setDefaultUnit(kony.flex.DP);
    CopyflxLine0ac6a986644c545.add();
    var CopyflxLine0gb39f129db9840 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "CopyflxLine0gb39f129db9840",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "17.20%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    CopyflxLine0gb39f129db9840.setDefaultUnit(kony.flex.DP);
    CopyflxLine0gb39f129db9840.add();
    var CopyflxLine0c1a857fd703e41 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "CopyflxLine0c1a857fd703e41",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "34.40%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    CopyflxLine0c1a857fd703e41.setDefaultUnit(kony.flex.DP);
    CopyflxLine0c1a857fd703e41.add();
    var CopyflxLine0c12556135a4c46 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "CopyflxLine0c12556135a4c46",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "51.60%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    CopyflxLine0c12556135a4c46.setDefaultUnit(kony.flex.DP);
    CopyflxLine0c12556135a4c46.add();
    var CopyflxLine0d941e26d04ce43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": -0,
        "clipBounds": true,
        "height": "2dp",
        "id": "CopyflxLine0d941e26d04ce43",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "68.80%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    CopyflxLine0d941e26d04ce43.setDefaultUnit(kony.flex.DP);
    CopyflxLine0d941e26d04ce43.add();
    var CopyflxLine0b13c4703875a40 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "CopyflxLine0b13c4703875a40",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "86%",
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    CopyflxLine0b13c4703875a40.setDefaultUnit(kony.flex.DP);
    CopyflxLine0b13c4703875a40.add();
    CopyflxEnterOTP0d6f18ec15b1843.add(CopytxtOTP0b4b452c99d2a4c, CopylblTxt0e55a33a1d3724c, CopylblTxt0a6e4aff004a64a, CopylblTxt0c0d96837cca54f, CopylblTxt0ca6dd394b1264c, CopylblTxt0f822a2764e6e43, CopylblTxt0b40ef011641e4a, CopyflxLine0ac6a986644c545, CopyflxLine0gb39f129db9840, CopyflxLine0c1a857fd703e41, CopyflxLine0c12556135a4c46, CopyflxLine0d941e26d04ce43, CopyflxLine0b13c4703875a40);
    flxIphoneCustomWid.add(loadCustomWidgetForm, iPhoneLblClose, flxCustWidLine, CopyflxEnterOTP0d6f18ec15b1843);
    var lblNotYourNum = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Click here if that's not your number"
        },
        "centerX": "50%",
        "height": "10%",
        "id": "lblNotYourNum",
        "isVisible": true,
        "onTouchEnd": AS_Label_d75d6d09939644a7bb7227a8d100ff27,
        "skin": "CopysknLblWhite0fbfcdb6497644a",
        "text": kony.i18n.getLocalizedString("i18n.otp.contactcenter"),
        "top": "55%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxVisitNearestBranch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "6%",
        "id": "flxVisitNearestBranch",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_idbbf915bd574d3288113fd76f1823c8,
        "right": "2%",
        "skin": "sknFlxRoundedCorner",
        "top": "54%",
        "width": "45%",
        "zIndex": 1
    }, {}, {});
    flxVisitNearestBranch.setDefaultUnit(kony.flex.DP);
    var lblLocationIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Visit"
        },
        "height": "100%",
        "id": "lblLocationIcon",
        "isVisible": true,
        "left": "5%",
        "skin": "CopyslLabel0g0e6768da08b40",
        "text": "y",
        "top": "0%",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblVisitNearestBranch = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Nearest Branch"
        },
        "height": "100%",
        "id": "lblVisitNearestBranch",
        "isVisible": true,
        "left": "12.93%",
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.otp.callContactCenter"),
        "top": "0.00%",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxVisitNearestBranch.add(lblLocationIcon, lblVisitNearestBranch);
    var btnResendOTP = new kony.ui.Button({
        "accessibilityConfig": {
            "a11yLabel": "Resend the One time password"
        },
        "centerX": "50.00%",
        "focusSkin": "slButtonGreenFocus",
        "height": "6%",
        "id": "btnResendOTP",
        "isVisible": false,
        "onClick": AS_Button_ebf65b62b6a54b299c7f13a169c23243,
        "skin": "slButtonGreen",
        "text": kony.i18n.getLocalizedString("i18n.pinLogin.reSendPassword"),
        "top": "73%",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var lblInvalidCredentialsKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblInvalidCredentialsKA",
        "isVisible": false,
        "right": "5%",
        "skin": "sknInvalidCredKA",
        "text": kony.i18n.getLocalizedString("i18n.transfers.incorrectOTP"),
        "top": "48%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblContactCenterNumber = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Nearest Branch"
        },
        "centerX": "50%",
        "height": "5%",
        "id": "lblContactCenterNumber",
        "isVisible": true,
        "onTouchEnd": AS_Label_d95f5ebb4bcb4fa38a1607b412c63cd7,
        "skin": "CopysknLblWhite0a7ef733745d04d",
        "text": "+962 6 5807777",
        "top": "66%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxOTP.add(lblInstruction3, lblPhoneNum, flxEnterOTP, flxIphoneCustomWid, lblNotYourNum, flxVisitNearestBranch, btnResendOTP, lblInvalidCredentialsKA, lblContactCenterNumber);
    flxMainContainer.add(imgBackground, flxHead, flxOTP);
    frmNewUserOnboardVerificationKA.add(FlxMain, flxMainContainer);
};

function frmNewUserOnboardVerificationKAGlobals() {
    frmNewUserOnboardVerificationKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmNewUserOnboardVerificationKA,
        "bounces": false,
        "enabledForIdleTimeout": true,
        "id": "frmNewUserOnboardVerificationKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "onDestroy": AS_Form_hee1174092bf4999943b1d9c00270e2a,
        "onHide": AS_Form_cb7c790f841b4b238623e52e86776369,
        "postShow": AS_Form_a1b4db68d57e4374940abe09f05cf510,
        "preShow": AS_Form_cecdfd93f0ca459a80129b9059878066,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NONE,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};