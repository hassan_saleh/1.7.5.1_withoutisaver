function addWidgetsfrmFaceIDCapture() {
    frmFaceIDCapture.setDefaultUnit(kony.flex.DP);
    var flxHeaderKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeaderKA.setDefaultUnit(kony.flex.DP);
    var flxHeaderContainerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50.00%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderContainerKA",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "skin": "slFbox",
        "top": "0dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    flxHeaderContainerKA.setDefaultUnit(kony.flex.DP);
    var imgAuthMode1KA = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgAuthMode1KA",
        "isVisible": true,
        "left": "0%",
        "skin": "slImage",
        "src": "touchiconactive.png",
        "width": "45dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxLine1KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "3.50%",
        "id": "flxLine1KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-2dp",
        "skin": "sknFlxLine",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    flxLine1KA.setDefaultUnit(kony.flex.DP);
    flxLine1KA.add();
    var imgAuthMode2KA = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgAuthMode2KA",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "pin.png",
        "width": "45dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxLine2KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "3.50%",
        "id": "flxLine2KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-3dp",
        "skin": "sknFlxLine",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    flxLine2KA.setDefaultUnit(kony.flex.DP);
    flxLine2KA.add();
    var imgAuthMode3KA = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgAuthMode3KA",
        "isVisible": true,
        "left": "-6dp",
        "skin": "slImage",
        "src": "face.png",
        "width": "45dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderContainerKA.add(imgAuthMode1KA, flxLine1KA, imgAuthMode2KA, flxLine2KA, imgAuthMode3KA);
    var flxHeaderText = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderText",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "skin": "slFbox",
        "top": "10dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    flxHeaderText.setDefaultUnit(kony.flex.DP);
    var Label0i10bb778338948 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "Label0i10bb778338948",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.EnableFaceIdHeader"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxHeaderText.add(Label0i10bb778338948);
    flxHeaderKA.add(flxHeaderContainerKA, flxHeaderText);
    var flxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "slFbox",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var lblHeaderDesc = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblHeaderDesc",
        "isVisible": true,
        "skin": "sknonboardingText",
        "text": kony.i18n.getLocalizedString("i18n.Holdyourphone8to20inchesfromyourface"),
        "top": "5%",
        "width": "98%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgFaceCapture = new kony.ui.Image2({
        "centerX": "50%",
        "height": "250dp",
        "id": "imgFaceCapture",
        "isVisible": true,
        "skin": "slImage",
        "src": "cameraface.png",
        "top": "5%",
        "width": "250dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblFooterDesc = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblFooterDesc",
        "isVisible": true,
        "skin": "sknonboardingText",
        "text": kony.i18n.getLocalizedString("i18n.PlaceyourfaceincircleandBlinkyoureyes"),
        "top": "5%",
        "width": "98%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblProgress = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblProgress",
        "isVisible": true,
        "skin": "sknonboardingText190",
        "text": kony.i18n.getLocalizedString("i18n.0%"),
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnCancel = new kony.ui.Button({
        "centerX": "50%",
        "height": "42dp",
        "id": "btnCancel",
        "isVisible": true,
        "onClick": AS_Button_db95248c8a1b48a1be59130b70ec8f12,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.START"),
        "top": "5%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxMain.add(lblHeaderDesc, imgFaceCapture, lblFooterDesc, lblProgress, btnCancel);
    frmFaceIDCapture.add(flxHeaderKA, flxMain);
};

function frmFaceIDCaptureGlobals() {
    frmFaceIDCapture = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmFaceIDCapture,
        "enabledForIdleTimeout": true,
        "id": "frmFaceIDCapture",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_i0bf97d045334061a78880a4609a40f9,
        "skin": "sknfrmbkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};