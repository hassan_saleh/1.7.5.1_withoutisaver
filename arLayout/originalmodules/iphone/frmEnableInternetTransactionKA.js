function addWidgetsfrmEnableInternetTransactionKA() {
    frmEnableInternetTransactionKA.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "skin": "slFbox0b7d74518f07a4d",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var lblFormHeading = new kony.ui.Label({
        "centerX": "50%",
        "height": "100%",
        "id": "lblFormHeading",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.cards.updatelimit"),
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxNext = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxNext",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "2%",
        "skin": "slFbox",
        "top": "5%",
        "width": "18%",
        "zIndex": 2
    }, {}, {});
    flxNext.setDefaultUnit(kony.flex.DP);
    var lblNext = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Next Page"
        },
        "height": "100%",
        "id": "lblNext",
        "isVisible": false,
        "left": "0dp",
        "onTouchEnd": AS_Label_de7bda87f648474b983224ccd233f390,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxNext.add(lblNext);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_j47f48a17e4a40e983dbf82d04a858c3,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btn = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "0%",
        "id": "btn",
        "isVisible": true,
        "left": "0%",
        "skin": "btnBack0b71f859656c647",
        "top": "0%",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxBack.add(lblBackIcon, lblBack, btn);
    flxHeader.add(lblFormHeading, flxNext, flxBack);
    var flxBody = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "71%",
        "horizontalScrollIndicator": true,
        "id": "flxBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "9%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBody.setDefaultUnit(kony.flex.DP);
    var flxToggleECom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxToggleECom",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknFlxLightGreyColor",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxToggleECom.setDefaultUnit(kony.flex.DP);
    var lblEcommTransaction = new kony.ui.Label({
        "height": "100%",
        "id": "lblEcommTransaction",
        "isVisible": true,
        "left": "5%",
        "skin": "sknlblTouchIdsmall",
        "text": kony.i18n.getLocalizedString("i18n.ecomm.label1"),
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxSwitchOffET = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSwitchOffET",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_b501f925e01f4370b08ed210deba99be,
        "right": "3%",
        "skin": "sknflxGrey",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSwitchOffET.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0hfee6fe5ed994b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0hfee6fe5ed994b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0hfee6fe5ed994b.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0hfee6fe5ed994b.add();
    var Copyflxlakeer0a0654737f4504c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0a0654737f4504c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0a0654737f4504c.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0a0654737f4504c.add();
    flxSwitchOffET.add(CopyflxRoundDBlue0hfee6fe5ed994b, Copyflxlakeer0a0654737f4504c);
    var flxSwitchOnET = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSwitchOnET",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_b501f925e01f4370b08ed210deba99be,
        "right": "3%",
        "skin": "sknflxyellow",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSwitchOnET.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0j3222bd94cbc49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0j3222bd94cbc49",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 2
    }, {}, {});
    CopyflxRoundDBlueOff0j3222bd94cbc49.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0j3222bd94cbc49.add();
    var CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.add();
    flxSwitchOnET.add(CopyflxRoundDBlueOff0j3222bd94cbc49, CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b);
    flxToggleECom.add(lblEcommTransaction, flxSwitchOffET, flxSwitchOnET);
    var flxTogglemailOrder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxTogglemailOrder",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknFlxLightGreyColor",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTogglemailOrder.setDefaultUnit(kony.flex.DP);
    var lblMailOrderTansactions = new kony.ui.Label({
        "height": "100%",
        "id": "lblMailOrderTansactions",
        "isVisible": true,
        "left": "5%",
        "skin": "sknlblTouchIdsmall",
        "text": kony.i18n.getLocalizedString("i18n.EcommLimit.MailOrder"),
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxSwitchOffMO = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSwitchOffMO",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_b66f189e397044aaab7bd93b11b0faae,
        "right": "3%",
        "skin": "sknflxGrey",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSwitchOffMO.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0i13b50d999c446 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0i13b50d999c446",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0i13b50d999c446.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0i13b50d999c446.add();
    var Copyflxlakeer0e9ab052dc56845 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0e9ab052dc56845",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0e9ab052dc56845.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0e9ab052dc56845.add();
    flxSwitchOffMO.add(CopyflxRoundDBlue0i13b50d999c446, Copyflxlakeer0e9ab052dc56845);
    var flxSwitchOnMO = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSwitchOnMO",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_b66f189e397044aaab7bd93b11b0faae,
        "right": "3%",
        "skin": "sknflxyellow",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSwitchOnMO.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0af5f3c47476646 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0af5f3c47476646",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 2
    }, {}, {});
    CopyflxRoundDBlueOff0af5f3c47476646.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0af5f3c47476646.add();
    var CopyflxNaveenbhaiKiLakeer0b45b558ccc794d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0b45b558ccc794d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0b45b558ccc794d.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0b45b558ccc794d.add();
    flxSwitchOnMO.add(CopyflxRoundDBlueOff0af5f3c47476646, CopyflxNaveenbhaiKiLakeer0b45b558ccc794d);
    flxTogglemailOrder.add(lblMailOrderTansactions, flxSwitchOffMO, flxSwitchOnMO);
    var flxdays = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "7%",
        "id": "flxdays",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknBOJblueBG",
        "top": "4%",
        "width": "90%"
    }, {}, {});
    flxdays.setDefaultUnit(kony.flex.DP);
    var btnDaily = new kony.ui.Button({
        "focusSkin": "sknBtnBGWhiteBlue105Rd10",
        "height": "98%",
        "id": "btnDaily",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_a10c970a784842da835dc1f056b63cdf,
        "skin": "sknBtnBGBlueWhite105Rd10",
        "text": kony.i18n.getLocalizedString("i18n.transfers.Daily"),
        "top": "1%",
        "width": "33%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnWeekly = new kony.ui.Button({
        "focusSkin": "sknBtnBGBlueWhite105Rd10",
        "height": "99%",
        "id": "btnWeekly",
        "isVisible": true,
        "left": "33.50%",
        "onClick": AS_Button_a10c970a784842da835dc1f056b63cdf,
        "skin": "sknBtnBGBlueWhite105Rd10",
        "text": kony.i18n.getLocalizedString("i18n.transfers.WeeklyOnce"),
        "top": "1%",
        "width": "33%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var btnMonthly = new kony.ui.Button({
        "focusSkin": "sknBtnBGBlueWhite105Rd10",
        "height": "98%",
        "id": "btnMonthly",
        "isVisible": true,
        "onClick": AS_Button_a10c970a784842da835dc1f056b63cdf,
        "right": "0%",
        "skin": "sknBtnBGWhiteBlue105Rd10",
        "text": kony.i18n.getLocalizedString("i18n.transfers.MonthlyOnce"),
        "top": "1%",
        "width": "33%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxdays.add(btnDaily, btnWeekly, btnMonthly);
    var flxLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "64%",
        "id": "flxLimit",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLimit.setDefaultUnit(kony.flex.DP);
    var flxECommerceLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "52%",
        "id": "flxECommerceLimit",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxECommerceLimit.setDefaultUnit(kony.flex.DP);
    var flxecomLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxecomLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknslFbox",
        "top": "10%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxecomLimit.setDefaultUnit(kony.flex.PERCENTAGE);
    var txtEcomLimit = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "txtEcomLimit",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "onTextChange": AS_TextField_j2f063e57eb14c58925fd4f626502832,
        "onTouchEnd": AS_TextField_a176250a93a04056ab52cf917386327e,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "100%",
        "zIndex": 10
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var borderBottom1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "3%",
        "id": "borderBottom1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "95%",
        "zIndex": 2
    }, {}, {});
    borderBottom1.setDefaultUnit(kony.flex.DP);
    borderBottom1.add();
    var lblEcom = new kony.ui.Label({
        "id": "lblEcom",
        "isVisible": true,
        "left": "2%",
        "skin": "sknCaiRegWhite50Op",
        "text": kony.i18n.getLocalizedString("i18n.EcommLimit.ecoomLimit"),
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblClose1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblClose1",
        "isVisible": false,
        "right": "2%",
        "skin": "sknClose",
        "text": "O",
        "top": "10%",
        "width": "10%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxecomLimit.add(txtEcomLimit, borderBottom1, lblEcom, lblClose1);
    var lblEcomLimit = new kony.ui.Label({
        "id": "lblEcomLimit",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblSmallWhite",
        "text": kony.i18n.getLocalizedString("i18n.EcommLimit.CreditLimit"),
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var sliderECommerceLmt = new kony.ui.Slider({
        "height": "45dp",
        "id": "sliderECommerceLmt",
        "isVisible": true,
        "left": "5%",
        "leftSkin": "slSliderLeftBlue",
        "max": 100,
        "min": 0,
        "onSlide": AS_Slider_ba69a556cd064015a94144de7e64efaa,
        "rightSkin": "slSliderRightBlue",
        "selectedValue": 50,
        "step": 1,
        "thumbImage": "slider_ios7.png",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {
        "thumbTintColor": "ffffff00"
    });
    flxECommerceLimit.add(flxecomLimit, lblEcomLimit, sliderECommerceLmt);
    var flxMailOederLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "52%",
        "id": "flxMailOederLimit",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMailOederLimit.setDefaultUnit(kony.flex.DP);
    var flxMailLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxMailLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknslFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxMailLimit.setDefaultUnit(kony.flex.PERCENTAGE);
    var txtMailLimit = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "txtMailLimit",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "onTextChange": AS_TextField_a176250a93a04056ab52cf917386327e,
        "onTouchEnd": AS_TextField_a176250a93a04056ab52cf917386327e,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "100%",
        "zIndex": 10
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var borderBottom2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "3%",
        "id": "borderBottom2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "95%",
        "zIndex": 2
    }, {}, {});
    borderBottom2.setDefaultUnit(kony.flex.DP);
    borderBottom2.add();
    var lblMailOrder = new kony.ui.Label({
        "id": "lblMailOrder",
        "isVisible": true,
        "left": "2%",
        "skin": "sknCaiRegWhite50Op",
        "text": kony.i18n.getLocalizedString("i18n.EcommLimit.MailLimit"),
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblClose2 = new kony.ui.Label({
        "height": "100%",
        "id": "lblClose2",
        "isVisible": false,
        "right": "2%",
        "skin": "sknClose",
        "text": "O",
        "top": "10%",
        "width": "10%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxMailLimit.add(txtMailLimit, borderBottom2, lblMailOrder, lblClose2);
    var lblMaillmit = new kony.ui.Label({
        "id": "lblMaillmit",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblSmallWhite",
        "text": kony.i18n.getLocalizedString("i18n.EcommLimit.CreditLimit"),
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var sliderMailOrderLimit = new kony.ui.Slider({
        "height": "45dp",
        "id": "sliderMailOrderLimit",
        "isVisible": true,
        "left": "5%",
        "leftSkin": "slSliderLeftBlue",
        "max": 100,
        "min": 0,
        "onSlide": AS_Slider_ba69a556cd064015a94144de7e64efaa,
        "rightSkin": "slSliderRightBlue",
        "selectedValue": 50,
        "step": 1,
        "thumbImage": "slider_ios7.png",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {
        "thumbTintColor": "ffffff00"
    });
    flxMailOederLimit.add(flxMailLimit, lblMaillmit, sliderMailOrderLimit);
    var lblToggleOnmsg = new kony.ui.Label({
        "id": "lblToggleOnmsg",
        "isVisible": false,
        "left": "2%",
        "skin": "sknCaiRegWhite50Op",
        "text": kony.i18n.getLocalizedString("i18n.EcommLimit.ToggleOnMsg"),
        "top": "20%",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxLimit.add(flxECommerceLimit, flxMailOederLimit, lblToggleOnmsg);
    flxBody.add(flxToggleECom, flxTogglemailOrder, flxdays, flxLimit);
    var flxFooter = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0%",
        "clipBounds": true,
        "height": "20%",
        "id": "flxFooter",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFooter.setDefaultUnit(kony.flex.DP);
    var btnSaveInternetLimit = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "40%",
        "id": "btnSaveInternetLimit",
        "isVisible": true,
        "onClick": AS_Button_f5efa2c3112742edb6268f8ba3891519,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.ecommerce.updatebutton"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxFooter.add(btnSaveInternetLimit);
    frmEnableInternetTransactionKA.add(flxHeader, flxBody, flxFooter);
};

function frmEnableInternetTransactionKAGlobals() {
    frmEnableInternetTransactionKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmEnableInternetTransactionKA,
        "allowHorizontalBounce": false,
        "allowVerticalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmEnableInternetTransactionKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_ee4a2989fbdb4e88a135942451379940,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "bouncesZoom": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};