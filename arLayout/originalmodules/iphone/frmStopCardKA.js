function addWidgetsfrmStopCardKA() {
    frmStopCardKA.setDefaultUnit(kony.flex.DP);
    var iosTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "iosTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "s",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    iosTitleBar.setDefaultUnit(kony.flex.DP);
    var lblStopCardTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblStopCardTitle",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": "Stop Card",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnClose = new kony.ui.Button({
        "centerY": "50%",
        "height": "100%",
        "id": "btnClose",
        "isVisible": true,
        "onClick": AS_Button_dfaf69c6b0b745e690d089cd675e5699,
        "right": 2,
        "skin": "sknBtnTrans",
        "top": "5dp",
        "width": "35dp",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var lblClose = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblClose",
        "isVisible": true,
        "right": 2,
        "skin": "sknLblBoj145",
        "text": "O",
        "top": "5dp",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    iosTitleBar.add(lblStopCardTitle, btnClose, lblClose);
    var flxStopCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45%",
        "id": "flxStopCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStopCard.setDefaultUnit(kony.flex.DP);
    var lblReasonDesc = new kony.ui.Label({
        "id": "lblReasonDesc",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknnavBarTitle",
        "text": "Select the reason for card deactivation",
        "top": "26dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnSubmit = new kony.ui.Button({
        "bottom": "5dp",
        "centerX": "50%",
        "height": "50dp",
        "id": "btnSubmit",
        "isVisible": true,
        "left": "28dp",
        "onClick": AS_Button_h4a5fd8e0da34155bef6c5d9eefcfa45,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.NUO.Submit"),
        "width": "300dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxCardReason = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxCardReason",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "15dp",
        "onClick": AS_FlexContainer_cd84d7d765274d158e7ba003e13e4b3e,
        "skin": "slFbox",
        "top": "110dp",
        "width": "90%",
        "zIndex": 3
    }, {}, {});
    flxCardReason.setDefaultUnit(kony.flex.DP);
    var lblReason = new kony.ui.Label({
        "id": "lblReason",
        "isVisible": true,
        "left": "2%",
        "skin": "sknlblCairoL",
        "text": "Select a Reason",
        "top": "20%",
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnDropDown = new kony.ui.Button({
        "height": "100%",
        "id": "btnDropDown",
        "isVisible": true,
        "right": "2%",
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "o",
        "top": "2%",
        "width": "90%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var lstCardReason = new kony.ui.ListBox({
        "id": "lstCardReason",
        "isVisible": false,
        "left": "4dp",
        "masterData": [
            ["lb1", "Select a Reason"],
            ["lb2", "Card Lost"],
            ["lb3", "Card Damaged"]
        ],
        "selectedKey": "lb1",
        "selectedKeyValue": ["lb1", "Select a Reason"],
        "skin": "slListBoxNew",
        "top": "1dp",
        "width": "92%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "dropDownImage": "dropdown.png",
        "groupCells": false,
        "viewConfig": {
            "toggleViewConfig": {
                "viewStyle": constants.LISTBOX_TOGGLE_VIEW_STYLE_PLAIN
            }
        },
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var flxUnderlineBillerCategory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineBillerCategory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineBillerCategory.setDefaultUnit(kony.flex.DP);
    flxUnderlineBillerCategory.add();
    flxCardReason.add(lblReason, btnDropDown, lstCardReason, flxUnderlineBillerCategory);
    var flxBorderTransferType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "flxBorderTransferType",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "18dp",
        "skin": "skntLineDiv",
        "top": 145,
        "width": "78%",
        "zIndex": 1
    }, {}, {});
    flxBorderTransferType.setDefaultUnit(kony.flex.DP);
    flxBorderTransferType.add();
    flxStopCard.add(lblReasonDesc, btnSubmit, flxCardReason, flxBorderTransferType);
    frmStopCardKA.add(iosTitleBar, flxStopCard);
};

function frmStopCardKAGlobals() {
    frmStopCardKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmStopCardKA,
        "enabledForIdleTimeout": true,
        "id": "frmStopCardKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bouncesZoom": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};