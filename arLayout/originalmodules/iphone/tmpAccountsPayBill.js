function initializetmpAccountsPayBill() {
    yourAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknyourAccountCardFocus",
        "height": "62dp",
        "id": "yourAccount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknyourAccountCard",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    yourAccount.setDefaultUnit(kony.flex.DP);
    var colorAccount1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "100%",
        "id": "colorAccount1",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknCopyslFbox02562de9b22264b",
        "top": "0dp",
        "width": "6dp"
    }, {}, {});
    colorAccount1.setDefaultUnit(kony.flex.DP);
    colorAccount1.add();
    var nameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "nameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    nameContainer.setDefaultUnit(kony.flex.DP);
    var nameAccount1 = new kony.ui.Label({
        "id": "nameAccount1",
        "isVisible": true,
        "left": "15dp",
        "maxWidth": "90%",
        "skin": "sknaccountName",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var dummyAccountName = new kony.ui.Label({
        "id": "dummyAccountName",
        "isVisible": false,
        "left": "25dp",
        "maxWidth": "90%",
        "skin": "sknaccountName",
        "top": "22dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var dummyAccountNumber = new kony.ui.Label({
        "id": "dummyAccountNumber",
        "isVisible": false,
        "left": "35dp",
        "maxWidth": "90%",
        "skin": "sknaccountName",
        "top": "32dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblColorKA = new kony.ui.Label({
        "height": "100%",
        "id": "lblColorKA",
        "isVisible": true,
        "left": "0dp",
        "skin": "slLabel",
        "top": "0dp",
        "width": "6dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var isPFMLabel = new kony.ui.Label({
        "id": "isPFMLabel",
        "isVisible": false,
        "right": "12dp",
        "skin": "sknaccountAmount",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBankName = new kony.ui.Label({
        "id": "lblBankName",
        "isVisible": true,
        "left": "15dp",
        "maxWidth": "90%",
        "skin": "sknsegmentHeaderText",
        "top": "29dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    nameContainer.add(nameAccount1, dummyAccountName, dummyAccountNumber, lblColorKA, isPFMLabel, lblBankName);
    var amountAccount1 = new kony.ui.Label({
        "id": "amountAccount1",
        "isVisible": true,
        "right": "12dp",
        "skin": "sknaccountAmount",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var amtOutsatndingBal = new kony.ui.Label({
        "id": "amtOutsatndingBal",
        "isVisible": false,
        "right": "12dp",
        "skin": "sknaccountAmount",
        "text": "$00.00",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var amountcurrBal = new kony.ui.Label({
        "id": "amountcurrBal",
        "isVisible": false,
        "right": "12dp",
        "skin": "sknaccountAmount",
        "text": "$00.00",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var typeAccount = new kony.ui.Label({
        "bottom": "12dp",
        "id": "typeAccount",
        "isVisible": false,
        "right": "12dp",
        "skin": "sknaccountAvailableBalanceLabel",
        "text": "Available Balance",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var typeKA = new kony.ui.Label({
        "bottom": "2dp",
        "centerX": "50%",
        "id": "typeKA",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknaccountAvailableBalanceLabel",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var typeAccountBalanceView = new kony.ui.Label({
        "bottom": "12dp",
        "id": "typeAccountBalanceView",
        "isVisible": true,
        "right": "12dp",
        "skin": "sknaccountAvailableBalanceLabel",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    yourAccount.add(colorAccount1, nameContainer, amountAccount1, amtOutsatndingBal, amountcurrBal, typeAccount, typeKA, typeAccountBalanceView);
}