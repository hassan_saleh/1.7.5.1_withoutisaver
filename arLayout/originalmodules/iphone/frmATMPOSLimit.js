function addWidgetsfrmATMPOSLimit() {
    frmATMPOSLimit.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox0b7d74518f07a4d",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "95%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_g8ec2b96eee34ab18a5c47f2bbbf59b4,
        "skin": "slFbox",
        "top": "0%",
        "width": "18%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblFormHeading = new kony.ui.Label({
        "centerX": "50%",
        "height": "100%",
        "id": "lblFormHeading",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.atmpos.updatelimit"),
        "top": "0%",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxHeader.add(flxBack, lblFormHeading);
    var flxBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBody",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBody.setDefaultUnit(kony.flex.DP);
    var flxToggle = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "7%",
        "id": "flxToggle",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "5%",
        "skin": "sknBOJblueBG",
        "top": "3%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxToggle.setDefaultUnit(kony.flex.DP);
    var btnDaily = new kony.ui.Button({
        "focusSkin": "sknBtnBGBlueWhite105Rd10",
        "height": "100%",
        "id": "btnDaily",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_d03cc2209df14854bc9dbd020e5fd4d7,
        "skin": "sknBtnBGWhiteBlue105Rd10",
        "text": kony.i18n.getLocalizedString("i18n.transfers.Daily"),
        "top": "0%",
        "width": "33%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnWeekly = new kony.ui.Button({
        "focusSkin": "sknBtnBGBlueWhite105Rd10",
        "height": "100%",
        "id": "btnWeekly",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_d03cc2209df14854bc9dbd020e5fd4d7,
        "skin": "sknBtnBGBlueWhite105Rd10",
        "text": kony.i18n.getLocalizedString("i18n.transfers.WeeklyOnce"),
        "top": "0%",
        "width": "34%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnMonthly = new kony.ui.Button({
        "focusSkin": "sknBtnBGBlueWhite105Rd10",
        "height": "100%",
        "id": "btnMonthly",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_d03cc2209df14854bc9dbd020e5fd4d7,
        "skin": "sknBtnBGBlueWhite105Rd10",
        "text": kony.i18n.getLocalizedString("i18n.transfers.MonthlyOnce"),
        "top": "0%",
        "width": "33%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxToggle.add(btnDaily, btnWeekly, btnMonthly);
    var flxWithdrawalPOSlimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxWithdrawalPOSlimit",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWithdrawalPOSlimit.setDefaultUnit(kony.flex.DP);
    var flxWithdrawalLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35%",
        "id": "flxWithdrawalLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "5%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWithdrawalLimit.setDefaultUnit(kony.flex.DP);
    var flxWithdrawalLimitEntry = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90dp",
        "id": "flxWithdrawalLimitEntry",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknslFbox",
        "top": "10%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxWithdrawalLimitEntry.setDefaultUnit(kony.flex.PERCENTAGE);
    var txtWithdrawalLimit = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "23%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "50%",
        "id": "txtWithdrawalLimit",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "onTextChange": AS_TextField_e12d687073e74031a9afc37b7710bba6,
        "onTouchEnd": AS_TextField_c0d7475dffd44177b9ea0cad91e4a7f9,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "100%",
        "zIndex": 10
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var WithdrawalLimitBorder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "WithdrawalLimitBorder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "skntextFieldDivider",
        "top": "73%",
        "width": "95%",
        "zIndex": 2
    }, {}, {});
    WithdrawalLimitBorder.setDefaultUnit(kony.flex.DP);
    WithdrawalLimitBorder.add();
    var lblWithdrawalLimitTitle = new kony.ui.Label({
        "id": "lblWithdrawalLimitTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknCaiRegWhite50Op",
        "text": kony.i18n.getLocalizedString("i18n.atmpos.atmlimit"),
        "top": "28%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblWithdrawalLimitHint = new kony.ui.Label({
        "bottom": "0%",
        "id": "lblWithdrawalLimitHint",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblSmallWhite",
        "text": "Card limit is 500",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxWithdrawalLimitEntry.add(txtWithdrawalLimit, WithdrawalLimitBorder, lblWithdrawalLimitTitle, lblWithdrawalLimitHint);
    flxWithdrawalLimit.add(flxWithdrawalLimitEntry);
    var sliderATMLimit = new kony.ui.Slider({
        "height": "45dp",
        "id": "sliderATMLimit",
        "isVisible": true,
        "left": "5%",
        "leftSkin": "slSliderLeftBlue",
        "max": 100,
        "min": 0,
        "onSlide": AS_Slider_ba6b7476148346be8b0e3ddb48374e76,
        "rightSkin": "slSliderRightBlue",
        "selectedValue": 40,
        "step": 1,
        "thumbImage": "slider_ios7.png",
        "top": "-3%",
        "width": "90%",
        "zIndex": 1
    }, {}, {
        "thumbTintColor": "ffffff00"
    });
    var flxPOSLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35%",
        "id": "flxPOSLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "-5%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPOSLimit.setDefaultUnit(kony.flex.DP);
    var flxPOSLimitEntry = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90dp",
        "id": "flxPOSLimitEntry",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknslFbox",
        "top": "10%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPOSLimitEntry.setDefaultUnit(kony.flex.PERCENTAGE);
    var txtPOSLimit = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "23%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "50%",
        "id": "txtPOSLimit",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "onTextChange": AS_TextField_e12d687073e74031a9afc37b7710bba6,
        "onTouchEnd": AS_TextField_c0d7475dffd44177b9ea0cad91e4a7f9,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "100%",
        "zIndex": 10
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var POSLimitBorder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "POSLimitBorder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "skntextFieldDivider",
        "top": "73%",
        "width": "95%",
        "zIndex": 2
    }, {}, {});
    POSLimitBorder.setDefaultUnit(kony.flex.DP);
    POSLimitBorder.add();
    var lblPOSLimitTitle = new kony.ui.Label({
        "id": "lblPOSLimitTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknCaiRegWhite50Op",
        "text": kony.i18n.getLocalizedString("i18n.atmpos.poslimit"),
        "top": "28%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblPOSLimitHint = new kony.ui.Label({
        "bottom": "0%",
        "id": "lblPOSLimitHint",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblSmallWhite",
        "text": "Card limit is 500",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxPOSLimitEntry.add(txtPOSLimit, POSLimitBorder, lblPOSLimitTitle, lblPOSLimitHint);
    flxPOSLimit.add(flxPOSLimitEntry);
    var sliderPOSLimit = new kony.ui.Slider({
        "height": "45dp",
        "id": "sliderPOSLimit",
        "isVisible": true,
        "left": "5%",
        "leftSkin": "slSliderLeftBlue",
        "max": 100,
        "min": 0,
        "onSlide": AS_Slider_ba6b7476148346be8b0e3ddb48374e76,
        "rightSkin": "slSliderRightBlue",
        "selectedValue": 40,
        "step": 1,
        "thumbImage": "slider_ios7.png",
        "top": "-3%",
        "width": "90%",
        "zIndex": 1
    }, {}, {
        "thumbTintColor": "ffffff00"
    });
    flxWithdrawalPOSlimit.add(flxWithdrawalLimit, sliderATMLimit, flxPOSLimit, sliderPOSLimit);
    var btnUpdateLimit = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "90%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "10%",
        "id": "btnUpdateLimit",
        "isVisible": true,
        "onClick": AS_Button_g3dd2d9ed8a2424cb3172aa17f54f678,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.atmpos.updatelimit"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxBody.add(flxToggle, flxWithdrawalPOSlimit, btnUpdateLimit);
    frmATMPOSLimit.add(flxHeader, flxBody);
};

function frmATMPOSLimitGlobals() {
    frmATMPOSLimit = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmATMPOSLimit,
        "allowHorizontalBounce": false,
        "allowVerticalBounce": false,
        "bounces": false,
        "enabledForIdleTimeout": false,
        "id": "frmATMPOSLimit",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "postShow": AS_Form_cb073edae53142e2873a65545b38bf17,
        "skin": "sknmainGradient",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};