//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmFilterSIAr() {
frmFilterSI.setDefaultUnit(kony.flex.DP);
var flxFilterTransactionHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxFilterTransactionHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFilterTransactionHeader.setDefaultUnit(kony.flex.DP);
var flxFilterBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxFilterBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_cb76f3597ca64c1a896b8063355d00e1,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxFilterBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxFilterBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
var lblFilterHeader = new kony.ui.Label({
"centerX": "50%",
"height": "90%",
"id": "lblFilterHeader",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxFilterDone = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxFilterDone",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_g025c7184c0e44e0aeef33c5e519337c,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxFilterDone.setDefaultUnit(kony.flex.DP);
var Label0d6e6be1b179e44 = new kony.ui.Label({
"centerX": "49%",
"centerY": "50%",
"id": "Label0d6e6be1b179e44",
"isVisible": true,
"skin": "sknCarioSemiBold130GreenYellow",
"text": kony.i18n.getLocalizedString("i18n.overview.buttonDone"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [2, 1, 2, 1],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxFilterDone.add(Label0d6e6be1b179e44);
flxFilterTransactionHeader.add(flxFilterBack, lblFilterHeader, flxFilterDone);
var flxFilterTransaction = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxFilterTransaction",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFilterTransaction.setDefaultUnit(kony.flex.DP);
var Label0ff51e386d98042 = new kony.ui.Label({
"id": "Label0ff51e386d98042",
"isVisible": false,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.timeperiod"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxtimeperiod = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "7%",
"id": "flxtimeperiod",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"onClick": AS_FlexContainer_e623a58abed2467b883cd46b7462f8b9,
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxtimeperiod.setDefaultUnit(kony.flex.DP);
var Image0i195c328eee24b = new kony.ui.Image2({
"height": "70%",
"id": "Image0i195c328eee24b",
"isVisible": true,
"right": "5%",
"skin": "slImage",
"src": "cal.png",
"top": "15%",
"width": "8%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblDateRange = new kony.ui.Label({
"height": "70%",
"id": "lblDateRange",
"isVisible": true,
"right": "15%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.pickadate"),
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var Image0e7683ea123f546 = new kony.ui.Image2({
"height": "60%",
"id": "Image0e7683ea123f546",
"isVisible": true,
"left": "5%",
"skin": "slImage",
"src": "map_drilldown.png",
"top": "20%",
"width": "8%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var FlexContainer0b96ba0e3f06e4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "1.50%",
"id": "FlexContainer0b96ba0e3f06e4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0b96ba0e3f06e4a.setDefaultUnit(kony.flex.DP);
FlexContainer0b96ba0e3f06e4a.add();
flxtimeperiod.add(Image0i195c328eee24b, lblDateRange, Image0e7683ea123f546, FlexContainer0b96ba0e3f06e4a);
var lblAmountRange = new kony.ui.Label({
"id": "lblAmountRange",
"isVisible": true,
"right": "5.00%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.search.amountRange"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var FlexContainer0b0328c9555bb43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "FlexContainer0b0328c9555bb43",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
FlexContainer0b0328c9555bb43.setDefaultUnit(kony.flex.DP);
var txtAmountRangeFrom = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "sknCarioRegular125OPc50",
"height": "60%",
"id": "txtAmountRangeFrom",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "5%",
"onDone": AS_TextField_c5b6b757a9414e04bf4805be62e21103,
"onTextChange": AS_TextField_g7755c71608844039410545beefa9db4,
"placeholder": "0.000",
"secureTextEntry": false,
"skin": "sknCarioRegular125OPc50",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "30%",
"width": "25%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var FlexContainer0cf44c25bf9c24e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "40%",
"centerY": "63%",
"clipBounds": true,
"height": "2%",
"id": "FlexContainer0cf44c25bf9c24e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"width": "8%",
"zIndex": 1
}, {}, {});
FlexContainer0cf44c25bf9c24e.setDefaultUnit(kony.flex.DP);
FlexContainer0cf44c25bf9c24e.add();
var txtAmountRangeTo = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "sknCarioRegular125OPc50",
"height": "60%",
"id": "txtAmountRangeTo",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "50%",
"onDone": AS_TextField_j85da92a09c34e6f8317f10a23bc39a6,
"onTextChange": AS_TextField_bc4ded162d684558b1f082b71c78080c,
"placeholder": "0.000",
"secureTextEntry": false,
"skin": "sknCarioRegular125OPc50",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "30%",
"width": "25%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblCurrency = new kony.ui.Label({
"centerX": "85.00%",
"centerY": "60%",
"id": "lblCurrency",
"isVisible": true,
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.common.currencySymbol"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAmountMinLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "7%",
"clipBounds": true,
"height": "2%",
"id": "flxAmountMinLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "skntextFieldDivider",
"width": "28%",
"zIndex": 1
}, {}, {});
flxAmountMinLine.setDefaultUnit(kony.flex.DP);
flxAmountMinLine.add();
var flxAmountMaxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "7%",
"clipBounds": true,
"height": "2%",
"id": "flxAmountMaxLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "48%",
"skin": "skntextFieldDivider",
"width": "28%",
"zIndex": 1
}, {}, {});
flxAmountMaxLine.setDefaultUnit(kony.flex.DP);
flxAmountMaxLine.add();
FlexContainer0b0328c9555bb43.add(txtAmountRangeFrom, FlexContainer0cf44c25bf9c24e, txtAmountRangeTo, lblCurrency, flxAmountMinLine, flxAmountMaxLine);
flxFilterTransaction.add(Label0ff51e386d98042, flxtimeperiod, lblAmountRange, FlexContainer0b0328c9555bb43);
frmFilterSI.add(flxFilterTransactionHeader, flxFilterTransaction);
};
function frmFilterSIGlobalsAr() {
frmFilterSIAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmFilterSIAr,
"enabledForIdleTimeout": true,
"id": "frmFilterSI",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_j3cba81964c240f488bc696ef36f721d,
"skin": "sknBackground"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NEXTPREV,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
