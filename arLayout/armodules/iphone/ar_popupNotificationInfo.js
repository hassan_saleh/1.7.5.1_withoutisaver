//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetspopupNotificationInfoAr() {
var hbxAlertHeader = new kony.ui.Box({
"id": "hbxAlertHeader",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 1, 2,2, 2],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
var vbxNotificationIcon = new kony.ui.Box({
"id": "vbxNotificationIcon",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_VERTICAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "sknVBXRNDTRANSPARENT"
}, {
"containerWeight": 13,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 1, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var imgNotificationIcon = new kony.ui.Image2({
"id": "imgNotificationIcon",
"isVisible": true,
"skin": "slImage",
"src": "appicon_98.png"
}, {
"containerWeight": 100,
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
vbxNotificationIcon.add(imgNotificationIcon);
var lblAlertHeader = new kony.ui.Label({
"id": "lblAlertHeader",
"isVisible": true,
"skin": "sknCarioRegular120White70Opc",
"text": "Test Message"
}, {
"containerWeight": 74,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"hExpand": true,
"margin": [ 1, 1,3, 1],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var vbxCommonAlertClose = new kony.ui.Box({
"id": "vbxCommonAlertClose",
"isVisible": true,
"onClick": AS_Button_d3045aa8403f4be5b6be67af835bedd7,
"orientation": constants.BOX_LAYOUT_VERTICAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slVbox"
}, {
"containerWeight": 10,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 1, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_RIGHT
}, {});
var imgClose = new kony.ui.Image2({
"id": "imgClose",
"isVisible": true,
"skin": "slImage",
"src": "close_ad.png"
}, {
"containerWeight": 100,
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
vbxCommonAlertClose.add(imgClose);
hbxAlertHeader.add( vbxCommonAlertClose, lblAlertHeader,vbxNotificationIcon);
var line1 = new kony.ui.Line({
"id": "line1",
"isVisible": true,
"skin": "sknLineWHITE"
}, {
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"thickness": 1
}, {});
var hbxAlertBody = new kony.ui.Box({
"id": "hbxAlertBody",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 3, 3,3, 5],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var lblDescription = new kony.ui.Label({
"id": "lblDescription",
"isVisible": true,
"skin": "sknCarioRegular120White70Opc",
"text": "Welcome to the mobiel application of Bank of Jordan. We are heartly welcoming the user and giving away so many offers to new users, like free lamborgini SV and buggati chiron for the one who is making atleast of 1JOD trasaction perday."
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"hExpand": true,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
hbxAlertBody.add(lblDescription);
var hbxSingleOk = new kony.ui.Box({
"id": "hbxSingleOk",
"isVisible": false,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "sknGradientBlueLight"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 2],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
var Label0f420ba73f3ba40 = new kony.ui.Label({
"id": "Label0f420ba73f3ba40",
"isVisible": true,
"skin": "sknTrans"
}, {
"containerWeight": 25,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"hExpand": true,
"margin": [ 1, 1,1, 1],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnSingleOkay = new kony.ui.Button({
"focusSkin": "slButtonBlueFocus",
"id": "btnSingleOkay",
"isVisible": true,
"skin": "sknWhiteBGBlueFont",
"text": kony.i18n.getLocalizedString("i18n.NUO.OKay")
}, {
"containerWeight": 50,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"hExpand": true,
"margin": [ 5, 2,5, 2],
"marginInPixel": false,
"padding": [ 1, 1,1, 1],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"showProgressIndicator": true
});
var CopyLabel0fc57b930c2d740 = new kony.ui.Label({
"id": "CopyLabel0fc57b930c2d740",
"isVisible": true,
"skin": "sknTrans"
}, {
"containerWeight": 25,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"hExpand": true,
"margin": [ 1, 1,1, 1],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
hbxSingleOk.add( CopyLabel0fc57b930c2d740, btnSingleOkay,Label0f420ba73f3ba40);
popupNotificationInfo.add(hbxAlertHeader, line1, hbxAlertBody, hbxSingleOk);
};
function popupNotificationInfoGlobalsAr() {
popupNotificationInfoAr = new kony.ui.Popup({
"addWidgets": addWidgetspopupNotificationInfoAr,
"id": "popupNotificationInfo",
"isModal": true,
"skin": "sknPopupDarkBlue",
"transparencyBehindThePopup": 40
}, {
"containerHeight": null,
"containerHeightReference": constants.CONTAINER_HEIGHT_BY_FORM_REFERENCE,
"containerWeight": 80,
"padding": [ 0, 0,0, 5],
"paddingInPixel": false
}, {
"bounces": false,
"configureExtendTop": false,
"footerOverlap": false,
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL
});
};
