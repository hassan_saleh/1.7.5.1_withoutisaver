//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function initializeiphoneFooterAr() {
footerBackAr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "7%",
"id": "footerBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "skncontainerBkgWhite",
"top": "0",
"width": "100%"
}, {}, {});
footerBackAr.setDefaultUnit(kony.flex.DP);
var footerBackground = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "footerBackground",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "menu",
"top": "0",
"width": "100%"
}, {}, {});
footerBackground.setDefaultUnit(kony.flex.DP);
var FlxAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxAccounts",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxAccounts.setDefaultUnit(kony.flex.DP);
var img1 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img1",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_accounts_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label03174bff69bb54c = new kony.ui.Label({
"centerX": "50%",
"id": "Label03174bff69bb54c",
"isVisible": true,
"skin": "sknlblFootertitleFocus",
"text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnAccounts = new kony.ui.Button({
"accessibilityConfig": {
"a11yLabel": "Accounts"
},
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnfooterenablefont",
"height": "50dp",
"id": "btnAccounts",
"isVisible": true,
"onClick": AS_Button_f7ae49a6453c4a1cb1a64adfbb07648e,
"skin": "btnFooterDisableaccount",
"text": "H",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 12],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlxAccounts.add(img1, Label03174bff69bb54c, btnAccounts);
var FlxTranfers = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxTranfers",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_b36dd897d92442289c9ac539a9ece78b,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxTranfers.setDefaultUnit(kony.flex.DP);
var img2 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img2",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_t_and_p_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label02bec01fd5baf4c = new kony.ui.Label({
"centerX": "50%",
"id": "Label02bec01fd5baf4c",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopybtnAccounts0ia3b1ff37c304b = new kony.ui.Button({
"accessibilityConfig": {
"a11yLabel": "Cards"
},
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnFooterDisablefocusskin",
"height": "50dp",
"id": "CopybtnAccounts0ia3b1ff37c304b",
"isVisible": true,
"onClick": AS_Button_f0ac3a0109224e5584dc359b0d32304f,
"skin": "btnFooterDisable",
"text": "G",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlxTranfers.add(img2, Label02bec01fd5baf4c, CopybtnAccounts0ia3b1ff37c304b);
var FlxBot = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxBot",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_ef4f926e056b46dcbca0b4e17991191b,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 2
}, {}, {});
FlxBot.setDefaultUnit(kony.flex.DP);
var imgBot = new kony.ui.Image2({
"centerX": "50%",
"height": "40dp",
"id": "imgBot",
"isVisible": false,
"right": "13dp",
"skin": "slImage",
"src": "chaticonactive.png",
"top": "4dp",
"width": "40dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnAccounts0ff48f9feb0aa42 = new kony.ui.Button({
"accessibilityConfig": {
"a11yLabel": "Transfer"
},
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "CopybtnAccounts0ff48f9feb0aa42",
"isVisible": true,
"onClick": AS_Button_ebd8d1a83e9944e88dfb097712fcbe46,
"skin": "btnCard",
"text": "i",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var CopyLabel0i584dcaf2e8546 = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel0i584dcaf2e8546",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.alert.transfers"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlxBot.add(imgBot, CopybtnAccounts0ff48f9feb0aa42, CopyLabel0i584dcaf2e8546);
var FlxDeposits = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxDeposits",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_ddf914cbda2c4ae4b9c9d9b8e402b1c1,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxDeposits.setDefaultUnit(kony.flex.DP);
var img3 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img3",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_deposits_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label04221a71494e848 = new kony.ui.Label({
"centerX": "50%",
"id": "Label04221a71494e848",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.deposits"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopybtnAccounts0bc68a97c14fb49 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0bc68a97c14fb49",
"isVisible": true,
"skin": "btnCard",
"text": "J",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlxDeposits.add(img3, Label04221a71494e848, CopybtnAccounts0bc68a97c14fb49);
var flxEfawaterccom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxEfawaterccom",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "23%",
"zIndex": 2
}, {}, {});
flxEfawaterccom.setDefaultUnit(kony.flex.DP);
var btnEfawatercoom = new kony.ui.Button({
"accessibilityConfig": {
"a11yLabel": "E fawateercom"
},
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnEfawatercoom",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_a41f72bf153d40dfb4b5f00fd538b8f1,
"skin": "btnCard",
"text": "x",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblEfawatercoom = new kony.ui.Label({
"centerX": "50%",
"id": "lblEfawatercoom",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.footerBill.BillPayment"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxEfawaterccom.add(btnEfawatercoom, lblEfawatercoom);
var FlxMore = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxMore",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_4fe8de62a3864b8c9ac974efbf4d7217,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxMore.setDefaultUnit(kony.flex.DP);
var img4 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img4",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_more_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label0e5331028c2ef41 = new kony.ui.Label({
"centerX": "51%",
"id": "Label0e5331028c2ef41",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopybtnAccounts0ec4d23080f0146 = new kony.ui.Button({
"accessibilityConfig": {
"a11yLabel": "More"
},
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0ec4d23080f0146",
"isVisible": true,
"onClick": AS_Button_g98cf3b49bff4492bbd7454e5452c32c,
"skin": "btnCard",
"text": "K",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlxMore.add(img4, Label0e5331028c2ef41, CopybtnAccounts0ec4d23080f0146);
footerBackground.add( FlxMore, flxEfawaterccom, FlxDeposits, FlxBot, FlxTranfers,FlxAccounts);
footerBackAr.add(footerBackground);
}
