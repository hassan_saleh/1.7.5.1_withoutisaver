//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:02 EET 2020
function addWidgetsfrmAddExternalAccountKAAr() {
frmAddExternalAccountKA.setDefaultUnit(kony.flex.DP);
var titleBarAddExternalAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "titleBarAddExternalAccount",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntitleBarGradient",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarAddExternalAccount.setDefaultUnit(kony.flex.DP);
var CopytransferPayTitleLabel03db1989272ff4d = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "CopytransferPayTitleLabel03db1989272ff4d",
"isVisible": true,
"skin": "sknnavBarTitle",
"text": kony.i18n.getLocalizedString("i18n.common.addExternalAccount"),
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopybuttonLeft08e6c43ee6f1641 = new kony.ui.Button({
"focusSkin": "skntitleBarTextButtonFocus",
"height": "50dp",
"id": "CopybuttonLeft08e6c43ee6f1641",
"isVisible": true,
"right": "0dp",
"minWidth": "50dp",
"onClick": AS_Button_9675cdf333b34c9db2058d9e1e735424,
"skin": "skntitleBarTextButton",
"text": kony.i18n.getLocalizedString("i18n.common.cancel"),
"top": "0dp",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
titleBarAddExternalAccount.add(CopytransferPayTitleLabel03db1989272ff4d, CopybuttonLeft08e6c43ee6f1641);
var mainScrollContainer = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": 0,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "mainScrollContainer",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkg",
"top": "50dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
mainScrollContainer.setDefaultUnit(kony.flex.DP);
var externalAccountType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "externalAccountType",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "-3dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
externalAccountType.setDefaultUnit(kony.flex.DP);
var FlexContainer022833a88903841 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlexContainer022833a88903841",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "50%",
"zIndex": 1
}, {}, {});
FlexContainer022833a88903841.setDefaultUnit(kony.flex.DP);
var ImgDomestic = new kony.ui.Image2({
"centerY": "50%",
"height": "100%",
"id": "ImgDomestic",
"isVisible": true,
"right": "7%",
"onTouchEnd": AS_Image_a502a747ef17481caac5091c095f0dce,
"skin": "slImage",
"src": "radioselected.png",
"top": "13dp",
"width": "20dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label0791365e7c0714c = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "Label0791365e7c0714c",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.externalaccount.Domestic"),
"top": "0dp",
"width": "75%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlexContainer022833a88903841.add( Label0791365e7c0714c,ImgDomestic);
var CopyFlexContainer060dcfde25c3d43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyFlexContainer060dcfde25c3d43",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "50%",
"zIndex": 1
}, {}, {});
CopyFlexContainer060dcfde25c3d43.setDefaultUnit(kony.flex.DP);
var ImgInternational = new kony.ui.Image2({
"centerY": "50%",
"height": "100%",
"id": "ImgInternational",
"isVisible": true,
"right": "7%",
"onTouchEnd": AS_Image_a4293f32da08421f862b5d4e7175df39,
"skin": "slImage",
"src": "radiononselected.png",
"top": "13dp",
"width": "20dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabel02fdfe67554874a = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopyLabel02fdfe67554874a",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.transfer.International"),
"top": "0dp",
"width": "75%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer060dcfde25c3d43.add( CopyLabel02fdfe67554874a,ImgInternational);
externalAccountType.add( CopyFlexContainer060dcfde25c3d43,FlexContainer022833a88903841);
var CopyborderBottom0008e9525288547 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom0008e9525288547",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknpickBorder",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom0008e9525288547.setDefaultUnit(kony.flex.DP);
CopyborderBottom0008e9525288547.add();
var CopyLabel03e39ab4661a845 = new kony.ui.Label({
"centerX": "50%",
"height": "40dp",
"id": "CopyLabel03e39ab4661a845",
"isVisible": true,
"right": "0%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "0dp",
"width": "96%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyborderBottom067eebb20425141 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom067eebb20425141",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknpickBorder",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom067eebb20425141.setDefaultUnit(kony.flex.DP);
CopyborderBottom067eebb20425141.add();
var externalAccountCountry = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "externalAccountCountry",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
externalAccountCountry.setDefaultUnit(kony.flex.DP);
var CopyLabel0b353ede7efad4c = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0b353ede7efad4c",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.managePayee.CountryName"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var externalAccountNameTextField = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerX": "50%",
"focusSkin": "skngeneralTextFieldFocus",
"height": "38dp",
"id": "externalAccountNameTextField",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"placeholder": "India",
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "35dp",
"width": "90%"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var ListCountryKA = new kony.ui.ListBox({
"centerX": "50%",
"height": "38dp",
"id": "ListCountryKA",
"isVisible": true,
"masterData": [["3", "Austin"],["2", "US"],["1", "India"]],
"onSelection": AS_ListBox_d261bf65e17e49448939ff53a156e42c,
"skin": "slListBox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"dropDownImage": "dropdown.png",
"groupCells": false,
"viewConfig": {
"toggleViewConfig": {
"viewStyle": constants.LISTBOX_TOGGLE_VIEW_STYLE_PLAIN
}
},
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
var CopyborderBottom0a9a2bb59bddb4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom0a9a2bb59bddb4c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknpickBorder",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom0a9a2bb59bddb4c.setDefaultUnit(kony.flex.DP);
CopyborderBottom0a9a2bb59bddb4c.add();
externalAccountCountry.add(CopyLabel0b353ede7efad4c, externalAccountNameTextField, ListCountryKA, CopyborderBottom0a9a2bb59bddb4c);
var CopyexternalAccountNameContainer02cb7c7ca6e1a48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "CopyexternalAccountNameContainer02cb7c7ca6e1a48",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyexternalAccountNameContainer02cb7c7ca6e1a48.setDefaultUnit(kony.flex.DP);
var CopyLabel0010f3d10f13a48 = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0010f3d10f13a48",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.transfer.BeneficiaryName"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtBenficiryNameKAOld = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_WORDS,
"centerX": "50%",
"focusSkin": "skngeneralTextFieldFocus",
"height": "38dp",
"id": "txtBenficiryNameKAOld",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"placeholder": kony.i18n.getLocalizedString("i18n.account.enterNameBankRec"),
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "35dp",
"width": "90%"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyborderBottom0d3bf42d5faa845 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom0d3bf42d5faa845",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknpickBorder",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom0d3bf42d5faa845.setDefaultUnit(kony.flex.DP);
CopyborderBottom0d3bf42d5faa845.add();
CopyexternalAccountNameContainer02cb7c7ca6e1a48.add(CopyLabel0010f3d10f13a48, txtBenficiryNameKAOld, CopyborderBottom0d3bf42d5faa845);
var CopyexternalAccountNameContainer03d9566d885a944 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "CopyexternalAccountNameContainer03d9566d885a944",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyexternalAccountNameContainer03d9566d885a944.setDefaultUnit(kony.flex.DP);
var CopyLabel0f562082b4d5a4a = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0f562082b4d5a4a",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.account.accountNickName"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtAccountNickNameKA = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_WORDS,
"centerX": "50%",
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "txtAccountNickNameKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"placeholder": kony.i18n.getLocalizedString("i18n.transfer.Giveanickname"),
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "35dp",
"width": "90%"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyborderBottom097215727503242 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom097215727503242",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknpickBorder",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom097215727503242.setDefaultUnit(kony.flex.DP);
CopyborderBottom097215727503242.add();
CopyexternalAccountNameContainer03d9566d885a944.add(CopyLabel0f562082b4d5a4a, txtAccountNickNameKA, CopyborderBottom097215727503242);
var externalAccountTypeContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": false,
"id": "externalAccountTypeContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknCopyslFbox007a422d071dd4e",
"top": "0dp",
"width": "100%"
}, {}, {});
externalAccountTypeContainer.setDefaultUnit(kony.flex.DP);
var Label09bf7df0663964f = new kony.ui.Label({
"height": "50dp",
"id": "Label09bf7df0663964f",
"isVisible": true,
"right": "0dp",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.overview.accountType"),
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var externalAccountTypeSegment = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50.00%",
"data": [{
"imgicontick": "check_blue.png",
"lblNameKA": "Savings Account"
}, {
"imgicontick": "",
"lblNameKA": "Checking  Account"
}],
"groupCells": false,
"id": "externalAccountTypeSegment",
"isVisible": true,
"needPageIndicator": true,
"onRowClick": AS_Segment_8b9374c4e0ba4a2e97550f8dba7609e6,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowTemplate": CopyFlexContainer0b2b1c26ffbf74f,
"scrollingEvents": {},
"selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": false,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer0b2b1c26ffbf74f": "CopyFlexContainer0b2b1c26ffbf74f",
"contactListDivider": "contactListDivider",
"imgicontick": "imgicontick",
"lblNameKA": "lblNameKA"
},
"width": "95%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": false
});
var AccountTypePick = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"focusSkin": "sknyourAccountCardFocus",
"height": "60dp",
"id": "AccountTypePick",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntransferPayCardPick",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
AccountTypePick.setDefaultUnit(kony.flex.DP);
var fromAccountNameContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "fromAccountNameContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "sknslFbox",
"width": "44%",
"zIndex": 1
}, {}, {});
fromAccountNameContainer.setDefaultUnit(kony.flex.DP);
var fromNamePick = new kony.ui.Label({
"centerY": "33%",
"height": "20dp",
"id": "fromNamePick",
"isVisible": true,
"right": "13dp",
"maxWidth": "90%",
"skin": "sknaccountNamePick",
"text": kony.i18n.getLocalizedString("i18n.common.accountName"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblFromAccountBankNameKA = new kony.ui.Label({
"centerY": "75%",
"id": "lblFromAccountBankNameKA",
"isVisible": true,
"right": "13dp",
"skin": "skntransLeftLabel",
"text": kony.i18n.getLocalizedString("i18n.common.savingsAccount"),
"top": "55dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
fromAccountNameContainer.add(fromNamePick, lblFromAccountBankNameKA);
var CopyborderBottom0f914e305289b4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom0f914e305289b4b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknpickBorder",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom0f914e305289b4b.setDefaultUnit(kony.flex.DP);
CopyborderBottom0f914e305289b4b.add();
var editFromCard = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "skneditFormFocus",
"height": "100%",
"id": "editFromCard",
"isVisible": true,
"left": "0dp",
"skin": "skneditForm",
"text": kony.i18n.getLocalizedString("i18n.common.edit"),
"width": "93dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var fromlblAccountNumberKA = new kony.ui.Label({
"id": "fromlblAccountNumberKA",
"isVisible": false,
"right": "115dp",
"skin": "slLabel",
"text": "Label",
"top": "19dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
AccountTypePick.add(fromAccountNameContainer, CopyborderBottom0f914e305289b4b, editFromCard, fromlblAccountNumberKA);
var lblAccountTypeKA = new kony.ui.Label({
"id": "lblAccountTypeKA",
"isVisible": true,
"right": "23dp",
"skin": "slLabel",
"text": "BOJ",
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCountryNameKAOld = new kony.ui.Label({
"id": "lblCountryNameKAOld",
"isVisible": true,
"right": "23dp",
"skin": "slLabel",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblInternaionalAccountKA = new kony.ui.Label({
"id": "lblInternaionalAccountKA",
"isVisible": false,
"right": "23dp",
"skin": "slLabel",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
externalAccountTypeContainer.add(Label09bf7df0663964f, externalAccountTypeSegment, AccountTypePick, lblAccountTypeKA, lblCountryNameKAOld, lblInternaionalAccountKA);
var CopyborderBottom05049ab71905c44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom05049ab71905c44",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknpickBorder",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom05049ab71905c44.setDefaultUnit(kony.flex.DP);
CopyborderBottom05049ab71905c44.add();
var externalAccountNumberContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "externalAccountNumberContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "10dp",
"width": "100%",
"zIndex": 1
}, {}, {});
externalAccountNumberContainer.setDefaultUnit(kony.flex.DP);
var CopyLabel063c64beaee2545 = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel063c64beaee2545",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAccnoErr = new kony.ui.Label({
"height": "32dp",
"id": "lblAccnoErr",
"isVisible": false,
"left": "5%",
"skin": "sknD0021BFLatoRegular",
"text": "Invalid Account Number",
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var accountNumberContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "accountNumberContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
accountNumberContainer.setDefaultUnit(kony.flex.DP);
var externalAccountNumberTextFieldOld = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "externalAccountNumberTextFieldOld",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.overview.enterAccountNumberPlh"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
accountNumberContainer.add(externalAccountNumberTextFieldOld);
var CopyborderBottom0a26a506ce2d546 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom0a26a506ce2d546",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknpickBorder",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom0a26a506ce2d546.setDefaultUnit(kony.flex.DP);
CopyborderBottom0a26a506ce2d546.add();
externalAccountNumberContainer.add(CopyLabel063c64beaee2545, lblAccnoErr, accountNumberContainer, CopyborderBottom0a26a506ce2d546);
var externalRountingNumberContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "externalRountingNumberContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
externalRountingNumberContainer.setDefaultUnit(kony.flex.DP);
var CopyLabel0a393696e101f42 = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0a393696e101f42",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.transfer.RoutingNumber"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var routingNumberContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "routingNumberContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
routingNumberContainer.setDefaultUnit(kony.flex.DP);
var txtExternalRoutingNumberKA = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "txtExternalRoutingNumberKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.overview.enterRoutingNumberPlh"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
routingNumberContainer.add(txtExternalRoutingNumberKA);
var lblRoutingnoErr = new kony.ui.Label({
"height": "32dp",
"id": "lblRoutingnoErr",
"isVisible": false,
"left": "5%",
"skin": "sknD0021BFLatoRegular",
"text": "Invalid Routing Number",
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyborderBottom0h7be17a2ef9247 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom0h7be17a2ef9247",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknpickBorder",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom0h7be17a2ef9247.setDefaultUnit(kony.flex.DP);
CopyborderBottom0h7be17a2ef9247.add();
externalRountingNumberContainer.add(CopyLabel0a393696e101f42, routingNumberContainer, lblRoutingnoErr, CopyborderBottom0h7be17a2ef9247);
var externalSwiftCodeKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "externalSwiftCodeKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
externalSwiftCodeKA.setDefaultUnit(kony.flex.DP);
var CopyLabel0cca3849cee9048 = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0cca3849cee9048",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.transfer.SwiftCode"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyroutingNumberContainer0b50c393f8dac40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "CopyroutingNumberContainer0b50c393f8dac40",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyroutingNumberContainer0b50c393f8dac40.setDefaultUnit(kony.flex.DP);
var txtSwiftCodeKAOld = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "txtSwiftCodeKAOld",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.account.enterSwiftCode"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
CopyroutingNumberContainer0b50c393f8dac40.add(txtSwiftCodeKAOld);
var CopyborderBottom0795c152f323b46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom0795c152f323b46",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknpickBorder",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom0795c152f323b46.setDefaultUnit(kony.flex.DP);
CopyborderBottom0795c152f323b46.add();
externalSwiftCodeKA.add(CopyLabel0cca3849cee9048, CopyroutingNumberContainer0b50c393f8dac40, CopyborderBottom0795c152f323b46);
var CopybuttonWrapper076c20712f8344b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "150dp",
"id": "CopybuttonWrapper076c20712f8344b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopybuttonWrapper076c20712f8344b.setDefaultUnit(kony.flex.DP);
var saveExternalAccountButton = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknprimaryActionFocus",
"height": "42dp",
"id": "saveExternalAccountButton",
"isVisible": true,
"onClick": AS_Button_7ea8bea45e4a4de2b921c7d6459843ca,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.overview.saveExternalAccount"),
"top": "24dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
CopybuttonWrapper076c20712f8344b.add(saveExternalAccountButton);
mainScrollContainer.add(externalAccountType, CopyborderBottom0008e9525288547, CopyLabel03e39ab4661a845, CopyborderBottom067eebb20425141, externalAccountCountry, CopyexternalAccountNameContainer02cb7c7ca6e1a48, CopyexternalAccountNameContainer03d9566d885a944, externalAccountTypeContainer, CopyborderBottom05049ab71905c44, externalAccountNumberContainer, externalRountingNumberContainer, externalSwiftCodeKA, CopybuttonWrapper076c20712f8344b);
var flxConfirmPopUp = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxConfirmPopUp",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknFlexBlueBen",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxConfirmPopUp.setDefaultUnit(kony.flex.DP);
var flxConfirmHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxConfirmHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmHeader.setDefaultUnit(kony.flex.DP);
var flxHeaderBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxHeaderBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_e39ab003e52d4eb1a6335efa1d42442a,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxHeaderBack.setDefaultUnit(kony.flex.DP);
var lblHeaderBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblHeaderBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHeaderBack.add(lblHeaderBackIcon, lblHeaderBack);
var lblHeaderTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblHeaderTitle",
"isVisible": true,
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnClose = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknDeleteFocus",
"height": "80%",
"id": "btnClose",
"isVisible": true,
"left": "85%",
"onClick": AS_Button_ae42b05e3f1a40e496fde8ab3005b258,
"skin": "sknBtnBack",
"text": "O",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxConfirmHeader.add(flxHeaderBack, lblHeaderTitle, btnClose);
var flxImpDetail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "25%",
"id": "flxImpDetail",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxImpDetail.setDefaultUnit(kony.flex.DP);
var flxIcon1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50dp",
"id": "flxIcon1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxToIcon",
"top": "7%",
"width": "50dp",
"zIndex": 1
}, {}, {});
flxIcon1.setDefaultUnit(kony.flex.DP);
var lblInitial = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "80%",
"id": "lblInitial",
"isVisible": true,
"skin": "sknLblFromIcon",
"text": "RJ",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIcon1.add(lblInitial);
var lblBeneName = new kony.ui.Label({
"centerX": "50%",
"id": "lblBeneName",
"isVisible": true,
"skin": "sknBeneTitle",
"text": "Ragnar Jonasson",
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBeneAccNum = new kony.ui.Label({
"centerX": "50%",
"id": "lblBeneAccNum",
"isVisible": true,
"right": "0dp",
"skin": "sknLblNextDisabled",
"text": "SE9717065746587236898695",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxImpDetail.add(flxIcon1, lblBeneName, lblBeneAccNum);
var flxOtherDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxOtherDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOtherDetails.setDefaultUnit(kony.flex.DP);
var flxNickName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxNickName",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNickName.setDefaultUnit(kony.flex.DP);
var lblNickNameTitle = new kony.ui.Label({
"height": "50%",
"id": "lblNickNameTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblNickName = new kony.ui.Label({
"height": "50%",
"id": "lblNickName",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNickName.add(lblNickNameTitle, lblNickName);
var flxConfirmAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxConfirmAddress",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmAddress.setDefaultUnit(kony.flex.DP);
var lblConfirmAddTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmAddTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.address"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmAddr = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmAddr",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Sweden, Stockholm, 12 Holländargatan",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmAddress.add(lblConfirmAddTitle, lblConfirmAddr);
var flxConfirmSwift = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxConfirmSwift",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmSwift.setDefaultUnit(kony.flex.DP);
var lblConfirmSwiftTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmSwiftTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.BankSWIFT"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmSwift = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmSwift",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "79831222",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmSwift.add(lblConfirmSwiftTitle, lblConfirmSwift);
var flxConfirmBankName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxConfirmBankName",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmBankName.setDefaultUnit(kony.flex.DP);
var lblConfirmBankNameTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmBankNameTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmBankName = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmBankName",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Sweden Bank",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmBankName.add(lblConfirmBankNameTitle, lblConfirmBankName);
var flxBankBranchConf = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxBankBranchConf",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBankBranchConf.setDefaultUnit(kony.flex.DP);
var lblBankBranchTitle = new kony.ui.Label({
"height": "50%",
"id": "lblBankBranchTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.BankBranch"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBankBranchConf = new kony.ui.Label({
"height": "50%",
"id": "lblBankBranchConf",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBankBranchConf.add(lblBankBranchTitle, lblBankBranchConf);
var flxCityConf = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxCityConf",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCityConf.setDefaultUnit(kony.flex.DP);
var lblCityTitle = new kony.ui.Label({
"height": "50%",
"id": "lblCityTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.manage_payee.cityPlh"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCity = new kony.ui.Label({
"height": "50%",
"id": "lblCity",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCityConf.add(lblCityTitle, lblCity);
var flxConfirmCountry = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxConfirmCountry",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmCountry.setDefaultUnit(kony.flex.DP);
var lblConfirmCountryTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmCountryTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.NUO.Country"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmCountry = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmCountry",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Sweden",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmCountry.add(lblConfirmCountryTitle, lblConfirmCountry);
var flxConfirmEmail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxConfirmEmail",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmEmail.setDefaultUnit(kony.flex.DP);
var lblConfirmEmailTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmEmailTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Beneficiaryemail"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmEmail = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmEmail",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "someone@something.somecom",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmEmail.add(lblConfirmEmailTitle, lblConfirmEmail);
flxOtherDetails.add(flxNickName, flxConfirmAddress, flxConfirmSwift, flxConfirmBankName, flxBankBranchConf, flxCityConf, flxConfirmCountry, flxConfirmEmail);
var flxButtonHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "16%",
"id": "flxButtonHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxButtonHolder.setDefaultUnit(kony.flex.DP);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "50dp",
"id": "btnConfirm",
"isVisible": true,
"onClick": AS_Button_j3a396c4a6db4b0c8b31e56c2286117f,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxButtonHolder.add(btnConfirm);
var lblLanguage = new kony.ui.Label({
"id": "lblLanguage",
"isVisible": true,
"right": "-140dp",
"skin": "slLabel",
"text": "EN",
"top": "-700dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblFavourite = new kony.ui.Label({
"id": "lblFavourite",
"isVisible": true,
"right": "-130dp",
"skin": "slLabel",
"text": "true",
"top": "-690dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBranchNumber = new kony.ui.Label({
"id": "lblBranchNumber",
"isVisible": true,
"right": "-120dp",
"skin": "slLabel",
"text": "1232435",
"top": "-680dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCurrency = new kony.ui.Label({
"id": "lblCurrency",
"isVisible": true,
"right": "-120dp",
"skin": "slLabel",
"text": "USD",
"top": "-680dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmPopUp.add(flxConfirmHeader, flxImpDetail, flxOtherDetails, flxButtonHolder, lblLanguage, lblFavourite, lblBranchNumber, lblCurrency);
var flxFormMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxFormMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFormMain.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxTop = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxTop",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTop.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_g64d54efde31407e89f31c84d36cf94c,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "70%",
"id": "lblTitle",
"isVisible": true,
"maxNumberOfLines": 1,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.Bene.AddanewbeneficiarySmall"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblNext = new kony.ui.Label({
"centerY": "50%",
"id": "lblNext",
"isVisible": true,
"left": "86%",
"onTouchEnd": AS_Label_af07d11947354013927e48e9b8edae60,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTop.add(flxBack, lblTitle, lblNext);
var flxTab = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40%",
"id": "flxTab",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "5%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTab.setDefaultUnit(kony.flex.DP);
var flxContent = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "40%",
"clipBounds": true,
"height": "65%",
"id": "flxContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFboxOuterRing",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxContent.setDefaultUnit(kony.flex.DP);
var btnBOJ = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnBOJ",
"isVisible": true,
"onClick": AS_Button_fc4ea37e0cd8476485dcf072a71a2e5c,
"skin": "slButtonWhiteTab",
"text": kony.i18n.getLocalizedString("i18n.Bene.BOJ"),
"top": "0dp",
"width": "33%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnDomestic = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnDomestic",
"isVisible": true,
"onClick": AS_Button_d1a6793b429941aa800d9410b8ed12c9,
"skin": "slButtonWhiteTabDisabled",
"text": kony.i18n.getLocalizedString("i18n.externalaccount.Domestic"),
"top": "0dp",
"width": "33%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnInternational = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slButtonWhiteTabFocus",
"height": "98%",
"id": "btnInternational",
"isVisible": true,
"onClick": AS_Button_f01cc5f187b947d7af5ca061e7a43307,
"skin": "slButtonWhiteTabDisabled",
"text": kony.i18n.getLocalizedString("i18n.transfer.International"),
"width": "34%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxContent.add( btnInternational, btnDomestic,btnBOJ);
flxTab.add(flxContent);
flxHeader.add(flxTop, flxTab);
var flxMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "82%",
"horizontalScrollIndicator": true,
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxBeneficiary = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxBeneficiary",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBeneficiary.setDefaultUnit(kony.flex.DP);
var lblBenificiaryTitle = new kony.ui.Label({
"centerX": "50%",
"id": "lblBenificiaryTitle",
"isVisible": true,
"right": "0dp",
"skin": "slLabelTitle",
"text": kony.i18n.getLocalizedString("i18b.Bene.Beneficiarydetails"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBenficiryNameKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBenficiryNameKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBenficiryNameKA.setDefaultUnit(kony.flex.DP);
var lblBenficiryNameKA = new kony.ui.Label({
"id": "lblBenficiryNameKA",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Fullname"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtBenficiryNameKA = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_WORDS,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtBenficiryNameKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 35,
"onDone": AS_TextField_fbc1b9c4744d4af5a85c67acc1147aa1,
"onTextChange": AS_TextField_c849b6c4edde4136b42c36d424dcf212,
"onTouchEnd": AS_TextField_h9cabf573bdd4be3b659170adc1f5285,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_g4f4e0cfa73d4b58bc8dcb4a4a1b673f,
"showClearButton": true,
"showCloseButton": false,
"showProgressIndicator": false,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBenficiryNameKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBenficiryNameKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBenficiryNameKA.setDefaultUnit(kony.flex.DP);
flxUnderlineBenficiryNameKA.add();
var lblBeneFullName = new kony.ui.Label({
"id": "lblBeneFullName",
"isVisible": false,
"right": "300dp",
"skin": "slLabel",
"text": "Label",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBenficiryNameKA.add(lblBenficiryNameKA, txtBenficiryNameKA, flxUnderlineBenficiryNameKA, lblBeneFullName);
var txtBenficiryNameKAInline = new kony.ui.Label({
"id": "txtBenficiryNameKAInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBenficiryNickName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBenficiryNickName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBenficiryNickName.setDefaultUnit(kony.flex.DP);
var lblBenficiryNickName = new kony.ui.Label({
"id": "lblBenficiryNickName",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtBenficiryNickName = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_WORDS,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtBenficiryNickName",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 35,
"onDone": AS_TextField_h9d810c1660b4a039f40f20733af9c7c,
"onTextChange": AS_TextField_b234fd53545743b893641ef68036d06f,
"onTouchEnd": AS_TextField_j95852d9a84c49d3a769c1d1d8ae06d2,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_c8d6a29aa8f34c37a3f26de0af175c69,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBenficiryNickName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBenficiryNickName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBenficiryNickName.setDefaultUnit(kony.flex.DP);
flxUnderlineBenficiryNickName.add();
flxBenficiryNickName.add(lblBenficiryNickName, txtBenficiryNickName, flxUnderlineBenficiryNickName);
var txtBenficiryNickNameInline = new kony.ui.Label({
"id": "txtBenficiryNickNameInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAddressBene = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAddressBene",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAddressBene.setDefaultUnit(kony.flex.DP);
var lblAddressBene = new kony.ui.Label({
"id": "lblAddressBene",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.BeneficiaryAddress"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var tbxAddressBene = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxAddressBene",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 35,
"onDone": AS_TextField_bb3b1bd137dd4f0bb3e02ab20fed1163,
"onTextChange": AS_TextField_b590ebc603554d609305bcbdb55fd73a,
"onTouchEnd": AS_TextField_j1ef35a01059446f980eaa83fe2973d8,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_deb58d1a925045df99935d539e9c06af,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineAddressBene = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAddressBene",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAddressBene.setDefaultUnit(kony.flex.DP);
flxUnderlineAddressBene.add();
flxAddressBene.add(lblAddressBene, tbxAddressBene, flxUnderlineAddressBene);
var tbxAddressBeneInline = new kony.ui.Label({
"id": "tbxAddressBeneInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxCityName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxCityName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxCityName.setDefaultUnit(kony.flex.DP);
var tbxCity = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxCity",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"onDone": AS_TextField_d3fefef19b6644b9b6856a4da211fa0e,
"onTextChange": AS_TextField_gc91e6bc52d84b47a0552a41a63d4e55,
"onTouchEnd": AS_TextField_icbf023675ec4a2b94a0e89cba4c3073,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_d5679992e8b245da968678caf18c5e23,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineCityName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineCityName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineCityName.setDefaultUnit(kony.flex.DP);
flxUnderlineCityName.add();
var flxCity = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxCity",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_cb48f29e943c43aabbc9161703612ac1,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxCity.setDefaultUnit(kony.flex.DP);
var lblArrowCity = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowCity",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCity.add(lblArrowCity);
var lblCityName = new kony.ui.Label({
"id": "lblCityName",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.manage_payee.cityPlh"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "40%",
"width": "20%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCityName.add(tbxCity, flxUnderlineCityName, flxCity, lblCityName);
var tbxCityInline = new kony.ui.Label({
"id": "tbxCityInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxCountryName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxCountryName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxCountryName.setDefaultUnit(kony.flex.DP);
var lblCountryTitleStat = new kony.ui.Label({
"id": "lblCountryTitleStat",
"isVisible": false,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknlblanimated75",
"text": kony.i18n.getLocalizedString("i18n.NUO.Country"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "14%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var tbxCountry = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxCountry",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineCountryName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineCountryName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineCountryName.setDefaultUnit(kony.flex.DP);
flxUnderlineCountryName.add();
var flxCountry = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxCountry",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_bdb2e415284948929ceb83c73d370a9c,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxCountry.setDefaultUnit(kony.flex.DP);
var lblCountryNameKA = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblCountryNameKA",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.NUO.Country"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowCountry = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowCountry",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCountry.add(lblCountryNameKA, lblArrowCountry);
var lblCountryTitle = new kony.ui.Label({
"id": "lblCountryTitle",
"isVisible": false,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblSmall",
"text": kony.i18n.getLocalizedString("i18n.NUO.Country"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "18%",
"width": "85%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCountryName.add(lblCountryTitleStat, tbxCountry, flxUnderlineCountryName, flxCountry, lblCountryTitle);
var tbxCountryInline = new kony.ui.Label({
"id": "tbxCountryInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBeneficiary.add(lblBenificiaryTitle, flxBenficiryNameKA, txtBenficiryNameKAInline, flxBenficiryNickName, txtBenficiryNickNameInline, flxAddressBene, tbxAddressBeneInline, flxCityName, tbxCityInline, flxCountryName, tbxCountryInline);
var flxAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxAddress",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "5%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAddress.setDefaultUnit(kony.flex.DP);
var lblAddressTitle = new kony.ui.Label({
"centerX": "50%",
"id": "lblAddressTitle",
"isVisible": true,
"right": "0dp",
"skin": "slLabelTitle",
"text": kony.i18n.getLocalizedString("i18n.common.address"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAddress.add(lblAddressTitle);
var flxBankDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxBankDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "5%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBankDetails.setDefaultUnit(kony.flex.DP);
var lblBankDetailsTitle = new kony.ui.Label({
"centerX": "50%",
"id": "lblBankDetailsTitle",
"isVisible": true,
"right": "0dp",
"skin": "slLabelTitle",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankdetails"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAccountNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAccountNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAccountNumber.setDefaultUnit(kony.flex.DP);
var lblAccountNumber = new kony.ui.Label({
"id": "lblAccountNumber",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Accountnumber"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var externalAccountNumberTextField = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_ALL,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "externalAccountNumberTextField",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 42,
"onDone": AS_TextField_c2c7eaecc82d43a0abf0863ad35483b8,
"onTextChange": AS_TextField_e4fa799e18f84841af1ef1dec737fe41,
"onTouchEnd": AS_TextField_e48bbbdea581414980de436f5db80a8f,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_ac9fe40997374fb186a5486c9ed7a716,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineAccountNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAccountNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAccountNumber.setDefaultUnit(kony.flex.DP);
flxUnderlineAccountNumber.add();
flxAccountNumber.add(lblAccountNumber, externalAccountNumberTextField, flxUnderlineAccountNumber);
var externalAccountNumberTextFieldInline = new kony.ui.Label({
"id": "externalAccountNumberTextFieldInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxSwiftCode = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxSwiftCode",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"width": "90%",
"zIndex": 1
}, {}, {});
flxSwiftCode.setDefaultUnit(kony.flex.DP);
var lblSwiftCode = new kony.ui.Label({
"id": "lblSwiftCode",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.transfer.swiftandrouting"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtSwiftCodeKA = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_ALL,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtSwiftCodeKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 11,
"onDone": AS_TextField_a5a200bfd5e74dbe9ea5d49dc60a494a,
"onTextChange": AS_TextField_db1b743aa0b64d358de6b8dbfb53eda1,
"onTouchEnd": AS_TextField_eed7bbd8e9004efe8c45516c5c101a10,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_a854ff690dbb4f84b62b19943f3042dc,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineSwiftCode = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineSwiftCode",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineSwiftCode.setDefaultUnit(kony.flex.DP);
flxUnderlineSwiftCode.add();
flxSwiftCode.add(lblSwiftCode, txtSwiftCodeKA, flxUnderlineSwiftCode);
var txtSwiftCodeKAInline = new kony.ui.Label({
"id": "txtSwiftCodeKAInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBankName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBankName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBankName.setDefaultUnit(kony.flex.DP);
var lblBankNameStat = new kony.ui.Label({
"id": "lblBankNameStat",
"isVisible": false,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknlblanimated75",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "14%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var tbxBankName = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxBankName",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBankName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBankName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBankName.setDefaultUnit(kony.flex.DP);
flxUnderlineBankName.add();
var flxBank = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxBank",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_heccb89fe66340c7bf5b055f5cb12ab5,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxBank.setDefaultUnit(kony.flex.DP);
var lblBankName = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblBankName",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowBank = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowBank",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBank.add(lblBankName, lblArrowBank);
flxBankName.add(lblBankNameStat, tbxBankName, flxUnderlineBankName, flxBank);
var tbxBankNameInline = new kony.ui.Label({
"id": "tbxBankNameInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBankNameInt = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBankNameInt",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBankNameInt.setDefaultUnit(kony.flex.DP);
var tbxBankNameInt = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxBankNameInt",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"onDone": AS_TextField_b7842585bb6043e19b5c11d4c7f554e9,
"onTextChange": AS_TextField_gea029f6fa9e4f8aadaa9b66f7adffe6,
"onTouchEnd": AS_TextField_gfde193bba4a443cb7cab87f4c1b976e,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_j495c9a29829490cbf0f450aef8539bd,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBankNameInt = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBankNameInt",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBankNameInt.setDefaultUnit(kony.flex.DP);
flxUnderlineBankNameInt.add();
var lblBankNameInt = new kony.ui.Label({
"id": "lblBankNameInt",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBankNameInt.add(tbxBankNameInt, flxUnderlineBankNameInt, lblBankNameInt);
var tbxBankNameIntInline = new kony.ui.Label({
"id": "tbxBankNameIntInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBankBranch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBankBranch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBankBranch.setDefaultUnit(kony.flex.DP);
var lblBankBranchStat = new kony.ui.Label({
"id": "lblBankBranchStat",
"isVisible": false,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknlblanimated75",
"text": kony.i18n.getLocalizedString("i18n.Bene.BankBranch"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "14%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var tbxBankBranch = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxBankBranch",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBankBranch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBankBranch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBankBranch.setDefaultUnit(kony.flex.DP);
flxUnderlineBankBranch.add();
var flxBankBranchHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxBankBranchHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_c1f53a703d084d6bb87ce9621b8115c7,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxBankBranchHolder.setDefaultUnit(kony.flex.DP);
var lblBankBranch = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblBankBranch",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.BankBranch"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowBankBranch = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowBankBranch",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBankBranchHolder.add(lblBankBranch, lblArrowBankBranch);
flxBankBranch.add(lblBankBranchStat, tbxBankBranch, flxUnderlineBankBranch, flxBankBranchHolder);
var tbxBankBranchInline = new kony.ui.Label({
"id": "tbxBankBranchInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxCityBank = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxCityBank",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxCityBank.setDefaultUnit(kony.flex.DP);
var tbxCityBank = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxCityBank",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"onDone": AS_TextField_g0e5b43c53594a1fb4aee2a0d8041450,
"onTextChange": AS_TextField_c1a2a34d81ca4328b000b3a0f4a81b26,
"onTouchEnd": AS_TextField_j04bc1fa45c3471bbc58b1bcef410787,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_b1c895a6f4bc45459f21bc22318b125a,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineCityBank = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineCityBank",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineCityBank.setDefaultUnit(kony.flex.DP);
flxUnderlineCityBank.add();
var flxCityBankHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxCityBankHolder",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_dc819701636642e3ba1092ec548b26d9,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxCityBankHolder.setDefaultUnit(kony.flex.DP);
var lblArrowCityBank = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowCityBank",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCityBankHolder.add(lblArrowCityBank);
var lblCityBank = new kony.ui.Label({
"id": "lblCityBank",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.manage_payee.cityPlh"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCityBank.add(tbxCityBank, flxUnderlineCityBank, flxCityBankHolder, lblCityBank);
var tbxCityBankInline = new kony.ui.Label({
"id": "tbxCityBankInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxCountryBankDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxCountryBankDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxCountryBankDetails.setDefaultUnit(kony.flex.DP);
var lblBankCountryStat = new kony.ui.Label({
"id": "lblBankCountryStat",
"isVisible": false,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknlblanimated75",
"text": kony.i18n.getLocalizedString("i18n.NUO.Country"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "14%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtCountryBankDetails = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtCountryBankDetails",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblCountryBankDetailsUnderLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "lblCountryBankDetailsUnderLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
lblCountryBankDetailsUnderLine.setDefaultUnit(kony.flex.DP);
lblCountryBankDetailsUnderLine.add();
var flxCountryBankDetailsHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxCountryBankDetailsHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_h43440dace2c40319d66bbd24399b1d8,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxCountryBankDetailsHolder.setDefaultUnit(kony.flex.DP);
var lblArrowCountryBankDetails = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowCountryBankDetails",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCountryBankDetailsHolder.add(lblArrowCountryBankDetails);
var lblCountryBankDetails = new kony.ui.Label({
"id": "lblCountryBankDetails",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.NUO.Country"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCountryBankDetails.add(lblBankCountryStat, txtCountryBankDetails, lblCountryBankDetailsUnderLine, flxCountryBankDetailsHolder, lblCountryBankDetails);
var flxAddressBank = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAddressBank",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAddressBank.setDefaultUnit(kony.flex.DP);
var lblAddressBank = new kony.ui.Label({
"id": "lblAddressBank",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.BankAddress"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var tbxAddressBank = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxAddressBank",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 75,
"onDone": AS_TextField_a1a7a1ddf2664980aff9f3852c61bc7a,
"onTextChange": AS_TextField_beae2cc3877a4d759b1db2321a58158e,
"onTouchEnd": AS_TextField_d7783b0248124d92b2c443f3ce53e0e4,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_hd1921f8f21d4f06aa5983df19b82b7b,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineAddressBank = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAddressBank",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAddressBank.setDefaultUnit(kony.flex.DP);
flxUnderlineAddressBank.add();
flxAddressBank.add(lblAddressBank, tbxAddressBank, flxUnderlineAddressBank);
var tbxAddressBankInline = new kony.ui.Label({
"id": "tbxAddressBankInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBankDetails.add(lblBankDetailsTitle, flxAccountNumber, externalAccountNumberTextFieldInline, flxSwiftCode, txtSwiftCodeKAInline, flxBankName, tbxBankNameInline, flxBankNameInt, tbxBankNameIntInline, flxBankBranch, tbxBankBranchInline, flxCityBank, tbxCityBankInline, flxCountryBankDetails, flxAddressBank, tbxAddressBankInline);
var flxMoreInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxMoreInfo",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "5%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMoreInfo.setDefaultUnit(kony.flex.DP);
var lblMoreInfo = new kony.ui.Label({
"centerX": "50%",
"id": "lblMoreInfo",
"isVisible": true,
"right": "0dp",
"skin": "slLabelTitle",
"text": kony.i18n.getLocalizedString("i18n.Bene.Moreinformation"),
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxEmail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxEmail",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"width": "90%",
"zIndex": 1
}, {}, {});
flxEmail.setDefaultUnit(kony.flex.DP);
var lblEmail = new kony.ui.Label({
"id": "lblEmail",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Beneficiaryemail"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var tbxEmail = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxEmail",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
"maxTextLength": 25,
"onDone": AS_TextField_be9aaea0f7ed4846b70d621bf4969c15,
"onTextChange": AS_TextField_g9fb3b3833144211b28dd2c6447f38c4,
"onTouchEnd": AS_TextField_bd1a1df65be5427d9e44909204c14021,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onEndEditing": AS_TextField_g73174f11e1845008bdeed5e297af2e1,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineEmail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineEmail",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineEmail.setDefaultUnit(kony.flex.DP);
flxUnderlineEmail.add();
flxEmail.add(lblEmail, tbxEmail, flxUnderlineEmail);
var tbxEmailInline = new kony.ui.Label({
"id": "tbxEmailInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBeneRelation = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBeneRelation",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBeneRelation.setDefaultUnit(kony.flex.DP);
var lblBeneRelation = new kony.ui.Label({
"id": "lblBeneRelation",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Beneficiaryrelationship"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var tbxBeneRelation = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxBeneRelation",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 50,
"onDone": AS_TextField_f569d88da2fb4a3b8724a1c3e6b8d123,
"onTextChange": AS_TextField_i3509ce956f84da58a7dcd3e951569b6,
"onTouchEnd": AS_TextField_d1d49f1007064f32b12c3e17ba75cc7b,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_b87316127ce746d2b963172eb694dc2a,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBeneRelation = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBeneRelation",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBeneRelation.setDefaultUnit(kony.flex.DP);
flxUnderlineBeneRelation.add();
flxBeneRelation.add(lblBeneRelation, tbxBeneRelation, flxUnderlineBeneRelation);
var tbxBeneRelationInline = new kony.ui.Label({
"id": "tbxBeneRelationInline",
"isVisible": false,
"maxNumberOfLines": 1,
"left": "6%",
"skin": "sknInline",
"text": "Inline Error",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxMoreInfo.add(lblMoreInfo, flxEmail, tbxEmailInline, flxBeneRelation, tbxBeneRelationInline);
var flxSpace = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxSpace",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSpace.setDefaultUnit(kony.flex.DP);
var lblTrick = new kony.ui.Label({
"id": "lblTrick",
"isVisible": false,
"right": "131dp",
"skin": "slLabel",
"text": "Label",
"top": "38dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxSpace.add(lblTrick);
flxMain.add(flxBeneficiary, flxAddress, flxBankDetails, flxMoreInfo, flxSpace);
flxFormMain.add(flxHeader, flxMain);
frmAddExternalAccountKA.add(titleBarAddExternalAccount, mainScrollContainer, flxConfirmPopUp, flxFormMain);
};
function frmAddExternalAccountKAGlobalsAr() {
frmAddExternalAccountKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmAddExternalAccountKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmAddExternalAccountKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_a3dcbc8f479042f2a850f706e6e247a9,
"preShow": AS_Form_1c81da837d7c41ff8b7dc530f7969f94,
"skin": "sknmainGradient",
"statusBarHidden": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": true,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NEXTPREV,
"inTransitionConfig": {
"transitionDirection": "fromBottom",
"transitionEffect": "transitionMoveIn"
},
"needsIndicatorDuringPostShow": false,
"outTransitionConfig": {
"transitionDirection": "fromBottom",
"transitionEffect": "transitionMoveOut"
},
"retainScrollPosition": false,
"statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
"titleBar": false
});
};
