//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function initializetmpSegCardsLandingAr() {
    flxCardsLandingTemplateAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "flexTransparent",
        "height": "260dp",
        "id": "flxCardsLandingTemplate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flexTransparent"
    }, {}, {});
    flxCardsLandingTemplateAr.setDefaultUnit(kony.flex.DP);
    var cardType = new kony.ui.Label({
        "height": "13%",
        "id": "cardType",
        "isVisible": true,
        "right": "14%",
        "skin": "sknLblBack",
        "top": "4%",
        "width": "63%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxCardDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxCardDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "14%",
        "skin": "slFbox",
        "top": "19%",
        "width": "72%",
        "zIndex": 1
    }, {}, {});
    flxCardDetails.setDefaultUnit(kony.flex.DP);
    var cardImage = new kony.ui.Image2({
        "centerX": "50%",
        "height": "100%",
        "id": "cardImage",
        "isVisible": true,
        "skin": "slImage",
        "src": "card_affluent_debit.png",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var cardNumber = new kony.ui.Label({
        "height": "16%",
        "id": "cardNumber",
        "isVisible": true,
        "right": "22%",
        "skin": "sknLblWhiteTmp",
        "top": "27%",
        "width": "62%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CardHolder = new kony.ui.Label({
        "bottom": "6%",
        "id": "CardHolder",
        "isVisible": true,
        "right": "8%",
        "skin": "sknLblWhiteTmp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var ValidThru = new kony.ui.Label({
        "id": "ValidThru",
        "isVisible": true,
        "right": "42.06%",
        "skin": "sknLblWhiteTmp",
        "top": "62.50%",
        "width": "25%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCardDetails.add(cardImage, cardNumber, CardHolder, ValidThru);
    var btnNav = new kony.ui.Button({
        "focusSkin": "btnRightArw",
        "height": "50dp",
        "id": "btnNav",
        "isVisible": false,
        "onClick": AS_Button_b6e13270cfa14c249cdba2f4a8b6ed6e,
        "left": "35dp",
        "skin": "btnRightArw",
        "text": "L",
        "top": "-0.07%",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnNav1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "btnNav1",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10%",
        "skin": "slFbox",
        "top": "0%",
        "width": "15%",
        "zIndex": 1
    }, {}, {});
    btnNav1.setDefaultUnit(kony.flex.DP);
    var lblIncommingRing = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "60%",
        "id": "lblIncommingRing",
        "isVisible": true,
        "skin": "sknBOJttfLightBlue",
        "text": "L",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    btnNav1.add(lblIncommingRing);
    var CopybtnNav0b0169acff9c84a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "CopybtnNav0b0169acff9c84a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10%",
        "skin": "slFbox",
        "top": "0%",
        "width": "15%",
        "zIndex": 1
    }, {}, {});
    CopybtnNav0b0169acff9c84a.setDefaultUnit(kony.flex.DP);
    var CopylblIncommingRing0h6f8267705004b = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "60%",
        "id": "CopylblIncommingRing0h6f8267705004b",
        "isVisible": true,
        "skin": "sknBOJttfLightBlue",
        "text": "s",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblIncommingRing0db621a02819242 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "60%",
        "id": "CopylblIncommingRing0db621a02819242",
        "isVisible": true,
        "skin": "CopysknBOJttfLightBlue0f2f2a5a952b945",
        "text": kony.i18n.getLocalizedString("i18n.common.reverseback"),
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopybtnNav0b0169acff9c84a.add(CopylblIncommingRing0h6f8267705004b, CopylblIncommingRing0db621a02819242);
    var cardTypeFlag = new kony.ui.Label({
        "bottom": "0%",
        "height": "0%",
        "id": "cardTypeFlag",
        "isVisible": true,
        "right": "0%",
        "skin": "sknTrans",
        "width": "0%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCardsLandingTemplateAr.add(cardType, flxCardDetails, btnNav, btnNav1, CopybtnNav0b0169acff9c84a, cardTypeFlag);
}
