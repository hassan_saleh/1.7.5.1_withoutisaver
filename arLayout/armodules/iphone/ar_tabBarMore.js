//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function initializetabBarMoreAr() {
CopytabBarBackground0f4227cb8dba14fAr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "49dp",
"id": "CopytabBarBackground0f4227cb8dba14f",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "skncontainerBkgWhite"
}, {}, {});
CopytabBarBackground0f4227cb8dba14fAr.setDefaultUnit(kony.flex.DP);
var FlexContainer0c6e533ec2e5d48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlexContainer0c6e533ec2e5d48",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_e1a880ee03f848868ca550dac0f035cc,
"skin": "sknfocusmenu",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlexContainer0c6e533ec2e5d48.setDefaultUnit(kony.flex.DP);
var Image0748aa60c715044 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "Image0748aa60c715044",
"isVisible": true,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_accounts_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label0c0d55c2acd1d43 = new kony.ui.Label({
"centerX": "50%",
"id": "Label0c0d55c2acd1d43",
"isVisible": true,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlexContainer0c6e533ec2e5d48.add(Image0748aa60c715044, Label0c0d55c2acd1d43);
var CopyFlexContainer02d619202407a4e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyFlexContainer02d619202407a4e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_3038d2665f8a420295c531da1bbcf36e,
"skin": "sknfocusmenu",
"top": "0dp",
"width": "21%",
"zIndex": 1
}, {}, {});
CopyFlexContainer02d619202407a4e.setDefaultUnit(kony.flex.DP);
var CopyImage06fda7a0f69c945 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "CopyImage06fda7a0f69c945",
"isVisible": true,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_t_and_p_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabel083ff33bf410d45 = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel083ff33bf410d45",
"isVisible": true,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.Payments"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer02d619202407a4e.add(CopyImage06fda7a0f69c945, CopyLabel083ff33bf410d45);
var FlxBot = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxBot",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_hf2dc7a3db2a437abdeb59f2679b3629,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxBot.setDefaultUnit(kony.flex.DP);
var imgBot = new kony.ui.Image2({
"centerX": "50%",
"height": "40dp",
"id": "imgBot",
"isVisible": true,
"right": "13dp",
"skin": "slImage",
"src": "chaticonactive.png",
"top": "4dp",
"width": "40dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxBot.add(imgBot);
var CopyFlexContainer0dceea0807f8140 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyFlexContainer0dceea0807f8140",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_e4320aa0b2c440fc820037565327674f,
"skin": "sknfocusmenu",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0dceea0807f8140.setDefaultUnit(kony.flex.DP);
var CopyImage05728820c622549 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "CopyImage05728820c622549",
"isVisible": true,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_deposits_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabel0a52cc38683b843 = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel0a52cc38683b843",
"isVisible": true,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.deposits"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0dceea0807f8140.add(CopyImage05728820c622549, CopyLabel0a52cc38683b843);
var CopyFlexContainer0419cabfa34b74c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyFlexContainer0419cabfa34b74c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "19%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0419cabfa34b74c.setDefaultUnit(kony.flex.DP);
var CopyImage034a3e77119c647 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "CopyImage034a3e77119c647",
"isVisible": true,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_more_icon_active.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabel0fa87e35583714a = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel0fa87e35583714a",
"isVisible": true,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0419cabfa34b74c.add(CopyImage034a3e77119c647, CopyLabel0fa87e35583714a);
CopytabBarBackground0f4227cb8dba14fAr.add( CopyFlexContainer0419cabfa34b74c, CopyFlexContainer0dceea0807f8140, FlxBot, CopyFlexContainer02d619202407a4e,FlexContainer0c6e533ec2e5d48);
}
