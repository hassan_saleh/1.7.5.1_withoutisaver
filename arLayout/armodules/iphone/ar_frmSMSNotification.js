//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmSMSNotificationAr() {
    frmSMSNotification.setDefaultUnit(kony.flex.DP);
    var flxSMSHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxSMSHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSMSHeader.setDefaultUnit(kony.flex.DP);
    var lblSMSHeader = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblSMSHeader",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": "SMS Notification",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnBack = new kony.ui.Button({
        "focusSkin": "sknBtnBack",
        "height": "100%",
        "id": "btnBack",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_c004a147154541408558d7a6840daf1d,
        "skin": "sknBtnBack",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0%",
        "width": "15%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "11%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxSMSHeader.add(lblSMSHeader, btnBack, lblBack);
    var flxMain = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "90%",
        "horizontalScrollIndicator": true,
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var flxSMSNotification = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxSMSNotification",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSMSNotification.setDefaultUnit(kony.flex.DP);
    var flxSMSNotificationDIsabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSMSNotificationDIsabled",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_f1e10794092045d596fd821aae1b1052,
        "left": "5%",
        "skin": "sknflxWhiteOpacity60",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSMSNotificationDIsabled.setDefaultUnit(kony.flex.DP);
    var flxInnerDisabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxInnerDisabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    flxInnerDisabled.setDefaultUnit(kony.flex.DP);
    flxInnerDisabled.add();
    var flxILineDisabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "14dp",
        "id": "flxILineDisabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 10,
        "skin": "sknLineDarkBlueOpacity60",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    flxILineDisabled.setDefaultUnit(kony.flex.DP);
    flxILineDisabled.add();
    flxSMSNotificationDIsabled.add(flxInnerDisabled, flxILineDisabled);
    var flxSMSNotificationEnabledToggle = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSMSNotificationEnabledToggle",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_f6796ed65f514d80b5884af0ba9cfb29,
        "left": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxSMSNotificationEnabledToggle.setDefaultUnit(kony.flex.DP);
    var flxInnerEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "47%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxInnerEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    flxInnerEnabled.setDefaultUnit(kony.flex.DP);
    flxInnerEnabled.add();
    var flxLineEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "14dp",
        "id": "flxLineEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    flxLineEnabled.setDefaultUnit(kony.flex.DP);
    flxLineEnabled.add();
    flxSMSNotificationEnabledToggle.add(flxInnerEnabled, flxLineEnabled);
    var lblSMSNotification = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSMSNotification",
        "isVisible": true,
        "right": "5%",
        "maxNumberOfLines": 1,
        "skin": "sknLblBack",
        "text": "SMS Notifications",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "1dp",
        "width": "73%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxUnderLineSMS = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderLineSMS",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "flxUnderLine",
        "top": "98%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderLineSMS.setDefaultUnit(kony.flex.DP);
    flxUnderLineSMS.add();
    flxSMSNotification.add(flxSMSNotificationDIsabled, flxSMSNotificationEnabledToggle, lblSMSNotification, flxUnderLineSMS);
    var segAccountsSMS = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblAccountNumber": "***077",
            "lblAccountType": "Savings Account"
        }, {
            "lblAccountNumber": "***078",
            "lblAccountType": "Current Account"
        }, {
            "lblAccountNumber": "***079",
            "lblAccountType": "Savings Account"
        }],
        "groupCells": false,
        "id": "segAccountsSMS",
        "isVisible": true,
        "right": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "slSegAllBene",
        "rowSkin": "slSegAllBene",
        "rowTemplate": flxSMSNotificationEnabled,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxAlertArrow": "flxAlertArrow",
            "flxILineDisabled": "flxILineDisabled",
            "flxInnerDisabled": "flxInnerDisabled",
            "flxInnerEnabled": "flxInnerEnabled",
            "flxLineEnabled": "flxLineEnabled",
            "flxSMSNotificationEnabled": "flxSMSNotificationEnabled",
            "flxSMSSwitchDIsabled": "flxSMSSwitchDIsabled",
            "flxSMSSwitchEnabled": "flxSMSSwitchEnabled",
            "flxUnderLineSMS": "flxUnderLineSMS",
            "lblAccountNumber": "lblAccountNumber",
            "lblAccountType": "lblAccountType",
            "lblRightIcon": "lblRightIcon"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    flxMain.add(flxSMSNotification, segAccountsSMS);
    frmSMSNotification.add(flxSMSHeader, flxMain);
};
function frmSMSNotificationGlobalsAr() {
    frmSMSNotificationAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSMSNotificationAr,
        "enabledForIdleTimeout": true,
        "id": "frmSMSNotification",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};
