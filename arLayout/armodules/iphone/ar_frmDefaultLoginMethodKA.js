//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:02 EET 2020
function addWidgetsfrmDefaultLoginMethodKAAr() {
    frmDefaultLoginMethodKA.setDefaultUnit(kony.flex.DP);
    var iosTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "iosTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    iosTitleBar.setDefaultUnit(kony.flex.DP);
    var userSettingsLabel = new kony.ui.Label({
        "centerX": "50%",
        "id": "userSettingsLabel",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": "Default Login Method",
        "top": "15dp",
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var backButton = new kony.ui.Button({
        "focusSkin": "sknleftBackButtonFocus",
        "height": "50dp",
        "id": "backButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_f0e04a12851f470bae777e17e9d3889c,
        "skin": "sknleftBackButtonNormal",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    iosTitleBar.add(userSettingsLabel, backButton);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var androidSettings = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "androidSettings",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidSettings.setDefaultUnit(kony.flex.DP);
    var segmentBorderBottomAndroid = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "segmentBorderBottomAndroid",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "-1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    segmentBorderBottomAndroid.setDefaultUnit(kony.flex.DP);
    segmentBorderBottomAndroid.add();
    var lbldesc = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbldesc",
        "isVisible": true,
        "right": "106dp",
        "skin": "sknOnBoarding26",
        "text": "Please select which one you want as default login option",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxFooter = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "62%",
        "id": "flxFooter",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "skncontainerBkgGrayf7f7f7",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFooter.setDefaultUnit(kony.flex.DP);
    var segLoginMethods = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "imgEnable": "check_green.png",
            "imgIcon": "pin.png",
            "lblLoginMethod": "PIN",
            "tmpAuthMode": "pin"
        }, {
            "imgEnable": "check_green.png",
            "imgIcon": "face.png",
            "lblLoginMethod": "FACE RECOGNITION",
            "tmpAuthMode": "faceid"
        }, {
            "imgEnable": "check_green.png",
            "imgIcon": "password.png",
            "lblLoginMethod": "PASSWORD",
            "tmpAuthMode": "password"
        }, {
            "imgEnable": "check_green.png",
            "imgIcon": "touch.png",
            "lblLoginMethod": "TOUCH ID",
            "tmpAuthMode": "touchid"
        }],
        "groupCells": false,
        "id": "segLoginMethods",
        "isVisible": true,
        "right": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_a42914fbf38b4218a2cc87e861aba52e,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowTemplate": flxtmpMain,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "5%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxDivider1": "flxDivider1",
            "flxtmpMain": "flxtmpMain",
            "imgEnable": "imgEnable",
            "imgIcon": "imgIcon",
            "lblLoginMethod": "lblLoginMethod",
            "tmpAuthMode": "tmpAuthMode"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    var btnContinue = new kony.ui.Button({
        "centerX": "49.97%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "btnContinue",
        "isVisible": true,
        "onClick": AS_Button_i63c1b126e134f0ab9e095dc3e9bf1ab,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.btnContinuee"),
        "top": "20%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxFooter.add(segLoginMethods, btnContinue);
    androidSettings.add(segmentBorderBottomAndroid, lbldesc, flxFooter);
    mainContent.add(androidSettings);
    frmDefaultLoginMethodKA.add(iosTitleBar, mainContent);
};
function frmDefaultLoginMethodKAGlobalsAr() {
    frmDefaultLoginMethodKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmDefaultLoginMethodKAAr,
        "bounces": false,
        "enabledForIdleTimeout": true,
        "id": "frmDefaultLoginMethodKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "preShow": AS_Form_h92a3ac07ddc4981ab4e2e066e25523f,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": true,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false
    });
};
