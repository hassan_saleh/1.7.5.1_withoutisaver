//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmPaymentDashboardAr() {
frmPaymentDashboard.setDefaultUnit(kony.flex.DP);
var flxMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "93%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slPaymentSKin",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxHeaderDashboard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeaderDashboard",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1000
}, {}, {});
flxHeaderDashboard.setDefaultUnit(kony.flex.DP);
var flxSide = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxSide",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": 0,
"skin": "slFbox",
"top": "0dp",
"width": "100dp",
"zIndex": 1
}, {}, {});
flxSide.setDefaultUnit(kony.flex.DP);
var btnMessage = new kony.ui.Button({
"focusSkin": "BtnNotificationMail",
"height": "50dp",
"id": "btnMessage",
"isVisible": false,
"left": "10dp",
"skin": "BtnNotificationMail",
"text": "G",
"top": "0dp",
"width": "42dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var btnAccountInfo = new kony.ui.Button({
"focusSkin": "btnUser",
"height": "50dp",
"id": "btnAccountInfo",
"isVisible": true,
"left": "54dp",
"onClick": AS_Button_fab0e584303d45f094fffeaea1ac9cc7,
"skin": "btnUser",
"text": "F",
"top": "0dp",
"width": "40dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
flxSide.add(btnMessage, btnAccountInfo);
var imgLogo = new kony.ui.Image2({
"height": "30dp",
"id": "imgLogo",
"isVisible": false,
"left": "15dp",
"skin": "slImage",
"src": "logo03.png",
"top": "5dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_hcb07cd9d7e24932b1ecfd17b9c76d11,
"skin": "slFbox",
"top": "0%",
"width": "26%",
"zIndex": 10
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.accounts.accounts"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBack);
flxHeaderDashboard.add(flxSide, imgLogo, flxBack);
var flxOptions = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"centerX": "50%",
"clipBounds": true,
"enableScrolling": true,
"height": "90%",
"horizontalScrollIndicator": true,
"id": "flxOptions",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1000
}, {}, {});
flxOptions.setDefaultUnit(kony.flex.DP);
var flxRow1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "28%",
"id": "flxRow1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxRow1.setDefaultUnit(kony.flex.DP);
var flxLeft1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxLeft1",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "10%",
"skin": "slFbox",
"top": "0%",
"width": "40%",
"zIndex": 1
}, {}, {});
flxLeft1.setDefaultUnit(kony.flex.DP);
var btnOwnAccount = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonPaymentDashFocus",
"height": "70dp",
"id": "btnOwnAccount",
"isVisible": true,
"onClick": AS_Button_jb1f26bf82a349158fc8f47d8e2ea7f1,
"skin": "slButtonPaymentDash",
"text": "8",
"top": "0dp",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var lblOwnAccount = new kony.ui.Label({
"centerX": "50%",
"id": "lblOwnAccount",
"isVisible": true,
"skin": "sknLblPayment",
"text": kony.i18n.getLocalizedString("i18n.PaymentDash.Transferbetween"),
"top": "1%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblOwnAccount2 = new kony.ui.Label({
"centerX": "50%",
"id": "lblOwnAccount2",
"isVisible": true,
"skin": "sknLblPayment",
"text": kony.i18n.getLocalizedString("i18n.PaymentDash.accounts"),
"top": "0dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxLeft1.add(btnOwnAccount, lblOwnAccount, lblOwnAccount2);
var flxRight1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxRight1",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "50%",
"skin": "slFbox",
"top": "0dp",
"width": "40%",
"zIndex": 1
}, {}, {});
flxRight1.setDefaultUnit(kony.flex.DP);
var btnSendMoney = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonPaymentDashFocus",
"height": "70dp",
"id": "btnSendMoney",
"isVisible": true,
"onClick": AS_Button_bfe435d2d5a1426188620610f29e4cb8,
"skin": "slButtonPaymentDash",
"text": "v",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var lblSendMoney = new kony.ui.Label({
"centerX": "50%",
"id": "lblSendMoney",
"isVisible": true,
"skin": "sknLblPayment",
"text": kony.i18n.getLocalizedString("i18n.Bene.SendMoney"),
"top": "1%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxRight1.add(btnSendMoney, lblSendMoney);
flxRow1.add(flxLeft1, flxRight1);
var flxRow2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "28%",
"id": "flxRow2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxRow2.setDefaultUnit(kony.flex.DP);
var flxLeft2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxLeft2",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "10%",
"skin": "slFbox",
"width": "40%",
"zIndex": 1
}, {}, {});
flxLeft2.setDefaultUnit(kony.flex.DP);
var btnStandingI = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonPaymentDashFocus",
"height": "70dp",
"id": "btnStandingI",
"isVisible": true,
"onClick": AS_Button_g10d456014a94750a1244c3d19f2ae1e,
"skin": "slButtonPaymentDash",
"text": "T",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var lblStandingI = new kony.ui.Label({
"centerX": "50%",
"id": "lblStandingI",
"isVisible": true,
"skin": "sknLblPayment",
"text": kony.i18n.getLocalizedString("i18n.PaymentDash.Standing"),
"top": "1%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblStandingI2 = new kony.ui.Label({
"centerX": "50%",
"id": "lblStandingI2",
"isVisible": true,
"skin": "sknLblPayment",
"text": kony.i18n.getLocalizedString("i18n.PaymentDash.Instructions"),
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxLeft2.add(btnStandingI, lblStandingI, lblStandingI2);
var flxRight2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxRight2",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "50%",
"skin": "slFbox",
"top": "0dp",
"width": "40%",
"zIndex": 1
}, {}, {});
flxRight2.setDefaultUnit(kony.flex.DP);
var btneFawateer = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonPaymentDashFocus",
"height": "70dp",
"id": "btneFawateer",
"isVisible": true,
"onClick": AS_Button_d5f229473b5044b99dc11e0d659599ce,
"skin": "slButtonPaymentDash",
"text": "x",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var lbleFawateer = new kony.ui.Label({
"centerX": "50%",
"id": "lbleFawateer",
"isVisible": true,
"skin": "sknLblPayment",
"text": kony.i18n.getLocalizedString("i18n.PaymentDash.BillsPayment"),
"top": "1%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxRight2.add(btneFawateer, lbleFawateer);
var flxCreditCardPayment = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxCreditCardPayment",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "50%",
"skin": "slFbox",
"top": "0%",
"width": "40%",
"zIndex": 1
}, {}, {});
flxCreditCardPayment.setDefaultUnit(kony.flex.DP);
var btnCreditCardPayment = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknBOJNewIconsttf",
"height": "70dp",
"id": "btnCreditCardPayment",
"isVisible": true,
"onClick": AS_Button_d78bb0909edb480999b7eafb2f538616,
"skin": "sknBOJNewIconsttf",
"text": "B",
"top": "0",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblCreditCardPaymentTitle = new kony.ui.Label({
"centerX": "50%",
"id": "lblCreditCardPaymentTitle",
"isVisible": true,
"skin": "sknLblPayment",
"text": kony.i18n.getLocalizedString("i18n.cards.ccpayment"),
"top": "1%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCreditCardPayment.add(btnCreditCardPayment, lblCreditCardPaymentTitle);
flxRow2.add(flxLeft2, flxRight2, flxCreditCardPayment);
var flxRow3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "28%",
"id": "flxRow3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxRow3.setDefaultUnit(kony.flex.DP);
var flxLeft3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxLeft3",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "10%",
"skin": "slFbox",
"width": "40%",
"zIndex": 1
}, {}, {});
flxLeft3.setDefaultUnit(kony.flex.DP);
var btnJoMoPay = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonPaymentDashFocus",
"height": "70dp",
"id": "btnJoMoPay",
"isVisible": true,
"onClick": AS_Button_bc91834aad404360ab992cf313423d92,
"skin": "slButtonPaymentDash",
"text": "S",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var lblJoMoPay = new kony.ui.Label({
"centerX": "50%",
"id": "lblJoMoPay",
"isVisible": true,
"skin": "sknLblPayment",
"text": kony.i18n.getLocalizedString("i18n.jomopay.jomopay"),
"top": "1%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxLeft3.add(btnJoMoPay, lblJoMoPay);
var frmCLIQ = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "frmCLIQ",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "50%",
"skin": "slFbox",
"top": "0%",
"width": "40%",
"zIndex": 1
}, {}, {});
frmCLIQ.setDefaultUnit(kony.flex.DP);
var btnCLIQ = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonPaymentDashFocusFont3",
"height": "70dp",
"id": "btnCLIQ",
"isVisible": true,
"onClick": AS_Button_f6c54d4816904ffa8f127f28bd502b7a,
"skin": "slButtonPaymentDashFont4",
"text": "D",
"top": "0",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblCLIQTitle = new kony.ui.Label({
"centerX": "50%",
"id": "lblCLIQTitle",
"isVisible": true,
"skin": "sknLblPayment",
"text": kony.i18n.getLocalizedString("i18n.CLIQ.Title"),
"top": "1%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
frmCLIQ.add(btnCLIQ, lblCLIQTitle);
flxRow3.add(flxLeft3, frmCLIQ);
var flxRow4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "28%",
"id": "flxRow4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxRow4.setDefaultUnit(kony.flex.DP);
var flxRight3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxRight3",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "10%",
"skin": "slFbox",
"top": "0dp",
"width": "40%",
"zIndex": 1
}, {}, {});
flxRight3.setDefaultUnit(kony.flex.DP);
var btnPaymentH = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonPaymentDashFocus",
"height": "70dp",
"id": "btnPaymentH",
"isVisible": true,
"onClick": AS_Button_b963459b4fa5446e9ec4c4653602dc2e,
"skin": "slButtonPaymentDash",
"text": "A",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var lblPaymentH = new kony.ui.Label({
"centerX": "50%",
"id": "lblPaymentH",
"isVisible": true,
"skin": "sknLblPayment",
"text": kony.i18n.getLocalizedString("i18n.PaymentDash.Paymenthistory"),
"top": "1%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxRight3.add(btnPaymentH, lblPaymentH);
flxRow4.add(flxRight3);
flxOptions.add(flxRow1, flxRow2, flxRow3, flxRow4);
flxMain.add(flxHeaderDashboard, flxOptions);
var flxFooter = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "7%",
"id": "flxFooter",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "menu",
"top": "0",
"width": "100%"
}, {}, {});
flxFooter.setDefaultUnit(kony.flex.DP);
var FlxAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxAccounts",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_hc819bf76a42416c9d326f89589fb4da,
"skin": "sknfocusmenu",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxAccounts.setDefaultUnit(kony.flex.DP);
var img1 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img1",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_accounts_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label03174bff69bb54c = new kony.ui.Label({
"centerX": "50%",
"id": "Label03174bff69bb54c",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnAccounts = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnAccounts",
"isVisible": true,
"onClick": AS_Button_f7ae49a6453c4a1cb1a64adfbb07648e,
"skin": "btnCard",
"text": "H",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
FlxAccounts.add(img1, Label03174bff69bb54c, btnAccounts);
var FlxTranfers = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxTranfers",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_g4029f96c7fc42b78a6af69ecc60b3d0,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxTranfers.setDefaultUnit(kony.flex.DP);
var img2 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img2",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_t_and_p_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label02bec01fd5baf4c = new kony.ui.Label({
"centerX": "50%",
"id": "Label02bec01fd5baf4c",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnTransfers = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnFooterDisablefocusskin",
"height": "50dp",
"id": "btnTransfers",
"isVisible": true,
"onClick": AS_Button_b9ed29b3b2314434a30d66d7089c811f,
"skin": "btnFooterDisable",
"text": "G",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
FlxTranfers.add(img2, Label02bec01fd5baf4c, btnTransfers);
var FlxPayment = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxPayment",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_d97cac1716e14c46b5dc2a36295ad333,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxPayment.setDefaultUnit(kony.flex.DP);
var imgBot = new kony.ui.Image2({
"centerX": "50%",
"height": "40dp",
"id": "imgBot",
"isVisible": false,
"right": "13dp",
"skin": "slImage",
"src": "chaticonactive.png",
"top": "4dp",
"width": "40dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnPayment = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnfooterenablefont",
"height": "50dp",
"id": "btnPayment",
"isVisible": true,
"skin": "btnFooterDisableaccount",
"text": "i",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var CopyLabel0i584dcaf2e8546 = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel0i584dcaf2e8546",
"isVisible": true,
"skin": "sknlblFootertitleFocus",
"text": kony.i18n.getLocalizedString("i18n.alert.transfers"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlxPayment.add(imgBot, btnPayment, CopyLabel0i584dcaf2e8546);
var FlxDeposits = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxDeposits",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_e03dca9494c949bcb7fd29673673a407,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxDeposits.setDefaultUnit(kony.flex.DP);
var img3 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img3",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_deposits_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label04221a71494e848 = new kony.ui.Label({
"centerX": "50%",
"id": "Label04221a71494e848",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.deposits"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnDeposit = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnDeposit",
"isVisible": true,
"skin": "btnCard",
"text": "J",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
FlxDeposits.add(img3, Label04221a71494e848, btnDeposit);
var flxEfawatercoom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxEfawatercoom",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "23%",
"zIndex": 1
}, {}, {});
flxEfawatercoom.setDefaultUnit(kony.flex.DP);
var btnEfawatercoom = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnEfawatercoom",
"isVisible": true,
"onClick": AS_Button_bddcef6018bf4f81a52940ce7040d729,
"skin": "btnCard",
"text": "x",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 12],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblEfawatercoom = new kony.ui.Label({
"centerX": "50%",
"id": "lblEfawatercoom",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.footerBill.BillPayment"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxEfawatercoom.add(btnEfawatercoom, lblEfawatercoom);
var FlxMore = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxMore",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_iccdfe97dfb34ecc83358f7d341db4c2,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxMore.setDefaultUnit(kony.flex.DP);
var img4 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img4",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_more_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label0e5331028c2ef41 = new kony.ui.Label({
"centerX": "50%",
"id": "Label0e5331028c2ef41",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnMore = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnMore",
"isVisible": true,
"onClick": AS_Button_bfb91a8fef87424d8ebc11c99f7921b3,
"skin": "btnCard",
"text": "K",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 12],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
FlxMore.add(img4, Label0e5331028c2ef41, btnMore);
flxFooter.add( FlxMore, flxEfawatercoom, FlxDeposits, FlxPayment, FlxTranfers,FlxAccounts);
frmPaymentDashboard.add(flxMain, flxFooter);
};
function frmPaymentDashboardGlobalsAr() {
frmPaymentDashboardAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmPaymentDashboardAr,
"allowHorizontalBounce": false,
"allowVerticalBounce": false,
"bounces": false,
"enableScrolling": true,
"enabledForIdleTimeout": true,
"id": "frmPaymentDashboard",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"preShow": AS_Form_badc5fdb8d4248e28dce0ace96e7a751,
"skin": "sknBackground",
"verticalScrollIndicator": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"bouncesZoom": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": true,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
