//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmNewBillKAAr() {
frmNewBillKA.setDefaultUnit(kony.flex.DP);
var mainContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": false,
"id": "mainContainer",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"minHeight": "100%",
"skin": "skncontainerBkg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
mainContainer.setDefaultUnit(kony.flex.DP);
var titleBarNewPayBill = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "titleBarNewPayBill",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntitleBarGradient",
"top": "0dp",
"width": "101%",
"zIndex": 1
}, {}, {});
titleBarNewPayBill.setDefaultUnit(kony.flex.DP);
var transferPayTitleLabel = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "transferPayTitleLabel",
"isVisible": true,
"skin": "sknnavBarTitle",
"text": kony.i18n.getLocalizedString("i18n.transfer.payBill"),
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var buttonLeft = new kony.ui.Button({
"focusSkin": "skntitleBarTextButtonFocus",
"height": "50dp",
"id": "buttonLeft",
"isVisible": true,
"right": "0dp",
"minWidth": "50dp",
"onClick": AS_NamedActions_9d0527c068364c3fac9c44307313cdd1,
"skin": "skntitleBarTextButton",
"text": kony.i18n.getLocalizedString("i18n.common.cancel"),
"top": "0dp",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
titleBarNewPayBill.add(transferPayTitleLabel, buttonLeft);
var fromCard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "fromCard",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknCopyslFbox007a422d071dd4e",
"top": "0dp",
"width": "100%"
}, {}, {});
fromCard.setDefaultUnit(kony.flex.DP);
var fromCardInner = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": false,
"id": "fromCardInner",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "3%",
"skin": "skntransferCardInner",
"top": "60dp",
"width": "94%",
"zIndex": 1
}, {}, {});
fromCardInner.setDefaultUnit(kony.flex.DP);
var CopytoInternalLabel0dd19c29451b147 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "CopytoInternalLabel0dd19c29451b147",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknCopyslFbox0581119363ce54c",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {}, {});
CopytoInternalLabel0dd19c29451b147.setDefaultUnit(kony.flex.DP);
var CopyborderBottom041db968c787c4e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom041db968c787c4e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "sknCopyslFbox0a29a14ecfe6442",
"width": "96%",
"zIndex": 1
}, {}, {});
CopyborderBottom041db968c787c4e.setDefaultUnit(kony.flex.DP);
CopyborderBottom041db968c787c4e.add();
var CopyLabel03e39ab4661a845 = new kony.ui.Label({
"centerY": "50%",
"id": "CopyLabel03e39ab4661a845",
"isVisible": true,
"right": "4%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.transfer.xyzBankAccounts"),
"top": "0dp",
"width": "96%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopytoInternalLabel0dd19c29451b147.add(CopyborderBottom041db968c787c4e, CopyLabel03e39ab4661a845);
var segInternalFromAccountsKA = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [{
"amountAccount1": "$00.00",
"amountcurrBal": "$00.00",
"amtOutsatndingBal": "$00.00",
"lblColorKA": "",
"lblRowSeparator": "Label",
"nameAccount1": "Account Name",
"typeAccount": "Available Balance",
"typeKA": ""
}, {
"amountAccount1": "$00.00",
"amountcurrBal": "$00.00",
"amtOutsatndingBal": "$00.00",
"lblColorKA": "",
"lblRowSeparator": "Label",
"nameAccount1": "Account Name",
"typeAccount": "Available Balance",
"typeKA": ""
}, {
"amountAccount1": "$00.00",
"amountcurrBal": "$00.00",
"amtOutsatndingBal": "$00.00",
"lblColorKA": "",
"lblRowSeparator": "Label",
"nameAccount1": "Account Name",
"typeAccount": "Available Balance",
"typeKA": ""
}],
"groupCells": false,
"id": "segInternalFromAccountsKA",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_dd252b3da767456c8c183d651656ec9a,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": yourAccount1,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": false,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"amountAccount1": "amountAccount1",
"amountcurrBal": "amountcurrBal",
"amtOutsatndingBal": "amtOutsatndingBal",
"dummyAccountName": "dummyAccountName",
"dummyAccountNumber": "dummyAccountNumber",
"isPFMLabel": "isPFMLabel",
"lblBankName": "lblBankName",
"lblColorKA": "lblColorKA",
"lblRowSeparator": "lblRowSeparator",
"nameAccount1": "nameAccount1",
"nameContainer": "nameContainer",
"typeAccount": "typeAccount",
"typeKA": "typeKA",
"yourAccount1": "yourAccount1"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": false
});
var lblTransactionType = new kony.ui.Label({
"id": "lblTransactionType",
"isVisible": false,
"right": "206dp",
"skin": "slLabel",
"text": "Label",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
fromCardInner.add(CopytoInternalLabel0dd19c29451b147, segInternalFromAccountsKA, lblTransactionType);
var fromCardTitle = new kony.ui.Label({
"centerX": "50%",
"height": "60dp",
"id": "fromCardTitle",
"isVisible": true,
"skin": "skntransferPaySectionHeader",
"text": kony.i18n.getLocalizedString("i18n.transfer.transferMoneyFromC"),
"top": "0dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var fromAccountPick = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"focusSkin": "sknyourAccountCardFocus",
"height": "84dp",
"id": "fromAccountPick",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntransferPayCardPick",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
fromAccountPick.setDefaultUnit(kony.flex.DP);
var fromAccountNameContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": false,
"height": "100%",
"id": "fromAccountNameContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknslFbox",
"top": "0dp",
"width": "44%",
"zIndex": 1
}, {}, {});
fromAccountNameContainer.setDefaultUnit(kony.flex.DP);
var fromNamePick = new kony.ui.Label({
"centerY": "50%",
"id": "fromNamePick",
"isVisible": true,
"right": "13dp",
"maxWidth": "90%",
"skin": "sknaccountNamePick",
"text": kony.i18n.getLocalizedString("i18n.common.accountName"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var fromAccountColorPick = new kony.ui.Label({
"centerY": "60%",
"height": "50%",
"id": "fromAccountColorPick",
"isVisible": true,
"right": "1dp",
"maxWidth": "90%",
"skin": "sknaccountNamePick",
"width": "4dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var fromAccountBankNameKA = new kony.ui.Label({
"id": "fromAccountBankNameKA",
"isVisible": true,
"right": "13dp",
"skin": "skntransLeftLabel",
"text": "XYZ Bank",
"top": "55dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
fromAccountNameContainer.add(fromNamePick, fromAccountColorPick, fromAccountBankNameKA);
var fromAccountAmountContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "fromAccountAmountContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "11%",
"skin": "sknslFbox",
"top": "0dp",
"width": "35%",
"zIndex": 1
}, {}, {});
fromAccountAmountContainer.setDefaultUnit(kony.flex.DP);
var fromAmountPick = new kony.ui.Label({
"centerY": "50%",
"id": "fromAmountPick",
"isVisible": true,
"left": "20dp",
"skin": "sknaccountAmountSelected",
"text": "$00.00",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyamountAccountOne09f61920910454b = new kony.ui.Label({
"bottom": "13dp",
"id": "CopyamountAccountOne09f61920910454b",
"isVisible": true,
"left": "20dp",
"skin": "sknaccountAvailableBalanceLabelSelected",
"text": kony.i18n.getLocalizedString("i18n.deposit.availableBalance"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
fromAccountAmountContainer.add(fromAmountPick, CopyamountAccountOne09f61920910454b);
var editFromCard = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "skneditFormFocus",
"height": "100%",
"id": "editFromCard",
"isVisible": true,
"onClick": AS_Button_9cf36eaf8cdb4b089d1d70a36f1a9116,
"left": "0dp",
"skin": "skneditForm",
"text": kony.i18n.getLocalizedString("i18n.common.edit"),
"width": "56dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var fromlblAccountNumberKA = new kony.ui.Label({
"id": "fromlblAccountNumberKA",
"isVisible": false,
"right": "115dp",
"skin": "slLabel",
"top": "19dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var fromLblFlex = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": false,
"id": "fromLblFlex",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
fromLblFlex.setDefaultUnit(kony.flex.DP);
var fromLabel = new kony.ui.Label({
"id": "fromLabel",
"isVisible": true,
"right": "13dp",
"skin": "skntransLeftLabel",
"text": kony.i18n.getLocalizedString("i18n.common.fromc"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
fromLblFlex.add(fromLabel);
fromAccountPick.add(fromAccountNameContainer, fromAccountAmountContainer, editFromCard, fromlblAccountNumberKA, fromLblFlex);
var lblAlerts1 = new kony.ui.Label({
"centerX": "50%",
"height": "60dp",
"id": "lblAlerts1",
"isVisible": false,
"skin": "sknfrontback30363FwhiteBgKA",
"text": kony.i18n.getLocalizedString("i18n.alerts.NofromAccounts"),
"top": "0dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
fromCard.add(fromCardInner, fromCardTitle, fromAccountPick, lblAlerts1);
var toCard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "toCard",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknCopyslFbox0320dc8ccb87b4d",
"top": "0dp",
"width": "100%"
}, {}, {});
toCard.setDefaultUnit(kony.flex.DP);
var toCardInner = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": false,
"id": "toCardInner",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "3%",
"skin": "skntransferCardInner",
"top": "60dp",
"width": "94%",
"zIndex": 1
}, {}, {});
toCardInner.setDefaultUnit(kony.flex.DP);
var toInternalLabel = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "toInternalLabel",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknCopyslFbox0581119363ce54c",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {}, {});
toInternalLabel.setDefaultUnit(kony.flex.DP);
var CopyborderBottom06399950388e345 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom06399950388e345",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "sknCopyslFbox0a29a14ecfe6442",
"width": "96%",
"zIndex": 1
}, {}, {});
CopyborderBottom06399950388e345.setDefaultUnit(kony.flex.DP);
CopyborderBottom06399950388e345.add();
var Label0cecf1132bf8049 = new kony.ui.Label({
"centerY": "50%",
"id": "Label0cecf1132bf8049",
"isVisible": true,
"right": "4%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.transfer.recentAccounts"),
"top": "0dp",
"width": "96%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
toInternalLabel.add(CopyborderBottom06399950388e345, Label0cecf1132bf8049);
var segPayeeNamesKA = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50%",
"data": [{
"lblAccountNumberKA": "Label",
"lblContact": "Example",
"lblRowSeparator": "",
"lblType": "ABC Bank"
}, {
"lblAccountNumberKA": "Label",
"lblContact": "Example",
"lblRowSeparator": "",
"lblType": "ABC Bank"
}, {
"lblAccountNumberKA": "Label",
"lblContact": "Example",
"lblRowSeparator": "",
"lblType": "ABC Bank"
}],
"groupCells": false,
"id": "segPayeeNamesKA",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_aff6a0d3062a419ea604b64c8e1953ea,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": Copycontainer03f00119dae464d,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": false,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Copycontainer03f00119dae464d": "Copycontainer03f00119dae464d",
"lblAccountNumberKA": "lblAccountNumberKA",
"lblContact": "lblContact",
"lblRowSeparator": "lblRowSeparator",
"lblType": "lblType"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": false
});
var lblAlerts = new kony.ui.Label({
"centerX": "50%",
"height": "60dp",
"id": "lblAlerts",
"isVisible": false,
"skin": "sknfrontback30363FwhiteBgKA",
"text": kony.i18n.getLocalizedString("i18n.alerts.NoAccountsMsg"),
"top": "0dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flexRecent = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "58dp",
"id": "flexRecent",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {}, {});
flexRecent.setDefaultUnit(kony.flex.DP);
var lblEmptyRecentAccounts = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblEmptyRecentAccounts",
"isVisible": true,
"skin": "sknEmptyRecentAccounts",
"text": kony.i18n.getLocalizedString("i18n.billPay.recentAccounts"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flexRecent.add(lblEmptyRecentAccounts);
var btnViewAll = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknsecondaryActionFocus",
"height": "58dp",
"id": "btnViewAll",
"isVisible": true,
"onClick": AS_Button_df07e2eab4ce47c380ff09521e2da648,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.billPay.viewAll"),
"top": "0dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var CopyborderBottom0j246735cff164d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom0j246735cff164d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknCopyslFbox0a29a14ecfe6442",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom0j246735cff164d.setDefaultUnit(kony.flex.DP);
CopyborderBottom0j246735cff164d.add();
var addExternalAccount = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknsecondaryActionFocus",
"height": "58dp",
"id": "addExternalAccount",
"isVisible": true,
"onClick": AS_Button_099bf61b93ef41c3bff1bf1bd00ec1f8,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.common.addNewPayee"),
"top": "0dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var CopyborderBottom0d6325f6a994246 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyborderBottom0d6325f6a994246",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknCopyslFbox0a29a14ecfe6442",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyborderBottom0d6325f6a994246.setDefaultUnit(kony.flex.DP);
CopyborderBottom0d6325f6a994246.add();
toCardInner.add(toInternalLabel, segPayeeNamesKA, lblAlerts, flexRecent, btnViewAll, CopyborderBottom0j246735cff164d, addExternalAccount, CopyborderBottom0d6325f6a994246);
var toCardTitle = new kony.ui.Label({
"centerX": "50%",
"height": "60dp",
"id": "toCardTitle",
"isVisible": true,
"skin": "skntransferPaySectionHeader",
"text": kony.i18n.getLocalizedString("i18n.transfer.payToC"),
"top": "0dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var toAccountPick = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"focusSkin": "sknyourAccountCardFocus",
"height": "62dp",
"id": "toAccountPick",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntransferPayCardPick",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
toAccountPick.setDefaultUnit(kony.flex.DP);
var toAccountNameContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "toAccountNameContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknslFbox",
"top": "0dp",
"width": "44%",
"zIndex": 1
}, {}, {});
toAccountNameContainer.setDefaultUnit(kony.flex.DP);
var toNamePick = new kony.ui.Label({
"centerY": "65%",
"id": "toNamePick",
"isVisible": true,
"right": "13dp",
"maxWidth": "90%",
"skin": "sknaccountNamePick",
"text": kony.i18n.getLocalizedString("i18n.common.accountName"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
toAccountNameContainer.add(toNamePick);
var editToCard = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "skneditFormFocus",
"height": "100%",
"id": "editToCard",
"isVisible": true,
"onClick": AS_Button_51c94f418c5c4b889e33aacf7ca712a2,
"left": "0dp",
"skin": "skneditForm",
"text": kony.i18n.getLocalizedString("i18n.common.edit"),
"width": "56dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var tolblAccountNumberKA = new kony.ui.Label({
"id": "tolblAccountNumberKA",
"isVisible": false,
"right": "115dp",
"skin": "slLabel",
"top": "19dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var toLblFlex = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": false,
"id": "toLblFlex",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
toLblFlex.setDefaultUnit(kony.flex.DP);
var toLabel = new kony.ui.Label({
"id": "toLabel",
"isVisible": true,
"right": "13dp",
"skin": "skntransLeftLabel",
"text": kony.i18n.getLocalizedString("i18n.common.To"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
toLblFlex.add(toLabel);
toAccountPick.add(toAccountNameContainer, editToCard, tolblAccountNumberKA, toLblFlex);
var lblTransactionIdKA = new kony.ui.Label({
"id": "lblTransactionIdKA",
"isVisible": false,
"right": "14dp",
"skin": "slLabel",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
toCard.add(toCardInner, toCardTitle, toAccountPick, lblTransactionIdKA);
var amountCard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "120dp",
"id": "amountCard",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknCopyslFbox007a422d071dd4e",
"top": "0dp",
"width": "100%"
}, {}, {});
amountCard.setDefaultUnit(kony.flex.DP);
var amountCardInner = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": false,
"id": "amountCardInner",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skntransferCardInner",
"top": "60dp",
"width": "100%",
"zIndex": 1
}, {}, {});
amountCardInner.setDefaultUnit(kony.flex.DP);
var CopyamountWrapper0be5b03b6e9a449 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "60dp",
"id": "CopyamountWrapper0be5b03b6e9a449",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "sknCopyslFbox05b3140035fa546",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyamountWrapper0be5b03b6e9a449.setDefaultUnit(kony.flex.DP);
var lblCurrencyType = new kony.ui.Label({
"id": "lblCurrencyType",
"isVisible": true,
"right": "5%",
"skin": "sknCopyslLabel09de2a4a1c0e745",
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var amountTextContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "amountTextContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {}, {});
amountTextContainer.setDefaultUnit(kony.flex.DP);
var amountTextField = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "sknamountEntryField",
"height": "60dp",
"id": "amountTextField",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
"right": "0dp",
"maxTextLength": null,
"onTextChange": AS_TextField_1bf0f9d628104f34bbac1e31416e1cb1,
"placeholder": "0",
"secureTextEntry": false,
"skin": "sknamountEntryField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,2, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"closeButtonText": "Done",
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknamountEntryField",
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblInvalidAmount = new kony.ui.Label({
"bottom": "12%",
"id": "lblInvalidAmount",
"isVisible": false,
"left": "5%",
"skin": "sknD0021BFLatoRegular",
"text": "Invalid Amount",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
amountTextContainer.add(amountTextField, lblInvalidAmount);
CopyamountWrapper0be5b03b6e9a449.add( amountTextContainer,lblCurrencyType);
amountCardInner.add(CopyamountWrapper0be5b03b6e9a449);
var amountTitle = new kony.ui.Label({
"centerX": "50%",
"height": "60dp",
"id": "amountTitle",
"isVisible": true,
"right": "0dp",
"skin": "skntransferPaySectionHeader",
"text": kony.i18n.getLocalizedString("i18n.deposit.amount"),
"top": "0dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
amountCard.add(amountCardInner, amountTitle);
var dateCard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "dateCard",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknCopyslFbox007a422d071dd4e",
"top": "1dp",
"width": "100%"
}, {}, {});
dateCard.setDefaultUnit(kony.flex.DP);
var datePick = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"focusSkin": "sknyourAccountCardFocus",
"height": "62dp",
"id": "datePick",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntransferPayCard",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
datePick.setDefaultUnit(kony.flex.DP);
var datePickContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "datePickContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"zIndex": 1
}, {}, {});
datePickContainer.setDefaultUnit(kony.flex.DP);
var calDateKA = new kony.ui.Calendar({
"calendarIcon": "calendar.png",
"centerY": "65%",
"dateFormat": "dd/MMM/yyyy",
"id": "calDateKA",
"isVisible": true,
"right": "13dp",
"skin": "sknstandardDatepicker",
"validEndDate": [1, 6, 2017],
"validStartDate": [31, 5, 2016],
"viewConfig": {},
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"mode": constants.CALENDAR_WHEEL_ONLY_DATE
});
var dateLabel = new kony.ui.Label({
"id": "dateLabel",
"isVisible": true,
"right": "13dp",
"skin": "skntransLeftLabel",
"text": "Date: ",
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
datePickContainer.add(calDateKA, dateLabel);
datePick.add(datePickContainer);
dateCard.add(datePick);
var confirmContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "200dp",
"id": "confirmContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknCopyslFbox0cb1e5738a8eb4e",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
confirmContainer.setDefaultUnit(kony.flex.DP);
var confirmTransaction = new kony.ui.Button({
"focusSkin": "sknprimaryActionFocus",
"height": "42dp",
"id": "confirmTransaction",
"isVisible": true,
"right": "10%",
"onClick": AS_Button_b650f5e4fa9442e5a4830f7ef471a8ac,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.manage_payee.btnPayBill"),
"top": "20dp",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
confirmContainer.add(confirmTransaction);
mainContainer.add(titleBarNewPayBill, fromCard, toCard, amountCard, dateCard, confirmContainer);
var flxFormMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxFormMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFormMain.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxTop = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxTop",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTop.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_cc3223c3068e49e491a4b759f6c8b94f,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "70%",
"id": "lblTitle",
"isVisible": true,
"maxNumberOfLines": 1,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.billsPay.PayNow"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblNext = new kony.ui.Label({
"centerY": "50%",
"id": "lblNext",
"isVisible": true,
"left": "86%",
"onTouchStart": AS_Label_cc8ef073b8dd4ef691820b676f4d6388,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTop.add(flxBack, lblTitle, lblNext);
var flxTab = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40%",
"id": "flxTab",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "5%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTab.setDefaultUnit(kony.flex.DP);
var flxContent = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "65%",
"id": "flxContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFboxOuterRing",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxContent.setDefaultUnit(kony.flex.DP);
var btnPostPaid = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnPostPaid",
"isVisible": true,
"right": "0",
"onClick": AS_Button_b69c2c721b7b46d89e00468b205c6fef,
"skin": "slButtonWhiteTab",
"text": kony.i18n.getLocalizedString("i18n.billsPay.PostPaid"),
"top": "0dp",
"width": "50%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnPrePaid = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnPrePaid",
"isVisible": true,
"right": "0",
"onClick": AS_Button_jb3ba4c4a68b4363beb962d2c9299486,
"skin": "slButtonWhiteTabDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.PrePaid"),
"top": "0dp",
"width": "50%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxContent.add( btnPrePaid,btnPostPaid);
flxTab.add(flxContent);
flxHeader.add(flxTop, flxTab);
var flxMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"centerX": "50%",
"clipBounds": true,
"enableScrolling": true,
"height": "82%",
"horizontalScrollIndicator": true,
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxBillerCategory = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBillerCategory",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBillerCategory.setDefaultUnit(kony.flex.DP);
var tbxBillerCategory = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxBillerCategory",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBillerCategory = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBillerCategory",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBillerCategory.setDefaultUnit(kony.flex.DP);
flxUnderlineBillerCategory.add();
var flxBillerCategoryHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxBillerCategoryHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_db785f233e3b4d4286756cf0f16f6437,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxBillerCategoryHolder.setDefaultUnit(kony.flex.DP);
var lblBillerCategory = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblBillerCategory",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.BillerCategory"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowBillerCategory = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowBillerCategory",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBillerCategoryHolder.add(lblBillerCategory, lblArrowBillerCategory);
flxBillerCategory.add(tbxBillerCategory, flxUnderlineBillerCategory, flxBillerCategoryHolder);
var flxBillerName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBillerName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBillerName.setDefaultUnit(kony.flex.DP);
var tbxBillerName = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxBillerName",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBillerName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBillerName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBillerName.setDefaultUnit(kony.flex.DP);
flxUnderlineBillerName.add();
var flxBillerNameHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxBillerNameHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_cfc19f88905544d7b9147bb65864fe51,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxBillerNameHolder.setDefaultUnit(kony.flex.DP);
var lblBillerName = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblBillerName",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.billerName"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowBillerName = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowBillerName",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBillerNameHolder.add(lblBillerName, lblArrowBillerName);
flxBillerName.add(tbxBillerName, flxUnderlineBillerName, flxBillerNameHolder);
var flxServiceType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxServiceType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxServiceType.setDefaultUnit(kony.flex.DP);
var tbxServiceType = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxServiceType",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineServiceType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineServiceType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineServiceType.setDefaultUnit(kony.flex.DP);
flxUnderlineServiceType.add();
var flxServiceTypeHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxServiceTypeHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_e842dbce171445b2b3eed7ab8cf42267,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxServiceTypeHolder.setDefaultUnit(kony.flex.DP);
var lblServiceType = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblServiceType",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowServiceType = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowServiceType",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxServiceTypeHolder.add(lblServiceType, lblArrowServiceType);
flxServiceType.add(tbxServiceType, flxUnderlineServiceType, flxServiceTypeHolder);
var flxDenomination = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxDenomination",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxDenomination.setDefaultUnit(kony.flex.DP);
var tbxDenomination = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxDenomination",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineDenomination = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineDenomination",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineDenomination.setDefaultUnit(kony.flex.DP);
flxUnderlineDenomination.add();
var flxDenominationHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxDenominationHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_g66e3994d1a643f79821edf39275700b,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxDenominationHolder.setDefaultUnit(kony.flex.DP);
var lblDenomination = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblDenomination",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": "Denomination",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "85%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowDenomination = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowDenomination",
"isVisible": true,
"right": "90%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxDenominationHolder.add(lblDenomination, lblArrowDenomination);
flxDenomination.add(tbxDenomination, flxUnderlineDenomination, flxDenominationHolder);
var flxBillerNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "90dp",
"id": "flxBillerNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBillerNumber.setDefaultUnit(kony.flex.DP);
var tbxBillerNumber = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "25%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "55%",
"id": "tbxBillerNumber",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"maxTextLength": 35,
"onDone": AS_TextField_ea7f25c0ddeb4a0cb14ec205d66c8e0f,
"onTextChange": AS_TextField_fc5e20ff942742e5bdc5c4df915ba81e,
"onTouchStart": AS_TextField_fac8df2dc4af48f7895969d86fa85f15,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 2
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_j3796e31a5fa4527bb58c5d6f67e8e9d,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBillerNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBillerNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "75%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBillerNumber.setDefaultUnit(kony.flex.DP);
flxUnderlineBillerNumber.add();
var lblBillerNumber = new kony.ui.Label({
"id": "lblBillerNumber",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.BillerNumber"),
"top": "34%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblserviceTypeHint = new kony.ui.Label({
"id": "lblserviceTypeHint",
"isVisible": false,
"right": "2%",
"left": 0,
"skin": "sknLblWhite100",
"top": "75%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBillerNumber.add(tbxBillerNumber, flxUnderlineBillerNumber, lblBillerNumber, lblserviceTypeHint);
var flxAmounttoPay = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAmounttoPay",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAmounttoPay.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0j2905df96caa44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0j2905df96caa44",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0j2905df96caa44.setDefaultUnit(kony.flex.DP);
CopylblUnderline0j2905df96caa44.add();
var CopylblAmount0ff41ac4bb82b48 = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0ff41ac4bb82b48",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": "Amount to Pay",
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0aad558f3b2a24f = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "CopyLabel0aad558f3b2a24f",
"isVisible": false,
"skin": "sknLblBack",
"text": "10.000",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var TextField0i6f61eae04ab4a = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "TextField0i6f61eae04ab4a",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"placeholder": "0.000",
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 2
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxAmounttoPay.add(CopylblUnderline0j2905df96caa44, CopylblAmount0ff41ac4bb82b48, CopyLabel0aad558f3b2a24f, TextField0i6f61eae04ab4a);
var flxDueAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxDueAmount",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxDueAmount.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0ee4cc246b9414f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0ee4cc246b9414f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0ee4cc246b9414f.setDefaultUnit(kony.flex.DP);
CopylblUnderline0ee4cc246b9414f.add();
var CopylblAmount0d113905789c54b = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0d113905789c54b",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": "Due Amount",
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var Label0g6cf31795ba04e = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "Label0g6cf31795ba04e",
"isVisible": true,
"skin": "sknLblBack",
"text": "1000.000 JOD",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxDueAmount.add(CopylblUnderline0ee4cc246b9414f, CopylblAmount0d113905789c54b, Label0g6cf31795ba04e);
var flxMinimumAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxMinimumAmount",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxMinimumAmount.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0had97db2c9184a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0had97db2c9184a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0had97db2c9184a.setDefaultUnit(kony.flex.DP);
CopylblUnderline0had97db2c9184a.add();
var CopylblAmount0b770bb8285be4b = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0b770bb8285be4b",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": "Minimum Amount",
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0g85ffd430a1442 = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "CopyLabel0g85ffd430a1442",
"isVisible": true,
"right": "115dp",
"skin": "sknLblBack",
"text": "50.000 JOD",
"top": "20dp",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxMinimumAmount.add(CopylblUnderline0had97db2c9184a, CopylblAmount0b770bb8285be4b, CopyLabel0g85ffd430a1442);
var flxMaximumAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxMaximumAmount",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxMaximumAmount.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0d2fd8c33debc4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0d2fd8c33debc4d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0d2fd8c33debc4d.setDefaultUnit(kony.flex.DP);
CopylblUnderline0d2fd8c33debc4d.add();
var CopylblAmount0baddddd3cfcb4c = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0baddddd3cfcb4c",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": "Maximum Amount",
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0fd0aa586940148 = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "CopyLabel0fd0aa586940148",
"isVisible": true,
"right": "115dp",
"skin": "sknLblBack",
"text": "1000.000 JOD",
"top": "20dp",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxMaximumAmount.add(CopylblUnderline0d2fd8c33debc4d, CopylblAmount0baddddd3cfcb4c, CopyLabel0fd0aa586940148);
var flxIssueDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxIssueDate",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxIssueDate.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0d87b81a58dc14f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0d87b81a58dc14f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0d87b81a58dc14f.setDefaultUnit(kony.flex.DP);
CopylblUnderline0d87b81a58dc14f.add();
var CopylblAmount0i85c07b370bd41 = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0i85c07b370bd41",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": "Issue Date",
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0dcd99475ff4142 = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "CopyLabel0dcd99475ff4142",
"isVisible": true,
"right": "115dp",
"skin": "sknLblBack",
"text": "18/02/2018",
"top": "20dp",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIssueDate.add(CopylblUnderline0d87b81a58dc14f, CopylblAmount0i85c07b370bd41, CopyLabel0dcd99475ff4142);
var flxDueDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxDueDate",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxDueDate.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0f7e1a075a4e544 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0f7e1a075a4e544",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0f7e1a075a4e544.setDefaultUnit(kony.flex.DP);
CopylblUnderline0f7e1a075a4e544.add();
var CopylblAmount0b4f72543e63046 = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0b4f72543e63046",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": "Due Date",
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0ce2343ca637e41 = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "CopyLabel0ce2343ca637e41",
"isVisible": true,
"right": "115dp",
"skin": "sknLblBack",
"text": "18/02/2018",
"top": "20dp",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxDueDate.add(CopylblUnderline0f7e1a075a4e544, CopylblAmount0b4f72543e63046, CopyLabel0ce2343ca637e41);
var flxFee = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxFee",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxFee.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0cfde8e7016ef44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0cfde8e7016ef44",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0cfde8e7016ef44.setDefaultUnit(kony.flex.DP);
CopylblUnderline0cfde8e7016ef44.add();
var CopylblAmount0c6fe141fc33243 = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0c6fe141fc33243",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": "Fees",
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0b559c02331504c = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "CopyLabel0b559c02331504c",
"isVisible": true,
"right": "115dp",
"skin": "sknLblBack",
"text": "1 JOD",
"top": "20dp",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxFee.add(CopylblUnderline0cfde8e7016ef44, CopylblAmount0c6fe141fc33243, CopyLabel0b559c02331504c);
var flxTotalAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxTotalAmount",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxTotalAmount.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0d8dc968a336541 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0d8dc968a336541",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0d8dc968a336541.setDefaultUnit(kony.flex.DP);
CopylblUnderline0d8dc968a336541.add();
var CopylblAmount0gb9935716d5044 = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0gb9935716d5044",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": "Total Amount",
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0f2df8b43ed004e = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "CopyLabel0f2df8b43ed004e",
"isVisible": true,
"right": "115dp",
"skin": "sknLblBack",
"text": "51 JOD",
"top": "20dp",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTotalAmount.add(CopylblUnderline0d8dc968a336541, CopylblAmount0gb9935716d5044, CopyLabel0f2df8b43ed004e);
var flxRadioAccCardsSelection = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": false,
"height": "70dp",
"id": "flxRadioAccCardsSelection",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxRadioAccCardsSelection.setDefaultUnit(kony.flex.DP);
var RadioBtnAccCards = new kony.ui.RadioButtonGroup({
"centerY": "65%",
"height": "60%",
"id": "RadioBtnAccCards",
"isVisible": false,
"right": "2%",
"masterData": [["1", kony.i18n.getLocalizedString("i18n.billsPay.Accounts")],["2", kony.i18n.getLocalizedString("i18n.billsPay.Cards")]],
"onSelection": AS_RadioButtonGroup_b84f38c24d3f43e78f57ff3a37729c2f,
"selectedKey": "1",
"selectedKeyValue": ["1", "Accounts"],
"skin": "CopyslRadioButtonGroup0b4da25f7d5c746",
"top": "17dp",
"width": "90%",
"zIndex": 1
}, {
"itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"groupCells": false,
"viewConfig": {
"toggleViewConfig": {
"viewStyle": constants.RADIOGROUP_TOGGLE_VIEW_STYLE_PLAIN
}
},
"viewType": constants.RADIOGROUP_VIEW_TYPE_ONSCREENWHEEL
});
var btnBillsPayAccounts = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknRDOWhiteBRDBOJFont",
"id": "btnBillsPayAccounts",
"isVisible": true,
"right": "5%",
"onClick": AS_Button_dce53326846441e79f542df80f769e50,
"skin": "sknRDOWhiteBRDBOJFont",
"text": "t",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblBillsPayAccounts = new kony.ui.Label({
"centerY": "50%",
"id": "lblBillsPayAccounts",
"isVisible": true,
"right": "15%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnBillsPayCards = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknRDOWhiteBRDBOJFont",
"id": "btnBillsPayCards",
"isVisible": false,
"right": "55%",
"onClick": AS_Button_g000e7df4b70472eb1a347e2921be6fe,
"skin": "sknRDOWhiteBRDBOJFont",
"text": "s",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblBillsPayCards = new kony.ui.Label({
"centerY": "50%",
"id": "lblBillsPayCards",
"isVisible": false,
"right": "65%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxRadioAccCardsSelection.add(RadioBtnAccCards, btnBillsPayAccounts, lblBillsPayAccounts, btnBillsPayCards, lblBillsPayCards);
var flxPaymentMode = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxPaymentMode",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxPaymentMode.setDefaultUnit(kony.flex.DP);
var tbxPaymentMode = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxPaymentMode",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlinePaymentMode = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlinePaymentMode",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlinePaymentMode.setDefaultUnit(kony.flex.DP);
flxUnderlinePaymentMode.add();
var flxPaymentModeTypeHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxPaymentModeTypeHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_d9e1958ddf1d4996a48955f75dc1131a,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxPaymentModeTypeHolder.setDefaultUnit(kony.flex.DP);
var lblPaymentMode = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblPaymentMode",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowPaymentMode = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowPaymentMode",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxPaymentModeTypeHolder.add(lblPaymentMode, lblArrowPaymentMode);
flxPaymentMode.add(tbxPaymentMode, flxUnderlinePaymentMode, flxPaymentModeTypeHolder);
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAmount",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var tbxAmount = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxAmount",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
"maxTextLength": null,
"onDone": AS_TextField_cd07318128654403a104f8346064d68d,
"onTextChange": AS_TextField_aedadc3617a44f70b3b6192768f3bea8,
"onTouchStart": AS_TextField_f2e5318357034800a2dbab64f3d0fb6e,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"width": "100%",
"zIndex": 2
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_dfa61869440347beb667108845967cd5,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblUnderline = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "lblUnderline",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
lblUnderline.setDefaultUnit(kony.flex.DP);
lblUnderline.add();
var flxAmountTypeHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxAmountTypeHolder",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmountTypeHolder.setDefaultUnit(kony.flex.DP);
var CopylblArrowPaymentMode0a6c9e8837c8e4b = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblArrowPaymentMode0a6c9e8837c8e4b",
"isVisible": true,
"right": "90%",
"skin": "sknBackIconDisabled",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAmountTypeHolder.add(CopylblArrowPaymentMode0a6c9e8837c8e4b);
var lblAmount = new kony.ui.Label({
"id": "lblAmount",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Amount"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAmount.add(tbxAmount, lblUnderline, flxAmountTypeHolder, lblAmount);
var CopyflxAmount0ba64434a77864d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "CopyflxAmount0ba64434a77864d",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyflxAmount0ba64434a77864d.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0ae1c60bc0a3742 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0ae1c60bc0a3742",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0ae1c60bc0a3742.setDefaultUnit(kony.flex.DP);
CopylblUnderline0ae1c60bc0a3742.add();
var CopylblAmount0f8ac53592c5b4b = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0f8ac53592c5b4b",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": "Due Amount",
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0d443c3e9873447 = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "CopyLabel0d443c3e9873447",
"isVisible": true,
"right": "115dp",
"skin": "sknLblBack",
"text": "Label",
"top": "20dp",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxAmount0ba64434a77864d.add(CopylblUnderline0ae1c60bc0a3742, CopylblAmount0f8ac53592c5b4b, CopyLabel0d443c3e9873447);
var CopyflxAmount0i0a9802158c244 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "CopyflxAmount0i0a9802158c244",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyflxAmount0i0a9802158c244.setDefaultUnit(kony.flex.DP);
var CopytbxAmount0f4a624c0ce2b49 = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "CopytbxAmount0f4a624c0ce2b49",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"text": "0.00",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylblUnderline0bd4c2d5fe8e74f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0bd4c2d5fe8e74f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0bd4c2d5fe8e74f.setDefaultUnit(kony.flex.DP);
CopylblUnderline0bd4c2d5fe8e74f.add();
var CopylblAmount0b683426e0b864e = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0b683426e0b864e",
"isVisible": true,
"right": "2%",
"skin": "sknLblSmall",
"text": "Amount",
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxAmount0i0a9802158c244.add(CopytbxAmount0f4a624c0ce2b49, CopylblUnderline0bd4c2d5fe8e74f, CopylblAmount0b683426e0b864e);
var CopyflxAmount0f826bde07af648 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "CopyflxAmount0f826bde07af648",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyflxAmount0f826bde07af648.setDefaultUnit(kony.flex.DP);
var CopytbxAmount0hf7f5e583d3e49 = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "CopytbxAmount0hf7f5e583d3e49",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"text": "0.00",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylblUnderline0d2c7e30b72bd42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0d2c7e30b72bd42",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0d2c7e30b72bd42.setDefaultUnit(kony.flex.DP);
CopylblUnderline0d2c7e30b72bd42.add();
var CopylblAmount0ha80ad831a6241 = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0ha80ad831a6241",
"isVisible": true,
"right": "2%",
"skin": "sknLblSmall",
"text": "Amount",
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxAmount0f826bde07af648.add(CopytbxAmount0hf7f5e583d3e49, CopylblUnderline0d2c7e30b72bd42, CopylblAmount0ha80ad831a6241);
var flxIDType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxIDType",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxIDType.setDefaultUnit(kony.flex.DP);
var tbxIDType = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxIDType",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineIDType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineIDType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineIDType.setDefaultUnit(kony.flex.DP);
flxUnderlineIDType.add();
var flxIDTypeHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxIDTypeHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxIDTypeHolder.setDefaultUnit(kony.flex.DP);
var lblIDType = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblIDType",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": "ID Type",
"width": "80%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowIDType = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowIDType",
"isVisible": true,
"right": "92%",
"skin": "sknBackIconDisabled",
"text": "d",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIDTypeHolder.add(lblIDType, lblArrowIDType);
flxIDType.add(tbxIDType, flxUnderlineIDType, flxIDTypeHolder);
var flxNationality = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxNationality",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxNationality.setDefaultUnit(kony.flex.DP);
var tbxNationality = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxNationality",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineNationality = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineNationality",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineNationality.setDefaultUnit(kony.flex.DP);
flxUnderlineNationality.add();
var flxNationalityHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxNationalityHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxNationalityHolder.setDefaultUnit(kony.flex.DP);
var lblNationality = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblNationality",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": "Nationality",
"width": "80%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowNationality = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowNationality",
"isVisible": true,
"right": "92%",
"skin": "sknBackIconDisabled",
"text": "d",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNationalityHolder.add(lblNationality, lblArrowNationality);
flxNationality.add(tbxNationality, flxUnderlineNationality, flxNationalityHolder);
var flxAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAddress",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAddress.setDefaultUnit(kony.flex.DP);
var tbxAddress = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxAddress",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAddress",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAddress.setDefaultUnit(kony.flex.DP);
flxUnderlineAddress.add();
var lblAddress = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lblAddress",
"isVisible": true,
"skin": "sknLblNextDisabled",
"text": "Address",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAddress.add(tbxAddress, flxUnderlineAddress, lblAddress);
var flxNickName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxNickName",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxNickName.setDefaultUnit(kony.flex.DP);
var tbxNickName = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxNickName",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineNickName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineNickName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineNickName.setDefaultUnit(kony.flex.DP);
flxUnderlineNickName.add();
var lblNickName = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lblNickName",
"isVisible": true,
"skin": "sknLblNextDisabled",
"text": "Nick Name",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNickName.add(tbxNickName, flxUnderlineNickName, lblNickName);
var flxSpace = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxSpace",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"skin": "slFbox",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSpace.setDefaultUnit(kony.flex.DP);
flxSpace.add();
flxMain.add(flxBillerCategory, flxBillerName, flxServiceType, flxDenomination, flxBillerNumber, flxAmounttoPay, flxDueAmount, flxMinimumAmount, flxMaximumAmount, flxIssueDate, flxDueDate, flxFee, flxTotalAmount, flxRadioAccCardsSelection, flxPaymentMode, flxAmount, CopyflxAmount0ba64434a77864d, CopyflxAmount0i0a9802158c244, CopyflxAmount0f826bde07af648, flxIDType, flxNationality, flxAddress, flxNickName, flxSpace);
flxFormMain.add(flxHeader, flxMain);
var flxPopupOuter = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxPopupOuter",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "CopyslFbox0c5d1f6e07b0543",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPopupOuter.setDefaultUnit(kony.flex.DP);
var flxInnerPopup = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bounces": true,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "flxInnerPopup",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "CopyslFSbox0aabc4031db8b4f",
"verticalScrollIndicator": true,
"width": "90%",
"zIndex": 1
}, {}, {});
flxInnerPopup.setDefaultUnit(kony.flex.DP);
flxInnerPopup.add();
var segNewPopup = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50%",
"centerY": "50%",
"data": [{
"lblServiceTypePrepaid": "Denomination"
}, {
"lblServiceTypePrepaid": "Others"
}],
"groupCells": false,
"id": "segNewPopup",
"isVisible": true,
"needPageIndicator": true,
"onRowClick": AS_Segment_j53c5668c7d3470faeb19f0e4e903b74,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": tmpflxServiceType,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"lblServiceTypePrepaid": "lblServiceTypePrepaid",
"tmpflxServiceType": "tmpflxServiceType"
},
"width": "85%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": true,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_ROW_SELECT,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": true
});
flxPopupOuter.add(flxInnerPopup, segNewPopup);
var lblHiddenAmount = new kony.ui.Label({
"id": "lblHiddenAmount",
"isVisible": false,
"right": "245dp",
"skin": "slLabel",
"top": "448dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
frmNewBillKA.add(mainContainer, flxFormMain, flxPopupOuter, lblHiddenAmount);
};
function frmNewBillKAGlobalsAr() {
frmNewBillKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmNewBillKAAr,
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"enableScrolling": true,
"enabledForIdleTimeout": true,
"id": "frmNewBillKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_d8093a4ba4d745c4908d0a6853f2db03,
"skin": "sknmainGradient",
"statusBarHidden": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"bouncesZoom": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": true,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NEXTPREV,
"inTransitionConfig": {
"transitionDirection": "none",
"transitionEffect": "none"
},
"maxZoomScale": 1,
"minZoomScale": 1,
"needsIndicatorDuringPostShow": false,
"outTransitionConfig": {
"transitionDirection": "none",
"transitionEffect": "none"
},
"retainScrollPosition": false,
"statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
"titleBar": false,
"zoomScale": 1
});
};
