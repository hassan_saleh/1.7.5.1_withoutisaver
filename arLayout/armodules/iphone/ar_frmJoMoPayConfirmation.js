//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmJoMoPayConfirmationAr() {
frmJoMoPayConfirmation.setDefaultUnit(kony.flex.DP);
var flxJoMoPayHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxJoMoPayHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJoMoPayHeader.setDefaultUnit(kony.flex.DP);
var lblJoMoPay = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"id": "lblJoMoPay",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.jomopay.confirmdetails"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnBack = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
"height": "100%",
"id": "btnBack",
"isVisible": false,
"left": "0%",
"onClick": AS_Button_d3aa7357d4a044a39303ddfc911b264f,
"skin": "CopyslButtonGlossBlue0e73a02c4810645",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0%",
"width": "15%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_e7ca6d55b20948f1b01838312e8f1ff4,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
flxJoMoPayHeader.add(lblJoMoPay, btnBack, flxBack);
var flxJoMoPayDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxJoMoPayDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "sknDetails",
"top": "9%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJoMoPayDetails.setDefaultUnit(kony.flex.DP);
var btnProfilePhoto = new kony.ui.Button({
"centerX": "50%",
"centerY": "8%",
"focusSkin": "sknbtnProfileMobile",
"height": "10%",
"id": "btnProfilePhoto",
"isVisible": true,
"skin": "sknbtnProfileMobile",
"text": "F",
"width": "16%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblPhoneNo = new kony.ui.Label({
"centerX": "50%",
"id": "lblPhoneNo",
"isVisible": true,
"skin": "sknTransferType",
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxTransfer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxTransfer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTransfer.setDefaultUnit(kony.flex.DP);
var lblTransferTypeStatic = new kony.ui.Label({
"height": "51%",
"id": "lblTransferTypeStatic",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.jomopay.transfertype"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxTransferType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxTransferType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "3%",
"skin": "slFbox",
"width": "96%",
"zIndex": 1
}, {}, {});
flxTransferType.setDefaultUnit(kony.flex.DP);
var lblTransferType = new kony.ui.Label({
"id": "lblTransferType",
"isVisible": true,
"right": "2%",
"skin": "sknTransferType",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTransferType.add(lblTransferType);
flxTransfer.add(lblTransferTypeStatic, flxTransferType);
var flxAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxAccount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccount.setDefaultUnit(kony.flex.DP);
var lblAccountStaticText = new kony.ui.Label({
"height": "50%",
"id": "lblAccountStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.settings.account"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAccountType = new kony.ui.Label({
"id": "lblAccountType",
"isVisible": true,
"right": "5%",
"skin": "sknTransferType",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAccount.add(lblAccountStaticText, lblAccountType);
var flxJoMoPayType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxJoMoPayType",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJoMoPayType.setDefaultUnit(kony.flex.DP);
var flxJoMoPayTypeStaticText = new kony.ui.Label({
"height": "50%",
"id": "flxJoMoPayTypeStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.jomopay.jomopaytype"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxJoMoPay = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxJoMoPay",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "3%",
"skin": "slFbox",
"top": "2%",
"width": "96%",
"zIndex": 1
}, {}, {});
flxJoMoPay.setDefaultUnit(kony.flex.DP);
var lblType = new kony.ui.Label({
"id": "lblType",
"isVisible": true,
"right": "2%",
"skin": "sknTransferType",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxJoMoPay.add(lblType);
flxJoMoPayType.add(flxJoMoPayTypeStaticText, flxJoMoPay);
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0.00%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var lblAmountStaticText = new kony.ui.Label({
"height": "50%",
"id": "lblAmountStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAmountInside = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxAmountInside",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "95%",
"zIndex": 1
}, {}, {});
flxAmountInside.setDefaultUnit(kony.flex.DP);
var lblAmount = new kony.ui.Label({
"id": "lblAmount",
"isVisible": true,
"right": "0%",
"skin": "sknTransferType",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblStaticJOD = new kony.ui.Label({
"id": "lblStaticJOD",
"isVisible": true,
"right": "18%",
"skin": "sknTransferType",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAmountInside.add(lblAmount, lblStaticJOD);
flxAmount.add(lblAmountStaticText, flxAmountInside);
var flxFees = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxFees",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFees.setDefaultUnit(kony.flex.DP);
var lblFeesStaticText = new kony.ui.Label({
"height": "50%",
"id": "lblFeesStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.opening_account.fees"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxFeeInside = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxFeeInside",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "95%",
"zIndex": 1
}, {}, {});
flxFeeInside.setDefaultUnit(kony.flex.DP);
var lblFee = new kony.ui.Label({
"id": "lblFee",
"isVisible": true,
"right": "0%",
"skin": "sknTransferType",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblJOD = new kony.ui.Label({
"id": "lblJOD",
"isVisible": true,
"right": "18%",
"skin": "sknTransferType",
"text": "JOD",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxFeeInside.add(lblFee, lblJOD);
flxFees.add(lblFeesStaticText, flxFeeInside);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhite",
"height": "8%",
"id": "btnConfirm",
"isVisible": true,
"right": "10%",
"onClick": AS_Button_aff539101b0f496388ef9b750fbe0704,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.P2P.confirm"),
"top": "15%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
flxJoMoPayDetails.add(btnProfilePhoto, lblPhoneNo, flxTransfer, flxAccount, flxJoMoPayType, flxAmount, flxFees, btnConfirm);
var flxHiddenContents = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxHiddenContents",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "100%",
"width": "100%",
"zIndex": 5
}, {}, {});
flxHiddenContents.setDefaultUnit(kony.flex.DP);
var lblTransactionType = new kony.ui.Label({
"id": "lblTransactionType",
"isVisible": true,
"right": "0%",
"skin": "slLabel",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblDescription = new kony.ui.Label({
"id": "lblDescription",
"isVisible": true,
"right": "0%",
"skin": "slLabel",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblFromAccountNumber = new kony.ui.Label({
"id": "lblFromAccountNumber",
"isVisible": true,
"right": "0%",
"skin": "slLabel",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblSourceBranchCode = new kony.ui.Label({
"id": "lblSourceBranchCode",
"isVisible": true,
"right": "0%",
"skin": "slLabel",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblfromAccountCurrency = new kony.ui.Label({
"id": "lblfromAccountCurrency",
"isVisible": true,
"right": "0%",
"skin": "slLabel",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblJomopayType = new kony.ui.Label({
"id": "lblJomopayType",
"isVisible": true,
"right": "0%",
"skin": "slLabel",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblTransferFlag = new kony.ui.Label({
"id": "lblTransferFlag",
"isVisible": true,
"right": "0%",
"skin": "slLabel",
"text": "J",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHiddenContents.add(lblTransactionType, lblDescription, lblFromAccountNumber, lblSourceBranchCode, lblfromAccountCurrency, lblJomopayType, lblTransferFlag);
frmJoMoPayConfirmation.add(flxJoMoPayHeader, flxJoMoPayDetails, flxHiddenContents);
};
function frmJoMoPayConfirmationGlobalsAr() {
frmJoMoPayConfirmationAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmJoMoPayConfirmationAr,
"enabledForIdleTimeout": true,
"id": "frmJoMoPayConfirmation",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
