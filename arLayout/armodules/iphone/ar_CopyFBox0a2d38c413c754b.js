//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:02 EET 2020
function initializeCopyFBox0a2d38c413c754bAr() {
    CopyFBox0a2d38c413c754bAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "CopyFBox0a2d38c413c754b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox0a2d38c413c754bAr.setDefaultUnit(kony.flex.DP);
    var lblServiceList = new kony.ui.Label({
        "id": "lblServiceList",
        "isVisible": true,
        "right": "0dp",
        "minHeight": "20dp",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.locateus.frmLocatorBranchDetailsKA.CopyatmServices00ff586c7132d45"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFBox0a2d38c413c754bAr.add(lblServiceList);
}
