//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmLoginAuthSuccessKAAr() {
    frmLoginAuthSuccessKA.setDefaultUnit(kony.flex.DP);
    var flxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "28%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxSuccessIcon = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "35dp",
        "id": "flxSuccessIcon",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknsuccessIcon",
        "top": "1%",
        "width": "35dp",
        "zIndex": 1
    }, {}, {});
    flxSuccessIcon.setDefaultUnit(kony.flex.DP);
    var CopyImage031bb4461f4b644 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "50%",
        "id": "CopyImage031bb4461f4b644",
        "isVisible": true,
        "skin": "sknslImage",
        "src": "check.png",
        "width": "50%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxSuccessIcon.add(CopyImage031bb4461f4b644);
    var flxCongrats = new kony.ui.Label({
        "id": "flxCongrats",
        "isVisible": true,
        "right": "8dp",
        "skin": "sknFaceIDHeader",
        "text": "Congratulations",
        "top": "10dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDevregistraionSuccess = new kony.ui.Label({
        "centerX": "50.03%",
        "id": "lblDevregistraionSuccess",
        "isVisible": true,
        "right": "20%",
        "skin": "sknLatoRegularlbl",
        "text": "You have successfully created below login methods",
        "top": "20dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var ChoiceLabelDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "4dp",
        "id": "ChoiceLabelDivider",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "skin": "sknsegmentDivider",
        "top": "30dp",
        "width": "92%",
        "zIndex": 1
    }, {}, {});
    ChoiceLabelDivider.setDefaultUnit(kony.flex.DP);
    ChoiceLabelDivider.add();
    flxHeader.add(flxSuccessIcon, flxCongrats, lblDevregistraionSuccess, ChoiceLabelDivider);
    var flxFooter = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "62%",
        "id": "flxFooter",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFooter.setDefaultUnit(kony.flex.DP);
    var lbldesc = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbldesc",
        "isVisible": true,
        "right": "106dp",
        "skin": "sknlblLight",
        "text": "Please select which one you want as default login option",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var segLoginMethods = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "imgEnable": "check_green.png",
            "imgIcon": "touch.png",
            "lblLoginMethod": "TOUCH ID",
            "tmpAuthMode": ""
        }, {
            "imgEnable": "check_green.png",
            "imgIcon": "pin.png",
            "lblLoginMethod": "PIN",
            "tmpAuthMode": ""
        }, {
            "imgEnable": "check_green.png",
            "imgIcon": "face.png",
            "lblLoginMethod": kony.i18n.getLocalizedString("i18n.FACEID"),
            "tmpAuthMode": ""
        }, {
            "imgEnable": "check_green.png",
            "imgIcon": "password.png",
            "lblLoginMethod": "PASSWORD",
            "tmpAuthMode": ""
        }],
        "groupCells": false,
        "id": "segLoginMethods",
        "isVisible": true,
        "right": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_e9611ac415734fb09d17b871d820e764,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowTemplate": flxtmpMain,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "5%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxtmpMain": "flxtmpMain",
            "imgEnable": "imgEnable",
            "imgIcon": "imgIcon",
            "lblLoginMethod": "lblLoginMethod"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    var btnContinue = new kony.ui.Button({
        "centerX": "49.97%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "btnContinue",
        "isVisible": true,
        "onClick": AS_Button_f33c8ad1ff84466fa3ec729cfe448fe8,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.btnContinuee"),
        "top": "8%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxFooter.add(lbldesc, segLoginMethods, btnContinue);
    flxMain.add(flxHeader, flxFooter);
    frmLoginAuthSuccessKA.add(flxMain);
};
function frmLoginAuthSuccessKAGlobalsAr() {
    frmLoginAuthSuccessKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmLoginAuthSuccessKAAr,
        "bounces": false,
        "enableScrolling": true,
        "enabledForIdleTimeout": true,
        "id": "frmLoginAuthSuccessKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "pagingEnabled": false,
        "preShow": AS_Form_a2f3e90bc62941918d366894292146e6,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "transitionFade"
        },
        "retainScrollPosition": false,
        "titleBar": false
    });
};
