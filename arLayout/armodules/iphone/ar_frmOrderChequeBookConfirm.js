//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmOrderChequeBookConfirmAr() {
frmOrderChequeBookConfirm.setDefaultUnit(kony.flex.DP);
var flxnewSubAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxnewSubAccount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxnewSubAccount.setDefaultUnit(kony.flex.DP);
var lblJoMoPay = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"id": "lblJoMoPay",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnBack = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
"height": "100%",
"id": "btnBack",
"isVisible": false,
"left": "0%",
"onClick": AS_Button_d1bb6aea9ef6470fa84a3333926c4fda,
"skin": "CopyslButtonGlossBlue0e73a02c4810645",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0%",
"width": "15%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_hecccea50fb44b9b811679bfb49978c3,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
flxnewSubAccount.add(lblJoMoPay, btnBack, flxBack);
var flxAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "11%",
"id": "flxAccount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "14%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccount.setDefaultUnit(kony.flex.DP);
var lblTransferTypeStatic = new kony.ui.Label({
"height": "40%",
"id": "lblTransferTypeStatic",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.settings.account"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxTransferType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxTransferType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "3%",
"skin": "slFbox",
"width": "96%",
"zIndex": 1
}, {}, {});
flxTransferType.setDefaultUnit(kony.flex.DP);
var lblAccount = new kony.ui.Label({
"id": "lblAccount",
"isVisible": true,
"right": "2%",
"skin": "sknLblWhike125",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTransferType.add(lblAccount);
flxAccount.add(lblTransferTypeStatic, flxTransferType);
var flxBranch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "11%",
"id": "flxBranch",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "27%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBranch.setDefaultUnit(kony.flex.DP);
var lblAccountStaticText = new kony.ui.Label({
"height": "40%",
"id": "lblAccountStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.locateus.branch"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBranch = new kony.ui.Label({
"id": "lblBranch",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhike125",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBranch.add(lblAccountStaticText, lblBranch);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhite",
"height": "8%",
"id": "btnConfirm",
"isVisible": true,
"right": "10%",
"onClick": AS_Button_ef1d7687c2dc40818c312c5769ff0ea5,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.P2P.confirm"),
"top": "87%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var Label0bd39896ab0b246 = new kony.ui.Label({
"id": "Label0bd39896ab0b246",
"isVisible": true,
"right": "4%",
"skin": "sknLblWhite100",
"text": kony.i18n.getLocalizedString("i18n.accounts.ordersubjecttofees"),
"top": "72%",
"width": "92%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxAccount0ccd1462b83054a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "11%",
"id": "CopyflxAccount0ccd1462b83054a",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "40%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxAccount0ccd1462b83054a.setDefaultUnit(kony.flex.DP);
var CopylblTransferTypeStatic0bb0a6097fb3e40 = new kony.ui.Label({
"height": "40%",
"id": "CopylblTransferTypeStatic0bb0a6097fb3e40",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.ordercheck.numberofLeaves"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxTransferType0fa9d05fdfe7e4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "CopyflxTransferType0fa9d05fdfe7e4c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "3%",
"skin": "slFbox",
"width": "96%",
"zIndex": 1
}, {}, {});
CopyflxTransferType0fa9d05fdfe7e4c.setDefaultUnit(kony.flex.DP);
var lblLeaves = new kony.ui.Label({
"id": "lblLeaves",
"isVisible": true,
"right": "2%",
"skin": "sknLblWhike125",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxTransferType0fa9d05fdfe7e4c.add(lblLeaves);
CopyflxAccount0ccd1462b83054a.add(CopylblTransferTypeStatic0bb0a6097fb3e40, CopyflxTransferType0fa9d05fdfe7e4c);
var CopyflxBranch0d4e114b0c2a741 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "11%",
"id": "CopyflxBranch0d4e114b0c2a741",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "53%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxBranch0d4e114b0c2a741.setDefaultUnit(kony.flex.DP);
var CopylblAccountStaticText0b80c8a938e0945 = new kony.ui.Label({
"height": "40%",
"id": "CopylblAccountStaticText0b80c8a938e0945",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.ordercheck.numberofbook"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBook = new kony.ui.Label({
"id": "lblBook",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhike125",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxBranch0d4e114b0c2a741.add(CopylblAccountStaticText0b80c8a938e0945, lblBook);
var lblBranchCode = new kony.ui.Label({
"id": "lblBranchCode",
"isVisible": false,
"right": "100%",
"skin": "slLabel",
"text": "abel",
"top": "100%",
"width": "1px",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAccountNumber = new kony.ui.Label({
"id": "lblAccountNumber",
"isVisible": false,
"right": "110%",
"skin": "slLabel",
"text": "abel",
"top": "110%",
"width": "1px",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
frmOrderChequeBookConfirm.add(flxnewSubAccount, flxAccount, flxBranch, btnConfirm, Label0bd39896ab0b246, CopyflxAccount0ccd1462b83054a, CopyflxBranch0d4e114b0c2a741, lblBranchCode, lblAccountNumber);
};
function frmOrderChequeBookConfirmGlobalsAr() {
frmOrderChequeBookConfirmAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmOrderChequeBookConfirmAr,
"enabledForIdleTimeout": true,
"id": "frmOrderChequeBookConfirm",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
