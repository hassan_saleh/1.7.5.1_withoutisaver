//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmPrePaidPayeeDetailsKAAr() {
frmPrePaidPayeeDetailsKA.setDefaultUnit(kony.flex.DP);
var flxMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxTop = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxTop",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTop.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "70%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_c2b5e98f80b74d6e8847ae9b1d24c416,
"skin": "slFbox",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0%",
"skin": "sknBackIcon",
"text": "j",
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0%",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBack);
var Label0ae33771e752947 = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "70%",
"id": "Label0ae33771e752947",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.transfer.billDetails"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblNext = new kony.ui.Label({
"centerY": "50%",
"height": "70%",
"id": "lblNext",
"isVisible": true,
"left": "86%",
"onTouchEnd": AS_Label_febecbb6be924a1db9a9a91e6e58b1e0,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTop.add(flxBack, Label0ae33771e752947, lblNext);
flxHeader.add(flxTop);
var flxNewBill = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"centerX": "50%",
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxNewBill",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxNewBill.setDefaultUnit(kony.flex.DP);
var flxBillerName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "12%",
"id": "flxBillerName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0.00%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBillerName.setDefaultUnit(kony.flex.DP);
var lblBillerName = new kony.ui.Label({
"height": "40%",
"id": "lblBillerName",
"isVisible": true,
"right": "0%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.billsPay.billerName"),
"top": "4%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBorderBillerName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderBillerName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBorderBillerName.setDefaultUnit(kony.flex.DP);
flxBorderBillerName.add();
var txtBillerName = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "txtBillerName",
"isVisible": true,
"skin": "sknLblBack",
"text": "orange",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtBillerCode = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "50%",
"id": "txtBillerCode",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"maxTextLength": 100,
"secureTextEntry": false,
"skin": "txtBox0b5a21c6c49d64b",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0.00%",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": false,
"showCloseButton": false,
"showProgressIndicator": false,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxBillerName.add(lblBillerName, flxBorderBillerName, txtBillerName, txtBillerCode);
var flxServiceType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "12%",
"id": "flxServiceType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0.00%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxServiceType.setDefaultUnit(kony.flex.DP);
var lbServiceType = new kony.ui.Label({
"height": "40%",
"id": "lbServiceType",
"isVisible": true,
"right": "0%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
"top": "4%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBorderServiceType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderServiceType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBorderServiceType.setDefaultUnit(kony.flex.DP);
flxBorderServiceType.add();
var txtServiceType = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "txtServiceType",
"isVisible": true,
"skin": "sknLblBack",
"text": "Prepaid",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtServiceCode = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "50%",
"id": "txtServiceCode",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"maxTextLength": 100,
"secureTextEntry": false,
"skin": "txtBox0b5a21c6c49d64b",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0.00%",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": false,
"showCloseButton": false,
"showProgressIndicator": false,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxServiceType.add(lbServiceType, flxBorderServiceType, txtServiceType, txtServiceCode);
var flxDenomination = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "12%",
"id": "flxDenomination",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0.00%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxDenomination.setDefaultUnit(kony.flex.DP);
var lblDenomination = new kony.ui.Label({
"height": "40%",
"id": "lblDenomination",
"isVisible": true,
"right": "0%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.prepaid.denomination"),
"top": "4%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBorderDenomination = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderDenomination",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBorderDenomination.setDefaultUnit(kony.flex.DP);
flxBorderDenomination.add();
var txtDenomination = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "txtDenomination",
"isVisible": true,
"skin": "sknLblBack",
"text": "Prepaid",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtDenominationCode = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "100%",
"id": "txtDenominationCode",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"maxTextLength": 100,
"secureTextEntry": false,
"skin": "txtBox0b5a21c6c49d64b",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0.00%",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": false,
"showCloseButton": false,
"showProgressIndicator": false,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxDenomination.add(lblDenomination, flxBorderDenomination, txtDenomination, txtDenominationCode);
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "12%",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0.00%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var lblAmount = new kony.ui.Label({
"height": "40%",
"id": "lblAmount",
"isVisible": true,
"right": "0%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Amount"),
"top": "4%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBorderAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBorderAmount.setDefaultUnit(kony.flex.DP);
flxBorderAmount.add();
var txtAmount = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "txtAmount",
"isVisible": true,
"skin": "sknLblBack",
"text": "100",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAmount.add(lblAmount, flxBorderAmount, txtAmount);
var flxBillingNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "12%",
"id": "flxBillingNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBillingNumber.setDefaultUnit(kony.flex.DP);
var lblBillingNumber = new kony.ui.Label({
"height": "40%",
"id": "lblBillingNumber",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": kony.i18n.getLocalizedString("i18n.billsPay.BillerNumber"),
"top": "4%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBorderBillingNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderBillingNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBorderBillingNumber.setDefaultUnit(kony.flex.DP);
flxBorderBillingNumber.add();
var txtBillingNumber = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "txtBillingNumber",
"isVisible": true,
"skin": "sknLblBack",
"text": "0777777777",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBillingNumber.add(lblBillingNumber, flxBorderBillingNumber, txtBillingNumber);
var flxRadioAccCardsSelection = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": false,
"height": "12%",
"id": "flxRadioAccCardsSelection",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "1%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxRadioAccCardsSelection.setDefaultUnit(kony.flex.DP);
var btnBillsPayAccounts = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknRDOWhiteBRDBOJFont",
"id": "btnBillsPayAccounts",
"isVisible": true,
"right": "5%",
"onClick": AS_Button_c75558c8b42447bd8d969594b7280af5,
"skin": "sknRDOWhiteBRDBOJFont",
"text": "t",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblBillsPayAccounts = new kony.ui.Label({
"centerY": "50%",
"id": "lblBillsPayAccounts",
"isVisible": true,
"right": "15%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnBillsPayCards = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknRDOWhiteBRDBOJFont",
"id": "btnBillsPayCards",
"isVisible": false,
"right": "55%",
"onClick": AS_Button_a8c331a07fd145f398ebd6eba855c2a6,
"skin": "sknRDOWhiteBRDBOJFont",
"text": "s",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblBillsPayCards = new kony.ui.Label({
"centerY": "50%",
"id": "lblBillsPayCards",
"isVisible": false,
"right": "65%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxRadioAccCardsSelection.add(btnBillsPayAccounts, lblBillsPayAccounts, btnBillsPayCards, lblBillsPayCards);
var flxPaymentMode = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "10%",
"id": "flxPaymentMode",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"left": 0,
"skin": "slFbox",
"top": "0.00%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxPaymentMode.setDefaultUnit(kony.flex.DP);
var tbxPaymentMode = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxPaymentMode",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlinePaymentMode = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "flxUnderlinePaymentMode",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlinePaymentMode.setDefaultUnit(kony.flex.DP);
flxUnderlinePaymentMode.add();
var flxPaymentModeTypeHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxPaymentModeTypeHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_b51267f0fe5b47cc9fd9159bc2a1b4d8,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxPaymentModeTypeHolder.setDefaultUnit(kony.flex.DP);
var lblPaymentMode = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblPaymentMode",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowPaymentMode = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowPaymentMode",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxPaymentModeTypeHolder.add(lblPaymentMode, lblArrowPaymentMode);
flxPaymentMode.add(tbxPaymentMode, flxUnderlinePaymentMode, flxPaymentModeTypeHolder);
var flxConversionAmt = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "5%",
"id": "flxConversionAmt",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConversionAmt.setDefaultUnit(kony.flex.DP);
var lblVal = new kony.ui.Label({
"id": "lblVal",
"isVisible": true,
"right": "20dp",
"skin": "sknLblCurr",
"text": "0.000 JOD",
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblFromCurr = new kony.ui.Label({
"id": "lblFromCurr",
"isVisible": true,
"right": "55%",
"skin": "sknLblCurr",
"text": "1 JOD",
"top": "10dp",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblToCurr = new kony.ui.Label({
"id": "lblToCurr",
"isVisible": true,
"right": "73%",
"skin": "sknLblCurr",
"text": "0.746464 JOD",
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblEquals = new kony.ui.Label({
"id": "lblEquals",
"isVisible": true,
"right": "69%",
"skin": "sknLblCurr",
"text": "=",
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConversionAmt.add(lblVal, lblFromCurr, lblToCurr, lblEquals);
var flxIDType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxIDType",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxIDType.setDefaultUnit(kony.flex.DP);
var tbxIDType = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxIDType",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineIDType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineIDType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineIDType.setDefaultUnit(kony.flex.DP);
flxUnderlineIDType.add();
var flxIDTypeHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxIDTypeHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxIDTypeHolder.setDefaultUnit(kony.flex.DP);
var lblIDType = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblIDType",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": "ID Type",
"width": "80%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowIDType = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowIDType",
"isVisible": true,
"right": "92%",
"skin": "sknBackIconDisabled",
"text": "d",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIDTypeHolder.add(lblIDType, lblArrowIDType);
flxIDType.add(tbxIDType, flxUnderlineIDType, flxIDTypeHolder);
var flxNationality = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxNationality",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxNationality.setDefaultUnit(kony.flex.DP);
var tbxNationality = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxNationality",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineNationality = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineNationality",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineNationality.setDefaultUnit(kony.flex.DP);
flxUnderlineNationality.add();
var flxNationalityHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxNationalityHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxNationalityHolder.setDefaultUnit(kony.flex.DP);
var lblNationality = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblNationality",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": "Nationality",
"width": "80%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowNationality = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowNationality",
"isVisible": true,
"right": "92%",
"skin": "sknBackIconDisabled",
"text": "d",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNationalityHolder.add(lblNationality, lblArrowNationality);
flxNationality.add(tbxNationality, flxUnderlineNationality, flxNationalityHolder);
var flxAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAddress",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAddress.setDefaultUnit(kony.flex.DP);
var tbxAddress = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxAddress",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAddress",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAddress.setDefaultUnit(kony.flex.DP);
flxUnderlineAddress.add();
var lblAddress = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lblAddress",
"isVisible": true,
"skin": "sknLblNextDisabled",
"text": "Address",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAddress.add(tbxAddress, flxUnderlineAddress, lblAddress);
var flxNickName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxNickName",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxNickName.setDefaultUnit(kony.flex.DP);
var tbxNickName = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxNickName",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineNickName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineNickName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineNickName.setDefaultUnit(kony.flex.DP);
flxUnderlineNickName.add();
var lblNickName = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lblNickName",
"isVisible": true,
"skin": "sknLblNextDisabled",
"text": "Nick Name",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNickName.add(tbxNickName, flxUnderlineNickName, lblNickName);
var flxSpace = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxSpace",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"skin": "slFbox",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSpace.setDefaultUnit(kony.flex.DP);
flxSpace.add();
flxNewBill.add(flxBillerName, flxServiceType, flxDenomination, flxAmount, flxBillingNumber, flxRadioAccCardsSelection, flxPaymentMode, flxConversionAmt, flxIDType, flxNationality, flxAddress, flxNickName, flxSpace);
flxMain.add(flxHeader, flxNewBill);
var flxConfirmPopUp = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxConfirmPopUp",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmPopUp.setDefaultUnit(kony.flex.DP);
var flxConfirmHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxConfirmHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmHeader.setDefaultUnit(kony.flex.DP);
var flxHeaderBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxHeaderBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_h0320c799ce040e9a70ce70d564e9881,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxHeaderBack.setDefaultUnit(kony.flex.DP);
var lblHeaderBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblHeaderBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHeaderBack.add(lblHeaderBackIcon, lblHeaderBack);
var lblHeaderTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblHeaderTitle",
"isVisible": true,
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnClose = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknDeleteFocus",
"height": "80%",
"id": "btnClose",
"isVisible": true,
"left": "85%",
"onClick": AS_Button_g99a40b8babe4f939457b51f3ee27eb0,
"skin": "sknBtnBack",
"text": "O",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxConfirmHeader.add(flxHeaderBack, lblHeaderTitle, btnClose);
var flxImpDetail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "25%",
"id": "flxImpDetail",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxImpDetail.setDefaultUnit(kony.flex.DP);
var CopyflxIcon0a41947647db445 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50dp",
"id": "CopyflxIcon0a41947647db445",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxToIcon",
"top": "7%",
"width": "50dp",
"zIndex": 1
}, {}, {});
CopyflxIcon0a41947647db445.setDefaultUnit(kony.flex.DP);
var CopylblInitial0df20680ba88e45 = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "80%",
"id": "CopylblInitial0df20680ba88e45",
"isVisible": true,
"skin": "sknLblFromIcon",
"text": "OF",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxIcon0a41947647db445.add(CopylblInitial0df20680ba88e45);
var lblBillerNamePopup = new kony.ui.Label({
"centerX": "50%",
"id": "lblBillerNamePopup",
"isVisible": true,
"skin": "sknBeneTitle",
"text": "Orange Fixed",
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBillerNumPopup = new kony.ui.Label({
"centerX": "50%",
"id": "lblBillerNumPopup",
"isVisible": true,
"right": "0dp",
"skin": "sknLblNextDisabled",
"text": "064385497",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxImpDetail.add(CopyflxIcon0a41947647db445, lblBillerNamePopup, lblBillerNumPopup);
var flxOtherDetails = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "50%",
"horizontalScrollIndicator": true,
"id": "flxOtherDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxOtherDetails.setDefaultUnit(kony.flex.DP);
var flxBillerCategoryPopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxBillerCategoryPopup",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBillerCategoryPopup.setDefaultUnit(kony.flex.DP);
var lblConfirmBillerCategoryTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmBillerCategoryTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.BillerCategory"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmBillerCategory = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmBillerCategory",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "TDFfXYD",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBillerCategoryPopup.add(lblConfirmBillerCategoryTitle, lblConfirmBillerCategory);
var flxServiceTypePopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxServiceTypePopup",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxServiceTypePopup.setDefaultUnit(kony.flex.DP);
var lblConfirmServiceTypeTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmServiceTypeTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmServiceType = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmServiceType",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Fixed",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxServiceTypePopup.add(lblConfirmServiceTypeTitle, lblConfirmServiceType);
var flxIDTypePopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxIDTypePopup",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIDTypePopup.setDefaultUnit(kony.flex.DP);
var lblConfirmIDTypeTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmIDTypeTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "ID Type",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmIDType = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmIDType",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "National ID",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIDTypePopup.add(lblConfirmIDTypeTitle, lblConfirmIDType);
var flxIDNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxIDNumber",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIDNumber.setDefaultUnit(kony.flex.DP);
var lblConfirmIDNumberTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmIDNumberTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "ID Number",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmIDNumber = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmIDNumber",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "38129048293",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIDNumber.add(lblConfirmIDNumberTitle, lblConfirmIDNumber);
var flxNationalityPopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxNationalityPopup",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNationalityPopup.setDefaultUnit(kony.flex.DP);
var lblConfirmNationalityTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmNationalityTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Nationality",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmNationality = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmNationality",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Jordan",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNationalityPopup.add(lblConfirmNationalityTitle, lblConfirmNationality);
var flxAddressPopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxAddressPopup",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAddressPopup.setDefaultUnit(kony.flex.DP);
var lblConfirmAddressTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmAddressTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Address",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmAddress = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmAddress",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Amman - Haidar Abad",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAddressPopup.add(lblConfirmAddressTitle, lblConfirmAddress);
var flxNickNamePopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxNickNamePopup",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNickNamePopup.setDefaultUnit(kony.flex.DP);
var lblConfirmNickNameTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmNickNameTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmNickName = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmNickName",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Some random nick name",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNickNamePopup.add(lblConfirmNickNameTitle, lblConfirmNickName);
var flxAmountConf = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxAmountConf",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmountConf.setDefaultUnit(kony.flex.DP);
var lblAmountTitle = new kony.ui.Label({
"height": "50%",
"id": "lblAmountTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Amount"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAmountConf = new kony.ui.Label({
"height": "50%",
"id": "lblAmountConf",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAmountConf.add(lblAmountTitle, lblAmountConf);
var flxDenominationConf = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxDenominationConf",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDenominationConf.setDefaultUnit(kony.flex.DP);
var lblDenominationTitle = new kony.ui.Label({
"height": "50%",
"id": "lblDenominationTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.prepaid.denomination"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblDenominationConf = new kony.ui.Label({
"height": "50%",
"id": "lblDenominationConf",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Some random nick name",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxDenominationConf.add(lblDenominationTitle, lblDenominationConf);
flxOtherDetails.add(flxBillerCategoryPopup, flxServiceTypePopup, flxIDTypePopup, flxIDNumber, flxNationalityPopup, flxAddressPopup, flxNickNamePopup, flxAmountConf, flxDenominationConf);
var flxButtonHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18%",
"id": "flxButtonHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxButtonHolder.setDefaultUnit(kony.flex.DP);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "50dp",
"id": "btnConfirm",
"isVisible": true,
"onClick": AS_Button_i977a60e37a44a389baa53ed69138b9a,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxButtonHolder.add(btnConfirm);
var lblLanguage = new kony.ui.Label({
"id": "lblLanguage",
"isVisible": false,
"right": "-140dp",
"skin": "slLabel",
"text": "eng",
"top": "-700dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblType0da3fef3109644f = new kony.ui.Label({
"id": "CopylblType0da3fef3109644f",
"isVisible": false,
"right": "-130dp",
"skin": "slLabel",
"top": "-690dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmPopUp.add(flxConfirmHeader, flxImpDetail, flxOtherDetails, flxButtonHolder, lblLanguage, CopylblType0da3fef3109644f);
frmPrePaidPayeeDetailsKA.add(flxMain, flxConfirmPopUp);
};
function frmPrePaidPayeeDetailsKAGlobalsAr() {
frmPrePaidPayeeDetailsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmPrePaidPayeeDetailsKAAr,
"bounces": false,
"enabledForIdleTimeout": false,
"id": "frmPrePaidPayeeDetailsKA",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": true,
"skin": "slFormCommon",
"verticalScrollIndicator": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
