//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function initializetmpsegSIListAr() {
    flxtmpSIListAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxtmpSIList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxtmpSIListAr.setDefaultUnit(kony.flex.DP);
    var flxUserDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxUserDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "18%",
        "skin": "slFbox",
        "top": "0%",
        "width": "42%",
        "zIndex": 1
    }, {}, {});
    flxUserDetails.setDefaultUnit(kony.flex.DP);
    var lblTransactiondate = new kony.ui.Label({
        "centerY": "40%",
        "id": "lblTransactiondate",
        "isVisible": false,
        "right": "5%",
        "skin": "sknSIDate",
        "text": "10 Feb 2010",
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTransactionDesc = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblTransactionDesc",
        "isVisible": true,
        "right": "5%",
        "skin": "sknSIDesc",
        "width": "100%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxUserDetails.add(lblTransactiondate, lblTransactionDesc);
    var flxTAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxTAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "60%",
        "skin": "slFbox",
        "top": "0%",
        "width": "30%",
        "zIndex": 1
    }, {}, {});
    flxTAmount.setDefaultUnit(kony.flex.DP);
    var lblAmount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAmount",
        "isVisible": true,
        "left": "5%",
        "skin": "sknSIMoney",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxTAmount.add(lblAmount);
    var CopyflxTAmount0bc17ffab355945 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "CopyflxTAmount0bc17ffab355945",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "90%",
        "skin": "slFbox",
        "top": "0%",
        "width": "10%",
        "zIndex": 1
    }, {}, {});
    CopyflxTAmount0bc17ffab355945.setDefaultUnit(kony.flex.DP);
    var CopylblAmount0a2751be25d5944 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblAmount0a2751be25d5944",
        "isVisible": true,
        "left": "40%",
        "skin": "sknArrowSI",
        "text": "o",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyflxTAmount0bc17ffab355945.add(CopylblAmount0a2751be25d5944);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "3%",
        "skin": "sknFlxRoundContainer",
        "top": "15%",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxIcon1.add(lblInitial);
    flxtmpSIListAr.add(flxUserDetails, flxTAmount, CopyflxTAmount0bc17ffab355945, flxIcon1);
}
