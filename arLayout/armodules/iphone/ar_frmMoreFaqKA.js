//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmMoreFaqKAAr() {
    frmMoreFaqKA.setDefaultUnit(kony.flex.DP);
    var iosTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "iosTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    iosTitleBar.setDefaultUnit(kony.flex.DP);
    var interestRatesTitleLabel = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "interestRatesTitleLabel",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.more.faqS"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var backButton = new kony.ui.Button({
        "focusSkin": "sknleftBackButtonFocus",
        "height": "50dp",
        "id": "backButton",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_2fbe29d1bab74463a082f311e2807da0,
        "skin": "sknleftBackButtonNormal",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "9%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    iosTitleBar.add(interestRatesTitleLabel, backButton, lblBack);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": 0,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkgGray",
        "top": "50dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var rTxtPrivacyPolicyKA = new kony.ui.RichText({
        "id": "rTxtPrivacyPolicyKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknRichTxt100LR30363fKA",
        "text": "<br><b>111What is bill payment?</b></br>\n\n<br>Etiam nec pulvinar dui, eget eleifend felis. Proin bibendum molestie dolor. Aenean dictum pharetra mauris, ultrices pretium nunc imperdiet in.</br>\n\n<b><br>What is remote deposit capture?</br></b>\n\n<br>Nulla dictum tincidunt turpis eu consequat. Sed adipiscing eros a nisi dictum mollis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus urna lorem, porta sed congue ut, sollicitudin quis erat.</br>\n\n<b><br>What is pay a person?</br></b>\n\n<br>Vivamus massa odio, dignissim ac ante at, euismod dignissim risus. Cras ut blandit lorem. Maecenas nisl quam, cursus nec aliquet facilisis, mattis id augue. Fusce semper odio et gravida interdum. Suspendisse quis lacus nulla. Nullam et nibh ligula. Nunc vitae nulla et arcu mollis iaculis. Vestibulum venenatis risus ut ligula lacinia malesuada.</br>",
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 2, 2,5, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var txtFaqKA = new kony.ui.TextArea2({
        "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
        "height": "120dp",
        "id": "txtFaqKA",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
        "right": "37dp",
        "numberOfVisibleLines": 3,
        "skin": "slTextArea",
        "text": "TextArea2",
        "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 2, 2,2, 2],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTAREA_KEYBOARD_LABEL_DONE,
        "showCloseButton": true,
        "showProgressIndicator": false
    });
    mainContent.add(rTxtPrivacyPolicyKA, txtFaqKA);
    var flxFAQ = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "90%",
        "horizontalScrollIndicator": true,
        "id": "flxFAQ",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "10%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFAQ.setDefaultUnit(kony.flex.DP);
    flxFAQ.add();
    frmMoreFaqKA.add(iosTitleBar, mainContent, flxFAQ);
};
function frmMoreFaqKAGlobalsAr() {
    frmMoreFaqKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmMoreFaqKAAr,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmMoreFaqKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_b808d122a32948a9b3437b73c6c91cdc,
        "skin": "sknmainGradient",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": true,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false
    });
};
