//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmScheduledTransactionDetailsKAAr() {
    frmScheduledTransactionDetailsKA.setDefaultUnit(kony.flex.DP);
    var titleBarWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknaccountTypeChecking",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarWrapper.setDefaultUnit(kony.flex.DP);
    var iosTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "iosTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    iosTitleBar.setDefaultUnit(kony.flex.DP);
    var transferPayTitleLabel = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "22dp",
        "id": "transferPayTitleLabel",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.transfer.scheduledTransactionDetails"),
        "width": "72%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var backButton = new kony.ui.Button({
        "focusSkin": "sknleftBackButtonFocus",
        "height": "50dp",
        "id": "backButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_1b36024ad54e44e88fc88d9e54cb1670,
        "skin": "sknleftBackButtonNormal",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    iosTitleBar.add(transferPayTitleLabel, backButton);
    var editScheduledTransactionButton = new kony.ui.Button({
        "focusSkin": "skntitleBarTextButtonFocus",
        "height": "50dp",
        "id": "editScheduledTransactionButton",
        "isVisible": true,
        "minWidth": "50dp",
        "onClick": AS_Button_d8da91e7966647bd95708b797ecfc5c7,
        "left": "0dp",
        "skin": "skntitleBarTextButton",
        "text": kony.i18n.getLocalizedString("i18n.common.edit"),
        "top": "0dp",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    titleBarWrapper.add(iosTitleBar, editScheduledTransactionButton);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "50dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var FlexContainer042b0725667e643 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "145dp",
        "id": "FlexContainer042b0725667e643",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer042b0725667e643.setDefaultUnit(kony.flex.DP);
    var FlexContainer00bb80511034544 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "FlexContainer00bb80511034544",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    FlexContainer00bb80511034544.setDefaultUnit(kony.flex.DP);
    var transactionAmount = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionAmount",
        "isVisible": true,
        "skin": "skndetailPageNumber",
        "text": kony.i18n.getLocalizedString("i18n.transfer.frmScheduledTransactionDetailsKA.transactionAmount"),
        "top": "30dp",
        "width": "80%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var transactionName = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionName",
        "isVisible": true,
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.transfer.frmScheduledTransactionDetailsKA.transactionName"),
        "top": "5dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var transactionDate = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionDate",
        "isVisible": true,
        "skin": "skndetailPageDate",
        "text": kony.i18n.getLocalizedString("i18n.transfer.frmScheduledTransactionDetailsKA.transactionDate"),
        "top": "3dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    FlexContainer00bb80511034544.add(transactionAmount, transactionName, transactionDate);
    var divider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    divider.setDefaultUnit(kony.flex.DP);
    divider.add();
    FlexContainer042b0725667e643.add(FlexContainer00bb80511034544, divider);
    var additionalDetails1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "additionalDetails1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    additionalDetails1.setDefaultUnit(kony.flex.DP);
    var Label0e7a26666ebf141 = new kony.ui.Label({
        "id": "Label0e7a26666ebf141",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.common.fromc"),
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var transactionFrom = new kony.ui.Label({
        "id": "transactionFrom",
        "isVisible": true,
        "right": "5%",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.transfer.frmScheduledTransactionDetailsKA.transactionFrom"),
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var divider2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    divider2.setDefaultUnit(kony.flex.DP);
    divider2.add();
    additionalDetails1.add(Label0e7a26666ebf141, transactionFrom, divider2);
    var CopynotesWrapper03de47f39b8bc46 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "CopynotesWrapper03de47f39b8bc46",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopynotesWrapper03de47f39b8bc46.setDefaultUnit(kony.flex.DP);
    var CopyLabel07f3ee643ff4e47 = new kony.ui.Label({
        "id": "CopyLabel07f3ee643ff4e47",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.common.notesc"),
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var transactionNotes = new kony.ui.Label({
        "id": "transactionNotes",
        "isVisible": true,
        "right": "5%",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.common.transferToSavings"),
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var Copydivider06e184b7d586e4a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider06e184b7d586e4a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider06e184b7d586e4a.setDefaultUnit(kony.flex.DP);
    Copydivider06e184b7d586e4a.add();
    CopynotesWrapper03de47f39b8bc46.add(CopyLabel07f3ee643ff4e47, transactionNotes, Copydivider06e184b7d586e4a);
    var repeatTransactionContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "repeatTransactionContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    repeatTransactionContainer.setDefaultUnit(kony.flex.DP);
    var deleteScheduleTransactionButton = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "60dp",
        "id": "deleteScheduleTransactionButton",
        "isVisible": true,
        "onClick": AS_Button_12e95983c2b04e12bb9d0bf9d4011bd8,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.deleteScheduledTransaction"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var Copydivider0b2398672408341 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0b2398672408341",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    Copydivider0b2398672408341.setDefaultUnit(kony.flex.DP);
    Copydivider0b2398672408341.add();
    repeatTransactionContainer.add(deleteScheduleTransactionButton, Copydivider0b2398672408341);
    mainContent.add(FlexContainer042b0725667e643, additionalDetails1, CopynotesWrapper03de47f39b8bc46, repeatTransactionContainer);
    frmScheduledTransactionDetailsKA.add(titleBarWrapper, mainContent);
};
function frmScheduledTransactionDetailsKAGlobalsAr() {
    frmScheduledTransactionDetailsKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmScheduledTransactionDetailsKAAr,
        "allowHorizontalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmScheduledTransactionDetailsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_526575c89f234285b5dd15864f20dcf2,
        "skin": "sknmainGradient",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false
    });
};
