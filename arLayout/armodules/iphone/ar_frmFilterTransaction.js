//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmFilterTransactionAr() {
frmFilterTransaction.setDefaultUnit(kony.flex.DP);
var flxFilterTransactionHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxFilterTransactionHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFilterTransactionHeader.setDefaultUnit(kony.flex.DP);
var flxFilterBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxFilterBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_dfbacc341e6a472a86e64ea72fa7408a,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxFilterBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxFilterBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
var lblFilterHeader = new kony.ui.Label({
"centerX": "50%",
"height": "90%",
"id": "lblFilterHeader",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxFilterDone = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxFilterDone",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_h58f754d30154631ad25eb2f6379bb6f,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxFilterDone.setDefaultUnit(kony.flex.DP);
var Label0d6e6be1b179e44 = new kony.ui.Label({
"centerX": "49%",
"centerY": "50%",
"id": "Label0d6e6be1b179e44",
"isVisible": true,
"skin": "sknCarioSemiBold130GreenYellow",
"text": kony.i18n.getLocalizedString("i18n.overview.buttonDone"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [2, 1, 2, 1],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxFilterDone.add(Label0d6e6be1b179e44);
flxFilterTransactionHeader.add(flxFilterBack, lblFilterHeader, flxFilterDone);
var flxFilterTransaction = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxFilterTransaction",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "9%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFilterTransaction.setDefaultUnit(kony.flex.DP);
var Label0ff51e386d98042 = new kony.ui.Label({
"id": "Label0ff51e386d98042",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.timeperiod"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxtimeperiod = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "7%",
"id": "flxtimeperiod",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"onClick": AS_FlexContainer_g789ccbfa863445e9de0c2ba654826bb,
"skin": "slFbox",
"top": "10%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxtimeperiod.setDefaultUnit(kony.flex.DP);
var Image0i195c328eee24b = new kony.ui.Image2({
"height": "70%",
"id": "Image0i195c328eee24b",
"isVisible": true,
"right": "5%",
"skin": "slImage",
"src": "cal.png",
"top": "15%",
"width": "8%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblDateRange = new kony.ui.Label({
"height": "70%",
"id": "lblDateRange",
"isVisible": true,
"right": "15%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.pickadate"),
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var Image0e7683ea123f546 = new kony.ui.Image2({
"height": "60%",
"id": "Image0e7683ea123f546",
"isVisible": true,
"left": "5%",
"skin": "slImage",
"src": "map_drilldown.png",
"top": "20%",
"width": "8%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var FlexContainer0b96ba0e3f06e4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "1.50%",
"id": "FlexContainer0b96ba0e3f06e4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0b96ba0e3f06e4a.setDefaultUnit(kony.flex.DP);
FlexContainer0b96ba0e3f06e4a.add();
flxtimeperiod.add(Image0i195c328eee24b, lblDateRange, Image0e7683ea123f546, FlexContainer0b96ba0e3f06e4a);
var lblAmountRange = new kony.ui.Label({
"id": "lblAmountRange",
"isVisible": true,
"right": "5.00%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.search.amountRange"),
"top": "20.04%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var FlexContainer0b0328c9555bb43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "FlexContainer0b0328c9555bb43",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "25%",
"width": "90%",
"zIndex": 1
}, {}, {});
FlexContainer0b0328c9555bb43.setDefaultUnit(kony.flex.DP);
var txtAmountRangeFrom = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "sknCarioRegular125OPc50",
"height": "60%",
"id": "txtAmountRangeFrom",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "5%",
"onDone": AS_TextField_jc1ad7d3ef9c4040a0adfcf21d63bc5e,
"onTextChange": AS_TextField_f2e65402127f4df890f4e326fc5f84fc,
"placeholder": "0.000",
"secureTextEntry": false,
"skin": "sknCarioRegular125OPc50",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "30%",
"width": "25%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var FlexContainer0cf44c25bf9c24e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "63%",
"clipBounds": true,
"height": "2%",
"id": "FlexContainer0cf44c25bf9c24e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "35%",
"skin": "skntextFieldDivider",
"width": "8%",
"zIndex": 1
}, {}, {});
FlexContainer0cf44c25bf9c24e.setDefaultUnit(kony.flex.DP);
FlexContainer0cf44c25bf9c24e.add();
var txtAmountRangeTo = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "sknCarioRegular125OPc50",
"height": "60%",
"id": "txtAmountRangeTo",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "50%",
"onDone": AS_TextField_fc8cf52d3b9c45b98b76d05ddc5bbb91,
"onTextChange": AS_TextField_cf222c5878b444aa885df2bc71375aee,
"placeholder": "0.000",
"secureTextEntry": false,
"skin": "sknCarioRegular125OPc50",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "30%",
"width": "25%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblCurrency = new kony.ui.Label({
"centerY": "60%",
"id": "lblCurrency",
"isVisible": true,
"right": "82%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.common.currencySymbol"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAmountMinLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "7%",
"clipBounds": true,
"height": "2%",
"id": "flxAmountMinLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "skntextFieldDivider",
"width": "28%",
"zIndex": 1
}, {}, {});
flxAmountMinLine.setDefaultUnit(kony.flex.DP);
flxAmountMinLine.add();
var flxAmountMaxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "7%",
"clipBounds": true,
"height": "2%",
"id": "flxAmountMaxLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "48%",
"skin": "skntextFieldDivider",
"width": "28%",
"zIndex": 1
}, {}, {});
flxAmountMaxLine.setDefaultUnit(kony.flex.DP);
flxAmountMaxLine.add();
FlexContainer0b0328c9555bb43.add(txtAmountRangeFrom, FlexContainer0cf44c25bf9c24e, txtAmountRangeTo, lblCurrency, flxAmountMinLine, flxAmountMaxLine);
var CopyLabel0ge4b0af66b9b44 = new kony.ui.Label({
"id": "CopyLabel0ge4b0af66b9b44",
"isVisible": true,
"right": "5.00%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.transactiontype"),
"top": "38%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxIncomming = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "flxIncomming",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "45%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIncomming.setDefaultUnit(kony.flex.DP);
var Label0c3cfdb47936d4d = new kony.ui.Label({
"centerY": "50%",
"id": "Label0c3cfdb47936d4d",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.incoming"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblIncommingRing = new kony.ui.Label({
"centerY": "50%",
"id": "lblIncommingRing",
"isVisible": true,
"onTouchEnd": AS_Label_i58ea91b1b4c4554aa1aeadbf8ec8b28,
"left": "7%",
"skin": "sknBOJttf150GreenYellow",
"text": "s",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblIncommingTick = new kony.ui.Label({
"centerY": "50%",
"id": "lblIncommingTick",
"isVisible": false,
"left": "8%",
"skin": "sknBOJttf100",
"text": "r",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIncomming.add(Label0c3cfdb47936d4d, lblIncommingRing, lblIncommingTick);
var flxOutgoing = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "flxOutgoing",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "53.10%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOutgoing.setDefaultUnit(kony.flex.DP);
var CopyLabel0e006575ff8644a = new kony.ui.Label({
"centerY": "50%",
"id": "CopyLabel0e006575ff8644a",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.outgoing"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblOutgoingRing = new kony.ui.Label({
"centerY": "50.00%",
"id": "lblOutgoingRing",
"isVisible": true,
"onTouchEnd": AS_Label_iad886b5b24b42129dd9163926c8aec0,
"left": "7.03%",
"skin": "sknBOJttf150GreenYellow",
"text": "s",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblOutGoingTick = new kony.ui.Label({
"centerY": "50%",
"id": "lblOutGoingTick",
"isVisible": false,
"left": "8%",
"skin": "sknBOJttf100",
"text": "r",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxOutgoing.add(CopyLabel0e006575ff8644a, lblOutgoingRing, lblOutGoingTick);
var flxCard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "flxCard",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "61.20%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCard.setDefaultUnit(kony.flex.DP);
var CopyLabel0g79eec6f68dd4b = new kony.ui.Label({
"centerY": "50%",
"id": "CopyLabel0g79eec6f68dd4b",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.cards.Card"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCardRing = new kony.ui.Label({
"centerY": "50%",
"id": "lblCardRing",
"isVisible": true,
"onTouchEnd": AS_Label_j102319a5c2d4fbfa96be5a075caefd7,
"left": "7%",
"skin": "sknBOJttf150GreenYellow",
"text": "s",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCardTick = new kony.ui.Label({
"centerY": "50%",
"id": "lblCardTick",
"isVisible": false,
"left": "8%",
"skin": "sknBOJttf100",
"text": "r",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCard.add(CopyLabel0g79eec6f68dd4b, lblCardRing, lblCardTick);
flxFilterTransaction.add(Label0ff51e386d98042, flxtimeperiod, lblAmountRange, FlexContainer0b0328c9555bb43, CopyLabel0ge4b0af66b9b44, flxIncomming, flxOutgoing, flxCard);
var flxDatePicker = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxDatePicker",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0.00%",
"skin": "slFbox",
"top": "9%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxDatePicker.setDefaultUnit(kony.flex.DP);
var CopyLabel0ba8b18300a1c45 = new kony.ui.Label({
"id": "CopyLabel0ba8b18300a1c45",
"isVisible": true,
"right": "5.00%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.daterange"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyFlexContainer0b07914c0869044 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "CopyFlexContainer0b07914c0869044",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_c93343ea6dbd47a8bac98774f099dd62,
"skin": "flxsegBg",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0b07914c0869044.setDefaultUnit(kony.flex.DP);
var lblLast30daystext = new kony.ui.Label({
"centerY": "50%",
"id": "lblLast30daystext",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.last30days"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLast30days = new kony.ui.Label({
"centerY": "50%",
"id": "lblLast30days",
"isVisible": false,
"left": "8%",
"skin": "sknBOJttf100",
"text": "r",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0b07914c0869044.add(lblLast30daystext, lblLast30days);
var CopyFlexContainer0b17fc240a90540 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "CopyFlexContainer0b17fc240a90540",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_d3b6dca0e8b847618115df872d8ef8cb,
"skin": "flxsegBg",
"top": "0.10%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0b17fc240a90540.setDefaultUnit(kony.flex.DP);
var lblCurrentMonth = new kony.ui.Label({
"centerY": "50%",
"id": "lblCurrentMonth",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblThisMonth = new kony.ui.Label({
"centerY": "50%",
"id": "lblThisMonth",
"isVisible": false,
"left": "8%",
"skin": "sknBOJttf100",
"text": "r",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0b17fc240a90540.add(lblCurrentMonth, lblThisMonth);
var CopyFlexContainer0j63b912471fe4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "CopyFlexContainer0j63b912471fe4f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_g0840b75a528464b974aaf017d989855,
"skin": "flxsegBg",
"top": "0.10%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0j63b912471fe4f.setDefaultUnit(kony.flex.DP);
var lblLast90daystext = new kony.ui.Label({
"centerY": "50%",
"id": "lblLast90daystext",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.last60days"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLast90days = new kony.ui.Label({
"centerY": "50%",
"id": "lblLast90days",
"isVisible": false,
"left": "8%",
"skin": "sknBOJttf100",
"text": "r",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0j63b912471fe4f.add(lblLast90daystext, lblLast90days);
var CopyFlexContainer0c22eb7f7721d41 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "CopyFlexContainer0c22eb7f7721d41",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_ad50464dfb8f4bd486fe5d20801ba7e5,
"skin": "flxsegBg",
"top": "0.10%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0c22eb7f7721d41.setDefaultUnit(kony.flex.DP);
var lblYear = new kony.ui.Label({
"centerY": "50%",
"id": "lblYear",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCurrentYear = new kony.ui.Label({
"centerY": "50%",
"id": "lblCurrentYear",
"isVisible": false,
"left": "8%",
"skin": "sknBOJttf100",
"text": "r",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0c22eb7f7721d41.add(lblYear, lblCurrentYear);
var CopyFlexContainer0i25ca93dcafe43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "CopyFlexContainer0i25ca93dcafe43",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_bda9f2a0ed074300a73ad38655a8d170,
"skin": "flxsegBg",
"top": "0.10%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0i25ca93dcafe43.setDefaultUnit(kony.flex.DP);
var lblOthers = new kony.ui.Label({
"centerY": "50%",
"id": "lblOthers",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.Others"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblOthersDate = new kony.ui.Label({
"centerY": "50%",
"id": "lblOthersDate",
"isVisible": false,
"left": "8%",
"skin": "sknBOJttf100",
"text": "r",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0i25ca93dcafe43.add(lblOthers, lblOthersDate);
var lblSetCustomDateRange = new kony.ui.Label({
"id": "lblSetCustomDateRange",
"isVisible": false,
"right": "5.00%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.setcustomdaterange"),
"top": "4%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyFlexContainer0f90c1916338042 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "CopyFlexContainer0f90c1916338042",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0f90c1916338042.setDefaultUnit(kony.flex.DP);
var lblDateFrom = new kony.ui.Label({
"height": "60%",
"id": "lblDateFrom",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"top": "30%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyFlexContainer0d80598fcd6d44c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "72%",
"clipBounds": true,
"height": "2%",
"id": "CopyFlexContainer0d80598fcd6d44c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "43%",
"skin": "skntextFieldDivider",
"width": "5%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0d80598fcd6d44c.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0d80598fcd6d44c.add();
var CopyFlexContainer0g5b3a6c46c784b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1%",
"clipBounds": true,
"height": "2%",
"id": "CopyFlexContainer0g5b3a6c46c784b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "skntextFieldDivider",
"width": "35%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0g5b3a6c46c784b.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0g5b3a6c46c784b.add();
var CopyFlexContainer0a61f8d99e19649 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1%",
"clipBounds": true,
"height": "2%",
"id": "CopyFlexContainer0a61f8d99e19649",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "50%",
"skin": "skntextFieldDivider",
"width": "35%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0a61f8d99e19649.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0a61f8d99e19649.add();
var calDatefrom = new kony.ui.Calendar({
"calendarIcon": "tran.png",
"dateFormat": "MM/dd/yyyy",
"focusSkin": "sknCarioRegular",
"height": "60%",
"id": "calDatefrom",
"isVisible": true,
"right": "5%",
"onSelection": AS_Calendar_h0bb159340504e548b3715dc9e0eae02,
"skin": "sknCarioRegular",
"top": "35%",
"validStartDate": [7, 2, 2018],
"viewConfig": {
"gridConfig": {
"gridCellSkin": "Copycalender0h05b9ba7ebfb45",
"gridSkin": "Copycalender0c4c48cb6d2174e",
"leftNavigationImage": "directions_turn_left.png",
"rightNavigationImage": "directions_turn_right.png"
}
},
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "28%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"mode": constants.CALENDAR_WHEEL_ONLY_DATE
});
var calDateTo = new kony.ui.Calendar({
"calendarIcon": "tran.png",
"dateFormat": "MM/dd/yyyy",
"focusSkin": "sknCarioRegular",
"height": "60%",
"id": "calDateTo",
"isVisible": true,
"right": "52%",
"onSelection": AS_Calendar_da082b7e4bb24f558bff3e9cbf6b40c8,
"skin": "sknCarioRegular",
"top": "35%",
"validStartDate": [7, 2, 2018],
"viewConfig": {
"gridConfig": {
"gridCellSkin": "Copycalender0h05b9ba7ebfb45",
"gridSkin": "Copycalender0c4c48cb6d2174e",
"leftNavigationImage": "directions_turn_left.png",
"rightNavigationImage": "directions_turn_right.png"
}
},
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "28%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"mode": constants.CALENDAR_WHEEL_ONLY_DATE
});
var lblDateTo = new kony.ui.Label({
"height": "60%",
"id": "lblDateTo",
"isVisible": true,
"right": "52%",
"skin": "sknCarioRegular120White70Opc",
"top": "30%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0jba95dfa466447 = new kony.ui.Label({
"id": "CopyLabel0jba95dfa466447",
"isVisible": true,
"right": "5.00%",
"skin": "sknCarioRegular90Opc70",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.from"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0ccd1a4d962ec45 = new kony.ui.Label({
"id": "CopyLabel0ccd1a4d962ec45",
"isVisible": true,
"right": "52%",
"skin": "sknCarioRegular90Opc70",
"text": kony.i18n.getLocalizedString("i18n.common.ToC"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0f90c1916338042.add(lblDateFrom, CopyFlexContainer0d80598fcd6d44c, CopyFlexContainer0g5b3a6c46c784b, CopyFlexContainer0a61f8d99e19649, calDatefrom, calDateTo, lblDateTo, CopyLabel0jba95dfa466447, CopyLabel0ccd1a4d962ec45);
flxDatePicker.add(CopyLabel0ba8b18300a1c45, CopyFlexContainer0b07914c0869044, CopyFlexContainer0b17fc240a90540, CopyFlexContainer0j63b912471fe4f, CopyFlexContainer0c22eb7f7721d41, CopyFlexContainer0i25ca93dcafe43, lblSetCustomDateRange, CopyFlexContainer0f90c1916338042);
var flxCardsFilterScreen = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxCardsFilterScreen",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "9%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxCardsFilterScreen.setDefaultUnit(kony.flex.DP);
var CopyFlexContainer0c481c34c5bb540 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40%",
"id": "CopyFlexContainer0c481c34c5bb540",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0c481c34c5bb540.setDefaultUnit(kony.flex.DP);
var lblDateFromCards = new kony.ui.Label({
"height": "20%",
"id": "lblDateFromCards",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"top": "20%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyFlexContainer0c7c87912c7ac48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 1,
"clipBounds": true,
"height": "1%",
"id": "CopyFlexContainer0c7c87912c7ac48",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "skntextFieldDivider",
"top": "41%",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0c7c87912c7ac48.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0c7c87912c7ac48.add();
var CopyFlexContainer0d5ae4aa3c34e4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1%",
"id": "CopyFlexContainer0d5ae4aa3c34e4c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "skntextFieldDivider",
"top": "81%",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0d5ae4aa3c34e4c.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0d5ae4aa3c34e4c.add();
var calDateFromCards = new kony.ui.Calendar({
"calendarIcon": "cal.png",
"dateFormat": "MM/dd/yyyy",
"focusSkin": "sknCarioRegular",
"height": "15%",
"id": "calDateFromCards",
"isVisible": true,
"right": "5%",
"onSelection": AS_Calendar_i16e1beaf485409a8931149f63275a3b,
"skin": "sknCarioRegular",
"top": "25%",
"validStartDate": [7, 2, 2018],
"viewConfig": {
"gridConfig": {
"gridCellSkin": "Copycalender0h05b9ba7ebfb45",
"gridSkin": "Copycalender0c4c48cb6d2174e",
"leftNavigationImage": "directions_turn_left.png",
"rightNavigationImage": "directions_turn_right.png"
}
},
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "90%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"mode": constants.CALENDAR_WHEEL_ONLY_DATE
});
var calDateToCards = new kony.ui.Calendar({
"calendarIcon": "tran.png",
"dateFormat": "MM/dd/yyyy",
"focusSkin": "sknCarioRegular",
"height": "15%",
"id": "calDateToCards",
"isVisible": true,
"right": "5%",
"onSelection": AS_Calendar_d333c285dde444c5a69bb5d7dee0d11d,
"skin": "sknCarioRegular",
"top": "65%",
"validStartDate": [7, 2, 2018],
"viewConfig": {
"gridConfig": {
"gridCellSkin": "Copycalender0h05b9ba7ebfb45",
"gridSkin": "Copycalender0c4c48cb6d2174e",
"leftNavigationImage": "directions_turn_left.png",
"rightNavigationImage": "directions_turn_right.png"
}
},
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "90%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"mode": constants.CALENDAR_WHEEL_ONLY_DATE
});
var lblDateToCards = new kony.ui.Label({
"height": "20%",
"id": "lblDateToCards",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"top": "60%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0ief60611be0e42 = new kony.ui.Label({
"id": "CopyLabel0ief60611be0e42",
"isVisible": true,
"right": "5.00%",
"skin": "sknCarioRegular90Opc70",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.from"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0i237b695a64a49 = new kony.ui.Label({
"id": "CopyLabel0i237b695a64a49",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular90Opc70",
"text": kony.i18n.getLocalizedString("i18n.common.ToC"),
"top": "45%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCalenderIconFrom = new kony.ui.Label({
"centerY": "35%",
"id": "lblCalenderIconFrom",
"isVisible": true,
"left": "5%",
"skin": "sknLblBoj145",
"text": "T",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCalenderIconTo = new kony.ui.Label({
"centerY": "75%",
"id": "lblCalenderIconTo",
"isVisible": true,
"left": "5%",
"skin": "sknLblBoj145",
"text": "T",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0c481c34c5bb540.add(lblDateFromCards, CopyFlexContainer0c7c87912c7ac48, CopyFlexContainer0d5ae4aa3c34e4c, calDateFromCards, calDateToCards, lblDateToCards, CopyLabel0ief60611be0e42, CopyLabel0i237b695a64a49, lblCalenderIconFrom, lblCalenderIconTo);
flxCardsFilterScreen.add(CopyFlexContainer0c481c34c5bb540);
frmFilterTransaction.add(flxFilterTransactionHeader, flxFilterTransaction, flxDatePicker, flxCardsFilterScreen);
};
function frmFilterTransactionGlobalsAr() {
frmFilterTransactionAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmFilterTransactionAr,
"enabledForIdleTimeout": true,
"id": "frmFilterTransaction",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_bdefaf8bc28e451a9461234684a72847,
"skin": "sknBackground"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NEXTPREV,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
