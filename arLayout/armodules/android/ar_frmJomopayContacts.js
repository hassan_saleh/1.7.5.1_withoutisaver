//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:35 EET 2020
function addWidgetsfrmJomopayContactsAr() {
frmJomopayContacts.setDefaultUnit(kony.flex.DP);
var flxJoMoPayHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxJoMoPayHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJoMoPayHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_c5f00d906c7541bc8f4a2f4815cfb543,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
var lblJoMoPay = new kony.ui.Label({
"centerX": "50%",
"height": "90%",
"id": "lblJoMoPay",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.jomopay.contacts"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxJoMoPayHeader.add(flxBack, lblJoMoPay);
var flxSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "flxSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "8%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSearch.setDefaultUnit(kony.flex.DP);
var btnSearch = new kony.ui.Button({
"focusSkin": "sknSearch",
"height": "96%",
"id": "btnSearch",
"isVisible": true,
"right": "5%",
"skin": "sknSearch",
"text": "h",
"top": "0%",
"width": "8%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxBorderLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "flxBorderLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "90%",
"width": "90%",
"zIndex": 2
}, {}, {});
flxBorderLine.setDefaultUnit(kony.flex.DP);
flxBorderLine.add();
var txtSearch = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "CopyslTextBox0c1d969ae9edf4b",
"height": "96%",
"id": "txtSearch",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "13%",
"onTextChange": AS_TextField_a125f69b88174470a998be56345fd45a,
"placeholder": kony.i18n.getLocalizedString("i18n.transfer.searchPlh"),
"secureTextEntry": false,
"skin": "CopyslTextBox0c1d969ae9edf4b",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0%",
"width": "80%",
"zIndex": 2
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxSearch.add(btnSearch, flxBorderLine, txtSearch);
var flxJomoPayList = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "83%",
"horizontalScrollIndicator": true,
"id": "flxJomoPayList",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "17%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxJomoPayList.setDefaultUnit(kony.flex.DP);
var segJomopayList = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"btnPic": "",
"lblName": "",
"lblPhoneNum": ""
}],
"groupCells": false,
"height": "100%",
"id": "segJomopayList",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"onRowClick": AS_Segment_ha9f1adc915943c3b2a9db7753e7d021,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "Copyseg0c15349e52ca249",
"rowTemplate": flxJomoPayContacts,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "1e415b00",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"btnPic": "btnPic",
"flxJomoPayContacts": "flxJomoPayContacts",
"flxProfilePic": "flxProfilePic",
"flxUserDetails": "flxUserDetails",
"lblName": "lblName",
"lblPhoneNum": "lblPhoneNum"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxJomoPayList.add(segJomopayList);
frmJomopayContacts.add(flxJoMoPayHeader, flxSearch, flxJomoPayList);
};
function frmJomopayContactsGlobalsAr() {
frmJomopayContactsAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmJomopayContactsAr,
"enabledForIdleTimeout": true,
"id": "frmJomopayContacts",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_cb4e502943ee4513a459e6b223b2f880,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
