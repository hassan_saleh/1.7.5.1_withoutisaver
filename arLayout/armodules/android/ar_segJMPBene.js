//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:36 EET 2020
function initializesegJMPBeneAr() {
    flxSegJMPBeneAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "85dp",
        "id": "flxSegJMPBene",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    flxSegJMPBeneAr.setDefaultUnit(kony.flex.DP);
    var flxAnimateBenf = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxAnimateBenf",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAnimateBenf.setDefaultUnit(kony.flex.DP);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "1%",
        "skin": "sknFlxToIcon",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon1.add(lblInitial);
    var flxDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60%",
        "id": "flxDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "15%",
        "skin": "slFbox",
        "top": "32%",
        "width": "54%",
        "zIndex": 1
    }, {}, {});
    flxDetails.setDefaultUnit(kony.flex.DP);
    var BenificiaryName = new kony.ui.Label({
        "id": "BenificiaryName",
        "isVisible": true,
        "right": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblBack",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var accountNumber = new kony.ui.Label({
        "id": "accountNumber",
        "isVisible": true,
        "right": "0%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0%",
        "width": "84%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDetails.add(BenificiaryName, accountNumber);
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "99%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    var flxButtonHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxButtonHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "90%",
        "skin": "flexTransparent",
        "top": "0%",
        "width": "10%",
        "zIndex": 1
    }, {}, {});
    flxButtonHolder.setDefaultUnit(kony.flex.DP);
    var btnDelete = new kony.ui.Button({
        "focusSkin": "CopysknSegmentDelete0h225a870b5144b",
        "height": "100%",
        "id": "btnDelete",
        "isVisible": true,
        "right": "0%",
        "onClick": AS_Button_be6a7dc1468547daa6515573021dc30e,
        "skin": "CopysknSegmentDelete0h225a870b5144b",
        "text": "w",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxButtonHolder.add(btnDelete);
    var btnUpdateBene = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnBack",
        "height": "100%",
        "id": "btnUpdateBene",
        "isVisible": true,
        "onClick": AS_Button_j1979157760d4889bb4c090698b04a8b,
        "left": "20%",
        "skin": "sknBtnBack",
        "text": "0",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var btnFav = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnBack",
        "height": "100%",
        "id": "btnFav",
        "isVisible": true,
        "onClick": AS_Button_ge741f86c1c94ffb9fb6be9a8c9242a4,
        "left": "10%",
        "skin": "sknBtnBack",
        "text": "k",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var lblSeparator = new kony.ui.Label({
        "height": "1px",
        "id": "lblSeparator",
        "isVisible": true,
        "right": "0%",
        "skin": "skndivider",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "98%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAnimateBenf.add(flxIcon1, flxDetails, contactListDivider, flxButtonHolder, btnUpdateBene, btnFav, lblSeparator);
    flxSegJMPBeneAr.add(flxAnimateBenf);
}
