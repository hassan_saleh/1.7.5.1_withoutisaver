//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:35 EET 2020
function addWidgetsfrmAuthorizationAlternativesAr() {
frmAuthorizationAlternatives.setDefaultUnit(kony.flex.DP);
var flxMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxHead = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHead",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFlxHeaderImg",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHead.setDefaultUnit(kony.flex.DP);
var btnQuickCancel = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "btnCardsScreen",
"height": "100%",
"id": "btnQuickCancel",
"isVisible": true,
"left": "0%",
"onClick": AS_Button_c93732f3b9ec4b06b3a8ad2584ee1520,
"skin": "btnCardsScreen",
"text": "O",
"width": "13%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var lblQuickAccess = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblQuickAccess",
"isVisible": true,
"left": "13%",
"skin": "CopylblAmountCurrency0de5d74cad09b40",
"text": kony.i18n.getLocalizedString("i18n.quickAccessSetting.quickAccessPrelogin"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "70%",
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "48%",
"clipBounds": true,
"height": "100%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"onClick": AS_FlexContainer_e262328723ab4affbfb2d52370caf21a,
"right": "0%",
"skin": "slFbox",
"width": "17%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var flxAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxAccounts",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "0dp",
"onClick": AS_FlexContainer_befaef06ed01478c89b2df1e21addc7e,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {}, {});
flxAccounts.setDefaultUnit(kony.flex.DP);
var lblAccounts = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblAccounts",
"isVisible": true,
"left": "1%",
"onTouchStart": AS_Label_f52791aebcb949f590e0805cd9bb0a52,
"skin": "sknAcclbl",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblNext = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblNext",
"isVisible": true,
"left": "1%",
"onTouchStart": AS_Label_b323fb75d36a41e7b78fafe1e58cf6fe,
"skin": "sknFwdArrow",
"text": "o",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccounts.add(lblAccounts, lblNext);
var btnSaveQuickBal = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "jomopaynextEnabled",
"height": "100%",
"id": "btnSaveQuickBal",
"isVisible": true,
"onClick": AS_Button_hd30c8b058e84e48b65dfae882ee3f4e,
"right": "0%",
"skin": "jomopaynextEnabled",
"text": kony.i18n.getLocalizedString("i18n.common.save"),
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxBack.add(flxAccounts, btnSaveQuickBal);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "48%",
"id": "lblTitle",
"isVisible": false,
"left": "140dp",
"skin": "sknLblTitle",
"text": "Preferences",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "8dp",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHead.add(btnQuickCancel, lblQuickAccess, flxBack, lblTitle);
var FlexScrollContainer0ade157aa36f344 = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "FlexScrollContainer0ade157aa36f344",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
FlexScrollContainer0ade157aa36f344.setDefaultUnit(kony.flex.DP);
var lblAuthorization = new kony.ui.Label({
"id": "lblAuthorization",
"isVisible": true,
"right": "5%",
"skin": "sknlblTouchIdsmall",
"text": kony.i18n.getLocalizedString("i18n.auth.common"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAuthBody = new kony.ui.Label({
"id": "lblAuthBody",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.auth.text"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAlterAuthTouchID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "flxAlterAuthTouchID",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"onClick": AS_FlexContainer_i9556f057da4410aae2546d3d5159c0e,
"skin": "sknFlxLightGreyColor",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAlterAuthTouchID.setDefaultUnit(kony.flex.DP);
var lblAlternateMethodtext = new kony.ui.Label({
"centerY": "50%",
"id": "lblAlternateMethodtext",
"isVisible": true,
"right": "5%",
"skin": "sknlblTouchIdsmall",
"text": kony.i18n.getLocalizedString("i18n.common.touchID"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyImage0eeeb79bca7e048 = new kony.ui.Image2({
"centerY": "50%",
"height": "80%",
"id": "CopyImage0eeeb79bca7e048",
"isVisible": false,
"right": "57%",
"skin": "slImage",
"src": "touch_id_icon.png",
"top": "10%",
"width": "15%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxSwitchOffTouchLogin = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxSwitchOffTouchLogin",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "83%",
"skin": "sknflxGrey",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSwitchOffTouchLogin.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0hfee6fe5ed994b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlue0hfee6fe5ed994b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0hfee6fe5ed994b.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0hfee6fe5ed994b.add();
var Copyflxlakeer0a0654737f4504c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "Copyflxlakeer0a0654737f4504c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
Copyflxlakeer0a0654737f4504c.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0a0654737f4504c.add();
flxSwitchOffTouchLogin.add(CopyflxRoundDBlue0hfee6fe5ed994b, Copyflxlakeer0a0654737f4504c);
var flxSwitchOnTouchLogin = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxSwitchOnTouchLogin",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "83%",
"skin": "sknflxyellow",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSwitchOnTouchLogin.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0j3222bd94cbc49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlueOff0j3222bd94cbc49",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 2
}, {}, {});
CopyflxRoundDBlueOff0j3222bd94cbc49.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0j3222bd94cbc49.add();
var CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.add();
flxSwitchOnTouchLogin.add(CopyflxRoundDBlueOff0j3222bd94cbc49, CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b);
flxAlterAuthTouchID.add(lblAlternateMethodtext, CopyImage0eeeb79bca7e048, flxSwitchOffTouchLogin, flxSwitchOnTouchLogin);
var flxAltAuthPIN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "flxAltAuthPIN",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"onClick": AS_FlexContainer_a83f5d9c6c234ff39649c015ac67bd58,
"skin": "sknFlxLightGreyColor",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAltAuthPIN.setDefaultUnit(kony.flex.DP);
var CopyLabel0a2739695179248 = new kony.ui.Label({
"centerY": "50%",
"id": "CopyLabel0a2739695179248",
"isVisible": true,
"right": "5%",
"skin": "sknlblTouchIdsmall",
"text": kony.i18n.getLocalizedString("i18n.common.pin"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyImage0a6fdf1ad117749 = new kony.ui.Image2({
"centerY": "50%",
"height": "80%",
"id": "CopyImage0a6fdf1ad117749",
"isVisible": false,
"right": "57%",
"skin": "slImage",
"src": "touch_id_icon.png",
"top": "10%",
"width": "15%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxSwitchOnPinLogin = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxSwitchOnPinLogin",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "83%",
"skin": "sknflxyellow",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSwitchOnPinLogin.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0ef9f0f04695e40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlueOff0ef9f0f04695e40",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 2
}, {}, {});
CopyflxRoundDBlueOff0ef9f0f04695e40.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0ef9f0f04695e40.add();
var CopyflxNaveenbhaiKiLakeer0bb5a0d12709d48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "CopyflxNaveenbhaiKiLakeer0bb5a0d12709d48",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0bb5a0d12709d48.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0bb5a0d12709d48.add();
flxSwitchOnPinLogin.add(CopyflxRoundDBlueOff0ef9f0f04695e40, CopyflxNaveenbhaiKiLakeer0bb5a0d12709d48);
var flxSwitchOffPinLogin = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxSwitchOffPinLogin",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "83%",
"skin": "sknflxGrey",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSwitchOffPinLogin.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0ad733803781440 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlue0ad733803781440",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0ad733803781440.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0ad733803781440.add();
var Copyflxlakeer0a6b2b71b692c4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "Copyflxlakeer0a6b2b71b692c4c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
Copyflxlakeer0a6b2b71b692c4c.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0a6b2b71b692c4c.add();
flxSwitchOffPinLogin.add(CopyflxRoundDBlue0ad733803781440, Copyflxlakeer0a6b2b71b692c4c);
flxAltAuthPIN.add(CopyLabel0a2739695179248, CopyImage0a6fdf1ad117749, flxSwitchOnPinLogin, flxSwitchOffPinLogin);
var lblQuickAccessBody = new kony.ui.Label({
"id": "lblQuickAccessBody",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.quickAccessSetting.quickAccessPreloginDesc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "4%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxQuickBalance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxQuickBalance",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "5%",
"skin": "sknFlxLightGreyColor",
"top": "5%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxQuickBalance.setDefaultUnit(kony.flex.DP);
var flxQuickBalanceSwitch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "flxQuickBalanceSwitch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5.03%",
"skin": "sknflxTransprnt",
"top": "18dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxQuickBalanceSwitch.setDefaultUnit(kony.flex.DP);
var CopyLabel0aa162570bef148 = new kony.ui.Label({
"id": "CopyLabel0aa162570bef148",
"isVisible": true,
"right": "0%",
"skin": "sknlblTouchIdsmall",
"text": kony.i18n.getLocalizedString("i18n.quickAccessSetting.enableQuickBalance"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxSwitchOn = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxSwitchOn",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_a1f6c6f666db4f85a7469341d8131fab,
"left": "2%",
"skin": "sknflxyellow",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSwitchOn.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0ed98162cb87345 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlueOff0ed98162cb87345",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 2
}, {}, {});
CopyflxRoundDBlueOff0ed98162cb87345.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0ed98162cb87345.add();
var CopyflxNaveenbhaiKiLakeer0b879f5d5aba342 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "CopyflxNaveenbhaiKiLakeer0b879f5d5aba342",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0b879f5d5aba342.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0b879f5d5aba342.add();
flxSwitchOn.add(CopyflxRoundDBlueOff0ed98162cb87345, CopyflxNaveenbhaiKiLakeer0b879f5d5aba342);
var flxSwitchOff = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "49%",
"clipBounds": true,
"height": "25dp",
"id": "flxSwitchOff",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_i8f4dea6e41041908d7fbd3569c1fe36,
"left": "2%",
"skin": "sknflxGrey",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSwitchOff.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0j40e19dd87274a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlue0j40e19dd87274a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0j40e19dd87274a.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0j40e19dd87274a.add();
var Copyflxlakeer0e8f83b92988f43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "Copyflxlakeer0e8f83b92988f43",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
Copyflxlakeer0e8f83b92988f43.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0e8f83b92988f43.add();
flxSwitchOff.add(CopyflxRoundDBlue0j40e19dd87274a, Copyflxlakeer0e8f83b92988f43);
flxQuickBalanceSwitch.add(CopyLabel0aa162570bef148, flxSwitchOn, flxSwitchOff);
var segAccounts = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "51.06%",
"data": [{
"lblAccNumber": "Label",
"lblAccount": "Salary account ***125",
"lblAccountName": "Salary Account",
"lblIncommingRing": "s",
"lblIncommingTick": "r"
}, {
"lblAccNumber": "Label",
"lblAccount": "Savings account ***125",
"lblAccountName": "Salary Account",
"lblIncommingRing": "s",
"lblIncommingTick": "r"
}],
"groupCells": false,
"id": "segAccounts",
"isVisible": true,
"right": "4%",
"needPageIndicator": true,
"onRowClick": AS_Segment_f3447e178f93428286a796ca215d9354,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "sknsegAcc",
"rowTemplate": flxSegQuickBalance,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": false,
"showScrollbars": false,
"top": "10dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"flxSegQuickBalance": "flxSegQuickBalance",
"lblAccNumber": "lblAccNumber",
"lblAccount": "lblAccount",
"lblAccountName": "lblAccountName",
"lblIncommingRing": "lblIncommingRing",
"lblIncommingTick": "lblIncommingTick"
},
"width": "92%",
"zIndex": 1,
"enableReordering": false
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var LabelNoRecordsKA = new kony.ui.Label({
"id": "LabelNoRecordsKA",
"isVisible": false,
"right": "16%",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.alerts.NoAccountsMsg"),
"top": "20dp",
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxQuickBalance.add(flxQuickBalanceSwitch, segAccounts, LabelNoRecordsKA);
var flxTransactionPreview = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxTransactionPreview",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknFlxLightGreyColor",
"top": "6%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxTransactionPreview.setDefaultUnit(kony.flex.DP);
var CopyflxAltAuthPIN0i58c1e23dacd40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerY": "50%",
"clipBounds": true,
"id": "CopyflxAltAuthPIN0i58c1e23dacd40",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknflxTransprnt",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyflxAltAuthPIN0i58c1e23dacd40.setDefaultUnit(kony.flex.DP);
var CopyLabel0d262912d12cf4f = new kony.ui.Label({
"id": "CopyLabel0d262912d12cf4f",
"isVisible": true,
"right": "0%",
"skin": "sknlblTouchIdsmall",
"text": kony.i18n.getLocalizedString("i18n.quickAccessSetting.enableQuickTransactions"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopySwitch0ca632b3700c14e = new kony.ui.Switch({
"id": "CopySwitch0ca632b3700c14e",
"isVisible": false,
"rightSideText": "ON",
"left": "4%",
"leftSideText": "OFF",
"selectedIndex": 1,
"skin": "slSwitch",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyflxAltAuthPIN0i58c1e23dacd40.add(CopyLabel0d262912d12cf4f, CopySwitch0ca632b3700c14e);
var flxSwitchTransactionOff = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxSwitchTransactionOff",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_d23e8aa0f78a492cb66e249abdbeff7f,
"left": "6%",
"skin": "sknflxGrey",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSwitchTransactionOff.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0f9ae119ea32d47 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlue0f9ae119ea32d47",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0f9ae119ea32d47.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0f9ae119ea32d47.add();
var Copyflxlakeer0f1a316c4443246 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "Copyflxlakeer0f1a316c4443246",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
Copyflxlakeer0f1a316c4443246.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0f1a316c4443246.add();
flxSwitchTransactionOff.add(CopyflxRoundDBlue0f9ae119ea32d47, Copyflxlakeer0f1a316c4443246);
var flxSwitchTransactionsOn = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxSwitchTransactionsOn",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_jab4ee56fd12423ea5f8bf78b0ea8a6e,
"left": "6%",
"skin": "sknflxyellow",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSwitchTransactionsOn.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0e1af28408cf640 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlueOff0e1af28408cf640",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 2
}, {}, {});
CopyflxRoundDBlueOff0e1af28408cf640.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0e1af28408cf640.add();
var CopyflxNaveenbhaiKiLakeer0affad3d053c64d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "CopyflxNaveenbhaiKiLakeer0affad3d053c64d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0affad3d053c64d.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0affad3d053c64d.add();
flxSwitchTransactionsOn.add(CopyflxRoundDBlueOff0e1af28408cf640, CopyflxNaveenbhaiKiLakeer0affad3d053c64d);
flxTransactionPreview.add(CopyflxAltAuthPIN0i58c1e23dacd40, flxSwitchTransactionOff, flxSwitchTransactionsOn);
FlexScrollContainer0ade157aa36f344.add(lblAuthorization, lblAuthBody, flxAlterAuthTouchID, flxAltAuthPIN, lblQuickAccessBody, flxQuickBalance, flxTransactionPreview);
flxMain.add(flxHead, FlexScrollContainer0ade157aa36f344);
frmAuthorizationAlternatives.add(flxMain);
};
function frmAuthorizationAlternativesGlobalsAr() {
frmAuthorizationAlternativesAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmAuthorizationAlternativesAr,
"enabledForIdleTimeout": true,
"id": "frmAuthorizationAlternatives",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_cf6b6d5d09544b57a668bc3773fce7a2,
"skin": "sknfrmAuth"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_a25cebf0501b48efa9ff3788ff622322,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
