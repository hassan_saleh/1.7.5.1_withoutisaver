//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:35 EET 2020
function addWidgetsfrmConfirmDepositKAAr() {
    frmConfirmDepositKA.setDefaultUnit(kony.flex.DP);
    var titleBarWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarWrapper.setDefaultUnit(kony.flex.DP);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var androidTitleLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "androidTitleLabel",
        "isVisible": true,
        "right": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.deposit.ConfirmDeposit"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    androidTitleBar.add(androidTitleLabel);
    var backButton = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "skntitleBarTextButtonFocus",
        "height": "50dp",
        "id": "backButton",
        "isVisible": true,
        "right": "0dp",
        "minWidth": "50dp",
        "onClick": AS_Button_0c0e9c80ed7147f4922863f0f04c9074,
        "skin": "skntitleBarTextButton",
        "text": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,2, 0],
        "paddingInPixel": false
    }, {});
    titleBarWrapper.add(androidTitleBar, backButton);
    var flxMainKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxMainKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainKA.setDefaultUnit(kony.flex.DP);
    var flxDescriptionKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "115dp",
        "id": "flxDescriptionKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDescriptionKA.setDefaultUnit(kony.flex.DP);
    var flxamountKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxamountKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flxamountKA.setDefaultUnit(kony.flex.DP);
    var lblCurrencytype = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblCurrencytype",
        "isVisible": true,
        "skin": "skndetailPageNumber",
        "text": "kk",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionAmount = new kony.ui.Label({
        "id": "transactionAmount",
        "isVisible": false,
        "right": "0%",
        "skin": "skndetailPageNumber",
        "text": "5000",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxamountKA.add(lblCurrencytype, transactionAmount);
    var lbltransactionAmount = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbltransactionAmount",
        "isVisible": false,
        "right": "0%",
        "skin": "skndetailPageNumber",
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionTo = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionTo",
        "isVisible": true,
        "skin": "skndetailPageDate",
        "text": kony.i18n.getLocalizedString("i18n.deposit.DepositTo"),
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionName = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionName",
        "isVisible": true,
        "skin": "sknaccountName",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var toAccountNumberKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "toAccountNumberKA",
        "isVisible": false,
        "skin": "sknNumber",
        "text": "Father's Savings Account",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var createdDate = new kony.ui.Label({
        "centerX": "50%",
        "id": "createdDate",
        "isVisible": false,
        "skin": "sknNumber",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var depositsType = new kony.ui.Label({
        "centerX": "50%",
        "id": "depositsType",
        "isVisible": false,
        "skin": "sknNumber",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Copydivider04de6f2742ada45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider04de6f2742ada45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": 10,
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider04de6f2742ada45.setDefaultUnit(kony.flex.DP);
    Copydivider04de6f2742ada45.add();
    flxDescriptionKA.add(flxamountKA, lbltransactionAmount, transactionTo, transactionName, toAccountNumberKA, createdDate, depositsType, Copydivider04de6f2742ada45);
    var flxNotesKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxNotesKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNotesKA.setDefaultUnit(kony.flex.DP);
    var lblTitleNotesKA = new kony.ui.Label({
        "id": "lblTitleNotesKA",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.common.notesc"),
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionNotes = new kony.ui.Label({
        "bottom": "15dp",
        "id": "transactionNotes",
        "isVisible": true,
        "right": "5%",
        "skin": "sknNumber",
        "text": "Note details appear here. This can be a muliple line descripsion",
        "top": "10dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var notesLineDividerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "notesLineDividerKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    notesLineDividerKA.setDefaultUnit(kony.flex.DP);
    notesLineDividerKA.add();
    flxNotesKA.add(lblTitleNotesKA, transactionNotes, notesLineDividerKA);
    var flxBtnsKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10%",
        "clipBounds": true,
        "height": "130dp",
        "id": "flxBtnsKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "slFbox",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBtnsKA.setDefaultUnit(kony.flex.DP);
    var btnConfirm = new kony.ui.Button({
        "centerX": "49.97%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "btnConfirm",
        "isVisible": true,
        "onClick": AS_Button_f022bd62cc004cc08cb70042c44ddcec,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.transfers.CONFIRM"),
        "top": "10.00%",
        "width": "60%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var btnEditDepositKA = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "35dp",
        "id": "btnEditDepositKA",
        "isVisible": true,
        "onClick": AS_Button_b33522cb78f9484fa409bc04460e2a7d,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.edit"),
        "top": "5dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxBtnsKA.add(btnConfirm, btnEditDepositKA);
    flxMainKA.add(flxDescriptionKA, flxNotesKA, flxBtnsKA);
    var lblfrontKA = new kony.ui.Label({
        "id": "lblfrontKA",
        "isVisible": false,
        "right": "61dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblbackKA = new kony.ui.Label({
        "id": "lblbackKA",
        "isVisible": false,
        "right": "74dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "394dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    frmConfirmDepositKA.add(titleBarWrapper, flxMainKA, lblfrontKA, lblbackKA);
};
function frmConfirmDepositKAGlobalsAr() {
    frmConfirmDepositKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmConfirmDepositKAAr,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmConfirmDepositKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_980b115b0ca04bf5bdaa33c98d975247,
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
