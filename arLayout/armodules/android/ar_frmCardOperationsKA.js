//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:35 EET 2020
function addWidgetsfrmCardOperationsKAAr() {
frmCardOperationsKA.setDefaultUnit(kony.flex.DP);
var touchFeature = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "110%",
"id": "touchFeature",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
touchFeature.setDefaultUnit(kony.flex.DP);
var flxCardDetailsKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxCardDetailsKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "60dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCardDetailsKA.setDefaultUnit(kony.flex.DP);
var lblCardOperationHeaderKA = new kony.ui.Label({
"bottom": "20dp",
"id": "lblCardOperationHeaderKA",
"isVisible": true,
"right": "0dp",
"skin": "sknsecondaryHeader",
"text": kony.i18n.getLocalizedString("i18n.manage_cards.cancellingCreditCard"),
"top": "0dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxCardTypeKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20dp",
"id": "flxCardTypeKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "30dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCardTypeKA.setDefaultUnit(kony.flex.DP);
var lblCardTypeKA = new kony.ui.Label({
"id": "lblCardTypeKA",
"isVisible": true,
"skin": "sknaccountName",
"text": "Card Type : Supplement Card",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCardTypeKA.add(lblCardTypeKA);
var flxCardNumberKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20dp",
"id": "flxCardNumberKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "3dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCardNumberKA.setDefaultUnit(kony.flex.DP);
var lblCardNumberKA = new kony.ui.Label({
"id": "lblCardNumberKA",
"isVisible": true,
"right": 0,
"skin": "sknaccountName",
"text": "Card Number : XXXX XXXX XXXX 1234",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCardNumberKA.add(lblCardNumberKA);
var flxValidThroguhKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20dp",
"id": "flxValidThroguhKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "3dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxValidThroguhKA.setDefaultUnit(kony.flex.DP);
var lblValidThroughKA = new kony.ui.Label({
"centerX": "50%",
"id": "lblValidThroughKA",
"isVisible": true,
"skin": "sknaccountName",
"text": "Valid Through : 02 / 20",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxValidThroguhKA.add(lblValidThroughKA);
var flxCardHolderNameKa = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20dp",
"id": "flxCardHolderNameKa",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "3dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCardHolderNameKa.setDefaultUnit(kony.flex.DP);
var lblCardHolderNameKA = new kony.ui.Label({
"centerX": "50.00%",
"id": "lblCardHolderNameKA",
"isVisible": true,
"skin": "sknaccountName",
"text": "Name  : Govind",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCardHolderNameKa.add(lblCardHolderNameKA);
var txtAreaReasonKA = new kony.ui.TextArea2({
"autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_SENTENCES,
"centerX": "49.97%",
"height": "120dp",
"id": "txtAreaReasonKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
"right": "5dp",
"numberOfVisibleLines": 3,
"placeholder": kony.i18n.getLocalizedString("i18n.cards.Providereasonhere"),
"skin": "sknLatoRegularBlack108",
"textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
"top": "10dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 2, 2,2, 2],
"paddingInPixel": false
}, {
"placeholderSkin": "sknLotoRegular100PlaceholderKA"
});
var enableTouchID = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknprimaryActionFocus",
"height": "42dp",
"id": "enableTouchID",
"isVisible": true,
"onClick": AS_Button_024147f73b3949aaa8ec1d29ecef8871,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.common.btnContinue"),
"top": "40dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var noThanks = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "noThanks",
"isVisible": true,
"onClick": AS_Button_e7f9754edb484273a7a66e136841659e,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.manage_cards.noThanks"),
"top": "4dp",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxCustomerCareCallKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "flxCustomerCareCallKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"skin": "skngenericCard",
"top": 0,
"width": "100%",
"zIndex": 1
}, {}, {});
flxCustomerCareCallKA.setDefaultUnit(kony.flex.DP);
var lblCustomerCareTextKA = new kony.ui.Label({
"centerY": "50%",
"id": "lblCustomerCareTextKA",
"isVisible": true,
"right": "10dp",
"skin": "sknaccountName",
"text": kony.i18n.getLocalizedString("i18n.locateUs.CallCustomerCare"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": "200dp"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnCustomerCareCallKA = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknBtnPhoneCallKA",
"height": "30dp",
"id": "btnCustomerCareCallKA",
"isVisible": true,
"onClick": AS_Button_15645b56d83d45808665b2ccc152d871,
"left": "10dp",
"skin": "sknBtnPhoneCallKA",
"width": "30dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxCustomerCareCallKA.add(lblCustomerCareTextKA, btnCustomerCareCallKA);
flxCardDetailsKA.add(lblCardOperationHeaderKA, flxCardTypeKA, flxCardNumberKA, flxValidThroguhKA, flxCardHolderNameKa, txtAreaReasonKA, enableTouchID, noThanks, flxCustomerCareCallKA);
var lblCardIdKA = new kony.ui.Label({
"id": "lblCardIdKA",
"isVisible": false,
"right": "58dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "538dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblActionKA = new kony.ui.Label({
"id": "lblActionKA",
"isVisible": false,
"right": "204dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "534dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
touchFeature.add(flxCardDetailsKA, lblCardIdKA, lblActionKA);
frmCardOperationsKA.add(touchFeature);
};
function frmCardOperationsKAGlobalsAr() {
frmCardOperationsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmCardOperationsKAAr,
"bounces": false,
"enableScrolling": true,
"enabledForIdleTimeout": true,
"id": "frmCardOperationsKA",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"pagingEnabled": false,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_fd3f4ce7dac7495c912e4f95b1dfe23d,
"outTransitionConfig": {
"formAnimation": 0
},
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
