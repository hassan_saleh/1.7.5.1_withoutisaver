//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:34 EET 2020
function initializeCopyFBox02ced27cc008242Ar() {
    CopyFBox02ced27cc008242Ar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyFBox02ced27cc008242",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox02ced27cc008242Ar.setDefaultUnit(kony.flex.DP);
    var moreListLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "moreListLabel",
        "isVisible": true,
        "right": "10%",
        "text": "Label",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var moreListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "moreListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "49dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "containerWeight": 100
    }, {});
    moreListDivider.setDefaultUnit(kony.flex.DP);
    moreListDivider.add();
    CopyFBox02ced27cc008242Ar.add(moreListLabel, moreListDivider);
}
