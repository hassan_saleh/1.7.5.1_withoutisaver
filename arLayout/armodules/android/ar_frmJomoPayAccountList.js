//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:35 EET 2020
function addWidgetsfrmJomoPayAccountListAr() {
frmJomoPayAccountList.setDefaultUnit(kony.flex.DP);
var flxJoMoPayHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxJoMoPayHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJoMoPayHeader.setDefaultUnit(kony.flex.DP);
var lblJoMoPay = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"id": "lblJoMoPay",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnBack = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
"height": "100%",
"id": "btnBack",
"isVisible": false,
"left": "0%",
"onClick": AS_Button_c7b350cf5948423895fbcfb35e9f2d2a,
"skin": "CopyslButtonGlossBlue0e73a02c4810645",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0%",
"width": "15%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_i0b54fe9bfc64f9bb1ab95989fc5ffb0,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 4
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
flxJoMoPayHeader.add(lblJoMoPay, btnBack, flxBack);
var flxPopupOuter = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxPopupOuter",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPopupOuter.setDefaultUnit(kony.flex.DP);
var flxInnerPopup = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bounces": true,
"centerX": "50%",
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "flxInnerPopup",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "CopyslFSbox0aabc4031db8b4f",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxInnerPopup.setDefaultUnit(kony.flex.DP);
flxInnerPopup.add();
var segPopup = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"data": [{
"lblJPTransfer": "",
"lblJomopayType": ""
}],
"groupCells": false,
"height": "100%",
"id": "segPopup",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"onRowClick": AS_Segment_ed8832a250094b22af42d53f771def8e,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "Copyseg0ece801af6b5645",
"rowTemplate": flxJPPopup,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "ffffff55",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"flxJPPopup": "flxJPPopup",
"lblJPTransfer": "lblJPTransfer",
"lblJomopayType": "lblJomopayType"
},
"width": "100%",
"zIndex": 20
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxPopupOuter.add(flxInnerPopup, segPopup);
frmJomoPayAccountList.add(flxJoMoPayHeader, flxPopupOuter);
};
function frmJomoPayAccountListGlobalsAr() {
frmJomoPayAccountListAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmJomoPayAccountListAr,
"enabledForIdleTimeout": true,
"id": "frmJomoPayAccountList",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_b89a884fc43b48598781872c1c15bd19,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
