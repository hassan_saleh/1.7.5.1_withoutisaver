//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:36 EET 2020
function initializetmpBudgetListKAAr() {
flxBudgetListKAAr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"focusSkin": "sknyourAccountCardFocus",
"height": "80dp",
"id": "flxBudgetListKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknyourAccountCard",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBudgetListKAAr.setDefaultUnit(kony.flex.DP);
var flxTopLevelKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70%",
"id": "flxTopLevelKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTopLevelKA.setDefaultUnit(kony.flex.DP);
var ImgCategoryKA = new kony.ui.Image2({
"height": "95%",
"id": "ImgCategoryKA",
"isVisible": true,
"right": "6.35%",
"skin": "slImage",
"src": "budget_furnish.png",
"top": "5%",
"width": "10%"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var fullColorKA = new kony.ui.Label({
"bottom": 12,
"height": "70%",
"id": "fullColorKA",
"isVisible": true,
"right": "2%",
"skin": "sknBlueDark64A1D6",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "15%",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,2, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var restofColorKA = new kony.ui.Label({
"bottom": 12,
"height": "70%",
"id": "restofColorKA",
"isVisible": true,
"right": "0%",
"skin": "sknBlueLightB1D0EA",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "15%",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPercentageValKA = new kony.ui.Label({
"centerY": "50%",
"id": "lblPercentageValKA",
"isVisible": true,
"right": "2%",
"skin": "sknPercentageBoldKA",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTopLevelKA.add( lblPercentageValKA, restofColorKA, fullColorKA,ImgCategoryKA);
var flxBottomLevelKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30%",
"id": "flxBottomLevelKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "52dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBottomLevelKA.setDefaultUnit(kony.flex.DP);
var lblBelowLabelKA = new kony.ui.Label({
"id": "lblBelowLabelKA",
"isVisible": true,
"right": "20%",
"skin": "sknBottomLabelKA",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBottomLevelKA.add(lblBelowLabelKA);
flxBudgetListKAAr.add(flxTopLevelKA, flxBottomLevelKA);
}
