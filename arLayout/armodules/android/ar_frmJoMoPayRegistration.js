//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:35 EET 2020
function addWidgetsfrmJoMoPayRegistrationAr() {
frmJoMoPayRegistration.setDefaultUnit(kony.flex.DP);
var flxBodyReg = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxBodyReg",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "CopyslFbox0j1f7123f1c1b45",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxBodyReg.setDefaultUnit(kony.flex.DP);
var flxRegHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxRegHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxRegHeader.setDefaultUnit(kony.flex.DP);
var lblJomoPayReg = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"id": "lblJomoPayReg",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.jomopay.createaccount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnNext = new kony.ui.Button({
"focusSkin": "jomopaynextDisabled",
"height": "100%",
"id": "btnNext",
"isVisible": true,
"onClick": AS_Button_a647c3817c074b2bba981a5cb6184513,
"right": "0.00%",
"skin": "jomopaynextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "0.18%",
"width": "20%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var flxback = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxback",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_gc5babe6f53d4e398bd12047420466a3,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxback.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"height": "100%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxback.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
flxRegHeader.add(lblJomoPayReg, btnNext, flxback);
var flxRegBody = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "flxRegBody",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "CopysknDetails0d464d7b6c2be46",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxRegBody.setDefaultUnit(kony.flex.DP);
var flxNationalID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "12%",
"id": "flxNationalID",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxNationalID.setDefaultUnit(kony.flex.DP);
var lblNationalID = new kony.ui.Label({
"id": "lblNationalID",
"isVisible": true,
"right": "1%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.nationalid"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxNational = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"clipBounds": true,
"height": "56%",
"id": "flxNational",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNational.setDefaultUnit(kony.flex.DP);
var txtNationalID = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "100%",
"id": "txtNationalID",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"maxTextLength": 15,
"onDone": AS_TextField_cc0dca9b00994741ac95cec1b83895b3,
"onTextChange": AS_TextField_e2928993b4f74feeb62cbf61a2291285,
"onTouchEnd": AS_TextField_if60d854502945d58ffe029dab774a35,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0.00%",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onBeginEditing": AS_TextField_bdee0d49dd4047f898e21d080ba9b8a9,
"onEndEditing": AS_TextField_c997f2cdf5604f639f39f8a9df693032,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxNational.add(txtNationalID);
var flxBorderBenificiary = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxBorderBenificiary",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxBorderBenificiary.setDefaultUnit(kony.flex.DP);
flxBorderBenificiary.add();
flxNationalID.add(lblNationalID, flxNational, flxBorderBenificiary);
var flxIDType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14.50%",
"id": "flxIDType",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "13%",
"width": "98%",
"zIndex": 1
}, {}, {});
flxIDType.setDefaultUnit(kony.flex.DP);
var lblIDTypeStatic = new kony.ui.Label({
"height": "35%",
"id": "lblIDTypeStatic",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.jomopay.idtype"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxID",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "37%",
"width": "96%",
"zIndex": 1
}, {}, {});
flxID.setDefaultUnit(kony.flex.DP);
var btnDropDownIDType = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"height": "100%",
"id": "btnDropDownIDType",
"isVisible": true,
"onClick": AS_Button_e064dc4537214511bee4705c4f350d57,
"left": "10%",
"skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"text": "d",
"top": "-5%",
"width": "90%",
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblIDType = new kony.ui.Label({
"height": "70%",
"id": "lblIDType",
"isVisible": true,
"right": "0%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.common.ID"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxID.add(btnDropDownIDType, lblIDType);
var flxBorderIDType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "20%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderIDType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "8%",
"skin": "skntextFieldDividerGreen",
"width": "87%",
"zIndex": 1
}, {}, {});
flxBorderIDType.setDefaultUnit(kony.flex.DP);
flxBorderIDType.add();
flxIDType.add(lblIDTypeStatic, flxID, flxBorderIDType);
var flxAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxAccount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccount.setDefaultUnit(kony.flex.DP);
var lblAccountStaticText = new kony.ui.Label({
"height": "35%",
"id": "lblAccountStaticText",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.settings.account"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAccountType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxAccountType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "slFbox",
"top": "38%",
"width": "96%",
"zIndex": 1
}, {}, {});
flxAccountType.setDefaultUnit(kony.flex.DP);
var btnDropDownAccount = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"height": "100%",
"id": "btnDropDownAccount",
"isVisible": true,
"onClick": AS_Button_d019d28df64245879ace1840df23d9d2,
"left": "7%",
"skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"text": "d",
"top": "2%",
"width": "93%",
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxAccountNo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxAccountNo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "1%",
"skin": "slFbox",
"top": "5%",
"width": "80%",
"zIndex": 2
}, {}, {});
flxAccountNo.setDefaultUnit(kony.flex.DP);
var lblAccountType = new kony.ui.Label({
"height": "100%",
"id": "lblAccountType",
"isVisible": true,
"right": "0%",
"skin": "sknTransferType",
"text": "Current Account",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountCode = new kony.ui.Label({
"height": "100%",
"id": "lblAccountCode",
"isVisible": true,
"right": "57%",
"skin": "lblsegtextsmall0b5a3b38d4be646",
"text": " ***2002",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccountNo.add(lblAccountType, lblAccountCode);
flxAccountType.add(btnDropDownAccount, flxAccountNo);
var flxBorderAccountType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "5%",
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderAccountType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDividerGreen",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBorderAccountType.setDefaultUnit(kony.flex.DP);
flxBorderAccountType.add();
flxAccount.add(lblAccountStaticText, flxAccountType, flxBorderAccountType);
var flxJoMoPayType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxJoMoPayType",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJoMoPayType.setDefaultUnit(kony.flex.DP);
var flxJoMoPayTypeStaticText = new kony.ui.Label({
"id": "flxJoMoPayTypeStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.jomopay.jomopaytype"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxJoMoPay = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxJoMoPay",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "slFbox",
"top": "38%",
"width": "96%",
"zIndex": 1
}, {}, {});
flxJoMoPay.setDefaultUnit(kony.flex.DP);
var lblType = new kony.ui.Label({
"height": "100%",
"id": "lblType",
"isVisible": true,
"right": "1%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobiletype"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnAliasDropDown = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"height": "100%",
"id": "btnAliasDropDown",
"isVisible": true,
"onClick": AS_Button_e25ee67a7a024673a10b095a6feb0cc3,
"left": "7%",
"skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"text": "d",
"top": "2%",
"width": "93%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxJoMoPay.add(lblType, btnAliasDropDown);
var flxBorderjomopaytype = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderjomopaytype",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDividerGreen",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBorderjomopaytype.setDefaultUnit(kony.flex.DP);
flxBorderjomopaytype.add();
flxJoMoPayType.add(flxJoMoPayTypeStaticText, flxJoMoPay, flxBorderjomopaytype);
var flxJomopayID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "12%",
"id": "flxJomopayID",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "1%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxJomopayID.setDefaultUnit(kony.flex.DP);
var lblJomopayIDstatic = new kony.ui.Label({
"id": "lblJomopayIDstatic",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.jomopayID"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxJomopayIDs = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"clipBounds": true,
"height": "56%",
"id": "flxJomopayIDs",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJomopayIDs.setDefaultUnit(kony.flex.DP);
var txtJomopayID = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "100%",
"id": "txtJomopayID",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"maxTextLength": null,
"onDone": AS_TextField_c690bd23793846d593b62f10793b55a2,
"onTextChange": AS_TextField_g3be2e03d0dc435f9d4fe8b75f94a6eb,
"onTouchEnd": AS_TextField_e6a8afebd96e4a409322c6f74d829986,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0.00%",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onBeginEditing": AS_TextField_j58877c71f6249b1a1f0c16e115fbaca,
"onEndEditing": AS_TextField_hb7ab4ea13ba4c13843b6456a477ee2c,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxJomopayIDs.add(txtJomopayID);
var flxBorderjomopayID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxBorderjomopayID",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxBorderjomopayID.setDefaultUnit(kony.flex.DP);
flxBorderjomopayID.add();
flxJomopayID.add(lblJomopayIDstatic, flxJomopayIDs, flxBorderjomopayID);
var lblMobileIDHint = new kony.ui.Label({
"id": "lblMobileIDHint",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobilecashidhint"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAliasJMPR = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "12%",
"id": "flxAliasJMPR",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "1.00%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAliasJMPR.setDefaultUnit(kony.flex.DP);
var lblAliasJMPRstatic = new kony.ui.Label({
"id": "lblAliasJMPRstatic",
"isVisible": true,
"right": "1%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.aliastype"),
"textStyle": {},
"top": "40%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAliasTextbox = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"clipBounds": true,
"height": "56%",
"id": "flxAliasTextbox",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAliasTextbox.setDefaultUnit(kony.flex.DP);
var txtAlias = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "100%",
"id": "txtAlias",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"maxTextLength": 15,
"onDone": AS_TextField_a6e1de9baa724aa6817912d44f746c8a,
"onTextChange": AS_TextField_b60672505b88483eb6b80f3b1864dc42,
"onTouchEnd": AS_TextField_b5ae23195c9d432a8aee48a41cb7af12,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0.00%",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onBeginEditing": AS_TextField_ffca27a708f546d6b9220914d2fea901,
"onEndEditing": AS_TextField_e1d39ff494e447148c03a0aa64418b15,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxAliasTextbox.add(txtAlias);
var CopyflxBorderjomopayID0b6963ff7a40e46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "CopyflxBorderjomopayID0b6963ff7a40e46",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyflxBorderjomopayID0b6963ff7a40e46.setDefaultUnit(kony.flex.DP);
CopyflxBorderjomopayID0b6963ff7a40e46.add();
flxAliasJMPR.add(lblAliasJMPRstatic, flxAliasTextbox, CopyflxBorderjomopayID0b6963ff7a40e46);
var flxAliasHints = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxAliasHints",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {}, {});
flxAliasHints.setDefaultUnit(kony.flex.DP);
var lblAliasMinVal = new kony.ui.Label({
"id": "lblAliasMinVal",
"isVisible": true,
"right": "0dp",
"skin": "latoRegular24px",
"text": kony.i18n.getLocalizedString("i18n.JOMOPay.AliasMin"),
"textStyle": {},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAliasMaxVal = new kony.ui.Label({
"id": "lblAliasMaxVal",
"isVisible": true,
"right": "0dp",
"skin": "latoRegular24px",
"text": kony.i18n.getLocalizedString("i18n.JOMOPay.AliasStart"),
"textStyle": {},
"top": "3px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAliasNumVal = new kony.ui.Label({
"id": "lblAliasNumVal",
"isVisible": true,
"right": 0,
"skin": "latoRegular24px",
"text": kony.i18n.getLocalizedString("i18n.JOMOPay.Alias@"),
"textStyle": {},
"top": "3px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAliasCaseVal = new kony.ui.Label({
"id": "lblAliasCaseVal",
"isVisible": true,
"right": 0,
"skin": "latoRegular24px",
"text": kony.i18n.getLocalizedString("i18n.JOMOPay.AliasEnd"),
"textStyle": {},
"top": "3px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAliasHints.add(lblAliasMinVal, lblAliasMaxVal, lblAliasNumVal, lblAliasCaseVal);
var flxDateOfBirthEntery = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "12%",
"id": "flxDateOfBirthEntery",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "1%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxDateOfBirthEntery.setDefaultUnit(kony.flex.DP);
var lblDateOfBirthstatic = new kony.ui.Label({
"id": "lblDateOfBirthstatic",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.dateOfBirth"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDOBCalender = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"clipBounds": true,
"height": "55%",
"id": "flxDOBCalender",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDOBCalender.setDefaultUnit(kony.flex.DP);
var CopylblFilter0f7770a92cff247 = new kony.ui.Label({
"centerY": "45%",
"height": "100%",
"id": "CopylblFilter0f7770a92cff247",
"isVisible": true,
"left": "0%",
"skin": "CopyslLabel0b8f2bdd739144d",
"text": "T",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var calDateDOB = new kony.ui.Calendar({
"calendarIcon": "tran.png",
"dateFormat": "MM/dd/yyyy",
"focusSkin": "sknCarioRegular",
"height": "100%",
"id": "calDateDOB",
"isVisible": true,
"right": "0%",
"onSelection": AS_Calendar_e7dffb8d961e42bf8f4cf3cb6f458de7,
"skin": "sknCarioRegular",
"top": "0%",
"validStartDate": [7, 2, 2018],
"viewConfig": {
"gridConfig": {
"gridCellSkin": "Copycalender0h05b9ba7ebfb45",
"gridSkin": "Copycalender0c4c48cb6d2174e",
"leftNavigationImage": "directions_turn_left.png",
"rightNavigationImage": "directions_turn_right.png"
}
},
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxDOBCalender.add(CopylblFilter0f7770a92cff247, calDateDOB);
var CopyflxBorderjomopayID0e39bf89d640840 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "CopyflxBorderjomopayID0e39bf89d640840",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "1%",
"skin": "skntextFieldDividerJomoPay",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyflxBorderjomopayID0e39bf89d640840.setDefaultUnit(kony.flex.DP);
CopyflxBorderjomopayID0e39bf89d640840.add();
var lblDateOfBirth = new kony.ui.Label({
"id": "lblDateOfBirth",
"isVisible": true,
"right": "1%",
"skin": "lblAccountStaticText",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDateOfBirthEntery.add(lblDateOfBirthstatic, flxDOBCalender, CopyflxBorderjomopayID0e39bf89d640840, lblDateOfBirth);
var flxCountry = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxCountry",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCountry.setDefaultUnit(kony.flex.DP);
var flxStatictextCountry = new kony.ui.Label({
"id": "flxStatictextCountry",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.myprofile.Country"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxCountrySelect = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxCountrySelect",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "slFbox",
"top": "38%",
"width": "96%",
"zIndex": 1
}, {}, {});
flxCountrySelect.setDefaultUnit(kony.flex.DP);
var lblCountry = new kony.ui.Label({
"id": "lblCountry",
"isVisible": true,
"right": "1%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.jomopay.selectcountry"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "20%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnCountryDropDown = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"height": "100%",
"id": "btnCountryDropDown",
"isVisible": true,
"onClick": AS_Button_ed50bda53f5c46939f242f84319bc6f4,
"left": "7%",
"skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"text": "d",
"top": "2%",
"width": "93%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxCountrySelect.add(lblCountry, btnCountryDropDown);
var flxBorderCountry = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderCountry",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDividerGreen",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBorderCountry.setDefaultUnit(kony.flex.DP);
flxBorderCountry.add();
var lblCountryCode = new kony.ui.Label({
"height": "40%",
"id": "lblCountryCode",
"isVisible": false,
"right": "0%",
"skin": "slLabel",
"text": "code",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCountry.add(flxStatictextCountry, flxCountrySelect, flxBorderCountry, lblCountryCode);
var flxCity = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxCity",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCity.setDefaultUnit(kony.flex.DP);
var flxStaticTextCity = new kony.ui.Label({
"id": "flxStaticTextCity",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.myprofile.City"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxCitySelect = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxCitySelect",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "slFbox",
"top": "38%",
"width": "96%",
"zIndex": 1
}, {}, {});
flxCitySelect.setDefaultUnit(kony.flex.DP);
var lblCity = new kony.ui.Label({
"id": "lblCity",
"isVisible": true,
"right": "1%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.jomopay.selectcity"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "20%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnCityDropDown = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"height": "100%",
"id": "btnCityDropDown",
"isVisible": true,
"onClick": AS_Button_f5c20349799a48c29aa3aafe1ad1a254,
"left": "7%",
"skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"text": "d",
"top": "3%",
"width": "93%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxCitySelect.add(lblCity, btnCityDropDown);
var flxBordercity = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxBordercity",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDividerGreen",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBordercity.setDefaultUnit(kony.flex.DP);
flxBordercity.add();
var lblCityCode = new kony.ui.Label({
"height": "40%",
"id": "lblCityCode",
"isVisible": false,
"right": "0%",
"skin": "slLabel",
"text": "code",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCity.add(flxStaticTextCity, flxCitySelect, flxBordercity, lblCityCode);
var flxMobileNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "12%",
"id": "flxMobileNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "1%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxMobileNumber.setDefaultUnit(kony.flex.DP);
var lblMobileNumberText = new kony.ui.Label({
"id": "lblMobileNumberText",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.phonenumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxMobile = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"clipBounds": true,
"height": "56%",
"id": "flxMobile",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMobile.setDefaultUnit(kony.flex.DP);
var txtPhoneNo = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "100%",
"id": "txtPhoneNo",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "1%",
"maxTextLength": null,
"onDone": AS_TextField_d17c3a9fee8e4fbdb873091c71f32296,
"onTextChange": AS_TextField_j5a2c0f701e74a55a2e09d45df0f674b,
"onTouchEnd": AS_TextField_b49d4180ced845678b40437a79b5055e,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onBeginEditing": AS_TextField_c7c9b2f1ab154f789b19300379eca59e,
"onEndEditing": AS_TextField_b3f0311974154626b7004d99748a3822,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxMobile.add(txtPhoneNo);
var flxBorderMobileNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxBorderMobileNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxBorderMobileNumber.setDefaultUnit(kony.flex.DP);
flxBorderMobileNumber.add();
flxMobileNumber.add(lblMobileNumberText, flxMobile, flxBorderMobileNumber);
var lblMobileNumberHint = new kony.ui.Label({
"id": "lblMobileNumberHint",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobilenumberhint"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxEmail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "12%",
"id": "flxEmail",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "1%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxEmail.setDefaultUnit(kony.flex.DP);
var lblEmailStatic = new kony.ui.Label({
"id": "lblEmailStatic",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.email"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxEmailID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"clipBounds": true,
"height": "56%",
"id": "flxEmailID",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxEmailID.setDefaultUnit(kony.flex.DP);
var txtEmail = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "100%",
"id": "txtEmail",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"maxTextLength": 50,
"onDone": AS_TextField_ddc38f9c7bb34478bf01e951649a4705,
"onTextChange": AS_TextField_c759c0998e314bdabae4453aa2d4ec7d,
"onTouchEnd": AS_TextField_g1de9efdbeca4491af93da23ccc230d7,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onBeginEditing": AS_TextField_f776956ac6bc4746999272ecd9ad92e6,
"onEndEditing": AS_TextField_c68b2a0fbb734998a9dd5531c706e3d1,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxEmailID.add(txtEmail);
var flxBorderEmail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxBorderEmail",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxBorderEmail.setDefaultUnit(kony.flex.DP);
flxBorderEmail.add();
flxEmail.add(lblEmailStatic, flxEmailID, flxBorderEmail);
var lblLanguage = new kony.ui.Label({
"height": "0dp",
"id": "lblLanguage",
"isVisible": false,
"right": "200%",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblfromAccountCurrency = new kony.ui.Label({
"height": "0dp",
"id": "lblfromAccountCurrency",
"isVisible": false,
"right": "200%",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFromAccountNumber = new kony.ui.Label({
"height": "0dp",
"id": "lblFromAccountNumber",
"isVisible": false,
"right": "200%",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPayType = new kony.ui.Label({
"height": "0dp",
"id": "lblPayType",
"isVisible": false,
"right": "200%",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblInvalidCredentialsKA = new kony.ui.Label({
"centerX": "50%",
"id": "lblInvalidCredentialsKA",
"isVisible": false,
"skin": "sknInvalidCredKA",
"text": kony.i18n.getLocalizedString("i18n.Bene.EmailError"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCustType = new kony.ui.Label({
"height": "0dp",
"id": "lblCustType",
"isVisible": false,
"right": "200%",
"skin": "slLabel",
"text": "150",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxRegBody.add(flxNationalID, flxIDType, flxAccount, flxJoMoPayType, flxJomopayID, lblMobileIDHint, flxAliasJMPR, flxAliasHints, flxDateOfBirthEntery, flxCountry, flxCity, flxMobileNumber, lblMobileNumberHint, flxEmail, lblLanguage, lblfromAccountCurrency, lblFromAccountNumber, lblPayType, lblInvalidCredentialsKA, lblCustType);
flxBodyReg.add(flxRegHeader, flxRegBody);
var flxConfirmPopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxConfirmPopup",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "CopyslFbox0b17aedfc0b4841",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmPopup.setDefaultUnit(kony.flex.DP);
var flxConfirmHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxConfirmHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFlxHeaderImg",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmHeader.setDefaultUnit(kony.flex.DP);
var flxHeaderBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxHeaderBack",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "2%",
"onClick": AS_FlexContainer_e643c3b49f1b44b5b818214902d021c8,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxHeaderBack.setDefaultUnit(kony.flex.DP);
var lblHeaderBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBackIcon",
"isVisible": true,
"left": "0%",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblHeaderBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBack",
"isVisible": true,
"left": "30%",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeaderBack.add(lblHeaderBackIcon, lblHeaderBack);
var lblHeaderTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblHeaderTitle",
"isVisible": true,
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnClose = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknDeleteFocus",
"height": "80%",
"id": "btnClose",
"isVisible": false,
"left": "85%",
"skin": "sknBtnBack",
"text": "O",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxConfirmHeader.add(flxHeaderBack, lblHeaderTitle, btnClose);
var flxDetails = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "81%",
"horizontalScrollIndicator": true,
"id": "flxDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "9%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxDetails.setDefaultUnit(kony.flex.DP);
var flxNationalIDConfirm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14%",
"id": "flxNationalIDConfirm",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNationalIDConfirm.setDefaultUnit(kony.flex.DP);
var lblStaticNationalID = new kony.ui.Label({
"id": "lblStaticNationalID",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.nationalid"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblNationalIDConfirm = new kony.ui.Label({
"id": "lblNationalIDConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxNationalIDConfirm.add(lblStaticNationalID, lblNationalIDConfirm);
var flxIDTypeConfirm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14%",
"id": "flxIDTypeConfirm",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0.00%",
"skin": "slFbox",
"top": "12%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIDTypeConfirm.setDefaultUnit(kony.flex.DP);
var lblStaticIDType = new kony.ui.Label({
"id": "lblStaticIDType",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.idtype"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblIDTypeConfirm = new kony.ui.Label({
"id": "lblIDTypeConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxIDTypeConfirm.add(lblStaticIDType, lblIDTypeConfirm);
var flxPhoneConfirm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14%",
"id": "flxPhoneConfirm",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0.00%",
"skin": "slFbox",
"top": "62%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPhoneConfirm.setDefaultUnit(kony.flex.DP);
var lblstaticPhone = new kony.ui.Label({
"id": "lblstaticPhone",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.phoneNumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPhoneConfirm = new kony.ui.Label({
"id": "lblPhoneConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxPhoneConfirm.add(lblstaticPhone, lblPhoneConfirm);
var flxEmailConfirm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14%",
"id": "flxEmailConfirm",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0.00%",
"skin": "slFbox",
"top": "72%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxEmailConfirm.setDefaultUnit(kony.flex.DP);
var lblStaticEmail = new kony.ui.Label({
"id": "lblStaticEmail",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.email"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblEmailConfirm = new kony.ui.Label({
"id": "lblEmailConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxEmailConfirm.add(lblStaticEmail, lblEmailConfirm);
var flxJomopayIDConfirm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14%",
"id": "flxJomopayIDConfirm",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "12%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJomopayIDConfirm.setDefaultUnit(kony.flex.DP);
var lblStaticJomopayID = new kony.ui.Label({
"id": "lblStaticJomopayID",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.jomopayID"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblJomopayIDConfirm = new kony.ui.Label({
"id": "lblJomopayIDConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxJomopayIDConfirm.add(lblStaticJomopayID, lblJomopayIDConfirm);
var flxAlias = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14%",
"id": "flxAlias",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "22%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAlias.setDefaultUnit(kony.flex.DP);
var lblAlias = new kony.ui.Label({
"id": "lblAlias",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.aliastype"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblJMPAlias = new kony.ui.Label({
"id": "lblJMPAlias",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAlias.add(lblAlias, lblJMPAlias);
var flxJomopayTypeConfirm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14%",
"id": "flxJomopayTypeConfirm",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "52%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJomopayTypeConfirm.setDefaultUnit(kony.flex.DP);
var lblStaticJomoPayType = new kony.ui.Label({
"id": "lblStaticJomoPayType",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.jomopaytype"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblJomopayTypeConfirm = new kony.ui.Label({
"id": "lblJomopayTypeConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxJomopayTypeConfirm.add(lblStaticJomoPayType, lblJomopayTypeConfirm);
var flxDateOfBirth = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14%",
"id": "flxDateOfBirth",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "32%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDateOfBirth.setDefaultUnit(kony.flex.DP);
var lblDOBTitle = new kony.ui.Label({
"id": "lblDOBTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.dateOfBirth"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDOB = new kony.ui.Label({
"id": "lblDOB",
"isVisible": false,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDOBDummy = new kony.ui.Label({
"id": "lblDOBDummy",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDateOfBirth.add(lblDOBTitle, lblDOB, lblDOBDummy);
var flxCountryConfirm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14%",
"id": "flxCountryConfirm",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "42%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCountryConfirm.setDefaultUnit(kony.flex.DP);
var lblStaticCountry = new kony.ui.Label({
"id": "lblStaticCountry",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.myprofile.Country"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCountryConfirm = new kony.ui.Label({
"id": "lblCountryConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCountryCodeConfirm = new kony.ui.Label({
"id": "lblCountryCodeConfirm",
"isVisible": false,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCountryConfirm.add(lblStaticCountry, lblCountryConfirm, lblCountryCodeConfirm);
var flxCityConfirm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14%",
"id": "flxCityConfirm",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "52%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCityConfirm.setDefaultUnit(kony.flex.DP);
var lblStaticCity = new kony.ui.Label({
"id": "lblStaticCity",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.myprofile.City"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCityConfirm = new kony.ui.Label({
"id": "lblCityConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCityCodeConfirm = new kony.ui.Label({
"id": "lblCityCodeConfirm",
"isVisible": false,
"right": "5%",
"skin": "skncardconfirm",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCityConfirm.add(lblStaticCity, lblCityConfirm, lblCityCodeConfirm);
flxDetails.add(flxNationalIDConfirm, flxIDTypeConfirm, flxPhoneConfirm, flxEmailConfirm, flxJomopayIDConfirm, flxAlias, flxJomopayTypeConfirm, flxDateOfBirth, flxCountryConfirm, flxCityConfirm);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhite",
"height": "8%",
"id": "btnConfirm",
"isVisible": true,
"onClick": AS_Button_b69d381620454245959a43e0016ea246,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
"top": "90%",
"width": "80%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblTypeTrn = new kony.ui.Label({
"id": "lblTypeTrn",
"isVisible": false,
"right": "0dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "583dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxConfirmPopup.add(flxConfirmHeader, flxDetails, btnConfirm, lblTypeTrn);
frmJoMoPayRegistration.add(flxBodyReg, flxConfirmPopup);
};
function frmJoMoPayRegistrationGlobalsAr() {
frmJoMoPayRegistrationAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmJoMoPayRegistrationAr,
"enabledForIdleTimeout": true,
"id": "frmJoMoPayRegistration",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_hda449db0e5d434b84e1ae59158487cc,
"preShow": AS_Form_f1f4eeca8d6544ce8a6daea98cc07f8b,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_e26b285a21c2468a9c0e43de55e12450,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
