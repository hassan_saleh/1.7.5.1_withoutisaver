//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:35 EET 2020
function addWidgetsfrmDeviceRegistrationOptionsKAAr() {
    frmDeviceRegistrationOptionsKA.setDefaultUnit(kony.flex.DP);
    var touchFeature = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "touchFeature",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknslFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    touchFeature.setDefaultUnit(kony.flex.DP);
    var FlexContainer00a3c07fadcbf4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "85%",
        "id": "FlexContainer00a3c07fadcbf4b",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0%",
        "skin": "sknslFbox",
        "top": "3%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer00a3c07fadcbf4b.setDefaultUnit(kony.flex.DP);
    var CopyLabel0d00f815bcc7c41 = new kony.ui.Label({
        "id": "CopyLabel0d00f815bcc7c41",
        "isVisible": true,
        "right": "5%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.DeviceReg.MssgAndroid"),
        "top": "5%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRegistrationProfileMessage = new kony.ui.Label({
        "id": "lblRegistrationProfileMessage",
        "isVisible": true,
        "right": "5%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.iwatch.registermessage"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Label07b268452752c4c = new kony.ui.Label({
        "id": "Label07b268452752c4c",
        "isVisible": true,
        "right": "5%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.DeviceReg,Question"),
        "top": "6%",
        "width": "90%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var FlexContainer0cacdfd0ae7294e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "19%",
        "id": "FlexContainer0cacdfd0ae7294e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "6%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0cacdfd0ae7294e.setDefaultUnit(kony.flex.DP);
    var enableTouchID = new kony.ui.Button({
        "focusSkin": "slButtonGreenFocus",
        "height": "35%",
        "id": "enableTouchID",
        "isVisible": true,
        "right": "6%",
        "onClick": AS_Button_df94a69747984767a235f83f6cad5cef,
        "skin": "CopyslButtonGreen0h4fc8a66e86e48",
        "text": kony.i18n.getLocalizedString("i18n.login.cRegisterDevice"),
        "top": "0%",
        "width": "38%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var noThanks = new kony.ui.Button({
        "focusSkin": "slButtonBlueFocus",
        "height": "35%",
        "id": "noThanks",
        "isVisible": true,
        "right": "56%",
        "onClick": AS_Button_e4cdf6e3abac4b41a3cd06c81905d337,
        "skin": "sknbuttonblue",
        "text": kony.i18n.getLocalizedString("i18n.common.noThanks"),
        "top": "0%",
        "width": "36%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer0cacdfd0ae7294e.add(enableTouchID, noThanks);
    FlexContainer00a3c07fadcbf4b.add(CopyLabel0d00f815bcc7c41, lblRegistrationProfileMessage, Label07b268452752c4c, FlexContainer0cacdfd0ae7294e);
    var lblAppnotification = new kony.ui.Label({
        "height": "12%",
        "id": "lblAppnotification",
        "isVisible": false,
        "right": "5%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.common.devregNote"),
        "top": "88%",
        "width": "90%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    touchFeature.add(FlexContainer00a3c07fadcbf4b, lblAppnotification);
    frmDeviceRegistrationOptionsKA.add(touchFeature);
};
function frmDeviceRegistrationOptionsKAGlobalsAr() {
    frmDeviceRegistrationOptionsKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmDeviceRegistrationOptionsKAAr,
        "bounces": false,
        "enableScrolling": true,
        "enabledForIdleTimeout": true,
        "id": "frmDeviceRegistrationOptionsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "pagingEnabled": false,
        "preShow": AS_Form_f73604e1824b44dbab530d4b72db6950,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_85cedac0442743ffb1f2e2d60ab856b1,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
