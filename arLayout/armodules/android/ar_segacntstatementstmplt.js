//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:36 EET 2020
function initializesegacntstatementstmpltAr() {
    CopyFlexContainer035bb5165f25443Ar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "CopyFlexContainer035bb5165f25443",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    CopyFlexContainer035bb5165f25443Ar.setDefaultUnit(kony.flex.DP);
    var transactionDate = new kony.ui.Label({
        "centerY": "33.64%",
        "height": "20dp",
        "id": "transactionDate",
        "isVisible": true,
        "right": "5%",
        "skin": "sknRegisterMobileBank",
        "top": "0dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionName = new kony.ui.Label({
        "centerY": "66.67%",
        "height": "20dp",
        "id": "transactionName",
        "isVisible": true,
        "right": "5%",
        "skin": "skn383838LatoRegular107KA",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "left": 0,
        "skin": "skntextFieldDivider",
        "top": "59dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    var lblStstementLinkKA = new kony.ui.Label({
        "id": "lblStstementLinkKA",
        "isVisible": false,
        "right": "250dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "14dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer035bb5165f25443Ar.add(transactionDate, transactionName, contactListDivider, lblStstementLinkKA);
}
