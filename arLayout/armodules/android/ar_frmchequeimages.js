//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:35 EET 2020
function addWidgetsfrmchequeimagesAr() {
    frmchequeimages.setDefaultUnit(kony.flex.DP);
    var titleBarWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarWrapper.setDefaultUnit(kony.flex.DP);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var androidTitleLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "androidTitleLabel",
        "isVisible": true,
        "right": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.deposit.chequeImages"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var androidBack = new kony.ui.Button({
        "focusSkin": "sknandroidBackButtonFocus",
        "height": "50dp",
        "id": "androidBack",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_4657c0c6ad4f4fe68af07e009c768fbf,
        "skin": "sknandroidBackButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    androidTitleBar.add(androidTitleLabel, androidBack);
    titleBarWrapper.add(androidTitleBar);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkgGray",
        "top": "50dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var searchSegmentedControllerWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "56dp",
        "id": "searchSegmentedControllerWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    searchSegmentedControllerWrapper.setDefaultUnit(kony.flex.DP);
    var androidSegmentedController = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "56dp",
        "id": "androidSegmentedController",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidSegmentedController.setDefaultUnit(kony.flex.DP);
    var btnfront = new kony.ui.Button({
        "focusSkin": "sknaccountFilterButtonFocus",
        "height": "52dp",
        "id": "btnfront",
        "isVisible": true,
        "right": "0%",
        "onClick": AS_Button_f3d583bcb88c43e8a4bb4c9bb2494649,
        "skin": "sknandroidSegmentedTextActive",
        "text": kony.i18n.getLocalizedString("i18n.deposit.btnFront"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 6, 0,6, 0],
        "paddingInPixel": false
    }, {});
    var btnback = new kony.ui.Button({
        "focusSkin": "sknaccountFilterButtonFocus",
        "height": "52dp",
        "id": "btnback",
        "isVisible": true,
        "right": "50%",
        "onClick": AS_Button_b5961301ccb441dab687cc27bd8f94b1,
        "skin": "sknandroidSegmentedTextInactive",
        "text": kony.i18n.getLocalizedString("i18n.deposit.btnBack"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 6, 0,6, 0],
        "paddingInPixel": false
    }, {});
    var flxdivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "5dp",
        "clipBounds": true,
        "height": "5dp",
        "id": "flxdivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknCopyslFbox031e643cbc0cd4b",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flxdivider.setDefaultUnit(kony.flex.DP);
    flxdivider.add();
    androidSegmentedController.add(btnfront, btnback, flxdivider);
    searchSegmentedControllerWrapper.add(androidSegmentedController);
    var FlexContainer04824959abde740 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "80%",
        "id": "FlexContainer04824959abde740",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "30dp",
        "left": "0dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    FlexContainer04824959abde740.setDefaultUnit(kony.flex.DP);
    var frontImage = new kony.ui.Image2({
        "height": "100%",
        "id": "frontImage",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "check_front.png",
        "top": "0dp",
        "width": "100%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var backImage = new kony.ui.Image2({
        "height": "100%",
        "id": "backImage",
        "isVisible": false,
        "right": "0dp",
        "skin": "slImage",
        "src": "check_front.png",
        "top": "0dp",
        "width": "100%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer04824959abde740.add(frontImage, backImage);
    mainContent.add(searchSegmentedControllerWrapper, FlexContainer04824959abde740);
    frmchequeimages.add(titleBarWrapper, mainContent);
};
function frmchequeimagesGlobalsAr() {
    frmchequeimagesAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmchequeimagesAr,
        "allowHorizontalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmchequeimages",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 2
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_813585104d884100aa1038e9d4b5a004,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
