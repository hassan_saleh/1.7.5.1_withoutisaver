//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:36 EET 2020
function addWidgetspopupStopCardAr() {
var CopyHBxTitle = new kony.ui.Box({
"id": "CopyHBxTitle",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "sknWhiteBGHBX"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var lblTitle = new kony.ui.Label({
"id": "lblTitle",
"isVisible": true,
"skin": "sknlblblue",
"text": "Stop Card",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
}
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"hExpand": true,
"margin": [ 1, 3,40, 3],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false
});
CopyHBxTitle.add(lblTitle);
var HBxTitle = new kony.ui.Box({
"id": "HBxTitle",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 2,0, 0],
"marginInPixel": false,
"padding": [ 0, 1,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var lblStopCardDesc = new kony.ui.Label({
"id": "lblStopCardDesc",
"isVisible": true,
"skin": "sknlblblue",
"text": kony.i18n.getLocalizedString("i18n.stopCard.Activate"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
}
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"hExpand": true,
"margin": [ 0, 2,0, 6],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false
});
HBxTitle.add(lblStopCardDesc);
var HBxCardCancelReason = new kony.ui.Box({
"id": "HBxCardCancelReason",
"isVisible": false,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "sknHbxDropdown"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 7, 6,7, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var lstbxCardStoplReason = new kony.ui.ListBox({
"focusSkin": "slListBoxNew",
"id": "lstbxCardStoplReason",
"isVisible": true,
"masterData": [["lb1", "Select reason"],["lb2", "Card cancel reason1"],["Key494", "Card cancel reason1"]],
"selectedKey": "lb1",
"selectedKeyValue": ["lb1", "Select reason"],
"skin": "slListBoxNew"
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"hExpand": true,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"applySkinsToPopup": true,
"dropDownImage": "dropdownlist.png",
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
HBxCardCancelReason.add(lstbxCardStoplReason);
var HBxSubmit = new kony.ui.Box({
"id": "HBxSubmit",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 3, 5,3, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var btnYes = new kony.ui.Button({
"id": "btnYes",
"isVisible": true,
"onClick": AS_Button_b978bc73031f4deab8675fa0ce6925c4,
"skin": "sknCancleBtn",
"text": kony.i18n.getLocalizedString("i18n.common.YES")
}, {
"containerWeight": 50,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"hExpand": true,
"margin": [ 6, 4,6, 6],
"marginInPixel": false,
"padding": [ 3, 3,3, 3],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
var btnNo = new kony.ui.Button({
"id": "btnNo",
"isVisible": true,
"onClick": AS_Button_c322c2802fc34c988af7498ffb6d3c56,
"skin": "sknCancleBtn",
"text": kony.i18n.getLocalizedString("i18n.common.NO")
}, {
"containerWeight": 50,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"hExpand": true,
"margin": [ 6, 4,6, 6],
"marginInPixel": false,
"padding": [ 3, 3,3, 3],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
HBxSubmit.add( btnNo,btnYes);
popupStopCard.add(CopyHBxTitle, HBxTitle, HBxCardCancelReason, HBxSubmit);
};
function popupStopCardGlobalsAr() {
popupStopCardAr = new kony.ui.Popup({
"addWidgets": addWidgetspopupStopCardAr,
"id": "popupStopCard",
"isModal": true,
"skin": "sknWTBGWTBRDRND",
"transparencyBehindThePopup": 25
}, {
"containerWeight": 90,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"windowSoftInputMode": constants.POPUP_ADJUST_PAN
});
};
