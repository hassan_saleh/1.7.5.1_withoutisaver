//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:36 EET 2020
function initializetransactionTemplateColorAr() {
    CopyFlexContainer047db9e4d3aaf43Ar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "CopyFlexContainer047db9e4d3aaf43",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    CopyFlexContainer047db9e4d3aaf43Ar.setDefaultUnit(kony.flex.DP);
    var lblAccountTypeKA = new kony.ui.Label({
        "centerY": "50%",
        "height": "44dp",
        "id": "lblAccountTypeKA",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknAccountTypesLblColor",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": "6dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountType = new kony.ui.Label({
        "centerY": "34%",
        "height": "20dp",
        "id": "lblAccountType",
        "isVisible": false,
        "left": "26dp",
        "skin": "sknRegisterMobileBank",
        "width": "35%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionAmount = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "transactionAmount",
        "isVisible": true,
        "left": "26dp",
        "skin": "skn383838LatoRegular107KA",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSepKA = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblSepKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLineEDEDEDKA",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var AccountTypeKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "44dp",
        "id": "AccountTypeKA",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "6dp",
        "zIndex": 1
    }, {}, {});
    AccountTypeKA.setDefaultUnit(kony.flex.DP);
    AccountTypeKA.add();
    var FlexContainer093a8eea8c55f4a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerY": "63%",
        "clipBounds": true,
        "id": "FlexContainer093a8eea8c55f4a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "slFbox",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    FlexContainer093a8eea8c55f4a.setDefaultUnit(kony.flex.DP);
    var ImgTransactionFail = new kony.ui.Image2({
        "height": "20dp",
        "id": "ImgTransactionFail",
        "isVisible": false,
        "right": "0dp",
        "skin": "slImage",
        "src": "failedimage.png",
        "top": "0dp",
        "width": "20dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var transactionName = new kony.ui.Label({
        "id": "transactionName",
        "isVisible": true,
        "right": "20dp",
        "maxNumberOfLines": 2,
        "skin": "skn383838LatoRegular107KA",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer093a8eea8c55f4a.add(ImgTransactionFail, transactionName);
    var FlexContainer0c2ca3ef38dbd43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "28%",
        "clipBounds": true,
        "height": "20dp",
        "id": "FlexContainer0c2ca3ef38dbd43",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "slFbox",
        "width": "65%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0c2ca3ef38dbd43.setDefaultUnit(kony.flex.DP);
    var ImgRecurrence = new kony.ui.Image2({
        "centerY": "50%",
        "height": "100%",
        "id": "ImgRecurrence",
        "isVisible": false,
        "right": "0dp",
        "skin": "slImage",
        "src": "recuurencebox.png",
        "top": "0dp",
        "width": "20dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var ImgWithDraw = new kony.ui.Image2({
        "centerY": "60%",
        "height": "100%",
        "id": "ImgWithDraw",
        "isVisible": false,
        "right": "0dp",
        "skin": "slImage",
        "src": "pending.png",
        "top": "0dp",
        "width": "20dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var transactionDate = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "transactionDate",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknRegisterMobileBank",
        "width": "83%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblExpiryTime = new kony.ui.Label({
        "id": "lblExpiryTime",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknErrorMessageEC223BKA",
        "text": "21h:22m",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0c2ca3ef38dbd43.add(ImgRecurrence, ImgWithDraw, transactionDate, lblExpiryTime);
    var lblNotesKA = new kony.ui.Label({
        "id": "lblNotesKA",
        "isVisible": false,
        "right": "200dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "17dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var cashlessPhone = new kony.ui.Label({
        "id": "cashlessPhone",
        "isVisible": false,
        "right": "210dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "27dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var cashlessEmail = new kony.ui.Label({
        "id": "cashlessEmail",
        "isVisible": false,
        "right": "220dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "37dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var cashlessPersonName = new kony.ui.Label({
        "id": "cashlessPersonName",
        "isVisible": false,
        "right": "230dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "47dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var fromAccountName = new kony.ui.Label({
        "id": "fromAccountName",
        "isVisible": false,
        "right": "210dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "27dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionID = new kony.ui.Label({
        "id": "transactionID",
        "isVisible": false,
        "right": "220dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "37dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer047db9e4d3aaf43Ar.add(lblAccountTypeKA, lblAccountType, transactionAmount, lblSepKA, AccountTypeKA, FlexContainer093a8eea8c55f4a, FlexContainer0c2ca3ef38dbd43, lblNotesKA, cashlessPhone, cashlessEmail, cashlessPersonName, fromAccountName, transactionID);
}
