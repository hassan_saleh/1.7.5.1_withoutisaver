//Type your code here

function PreCallFrmRequestPinCard(){
  /*frmRequestPinCard.lblCircle1.skin ="sknlblBojFont2sTARiCON";
  frmRequestPinCard.lblCircle2.skin ="sknlblBojFont2sTARiCON";
  frmRequestPinCard.lblCircle3.skin ="sknlblBojFont2sTARiCON";
  frmRequestPinCard.lblCircle4.skin ="sknlblBojFont2sTARiCON";*/
  //frmRequestPinCard.txtPIN.text = "";
  frmRequestPinCard.flxInvalidAmountField.setVisibility(false);
  frmRequestPinCard.lblCurrHead.setVisibility(false);
  frmRequestPinCard.lblBranch.text = geti18Value("i18n.cards.reasonhead3");
  frmRequestPinCard.show();
}





function onTextChangeRequestPINCard() {
  var len = frmRequestPinCard.txtPIN.text.length;
  kony.print("frmRequestPinCard.txtPIN.text.length :: " + frmRequestPinCard.txtPIN.text.length);
  var langSelected = kony.store.getItem("langPrefObj");

  if (langSelected == "en") {

    if (len === 0) {
      frmRequestPinCard.lblCircle1.skin = "sknlblBojFont2sTARiCON";

    } else if (len == 1) {
      frmRequestPinCard.lblCircle1.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle2.skin = "sknlblBojFont2sTARiCON";
    } else if (len == 2) {
      frmRequestPinCard.lblCircle1.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle2.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle3.skin = "sknlblBojFont2sTARiCON";
    } else if (len == 3) {
      frmRequestPinCard.lblCircle1.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle2.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle3.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle4.skin = "sknlblBojFont2sTARiCON";
    } else if (len == 4) {
      frmRequestPinCard.txtPIN.setFocus = false;
      frmRequestPinCard.lblCircle1.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle2.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle3.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle4.skin = "sknlblBojFont2sTARiCONActive";
    }
  } else {
    if (len === 0) {
      frmRequestPinCard.lblCircle4.skin = "sknlblBojFont2sTARiCON";
    } else if (len == 1) {
      frmRequestPinCard.lblCircle4.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle3.skin = "sknlblBojFont2sTARiCON";
    } else if (len == 2) {
      frmRequestPinCard.lblCircle4.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle3.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle2.skin = "sknlblBojFont2sTARiCON";
    } else if (len == 3) {
      frmRequestPinCard.lblCircle4.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle3.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle2.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle1.skin = "sknlblBojFont2sTARiCON";
    } else if (len == 4) {
      frmRequestPinCard.txtPIN.setFocus = false;
      frmRequestPinCard.lblCircle4.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle3.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle2.skin = "sknlblBojFont2sTARiCONActive";
      frmRequestPinCard.lblCircle1.skin = "sknlblBojFont2sTARiCONActive";
    }
  }
}



function setDatatoRequestNewPinBranchList(selectedItem){
   frmRequestPinCard.lblBranch.text = selectedItem.BRANCH_NAME;
  frmRequestPinCard.lblBranchCode.text = selectedItem.BRANCH_CODE;
  frmRequestPinCard.lblCurrHead.setVisibility(true);
  frmRequestPinCard.lblLine1.skin = "sknFlxGreenLine";
  if(frmRequestPinCard.lblBranch.text != geti18Value("i18n.cards.reasonhead3")){
     frmRequestPinCard.flxInvalidAmountField.setVisibility(false);
  }else{
     frmRequestPinCard.flxInvalidAmountField.setVisibility(true);
  }
  frmRequestPinCard.show();
 }



function confirmCallrequestNewPinService(){
  kony.print("confirmCallrequestNewPinService::");
  if(kony.sdk.isNetworkAvailable()){
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };


    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Cards", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Cards"); 
    dataObject.addField("custId",custid);
    
    var acctno = "", card_num = "";
    var p_acc_br = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num;
    for(i=0;i<kony.retailBanking.globalData.prCardLandinList.length;i++){
    if(p_acc_br === kony.retailBanking.globalData.prCardLandinList[i].card_num){
      acctno = isEmpty(kony.retailBanking.globalData.prCardLandinList[i].card_acc_num) ? "" : kony.retailBanking.globalData.prCardLandinList[i].card_acc_num;
     break;
    }
    }
    dataObject.addField("p_acc_br","1");
    dataObject.addField("acctno",acctno);
    card_num = CARDLIST_DETAILS.cardNumber;
    dataObject.addField("CardNum",card_num.substring(0,6)+"******"+card_num.substring(card_num.length-4,card_num.length));
    dataObject.addField("p_delv_br",frmRequestPinCard.lblBranchCode.text);
    dataObject.addField("p_channel","MOBILE");
    dataObject.addField("userId","BOJMOB");


    var serviceOptions = {"dataObject":dataObject,"headers":headers};

    kony.print("confirmCallrequestNewPinService params-->"+ JSON.stringify(serviceOptions) );
    modelObj.customVerb("cvDcReqNewPin",serviceOptions, confirmCallrequestNewPinServiceSuccessCallback, confirmCallrequestNewPinServiceErrorCallback);
  }else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}

function confirmCallrequestNewPinServiceSuccessCallback(response){
  kony.application.dismissLoadingScreen();
  gblRequestNewPinBranchList=false;
  if(!isEmpty(response) && response.opstatus === 0 && response.ErrorCode === "00000")
  {
    var reference = "";
    if(!isEmpty(response.referenceId))
    reference = geti18nkey("i18n.common.ReferenceNumber")+" " + response.referenceId;
         
  kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"),reference,geti18nkey("i18n.cards.gotoCards"), "CardsLanding", geti18Value("i18n.Bene.GotoAccDash"),geti18nkey("i18n.common.AccountDashboard"),"","",geti18Value("i18n.reqNewPinDescc"));

    
  } else{    
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
    exceptionLogCall("callConfirmCreateNewSubAccountSuccessCallback","Service ERROR","Service","opstatus other than 0");
    kony.application.dismissLoadingScreen();
  }
  
  /*else if(!isEmpty(response) &&  response.ErrorCode === "00500")
  {
    customAlertPopup("", geti18Value("i18n.requestNewPin.already"),popupCommonAlertDimiss, "");
    kony.application.dismissLoadingScreen();
  }*/
}


function confirmCallrequestNewPinServiceErrorCallback(response)
{
  gblRequestNewPinBranchList=false;
  kony.print("callConfirmCreateNewSubAccountErrorCallback -----"+JSON.stringify(response));
  customAlertPopup(geti18Value("i18n.NUO.Error"),  geti18Value("i18n.common.defaultErrorMsg"),popupCommonAlertDimiss, "");
  kony.application.dismissLoadingScreen();
    
}
