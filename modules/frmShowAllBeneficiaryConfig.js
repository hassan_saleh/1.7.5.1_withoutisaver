var frmShowAllBeneficiaryConfig= {
  "formid": "frmShowAllBeneficiary",
  "frmShowAllBeneficiary": {
      "entity": "ExternalAccounts",
      "objectServiceName": "RBObjects",
      "objectServiceOptions" : {"access":"online"},
  },
 "segAllBeneficiary": {
      "fieldprops": {
          "widgettype": "Segment",
          "entity": "ExternalAccounts",
          "additionalFields": ["benBranchNo","benAcctCurrency","isFav","benType"],
          "field": {
              "BenificiaryFullName": {
                  "widgettype": "Label",
                  "field": "beneficiaryName"
              },
               "BenificiaryName": {
                  "widgettype": "Label",
                  "field": "nickName"
              },
            "accountNumber":{
                "widgettype": "Label",
                  "field": "benAcctNo"
            }
         }
      }
  }
};