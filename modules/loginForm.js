var swipeGesture;
var swipeGesture1;
var myalert;

userAgent = kony.os.userAgent();

var setupTblTap = {
    fingers: 1,
    swipedistance: 50,
    swipevelocity: 60
}; //swipe gesture

function loginPreShow() {
  sendUnamecase = false;
  if (!isLoggedIn) {
//     fetchApplicationPropertiesWithoutLoading();
    fetchApplicationProperties();
  }
  var currentAppVersion = parseFloat(appConfig.appVersion);
  frmLoginKA.skin = "sknLoginMainPage";
  setUpLoginScreen();
}

function savePassword(password) {
    kony.print("Save Password");
}

function touchLoginShow() {
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  deviceRegFrom = "login";
  frmLoginKA.loginMainPINScreen.setVisibility(false);
  if(kony.boj.retailBanking.globals.Maintenance_Flag !== "Y"){
    if (kony.retailBanking.globalData.deviceInfo.isIphone()) {
      var config = {
        "promptMessage": kony.i18n.getLocalizedString("i18n.touchIdMsg"),
        "fallbackTitle": ""
      };
      kony.localAuthentication.authenticate(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID, statusCB, config);
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } else{
      showTouchIdAndroid();
    }
  }
}

function showTouchIdAndroid() {
    frmLoginKA.loginMainScreen.opacity = 0.2;
    frmLoginKA.loginMainScreen.setVisibility(false);
    frmLoginKA.loginMainScreen.setEnabled(false);
    frmLoginKA.flxtouchIdAndrd.setVisibility(true);
    frmLoginKA.flxAndrdTouchAlert.setVisibility(true);
    frmLoginKA.flxTouchIdtryAgain.setVisibility(false);
    var config = {};
    kony.localAuthentication.authenticate(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID, authCallBack, config);
  	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function authCallBack(resStatus, msg) {
    if (resStatus == 5000) {
        kony.print("Success touch id");
        kony.sdk.mvvm.LoginAction("", "", "T", custid, "", "T");

        frmLoginKA.lblTryAgain.text = kony.i18n.getLocalizedString("i18n.touchId.tryAgain");
        frmLoginKA.flxTouchIdtryAgain.setVisibility(false);

    } else if (resStatus == 5001) {
        kony.print("failed touch id");

        frmLoginKA.lblTryAgain.text = kony.i18n.getLocalizedString("i18n.touchId.tryAgain");
        frmLoginKA.flxTouchIdtryAgain.setVisibility(true);
        kony.sdk.mvvm.LoginAction("", "", "T", custid, "", "F");

    } else {
        kony.localAuthentication.cancelAuthentication();
        frmLoginKA.loginMainScreen.opacity = 1;
        frmLoginKA.loginMainPINScreen.opacity = 1;
        frmLoginKA.flxtouchIdAndrd.setVisibility(false);
        frmLoginKA.skin = "sknLoginMainPage";
        frmLoginKA.loginMainScreen.setVisibility(true);
        frmLoginKA.loginMainScreen.setEnabled(true);
        frmLoginKA.loginMainPINScreen.setEnabled(true);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }
}

function alertChangeUsername() {
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "Change User",
        "yesLabel": "Change",
        "noLabel": i18n_cancel,
        "message": "Do you want to change the User?",
        "alertHandler": switchUser
    }, {});
}

function switchUser(response) {
    if (response === true) {
        clearUserData();
        frmLoginKA.tbxusernameTextField.text = "";
        frmLoginKA.passwordTextField.text = "";
    	frmLoginKA.passwordTextField.secureTextEntry = true;
    	frmLoginKA.lblShowPass.text = "A";
        frmLoginKA.lblChangeUserName.text = "";
        frmLoginKA.tbxusernameTextField.setVisibility(true);
        //frmLoginKA.noThanks.setVisibility(false);
        frmLoginKA.lblChangeUserName.setVisibility(false);
        //frmLoginKA.touchIdContainer.setVisibility(false);
        //frmLoginKA.flxPinLoginFeature.setVisibility(false);
        //frmLoginKA.flxFacialAuthFeature.setVisibility(false);
    }
}

function clearUserData() {
    kony.store.setItem("userName", "");
    kony.store.setItem("credPassword", "");
    kony.store.setItem("firstTimeLogin", "");
    if (isSettingsFlagEnabled("isFaceEnrolled")) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        intializeFacialAuth();
        FaceAuth_initialize3();
    }
    kony.store.setItem("settingsflagsObject", kony.retailBanking.globalData.globals.settings);
}

function statusCB(status, message) {
    if (status == 5000) {
        kony.print("Success touch id");
        kony.sdk.mvvm.LoginAction("", "", "T", custid, "", "T");
    } else {
        switch (status) {
            case 5001:
                kony.print("Fail touch id");
                kony.sdk.mvvm.LoginAction("", "", "T", custid, "", "F");
                break;
            default:
                /*kony.localAuthentication.cancelAuthentication();
                frmLoginKA.loginMainScreen.opacity = 1;
                frmLoginKA.loginMainPINScreen.opacity = 1;
                frmLoginKA.flxtouchIdAndrd.setVisibility(false);
                frmLoginKA.loginMainScreen.setVisibility(true);
                frmLoginKA.loginMainScreen.setEnabled(true);
                frmLoginKA.loginMainPINScreen.setEnabled(true); */
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                break;
        }
        kony.print("status in statusCB-- > " + status);
    }
}

function loginPostShow() {

    animateLabel("DOWN", "lblUserName", frmLoginKA.tbxusernameTextField.text);
    animateLabel("DOWN", "lblPassword", frmLoginKA.passwordTextField.text);
    gblCardReorder = false;
    gblCardAddNickname = false;
    animateLoginFlex();
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.init();
      	appintKPNSCallback();
    } else {
      if(engOTPmsg === "" || engOTPmsg === null){
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.common.appalaunchnointernet"), popupCommonAlertDimissclose, "");
      }else{
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
      }
    }
  //#ifdef iphone
  if(kony.store.getItem("isPushMandatory") === true && kony.store.getItem("isDeviceRegisteredMandatory") === true){
    registerPush();
  }
  //#endif
}

function animateLoginFlex() {
    frmLoginKA.apScrollEnable.animate(
        kony.ui.createAnimation({
            "100": {
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": 1
        }, {
            "animationEnd": function() {
                kony.print("Login postShow :: apScrollEnable animation completed.");
            }
        }
    );

    frmLoginKA.loginCard.animate(
        kony.ui.createAnimation({
            "100": {
                "centerY": "50%",
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": 0.6,
            "delay": 0.1
        }, {
            "animationEnd": function() {
                kony.print("Login postShow :: apScrollEnable animation completed.");
            }
        }
    );
}

function loginHide() {
    frmLoginKA.tbxusernameTextField.text = "";
    frmLoginKA.passwordTextField.text = "";
	frmLoginKA.passwordTextField.secureTextEntry = true;
  	frmLoginKA.lblShowPass.text = "A";
    frmLoginKA.borderBottom.skin = "skntextFieldDivider";
    frmLoginKA.flxbdrpwd.skin = "skntextFieldDivider";
    animateLabel("DOWN", "lblUserName", frmLoginKA.tbxusernameTextField.text);
    animateLabel("DOWN", "lblPassword", frmLoginKA.passwordTextField.text);

    //   if (userAgent === "iPhone" || userAgent === "iPad") {
    //     if (//frmLoginKA.rememberUsernameSwitch.selectedIndex === 1) {
    //       frmLoginKA.tbxusernameTextField.text = null;
    //     }
    //   }
    //   frmLoginKA.passwordTextField.text = null;
}


////////////////////////////////
// Login card animation logic 
////////////////////////////////
function clickedLoginCardDeactivate() {
    if (frmLoginKA.loginCard.centerY === "40%") {
        loginKeyboardInactive();
    }
}

var usernameTextLength;
// on password endText and Done
function passwordOnEndEditing() {
    usernameTextLength = frmLoginKA.tbxusernameTextField.text;

    if (usernameTextLength.length >= 1) {
        // username textfield has text entered
        loginKeyboardInactive();
    }
}

function loginKeyboardActive() {
    frmLoginKA.loginCard.animate(
        kony.ui.createAnimation({
            100: {
                "centerY": "40%",
                "stepConfig": {
                    "timingFunction": easeOut
                }
            }
        }), {
            fillMode: forwards,
            duration: 0.3
        }, {
            animationEnd: function() {}
        });
    //   	frmLoginKA.logoContainer.animate(
    // 		kony.ui.createAnimation({100:{"centerY": "26%" ,"stepConfig":{"timingFunction": easeOut}}}),
    // 		{fillMode: forwards ,duration:0.3},
    // 		{animationEnd: function() {} });
}

function loginKeyboardInactiveLang() {
    frmLanguageChange.loginCard.animate(
        kony.ui.createAnimation({
            100: {
                "centerY": "50%",
                "stepConfig": {
                    "timingFunction": easeOut
                }
            }
        }), {
            fillMode: forwards,
            duration: 0.3
        }, {
            animationEnd: function() {}
        });
    //   	frmLoginKA.logoContainer.animate(
    // 		kony.ui.createAnimation({100:{"centerY": "62%" ,"stepConfig":{"timingFunction": easeOut}}}),
    // 		{fillMode: forwards ,duration:0.3},
    // 		{animationEnd: function() {} });
}

function loginKeyboardInactive() {
    frmLoginKA.loginCard.animate(
        kony.ui.createAnimation({
            100: {
                "centerY": "50%",
                "stepConfig": {
                    "timingFunction": easeOut
                }
            }
        }), {
            fillMode: forwards,
            duration: 0.3
        }, {
            animationEnd: function() {}
        });
    //   	frmLoginKA.logoContainer.animate(
    // 		kony.ui.createAnimation({100:{"centerY": "62%" ,"stepConfig":{"timingFunction": easeOut}}}),
    // 		{fillMode: forwards ,duration:0.3},
    // 		{animationEnd: function() {} });
}




////////////////////////////////
// Account Preview Swipe logic
////////////////////////////////
function apScrollEnableScrolling() {
    /*
      var bgScrollX = frmLoginKA.apScrollEnable.contentOffsetMeasured.x;
      var bgScrollXAbs = Math.abs(bgScrollX);

      if (bgScrollX < 0) {
          frmLoginKA.apScrollDisable.left = (-100 + (bgScrollXAbs * 0.75)) + "%";
          frmLoginKA.loginMainScreen.opacity = 1.0 - (bgScrollXAbs * 0.01);
          frmLoginKA.loginMainScreen.left = "0dp";
      } */
}

function apScrollEnableTouchEnd() {
    var bgScrollX = frmLoginKA.apScrollEnable.contentOffsetMeasured.x;
    if (bgScrollX < -50) {
        apAnimate();
    }
}

function apScrollDisableTouchEnd() {
    var bgScrollX = frmLoginKA.apScrollDisable.contentOffsetMeasured.x;
    if (bgScrollX > 25) {
        apUnAnimate();
    }
}

function apAnimate() {
    /* var apWidth = frmLoginKA.accountPreviewScreen.width;
     apWidth = "100%";
     var transform = kony.ui.makeAffineTransform();
     transform.translate(apWidth, 0);
     frmLoginKA.apScrollDisable.animate(
         kony.ui.createAnimation({
             100: {
                 "transform": transform,
                 "stepConfig": {
                     "timingFunction": easeOut
                 }
             }
         }), {
             fillMode: forwards,
             duration: 0.5
         }, {
             animationEnd: function() {}
         });

     frmLoginKA.apScrollEnable.animate(
         kony.ui.createAnimation({
             100: {
                 "opacity": 0.2,
                 "stepConfig": {
                     "timingFunction": easeOut
                 }
             }
         }), {
             fillMode: forwards,
             duration: 0.5
         }, {
             animationEnd: function() {}
         }); */
}

function apUnAnimate() {
    /* var transform = kony.ui.makeAffineTransform();
     transform.translate(0, 0);

     frmLoginKA.apScrollDisable.animate(
         kony.ui.createAnimation({
             100: {
                 "transform": transform,
                 "stepConfig": {}
             }
         }), {
             fillMode: forwards,
             duration: 0.5
         }, {
             animationEnd: function() {}
         });

     frmLoginKA.apScrollEnable.animate(
         kony.ui.createAnimation({
             100: {
                 "opacity": 1,
                 "stepConfig": {
                     "timingFunction": easeOut
                 }
             }
         }), {
             fillMode: forwards,
             duration: 0.5
         }, {
             animationEnd: function() {}
         });

     frmLoginKA.loginMainScreen.opacity = 1; */
}

// function hidePreloginBannerImage()
// {
//   kony.print("PreLoginAd image download Failed");
//   //frmLoginKA.bannerAd.animate(
//         kony.ui.createAnimation({"100":{ "opacity": 0,
//                   "stepConfig":{"timingFunction": easeOut}}}),
//         {"fillMode": forwards, "duration":0.5},
//         {"animationEnd": function () {}}
//     );
//     frmLoginKA.utilityLinks.animate(
//         kony.ui.createAnimation({"100":{ "bottom": 0,
//                   "stepConfig":{"timingFunction": easeIn}}}),
//         {"fillMode": forwards, "duration": 0.7},
//         {"animationEnd": function () {}
//         }
//     );
//   //frmLoginKA.bannerAd.setVisibility(false);
//   //frmLoginKA.bannerAd.setEnabled(false);
// }




//setGestureLoginScreen for account priview
function set_GESTUREQUICKPREVIEWLANG() {
    kony.print("Inside setGestureQuickPreview");
    var setupTblTap = {
        fingers: 1,
        swipedistance: 60,
        swipevelocity: 60
    }; //swipe gesture

    frmLanguageChange.setGestureRecognizer(2, setupTblTap, accountPreviewGustureCallback);


}

function setGestureQuickPreview() {
    var setupTblTap = {
        fingers: 1,
        swipedistance: 60,
        swipevelocity: 60
    }; //swipe gesture

    frmLoginKA.setGestureRecognizer(2, setupTblTap, accountPreviewGustureCallback);

}

function accountPreviewGustureCallback(myWidget, gestureInfo) {
    var lanCheck = false;
    var langSelected1 = kony.store.getItem("langPrefObj");
    if (langSelected1 === "ar") {
        if (frmLoginKA.apScrollDisable.right === "-100%") {
            lanCheck = true;
        }
    } else if (frmLoginKA.apScrollDisable.left === "-100%") {
        lanCheck = true;
    }
	var currentForm = kony.application.getCurrentForm().id;
  	var valid = false;
    if (lanCheck && frmLoginKA.loginMainScreen.isVisible) {
    	valid = true;
    }else if(currentForm === "frmLanguageChange" && lanCheck){
    	valid = true;
    }
	if(valid){
        kony.print("accountPreview::" + JSON.stringify(gestureInfo));
        var swipedSide = gestureInfo.swipeDirection;
        if (swipedSide === 2) {
            //fetchApplicationPropertiesWithoutLoading();
            //callDeviceRegistrationForCustID();
          	swipegetcustid();
        }
		valid = false;
    }    

}

function swipegetcustid(){
  kony.sdk.mvvm.KonyApplicationContext.init();
  var configParams = {
    "ShowLoadingScreenFunction": ShowLoadingScreen
  };
  var appFactory = kony.sdk.mvvm.KonyApplicationContext.getFactorySharedInstance();
  var appPropertiesInstance = appFactory.createConfigurationServiceManagerObject();
  var appPropertyObj = appPropertiesInstance.generateAppPropsObj(configParams);
  appPropertiesInstance.apply(appPropertyObj);
  var options = "online";
  var initManager = appFactory.createAppInitManagerObject();
  initManager.registerService("MetadataServiceManager", {
    "object": appFactory.createMetadataServiceManagerObject(),
    "params": {
      "options": options
    }
  });
  initManager.executeRegistedServices(callDeviceRegistrationForCustID, doNothing);
//   callDeviceRegistrationForCustID();
}

function setUpLoginScreen() {

    animateLabel("DOWN", "lblUserName", frmLoginKA.tbxusernameTextField.text);
    animateLabel("DOWN", "lblPassword", frmLoginKA.passwordTextField.text);
    frmLoginKA.tbxusernameTextField.text = "";
    frmLoginKA.passwordTextField.text = "";
	frmLoginKA.passwordTextField.secureTextEntry = true;
	frmLoginKA.lblShowPass.text = "A";
    frmLoginKA.borderBottom.skin = "skntextFieldDivider";
    frmLoginKA.flxbdrpwd.skin = "skntextFieldDivider";
    frmLoginKA.lblInvalidCredentialsKA.setVisibility(false);
    frmLoginKA.loginCard.opacity = 1;
    frmLoginKA.lblChangeUserName.setVisibility(false);
    frmLoginKA.tbxusernameTextField.setVisibility(true);
    frmLoginKA.borderBottom.skin = "skntextFieldDivider";
    frmLoginKA.flxbdrpwd.skin = "skntextFieldDivider";
    frmLoginKA.loginMainScreen.setVisibility(true);
    frmLoginKA.skin = "sknLoginMainPage";
    frmLoginKA.loginMainScreen.setEnabled(true);
    frmLoginKA.loginMainScreen.opacity = 1;
    frmLoginKA.flxtouchIdAndrd.setVisibility(false);
    frmLoginKA.loginMainPINScreen.setVisibility(false);
    setUtilIcons();
}


function setUtilIcons() {
    var isPinSupported = kony.store.getItem("isPinSupported");
    var isTouchSupported = kony.store.getItem("isTouchSupported");
	var model = kony.os.deviceInfo().model;
    kony.print("isPinSupported : " + isPinSupported);
    kony.print("isTouchSupported : " + isTouchSupported);
	
    if (isPinSupported !== null && isPinSupported !== undefined && isPinSupported !== "" && isPinSupported == "Y") {
        if (isTouchSupported !== null && isTouchSupported !== undefined && isTouchSupported !== "" && isTouchSupported == "Y") {
//             frmLoginKA.contactLink.left = 1 + "%"; frmLoginKA.contactLink
//             frmLoginKA.atmLink.left = 27 + "%"; // Edit by Ahmad on 11-19-2020
//             frmLoginKA.btnPinLogin.left = 54 + "%"; Ahmad 12-8-2020
          if(checkFaceIdIphone()){
//             frmLoginKA.btnFaceID.left = 81 + "%"; Ahmad 12-8-2020
//             frmLoginKA.btnTouchID.left = 81 + "%"; Ahmad 12-8-2020
            frmLoginKA.btnFaceID.setVisibility(true); 
            frmLoginKA.btnTouchID.setVisibility(false);
            frmLoginKA.lblFingerOnUser.setVisibility(false); // Edit by Ahmad on 11-18-2020
            frmLoginKA.lblFingerOnPassword.setVisibility(false); // Edit by Ahmad on 11-18-2020
          }else{
//             frmLoginKA.btnFaceID.left = 81 + "%"; Ahmad 12-8-2020
//             frmLoginKA.btnTouchID.left = 81 + "%"; Ahmad 12-8-2020
            frmLoginKA.btnFaceID.setVisibility(false);
            frmLoginKA.lblFaceidOnUser.setVisibility(false); // Edit by Ahmad on 11-18-2020
            frmLoginKA.lblFaceidOnPassword.setVisibility(false); // Edit by Ahmad on 11-18-2020
            frmLoginKA.btnTouchID.setVisibility(true); 
//             frmLoginKA.lblFingerOnUser.setVisibility(true); // Edit by Ahmad on 11-18-2020
//             frmLoginKA.lblFingerOnPassword.setVisibility(true); // Edit by Ahmad on 11-18-2020
          }

            frmLoginKA.btnPinLogin.setVisibility(true);
            
            frmLoginKA.forceLayout();
        } else {
//             frmLoginKA.contactLink.left = 4 + "%"; frmLoginKA.contactLink
//             frmLoginKA.atmLink.left = 41 + "%"; // Edit by Ahmad on 11-19-2020
//             frmLoginKA.btnPinLogin.left = 81 + "%"; Edit by Ahmad on 12-16-2020
            frmLoginKA.btnPinLogin.setVisibility(true); 
            frmLoginKA.btnFaceID.setVisibility(false);
            frmLoginKA.lblFaceidOnUser.setVisibility(false); // Edit by Ahmad on 11-18-2020
            frmLoginKA.lblFaceidOnPassword.setVisibility(false); // Edit by Ahmad on 11-18-2020
            frmLoginKA.btnTouchID.setVisibility(true); // Edit by Ahmad on 12-22-2020
            frmLoginKA.lblFingerOnUser.setVisibility(false); // Edit by Ahmad on 11-18-2020
            frmLoginKA.lblFingerOnPassword.setVisibility(false); // Edit by Ahmad on 11-18-2020
        }
    } else if (isTouchSupported !== null && isTouchSupported !== undefined && isTouchSupported !== "" && isTouchSupported == "Y") {
//         frmLoginKA.contactLink.left = 4 + "%"; Edit by Ahmad on 11-19-2020
//         frmLoginKA.atmLink.left = 41 + "%"; // Edit by Ahmad on 11-19-2020
//         frmLoginKA.btnPinLogin.setVisibility(false); // Edit by Ahmad on 11-21-2020
        if(checkFaceIdIphone()){ 
//             frmLoginKA.btnFaceID.left = 81 + "%"; Ahmad 12-8-2020
//             frmLoginKA.btnTouchID.left = 81 + "%"; Ahmad 12-8-2020
            frmLoginKA.btnFaceID.setVisibility(true); 
//             frmLoginKA.lblFaceidOnUser.setVisibility(true); // Edit by Ahmad on 11-18-2020
//             frmLoginKA.lblFaceidOnPassword.setVisibility(true); // Edit by Ahmad on 11-18-2020
            frmLoginKA.btnTouchID.setVisibility(false);
            frmLoginKA.lblFingerOnUser.setVisibility(false); // Edit by Ahmad on 11-18-2020
            frmLoginKA.lblFingerOnPassword.setVisibility(false); // Edit by Ahmad on 11-18-2020
          }else{
//             frmLoginKA.btnFaceID.left = 81 + "%";  Ahmad 12-8-2020
//             frmLoginKA.btnTouchID.left = 81 + "%"; Ahmad 12-8-2020  
            frmLoginKA.btnFaceID.setVisibility(false);
            frmLoginKA.lblFaceidOnUser.setVisibility(false); // Edit by Ahmad on 11-18-2020
            frmLoginKA.lblFaceidOnPassword.setVisibility(false); // Edit by Ahmad on 11-18-2020
//             frmLoginKA.btnTouchID.setVisibility(true); // Edit by Ahmad on 12-22-2020
          }

    } else {
//         frmLoginKA.contactLink.left = 19 + "%"; Edit by Ahmad on 11-19-2020
//         frmLoginKA.atmLink.left = 63 + "%"; // Edit by Ahmad on 11-19-2020
		frmLoginKA.btnFaceID.setVisibility(false);
        frmLoginKA.lblFaceidOnUser.setVisibility(false); // Edit by Ahmad on 11-18-2020
        frmLoginKA.lblFaceidOnPassword.setVisibility(false); // Edit by Ahmad on 11-18-2020
//         frmLoginKA.btnPinLogin.setVisibility(false); // Edit by Ahmad on 11-21-2020
        frmLoginKA.btnTouchID.setVisibility(true); 
        frmLoginKA.lblFingerOnUser.setVisibility(false); // Edit by Ahmad on 11-18-2020
        frmLoginKA.lblFingerOnPassword.setVisibility(false); // Edit by Ahmad on 11-18-2020
    }
	if(ENABLEBOJPAYAPP === "Y"){}
//     	frmLoginKA.flxPayAppLink.setVisibility(true); // Edit by Ahmad on 11-19-2020
	else
    	frmLoginKA.flxPayAppLink.setVisibility(false);
    frmLoginKA.forceLayout();
}

function set_LOGINOPTIONSINLANG() {
	kony.print("set_loginoptionsinlang"+JSON.stringify(frmLanguageChange));
	if(frmLanguageChange.flxLogin.isVisible === true){
        set_GESTUREQUICKPREVIEWLANG();
    }
	var currentAppVersion = parseFloat(appConfig.appVersion);
    frmLanguageChange.lblVersion.text = "V " + currentAppVersion;
	frmLanguageChange.lblUserName.text = geti18Value("i18n.login.username");
    frmLanguageChange.lblPassword.text = geti18Value("i18n.login.password");
    frmLanguageChange.signInButton.text = geti18Value("i18n.login.loginBOJ");
    frmLanguageChange.forgotpassword.text = geti18Value("i18n.common.ForUnamPwd");
    frmLanguageChange.lblRegisterMobileBanking.text = geti18Value("i18n.login.registerForMobileBank");
    if(kony.store.getItem("langPrefObj") !== undefined && kony.store.getItem("langPrefObj") !== null){
        if(kony.store.getItem("langPrefObj") === "ar"){
            frmLanguageChange.tbxusernameTextField.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
            frmLanguageChange.passwordTextField.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
        	frmLanguageChange.lblUserName.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
            frmLanguageChange.lblPassword.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
            
            frmLanguageChange.lblUserName.right = "2%";
          	frmLanguageChange.lblUserName.left = null;
            frmLanguageChange.lblPassword.right = "2%";
          	frmLanguageChange.lblPassword.left = null;
            frmLanguageChange.lblClosePass.right = "90%";
            frmLanguageChange.lblClose.right = "90%";
        }else{
            frmLanguageChange.tbxusernameTextField.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
            frmLanguageChange.passwordTextField.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
        	frmLanguageChange.lblUserName.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
            frmLanguageChange.lblPassword.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
            
            frmLanguageChange.lblUserName.left = "2%";
            frmLanguageChange.lblPassword.left = "2%";
            frmLanguageChange.lblClosePass.right = "2%";
            frmLanguageChange.lblClose.right = "2%";
        }
    }
    var isPinSupported = kony.store.getItem("isPinSupported");
    var isTouchSupported = kony.store.getItem("isTouchSupported");
	var model = kony.os.deviceInfo().model;
    kony.print("isPinSupported : " + isPinSupported);
    kony.print("isTouchSupported : " + isTouchSupported);
    if (isPinSupported !== null && isPinSupported !== undefined && isPinSupported !== "" && isPinSupported == "Y") {
        if (isTouchSupported !== null && isTouchSupported !== undefined && isTouchSupported !== "" && isTouchSupported == "Y") {
            frmLanguageChange.contactLink.left = 1 + "%"; 
            frmLanguageChange.atmLink.left = 27 + "%"; 
            frmLanguageChange.btnPinLogin.left = 54 + "%"; 
          if(checkFaceIdIphone()){
            frmLanguageChange.btnFaceID.left = 81 + "%"; 
            frmLanguageChange.btnTouchID.left = 81 + "%"; 
            frmLanguageChange.btnFaceID.setVisibility(true);
            frmLanguageChange.btnTouchID.setVisibility(false);
          }else{
//             frmLanguageChange.btnFaceID.left = 81 + "%"; Ahmad 12-8-2020
//             frmLanguageChange.btnTouchID.left = 81 + "%"; Ahmad 12-8-2020
            frmLanguageChange.btnFaceID.setVisibility(false);
            frmLanguageChange.btnTouchID.setVisibility(true);
          }

            frmLanguageChange.btnPinLogin.setVisibility(true);
            frmLanguageChange.forceLayout();
        } else {
            frmLanguageChange.contactLink.left = 4 + "%";
            frmLanguageChange.atmLink.left = 41 + "%";
            frmLanguageChange.btnPinLogin.left = 81 + "%";
            frmLanguageChange.btnPinLogin.setVisibility(true);
            frmLanguageChange.btnTouchID.setVisibility(false);
            frmLanguageChange.btnFaceID.setVisibility(false);
        }
    } else if (isTouchSupported !== null && isTouchSupported !== undefined && isTouchSupported !== "" && isTouchSupported == "Y") {
        frmLanguageChange.contactLink.left = 4 + "%";
        frmLanguageChange.atmLink.left = 41 + "%";
        frmLanguageChange.btnPinLogin.setVisibility(false);
      if(checkFaceIdIphone()){
        frmLanguageChange.btnFaceID.left = 81 + "%";
        frmLanguageChange.btnTouchID.left = 81 + "%";
        frmLanguageChange.btnFaceID.setVisibility(true);
        frmLanguageChange.btnTouchID.setVisibility(false);
      }else{
        frmLanguageChange.btnFaceID.left = 81 + "%";
        frmLanguageChange.btnTouchID.left = 81 + "%";
        frmLanguageChange.btnFaceID.setVisibility(false);
        frmLanguageChange.btnTouchID.setVisibility(true);
      }

    } else {
        frmLanguageChange.contactLink.left = 19 + "%";
        frmLanguageChange.atmLink.left = 63 + "%";
		frmLanguageChange.btnFaceID.setVisibility(false);
        frmLanguageChange.btnPinLogin.setVisibility(false);
        frmLanguageChange.btnTouchID.setVisibility(false);
    }
    frmLanguageChange.forceLayout();
}

function switch_LANGUAGE(form){
	try{
    	frmLanguageChange.flxMain.isVisible = true;
    	frmLanguageChange.flxLogin.isVisible = false;
    	frmLanguageChange.flxRememberLang.isVisible = false;
    	frmLanguageChange.chckBoxRememberMe.isVisible = false;
    	frmLanguageChange.lblRememCheckBox.text = "q";
    	if(form === "login"){
        	frmLanguageChange.show();
        }
    }catch(e){
    	kony.print("Exception_switch_LANGUAGE ::"+e);
    }
}
function SetupLoginTalkbackFeature(){
  frmLoginKA.lblRegisterMobileBanking.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.login.registerForMobileBank"),
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmLoginKA.tbxusernameTextField.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.login.username"),
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmLoginKA.passwordTextField.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.login.password"),
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmLoginKA.contactLink.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.information.support"),
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmLoginKA.atmLink.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.atm.accessibility"),
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmLoginKA.imgCloseTouchId.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.pinLogin.clear"),
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmLoginKA.imgMainImg.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.fingerprint.ICON"),
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmLoginKA.flxIconMove.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.LabibaIcon"),
		"a11yLabel": "",
		"a11yHint": ""
		};
}
function CallTouchId(){
    var isPinSupported = kony.store.getItem("isPinSupported");
    var isTouchSupported = kony.store.getItem("isTouchSupported");
	var model = kony.os.deviceInfo().model;
    kony.print("isPinSupported : " + isPinSupported);
    kony.print("isTouchSupported : " + isTouchSupported);
	
    if (isPinSupported !== null && isPinSupported !== undefined && isPinSupported !== "" && isPinSupported == "Y") {
        if (isTouchSupported !== null && isTouchSupported !== undefined && isTouchSupported !== "" && isTouchSupported == "Y") {
          if(checkFaceIdIphone()){
//             frmLoginKA.btnFaceID.setVisibility(true); 
//             frmLoginKA.btnTouchID.setVisibility(false);   
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.FaceID.msg"), popupCommonAlertDimiss, "");
            
          }else{
//             frmLoginKA.btnTouchID.setVisibility(true); 
//             frmLoginKA.btnFaceID.setVisibility(false);
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            ShowLoadingScreen();
            isAlternateLoginEnabled("T");

          }
            frmLoginKA.forceLayout();
        }else{
//             frmLoginKA.btnTouchID.setVisibility(false);
//             frmLoginKA.btnFaceID.setVisibility(false);
          customAlertPopup("", kony.i18n.getLocalizedString("i18n.FaceID.msg"), popupCommonAlertDimiss, "");
        }
    } else if (isTouchSupported !== null && isTouchSupported !== undefined && isTouchSupported !== "" && isTouchSupported == "Y") {
        if(checkFaceIdIphone()){ 
//             frmLoginKA.btnTouchID.setVisibility(false);
//             frmLoginKA.btnFaceID.setVisibility(true);
          customAlertPopup("", kony.i18n.getLocalizedString("i18n.FaceID.msg"), popupCommonAlertDimiss, "");
          }else{
//             frmLoginKA.btnTouchID.setVisibility(true);
//             frmLoginKA.btnFaceID.setVisibility(false);
              kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
              ShowLoadingScreen();
              isAlternateLoginEnabled("T");
          }

    } else {
//         frmLoginKA.btnTouchID.setVisibility(false);
//         frmLoginKA.btnFaceID.setVisibility(false);
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.FaceID.msg"), popupCommonAlertDimiss, "");
    }
    frmLoginKA.forceLayout();

}

function CallFaceId(){
    var isPinSupported = kony.store.getItem("isPinSupported");
    var isTouchSupported = kony.store.getItem("isTouchSupported");
	var model = kony.os.deviceInfo().model;
    kony.print("isPinSupported : " + isPinSupported);
    kony.print("isTouchSupported : " + isTouchSupported);
	
    if (isPinSupported !== null && isPinSupported !== undefined && isPinSupported !== "" && isPinSupported == "Y") {
        if (isTouchSupported !== null && isTouchSupported !== undefined && isTouchSupported !== "" && isTouchSupported == "Y") {
          if(checkFaceIdIphone()){
//             frmLoginKA.btnFaceID.setVisibility(true); 
//             frmLoginKA.btnTouchID.setVisibility(false);   
//             customAlertPopup("", kony.i18n.getLocalizedString("i18n.TOuch.notEnabledText"), popupCommonAlertDimiss, "");
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            ShowLoadingScreen();
            isAlternateLoginEnabled("T");
          }else{
//             frmLoginKA.btnTouchID.setVisibility(true); 
//             frmLoginKA.btnFaceID.setVisibility(false);
             customAlertPopup("", kony.i18n.getLocalizedString("i18n.FaceID.msg"), popupCommonAlertDimiss, "");
           

          }
            frmLoginKA.forceLayout();
        }else{
//             frmLoginKA.btnTouchID.setVisibility(true);
//             frmLoginKA.btnFaceID.setVisibility(false);
          customAlertPopup("", kony.i18n.getLocalizedString("i18n.FaceID.msg"), popupCommonAlertDimiss, "");
        }
    } else if (isTouchSupported !== null && isTouchSupported !== undefined && isTouchSupported !== "" && isTouchSupported == "Y") {
        if(checkFaceIdIphone()){ 
//             frmLoginKA.btnTouchID.setVisibility(false);
//             frmLoginKA.btnFaceID.setVisibility(true);
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            ShowLoadingScreen();
            isAlternateLoginEnabled("T");          
        }else{
//             frmLoginKA.btnTouchID.setVisibility(true);
//             frmLoginKA.btnFaceID.setVisibility(false);
          customAlertPopup("", kony.i18n.getLocalizedString("i18n.FaceID.msg"), popupCommonAlertDimiss, "");

          }

    } else {
//         frmLoginKA.btnTouchID.setVisibility(true);
//         frmLoginKA.btnFaceID.setVisibility(false);
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.FaceID.msg"), popupCommonAlertDimiss, "");
    }
    frmLoginKA.forceLayout();

}