//Created by Arpan
kony = kony || {};
kony.boj = kony.boj || {};
kony.boj.BillerResetFlag = true;
kony.boj.selectedBillerType = "PostPaid";

kony.boj.getSelectedBillerCode = function () {
  if (kony.boj.selectedBillerType === "PostPaid")
    return "116";
  return "117";
};

kony.boj.getSelectedBillerListCode = function () {
  if (kony.boj.selectedBillerType === "PostPaid")
    return "112";
  return "113";
};

kony.boj.resetSelectedItemsBiller = function (type) {
  kony.boj.Biller[type] = {
    "Operation": "",
    "selectedBillerCategory": {},
    "selectedBiller": {},
    "selectedType": {},
    "selectedIDType": {},
    "selectedNationality": {},
    "denotype": [],
    "selectedDenoType": {},
    "selectedList": []
  };
};

kony.boj.callService = function (operationName) {
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  kony.boj.BillerResetFlag = false;

  kony.boj.Biller[kony.boj.selectedBillerType].Operation = operationName;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options = {
    "access": "online",
    "objectName": "RBObjects"
  };
  var headers = {};
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("Payee", serviceName, options);
  var dataObject = kony.boj.addServiceField();
  var serviceOptions = {
    "dataObject": dataObject,
    "headers": headers
  };
  kony.print("Input params-->" + serviceOptions);
  kony.print("Input params-->" + JSON.stringify(serviceOptions));
  if (kony.sdk.isNetworkAvailable())
    modelObj.customVerb(operationName, serviceOptions, kony.boj.successBiller, kony.boj.failureBiller);
  else{
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
  }
};

kony.boj.addServiceField = function () {
  var dataObject = new kony.sdk.dto.DataObject("Payee");

  switch (kony.boj.Biller[kony.boj.selectedBillerType].Operation) {
    case "GetEfwBillerCategory":
      dataObject.addField("p_code_type", kony.boj.getSelectedBillerCode());
      break;
    case "GetEfwBillerList":
      dataObject.addField("p_code_type", kony.boj.getSelectedBillerListCode());
      dataObject.addField("p_catg_type", kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code);
      break;
    case "GetEfwServiceType":
      dataObject.addField("p_code_type", kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code);
      dataObject.addField("lang", kony.boj.lang);
      break;
    case "GetPPServiceType":
      dataObject.addField("p_biller_code", kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code);
      dataObject.addField("lang", kony.boj.lang);
      break;
    default:
      alert("No Service Selected");
  }
  return dataObject;
};

kony.boj.successBiller = function (res) {
  //     alert("success--" + JSON.stringify(kony.boj.lang));
  try{
  kony.print("get biller details ::"+JSON.stringify(res));
  frmBillerListForm.segDetails.setData([]);
  frmBillerListForm.segDetails.widgetDataMap = {
    lblData: "code_desc",
    lblInitial: "initial",
    flxIcon1: "icon"
  };
  switch (kony.boj.Biller[kony.boj.selectedBillerType].Operation) {
    case "GetEfwBillerCategory":
      frmBillerListForm.lblTitle.text = kony.i18n.getLocalizedString("i18n.billsPay.BillerCategory");
      res = res.EFWCodes;
      tempArr = [];
      for (var i in res) {
        if (res[i].lang !== undefined || res[i].lang !== null) {
          if (res[i].lang === kony.boj.lang) {
            res[i].initial = res[i].code_desc.substring(0, 2).toUpperCase();
            res[i].icon = {
              backgroundColor: kony.boj.getBackGroundColour(i)
            };
            tempArr.push(res[i]);
          }
        }
      }
      kony.boj.Biller[kony.boj.selectedBillerType].selectedList = tempArr;
      frmBillerListForm.segDetails.setData(tempArr);
      break;
    case "GetEfwBillerList":
      frmBillerListForm.lblTitle.text = kony.i18n.getLocalizedString("i18n.billsPay.BillerList");
      res = res.BillerList;
      tempArr = [];
      for (var i in res) {
        if (res[i].lang !== undefined) {
          if (res[i].lang === kony.boj.lang) {
            res[i].initial = res[i].code_desc.substring(0, 2).toUpperCase();
            res[i].icon = {
              backgroundColor: kony.boj.getBackGroundColour(i)
            };
            tempArr.push(res[i]);
          }
        }
      }
      kony.boj.Biller[kony.boj.selectedBillerType].selectedList = tempArr;
      frmBillerListForm.segDetails.setData(tempArr);
      //frmBillerListForm.segDetails.setData(res.BillerList);
      break;
    case "GetEfwServiceType":
      frmBillerListForm.lblTitle.text = kony.i18n.getLocalizedString("i18n.billsPay.ServiceType");
      res = res.BS_BOJ_EFW_SERVTYPES_TTYPEType;
      for (var i in res){
        res[i].initial = res[i].code_desc.substring(0, 2).toUpperCase();
        res[i].icon = {
              backgroundColor: kony.boj.getBackGroundColour(i)
            };
      }
      kony.boj.Biller[kony.boj.selectedBillerType].selectedList = res;
      frmBillerListForm.segDetails.setData(res);
      break;
    case "GetPPServiceType":
      frmBillerListForm.lblTitle.text = kony.i18n.getLocalizedString("i18n.billsPay.ServiceType");
      frmBillerListForm.segDetails.widgetDataMap = {
        lblData: "serv_type_desc",
        lblInitial: "initial",
        flxIcon1: "icon"
      };
      var item = [];
      for (i in res.bs_boj_efw_pp_servtypes_ttypeType) {
        if (res.bs_boj_efw_pp_servtypes_ttypeType[i].serv_type_desc !== undefined) {
          res.bs_boj_efw_pp_servtypes_ttypeType[i].initial = res.bs_boj_efw_pp_servtypes_ttypeType[i].serv_type_desc.substring(0, 2).toUpperCase();
          res.bs_boj_efw_pp_servtypes_ttypeType[i].icon = {
              backgroundColor: kony.boj.getBackGroundColour(i)
            };
          item.push(res.bs_boj_efw_pp_servtypes_ttypeType[i]);
        }
      }
      kony.boj.Biller[kony.boj.selectedBillerType].denotype = res.bs_boj_efw_pp_denos_ttypeType;
      var storeDatares = isEmpty(res) ? "" : res;
      kony.store.setItem("denoArray", storeDatares);
      kony.boj.Biller[kony.boj.selectedBillerType].selectedList = item;
      frmBillerListForm.segDetails.setData(item);
      break;
    default:
  }
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen("");
  frmBillerListForm.show();
  }catch(e){
  	kony.print("Exception_getBillerDetails ::"+e);
  }
};

kony.boj.failureBiller = function (res) {
  //   alert("failure:" + JSON.stringify(res));
  kony.print("failure--" + JSON.stringify(res));
  var Message = geti18nkey("i18n.common.somethingwentwrong");
  customAlertPopup(geti18Value("i18n.Bene.Failed"), Message,popupCommonAlertDimiss,"");
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen("");
};

kony.boj.manualService = function (operationName) {
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  kony.boj.Biller[kony.boj.selectedBillerType].Operation = operationName;
  kony.boj.BillerResetFlag = false;
  frmBillerListForm.segDetails.setData([]);

  switch (operationName) {
    case "Nationality":
      frmBillerListForm.lblTitle.text = kony.i18n.getLocalizedString("i18n.billsPay.Nationality");
      frmBillerListForm.segDetails.widgetDataMap = {
        lblData: "Description",
        lblInitial: "initial",
        flxIcon1: "icon"
      };
      break;
    case "IDType":
      frmBillerListForm.lblTitle.text = kony.i18n.getLocalizedString("i18n.billsPay.IDType");
      frmBillerListForm.segDetails.widgetDataMap = {
        lblData: "Description",
        lblInitial: "initial",
        flxIcon1: "icon"
      };
      break;
    case "Denomination":
      frmBillerListForm.lblTitle.text = kony.i18n.getLocalizedString("i18n.billsPay.Denomination");
      frmBillerListForm.segDetails.widgetDataMap = {
        lblData: "deno_desc",
        lblInitial: "initial",
        flxIcon1: "icon"
      };
      break;
    default:
  }

  res = kony.boj.Biller[operationName];
  tempArr = [];
  for (var i in res) {
    //     alert(JSON.stringify(res[i]));
    if (res[i].Language !== undefined) {
      if (res[i].Language === kony.boj.lang){
        res[i].initial = res[i].Description.substring(0, 2).toUpperCase();
        res[i].icon = {
              backgroundColor: kony.boj.getBackGroundColour(i)
            };
        tempArr.push(res[i]);
      }
    }
    if(res[i].deno_desc  !== undefined){
      res[i].initial = res[i].deno_desc.substring(0, 2).toUpperCase();
      res[i].icon = {
              backgroundColor: kony.boj.getBackGroundColour(i)
            };
      tempArr.push(res[i]);
    }
  }

  kony.boj.Biller[kony.boj.selectedBillerType].selectedList = tempArr;
  frmBillerListForm.segDetails.setData(tempArr);
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen("");
  frmBillerListForm.show();
};

kony.boj.onSegmentClickBiller = function (selectedItem) {
  var form;
  if (kony.boj.BillerTabSwitch)
    form = kony.application.getCurrentForm();
  else
    form = kony.application.getPreviousForm();

  if(form.id === "frmNewBillKA" && kony.boj.Biller[kony.boj.selectedBillerType].Operation !== "Denomination"){
    form.lblserviceTypeHint.text = "";
  }
  switch (kony.boj.Biller[kony.boj.selectedBillerType].Operation) {
    case "GetEfwBillerCategory":
      kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory = selectedItem;
      form.lblBillerCategory.text = selectedItem.code_desc;
      form.tbxBillerCategory.text = selectedItem.code;
      form.lblserviceTypeHint.isVisible = false;
      form.flxUnderlineBillerCategory.skin = "sknFlxGreenLine";
      form.lblBillerCategory.skin = "sknLblBack";

      if (!kony.boj.BillerTabSwitch)
        kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller = {};
      form.lblBillerName.text = kony.i18n.getLocalizedString("i18n.billsPay.billerName");
      form.tbxBillerName.text = "";
      form.flxUnderlineBillerName.skin = "sknFlxGreyLine";
      form.lblBillerName.skin = "sknLblNextDisabled";

      if (!kony.boj.BillerTabSwitch)
        kony.boj.Biller[kony.boj.selectedBillerType].selectedType = {};
      form.lblServiceType.text = kony.i18n.getLocalizedString("i18n.billsPay.ServiceType");
      form.tbxServiceType.text = "";
      form.flxUnderlineServiceType.skin = "sknFlxGreyLine";
      form.lblServiceType.skin = "sknLblNextDisabled";

      if (form.id === "frmNewBillKA" || form.id === "frmAddNewPrePayeeKA") {
        if (!kony.boj.BillerTabSwitch)
          kony.boj.Biller[kony.boj.selectedBillerType].selectedDenoType = {};
        form.lblDenomination.text = kony.i18n.getLocalizedString("i18n.billsPay.Denomination");
        form.tbxDenomination.text = "";
        form.flxUnderlineDenomination.skin = "sknFlxGreyLine";
        form.lblDenomination.skin = "sknLblNextDisabled";
      }
      break;
    case "GetEfwBillerList":
      kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller = selectedItem;
      form.lblBillerName.text = selectedItem.code_desc;
      form.tbxBillerName.text = selectedItem.code;
      form.lblserviceTypeHint.isVisible = false;
      form.flxUnderlineBillerName.skin = "sknFlxGreenLine";
      form.lblBillerName.skin = "sknLblBack";

      if (!kony.boj.BillerTabSwitch)
        kony.boj.Biller[kony.boj.selectedBillerType].selectedType = {};
      form.lblServiceType.text = kony.i18n.getLocalizedString("i18n.billsPay.ServiceType");
      form.tbxServiceType.text = "";
      form.flxUnderlineServiceType.skin = "sknFlxGreyLine";
      form.lblServiceType.skin = "sknLblNextDisabled";

      if (form.id === "frmNewBillKA" || form.id === "frmAddNewPrePayeeKA") {
        if (!kony.boj.BillerTabSwitch)
          kony.boj.Biller[kony.boj.selectedBillerType].selectedDenoType = {};
        form.lblDenomination.text = kony.i18n.getLocalizedString("i18n.billsPay.Denomination");
        form.tbxDenomination.text = "";
        form.flxUnderlineDenomination.skin = "sknFlxGreyLine";
        form.lblDenomination.skin = "sknLblNextDisabled";
      }
      break;
    case "GetEfwServiceType":
      kony.boj.Biller[kony.boj.selectedBillerType].selectedType = selectedItem;
      form.lblServiceType.text = selectedItem.code_desc;
      form.tbxServiceType.text = selectedItem.code;
      if(selectedItem.labl_desc !== undefined && selectedItem.labl_desc !== ""){
        form.lblserviceTypeHint.isVisible = true;
        form.lblserviceTypeHint.text = "* "+selectedItem.labl_desc;
      }
      form.flxUnderlineServiceType.skin = "sknFlxGreenLine";
      form.lblServiceType.skin = "sknLblBack";
      break;
    case "GetPPServiceType":
      kony.boj.Biller[kony.boj.selectedBillerType].selectedType = selectedItem;
      form.lblServiceType.text = selectedItem.serv_type_desc;
      form.tbxServiceType.text = selectedItem.serv_type_code;
      if(selectedItem.labl_desc !== undefined && selectedItem.labl_desc !== ""){
        form.lblserviceTypeHint.isVisible = true;
        form.lblserviceTypeHint.text = "* "+selectedItem.labl_desc;
      }
      form.flxUnderlineServiceType.skin = "sknFlxGreenLine";
      form.lblServiceType.skin = "sknLblBack";
      kony.store.setItem("denoFlag",{});
      if (!kony.boj.BillerTabSwitch)
        denominationVerification();
/*hassan here*/
      if ((form.id === "frmNewBillKA" || form.id === "frmAddNewPrePayeeKA") && kony.boj.addBillerFromQuickPay === false) {
        if (!kony.boj.BillerTabSwitch)
          kony.boj.Biller[kony.boj.selectedBillerType].selectedDenoType = {};
        form.lblDenomination.text = kony.i18n.getLocalizedString("i18n.billsPay.Denomination");
        form.tbxDenomination.text = "";
        form.flxUnderlineDenomination.skin = "sknFlxGreyLine";
        form.lblDenomination.skin = "sknLblNextDisabled";
      }
      break;
    case "Nationality":
      kony.boj.Biller[kony.boj.selectedBillerType].selectedNationality = selectedItem;
      form.lblNationality.text = selectedItem.Description;
      form.tbxNationality.text = selectedItem.Code;
      form.flxUnderlineNationality.skin = "sknFlxGreenLine";
      form.lblNationality.skin = "sknLblBack";
      break;
    case "IDType":
      kony.boj.Biller[kony.boj.selectedBillerType].selectedIDType = selectedItem;
      form.lblIDType.text = selectedItem.Description;
      form.tbxIDType.text = selectedItem["ID Type"];
      form.flxUnderlineIDType.skin = "sknFlxGreenLine";
      form.lblIDType.skin = "sknLblBack";
      break;
    case "Denomination":
      frmNewBillKA.flxDenomination.isVisible = true;
      kony.boj.Biller[kony.boj.selectedBillerType].selectedDenoType = selectedItem;
      form.lblDenomination.text = selectedItem.deno_desc;
      form.tbxDenomination.text = selectedItem.deno_code;//deno_code
      form.flxUnderlineDenomination.skin = "sknFlxGreenLine";
      form.lblDenomination.skin = "sknLblBack";
      var storeDataselectedItem = isEmpty(selectedItem.deno_desc) ? "" : selectedItem.deno_desc;
      kony.store.setItem("deno_desc",storeDataselectedItem);
      break;
    default:
      kony.print("----On click Segment no action----");
  }
  if (nextFromNewPrepaidBene())
    frmAddNewPayeeKA.lblNext.skin = "sknLblNextEnabled";
  else
    frmAddNewPayeeKA.lblNext.skin = "sknLblNextDisabled";
  kony.boj.BillerTabSwitch = false;
  form.show();
};

kony.boj.searchBillerList = function(searchTerm){
  var tempArr = [];
  searchTerm = searchTerm.toLowerCase();

  frmBillerListForm.segDetails.setData([]);

  switch (kony.boj.Biller[kony.boj.selectedBillerType].Operation) {
    case "GetEfwBillerCategory":
    case "GetEfwBillerList":
    case "GetEfwServiceType":
      frmBillerListForm.segDetails.widgetDataMap = {
        lblData: "code_desc",
        lblInitial: "initial",
        flxIcon1: "icon"
      };
      for(var i in kony.boj.Biller[kony.boj.selectedBillerType].selectedList){
        if(kony.boj.Biller[kony.boj.selectedBillerType].selectedList[i].code_desc.toLowerCase().indexOf(searchTerm) !== -1)
          tempArr.push(kony.boj.Biller[kony.boj.selectedBillerType].selectedList[i]);
      }
      frmBillerListForm.segDetails.setData(tempArr);
      break;

    case "GetPPServiceType":
      frmBillerListForm.segDetails.widgetDataMap = {
        lblData: "serv_type_desc",
        lblInitial: "initial",
        flxIcon1: "icon"
      };
      for(var i in kony.boj.Biller[kony.boj.selectedBillerType].selectedList){
        if(kony.boj.Biller[kony.boj.selectedBillerType].selectedList[i].serv_type_desc.toLowerCase().indexOf(searchTerm) !== -1)
          tempArr.push(kony.boj.Biller[kony.boj.selectedBillerType].selectedList[i]);
      }
      frmBillerListForm.segDetails.setData(tempArr);
      break;

    case "Nationality":
    case "IDType":
      frmBillerListForm.segDetails.widgetDataMap = {
        lblData: "Description",
        lblInitial: "initial",
        flxIcon1: "icon"
      };
      for(var i in kony.boj.Biller[kony.boj.selectedBillerType].selectedList){
        if(kony.boj.Biller[kony.boj.selectedBillerType].selectedList[i].Description.toLowerCase().indexOf(searchTerm) !== -1)
          tempArr.push(kony.boj.Biller[kony.boj.selectedBillerType].selectedList[i]);
      }
      frmBillerListForm.segDetails.setData(tempArr);
      break;

    case "Denomination":
      frmBillerListForm.segDetails.widgetDataMap = {
        lblData: "deno_desc",
        lblInitial: "initial",
        flxIcon1: "icon"
      };
      for(var i in kony.boj.Biller[kony.boj.selectedBillerType].selectedList){
        if(kony.boj.Biller[kony.boj.selectedBillerType].selectedList[i].deno_desc.toLowerCase().indexOf(searchTerm) !== -1)
          tempArr.push(kony.boj.Biller[kony.boj.selectedBillerType].selectedList[i]);
      }
      frmBillerListForm.segDetails.setData(tempArr);
      break;
    default:
      kony.print("----On click Segment no action----");
  }
};