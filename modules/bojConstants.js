kony = kony || {};
kony.boj = kony.boj || {};
var checkme = false;
kony.boj.retailBanking = kony.boj.retailBanking || {};
kony.boj.retailBanking.globals = kony.boj.retailBanking.globals || {};
kony.boj.retailBanking.globals ={
                          	LOADING_SCREEN_MESSAGE : "Loading Data...",  
 						  	IDLE_TIMEOUT :300,
  							APP_VERSION  : 0,
  							Maintenance_Flag : "",
							APP_USAGE_TIMEOUT :1200,
  							OTP_EXPIRE_LIMIT : 180, // value in seconds
  							OTP_RESEND_TIME : 30,  // value in seconds
  							serviceName : "RBObjects" 
};
var GENERIC_COSNTATNTS = {FLX:"flx",LBL:"lbl",BTN:"btn",TRUE:true,FALSE:false,
                          RIGHTICON:"RightIcon",AR:"ar",EN:"en",MAP:"M",LIST:"L"};
var PROFILE_CONSTANTS = {
  MENU:["Profile","AccountPreferences","ChangeLanguage","AlternateLogins","AlertandNotifications","CardPreferences"],
  PROFILE:["MyProfile","ChangeUsername","ChangePassword","MyRegisteredDevices","TnC1"],
  ACCOUNTPREFERENCES :["QuickBalance", "MyAccountSettings","ReorderMyAccountList","EstmtRegister","NewSubAccounts"],
  CHANGELANGUAGE: ["English","Arabic"],
  ALTERNATELOGINS:["PINLogin","AdditionalUser"],
  ALERTANDNOTIFICATIONS:["SMS","Alerts","AlertHistory"],
  CARDPREFERENCES :["MyCardSettings","ReorderMyCardList"],
  
};
var CARD_COSNTANTS = {
  ACTIVATE: "A",
  DEACTIVATE: "D",
  DEBITCARD: "D",
  CREDITCARD:"C",
  WEBCHARGE:"W"
};
var DEBITCARD_REASON_ARABIC = {
  FS:"اشتباه باحتيال",
  L:"مفقود",
  O:"أخرى",
  OB:"محتجزة في الصراف الآلي لبنك آخر",
  OWB:"محتجزة في الصراف الآلي لبنك العميل",
  RN3:"الإستخدام المسيء أو الإحتيال",
  S:"مسروق"
};
var DEBITCARD_REASON_ENGLISH = {
  FS:"Fraud Suspected",
  L:"Lost",
  O:"Others",
  OB:"Captured in Other Bank's ATM",
  OWB:"Captured in Own Bank's ATM",
  RN3:"Fraud",
  S:"Stolen"
};
var CREDITCARD_REASON_ARABIC = {
  L:"مفقود",
  RN3:"الإستخدام المسيء أو الإحتيال",
  S:"مسروق"
};
var CREDITCARD_REASON_ENGLISH = {
  L:"Lost",
  RN3:"Fraud",
  S:"Stolen"
};
var SKINCONSTANTS = {
  "BGBLUEROUNDEDWHITEFACE":"sknBtnBGBlueWhite105Rd10",
  "BGWHITEBLUEFACE":"sknBtnBGWhiteBlue105Rd10",
  "BGSHADEBLUE":"sknBGShadedBlueRounded",
  "FLXBGWHITE":"sknFlxBGWhiteRD10",
  "BTNBGLIGHTBLUE":"sknBtnBGLightBlueWhite105RD",
  "FLXBGBLUE":"sknFlxBGDarkBlueRD10"
};
var MAP_CONSTANTS = {
  "ALL":"ALL",
  "BRANCH":"BRANCH",
  "ATM":"ATM"
};

//iWatch Constatnts
var isiWatchRequest = false;

//cardsPaymentsuccess
var is_CARDPAYMENTSUCCESS = false;

var update_JOMOPAY_BENLIST = false;