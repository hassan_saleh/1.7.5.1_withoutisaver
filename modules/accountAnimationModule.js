//Type your code here
function setGestureDashboard() {
    kony.print("Inside setGestureDashboard");
    var setupTblTap = {
        fingers: 1,
        swipedistance: 55,
        swipevelocity: 60
    }; //swipe gesture


    frmAccountsLandingKA.flxAccountsMain.setGestureRecognizer(2, setupTblTap, accountDashboard);

    function accountDashboard(myWidget, gestureInfo) {

      
        kony.print("accountDashboard::" + JSON.stringify(gestureInfo));
        var swipedSide = gestureInfo.swipeDirection;
      if(kony.store.getItem("langPrefObj") == "ar"){
         if (swipedSide === 2) {
            if (frmAccountsLandingKA.segAccountsKADeals.data !== null && frmAccountsLandingKA.segAccountsKADeals.data.length === 1) {
                depositOnetimeCall();
            } else {
                dealsAnimation();
            }

        } else if (swipedSide === 1) {

        }
      }else{
        if (swipedSide === 1) {
            if (frmAccountsLandingKA.segAccountsKADeals.data !== null && frmAccountsLandingKA.segAccountsKADeals.data.length === 1) {
                depositOnetimeCall();
            } else {
                dealsAnimation();
            }

        } else if (swipedSide === 2) {

        }
        //loansAnimationRight();
      }

    }


    frmAccountsLandingKA.flxDeals.setGestureRecognizer(2, setupTblTap, dealsDashboard);

    function dealsDashboard(myWidget, gestureInfo) {
        kony.print("dealsDashboard::" + JSON.stringify(gestureInfo));
        var swipedSide = gestureInfo.swipeDirection;
      if(kony.store.getItem("langPrefObj") == "ar"){
        if (swipedSide === 2) {
            if (frmAccountsLandingKA.segAccountsKALoans.data !== null && frmAccountsLandingKA.segAccountsKALoans.data.length === 1) {
                LoanOnetimeCall();
            } else {
                loansAnimation();
            }

        } else if (swipedSide === 1) {
            if (frmAccountsLandingKA.segAccountsKA.data !== null && frmAccountsLandingKA.segAccountsKA.data.length === 1) {
                kony.print("Only one account:");
                var context = {
                    "sectionIndex": 0,
                    "rowIndex": 0,
                    "widgetInfo": {
                        "id": "segAccountsKA"
                    }
                };
                kony.print("accountDashboardFlowFlag" + kony.retailBanking.globalData.globals.accountDashboardFlowFlag);
                kony.retailBanking.globalData.globals.accountDashboardFlowFlag = true;
                onheaderDashboard(frmAccountsLandingKA.segAccountsKA.data, context);

            } else {
                accountsAnimattionRight();
            }
        }
      }else{
        if (swipedSide === 1) {
            if (frmAccountsLandingKA.segAccountsKALoans.data !== null && frmAccountsLandingKA.segAccountsKALoans.data.length === 1) {
                LoanOnetimeCall();
            } else {
                loansAnimation();
            }

        } else if (swipedSide === 2) {
            if (frmAccountsLandingKA.segAccountsKA.data !== null && frmAccountsLandingKA.segAccountsKA.data.length === 1) {
                kony.print("Only one account:");
                var context = {
                    "sectionIndex": 0,
                    "rowIndex": 0,
                    "widgetInfo": {
                        "id": "segAccountsKA"
                    }
                };
                kony.print("accountDashboardFlowFlag" + kony.retailBanking.globalData.globals.accountDashboardFlowFlag);
                kony.retailBanking.globalData.globals.accountDashboardFlowFlag = true;
                onheaderDashboard(frmAccountsLandingKA.segAccountsKA.data, context);

            } else {
                accountsAnimattionRight();
            }
        }
      }

    }


    frmAccountsLandingKA.flxLoans.setGestureRecognizer(2, setupTblTap, loansDashboard);

    function loansDashboard(myWidget, gestureInfo) {
        kony.print("loansDashboard::" + JSON.stringify(gestureInfo));
        var swipedSide = gestureInfo.swipeDirection;
      if(kony.store.getItem("langPrefObj") == "ar"){
         if (swipedSide === 2) {}
        //accountsAnimattion();
        else if (swipedSide === 1) {
            if (frmAccountsLandingKA.segAccountsKADeals.data !== null && frmAccountsLandingKA.segAccountsKADeals.data.length === 1) {
                depositOnetimeCall();
            } else {
                dealsAnimationRight();
            }
        }
      }else{
        if (swipedSide === 1) {}
        //accountsAnimattion();
        else if (swipedSide === 2) {
            if (frmAccountsLandingKA.segAccountsKADeals.data !== null && frmAccountsLandingKA.segAccountsKADeals.data.length === 1) {
                depositOnetimeCall();
            } else {
                dealsAnimationRight();
            }
        } 
      }
      
       
    }

}


/** **/
function accountsAnimattion() {
    kony.print("Inside accountsAnimattion");
    frmAccountsLandingKA.flxAccountsMain.left = "100%";
    // frmAccountsLandingKA.flxAccountsMain.setVisibility(true);


       
    frmAccountsLandingKA.flxAccountsMain.setVisibility(true);

    //     frmAccountsLandingKA.flxAccountsMain.zIndex = 1000;
    //     frmAccountsLandingKA.flxDeals.zIndex = 99;
    //     frmAccountsLandingKA.flxLoans.zIndex = 100;
    frmAccountsLandingKA.forceLayout();


    function animation_End() {
        frmAccountsLandingKA.flxDeals.setVisibility(false);
        frmAccountsLandingKA.flxLoans.setVisibility(false);
        frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
        frmAccountsLandingKA.forceLayout();
    }
    frmAccountsLandingKA.flxAccountsMain.animate(
        kony.ui.createAnimation({
            "0": {
                "left": "100%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            },
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.20
        }, {
            "animationStart": null,
            "animationEnd": animation_End
        });

}


/** **/
function dealsAnimation() {
    kony.print("Inside dealsAnimation");
    frmAccountsLandingKA.flxDeals.left = "100%";
    //frmAccountsLandingKA.flxDeals.setVisibility(true);


    frmAccountsLandingKA.flxDeals.setVisibility(true);
    //     frmAccountsLandingKA.flxAccountsMain.zIndex = 100;
    //     frmAccountsLandingKA.flxDeals.zIndex = 1000;
    //     frmAccountsLandingKA.flxLoans.zIndex = 99;
    frmAccountsLandingKA.forceLayout();


    function animation_End() {
        frmAccountsLandingKA.flxAccountsMain.setVisibility(false);
        frmAccountsLandingKA.flxLoans.setVisibility(false);
        frmAccountsLandingKA.flxDeals.setVisibility(true);
        frmAccountsLandingKA.forceLayout();
    }
    frmAccountsLandingKA.flxDeals.animate(
        kony.ui.createAnimation({
            "0": {
                "left": "100%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            },
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.20
        }, {
            "animationStart": null,
            "animationEnd": animation_End
        });

}


/** **/
function loansAnimation() {
    kony.print("Inside loansAnimation");
    frmAccountsLandingKA.flxLoans.left = "100%";
    //frmAccountsLandingKA.flxLoans.setVisibility(true);


    frmAccountsLandingKA.flxLoans.setVisibility(true);
    //     frmAccountsLandingKA.flxAccountsMain.zIndex = 99;
    //     frmAccountsLandingKA.flxDeals.zIndex = 100;
    //     frmAccountsLandingKA.flxLoans.zIndex = 1000;
    frmAccountsLandingKA.forceLayout();

    function animation_End() {
        frmAccountsLandingKA.flxAccountsMain.setVisibility(false);
        frmAccountsLandingKA.flxDeals.setVisibility(false);
        frmAccountsLandingKA.flxLoans.setVisibility(true);
        frmAccountsLandingKA.forceLayout();
    }
    frmAccountsLandingKA.flxLoans.animate(
        kony.ui.createAnimation({
            "0": {
                "left": "100%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            },
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.20
        }, {
            "animationStart": null,
            "animationEnd": animation_End
        });



}


/** **/
function accountsAnimattionRight() {
    kony.print("Inside accountsAnimattionRight");
    frmAccountsLandingKA.flxAccountsMain.left = "100%";
    //frmAccountsLandingKA.flxAccountsMain.setVisibility(true);


    frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
    //     frmAccountsLandingKA.flxAccountsMain.zIndex = 1000;
    //     frmAccountsLandingKA.flxDeals.zIndex = 100;
    //     frmAccountsLandingKA.flxLoans.zIndex = 99;
    frmAccountsLandingKA.forceLayout();


    function animation_End() {
        frmAccountsLandingKA.flxDeals.setVisibility(false);
        frmAccountsLandingKA.flxLoans.setVisibility(false);
        frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
        frmAccountsLandingKA.forceLayout();
    }
    frmAccountsLandingKA.flxAccountsMain.animate(
        kony.ui.createAnimation({
            "0": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            },
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.20
        }, {
            "animationStart": null,
            "animationEnd": animation_End
        });


}


/** **/
function dealsAnimationRight() {
    kony.print("Inside dealsAnimationRight");
    //frmAccountsLandingKA.flxDeals.setVisibility(true);
    frmAccountsLandingKA.flxDeals.left = "100%";


    frmAccountsLandingKA.flxDeals.setVisibility(true);
    //     frmAccountsLandingKA.flxAccountsMain.zIndex = 99;
    //     frmAccountsLandingKA.flxDeals.zIndex = 1000;
    //     frmAccountsLandingKA.flxLoans.zIndex = 100;
    frmAccountsLandingKA.forceLayout();

    function animation_End() {
        frmAccountsLandingKA.flxAccountsMain.setVisibility(false);
        frmAccountsLandingKA.flxLoans.setVisibility(false);
        frmAccountsLandingKA.flxDeals.setVisibility(true);
        frmAccountsLandingKA.forceLayout();
    }
    frmAccountsLandingKA.flxDeals.animate(
        kony.ui.createAnimation({
            "0": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            },
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.20
        }, {
            "animationStart": null,
            "animationEnd": animation_End
        });

}


/** **/
function loansAnimationRight() {
    kony.print("Inside loansAnimationRight");
    //frmAccountsLandingKA.flxLoans.setVisibility(true);
    frmAccountsLandingKA.flxLoans.left = "100%";

    frmAccountsLandingKA.flxLoans.setVisibility(true);
    //     frmAccountsLandingKA.flxAccountsMain.zIndex = 100;
    //     frmAccountsLandingKA.flxDeals.zIndex = 99;
    //     frmAccountsLandingKA.flxLoans.zIndex = 1000;
    frmAccountsLandingKA.forceLayout();

    function animation_End() {
        frmAccountsLandingKA.flxAccountsMain.setVisibility(false);
        frmAccountsLandingKA.flxDeals.setVisibility(false);
        frmAccountsLandingKA.flxLoans.setVisibility(true);
        frmAccountsLandingKA.forceLayout();
    }

    frmAccountsLandingKA.flxLoans.animate(
        kony.ui.createAnimation({
            "0": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            },
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.20
        }, {
            "animationStart": null,
            "animationEnd": animation_End
        });

}



function onrowclickDashboard(data, section, row) {
    kony.print("onrowclickDashboard::" + JSON.stringify(data) + "section::" + section + "row::" + row);
}

function onheaderDashboard(data, context) {
    try {
        kony.print("onrowclickDashboard::" + JSON.stringify(data) + "context::" + JSON.stringify(context));
        kony.print("widgetInfo::" + context.widgetInfo.id);
      var langSelected1 = kony.store.getItem("langPrefObj");
    var langSelected = langSelected1.toUpperCase();
        var segForm = context.widgetInfo.id;
        if (segForm === "segAccountsKA") {
            kony.print("segAccountsKA::");
            var segData = frmAccountsLandingKA.segAccountsKA.data;
            kony.print("frmAccountDetailKA.segAccountsKA.data ::" + segData[parseInt(context.sectionIndex)][0] + "context.sectionIndex " + context.sectionIndex);
            frmAccountDetailKA.flxDealsScreen.setVisibility(false);
            frmAccountDetailKA.flxLoansScreen.setVisibility(false);
            frmAccountDetailKA.accountDetailsScrollContainer.setVisibility(true);
            frmAccountDetailKA.accountDetailsScrollContainer.left = "0%";
            customerAccountDetails.length = segData.length;
            customerAccountDetails.data = segData;
            customerAccountDetails.currentIndex = parseInt(context.sectionIndex);
            if (frmAccountsLandingKA.segAccountsKA.data.length <= 1) {
                frmAccountInfoKA.flxPageIndicator.setVisibility(false);
            } else {
                frmAccountInfoKA.flxPageIndicator.setVisibility(true);
                create_PageIndicatorInfo(frmAccountInfoKA, 50, customerAccountDetails.length, customerAccountDetails.currentIndex);
            }
            gblAccountsCurrencyType = "JOD";
            var AccName = segData[parseInt(context.sectionIndex)][0].nickName.text.toString();
          //alert("AccName"+JSON.stringify(AccName));
           var accountName = AccName.substring(AccName.lastIndexOf("<label>")+1,AccName.lastIndexOf("</label>"))
           //alert("accountName"+accountName);
            frmAccountDetailKA.lblavailbalance.text = accountName.substring(6,accountName.length);
           //alert("frmAccountDetailKA.lblavailbalance.text"+frmAccountDetailKA.lblavailbalance.text);
            frmAccountDetailKA.availableBalanceAmount.text = segData[parseInt(context.sectionIndex)][0].availableBalance; //+ " " + segData[parseInt(context.sectionIndex)][0].currencyCode;
//            create_PageIndicator(frmAccountDetailKA, 50, customerAccountDetails.length);
        	create_PageIndicatorInfo(frmAccountDetailKA, 50, customerAccountDetails.length, customerAccountDetails.currentIndex);
            kony.print("customerAccountDetails ::" + JSON.stringify(customerAccountDetails));
            //     gesture_AccountAnimation();
            frmAccountDetailKA.flxAccountDetailsSecondary.isVisible = false;
            frmAccountDetailKA.flxAccountDetailsSecondary.left = "100%";
            frmAccountDetailKA.FlexContainer04dfa3182c5f34e.isVisible = true;
            frmAccountDetailKA.FlexContainer04dfa3182c5f34e.left = "0%";
            frmAccountDetailKA.lblNoResult.isVisible = false;
            frmAccountDetailKA.transactionSegment.isVisible = true;
            frmAccountDetailKA.lblExport.setEnabled(true);
            //     frmAccountDetailKA.show();
            kony.retailBanking.globalData.globals.accountDashboardFlowFlag = false;
            kony.print("Row Data ::" + JSON.stringify(segData[parseInt(context.sectionIndex)][1]));
            //if((segData[parseInt(context.sectionIndex)][1][0].lblNoData.isVisible === false || segData[parseInt(context.sectionIndex)][1][0].lblNoData.isVisible === "false")){
            //               gblAccountsCurrencyType = segData[parseInt(context.sectionIndex)][0].currencyCode;
            customerAccountDetails.isTransactionEmpty = false;
            fetch_TransactionList(kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)],{"toDATE":"","fromDATE":""});
            /**}else{
            	customerAccountDetails.isTransactionEmpty = true;
            	var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
              	var controller = INSTANCE.getFormController("frmAccountDetailKA");
            	var formmodel = controller.getFormModel();
            	formmodel.setViewAttributeByProperty("transactionSegment","isVisible",false);
    			formmodel.setViewAttributeByProperty("LabelNoRecordsKA","isVisible",true);
            	formmodel.setViewAttributeByProperty("LabelNoRecordsKA","text",geti18Value("i18n.accounts.noTransaction"));
            	formmodel.performActionOnView("CopylblFilter0f7770a92cff247","setEnabled",[false]);
            	formmodel.performActionOnView("lblExport","setEnabled",[false]);
            	formmodel.showView();
            }**/
            //fetch_TransactionList(kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)]);
            //     var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            //     kony.print("INSTANCE ::"+INSTANCE);
            //     var controller = INSTANCE.getFormController("frmAccountDetailKA");
            //     kony.print("controller ::"+controller);
            //     var navObject = new kony.sdk.mvvm.NavigationObject();
            //     kony.print("navObject ::"+navObject)
            //     navObject.setRequestOptions("transactionSegment",{"headers":{"session_token":kony.retailBanking.globalData.session_token}, "queryParams" : {"custId": "custId","accountNumber":"0013050052132001","p_Branch":"1","p_no_Of_Txn":"100","lang":"EN"}});
            //     controller.loadDataAndShowForm(navObject);

        } else if (segForm === "segAccountsKADeals") {
            kony.print("segAccountsKADeals::");
            DealsLoansCurrIndex = parseInt(context.rowIndex);
            var segData = frmAccountsLandingKA.segAccountsKADeals.data;
            frmAccountDetailKA.flxDealsScreen.setVisibility(true);
            frmAccountInfoKA.flxPageIndicator.setVisibility(true);
            frmAccountDetailKA.flxLoansScreen.setVisibility(false);
            gblAccountsCurrencyType = "JOD";
            if (!isEmpty(frmAccountsLandingKA.segAccountsKADeals.data[parseInt(context.rowIndex)].currencyCode)) {
                gblAccountsCurrencyType = frmAccountsLandingKA.segAccountsKADeals.data[parseInt(context.rowIndex)].currencyCode;
            }
            //           	create_PageIndicatorInfo(frmAccountInfoKA, 50, frmAccountsLandingKA.segAccountsKADeals.data.length,DealsLoansCurrIndex);
            var AccName =  frmAccountsLandingKA.segAccountsKADeals.data[parseInt(context.rowIndex)].nickName.text.toString();
            var accountName = AccName.substring(AccName.lastIndexOf("<label>")+1,AccName.lastIndexOf("</label>"))
            frmAccountInfoKA.accountNumberLabel.text =accountName.substring(6,accountName.length);kony.print("segAccountsKADeals segData ::" + kony.retailBanking.globalData.accountsDashboardData.dealsData[parseInt(context.rowIndex)]);
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var options = {
                "access": "online",
                "objectName": "RBObjects"
            };
            var headers = {};
            var serviceName = "RBObjects";
            var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
            var dataObject = new kony.sdk.dto.DataObject("Accounts");
//             dataObject.addField("usr", "icbsserv");
//             dataObject.addField("pass", "icbsserv_1");
            dataObject.addField("custId", custid);
            dataObject.addField("p_td_ref_no", kony.retailBanking.globalData.accountsDashboardData.dealsData[parseInt(context.rowIndex)].reference_no.toString());
            dataObject.addField("p_acc_No", kony.retailBanking.globalData.accountsDashboardData.dealsData[parseInt(context.rowIndex)].accountID.toString());
            dataObject.addField("p_Branch", 1);
            dataObject.addField("AccountFlag", "D");
            dataObject.addField("p_lan", langSelected);
            var serviceOptions = {
                "dataObject": dataObject,
                "headers": headers
            };
            // kony.print("dealsDatadealsData::"+JSON.stringify(kony.retailBanking.globalData.accountsDashboardData.dealsData));
            kony.print("dataObject::" + JSON.stringify(dataObject));
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            if (kony.sdk.isNetworkAvailable()) {
                modelObj.customVerb("getDetails", serviceOptions, accountDetailsSuccess, accountDetailsError);
            } else {
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
            }

            /* frmAccountDetailKA.accountDetailsScrollContainer.setVisibility(false);
frmAccountDetailKA.flxLoansScreen.setVisibility(false);
frmAccountDetailKA.flxDealsScreen.setVisibility(true);
frmAccountDetailKA.flxDealsScreen.left="0%"; */

        } else if (segForm === "segAccountsKALoans") {
            kony.print("segAccountsKALoans::");

            DealsLoansCurrIndex = parseInt(context.rowIndex);
            var segData = frmAccountsLandingKA.segAccountsKALoans.data;
            kony.print("segAccountsKALoans segData ::" + JSON.stringify(segData));
            frmAccountInfoKA.flxPageIndicator.setVisibility(true);
            frmAccountDetailKA.flxDealsScreen.setVisibility(false);
            frmAccountDetailKA.flxLoansScreen.setVisibility(true);
            gblAccountsCurrencyType = "JOD";
            if (!isEmpty(frmAccountsLandingKA.segAccountsKALoans.data[parseInt(context.rowIndex)].currencyCode)) {
                gblAccountsCurrencyType = frmAccountsLandingKA.segAccountsKALoans.data[parseInt(context.rowIndex)].currencyCode;
            }
            //create_PageIndicatorInfo(frmAccountInfoKA, 50, frmAccountsLandingKA.segAccountsKALoans.data.length,DealsLoansCurrIndex);
            kony.print("reference_no ::" + kony.retailBanking.globalData.accountsDashboardData.loansData[parseInt(context.rowIndex)].reference_no);
            kony.print("accountID ::" + kony.retailBanking.globalData.accountsDashboardData.loansData[parseInt(context.rowIndex)].accountID);
           var AccName =  frmAccountsLandingKA.segAccountsKALoans.data[parseInt(context.rowIndex)].nickName.text.toString();
          var accountName = AccName.substring(AccName.lastIndexOf("<label>")+1,AccName.lastIndexOf("</label>"))
            frmAccountInfoKA.accountNumberLabel.text =accountName.substring(6,accountName.length);
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var options = {
                "access": "online",
                "objectName": "RBObjects"
            };
            var headers = {};
            var serviceName = "RBObjects";
            var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
            var dataObject = new kony.sdk.dto.DataObject("Accounts");
//             dataObject.addField("usr", "icbsserv");
//             dataObject.addField("pass", "icbsserv_1");
            dataObject.addField("custId", custid);
            dataObject.addField("p_loan_ref_no", kony.retailBanking.globalData.accountsDashboardData.loansData[parseInt(context.rowIndex)].reference_no.toString());
            dataObject.addField("p_acc_No", kony.retailBanking.globalData.accountsDashboardData.loansData[parseInt(context.rowIndex)].accountID.toString());
            dataObject.addField("p_Branch", 11);
            dataObject.addField("p_acct_Type", "217");
            dataObject.addField("AccountFlag", "L");
            dataObject.addField("p_lan", langSelected);
            var serviceOptions = {
                "dataObject": dataObject,
                "headers": headers
            };
            // kony.print("dataObjectLoans::"+JSON.stringify(kony.retailBanking.globalData.accountsDashboardData.loansData));
            kony.print("dataObjectLoans::" + JSON.stringify(dataObject));
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            if (kony.sdk.isNetworkAvailable()) {
                modelObj.customVerb("getDetails", serviceOptions, accountDetailsSuccess, accountDetailsError);
            } else {
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
            }
            //     accountInfo("loans");

            /*  frmAccountDetailKA.accountDetailsScrollContainer.setVisibility(false);
frmAccountDetailKA.flxDealsScreen.setVisibility(false);
frmAccountDetailKA.flxLoansScreen.setVisibility(true);
frmAccountDetailKA.flxLoansScreen.left="0%"; */

        }

    } catch (e) {
		exceptionLogCall("onheaderDashboard","UI ERROR","UI",e);
        kony.print("Exception_onheaderDashboard ::" + e);
    }
}

function accountDetailsSuccess(response) {
    kony.print("AccountDetailsSuccess ::" + JSON.stringify(response));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

    if (!isEmpty(response.errmsg)) {
        if (response.errmsg.length > 0) {
            var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
            customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
        }
    } else {
        if (response.DepositeDetail != undefined) {
            if (frmAccountsLandingKA.segAccountsKADeals.data.length <= 1) {
                frmAccountInfoKA.flxPageIndicator.setVisibility(false);
            } else {
                frmAccountInfoKA.flxPageIndicator.setVisibility(true);
                create_PageIndicatorInfo(frmAccountInfoKA, 50, frmAccountsLandingKA.segAccountsKADeals.data.length, DealsLoansCurrIndex);
            }

            accountInfo("deals", response.DepositeDetail);
        } else if (response.LoanDetails != undefined) {
            if (frmAccountsLandingKA.segAccountsKALoans.data.length <= 1) {
                frmAccountInfoKA.flxPageIndicator.setVisibility(false);
            } else {
                frmAccountInfoKA.flxPageIndicator.setVisibility(true);
                create_PageIndicatorInfo(frmAccountInfoKA, 50, frmAccountsLandingKA.segAccountsKALoans.data.length, DealsLoansCurrIndex);
            }

            accountInfo("loans", response.LoanDetails);
        }
    }
}

function accountDetailsError(error) {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    var errorCode = "";
    kony.print("AccountDetailsError ::" + JSON.stringify(error));
    if (error.code !== null) {
        errorCode = error.code;
    } else if (error.opstatus !== null) {
        errorCode = error.opstatus;
    } else if (error.errorCode !== null) {
        errorCode = error.errorCode;
    }
    var errorMessage = getServiceErrorMessage(errorCode,"accountDetailsError");
	
    customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");

}




function fetch_TransactionList(serv_Data,transaction_DATE) {

//     clearForm.filterTransactions();
	var no_TNX = "100";
  kony.print("transaction_DATE ::"+JSON.stringify(transaction_DATE));
	frmFilterTransaction.flxFilterDone.setEnabled(true);
	if(transaction_DATE !== undefined && transaction_DATE !== null && transaction_DATE !== ""){
    	if(transaction_DATE.no_TNX !== undefined && transaction_DATE.no_TNX !== ""){
        	no_TNX = transaction_DATE.no_TNX;
        }else if(transaction_DATE.toDATE !== "" && transaction_DATE.fromDATE !== ""){
          no_TNX = "";
        }
    }
    frmAccountDetailKA.transactionSegment.isVisible = true;
    frmAccountDetailKA.lblExport.setEnabled(true);
    frmAccountDetailKA.lblNoResult.isVisible = false;
    kony.print("fetch_TransactionList ::" + JSON.stringify(serv_Data));
	kony.print("fetch_TransactionList Date :: " +transaction_DATE.fromDATE+" :: "+transaction_DATE.toDATE+" :: "+no_TNX);
    gblAccountsCurrencyType = serv_Data.currencyCode;
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmAccountDetailKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    kony.print("current Locale ::" + kony.i18n.getCurrentLocale());
	var queryparams = {
            "custId": custid,
            "accountNumber": serv_Data.accountID,
            "p_Branch": serv_Data.branchNumber,
            "p_no_Of_Txn": no_TNX,
        	"p_toDate":transaction_DATE.toDATE || "",
        	"p_fromDate":transaction_DATE.fromDATE || "",
            "lang": (kony.store.getItem("langPrefObj")==="en")?"eng":"ara"
        };
	kony.print("Qurey Patameters ::"+JSON.stringify(queryparams));
    navObject.setRequestOptions("transactionSegment", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        },
        "queryParams": queryparams
    });
    controller.loadDataAndShowForm(navObject);
}

function accountDashboardDataCall() {
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      if(!isEmpty(gblLaunchModeOBJ.lauchMode) && gblLaunchModeOBJ.lauchMode && (!isEmpty(gblLaunchModeOBJ.accno) || !isEmpty(gblLaunchModeOBJ.iban))){
        kony.print("gblLaunchModeOBJ::"+JSON.stringify(gblLaunchModeOBJ));
        removeDatafrmTransfersform();
        makeInlineErrorsTransfersInvisible();
        gblTModule="send";
        gblOnceSetBranch =true;
        getAllAccounts();
       
        //kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      }else{
        kony.print("accountDashboardDataCall ::::");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var controller = INSTANCE.getFormController("frmAccountsLandingKA");
        var navObject = new kony.sdk.mvvm.NavigationObject();
        navObject.setRequestOptions("segAccountsKA", {
            "headers": {
                "session_token": kony.retailBanking.globalData.session_token
            },
            "queryParams": {
                "custId": custid,
            	"lang": kony.store.getItem("langPrefObj")==="en"?"eng":"ara"
            }
        });
        controller.loadDataAndShowForm(navObject);
        //kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Dashboard ::::Done");
      }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        //alert("" + kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"));
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function depositOnetimeCall() {
    kony.print("depositOnetimeCall:");
    var context = {
        "sectionIndex": 0,
        "rowIndex": 0,
        "widgetInfo": {
            "id": "segAccountsKADeals"
        }
    };
    kony.print("accountDashboardFlowFlag" + kony.retailBanking.globalData.globals.accountDashboardFlowFlag);
    onheaderDashboard(frmAccountsLandingKA.segAccountsKADeals.data, context);
}



function LoanOnetimeCall() {
    kony.print("LoanOnetimeCall:");
    var context = {
        "sectionIndex": 0,
        "rowIndex": 0,
        "widgetInfo": {
            "id": "segAccountsKALoans"
        }
    };
    kony.print("accountDashboardFlowFlag" + kony.retailBanking.globalData.globals.accountDashboardFlowFlag);
    onheaderDashboard(frmAccountsLandingKA.segAccountsKALoans.data, context);
}

function accountOnetimeCall() {
    kony.print("accountOnetimeCall:");
    var context = {
        "sectionIndex": 0,
        "rowIndex": 0,
        "widgetInfo": {
            "id": "segAccountsKA"
        }
    };
    kony.print("accountDashboardFlowFlag" + kony.retailBanking.globalData.globals.accountDashboardFlowFlag);
    kony.retailBanking.globalData.globals.accountDashboardFlowFlag = true;
    onheaderDashboard(frmAccountsLandingKA.segAccountsKA.data, context);
}

function serv_cardStatement(data) {
    try {
        var cardnum = "";
        if (kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].cardTypeFlag == "C") {
            cardnum = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num;
        } else {
            cardnum = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_number;
        }
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Cards", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Cards");
//         dataObject.addField("usr", "icbsserv");
//         dataObject.addField("pass", "icbsserv_1");
        dataObject.addField("custId", custid);
        dataObject.addField("Year", data.Year);
        dataObject.addField("Month", data.Month);
        dataObject.addField("CardNum", cardnum);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("dataobject:: " + JSON.stringify(serviceOptions));
        if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb("GetCardStmt", serviceOptions, serv_successCardStatement, serv_errorCardStatement);
        }
    } catch (e) {
        kony.print("Exception_serv_cardStatement ::" + e);
        exceptionLogCall("serv_cardStatement","UI ERROR","UI",e);
        kony.application.dismissLoadingScreen();
    }
}

function serv_successCardStatement(response) {
    try {
        kony.print("in success :: " + JSON.stringify(response));
      if(!isEmpty(response.CreditCardStatement)){
        if(response.CreditCardStatement.length >0){
        for (var i in response.CreditCardStatement) {
            response.CreditCardStatement[i].description = response.CreditCardStatement[i].dr_cr_ind_for_trn_amt;// + "-" + response.CreditCardStatement[i].merchant;
            response.CreditCardStatement[i].amount = response.CreditCardStatement[i].trn_amt+" "+getISOCurrency(response.CreditCardStatement[i].trn_ccy);
            var transDate = response.CreditCardStatement[i].trn_date;
            if(!isEmpty(transDate)){
              transDate = transDate.split('-');
                transDate = transDate[2]+"/"+transDate[1]+"/"+transDate[0];     	
            }
            response.CreditCardStatement[i].transactionDate = transDate;
            if (response.CreditCardStatement[i].description.length > 30) {
                response.CreditCardStatement[i].description = response.CreditCardStatement[i].description.substring(0, 30) + "...";
            }
        }
        frmCardStatementKA.transactionSegment.widgetDataMap = {
            "transactionDate": "transactionDate",
            "transactionName": "description",
            "transactionAmount": "amount",
        	"lblCardStatementmerchant":"merchant"
        };
        frmCardStatementKA.transactionSegment.setData(response.CreditCardStatement);
         frmCardStatementKA.transactionSegment.setVisibility(true);
          frmCardStatementKA.LabelNoRecordsKA.setVisibility(false);
         
      }else{
        frmCardStatementKA.transactionSegment.setVisibility(false);
          frmCardStatementKA.LabelNoRecordsKA.setVisibility(true);
        frmCardStatementKA.transactionSegment.setData([]);
      }
        
      }else{
         frmCardStatementKA.transactionSegment.setVisibility(false);
          frmCardStatementKA.LabelNoRecordsKA.setVisibility(true);
        frmCardStatementKA.transactionSegment.setData([]);
      }
        kony.print("Statement data from segment ::" + JSON.stringify(frmCardStatementKA.transactionSegment.data));
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    	frmCardStatementKA.flxRetriveStatement.setVisibility(false);
    	frmCardStatementKA.flxCardsFilterScreen.isVisible = false;
        frmCardStatementKA.flxCardStatement.isVisible = true;
    	frmCardStatementKA.flxFilterDone.isVisible = false;
        frmCardStatementKA.show();
    } catch (e) {
        kony.print("Exception_" + e);
        customAlertPopup(geti18nkey("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
		exceptionLogCall("onheaderDashboard","UI ERROR","UI",e);
        kony.application.dismissLoadingScreen();
    }
}

function serv_errorCardStatement(error) {
  var errmsg = geti18Value("i18n.common.somethingwentwrong");
  try {
    kony.print("in error :: " + JSON.stringify(error));
    if (error.code !== undefined && error.code !== null && error.message !== undefined && error.message !== null) {
      errmsg =  error.message;
    } 

    frmCardStatementKA.flxCardsFilterScreen.isVisible = false;
    frmCardStatementKA.flxCardStatement.isVisible = true;
    frmCardStatementKA.flxFilterDone.isVisible = false;
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  } catch (e) {
    kony.print("Exception_serv_errorCardStatement ::" + e);
    exceptionLogCall("serv_successCardStatement","UI ERROR","UI",e);
    kony.application.dismissLoadingScreen();
  }
  customAlertPopup(geti18nkey("i18n.NUO.Error"),errmsg , popupCommonAlertDimiss, "");
  exceptionLogCall("serv_successCardStatement","UI ERROR","UI",errmsg);
}

function fetch_TRANSACTIONLIST(serv_Data,transaction_DATE){
	try{
    	if (kony.sdk.isNetworkAvailable()) {
      		kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        	var inputParams = {
                "custId": custid,
                "accountNumber": serv_Data.accountID,
                "p_Branch": serv_Data.branchNumber,
                "p_no_Of_Txn": "",
                "p_toDate":transaction_DATE.toDATE || "",
                "p_fromDate":transaction_DATE.fromDATE || "",
                "lang": (kony.store.getItem("langPrefObj")==="en")?"eng":"ara"
            };
        	kony.print("queryParams ::"+JSON.stringify(inputParams));
        	var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetTransactionDetails");
            appMFConfiguration.invokeOperation("prGetTransactionsDetails", {}, inputParams, success_TRANSLIST, failure_TRANSLIST);
        } else {
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
          customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    }catch(e){
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    	kony.print("Exception_fetch_TRANSACTIONLIST ::"+e);
    }
	frmFilterTransaction.flxFilterDone.setEnabled(true);
}

function success_TRANSLIST(response){
	try{
      kony.print("response ::"+JSON.stringify(response));
      var contains_DATA = false;
      if(response.opstatus === "0" || response.opstatus == 0){
        if(response.Transactions !== undefined && response.Transactions !== null && response.Transactions !== ""){
        var trans = response.Transactions;
        response = {};
        response = {transactionSegment :{}};
        response.transactionSegment = trans;
        var tempdata=setAccountDetailData(response,"transactionSegment");
        kony.print("tempData ::"+JSON.stringify(tempdata));
        if(tempdata[0] !== undefined && tempdata[0] !== null && tempdata[0].length > 0){
            frmAccountDetailKA.transactionSegment.removeAll();
            frmAccountDetailKA.LabelNoRecordsKA.isVisible = false;
            frmAccountDetailKA.transactionSegment.isVisible = true;
            if(validate_FILTEROPTIONS(filter_OPTIONS))
              frmAccountDetailKA.transactionSegment.setData(filterAccountTransactions(filter_OPTIONS, kony.application.getPreviousForm().transactionSegment, tempdata[0], kony.application.getPreviousForm()));
            else
              frmAccountDetailKA.transactionSegment.setData(tempdata[0]);
        	contains_DATA = true;
        }
        }
        kony.print("contains_DATA ::"+contains_DATA);
        if(!contains_DATA){
            frmAccountDetailKA.LabelNoRecordsKA.isVisible = true;
            frmAccountDetailKA.transactionSegment.isVisible = false;
            frmAccountDetailKA.transactionSegment.removeAll();
        }
        frmAccountDetailKA.show();
      }else{
      	customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
      }
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }catch(e){
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    	kony.print("Exception_success_TRANSLIST ::"+e);
    }
	frmFilterTransaction.flxFilterDone.setEnabled(true);
}

function failure_TRANSLIST(err){
	try{
    	customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }catch(e){
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    	kony.print("Exception_failure_TRANSLIST ::"+e);
    }
	frmFilterTransaction.flxFilterDone.setEnabled(true);
}



function setDefaultAccountsSetTabView(){
  frmAccountDetailKA.lblListView.skin = "lblListOn"; 
  frmAccountDetailKA.imgIndicator.setVisibility(true);
  frmAccountDetailKA.lblAccountOptions.skin = "lblFontSettIconsFont2";
  frmAccountDetailKA.imgDivSett.setVisibility(false);
  frmAccountDetailKA.flxAccountOptions.setVisibility(false);
  frmAccountDetailKA.flxSegmentWrapper.setVisibility(true);
}


function setDefaultAccountsInfoSetTabs(){
  frmAccountInfoKA.lblListInfoInfo.skin = "lblListOn"; 
  frmAccountInfoKA.imgDiv.setVisibility(true);
  frmAccountInfoKA.lblAccountOptions.skin = "lblFontSettIconsFont2";
  frmAccountInfoKA.imgDivSett.setVisibility(false);
frmAccountInfoKA.flxAccountOptions.setVisibility(false);
if(frmAccountInfoKA.lblLoanAccountInfo.text == kony.i18n.getLocalizedString("i18n.accounts.accounts"))
frmAccountInfoKA.flxaccountdetailsfordeposits.setVisibility(true);
if(frmAccountInfoKA.lblLoanAccountInfo.text == kony.i18n.getLocalizedString("i18n.accounts.loans"))
frmAccountInfoKA.flxLoanInfo.setVisibility(true);
if(frmAccountInfoKA.lblLoanAccountInfo.text == kony.i18n.getLocalizedString("i18n.accounts.deals"))
frmAccountInfoKA.flxDeposits.setVisibility(true);
}




function serv_cardStatementDownLoadPDFXL(data) {
    try {
      kony.print("serv_cardStatementDownLoadPDFXL"+JSON.stringify(data));
        var cardnum = "",splitfrom = null,splitto = null;
      var callProfiledata = callProfileServiceforCustName();
        if (kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].cardTypeFlag == "C") {
            cardnum = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num;
        } else {
            cardnum = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_number;
        }
      var cardLimit = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_limit;
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Cards", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Cards");
//         dataObject.addField("usr", "icbsserv");
//         dataObject.addField("pass", "icbsserv_1");
      var branchName = "";
          if(!isEmpty(customerAccountDetails.profileDetails.branch_code))
          for(var i in kony.boj.detailsForBene.Branch){
        	if(kony.boj.detailsForBene.Branch[i].BRANCH_CODE === customerAccountDetails.profileDetails.branch_code){
            	branchName = kony.boj.detailsForBene.Branch[i].BRANCH_NAME;
      			break;
            }
        }
      var custName = (!isEmpty(customerAccountDetails.profileDetails.customer_name) ? customerAccountDetails.profileDetails.customer_name : "");
      gbldownloadPDFFlag = data.Type;
        dataObject.addField("custId", custid);
//         dataObject.addField("Year", data.Year);
//         dataObject.addField("Month", data.Month);
    	splitfrom = data.dateFrom.split("/");
    	dataObject.addField("DateFrom", (parseInt(splitfrom[0])>9?splitfrom[0]:"0"+splitfrom[0])+""+(parseInt(splitfrom[1])>9?splitfrom[1]:"0"+splitfrom[1])+""+splitfrom[2]);
    	splitto = null;
    	splitto = data.dateTo.split("/");
    	dataObject.addField("DateTo", (parseInt(splitto[0])>9?splitto[0]:"0"+splitto[0])+""+(parseInt(splitto[1])>9?splitto[1]:"0"+splitto[1])+""+splitto[2]);
    	dataObject.addField("serviceFlag", data.serviceFlag);
        dataObject.addField("CardNum", cardnum);
        dataObject.addField("exportType", data.Type);
        dataObject.addField("cardLimit", cardLimit);
        dataObject.addField("custName", custName);
        dataObject.addField("branchName", branchName);
        dataObject.addField("stmtData", "");
      	dataObject.addField("tDate", "");
      	dataObject.addField("p_trn_nbr", "");
    	dataObject.addField("pass", "icbsserv_1");
		dataObject.addField("usr", "icbsserv");
        //dataObject.addField("serviceFlag", "CS");    // CS - Card statement ,   CT - Card txn
       
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("dataobject:: " + JSON.stringify(serviceOptions));
        if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb("exportCardTransactions", serviceOptions, serv_cardStatementDownLoadPDFXLSuccsss,serv_cardStatementDownLoadPDFXLError);//exportStmt
        }
    } catch (e) {
        kony.print("Exception_serv_cardStatement ::" + e);
        exceptionLogCall("serv_cardStatement","UI ERROR","UI",e);
        kony.application.dismissLoadingScreen();
    }
}

function serv_cardStatementDownLoadPDFXLSuccsss(response) {
    try {
        kony.print("in success :: " + JSON.stringify(response));
      var type  = "pdf";
      if(!isEmpty(response.exportData)){
        if(gbldownloadPDFFlag == "PDF"){
         type  = "pdf";
            }else{
              type = "xlsx";
            }
       var returnedValue = 0;
      var time_CARD = new Date().getDate()+""+(new Date().getMonth())+1+""+new Date().getFullYear()+""+new Date().getHours()+""+new Date().getMinutes()+""+new Date().getSeconds();
      var file_NAME = "CardStatement_"+time_CARD+".";
       kony.print("Exported file name :: "+file_NAME);
       //#ifdef android
         kony.print("Start");
      returnedValue = NSPDFViewer.savepdf(
		response.exportData, 
		file_NAME+""+type);
        kony.print("DONE"+returnedValue);
        if(parseInt(returnedValue) === 1){
        //var mainLocNPWP = kony.io.FileSystem.getExternalStorageDirectoryPath();
          //geti18Value("i18n.cardstmntDownloaded") +
         //var returnedValue1 = NSPDFViewer.openpdf(file_NAME+""+type);
          
          //kony.print("returnedValue1"+returnedValue1);
        customAlertPopup("",  "Card statement ("+file_NAME+type +") downloaded in the Internal Storage" , popupCommonAlertDimiss, openDocsandriod,kony.i18n.getLocalizedString("i18n.settings.ok"),kony.i18n.getLocalizedString("i18n.common.opens"));
        customAlertPopup(geti18nkey("i18n.appUpgrade.upgradeOfAnApp"), kony.i18n.getLocalizedString("i18n.appUgrademsg.text"), navtoPlayStore, exitAppOk, kony.i18n.getLocalizedString("i18n.common.Text"), kony.i18n.getLocalizedString("i18n.common.cancelC"));

        }
        else
        customAlertPopup("", geti18Value("i18n.cardstmntNoData"), popupCommonAlertDimiss, "");  
        //#endif
        
        //#ifdef iphone
     //Creates an object of class 'savePDF'
 if(savePDFObject === null || savePDFObject === undefined){
   kony.print("savePDFObjectif1:");
   savePDFObject = new PDFViewer.savePDF();
   kony.print("savePDFObjectif:");
 }else{
   kony.print("savePDFObjectelse:");
 }
   kony.print("fficall Iphone");
//Invokes method 'showPDF' on the object
        var fileNamee = file_NAME+""+type;
savePDFObject.showPDF(response.exportData,fileNamee);
        kony.print("DONE Iphone");
        //#endif
         kony.application.dismissLoadingScreen();
      }else{
        customAlertPopup("", geti18Value("i18n.cardstmntNoData"), popupCommonAlertDimiss, "");
        kony.application.dismissLoadingScreen();
      }       
       
    } catch (e) {
        kony.print("Exception_" + e);
        customAlertPopup(geti18nkey("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
		exceptionLogCall("serv_cardStatementDownLoadPDFXLSuccss","UI ERROR","UI",e);
        kony.application.dismissLoadingScreen();
    }
}

function openDocsandriod(){
   var returnedValue1 = NSPDFViewer.openpdf(file_NAME+""+type);
   popupCommonAlert.dismiss();
}

function serv_cardStatementDownLoadPDFXLError(error) {
  var errmsg = geti18Value("i18n.common.somethingwentwrong");
  try {
    kony.print("in error :: " + JSON.stringify(error));
    if (error.code !== undefined && error.code !== null && error.message !== undefined && error.message !== null) {
      errmsg =  error.message;
    } 

     kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  } catch (e) {
    kony.print("Exception_serv_errorCardStatement ::" + e);
    //exceptionLogCall("serv_cardStatementDownLoadPDFXLError","UI ERROR","UI",e);
    kony.application.dismissLoadingScreen();
  }
  customAlertPopup(geti18nkey("i18n.NUO.Error"),errmsg , popupCommonAlertDimiss, "");
  //exceptionLogCall("serv_cardStatementDownLoadPDFXLError","UI ERROR","UI",errmsg);
}



function navigatetoDownloadPDFXSLT(){
gblDownloadPDFFlow = true;
kony.print("navigatetoDownloadPDFXSLT");
frmCardStatementKA.flxCardsFilterScreen.setVisibility(false);
frmCardStatementKA.flxRetriveStatement.setVisibility(true);
//frmCardStatementKA.flxDownloadType.isVisible = true;
// frmCardStatementKA.lblDownloadPDFXLT.setVisibility(true);
//frmCardStatementKA.flxCardsFilterScreen.isVisible = true;
frmCardStatementKA.flxCardStatement.isVisible = false;
frmCardStatementKA.flxFilterDone.isVisible = true;
frmCardStatementKA.lblCardDate.text = "";
frmCardStatementKA.lblCardYear.text = "";
frmCardStatementKA.lblDownloadType.text = "";
frmCardStatementKA.lblDateFrom.text = "";
frmCardStatementKA.lblDateTo.text = "";
//frmCardStatementKA.Label0d6e6be1b179e44.skin = "sknLblNextDisabled";
frmCardStatementKA.show();
}






function getPermissionforStorage(){
  
  var result = kony.application.checkPermission(kony.os.RESOURCE_EXTERNAL_STORAGE);
  if(result.status == kony.application.PERMISSION_DENIED){ 
    //Indicates permission denied
    if(result.canRequestPermission){
      kony.application.requestPermission(kony.os.RESOURCE_EXTERNAL_STORAGE, permissionStatusCallback);
    }
    else{
      //alert("Please enable the permission in Device Settings to proceed. Open settings");
      customAlertPopup(geti18nkey("i18n.maps.Info"), geti18Value("i18n.SMS.enablePermission"), popupCommonAlertDimiss,"");
      // kony.timer.schedule("scanCard",timerScanCard, 3, false);
     }
  }else if(result.status == kony.application.PERMISSION_GRANTED){
     kony.print("PERMISSION_GRANTED");
   //serv_cardStatementDownLoadPDFXL({"Year":frmCardStatementKA.lblCardYear.text,"Month":frmCardStatementKA.lblCardDate.text,"Type":frmCardStatementKA.lblDownloadType.text}); 
    serv_cardStatementDownLoadPDFXL({"dateFrom":frmCardStatementKA.lblDateFrom.text,"dateTo":frmCardStatementKA.lblDateTo.text,"Type":frmCardStatementKA.lblDownloadType.text,"serviceFlag":"CT"});
        
  }
  else if(result.status == kony.application.PERMISSION_RESTRICTED){
    kony.print("PERMISSION_RESTRICTED");
 customAlertPopup(geti18nkey("i18n.maps.Info"), geti18Value("i18n.SMS.enablePermission"), popupCommonAlertDimiss,"");
      
  }


  function permissionStatusCallback(response){
kony.print("JSON:::" + JSON.stringify(response));
    if(response.status == "50002"){
      //serv_cardStatementDownLoadPDFXL({"Year":frmCardStatementKA.lblCardYear.text,"Month":frmCardStatementKA.lblCardDate.text,"Type":frmCardStatementKA.lblDownloadType.text}); 
      serv_cardStatementDownLoadPDFXL({"dateFrom":frmCardStatementKA.lblDateFrom.text,"dateTo":frmCardStatementKA.lblDateTo.text,"Type":frmCardStatementKA.lblDownloadType.text,"serviceFlag":"CT"});
       } else if(response.status == "50001"){ 
       customAlertPopup(geti18nkey("i18n.maps.Info"), geti18Value("i18n.SMS.enablePermission"), popupCommonAlertDimiss,"");
       }
  }

}