function newTransferFrequencyEdit(height){
  
	frmNewTransferKA.frequencyCard.animate(
        kony.ui.createAnimation({"100":{"height": frequencyCardHeight,"stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration},
        {"animationEnd": function () {}}
    );
    
    frmNewTransferKA.frequencyCardInner.animate(
    kony.ui.createAnimation({"100": { //"transform": transform, 
    "top": "60dp",
    "opacity": 1, "stepConfig": { "timingFunction": easeIn}}}), 
    {"fillMode": forwards, "duration": duration},
    {"animationStart": function() {frmNewTransferKA.frequencyCardInner.skin = skntransferCardInner;},
    "animationEnd": function() {}}
    );
  
  frmNewTransferKA.frequencyLabel.animate(
        kony.ui.createAnimation({"100":{"left":"-65dp","stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards,"duration": duration/2},
        {"animationEnd": function () {}}
    );
  
   frmNewTransferKA.frequencyPick.animate(
        kony.ui.createAnimation({
           "100":{"opacity": 0, "stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration/1.5, "delay": 0.2},
        {"animationEnd": function () {}}
    );
  
  frmNewTransferKA.frequencyPickContainer.animate(
        kony.ui.createAnimation({
           "100":{"top": "100%", "stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration, "delay": 0},
        {"animationEnd": function () {}}
    );
  
}

function newTransferFrequencyMadePick(height){
  
	frmNewTransferKA.frequencyCard.animate(
         kony.ui.createAnimation({"100":{"height": height,"stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration},
        {"animationEnd": function () {}}
	);
    
    frmNewTransferKA.frequencyCardInner.animate(
    kony.ui.createAnimation({"100": { //"transform": transform, 
    "top": "100%",
    "opacity": 0, "stepConfig": { "timingFunction": easeIn}}}), 
    {"fillMode": forwards, "duration": duration},
    {"animationStart": function() {frmNewTransferKA.frequencyCardInner.skin = skntransferCardInner;},
    "animationEnd": function() {}}
    );
  
  	frmNewTransferKA.frequencyLabel.animate(
        kony.ui.createAnimation({"100":{"left": labelLeft,"stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards,"duration": duration/2},
        {"animationEnd": function () {}}
    );
  
  	frmNewTransferKA.frequencyPick.animate(
        kony.ui.createAnimation({"100":{"opacity": 1, "stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration/1.5, "delay": 0.05},
        {"animationEnd": function () {}}
    );
  
  	 frmNewTransferKA.frequencyPickContainer.animate(
        kony.ui.createAnimation({
           "100":{"top": "0%", "stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration, "delay": 0},
        {"animationEnd": function () {}}
    );
}

function newTransferRecurrenceEdit(height){
  
	frmNewTransferKA.recurrenceCard.animate(
        kony.ui.createAnimation({"100":{"height": recurrenceCardHeight,"stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration},
        {"animationEnd": function () {}}
    );
    
    frmNewTransferKA.recurrenceCardInner.animate(
    kony.ui.createAnimation({"100": { //"transform": transform, 
    "top": "60dp",
    "opacity": 1, "stepConfig": { "timingFunction": easeIn}}}), 
    {"fillMode": forwards, "duration": duration},
    {"animationStart": function() {frmNewTransferKA.recurrenceCardInner.skin = skntransferCardInner;},
    "animationEnd": function() {}}
    );
   
   frmNewTransferKA.recurrencePick.animate(
        kony.ui.createAnimation({
           "100":{"opacity": 0, "stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration/1.5, "delay": 0.2},
        {"animationEnd": function () {}}
    );
  
  frmNewTransferKA.recurrencePickContainer.animate(
        kony.ui.createAnimation({
           "100":{"top": "100%", "stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration, "delay": 0},
        {"animationEnd": function () {}}
    ); 
    
  frmNewTransferKA.recurrenceLabel.animate(
    kony.ui.createAnimation({"100":{"left":"-65dp","stepConfig":{"timingFunction": easeIn}}}),
    {"fillMode": forwards,"duration": duration},
    {"animationEnd": function () {}}
    );
}

function newTransferRecurrenceMadePick(height){
    kony.print("newTransferRecurrenceMadePick called");
	frmNewTransferKA.recurrenceCard.animate(
         kony.ui.createAnimation({"100":{"height": height,"stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration},
        {"animationEnd": function () {}}
	);
    
    frmNewTransferKA.recurrenceCardInner.animate(
    kony.ui.createAnimation({"100": { //"transform": transform, 
    "top": "100%",
    "opacity": 0, "stepConfig": { "timingFunction": easeIn}}}), 
    {"fillMode": forwards, "duration": duration},
    {"animationStart": function() {frmNewTransferKA.recurrenceCardInner.skin = skntransferCardInner;},
    "animationEnd": function() {}}
    );
   
  	frmNewTransferKA.recurrencePick.animate(
        kony.ui.createAnimation({"100":{"opacity": 1, "stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration/1.5, "delay": 0.05},
        {"animationEnd": function () {}}
    );
  
  	 frmNewTransferKA.recurrencePickContainer.animate(
        kony.ui.createAnimation({
           "100":{"top": "0%", "stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": duration, "delay": 0.2},
        {"animationEnd": function () {}}
    );
    
    frmNewTransferKA.recurrenceLabel.animate(
    kony.ui.createAnimation({"100":{"left": labelLeft,"stepConfig":{"timingFunction": easeIn}}}),
    {"fillMode": forwards,"duration": duration, "delay": 0.2}, // duration/2
    {"animationEnd": function () {}}
    );
}

function serv_INREMIT_TRANSACTIONSTATUS(){
	try{
    	var servData = prepare_LOOPSERVICEDATA_REMIT_TRANSACTIONSTATUS();
    	kony.print("remit Serv data ::"+JSON.stringify(servData));
    	var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("LoopInRemitDetails");
      	if (kony.sdk.isNetworkAvailable()) {
          kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
          appMFConfiguration.invokeOperation("LoopInRemitDetails", {}, 
                                           servData, success_serv_INREMIT_TRANSACTIONSTATUS,function(err){kony.print("Error Data ::"+JSON.stringify(err));});
        }
    }catch(e){
    	kony.print("Exception_serv_INOUTREMIT_TRANSACTIONSTATUS ::"+e);
    }
}

function serv_OUTREMIT_TRANSACTIONSTATUS(){
	try{
    	var servData = prepare_LOOPSERVICEDATA_REMIT_TRANSACTIONSTATUS();
    	kony.print("remit Serv data ::"+JSON.stringify(servData));
    	var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("LoopOutRemitDetails");
    	if (kony.sdk.isNetworkAvailable()) {
    		kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    		appMFConfiguration.invokeOperation("LoopOutRemitDetails", {}, 
                                           servData, success_serv_OUTREMIT_TRANSACTIONSTATUS,function(err){
              																								kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
              																								customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.common.defaultErrorMsg"), popupCommonAlertDimiss, "");
            																								});
        }
    }catch(e){
    	kony.print("Exception_serv_INOUTREMIT_TRANSACTIONSTATUS ::"+e);
    }
}

function prepare_LOOPSERVICEDATA_REMIT_TRANSACTIONSTATUS(){
	try{
    	var accountData = kony.retailBanking.globalData.accountsDashboardData.accountsData;
    	var temp = {"custid":"",
                    "p_fcdb_ref_no":"",
                    "accountNo":"",
                    "branchCode":"",
                    "txnCur":"",
                    "fromAmount":"",
                    "toAmount":"",
                    "fromDate":"",
                    "toDate":"",
                    "p_txn_ref_no":"",
                    "lang":"",
                    "p_channel":"",
                    "p_user_id":"",
                    "loop_seperator": ":",
                    "loop_count":""};
    	var langObj = kony.store.getItem("langPrefObj");
    	if(langObj.toUpperCase() === "EN"){
        	langObj = "eng";
        }else{
        	langObj = "ara";
        }
    	for(var i in accountData){
        	if(((accountData[i].accountType === "S" || accountData[i].accountType === "C" || accountData[i].accountType === "Y" || accountData[i].accountType === "CC" || accountData[i].accountType === "B") && (accountData[i].productCode !== "320") && (accountData[i].favouriteStatus === "A"))){
            	temp.custid = (i == accountData.length)?temp.custid+""+custid:temp.custid+""+custid+":";
            	temp.accountNo = (i == accountData.length)?temp.accountNo+""+accountData[i].accountID:temp.accountNo+""+accountData[i].accountID+":";
            	temp.branchCode = (i == accountData.length)?temp.branchCode+""+accountData[i].branchNumber:temp.branchCode+""+accountData[i].branchNumber+":";
            	temp.lang = (i == accountData.length)?temp.lang+""+langObj:temp.lang+""+langObj+":";
            	temp.p_channel = (i == accountData.length)?temp.p_channel+"MOBILE":temp.p_channel+"MOBILE:";
            	temp.p_user_id = (i == accountData.length)?temp.p_user_id+"BOJMOB":temp.p_user_id+"BOJMOB:";
            }
        }
    	temp.loop_count = accountData.length;
    	return temp;
    }catch(e){
    	kony.print("Exception_prepare_LOOPSERVICEDATA_REMIT_TRANSACTIONSTATUS ::"+e);
    }
}

function success_serv_INREMIT_TRANSACTIONSTATUS(res){
	try{
    	var tempData = [];
    	kony.print("remit Success ::"+JSON.stringify(res));
    	if(res.LoopDataset !== undefined && res.LoopDataset.length > 0){
        	var initial = "";
        	for(var i in res.LoopDataset){
            	if(res.LoopDataset[i].inRemitDetails !== undefined && res.LoopDataset[i].inRemitDetails !== ""){
                	for(var j in res.LoopDataset[i].inRemitDetails){
                      var date = isEmpty(res.LoopDataset[i].inRemitDetails[j].txn_date) ? res.LoopDataset[i].inRemitDetails[j].txn_date : format_date(res.LoopDataset[i].inRemitDetails[j].txn_date,"DD/MM/YYYY");
                    	initial = res.LoopDataset[i].inRemitDetails[j].payer_name.substring(0, 2).toUpperCase();
                    	tempData.push({"lblBeneficiaryName":{"text":res.LoopDataset[i].inRemitDetails[j].payer_name},
                                      "lblAmount":{"text":res.LoopDataset[i].inRemitDetails[j].cr_amt+" "+res.LoopDataset[i].inRemitDetails[j].cr_amt_ccy},
                                      "lblAccountNumber":{"text":res.LoopDataset[i].inRemitDetails[j].cr_acc_no},
                                      "lblDate":{"text":date},
                                      "lblInitial":{"text":initial},
                                      "flxIcon1":{"backgroundColor":kony.boj.getBackGroundColour(initial)},
                                      "inRemitDetails":res.LoopDataset[i].inRemitDetails[j]});
                    }
                }
            }
        	frmTransferStatus.segTransferStatus.widgetDataMap = {"flxIcon1":"flxIcon1",
                                                                 "lblInitial":"lblInitial",
                                                                 "lblBeneficiaryName":"lblBeneficiaryName",
                                                                 "lblAmount":"lblAmount",
                                                                 "lblAccountNumber":"lblAccountNumber",
                                                                 "lblDate":"lblDate"};
          kony.print("remit SegData ::"+JSON.stringify(tempData));
          kony.retailBanking.globalData.remitDetails = tempData;
        }
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        if(tempData.length > 0){
          frmTransferStatus.lblNoRecors.setVisibility(false);
          frmTransferStatus.segTransferStatus.removeAll();
          frmTransferStatus.segTransferStatus.setData(tempData);
          frmTransferStatus.segTransferStatus.setVisibility(true);
        }else{
          frmTransferStatus.segTransferStatus.removeAll();
          frmTransferStatus.segTransferStatus.setVisibility(false);
          frmTransferStatus.lblNoRecors.setVisibility(true);
        }
        frmTransferStatus.show();
    }catch(e){
    	kony.print("Exception_serv_INOUTREMIT_TRANSACTIONSTATUS ::"+e);
    }
}

function success_serv_OUTREMIT_TRANSACTIONSTATUS(res){
	try{
    	var tempData = [];
    	kony.print("remit Success ::"+JSON.stringify(res));
    	if(res.LoopDataset !== undefined && res.LoopDataset.length > 0){
        	var initial = "";
        	for(var i in res.LoopDataset){
            	if(res.LoopDataset[i].outRemitDetails !== undefined && res.LoopDataset[i].outRemitDetails !== ""){
                	for(var j in res.LoopDataset[i].outRemitDetails){
                        var date = isEmpty(res.LoopDataset[i].outRemitDetails[j].txn_date) ? res.LoopDataset[i].outRemitDetails[j].txn_date : format_date(res.LoopDataset[i].outRemitDetails[j].txn_date,"DD/MM/YYYY");
                    	initial = res.LoopDataset[i].outRemitDetails[j].ben_name.substring(0, 2).toUpperCase();
                    	tempData.push({"lblBeneficiaryName":{"text":res.LoopDataset[i].outRemitDetails[j].ben_name},
                                      "lblAmount":{"text":res.LoopDataset[i].outRemitDetails[j].debit_amt+" "+res.LoopDataset[i].outRemitDetails[j].debit_ccy},
                                      "lblAccountNumber":{"text":res.LoopDataset[i].outRemitDetails[j].ben_acc_no},
                                      "lblDate":{"text":date},
                                      "lblInitial":{"text":initial},
                                      "flxIcon1":{"backgroundColor":kony.boj.getBackGroundColour(initial)},
                                      "outRemitDetails":res.LoopDataset[i].outRemitDetails[j]});
                    }
                }
            }
        	frmTransferStatus.segTransferStatus.widgetDataMap = {"flxIcon1":"flxIcon1",
                                                                 "lblInitial":"lblInitial",
                                                                 "lblBeneficiaryName":"lblBeneficiaryName",
                                                                 "lblAmount":"lblAmount",
                                                                 "lblAccountNumber":"lblAccountNumber",
                                                                 "lblDate":"lblDate"};
          kony.print("remit SegData ::"+JSON.stringify(tempData));
          kony.retailBanking.globalData.remitDetails = tempData;
        }
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        if(tempData.length > 0){
          frmTransferStatus.lblNoRecors.setVisibility(false);
          frmTransferStatus.segTransferStatus.removeAll();
          frmTransferStatus.segTransferStatus.setData(tempData);
          frmTransferStatus.segTransferStatus.setVisibility(true);
        }else{
          frmTransferStatus.segTransferStatus.removeAll();
          frmTransferStatus.segTransferStatus.setVisibility(false);
          frmTransferStatus.lblNoRecors.setVisibility(true);
        }
        frmTransferStatus.show();
    }catch(e){
    	kony.print("Exception_success_serv_OUTREMIT_TRANSACTIONSTATUS ::"+e);
    }
}

function search_REMITDETAILS(searchValue){
	try{
    	var ben_name = "";
    	var tempData = [];
    	if(searchValue !== "" && searchValue !== null){
          for(var i in kony.retailBanking.globalData.remitDetails){
              ben_name = kony.retailBanking.globalData.remitDetails[i].lblBeneficiaryName.text;
          	  ben_name = ben_name.toUpperCase();
              if(ben_name.match(searchValue.toUpperCase()) !== null && ben_name.match(searchValue.toUpperCase()) !== "null" && ben_name.match(searchValue.toUpperCase()) !== ""){
                  tempData.push(kony.retailBanking.globalData.remitDetails[i]);
              }
          }
        }else{
        	tempData = null;
        	tempData = kony.retailBanking.globalData.remitDetails;
        }
    	frmTransferStatus.segTransferStatus.widgetDataMap = {"flxIcon1":"flxIcon1",
                                                                 "lblInitial":"lblInitial",
                                                                 "lblBeneficiaryName":"lblBeneficiaryName",
                                                                 "lblAmount":"lblAmount",
                                                                 "lblAccountNumber":"lblAccountNumber",
                                                                 "lblDate":"lblDate"};
    	if(tempData.length > 0){
          frmTransferStatus.lblNoRecors.setVisibility(false);
          frmTransferStatus.segTransferStatus.removeAll();
          frmTransferStatus.segTransferStatus.setData(tempData);
          frmTransferStatus.segTransferStatus.setVisibility(true);
        }else{
        	frmTransferStatus.segTransferStatus.removeAll();
        	frmTransferStatus.segTransferStatus.setVisibility(false);
        	frmTransferStatus.lblNoRecors.setVisibility(true);
        }
    	//frmTransferStatus.show();
    }catch(e){
    	kony.print("Exception_search_REMITDETAILS ::"+e);
    }
}

function set_TRANSACTIONDETAILS(flag, data){
	try{
    	var date = null;
    	frmTransactionDetailKA.flxBankAddress.setVisibility(false);
    	frmTransactionDetailKA.flxBeneficiaryAddress.setVisibility(false);
    	frmTransactionDetailKA.lblNotesTitle.setVisibility(false);
    	frmTransactionDetailKA.lblNotes.setVisibility(false);
    	frmTransactionDetailKA.lblTransactionEffectedDateTitle.setVisibility(false);
    	frmTransactionDetailKA.lblTransactionEffectedDate.setVisibility(false);
    	frmTransactionDetailKA.lblCreditAccountTitle.setVisibility(false);
    	frmTransactionDetailKA.lblCreditAccount.setVisibility(false);
    	frmTransactionDetailKA.lblPurposeTitle.setVisibility(false);
    	frmTransactionDetailKA.lblPurpose.setVisibility(false);
    	frmTransactionDetailKA.lblBranchTitle.setVisibility(false);
        frmTransactionDetailKA.lblBranch.setVisibility(false);
    	frmTransactionDetailKA.lblRemarksTitle.setVisibility(false);
        frmTransactionDetailKA.lblRemarks.setVisibility(false);
    	frmTransactionDetailKA.lblReferenceIdTitle.text = geti18Value("i18n.common.ReferenceNumber");
    	frmTransactionDetailKA.lblReferenceId.text = data.txn_ref_no;
    	frmTransactionDetailKA.lblAmountTitle.text = geti18Value("i18n.accounts.amount");
    	frmTransactionDetailKA.lblFeesTitle.text = geti18Value("i18n.opening_account.fees");
    	frmTransactionDetailKA.lblTransactionDateTitle.text = geti18Value("i18n.common.transactiondate");
    	date = data.txn_date.split("-");
        frmTransactionDetailKA.lblTransactionDate.text = date[2]+"/"+date[1]+"/"+date[0];
    	if(data.remarks !== "" && data.remarks !== null){
          frmTransactionDetailKA.lblRemarksTitle.setVisibility(true);
          frmTransactionDetailKA.lblRemarks.setVisibility(true);
          frmTransactionDetailKA.lblRemarksTitle.text = geti18Value("i18n.common.remarks");
          frmTransactionDetailKA.lblRemarks.text = data.remarks;
        }
    	if(flag === "inRemitDetails"){
        	frmTransactionDetailKA.lblPurposeTitle.setVisibility(true);
    		frmTransactionDetailKA.lblPurpose.setVisibility(true);
        	frmTransactionDetailKA.lblPayeeNameTitle.text = geti18Value("i18n.transfer.payeeNameC");
        	frmTransactionDetailKA.lblPayeeName.text = data.payer_name;
        	frmTransactionDetailKA.lblPayeeAccountTitle.text = geti18Value("i18n.common.payeeaccount");
        	frmTransactionDetailKA.lblPayeeAccount.text = data.payee_account;
        	frmTransactionDetailKA.lblAmount.text = data.cr_amt+" "+data.cr_amt_ccy;
        	frmTransactionDetailKA.lblFees.text = data.fee_charges_amt+" "+data.fee_charges_ccy;
        	frmTransactionDetailKA.lblTransactionEffectedDateTitle.text = geti18Value("i18n.common.effecteddate");
        	date = data.value_date.split("-");
        	frmTransactionDetailKA.lblTransactionEffectedDate.text = date[2]+"/"+date[1]+"/"+date[0];//data.value_date;
        	frmTransactionDetailKA.lblPurposeTitle.text = geti18Value("i18n.confirmdetails.purposeoftransfer");
        	frmTransactionDetailKA.lblPurpose.text = data.remit_purpose;
        	frmTransactionDetailKA.lblBankTitle.text = geti18Value("i18n.common.bank");
        	frmTransactionDetailKA.lblBank.text = data.debit_bank_name;
        	if(data.debit_branch_name !== "" && data.debit_branch_name !== null){
              frmTransactionDetailKA.lblBranchTitle.setVisibility(true);
              frmTransactionDetailKA.lblBranch.setVisibility(true);
              frmTransactionDetailKA.lblBranchTitle.text = geti18Value("i18n.locateus.branch");
              frmTransactionDetailKA.lblBranch.text = data.debit_branch_name;
            }
        }else if(flag === "outRemitDetails"){
        	frmTransactionDetailKA.flxBankAddress.setVisibility(true);
            frmTransactionDetailKA.flxBeneficiaryAddress.setVisibility(true);
            frmTransactionDetailKA.lblTransactionEffectedDateTitle.setVisibility(true);
            frmTransactionDetailKA.lblTransactionEffectedDate.setVisibility(true);
            frmTransactionDetailKA.lblCreditAccountTitle.setVisibility(true);
            frmTransactionDetailKA.lblCreditAccount.setVisibility(true);
            frmTransactionDetailKA.lblPurposeTitle.setVisibility(true);
            frmTransactionDetailKA.lblPurpose.setVisibility(true);
        	frmTransactionDetailKA.lblPayeeNameTitle.text = geti18Value("i18n.transfer.BeneficiaryName");
        	frmTransactionDetailKA.lblPayeeName.text = data.ben_name;
        	frmTransactionDetailKA.lblPayeeAccountTitle.text = geti18Value("i18n.common.beneficiaryaccount");
        	frmTransactionDetailKA.lblPayeeAccount.text = data.ben_acc_no;
        	frmTransactionDetailKA.lblCreditAccountTitle.text = geti18Value("i18n.common.debitaccount");
        	frmTransactionDetailKA.lblCreditAccount.text = data.debit_acc;
        	frmTransactionDetailKA.lblAmount.text = data.debit_amt+" "+data.debit_amt_curreny;
        	frmTransactionDetailKA.lblFees.text = data.charges_amount+" "+data.charges_ccy;
        	frmTransactionDetailKA.lblBankTitle.text = geti18Value("i18n.common.beneficiarybank");
        	frmTransactionDetailKA.lblBank.text = data.ben_bank_name;
        	if(data.ben_branch !== "" && data.ben_branch !== null){
              frmTransactionDetailKA.lblBranchTitle.setVisibility(true);
              frmTransactionDetailKA.lblBranch.setVisibility(true);
              frmTransactionDetailKA.lblBranchTitle.text = geti18Value("i18n.common.beneficiarybranch");
              frmTransactionDetailKA.lblBranch.text = data.ben_branch;
            }
        	frmTransactionDetailKA.lblBankAddress1.text = data.ben_bank_addr1;
        	frmTransactionDetailKA.lblBankAddress2.text = data.ben_bank_addr2;
        	frmTransactionDetailKA.lblBankAddress3.text = data.ben_bank_addr3;
        	frmTransactionDetailKA.lblBeneficaryAddress1.text = data.ben_addr1;
        	frmTransactionDetailKA.lblBeneficaryAddress2.text = data.ben_addr2;
        	frmTransactionDetailKA.lblBeneficaryAddress3.text = data.ben_addr3;
        	if(data.remittance_notes !== "" && data.remittance_notes !== null){
            	frmTransactionDetailKA.lblNotesTitle.setVisibility(true);
            	frmTransactionDetailKA.lblNotes.setVisibility(true);
        		frmTransactionDetailKA.lblNotes.text = data.remittance_notes;
            }
        }
    frmTransactionDetailKA.show();
    }catch(e){
    	kony.print("Exception_set_TRANSACTIONDETAILS ::"+e);
    }
}

function serv_FETCH_TRANSACTION_LIMIT(){
  try{
    if(kony.sdk.isNetworkAvailable()){
      //kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprGetTransferLimit");
      var serviceParams = {"custId": custid,
                           "sourceAccount":kony.store.getItem("frmAccount").accountID,
                           "benAccount":"",
                           "transAmount":""};
      kony.print("input params ::"+JSON.stringify(serviceParams));
      appMFConfiguration.invokeOperation("prGetTransferLimit", {},serviceParams,
                                         function(response){
        									kony.print("Transaction Limit ::"+JSON.stringify(response));
        									var valuee = "";
        									if(response.errorCode === "00000"){
                                              transaction_LIMIT.bojLimit = response.bojTransferLimit;
                                              transaction_LIMIT.nationalLimit = response.nationalTransferLimit;
                                              transaction_LIMIT.internationalLimit = response.internationalTransferLimit;
                                              transaction_LIMIT.ownLimit = response.ownTransferLimit;
                                            	if(gblSelectedBene.benType === "IFB"){
                                                  valuee = formatAmountwithcomma(response.bojTransferLimit, (kony.store.getItem("frmAccount").currencyCode === "JOD")?3:2);
                                            	  transaction_LIMIT.limit = response.bojTransferLimit;
                                                }else if(gblSelectedBene.benType === "DTB"){
                                                  valuee = formatAmountwithcomma(response.nationalTransferLimit, (kony.store.getItem("frmAccount").currencyCode === "JOD")?3:2);
                                                  transaction_LIMIT.limit = response.nationalTransferLimit;
                                                }else if(gblSelectedBene.benType === "ITB"){
                                                  valuee = formatAmountwithcomma(response.internationalTransferLimit, (kony.store.getItem("frmAccount").currencyCode === "JOD")?3:2);
                                                  transaction_LIMIT.limit = response.internationalTransferLimit;
                                                }else{
                                                	valuee = formatAmountwithcomma(response.ownTransferLimit, (kony.store.getItem("frmAccount").currencyCode === "JOD")?3:2);
                                                  	transaction_LIMIT.limit = response.ownTransferLimit;
                                                }
                                            	if(kony.store.getItem("frmAccount").currencyCode !== CURRENCY){
                                                  serv_GET_EXCHANGE_RATES_FOR_TRANSACTION_LIMIT(valuee.replace(/,/g,""));
                                                }else{
                                              /*transaction_LIMIT.own = response.bojTransferLimit;
                                              transaction_LIMIT.boj = response.bojTransferLimit;
                                              transaction_LIMIT.national = response.nationalTransferLimit;
                                              transaction_LIMIT.international = response.internationalTransferLimit;*/
                                              if(kony.store.getItem("langPrefObj") === "ar")
                                                if(new RegExp(/\-/g).test(valuee)){
                                                  valuee = valuee.replace("-", "")+ "-";
                                                }
                                              frmNewTransferKA.lblAvailableLimit.text = geti18Value("i18n.common.AvailableLimit")+" "+valuee+" "+CURRENCY;
                                              frmNewTransferKA.lblAvailableLimit.setVisibility(true);
                                              }	
                                            }
      									},function(error){});
    }else{
//     	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    	customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
  }catch(e){
  	kony.print("Exception_serv_FETCH_TRANSACTION_LIMIT ::"+e);
  }
}

function serv_GET_EXCHANGE_RATES_FOR_TRANSACTION_LIMIT(amount){
	try{
    	var toAccCCode = "";
    	var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options ={    "access": "online",
                      	  "objectName": "RBObjects"
                      };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Transactions",serviceName,options);
        var dataObject = new kony.sdk.dto.DataObject("Transactions");
    	
        toAccCCode = getCurrencyCode_FROM_CURRENCY_ISO(kony.store.getItem("frmAccount").currencyCode);
        toAccCCode = (toAccCCode.length == 1)?append_VALUE("00"+toAccCCode,"0",13):(toAccCCode.length == 2)?append_VALUE("0"+toAccCCode,"0",13):append_VALUE(toAccCCode,"0",13);

        dataObject.addField("accountNumber","0010000000000000");
        dataObject.addField("ExternalAccountNumber",toAccCCode);
        dataObject.addField("amount",amount);
    	dataObject.addField("p_trnsf_cur","1");
        dataObject.addField("TransferFlag","T");
        dataObject.addField("p_billId","");
        dataObject.addField("p_billNo","");
        var serviceOptions = {"dataObject":dataObject,"headers":headers};
    	kony.print("input params ::"+JSON.stringify(dataObject));
        modelObj.customVerb("getFeeData",serviceOptions, function(res){kony.print("res ::"+JSON.stringify(res));
                                                                       if(res.ref_code === "00000"){
                                                                           var valuee = formatAmountwithcomma(res.transferResponse[0].cr_amt, (kony.store.getItem("frmAccount").currencyCode === "JOD")?3:2);
                                                                       	   transaction_LIMIT.limit = res.transferResponse[0].cr_amt;
                                                                           if(kony.store.getItem("langPrefObj") === "ar")
                                                                            if(new RegExp(/\-/g).test(valuee)){
                                                                              valuee = valuee.replace("-", "")+ "-";
                                                                            }
                                                                           frmNewTransferKA.lblAvailableLimit.text = geti18Value("i18n.common.AvailableLimit")+" "+valuee+" "+kony.store.getItem("frmAccount").currencyCode;
                                                                       	   frmNewTransferKA.lblAvailableLimit.setVisibility(true);
                                                                       }else{
                                                                         transaction_LIMIT.limit = "";
                                                                         frmNewTransferKA.lblAvailableLimit.setVisibility(false);
                                                                       }
                                                                       
                                                                      }, function(err){kony.print("err ::"+JSON.stirngify(err));});
    }catch(e){
    	kony.print("Exception_serv_GET_EXCHANGE_RATES_FOR_TRANSACTION_LIMIT ::"+e);
    }
}

function validate_TRANSACTION_LIMIT(amount){
	try{
    	if(gblTModule !== "own" && amount !== "." && parseFloat(amount) > 0)
    		return (parseFloat(amount) <= parseFloat(transaction_LIMIT.limit));
    	else
        	return true;
    }catch(e){
    	kony.print("Exception_validate_TRANSACTION_LIMIT ::"+e);
    }
}

function show_TRANSFER_AMOUNT_HELP(currentForm){
  	try{
    	/*var content = geti18Value("i18n.transferlimit.helpContent");
    	content = content.replace("OWNLIMIT",transaction_LIMIT.ownLimit);
        content = content.replace("BOJLIMIT",transaction_LIMIT.bojLimit);
        content = content.replace("NATIONALLIMIT",transaction_LIMIT.nationalLimit);
        content = content.replace("INTERNATIONALLIMIT",transaction_LIMIT.internationalLimit);
    	navigation_STORE_PREVIOUS_FORM(currentForm);
    	frmTermsAndCondtions.lblTncTitle.text = geti18Value("i18n.common.help");
    	frmTermsAndCondtions.richtxtTitle.text = geti18Value("i18n.transferlimit.helpTitle");
    	frmTermsAndCondtions.richtxtTitle.setVisibility(true);
    	frmTermsAndCondtions.lblTnc.text = content;//geti18Value("i18n.transferlimit.helpContent");
    	frmTermsAndCondtions.show();*/

    //frmBrowser.lblBrowserTitle.text = "Transfer Limit";
    //	frmBrowser.flxBrowserContainer.setVisibility(false);
    frmBrowser.flxTransferAmountHelp.setVisibility(true);
    var diff = 0;
    frmBrowser.lblOwnTransferHint.text = geti18Value("ii8n.available.dailyLimit")+" "+geti18Value("ii18n.available.unlimited")+"\n"+geti18Value("ii18n.available.utilize")+" "+geti18Value("ii18n.available.unlimited")+"\n"+geti18Value("i18n.common.AvailableLimit")+" "+geti18Value("ii18n.available.unlimited");
    //     	diff = 10000 - parseInt(transaction_LIMIT.ownLimit);
    var ownLimit = isEmpty(transaction_LIMIT.ownLimit) ? 0 : parseInt(transaction_LIMIT.ownLimit);

    diff = 10000 - ownLimit;

    frmBrowser.lblOwnAccountTransferDiffCurrHint.text = geti18Value("ii8n.available.dailyLimit")+" 10000 "+geti18Value("i18n.JODCurrency")+"\n"+geti18Value("ii18n.available.utilize")+diff+" "+geti18Value("i18n.JODCurrency")+"\n"+geti18Value("i18n.common.AvailableLimit")+ownLimit+" "+geti18Value("i18n.JODCurrency");
    var bojLimit = isEmpty(transaction_LIMIT.bojLimit) ? 0 : parseInt(transaction_LIMIT.bojLimit);


    diff = 10000 - bojLimit;
    //       diff = 10000 - parseInt(transaction_LIMIT.bojLimit);
    frmBrowser.lblBOJAccountTransferHint.text = geti18Value("ii8n.available.dailyLimit")+" 10000 "+geti18Value("i18n.JODCurrency")+"\n"+geti18Value("ii18n.available.utilize")+diff+" "+geti18Value("i18n.JODCurrency")+"\n"+geti18Value("i18n.common.AvailableLimit")+bojLimit+" "+geti18Value("i18n.JODCurrency");
    //     	diff = 10000 - parseInt(transaction_LIMIT.nationalLimit);
    var nationalLimit = isEmpty(transaction_LIMIT.nationalLimit) ? 0 : parseInt(transaction_LIMIT.nationalLimit);

    diff = 10000 - nationalLimit;

    frmBrowser.lblNationalAccountTransferHint.text = geti18Value("ii8n.available.dailyLimit")+" 10000 "+geti18Value("i18n.JODCurrency")+"\n"+geti18Value("ii18n.available.utilize")+diff+" "+geti18Value("i18n.JODCurrency")+"\n"+geti18Value("i18n.common.AvailableLimit")+nationalLimit+" JOD";
   
        var internationalLimit = isEmpty(transaction_LIMIT.internationalLimit) ? 0 : parseInt(transaction_LIMIT.internationalLimit);

    diff = 10000 - internationalLimit;
    //       diff = 10000 - parseInt(transaction_LIMIT.internationalLimit);
    frmBrowser.lblInternationalAccountTransferHint.text = geti18Value("ii8n.available.dailyLimit")+" 10000 JOD\n"+geti18Value("ii18n.available.utilize")+diff+" JOD\n"+geti18Value("i18n.common.AvailableLimit")+internationalLimit+" "+geti18Value("i18n.JODCurrency");
    frmBrowser.show();
  }catch(e){
    kony.print("Exception_show_TRANSFER_AMOUNT_HELP ::"+e);
  }
}