//Type your code here

function navigatetofrmRequestSattementAccounts(){
  assignCallbacksforfrmOrderandStatements();
   if(kony.application.getCurrentForm().id ==="frmManageCardsKA"){
     frmRequestStatementAccounts.lblNumber.text = mask_CreditCardNumber(CARDLIST_DETAILS.cardNumber);
     frmRequestStatementAccounts.lblNumber1.text  = CARDLIST_DETAILS.cardNumber.substring(0,6)+"******"+CARDLIST_DETAILS.cardNumber.substring(CARDLIST_DETAILS.cardNumber.length-4,CARDLIST_DETAILS.cardNumber.length);
   frmRequestStatementAccounts.lblAccountNumber.text = geti18Value("i18n.cards.CreditCardNmrr");
     frmRequestStatementAccountsConfirm.lblAccountStaticText = geti18Value("i18n.cards.CreditCardNmrr");
   }else{
  		frmRequestStatementAccounts.lblNumber.text = geti18Value("i18n.common.accountNumber");
      frmRequestStatementAccounts.lblAccountNumber.text = geti18Value("i18n.common.accountNumber");
      frmRequestStatementAccountsConfirm.lblAccountStaticText =  geti18Value("i18n.common.accountNumber");
		
   }
   frmRequestStatementAccounts.lblYearFromValue.text 	= geti18Value("i18n.filtertransaction.year");
   frmRequestStatementAccounts.lblYearToValue.text 		= geti18Value("i18n.filtertransaction.year");
   frmRequestStatementAccounts.lblMonthFromValue.text 	= geti18Value("i18n.common.month");
   frmRequestStatementAccounts.lblMonthToValue.text 	= geti18Value("i18n.common.month");

  
//      frmRequestStatementAccounts.lblAccountNumber.setVisibility(false);
  frmRequestStatementAccounts.lblLine4.skin = "sknFlxGreyLine";
  
      frmRequestStatementAccounts.lblYearFrom.setVisibility(false);
    frmRequestStatementAccounts.lblLineFY.skin = "sknFlxGreyLine";
   
    frmRequestStatementAccounts.lblYearTo.setVisibility(false);
    frmRequestStatementAccounts.lblLineTY.skin = "sknFlxGreyLine";
   
    frmRequestStatementAccounts.lblMonthFrom.setVisibility(false);
    frmRequestStatementAccounts.lblLineFM.skin = "sknFlxGreyLine";
   
    frmRequestStatementAccounts.lblMonthTo.setVisibility(false);
    frmRequestStatementAccounts.lblLineTM.skin = "sknFlxGreyLine";
    
  frmRequestStatementAccounts.lblBranchValue.text = geti18Value("i18n.Map.Branchname");
  frmRequestStatementAccounts.lblCurrHead.setVisibility(false);
  frmRequestStatementAccounts.lbllinee1.skin = "sknFlxGreyLine";
  
  frmRequestStatementAccounts.CalTo.clear();
  var date = new Date();
  frmRequestStatementAccounts.CalTo.validEndDate = [date.getDate(),date.getMonth()+1,date.getFullYear()];
  frmRequestStatementAccounts.CalTo.date = [date.getDate(),date.getMonth()+1,date.getFullYear()];

  frmRequestStatementAccounts.lblToDate.text =  geti18Value("i18n.common.To");
  frmRequestStatementAccounts.lblTo.setVisibility(false);
  frmRequestStatementAccounts.lblLine3.skin = "sknFlxGreyLine";
  
  frmRequestStatementAccounts.CalFrom.clear();
  frmRequestStatementAccounts.CalFrom.validEndDate = [date.getDate(),date.getMonth()+1,date.getFullYear()];
  frmRequestStatementAccounts.CalFrom.date = [date.getDate(),date.getMonth()+1,date.getFullYear()];
  frmRequestStatementAccounts.lblFromDate.text = geti18Value("i18n.common.fromc");
  frmRequestStatementAccounts.lblFrom.setVisibility(false);
  frmRequestStatementAccounts.lblLine2.skin = "sknFlxGreyLine";
  frmRequestStatementAccounts.txtAddressMail.text = "";
   
  
  frmRequestStatementAccounts.btnBranch.text = "t";
  frmRequestStatementAccounts.btnMail.text = "s";
  frmRequestStatementAccounts.flxBranch.setVisibility(true);
  frmRequestStatementAccounts.flxAddressMail.setVisibility(false);
  frmRequestStatementAccounts.lblNext.skin = "sknLblNextDisabled";
//   frmRequestStatementAccounts.show();
  if(gblRequestStatementCrediCards === true){
  	frmRequestStatementAccounts.show();
  }else{
  	set_ACCOUNTS_REQUESTSTATEMENT();
  }
}



function nextSkinCheckForRequestStaement(){
  var AccCardNum = false;
  if(gblRequestStatementCrediCards){
    if(frmRequestStatementAccounts.lblNumber.text != geti18Value("i18n.cards.CreditCardNmrr")){
      AccCardNum = true;
    }
  }else{
    if(frmRequestStatementAccounts.lblNumber.text != geti18Value("i18n.common.accountNumber")){
      AccCardNum = true;
    }
  }
  
  if((AccCardNum && frmRequestStatementAccounts.lblYearFromValue.text != geti18Value("i18n.filtertransaction.year") && frmRequestStatementAccounts.lblYearToValue.text 	!= geti18Value("i18n.filtertransaction.year") && frmRequestStatementAccounts.lblMonthFromValue.text != geti18Value("i18n.common.month") && frmRequestStatementAccounts.lblMonthToValue.text != geti18Value("i18n.common.month")) && ((!isEmpty(frmRequestStatementAccounts.txtAddressMail.text))  || (frmRequestStatementAccounts.lblBranchValue.text != geti18Value("i18n.Map.Branchname")) ) ){
    frmRequestStatementAccounts.lblNext.skin = "sknLblNextEnabled";
  }else{
   
    frmRequestStatementAccounts.lblNext.skin = "sknLblNextDisabled";
  }
}





function nextToConfirmofRequestStaement(){
  
  if(frmRequestStatementAccounts.lblNext.skin === "sknLblNextEnabled"){
  frmRequestStatementAccountsConfirm.lblAccountNumber.text = frmRequestStatementAccounts.lblNumber.text;
  frmRequestStatementAccountsConfirm.lblFrom.text = frmRequestStatementAccounts.lblMonthFromValue.text + "/" + frmRequestStatementAccounts.lblYearFromValue.text;
    frmRequestStatementAccountsConfirm.lblTo.text = frmRequestStatementAccounts.lblMonthToValue.text+ "/" + frmRequestStatementAccounts.lblYearToValue.text;
    frmRequestStatementAccounts.lblFromDateHidee.text = frmRequestStatementAccounts.lblMonthFromValue.text + "/01/"+ frmRequestStatementAccounts.lblYearFromValue.text;
    frmRequestStatementAccounts.lblToDateHidee.text = frmRequestStatementAccounts.lblMonthToValue.text+ "/30/" + frmRequestStatementAccounts.lblYearToValue.text;
    if(frmRequestStatementAccounts.btnBranch.text === "t"){
      frmRequestStatementAccountsConfirm.lblModeSelected.text = geti18Value("i18n.locateus.branch");
        frmRequestStatementAccountsConfirm.lblMode.text = geti18Value("i18n.locateus.branch");
      frmRequestStatementAccountsConfirm.lblBranch.text = frmRequestStatementAccounts.lblBranchValue.text;   
    }else{
      frmRequestStatementAccountsConfirm.lblMode.text = geti18Value("i18n.accounts.mailAddress");
      frmRequestStatementAccountsConfirm.lblBranch.text = frmRequestStatementAccounts.txtAddressMail.text;
      frmRequestStatementAccountsConfirm.lblModeSelected.text = geti18Value("i18n.accounts.mailAddress");

    }
    if(inYearsDiffDate(frmRequestStatementAccounts.lblFromDateHidee.text,frmRequestStatementAccounts.lblToDateHidee.text)){
      if(kony.store.getItem("langPrefObj") == "ar"){
      	frmRequestStatementAccountsConfirm.lblHint.text =  geti18Value("i18n.accounts.requeststatementMSG")+" (0.5 JOD = "+geti18Value("i18n.requestStmntPage")+" 1)";
      }else{
        frmRequestStatementAccountsConfirm.lblHint.text =  geti18Value("i18n.accounts.requeststatementMSG")+ " (1 "+geti18Value("i18n.requestStmntPage")+ " = 0.5 JOD)";
    }
    }else{
      if(kony.store.getItem("langPrefObj") == "ar"){
      	frmRequestStatementAccountsConfirm.lblHint.text =  geti18Value("i18n.accounts.requeststatementMSG")+" (0.25 JOD = "+geti18Value("i18n.requestStmntPage")+" 1)";
      }else{
      	frmRequestStatementAccountsConfirm.lblHint.text =  geti18Value("i18n.accounts.requeststatementMSG")+ " (1 "+geti18Value("i18n.requestStmntPage")+ " = 0.25 JOD)";
    }
    }
    frmRequestStatementAccountsConfirm.show();
  }
}




function setDatatoRequestStatementBranchList(selectedItem){
   frmRequestStatementAccounts.lblBranchValue.text = selectedItem.BRANCH_NAME;
  frmRequestStatementAccounts.lblBranchCode.text = selectedItem.BRANCH_NAME;//selectedItem.BRANCH_CODE;
  frmRequestStatementAccounts.lbllinee1.skin = "sknFlxGreenLine";
  frmRequestStatementAccounts.lblCurrHead.setVisibility(true);
  nextSkinCheckForRequestStaement();
  frmRequestStatementAccounts.show();
}

function setDatatoRequestStatementCreditCardNumber(selectedItem){
   frmRequestStatementAccounts.lblNumber.text = selectedItem.hiddenCardNum;
  frmRequestStatementAccounts.lblNumber1.text  = selectedItem.card_num.substring(0,6)+"******"+selectedItem.card_num.substring(selectedItem.card_num.length-4,selectedItem.card_num.length);
    frmRequestStatementAccounts.lblAccountNumber.setVisibility(true);
    frmRequestStatementAccounts.lblLine4.skin = "sknFlxGreenLine";
   nextSkinCheckForRequestStaement();
  frmRequestStatementAccounts.show();
}






function callConfirmRequestStatementOrder(){
  kony.print("callConfirmRequestStatementOrder::");
  if(kony.sdk.isNetworkAvailable()){
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };


    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Accounts"); 
    dataObject.addField("custId",custid);
    var mode = "B";
    var adrss = "";
    if(frmRequestStatementAccounts.btnBranch.text === "t"){
      mode = "B";
      adrss = frmRequestStatementAccounts.lblBranchCode.text;
    }else{
      mode = "M";
      adrss = frmRequestStatementAccountsConfirm.lblBranch.text;
    }
    var acc_br = "";
      if(!gblRequestStatementCrediCards){
     var accountsData = kony.retailBanking.globalData.accountsDashboardData.accountsData;
  if(!isEmpty(accountsData)){
    for(var i in accountsData){
      if(accountsData[i]["accountID"] === frmRequestStatementAccounts.lblNumber1.text){
         acc_br = accountsData[i]["branchNumber"]; 
        break;
      }
  }
  }
      }else{
       acc_br =  "";
      }
   dataObject.addField("p_datefrom",frmRequestStatementAccounts.lblFromService.text);
    dataObject.addField("p_dateto",frmRequestStatementAccounts.lblToService.text);
    dataObject.addField("acctno",frmRequestStatementAccounts.lblNumber1.text);

    dataObject.addField("p_acc_br",acc_br);
    dataObject.addField("p_delv_mod",adrss);
    var serviceOptions = {"dataObject":dataObject,"headers":headers};

    kony.print("callConfirmRequestStatementOrderInput params-->"+ JSON.stringify(serviceOptions));
    modelObj.customVerb("cvAccStmtOrder",serviceOptions, callConfirmRequestStatementOrderSuccessCallback, callConfirmRequestStatementOrderErrorCallback);
  }else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}

function callConfirmRequestStatementOrderSuccessCallback(response){
  kony.application.dismissLoadingScreen();
  gblTModule = "";
  
  if(!isEmpty(response) && response.opstatus === 0 && response.ErrorCode === "00000")
  {
   
    var reference = "";
    if(!isEmpty(response.referenceId))
      reference = geti18nkey("i18n.common.ReferenceNumber")+" " + response.referenceId;

    if(gblRequestStatementCrediCards){
          kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"),reference,geti18nkey("i18n.cards.gotoCards"), "CardsLanding", "", "","","",geti18Value("i18n.accounts.requestStatementDeliverMSG"));

    }else{
       kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"),reference,geti18Value("i18n.Bene.GotoAccDash"),geti18nkey("i18n.common.AccountDashboard"), "", "","","",geti18Value("i18n.accounts.requestStatementDeliverMSG"));
   
    }
 gblrequestStatementBranch=false;
    gblRequestStatementCrediCards = false;
  }else{
    
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
    exceptionLogCall("callConfirmOrderCheckBookSuccessCallback","Service ERROR","Service","opstatus other than 0");
    kony.application.dismissLoadingScreen();
  }
}


function callConfirmRequestStatementOrderErrorCallback(response){
  //gblrequestStatementBranch=false;
  gblTModule = "";
  kony.print("callConfirmRequestStatementOrderErrorCallback -----"+JSON.stringify(response));
  customAlertPopup(geti18Value("i18n.NUO.Error"),  geti18Value("i18n.common.defaultErrorMsg"),popupCommonAlertDimiss, "");
  kony.application.dismissLoadingScreen();

}
 
function assignCallbacksforfrmOrderandStatements(){
 if(kony.application.getCurrentForm().id ==="frmAccountInfoKA"){
   frmRequestStatementAccounts.flxBack.onClick = navigateBacktofrmfrmAccountInfoKA;
    frmRequestStatementAccounts.onDeviceBack = navigateBacktofrmfrmAccountInfoKA;
     frmOrderCheckBook.flxBack.onClick = navigateBacktofrmfrmAccountInfoKA;
    frmOrderCheckBook.onDeviceBack = navigateBacktofrmfrmAccountInfoKA;
     
     function navigateBacktofrmfrmAccountInfoKA(){
     
       customAlertPopup(geti18Value("i18n.cards.Confirmation"), 
                 geti18Value("i18n.transfers.eraseAllAlert"),
                  onClickYESBack, onClickNoBack, 
                 geti18Value("i18n.common.YES"),
                 geti18Value("i18n.common.NO"));

function onClickNoBack(){
  
  popupCommonAlertDimiss();
}
       
       function onClickYESBack(){
         gblsubaccBranchList =false;
       gblrequestStatementBranch= false;
         gblRequestStatementCrediCards = false;
         gblTModule = "";
          popupCommonAlertDimiss();
 accountInfo("account");
}
    
   }
 }else if(kony.application.getCurrentForm().id ==="frmManageCardsKA"){
   frmRequestStatementAccounts.flxBack.onClick = navigateBacktofrmfrmAccountInfoKA;
    frmRequestStatementAccounts.onDeviceBack = navigateBacktofrmfrmAccountInfoKA;
        
     function navigateBacktofrmfrmAccountInfoKA(){
     
       customAlertPopup(geti18Value("i18n.cards.Confirmation"), 
                 geti18Value("i18n.transfers.eraseAllAlert"),
                  onClickYESBack, onClickNoBack, 
                 geti18Value("i18n.common.YES"),
                 geti18Value("i18n.common.NO"));

function onClickNoBack(){
  
  popupCommonAlertDimiss();
}
       
       function onClickYESBack(){
         gblsubaccBranchList =false;
       gblrequestStatementBranch= false;
         gblRequestStatementCrediCards = false;
         gblTModule = "";
          popupCommonAlertDimiss();
 frmManageCardsKA.show();
}
    
   }
 }else{
   infoType = "account";
   frmRequestStatementAccounts.flxBack.onClick = navigateBacktofrmfrmAccountDetailKA;
    frmRequestStatementAccounts.onDeviceBack = navigateBacktofrmfrmAccountDetailKA;
     frmOrderCheckBook.flxBack.onClick = navigateBacktofrmfrmAccountDetailKA;
    frmOrderCheckBook.onDeviceBack = navigateBacktofrmfrmAccountDetailKA;
     function navigateBacktofrmfrmAccountDetailKA(){
     
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), 
                 geti18Value("i18n.transfers.eraseAllAlert"),
                  onClickYESBack, onClickNoBack, 
                 geti18Value("i18n.common.YES"),
                 geti18Value("i18n.common.NO"));

function onClickNoBack(){
   popupCommonAlertDimiss();
}
        function onClickYESBack(){
          gblsubaccBranchList =false;
       gblrequestStatementBranch = false;
          gblRequestStatementCrediCards = false;
          gblTModule = "";
           popupCommonAlertDimiss();
 transactionMove();
}
   };
 }

}



function inYearsDiffDate(d1,d2) {
   kony.print("d1::"+d1+"d2::"+d2);
 var date1 = new Date(d1);
var date2 = new Date(d2);
var timeDiff = Math.abs(date2.getTime() - date1.getTime());
var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
   kony.print("diffDays::"+diffDays);
  if(parseInt(diffDays) >= 365){
       return true;
  }else{
    return false;
  }
    }




function onClickAccReqStmntScreen(){
  var AccDetailsSegdata = frmAccountDetailsScreen.segFre.data;
  var dataSelected =  frmAccountDetailsScreen.segFre.selectedRowItems[0];
 
  var text;
 
  kony.print("Selected Account Data ::"+JSON.stringify(dataSelected));
  kony.print("gblTModule ::"+gblTModule);
 var accName =(dataSelected.anum.length>16)?(dataSelected.anum.substring(0,16)+"..."):dataSelected.anum;
  frmRequestStatementAccounts.lblNumber.text = accName+ " " +dataSelected.hiddenAccountNumber;
  
   frmRequestStatementAccounts.lblNumber1.text = dataSelected.accountID;
      frmRequestStatementAccounts.lblAccountNumber.setVisibility(true);
  frmRequestStatementAccounts.show();
}





function onClickAccBackToReqStmntScreen(){
 
  frmRequestStatementAccounts.show();
   gblTModule = "";
}
