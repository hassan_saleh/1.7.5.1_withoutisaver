function iWatchCardsCall(){
  kony.print("Iwatch :: Inside iWatchCardsCall ");
  if (kony.sdk.isNetworkAvailable()) {
    var scopeObj = this;
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);

    var dataObject = new kony.sdk.dto.DataObject("Cards");
    var Language =  "eng";//kony.store.getItem("langPrefObj")==="en"?"eng":"ara";

    dataObject.addField("custId", iWatchcustid);

    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("Iwatch :: ServiceOptions for iWatchCardsCall :: " + JSON.stringify(serviceOptions));
    objectService.customVerb("iWatchgetCards", serviceOptions,iWatchCardsCallSuccessCallback, iWatchCardsCallErrorCallback);
  }else {
    return_watchrequest({"cards": [],"isUserLoggedIn": true, "isNetworkAvailable": false,"isRetry": true});
  }
}

function iWatchCardsCallErrorCallback(err) {
  kony.print("Iwatch ::iWatchCardsCall-->" + "An Error has occured, cant get accounts" + JSON.stringify(err));
  if(!isEmpty(kony.retailBanking.globalData.prCardLandinList)){
    return_watchrequest({"cards": kony.retailBanking.globalData.prCardLandinList ,"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
  }else{
    return_watchrequest({"cards": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
  }
}

function iWatchCardsCallSuccessCallback(data){
  try{
    kony.print("Iwatch :: iWatchCardsCallSuccessCallback :: "+ JSON.stringify(data));

    kony.retailBanking.globalData.prCardLandinList = [];
    kony.retailBanking.globalData.prCreditCardPayList =[];
    if(!isEmpty(data.AllCardList)){
      kony.retailBanking.globalData.prCreditCardPayList = data.AllCardList;
    }
    if(!isEmpty(kony.retailBanking.globalData.prCreditCardPayList) && kony.retailBanking.globalData.prCreditCardPayList.length >0){
      for(var j in kony.retailBanking.globalData.prCreditCardPayList){
        if(kony.retailBanking.globalData.prCreditCardPayList[j].showHideFlag === "T" || kony.retailBanking.globalData.prCreditCardPayList[j].showHideFlag === "Y")
          kony.retailBanking.globalData.prCardLandinList.push(kony.retailBanking.globalData.prCreditCardPayList[j]);
      }
    }
    var tempdata = [];
    if(kony.retailBanking.globalData.prCardLandinList.length > 0){
      tempdata = process_CardListDetails(kony.retailBanking.globalData.prCardLandinList);
    }

    kony.print("Iwatch ::iWatchCardsCallSuccessCallback after process :: "+ JSON.stringify(kony.retailBanking.globalData.prCardLandinList));
    return_watchrequest({"cards": kony.retailBanking.globalData.prCardLandinList || [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
  }catch(err){
    kony.print("Iwatch :: Error  in iWatchCardsCallSuccessCallback ::  "+ err);
    return_watchrequest({"cards":[],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
  }
}