kony = kony || {};
kony.rb = kony.rb || {};

/**
 * BusinessController
 * frmNUOCreditCheckKA
 */

kony.rb.creditCheckBusinessController = function(mfObjSvc) {
    this.mfObjectServiceHandler = mfObjSvc;
};

/**
 * function userCreditCheck
 * check the acceptence of creditcheck
 */

kony.rb.creditCheckBusinessController.prototype.userCreditCheck = function(params,presentationSuccessCallback, presentationErrorCallback) {
    this.mfObjectServiceHandler.customPost("userCreditCheck", "NewUser", params, {}, success, error);

    function success() {
        presentationSuccessCallback();
    }

    function error(err) {
        presentationErrorCallback(err);
    }
};
/**
 * function getNewUserSecurityQuestions
 * get security questions
 */
kony.rb.creditCheckBusinessController.prototype.getNewUserSecurityQuestions = function(presentationSuccessCallback, presentationErrorCallback) {
    this.mfObjectServiceHandler.fetch("NewUserSecurityQuestions", {}, {}, success, error);
    var utlHandler = applicationManager.getUtilityHandler();

    function success(res) {
        var seQArray = res.records;
        for (var i = 0; i < seQArray.length; i++) {
            seQArray[i] = utlHandler.convertJSonToSeQObject(seQArray[i]);

        }
        presentationSuccessCallback(seQArray);
    }

    function error(err) {
        presentationErrorCallback(err);
        alert("inside error of business controller");
    }

};
/**
 * function createSecurityQuestions
 * create security questions
 */

kony.rb.creditCheckBusinessController.prototype.createSecurityQuestions = function(params, presentationSuccessCallback, presentationErrorCallback) {
    this.mfObjectServiceHandler.update("NewUserSecurityQuestions", params, {}, success, error);

    function success(res) {
        presentationSuccessCallback(res);
    }

    function error(err) {
        presentationErrorCallback(err);
    }
};
/**
 * function signatureUpload
 * signature upload
 */

kony.rb.creditCheckBusinessController.prototype.signatureUpload = function(params,presentationSuccessCallback, presentationErrorCallback) {
    this.mfObjectServiceHandler.customPost("signatureUpload", "NewUser", params, {}, success, error);

    function success(res) {
        presentationSuccessCallback(res);
    }

    function error(err) {
        presentationErrorCallback(err);
    }
};