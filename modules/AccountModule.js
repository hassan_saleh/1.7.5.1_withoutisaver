function launchParams(params){
  kony.print("param::"+JSON.stringify(params));
  var langFlag = kony.store.getItem("langFlagSettingObj");
  gblLaunchModeOBJ = {"lauchMode" : false , "accno" : "" , "iban" : ""};
  params = params? params : {};
  
  params.launchparams = params.launchparams? params.launchparams : {};
  params.launchmode = params.launchmode? params.launchmode : 1;

  if(params.launchmode == 3) {
    if(!isEmpty(params.launchparams.accno) || !isEmpty(params.launchparams.iban)){
      gblLaunchModeOBJ.accno =  isEmpty(params.launchparams.accno) ? "" : params.launchparams.accno;
      gblLaunchModeOBJ.iban =  isEmpty(params.launchparams.iban) ? "" : params.launchparams.iban;
      gblPayAppFlow =false;
      gblLaunchModeOBJ.lauchMode = true;
    }else if(!isEmpty(params.launchparams.GoTo))// hassan 28/12/2020
      {
        goToNationalIDRegistration();
        return frmRegisterUser;
      }
  }
 
    if(gblLaunchModeOBJ.lauchMode){ 
      if(isLoggedIn){ 
        
        kony.print("isLoggedIn true Send money::");
        if(!isEmpty(params.launchparams.accno) || !isEmpty(params.launchparams.iban)){
        removeDatafrmTransfersform();
        makeInlineErrorsTransfersInvisible();
        gblTModule="send";
        gblOnceSetBranch =true;
        getAllAccounts();
        }
        //return frmLanguageChange; 
      }else{
       if(langFlag){
          kony.print("isLoggedIn false Send money:: language selected::");
         //   if(!isLoggedIn){
      //   fetchApplicationProperties();
      //   }
        return frmLoginKA;
      }else{
         kony.print("isLoggedIn false Send money:: Language not selected::");
      return frmLanguageChange;
      }  
      }
    }else{
      if(langFlag){
         kony.print("Normal flow login ::");
        return frmLoginKA;
      }else{
         kony.print("Normal flow Language::");
        return frmLanguageChange;
      } 
    }  
  //return frmLoginKA;
}




// adding comment for testing
function openStatementURL(){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController(kony.application.getCurrentForm().id);
  var viewModel = controller.getFormModel();
  var selRecord = viewModel.getViewAttributeByProperty("contactsegment", "selectedItems")[0];
  kony.application.openURL(selRecord.StatementLink);
}

function accountsInfoPreShowskins(){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmAccountDetailKA");
  var controllerContextData = controller.getContextData() || new kony.sdk.mvvm.NavigationObject();
  if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
    var accountData = controllerContextData.getCustomInfo("selectedAccountObj");
    var accountType = accountData["accountType"];
  }
  if (accountType === kony.retailBanking.globalData.globals.Checking){
    frmAccountInfoKA.skin = sknaccountCheckingBkg;
    frmAccountInfoKA.titleBarAccountInfo.skin = sknaccountTypeChecking;
    frmAccountInfoKA.accountDetailsContainer.isVisible = false;
    frmAccountInfoKA.flxaccountdetailsfordeposits.isVisible = false;
    frmAccountInfoKA.flxaccountdetailsSavings.isVisible = true;
    //frmAccountInfoKA.addExternalAccount.isVisible = true;
  } else if (accountType === kony.retailBanking.globalData.globals.Savings){
    frmAccountInfoKA.skin = sknaccountSavingsBkg;
    frmAccountInfoKA.titleBarAccountInfo.skin = sknaccountTypeSavings;
    frmAccountInfoKA.accountDetailsContainer.isVisible = false;
    frmAccountInfoKA.flxaccountdetailsfordeposits.isVisible = false;
    frmAccountInfoKA.flxaccountdetailsSavings.isVisible = true;
    //frmAccountInfoKA.addExternalAccount.isVisible = true;
  } else if (accountType === kony.retailBanking.globalData.globals.CreditCard){
    frmAccountInfoKA.skin = sknaccountCreditBkg;
    frmAccountInfoKA.titleBarAccountInfo.skin = sknaccountTypeCredit;
    frmAccountInfoKA.accountDetailsContainer.isVisible = true;
    frmAccountInfoKA.flxaccountdetailsfordeposits.isVisible = false;
    frmAccountInfoKA.flxaccountdetailsSavings.isVisible = false;
    //frmAccountInfoKA.addExternalAccount.isVisible = false;
  }else if (accountType === kony.retailBanking.globalData.globals.Deposit){
    frmAccountInfoKA.skin=sknaccountDepositBkg;
    frmAccountInfoKA.titleBarAccountInfo.skin = sknaccounttypeDeposit;
    frmAccountInfoKA.accountDetailsContainer.isVisible = false;
    frmAccountInfoKA.flxaccountdetailsfordeposits.isVisible = true;
    frmAccountInfoKA.flxaccountdetailsSavings.isVisible = false;
    //frmAccountInfoKA.addExternalAccount.isVisible = false;
  } 
  else if (accountType == kony.retailBanking.globalData.globals.Mortgage){
    frmAccountInfoKA.skin=sknaccountMortageBkg;
    frmAccountInfoKA.titleBarAccountInfo.skin = sknaccounttypemortage;
    frmAccountInfoKA.accountDetailsContainer.isVisible = false;
    frmAccountInfoKA.flxaccountdetailsfordeposits.isVisible = true;
    frmAccountInfoKA.flxaccountdetailsSavings.isVisible = false;
    //frmAccountInfoKA.addExternalAccount.isVisible = false;
  }

  frmAccountInfoKA.successIcon.opacity = 0;
  frmAccountInfoKA.successImage.opacity = 0;
}

function accountsInfoEditPreShowskins(){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmAccountDetailKA");
  var controllerContextData = controller.getContextData();
  if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
    var accountData = controllerContextData.getCustomInfo("selectedAccountObj");
    var accountType = accountData["accountType"];
  }
  if (accountType === kony.retailBanking.globalData.globals.Checking){
    frmAccountInfoEditKA.skin = sknaccountCheckingBkg;
    frmAccountInfoEditKA.titleBarAccountInfo.skin = sknaccountTypeChecking;
    frmAccountInfoEditKA.accountDetailsContainer.isVisible = false;
    frmAccountInfoEditKA.flxaccountdetailsfordeposits.isVisible = false;
    frmAccountInfoEditKA.flxaccountdetailsSavings.isVisible = true;
  } else if (accountType === kony.retailBanking.globalData.globals.Savings){
    frmAccountInfoEditKA.skin = sknaccountSavingsBkg;
    frmAccountInfoEditKA.titleBarAccountInfo.skin = sknaccountTypeSavings;
    frmAccountInfoEditKA.accountDetailsContainer.isVisible = false;
    frmAccountInfoEditKA.flxaccountdetailsfordeposits.isVisible = false;
    frmAccountInfoEditKA.flxaccountdetailsSavings.isVisible = true;
  } else if (accountType === kony.retailBanking.globalData.globals.CreditCard){
    frmAccountInfoEditKA.skin = sknaccountCreditBkg;
    frmAccountInfoEditKA.titleBarAccountInfo.skin = sknaccountTypeCredit;
    frmAccountInfoEditKA.accountDetailsContainer.isVisible = true;
    frmAccountInfoEditKA.flxaccountdetailsfordeposits.isVisible = false;
    frmAccountInfoEditKA.flxaccountdetailsSavings.isVisible = false;
  }else if (accountType === kony.retailBanking.globalData.globals.Deposit){
    frmAccountInfoEditKA.skin=sknaccountDepositBkg;
    frmAccountInfoEditKA.titleBarAccountInfo.skin = sknaccounttypeDeposit;
    frmAccountInfoEditKA.accountDetailsContainer.isVisible = false;
    frmAccountInfoEditKA.flxaccountdetailsfordeposits.isVisible = true;
    frmAccountInfoEditKA.flxaccountdetailsSavings.isVisible = false;
  } 
  else if (accountType == kony.retailBanking.globalData.globals.Mortgage){
    frmAccountInfoEditKA.skin=sknaccountMortageBkg;
    frmAccountInfoEditKA.titleBarAccountInfo.skin = sknaccounttypemortage;
    frmAccountInfoEditKA.accountDetailsContainer.isVisible = false;
    frmAccountInfoEditKA.flxaccountdetailsfordeposits.isVisible = true;
    frmAccountInfoEditKA.flxaccountdetailsSavings.isVisible = false;
  }

  frmAccountInfoEditKA.successIcon.opacity = 0;
  frmAccountInfoEditKA.successImage.opacity = 0;
}

function populatingAccountsInfoScreen(setAccountNickname){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmAccountDetailKA");
  var controllerContextData = controller.getContextData();
  if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
    var accountData = controllerContextData.getCustomInfo("selectedAccountObj");
    if(setAccountNickname){
      frmAccountInfoKA.accountNicknameTextfield.text = accountData["nickName"];
    }
    frmAccountInfoKA.accountNicknameTextfield.setEnabled(false);
    if(accountData["creditCardNumber"]){
      frmAccountInfoKA.accountNumberLabel.text = kony.retailBanking.util.formatingAmount.maskingLastFourDigits(accountData["creditCardNumber"]);
    }
    if(accountData["currentBalance"]){
      var creditCurrBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["currentBalance"]);
      if(creditCurrBal.indexOf('-')==-1){
        frmAccountInfoKA.CopyaccountNumberLabel0e1c2ed08e25c42.text = "-" + creditCurrBal;
      }else{
        frmAccountInfoKA.CopyaccountNumberLabel0e1c2ed08e25c42.text = creditCurrBal;
      }
    }
    if(accountData["availableBalance"]){
      frmAccountInfoKA.CopyaccountNumberLabel045821e897a8646.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["availableBalance"]);
    }
    if(accountData["dueDate"]){
      frmAccountInfoKA.CopyaccountNumberLabel014bda555f8c040.text = kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(accountData["dueDate"]);
    }
    var availablePoints = accountData["availablePoints"];
    if(availablePoints.indexOf('.')==-1){
      frmAccountInfoKA.routingNumberLabel.text  = availablePoints;
    }else{
      frmAccountInfoKA.routingNumberLabel.text = availablePoints.substring(0,availablePoints.indexOf('.'));
    }
    // frmAccountInfoKA.interestRateLabel.text = accountData["interestRate"];
    if(accountData["lastStatementBalance"]){
      frmAccountInfoKA.interestRateLabel.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["lastStatementBalance"]);
    }
    if(accountData["minimumDue"]){ 
      frmAccountInfoKA.interestEarnedLabel.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["minimumDue"]);
    }
    if(accountData["availableBalance"]){
      frmAccountInfoKA.CopyaccountNumberLabel0b9e42672fcff47.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["availableBalance"]);
    }
    if(accountData["currentBalance"]){
      frmAccountInfoKA.CopyroutingNumberLabel07e049f0450604c.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["currentBalance"]);
    }
    frmAccountInfoKA.CopyinterestRateLabel0cf90d54edd014c.text = kony.retailBanking.util.maskAccountNumber(accountData["accountID"]);
    frmAccountInfoKA.CopyaccountNumberLabel036c71ac511b84d.text = kony.retailBanking.util.maskAccountNumber(accountData["accountID"]);
    frmAccountInfoKA.CopyroutingNumberLabel06e90c7d5900842.text = accountData["interestRate"]+"%";
    if(accountData["currentBalance"]){
      frmAccountInfoKA.CopyinterestRateLabel09f06e1600f0e43.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["currentBalance"]);
    }
    frmAccountInfoKA.CopyinterestRateLabel0b14fb4af446e45.text = accountData["paymentTerm"];
    if(accountData["openingDate"]){
      frmAccountInfoKA.CopyinterestEarnedLabel00b76261e56e741.text = kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(accountData["openingDate"]);
    }
    if(accountData["maturityDate"]){
      frmAccountInfoKA.CopyinterestEarnedLabel0daf983bec94d4c.text = kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(accountData["maturityDate"]);
    }
  }
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  frmAccountInfoKA.show();
}
function populatingAccountsInfoEditScreen(){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmAccountDetailKA");
  var formmodel = controller.getFormModel();
  var controllerContextData = controller.getContextData();
  if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
    var accountData = controllerContextData.getCustomInfo("selectedAccountObj");
    /*frmAccountInfoEditKA.routingNumberLabel.text = "8300399378";
          frmAccountInfoEditKA.interestEarnedLabel.text = "$5.24";
          frmAccountInfoEditKA.interestRateLabel.text = "0.25%";
          frmAccountInfoEditKA.CopyaccountNumberLabel0b9e42672fcff47.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["availableBalance"]);
          frmAccountInfoEditKA.CopyroutingNumberLabel07e049f0450604c.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["currentBalance"]);
          frmAccountInfoEditKA.CopyinterestRateLabel0cf90d54edd014c.text = accountData["accountID"];
          frmAccountInfoEditKA.CopyaccountNumberLabel036c71ac511b84d.text = accountData["accountID"];
          frmAccountInfoEditKA.CopyroutingNumberLabel06e90c7d5900842.text = accountData["interestRate"];
          frmAccountInfoEditKA.CopyinterestRateLabel09f06e1600f0e43.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["currentBalance"]);
          frmAccountInfoEditKA.CopyinterestRateLabel0b14fb4af446e45.text = "24 months";
          frmAccountInfoEditKA.CopyinterestEarnedLabel00b76261e56e741.text = "11/20/2015";
          frmAccountInfoEditKA.CopyinterestEarnedLabel0daf983bec94d4c.text = "11/20/2017";*/
    if(accountData["creditCardNumber"]){
      frmAccountInfoEditKA.accountNumberLabel.text = kony.retailBanking.util.formatingAmount.maskingLastFourDigits(accountData["creditCardNumber"]);
    }
    if(accountData["currentBalance"]){ 
      var creditCurrBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["currentBalance"]);
      if(creditCurrBal.indexOf('-')==-1){
        frmAccountInfoEditKA.CopyaccountNumberLabel0e1c2ed08e25c42.text = "-" + creditCurrBal;
      }else{
        frmAccountInfoEditKA.CopyaccountNumberLabel0e1c2ed08e25c42.text = creditCurrBal;
      }
    }
    if(accountData["availableBalance"]){
      frmAccountInfoEditKA.CopyaccountNumberLabel045821e897a8646.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["availableBalance"]);
    }
    if(accountData["dueDate"]){
      frmAccountInfoEditKA.CopyaccountNumberLabel014bda555f8c040.text = kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(accountData["dueDate"]);
    }
    var availablePoints = accountData["availablePoints"];
    if(availablePoints.indexOf('.')==-1){
      frmAccountInfoEditKA.routingNumberLabel.text = availablePoints;
    }else{
      frmAccountInfoEditKA.routingNumberLabel.text = availablePoints.substring(0,availablePoints.indexOf('.'));
    }
    // frmAccountInfoKA.interestRateLabel.text = accountData["interestRate"];
    if(accountData["lastStatementBalance"]){
      frmAccountInfoEditKA.interestRateLabel.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["lastStatementBalance"]);
    }
    if(accountData["minimumDue"]){
      frmAccountInfoEditKA.interestEarnedLabel.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["minimumDue"]);
    }
    if(accountData["availableBalance"]){
      frmAccountInfoEditKA.CopyaccountNumberLabel0b9e42672fcff47.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["availableBalance"]);
    }
    if(accountData["currentBalance"]){
      frmAccountInfoEditKA.CopyroutingNumberLabel07e049f0450604c.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["currentBalance"]);
    }
    var maskNum = kony.retailBanking.util.maskAccountNumber(accountData["accountID"]);
    var accountNum = new kony.sdk.mvvm.Data(accountData["accountID"],maskNum);
    formmodel.setWidgetData("CopyinterestRateLabel0cf90d54edd014c", accountNum);
    formmodel.setWidgetData("CopyaccountNumberLabel036c71ac511b84d", accountNum);

    frmAccountInfoEditKA.CopyroutingNumberLabel06e90c7d5900842.text = accountData["interestRate"]+"%";
    if(accountData["currentBalance"]){
      frmAccountInfoEditKA.CopyinterestRateLabel09f06e1600f0e43.text = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountData["currentBalance"]);
    }
    frmAccountInfoEditKA.CopyinterestRateLabel0b14fb4af446e45.text = accountData["paymentTerm"];
    if(accountData["openingDate"]){
      frmAccountInfoEditKA.CopyinterestEarnedLabel00b76261e56e741.text = kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(accountData["openingDate"]);
    }
    if(accountData["maturityDate"]){
      frmAccountInfoEditKA.CopyinterestEarnedLabel0daf983bec94d4c.text = kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(accountData["maturityDate"]);
    }
  }
}



//Dashboard
var gblAllAccounts=[];
var gblFromAccounts=[];

function accountLandingDashboard(data,segment){
  kony.print("processAccounts:::::11"+JSON.stringify(data[segment][segment].getData()));
  try {
    var segAccountListData = data[segment][segment].getData();;
    gblAllAccounts = data[segment][segment].getData();;
    var accountsData = [],dealsData = [],loansData = [];
    var accountPartitionData = {};
    var sectionAccData = [],segDealsData = [],segLoansData = [];
    if(segAccountListData && segAccountListData.length>0)
    {
      for(var i in segAccountListData)
      {  
        if((segAccountListData[i]["accountType"] == "S" || segAccountListData[i]["accountType"] == "C" || segAccountListData[i]["accountType"] == "Y" || segAccountListData[i]["accountType"] == "CC" || segAccountListData[i]["accountType"] == "B") && segAccountListData[i]["productCode"] != "320"){
          accountsData.push(segAccountListData[i]);
        }if(segAccountListData[i]["accountType"] == "T"){
          dealsData.push(segAccountListData[i]);
        }
        if(segAccountListData[i]["accountType"] == "L" ){
          loansData.push(segAccountListData[i]);
        }
      }

      kony.print("gblAllAccounts :: "+JSON.stringify(gblAllAccounts));
      kony.print("accountsData:::"+JSON.stringify(accountsData));
      kony.print("dealsData:::"+JSON.stringify(dealsData));
      kony.print("loansData:::"+JSON.stringify(loansData));
      gblFromAccounts = accountsData;
      accountPartitionData.accountsData = accountsData || {};
      accountPartitionData.dealsData = dealsData || {};
      accountPartitionData.loansData = loansData || {};
      //Accounts-----------------------------------------------------------------------------------------------
      if(accountsData && accountsData.length > 0)
      {

        kony.print("Inside accounts:::");
        for(var i in accountsData)
        {
          var segformat = [];
          var segRowData = [];
          var aHeader  = {};
          var availableBal,currBal,outstandingBal,accType;

          var accountNumber = accountsData[i]["accountID"];
          aHeader["nickName"] =  accountsData[i]["accountName"] +"***"+ accountNumber.substring(accountNumber.length-3, accountNumber.length);
          if(accountsData[i]["availableBalance"]){
            availableBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountsData[i]["availableBalance"]);
          }
          if(accountsData[i]["currentBalance"]){
            currBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountsData[i]["currentBalance"]);
          }
          if(accountsData[i]["outstandingBalance"]){
            outstandingBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountsData[i]["outstandingBalance"]);
          }
          if(accountsData[i]["accountType"]=="C"){
            currBal = "-"+currBal;
          }
          aHeader["availableBalance"] = availableBal;
          aHeader["currentBalance"] = currBal;
          aHeader["outstandingBalance"] = outstandingBal;
          if(accountsData[i]["accountType"]=="C"){
            accType = "Current account";
          }else if(accountsData[i]["accountType"]=="S"){
            accType = "Saving account";
          }else if(accountsData[i]["accountType"]=="Y"){
            accType = "Corporate account";
          }
          aHeader["accountType"] = accType;
          segformat.push(aHeader); 
          segRowData = [{"lblTransDate" : "22 Aug 2017","lblTransName" : "Jordon water", "lblTransAmount" : 130.201},
                        {"lblTransDate" : "22 Jan 2018","lblTransName" : "Jordon gas", "lblTransAmount" : 150.201}]
          segformat.push(segRowData);
          sectionAccData.push(segformat);
        }
        kony.print("sectionAccData:::"+JSON.stringify(sectionAccData));
      }


      //Deals-----------------------------------------------------------------------------------------------
      if(dealsData && dealsData.length > 0)
      {

        kony.print("Inside Deals section:::");
        for(var i in dealsData)
        {
          var segformat = [];
          var segRowData = [];
          var aHeader  = {};
          var availableBal,currBal,outstandingBal,accType;

          var accountNumber = dealsData[i]["accountID"];
          aHeader["nickName"] =  dealsData[i]["accountName"] +"***"+  accountNumber.substring(accountNumber.length-3, accountNumber.length);
          if(dealsData[i]["availableBalance"]){
            availableBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(dealsData[i]["availableBalance"]);
          }
          if(dealsData[i]["currentBalance"]){
            currBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(dealsData[i]["currentBalance"]);
          }
          if(dealsData[i]["outstandingBalance"]){
            outstandingBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(dealsData[i]["outstandingBalance"]);
          }
          if(dealsData[i]["accountType"]=="C"){
            currBal = "-"+currBal;
          }
          aHeader["availableBalance"] = availableBal;
          aHeader["currentBalance"] = currBal;
          aHeader["outstandingBalance"] = outstandingBal;
          aHeader["btnNav"] = {
            onClick: function(){kony.print("Do Nothing:::")}

          };
          if(dealsData[i]["accountType"]=="T"){
            accType = "Term Deposite";
          }
          aHeader["accountType"] = accType;
          segDealsData.push(aHeader); 
        }
        kony.print("segDealsData:::"+JSON.stringify(segDealsData));
      }

      //Loans-----------------------------------------------------------------------------------------------
      if(loansData && loansData.length > 0)
      {

        kony.print("Inside Loans section:::");
        for(var i in loansData)
        {
          var segformat = [];
          var segRowData = [];
          var aHeader  = {};
          var availableBal,currBal,outstandingBal,accType;

          var accountNumber = loansData[i]["accountID"];
          aHeader["nickName"] =  loansData[i]["accountName"] +"***"+ accountNumber.substring(accountNumber.length-3, accountNumber.length);
          if(loansData[i]["availableBalance"]){
            availableBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(loansData[i]["availableBalance"],"");
          }
          if(loansData[i]["currentBalance"]){
            currBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(loansData[i]["currentBalance"],"");
          }
          if(loansData[i]["outstandingBalance"]){
            outstandingBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(loansData[i]["outstandingBalance"],"");
          }
          if(loansData[i]["accountType"]=="C"){
            currBal = "-"+currBal;
          }
          aHeader["availableBalance"] = availableBal;
          aHeader["currentBalance"] = currBal;
          aHeader["outstandingBalance"] = outstandingBal;
          if(accountsData[i]["accountType"]=="L"){
            accType = "Loans";
          }
          aHeader["accountType"] = accType;
          segLoansData.push(aHeader); 
        }
        kony.print("segLoansData:::"+JSON.stringify(segLoansData));
      }  
      /* data = {"segAccountsKADeals" :{"segAccountsKADeals":{}},
                "segAccountsKALoans" :{"segAccountsKALoans":{}},
                "segAccountsKA" :{"segAccountsKA":{}}
               } 
         data[segment][segment].setData(sectionAccData);
        data["segAccountsKADeals"]["segAccountsKADeals"].setData(segDealsData);
        data["segAccountsKALoans"]["segAccountsKALoans"].setData(segLoansData); */
    }  
    data[segment][segment].setData(sectionAccData);
    return [sectionAccData,segDealsData,segLoansData];
  }
  catch (e) {
    kony.print("exception in frmAccountsLandingControllerExtension processAccounts - " + JSON.stringify(e));
    exceptionLogCall("accountLandingDashboard","UI ERROR","UI",e);
  }
}














//Processing Account Data


function accountListsetting(Data,segmentName){
  kony.print("accountListsetting:::Done"+JSON.stringify(Data));
  var segAccountListData = Data[segmentName][segmentName].getData();
  var availableBal,currBal,outstandingBal;
  if(segAccountListData && segAccountListData.length>0)
  {
    var processedSegData = [ ];
    var processedRowObj;
    for(var i in segAccountListData){
      processedRowObj = {};
      processedRowObj["lblAccountID"]=segAccountListData[i]["accountID"];
      if(segAccountListData[i]["nickName"]){
        processedRowObj["nickName"] = kony.retailBanking.util.validation.trucateTo(segAccountListData[i]["nickName"],35,32,"...");
      }else{
        var accountNumber = segAccountListData[i]["accountID"];
        processedRowObj["nickName"] =  segAccountListData[i]["accountName"] + accountNumber.substring(accountNumber.length-4, accountNumber.length);
      }
      if(segAccountListData[i]["availableBalance"]){
        availableBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(segAccountListData[i]["availableBalance"]);
      }
      if(segAccountListData[i]["currentBalance"]){
        currBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(segAccountListData[i]["currentBalance"]);
      }
      if(segAccountListData[i]["outstandingBalance"]){
        outstandingBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(segAccountListData[i]["outstandingBalance"]);
      }
      if(segAccountListData[i]["accountType"]=="CreditCard"){
        currBal = "-"+currBal;
      }
      var accountType = segAccountListData[i]["accountType"];
      if (accountType === kony.retailBanking.globalData.globals.Checking){
        processedRowObj["availableBalance"] = {
          "isVisible": true,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: availableBal
        };
        processedRowObj["currentBalance"] = {
          "isVisible": false,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: currBal
        };
        processedRowObj["outstandingBalance"] = {
          "isVisible": false,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: outstandingBal
        };
        processedRowObj["accountType"] = i18n_availableBalance;
      } else if (accountType === kony.retailBanking.globalData.globals.Savings){
        processedRowObj["availableBalance"] = {
          "isVisible": true,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: availableBal
        };
        processedRowObj["currentBalance"] = {
          "isVisible": false,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: currBal
        };
        processedRowObj["outstandingBalance"] = {
          "isVisible": false,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: outstandingBal
        };
        processedRowObj["accountType"] = i18n_availableBalance;
      } else if (accountType === kony.retailBanking.globalData.globals.CreditCard){
        processedRowObj["availableBalance"] = {
          "isVisible": false,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: availableBal
        };
        processedRowObj["currentBalance"] = {
          "isVisible": true,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: currBal
        };
        processedRowObj["outstandingBalance"] = {
          "isVisible": false,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: outstandingBal
        };
        processedRowObj["accountType"] = i18n_currentBalance;
      }else if (accountType === kony.retailBanking.globalData.globals.Deposit){
        processedRowObj["availableBalance"] = {
          "isVisible": false,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: availableBal
        };
        processedRowObj["currentBalance"] = {
          "isVisible": true,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: currBal
        };
        processedRowObj["outstandingBalance"] = {
          "isVisible": false,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: outstandingBal
        };
        processedRowObj["accountType"] = i18n_currentBalance;
      } 
      else if (accountType == kony.retailBanking.globalData.globals.Mortgage){
        processedRowObj["availableBalance"] = {
          "isVisible": false,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: availableBal
        };
        processedRowObj["currentBalance"] = {
          "isVisible": false,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: currBal
        };
        processedRowObj["outstandingBalance"] = {
          "isVisible": true,
          skin :getSknlblAmount(segAccountListData[i]["accountType"]),
          text: outstandingBal
        };
        processedRowObj["accountType"] = "Outstanding Balance";
      }
      processedRowObj["sknColor"] = {
        skin:getSkinColor(segAccountListData[i]["accountType"])
      }
      processedSegData.push(processedRowObj);
    }
    Data[segmentName][segmentName].setData(processedSegData);
  }else{

  }
  return Data;
}
function transactionListformatting(Data,segmentName){
  var segTransactionListData = Data[segmentName][segmentName].getData();
  if(segTransactionListData && segTransactionListData.length>0)
  {
    var processedSegData = [ ];
    var processedRowObj;
    for(var i in segTransactionListData){
      if(segTransactionListData[i]["isScheduled"]==="false"){
        processedRowObj = {};
        processedRowObj["description"] = kony.retailBanking.util.validation.trucateTo(segTransactionListData[i]["description"],35,32,"...");
        processedRowObj["amount"] = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(segTransactionListData[i]["amount"]);
        processedRowObj["transactionDate"] = kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(segTransactionListData[i]["transactionDate"]);
        /*if(segTransactionListData[i]["checkImage"]=="cheque"){
       processedRowObj["Imgcheck"] = {"src":"checkf.png","isVisible":true};
        }else{
      processedRowObj["Imgcheck"] = {"src":"checkf.png","isVisible":false};
    }*/
        if(segTransactionListData[i]["hasDepositImage"] !== "false"){
          processedRowObj["Imgcheck"] = {
            "isVisible": true,
            src :"checkf.png"
          };
        }else{
          if(segTransactionListData[i]["statusDescription"] === kony.retailBanking.globalData.globals.Failed){
            processedRowObj["Imgcheck"] = {
              "isVisible": true,
              src :"failedimage.png"
            }; 
          }else{
            processedRowObj["Imgcheck"] = {
              "isVisible": false,
              src :"failedimage.png"
            }; 
            if(segTransactionListData[i]["frequencyType"] !==undefined) 
            {
              processedRowObj["Imgcheck"] = {
                "isVisible": true,
                src :"recuurencebox.png"
              }; 
            } else
            {
              processedRowObj["Imgcheck"] = {
                "isVisible": false,
                src :"recuurencebox.png"
              };
            }
          }
        }    
        processedSegData.push(processedRowObj);
      } 
    }
    Data[segmentName][segmentName].setData(processedSegData);
  }
  return Data;
}



function tnCAccountsError(err)
{
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function updateAcceptTermnsnCofAccounts(){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options ={    "access": "online",
                "objectName": "RBObjects"
               };
  var headers = {"session_token":kony.retailBanking.globalData.session_token};
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("User",serviceName,options);
  var record = {};
  record["areAccountStatementTermsAccepted"] = true;
  var dataObject = new kony.sdk.dto.DataObject("User",record);
  var requestOptions = {"dataObject":dataObject, "headers":headers};
  modelObj.update(requestOptions, updateaccountsTnCSuccess, updateaccountsTnCError);

}

function updateaccountsTnCSuccess(response)
{
  //setUserObj();
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmacntstatementsKA");
  var navObject = new kony.sdk.mvvm.NavigationObject();

  var AccountDetailscontroller = INSTANCE.getFormController("frmAccountDetailKA");
  var controllerContextData = AccountDetailscontroller.getContextData();
  if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
    var accountData = controllerContextData.getCustomInfo("selectedAccountObj");
    var accountId = accountData["accountID"];
    navObject.setRequestOptions("contactsegment",{"headers":{"session_token":kony.retailBanking.globalData.session_token}, "queryParams" : {"accountID": accountId}});
    controller.performAction("loadDataAndShowForm",[navObject]);
  }
  // navObject.setRequestOptions("contactsegment",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});

  //getAccountSatements("frmacntstatementsKA");
}

function updateaccountsTnCError(err)
{
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}



function navigateToAcceptAccountStatements(frmForm,toForm){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController(frmForm);
  var controllerContextData = controller.getContextData();
  if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
    var accountData = controllerContextData.getCustomInfo("selectedAccountObj");
    var accountId = accountData["accountID"];
    var datamodel = new kony.sdk.mvvm.DataModel;

    var navigationObject = new kony.sdk.mvvm.NavigationObject;
    navigationObject.setDataModel(datamodel, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
    navigationObject.setRequestOptions("contactsegment",{"headers":{"session_token":kony.retailBanking.globalData.session_token}, "queryParams" : {"accountID": accountId}});
    controller.performAction("navigateTo",[toForm,navigationObject]);
    //controller.performAction("loadDataAndShowForm",[navObject]);
  }
}
function getAccountSatements(form)
{
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController(form);
  var navObject = new kony.sdk.mvvm.NavigationObject();
  navObject.setRequestOptions("contactsegment",{"headers":{"session_token":kony.retailBanking.globalData.session_token }});
  controller.performAction("loadDataAndShowForm",[navObject]);
}
/*
AccountSatement segment Populate Data
*/
function accountListstatement(Data,stateSeg){
  var statementData = Data[stateSeg][stateSeg].getData();

  if(statementData && statementData.length>0)
  {
    var processedSegData = [ ];
    var processedRowObj;
    for(var i in statementData){
      processedRowObj = {};
      //var tempDate = kony.retailBanking.util.formatingDate.getISODateTimeKA(statementData[i]["StatementMonth"], kony.retailBanking.util.BACKEND_DATE_FORMAT);
      //processedRowObj["StatementMonth"] = (tempDate instanceof Date)? tempDate.format('F d, Y') : "";
      processedRowObj["StatementMonth"] =  kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(statementData[i]["StatementMonth"]);
      processedRowObj["StatementDescription"] = statementData[i]["StatementDescription"];
      processedRowObj["StatementLink"] = statementData[i]["StatementLink"];
      processedSegData.push(processedRowObj);
    }
    Data[stateSeg][stateSeg].setData(processedSegData);
  }
  return  Data ;                               
}

/*
  This is used for header skin mapping from service as accounttype to Static form TermsAndCond PreShow
  */

//   function accountStatTermnsPreShow(){
//     //if(gblfrmName == "Account OverView"){
//        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
//    var controller = INSTANCE.getFormController("frmAccountDetailKA");
//    var controllerContextData=controller.getContextData();
//   //if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
//      var accStatementDetail=  controllerContextData.getCustomInfo("selectedAccountObj");
//    frmTermsAndConditionsKA.titleBarWrapper.skin = getSkinColor(accStatementDetail["accountType"]);
//frmTermsAndConditionsKA.skin = getSkinColorForBg(accStatementDetail["accountType"]);
//frmTermsAndConditionsKA.androidTitleBar.skin = getSkinColor(accStatementDetail["accountType"]);
//   frmTermsAndConditionsKA.iosTitleBar.skin=getSkinColor(accStatementDetail["accountType"]);

// }

//     }else{
//       frmTermsAndConditionsKA.titleBarWrapper.skin = skncontainerBkgNone;
//     }


// }
/*
header skin for accountDetail
*/
function accountStatementDetailPreShow(){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmAccountDetailKA");
  var controllerContextData=controller.getContextData();
  //if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
  var accStatementDetailPDF=  controllerContextData.getCustomInfo("selectedAccountObj");
  frmacntstatementdetailsKA.titleBarAccountInfo.skin = getSkinColor(accStatementDetailPDF["accountType"]);
  frmacntstatementdetailsKA.skin = getSkinColorForBg(accStatementDetailPDF["accountType"]);
  // frmacntstatementdetailsKA.flxAndroidTittleBarKA.skin = getSkinColor(accStatementDetailPDF["accountType"]);
  //frmacntstatementdetailsKA.iosTitleBar.skin=getSkinColor(accStatementDetailPDF["accountType"]);

}
/* 
prepopulate the skin for accountSatement
*/
function prepopulateAccountStatements(currentForm){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var listController = INSTANCE.getFormController(currentForm);
  var controllerContextData= listController.getContextData();
  kony.print(controllerContextData);
  if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
    var accStatements =  controllerContextData.getCustomInfo("selectedAccountObj");
    frmacntstatementsKA.titleBarAccountInfo.skin = getSkinColor(accStatements["accountType"]);
    frmacntstatementsKA.skin = getSkinColorForBg(accStatements["accountType"]);
  }
}

function navigateToAccInfo(frmForm,toForm){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController(frmForm);
  var controllerContextData = controller.getContextData();
  if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
    var accountData = controllerContextData.getCustomInfo("selectedAccountObj");
    var accountId = accountData["accountID"];
    var datamodel = new kony.sdk.mvvm.DataModel;
    //datamodel.setPrimaryKeyValueMap({"accountID": accountId});
    var navigationObject = new kony.sdk.mvvm.NavigationObject;
    navigationObject.setDataModel(datamodel, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
    navigationObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}, "queryParams" : {"accountID": accountId}});
    controller.performAction("navigateTo",[toForm,navigationObject]);
  }

}

function navigateToAccInfoEdit(frmForm,toForm){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmAccountDetailKA");
  var controllerContextData = controller.getContextData();
  if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
    var accountData = controllerContextData.getCustomInfo("selectedAccountObj");
    var accountId = accountData["accountID"];
    var datamodel = new kony.sdk.mvvm.DataModel;
    //datamodel.setPrimaryKeyValueMap({"accountID": accountId});
    var navigationObject = new kony.sdk.mvvm.NavigationObject;
    navigationObject.setDataModel(datamodel, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
    navigationObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}, "queryParams" : {"accountID": accountId}});

    controller.performAction("navigateTo",[toForm,navigationObject]);

  }
}

function navigateToDetails(fromForm,toForm,segName){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController(fromForm);
  var viewModel = controller.getFormModel();
  var index = viewModel.getViewAttributeByProperty(segName, "selectedRowIndex");
  var selectedRecord = getSelectedRecord(index[1],fromForm,segName);
  var navigationObject = new kony.sdk.mvvm.NavigationObject;
  navigationObject.setCustomInfo("selectedAccountObj",selectedRecord);
  navigationObject.setCustomInfo("Peekandpop",false);
  var accountId = selectedRecord["accountID"];
  var datamodel = new kony.sdk.mvvm.DataModel;
  navigationObject.setDataModel(datamodel, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
  navigationObject.setRequestOptions("transactionSegment",{"headers":{"session_token":kony.retailBanking.globalData.session_token}, "queryParams" : {"accountID": accountId}});
  fillOptionsInMore(selectedRecord);
  controller.performAction("navigateTo",[toForm,navigationObject]);
}

function navigateToDetailsPeekandPop(fromForm,toForm,segName,index){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController(fromForm);
  //index
  var selectedRecord = getSelectedRecord(index,fromForm,segName);
  var navigationObject = new kony.sdk.mvvm.NavigationObject;
  navigationObject.setCustomInfo("selectedAccountObj",selectedRecord);
  navigationObject.setCustomInfo("Peekandpop",true);
  var accountId = selectedRecord["accountID"];
  var datamodel = new kony.sdk.mvvm.DataModel;
  navigationObject.setDataModel(datamodel, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
  navigationObject.setRequestOptions("transactionSegment",{"headers":{"session_token":kony.retailBanking.globalData.session_token}, "queryParams" : {"accountID": accountId}});
  controller.performAction("navigateTo",[toForm,navigationObject]);
  return accountId;
}
function getSelectedRecord(index,formName,segmentName){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var listController = INSTANCE.getFormController(formName);
  var controllerContextData= listController.getContextData();
  if( controllerContextData && controllerContextData.getCustomInfo(segmentName)){
    var segData = controllerContextData.getCustomInfo(segmentName);
    return segData[index];
  }else{
    return;
  }
}
function populateAccountDetailsinAccountDetails(currentForm){
  if(currentForm=="frmAccountDetailKA")
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var listController = INSTANCE.getFormController(currentForm);
  var controllerContextData= listController.getContextData();
  if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
    var accDetails =  controllerContextData.getCustomInfo("selectedAccountObj");
    if(kony.retailBanking.globalData.deviceInfo.isIphone())
      frmAccountDetailKA.skin = getSkinColorForBg(accDetails["accountType"]);
    frmAccountDetailKA.accountDetailsOverview.skin = getSkinColor(accDetails["accountType"]);
    frmAccountDetailKA.titleBarAccountDetails.skin = getSkinColor(accDetails["accountType"]);
    if(accDetails["nickName"]){
      frmAccountDetailKA.accountDetailsHeader.text = accDetails["nickName"];
    }else{
      var accountNumber = accDetails["accountID"];
      frmAccountDetailKA.accountDetailsHeader.text =  accDetails["accountName"] + accountNumber.substring(accountNumber.length-4, accountNumber.length);
    }
    frmAccountDetailKA.accountDetailsTime.text = kony.retailBanking.globalData.globals.refreshTimeLabel;
    var availableBal,currentBal,outstandingBal,paymentTerm,lastStatementBalance,maturityDate,interestRate;
    if(accDetails["availableBalance"]){
      availableBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accDetails["availableBalance"]);
    }
    if(accDetails["currentBalance"]){
      currentBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accDetails["currentBalance"]);
    }
    if(accDetails["outstandingBalance"]){
      outstandingBal = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accDetails["outstandingBalance"]);
    }
    if(accDetails["accountType"]=="CreditCard"){
      currentBal = "-"+currentBal;
    }
    if(accDetails["dueDate"]){
      paymentTerm = kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(accDetails["dueDate"]);
    } 
    if(accDetails["lastStatementBalance"]){
      lastStatementBalance = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accDetails["lastStatementBalance"]);
    }
    if(accDetails["maturityDate"]){
      maturityDate = kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(accDetails["maturityDate"]);
    }
    if(accDetails["interestRate"]){
      interestRate = accDetails["interestRate"] + "%"
    }     
    var accountType = accDetails["accountType"];
    if (accountType === kony.retailBanking.globalData.globals.Checking){
      frmAccountDetailKA.lblavailbalance.text = i18n_availableBalance;
      frmAccountDetailKA.lblaccountbalance.text = i18n_currentBalance;
      frmAccountDetailKA.availableBalanceAmount.text = availableBal;
      frmAccountDetailKA.accountBalanceAmount.text = currentBal;     
    } else if (accountType === kony.retailBanking.globalData.globals.Savings){
      frmAccountDetailKA.lblavailbalance.text = i18n_availableBalance;
      frmAccountDetailKA.lblaccountbalance.text = i18n_currentBalance;
      frmAccountDetailKA.availableBalanceAmount.text = availableBal;
      frmAccountDetailKA.accountBalanceAmount.text = currentBal;
    } else if (accountType === kony.retailBanking.globalData.globals.CreditCard){
      frmAccountDetailKA.lblavailbalance.text = i18n_PaymentDueDateg;
      frmAccountDetailKA.lblaccountbalance.text = i18n_lastStatementBalanceg;
      frmAccountDetailKA.availableBalanceAmount.text = paymentTerm;
      frmAccountDetailKA.accountBalanceAmount.text = lastStatementBalance;

    }else if (accountType === kony.retailBanking.globalData.globals.Deposit){
      frmAccountDetailKA.lblavailbalance.text = i18n_currentBalance;
      frmAccountDetailKA.lblaccountbalance.text = i18n_maturityDate;
      frmAccountDetailKA.availableBalanceAmount.text = currentBal;
      frmAccountDetailKA.accountBalanceAmount.text = maturityDate;
    } 
    else if (accountType == kony.retailBanking.globalData.globals.Mortgage){
      frmAccountDetailKA.lblavailbalance.text = kony.retailBanking.globalData.globals.OustandingBalance;
      frmAccountDetailKA.lblaccountbalance.text = kony.retailBanking.globalData.globals.InterestRate;
      frmAccountDetailKA.availableBalanceAmount.text = outstandingBal;
      frmAccountDetailKA.accountBalanceAmount.text = interestRate;

    }

    if(accDetails["accountType"] == kony.retailBanking.globalData.globals.Deposit || accDetails["accountType"] == kony.retailBanking.globalData.globals.Mortgage ){
      frmAccountDetailKA.btnaccountstatements.isVisible = false;
      frmAccountDetailKA.accountInfoButton.centerX="50%";
      frmAccountDetailKA.StatementsWrapper.setVisibility(false);
      frmAccountDetailKA.MoreWrapper.setVisibility(false);
      frmAccountDetailKA.tabsWrapper.isVisible = false;
      frmAccountDetailKA.resourcesLabel.isVisible = true;
      //frmAccountDetailKA.accountDetailsOverview.height = "190dp";
      //frmAccountDetailKA.accountDetailsTransactions.top = "220dp";
    }
    else {
      frmAccountDetailKA.btnaccountstatements.isVisible = true;
      frmAccountDetailKA.accountInfoButton.centerX="25%";
      frmAccountDetailKA.tabsWrapper.isVisible = true;
      frmAccountDetailKA.resourcesLabel.isVisible = false;
      frmAccountDetailKA.StatementsWrapper.setVisibility(true);
      frmAccountDetailKA.MoreWrapper.setVisibility(true);

      //frmAccountDetailKA.accountDetailsOverview.height = "250dp";
      //frmAccountDetailKA.accountDetailsTransactions.top = "300dp";
    } 
  }
}

function navigateToTransactionDetailsAccounts(fromForm,toForm,segName){
  frmRecentTransactionDetailsKA.repeatTransactionButton.isVisible = false;
  //frmRecentTransactionDetailsKA.Copydivider0gad115d9e99342.isVisible = true;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController(fromForm);
  var viewModel = controller.getFormModel();
  var index = viewModel.getViewAttributeByProperty(segName, "selectedRowIndex");
  var response,tempDate;
  //frmRecentTransactionDetailsKA.lblseletedIndex.text=index[1];
  var controllerContextData= controller.getContextData();
  var segData = controllerContextData.getCustomInfo("transactionSegment");
  var selectedTransactionRecord = getSelectedRecord(index[1],fromForm,segName);
  var utlHandler = applicationManager.getUtilityHandler();
  var txnObj=utlHandler.convertJSonToTransactionsObject(selectedTransactionRecord);
  var vc=new kony.rb.frmRecentTransactionDetailsKAViewController();
  vc.frmRecentTransactionDetailsKApreshow(txnObj);
  //alert(selectedTransactionRecord);
  var DataTemp={};
  for(var key in selectedTransactionRecord){
    if(key == "amount"){
      //DataTemp[key] = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(selectedTransactionRecord[key]);
      DataTemp[key] = selectedTransactionRecord[key];
    }else if(key == "transactionDate"){
      //tempDate = kony.retailBanking.util.formatingDate.getISODateTimeKA(selectedTransactionRecord[key], kony.retailBanking.util.BACKEND_DATE_FORMAT);
      // DataTemp[key] = (tempDate instanceof Date)? tempDate.format('F d, Y') : "";
      //DataTemp[key] = kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(selectedTransactionRecord[key]);
      DataTemp[key] = selectedTransactionRecord[key];
    }else{
      DataTemp[key] = selectedTransactionRecord[key];
    }
  }
  if(selectedTransactionRecord.transactionType != "Deposit"){
    var navigationObject = new kony.sdk.mvvm.NavigationObject();
    navigationObject.setCustomInfo("selectedTransactionObj",DataTemp); 
    navigationObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
    controller.performAction("navigateTo",[toForm,navigationObject]);
  }else{
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmAccountDetailKA");
    var navigationObject = listController.getContextData();
    var accDetails =  navigationObject.getCustomInfo("selectedAccountObj");
    frmRecentDepositKA.skin = getSkinColorForBg(accDetails.accountType);
    frmRecentDepositKA.titleBarWrapper.skin = getSkinColor(accDetails.accountType);
    navigationObject.setCustomInfo("selDeposit",DataTemp);
    recentDepositPreshow();
  }
}

function paintHeaderInRecentTransaction(){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmAccountDetailKA");
  var controllerContextData=controller.getContextData();
  var accDetails =  controllerContextData.getCustomInfo("selectedAccountObj");
  frmRecentTransactionDetailsKA.skin = getSkinColorForBg(accDetails.accountType);
  frmRecentTransactionDetailsKA.titleBarWrapper.skin = getSkinColor(accDetails.accountType);
}

function resetPaintedHeaderInRecentTransaction(){
  frmRecentTransactionDetailsKA.skin = "sknmainGradient";
  frmRecentTransactionDetailsKA.titleBarWrapper.skin = "skncontainerBkgNone";
}

// AccountInfo Call

var ph;
//var phNum="";
function alertcall(){

  // ph = "284-123-1234";
  //phNum="284-123-1233";
  // var msg;
  var selRow = frmAccountInfoKA.contactSegmentList.selectedIndex[1];
  if(selRow == 0)
  {
    ph = "284-123-1234";
  }else {
    ph="284-123-1233";
  }
  kony.ui.Alert({
    "message": ph,
    "alertType": constants.ALERT_TYPE_CONFIRMATION,
    "alertTitle": i18n_call,
    "yesLabel": i18n_call,
    "noLabel": i18n_cancelC,
    "alertIcon": "phone_icon_inactive.png",
    "alertHandler": userResponse
  }, {
    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
  });

  function userResponse(response)
  {
    try
    {
      if(response === true)
        kony.phone.dial(ph);
    } 
    catch(e)
    {
      alert(i18n_dailError+" "+e);
      exceptionLogCall("userResponse","UI ERROR","UI",e);
    }
  }
}
function alertBankCall(filterParam){
  if(!filterParam){
    ph="284-123-1234";
  }
  else{
    ph="284-123-3465";
  }
  kony.ui.Alert({
    "message": ph,
    "alertType": constants.ALERT_TYPE_CONFIRMATION,
    "alertTitle": i18n_call,
    "yesLabel": i18n_call,
    "noLabel": i18n_cancelC,
    "alertIcon": "phone_icon_inactive.png",
    "alertHandler": userResponse
  }, {
    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
  });

  function userResponse(response)
  {
    try
    {
      if(response === true)
        kony.phone.dial(ph);
    } 
    catch(err)
    {
      alert(i18n_dailError+" "+err);
    }
  }
}

function alertcallAccountInfoEditKA(){

  // ph = "284-123-1234";
  //phNum="284-123-1233";
  // var msg;
  var selRow = frmAccountInfoEditKA.contactSegmentList.selectedIndex[1];
  if(selRow == 0)
  {
    ph = "284-123-1234";
  }else {
    ph="284-123-1233";
  }
  kony.ui.Alert({
    "message": ph,
    "alertType": constants.ALERT_TYPE_CONFIRMATION,
    "alertTitle": i18n_call,
    "yesLabel": i18n_call,
    "noLabel": i18n_cancelC,
    "alertIcon": "phone_icon_inactive.png",
    "alertHandler": userResponse
  }, {
    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
  });

  function userResponse(response)
  {
    try
    {
      if(response === true)
        kony.phone.dial(ph);
    } 
    catch(e)
    {
      alert(i18n_dailError+" "+e);
      exceptionLogCall("userResponse","UI ERROR","UI",e);
    }
  }
}
/**
 * @function registerForPeekAndPop
 *
 */
function registerForPeekAndPop () {
  frmAccountsLandingKA.segAccountsKA.registerForPeekAndPop(onPeekCallback, onPopCallback);
  var previewActionItems = [
    {
      "type": constants.PREVIEW_ACTION_TYPE_INDIVIDUAL,
      "title": i18n_searchTransactions,
      "style": constants.PREVIEW_ACTION_STYLE_DEFAULT,
      "onPreviewAction":navaigateToAccountDetailPAP
    },
    {
      "type": constants.PREVIEW_ACTION_TYPE_INDIVIDUAL,
      "title": i18n_viewAcntInfo,
      "style": constants.PREVIEW_ACTION_STYLE_DEFAULT,
      "onPreviewAction":navaigateToAccountInfoPAP
    },
    {
      "type": constants.PREVIEW_ACTION_TYPE_INDIVIDUAL,
      "title": i18n_viewAcntStatements,
      "style": constants.PREVIEW_ACTION_STYLE_DEFAULT,
      "onPreviewAction":navaigateToAccountStatementPAP
    }
  ];
  frmAccountDetailKA.setPreviewActionItems (previewActionItems);
}
/**
 * @function onPeekCallback
 *
 */
function onPeekCallback (widgetRef, contextInfo) {
  var Index = contextInfo.rowIndex;
  var rowFocus = contextInfo.rowRect;
  var previewInfoTable = {"peekForm": frmAccountDetailKA, "focusRect": rowFocus, "contentSize": [320, 480]};
  var accountID = navigateToDetailsPeekandPop("frmAccountsLandingKA","frmAccountDetailKA","segAccountsKA",Index);
  frmAccountDetailKA.info = {"accountId": accountID}
  return previewInfoTable;
}

/**
 * @function onPopCallback
 *
 */
function onPopCallback (widgetRef, peekForm) {
  return peekForm;  
}

/**
 * @function readAction
 *
 */
function navaigateToAccountDetailPAP () {
  var account = frmAccountDetailKA.info.accountId;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var frmController = INSTANCE.getFormController("frmAccountDetailKA");
  var navObject = new kony.sdk.mvvm.NavigationObject();
  var acnt = {"id":account};
  navObject.setCustomInfo("selAccount",acnt);
  navObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
  clearData();
  frmController.performAction("navigateTo",["frmSearchOptionsKA",navObject]); 
}

/**
 * @function readAction1
 *
 */
function navaigateToAccountInfoPAP () {
  var account = frmAccountDetailKA.info.accountId;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmAccountDetailKA");
  var accountId = account;
  var datamodel = new kony.sdk.mvvm.DataModel;
  var navigationObject = new kony.sdk.mvvm.NavigationObject;
  navigationObject.setDataModel(datamodel, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
  navigationObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}, "queryParams" : {"accountID": accountId}});
  controller.performAction("navigateTo",["frmAccountInfoKA",navigationObject]);
}

/**
 * @function readAction2
 *
 */
function navaigateToAccountStatementPAP () {
  var account = frmAccountDetailKA.info.accountId;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmacntstatementsKA");
  var navObject = new kony.sdk.mvvm.NavigationObject();
  var accountId = account;// accountData["accountID"];
  navObject.setRequestOptions("contactsegment",{"headers":{"session_token":kony.retailBanking.globalData.session_token}, "queryParams" : {"accountID": accountId}});
  controller.performAction("loadDataAndShowForm",[navObject]);
}


/**
 * @function appForceTouchCallBack
 *
 */
function appForceTouchCallBack (params) {
  var launchParams = params.launchparams;
  var launchMode = params.launchmode;
  if(launchMode === 2){
    if(launchParams && launchParams.type){
      if(launchParams.type === "map"){
        kony.retailbanking.pushNotificationsHandler.launchingFromNotification = true;
        kony.retailbanking.pushNotificationsHandler.isAppInBackGround = false;
        var obj = new kony.retailbanking.pushNotificationsHandler();
        obj.setNotificationData(launchParams);
        return frmLocatorKA;
      }
      if(launchParams.type === "overdraft"){

      }
    }
  }
  else{


    if (null !== params && params.launchmode === 3) {
      var quickActionItem = params.launchparams.quickactionitem;
      if(quickActionItem){
        if (quickActionItem.id === "ATM finder") {
          menuIconForceTouch = true;
          if(isLoggedIn){
            onClickLocateUS(true, "frmMoreLandingKA");
          }else{
            onClickLocateUS(false,"frmLoginKA");
          }
          return frmLocatorKA;
        }
        else if(quickActionItem.id == "Transfer Money" && isLoggedIn == true) {
          menuIconForceTouch = true;
          navigateToNewTransferForm("InitialLanding", null);
          return frmNewTransferKA;
        }
        else if(quickActionItem.id == "Pay a Bill" && isLoggedIn == true) {
          menuIconForceTouch = true;
          BillPayfromForm="NewBillPay";
          navigateToNewBillPayForm("InitialLanding", null);
          return frmNewBillKA;
        }
        else if(isLoggedIn == false){
          defaultPage = quickActionItem.id;
          return frmLoginKA;
        }
        else{
          return frmLoginKA;
        }
      }
    }
  }
}
function navOptForAccountDetails()
{
  if(kony.retailBanking.globalData.globals.nav_Object["accountType"]=="CreditCard")
  {
    frmAccountDetailKA.segAccountDetailNav1KA.isVisible=false;
    frmAccountDetailKA.segAccntCreditCardDetailsKA.isVisible=true;
    frmAccountDetailKA.flxNavSegment1.height="150dp";
    frmAccountDetailKA.flxNavSegment1.centerY="80%";
  }
  else
  {
    frmAccountDetailKA.segAccountDetailNav1KA.isVisible=true;
    frmAccountDetailKA.segAccntCreditCardDetailsKA.isVisible=false;
    frmAccountDetailKA.flxNavSegment1.height="300dp";
    frmAccountDetailKA.flxNavSegment1.centerY="70%";
  }
}
function setAccountDetailData(Data,recSeg)
{
  kony.print("Data ::"+JSON.stringify(Data)+" "+recSeg);
  var transacData = Data[recSeg];  
  var recentSegData = [];
  var scheduleSegData = [];
  var nextSet = 0;
  var total = 0;
  kony.print("primaryKeyValueMap ::"+transacData.length);
  if(!isEmpty(transacData[0].transactionId)){

    //   if(transacData.length == 1 && (transacData[0].primaryKeyValueMap.length == undefined || transacData[0].primaryKeyValueMap.length == 0)){
    //   	scheduleSegData[0] = [];
    //   }else{
    for(var i in transacData)
    {   
      var status = transacData[i]["statusDescription"];
      if(transacData[i]["statusDescription"] === kony.retailBanking.globalData.globals.Failed) 
      {
        //       transacData[i]["transactiontype"] = {
        //         "isVisible": true,
        //         src :"failedimage.png"
        //       }; 
      } else
      {
        //       transacData[i]["transactiontype"] = {
        //         "isVisible": false,
        //         src :"failedimage.png"
        //       };
      }
      if(status !== kony.retailBanking.globalData.globals.Failed) 
      {
        if(transacData[i]["frequencyType"] !==undefined && transacData[i]["frequencyType"] !== "Once") 
        {
          //         transacData[i]["transactiontype"] = {
          //           "isVisible": true,
          //           src :"recuurencebox.png"
          //         }; 
        } else
        {
          //         transacData[i]["transactiontype"] = {
          //           "isVisible": false,
          //           src :"recuurencebox.png"
          //         };
        }
      }else
      {
        if(transacData[i]["frequencyType"] !==undefined) 
        {
          //         transacData[i]["transactiontype"] = {
          //           "isVisible": false,
          //           src :"recuurencebox.png"
          //         };
        }

      }
      if(transacData[i]["transactionType"]==="Cardless"){
        if(transacData[i]["cashWithdrawalTransactionStatus"]==="pending"){
          var utlHandler = applicationManager.getUtilityHandler();
          var expiryValue=utlHandler.calculateExpiryTimeInHrsandMins(transacData[i].cashlessOTPValidDate);
          //var mins=utlHandler.calculateExpiryTimeInMins(transacData[i].cashlessOTPValidDate);
          //var expiryValue=hrs+"h:"+mins+"m";
          if(expiryValue!==-1){
            transacData[i]["lblExpiryTime"]={
              //  "isVisible": true,
              "text":expiryValue
            };
            //           transacData[i]["ImgWithDraw"] = {
            //             "isVisible": true,
            //             src :"pending.png"
            //           };
          }
          else{
            //           transacData[i]["lblExpiryTime"]={
            //             "isVisible": false
            //           };
            //           transacData[i]["ImgWithDraw"] = {
            //             "isVisible": false
            //           };
          }
        }
        else{
          //         transacData[i]["lblExpiryTime"]={
          //           "isVisible": false
          //         };
          //         transacData[i]["ImgWithDraw"] = {
          //           "isVisible": false
          //         }; 
        }
        transacData[i]["fromAccountName"] = transacData[i]["fromAccountName"];
        transacData[i]["lblCashlessOTP"]=transacData[i]["cashlessOTP"];
        transacData[i]["lblCashlessMode"]=transacData[i]["cashlessMode"];
        transacData[i]["lblNotesKA"]=transacData[i]["transactionNotes"];
      }
      else{
        //       transacData[i]["ImgWithDraw"] = {
        //         "isVisible": false,
        //         src :"pending.png"
        //       };
        //       transacData[i]["lblExpiryTime"] = {
        //         "isVisible": false
        //       };

      }

      //if(transacData[i]["transactionType"] !== kony.retailBanking.globalData.globals.Deposit)

      //transacData[i]["amount"] = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(transacData[i]["amount"]);
      if(transacData[i]["amount"] == null || transacData[i]["amount"] == "null"){
        transacData[i]["amount"] = "0.000";
      }
      transacData[i]["description"] = {"text":transacData[i]["description"],"isVisible":true};
      transacData[i]["lblLastTransaction"] = {"text":"","isVisible":false};
      transacData[i]["amount"] = formatamountwithCurrency(transacData[i]["amount"],gblAccountsCurrencyType);
      kony.print("transaction amount ::"+transacData[i]["amount"]);
      if((transacData[i]["category"] == "C") && ((transacData[i]["category"] != null) && (transacData[i]["category"] != undefined))){
        transacData[i]["amount"] = {"text":"+ "+transacData[i]["amount"]+" "+customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].currencyCode,"isVisible":true };    
        transacData[i]["category"] = "credit";
      }else if((transacData[i]["category"] == "D") && ((transacData[i]["category"] != null) && (transacData[i]["category"] != undefined))){
        transacData[i]["amount"] = {"text":"- "+transacData[i]["amount"]+" "+customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].currencyCode,"isVisible":true};
        transacData[i]["category"] = "debit";
      }
      if(transacData[i]["category"] === undefined)
      {
        kony.print("inside uncleared transaction");
        total = total+parseFloat(transacData[i]["amount"].replace(/,/g,""));
        transacData[i]["amount"] = {"text":transacData[i]["amount"]+" "+customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].currencyCode,"isVisible":true};
        var outp1 = transacData[i]["transactionDate"].split('-');
        transacData[i]["transactionDate"]= {"text":outp1[2]+"/"+outp1[1]+"/"+outp1[0],"isVisible":true};
        recentSegData.push(transacData[i]);
      }      
      else
      {
        //         if(i == 5 || i == 49 || i== 74){
        //           //kony.retailBanking.globalData.accountsTransactionList[nextSet] = scheduleSegData;
        //           nextSet = nextSet +1;
        //           kony.print("nextSet ::"+nextSet);
        //         }
        var outp = transacData[i]["transactionDate"].split('-');
        transacData[i]["transactionDate"]= {"text":outp[2]+"/"+outp[1]+"/"+outp[0],"isVisible":true};
        transacData[i]["lblLastTransaction"] = {"text":"","isVisible":false};
        kony.print("formatted date ::"+transacData[i]["transactionDate"]);
        scheduleSegData.push(transacData[i]);//kony.retailBanking.globalData.accountsTransactionList

      }



    }
    // 	kony.print("scheduleSegData ::"+JSON.stringify(scheduleSegData[nextSet]));
    //   }

    //   if(recentSegData.length == 0)
    //   {
    //     frmAccountDetailKA.transactionSegment.setVisibility(false);
    //     frmAccountDetailKA.LabelNoRecordsKA.setVisibility(true);
    //   }
    //   else
    //   {
    //     frmAccountDetailKA.transactionSegment.setVisibility(true);
    //     frmAccountDetailKA.LabelNoRecordsKA.setVisibility(false);
    //   }


    //   if(scheduleSegData.length == 0)
    //   {
    //     frmAccountDetailKA.scheduledTransactionSegment.setVisibility(false);
    //     frmAccountDetailKA.lblSheduledNoRecordsKA.setVisibility(true);
    //   }
    //   else
    //   {
    //     frmAccountDetailKA.scheduledTransactionSegment.setVisibility(true);
    //     frmAccountDetailKA.lblSheduledNoRecordsKA.setVisibility(false);
    //   }
    kony.retailBanking.globalData.accountsTransactionList = {"transaction":scheduleSegData,"unClearedTransaction":recentSegData,"unClearedTransactionTotal":total};
    var value1 = "";
    var value2 = "";
    var temp = "";
    kony.print("scheduleSegData ::"+JSON.stringify(scheduleSegData[0]));
    for (var l = 0; l < scheduleSegData[0].length; l++) {
      for (var k = l + 1; k < scheduleSegData[0].length; k++) {
        value1 = scheduleSegData[0][l].transactionDate.text.substring(3,5)+"/"+scheduleSegData[0][l].transactionDate.text.substring(0,2)+"/"+scheduleSegData[0][l].transactionDate.text.substring(6,scheduleSegData[0][l].transactionDate.text.length);
        value2 = scheduleSegData[0][k].transactionDate.text.substring(3,5)+"/"+scheduleSegData[0][k].transactionDate.text.substring(0,2)+"/"+scheduleSegData[0][k].transactionDate.text.substring(6,scheduleSegData[0][k].transactionDate.text.length);
        if ((new Date(value1) < new Date(value2))) {
          temp = scheduleSegData[0][l];
          scheduleSegData[0][l] = scheduleSegData[0][k];
          scheduleSegData[0][k] = temp;
        }
      }
    }
  }else{
    kony.retailBanking.globalData.accountsTransactionList = {"transaction":[],"unClearedTransaction":[],"unClearedTransactionTotal":0};
  }
  //   if(scheduleSegData.length > 0){
  //   	scheduleSegData.push({"lblLastTransaction":{"text":"For more details please login to online banking","isVisible":true},
  //                            "description":{"text":"","isVisible":false},
  //                            "amount":{"text":"","isVisible":false},
  //                            "transactionDate":{"text":"","isVisible":false}});
  //   }
  customerAccountDetails.indicatorIndex = 0;
  return [scheduleSegData, recentSegData];  
}
function recentTabSelectedAccounts(){
  frmAccountDetailKA.accountDetailsTransactions.animate(
    kony.ui.createAnimation({100:
                             { 
                               "left": '0%',
                               "stepConfig":{"timingFunction": easeOut}}}),
    {fillMode: forwards ,duration:0.3},
    {animationEnd: function() {} });

  frmAccountDetailKA.tabSelectedIndicator.animate(
    kony.ui.createAnimation({100:
                             { 
                               "left": '0%',
                               "stepConfig":{"timingFunction": easeOut}}}),
    {fillMode: forwards ,duration:0.3},
    {animationEnd: function() {} }); 

  frmAccountDetailKA.recentTabButton.skin = skntabSelected;
  frmAccountDetailKA.scheduledTabButton.skin = skntabDeselected;
}


function scheduledTabSelectedAccounts(){
  frmAccountDetailKA.accountDetailsTransactions.animate(
    kony.ui.createAnimation({100:
                             { 
                               "left": '-100%',
                               "stepConfig":{"timingFunction": easeOut}}}),
    {fillMode: forwards ,duration:0.3},
    {animationEnd: function() {} });

  frmAccountDetailKA.tabSelectedIndicator.animate(
    kony.ui.createAnimation({100:
                             { 
                               "left": '50%',
                               "stepConfig":{"timingFunction": easeOut}}}),
    {fillMode: forwards ,duration:0.3},
    {animationEnd: function() {} });

  frmAccountDetailKA.scheduledTabButton.skin = skntabSelected;
  frmAccountDetailKA.recentTabButton.skin = skntabDeselected;
}
//Nart Rawashdeh 29/11/2020 <<<
function sendInformaionSharingtoSSC(){
  var iban = kony.retailBanking.globalData.accountsDashboardData.accountsData[customerAccountDetails.currentIndex].iban;
  kony.print("Sent data : (" + custid + ")" + "(" + iban + ")");
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJSendIbanToSSC");
  appMFConfiguration.invokeOperation("prSendIbanToSsc", {},{"customerID": custid,
                                                            "iban": iban,
                                                            "p_channel":"MOBILE",
                                                            "p_user_id":"BOJMOB"},
                                     sendInformaionSharingtoSSCSuccess,
                                     sendInformaionSharingtoSSCFailure);
}

function sendInformaionSharingtoSSCSuccess(res){
  kony.print("Response Nart : " + res);
  kony.application.dismissLoadingScreen();
  if (res.returnCode === '00000'){
    kony.boj.populateSuccessScreen("success.png", 
                                   geti18nkey("i18n.Bene.Success"), 
                                   geti18nkey("i18n.SSC.SentSuccessfully"),
                                   geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.common.AccountDashboard"), "", "", "");
  }else{
    kony.application.dismissLoadingScreen();
    kony.boj.populateSuccessScreen("failure.png", 
                                   geti18nkey("i18n.Bene.Failed"), 
                                   geti18nkey("i18n.SSC.ErrorFromSSCMessage"),
                                   geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.common.AccountDashboard"), "", "", "");
  }
}
function sendInformaionSharingtoSSCFailure(res){
  kony.application.dismissLoadingScreen();
  customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
}
// >>>