var bic_num = "";
var b_iban = "";
function removeDatafrmTransfersformEPS(){
//   frmEPS.lblAccountName1.text = "";
//   frmEPS.lblAccountNumnextber1.text = "";
//   frmEPS.flxIcon1.setVisibility(false);
//   frmEPS.lblSelectanAccount1.setVisibility(true);
//   frmEPS.btnIBAN.skin = "slButtonBlueFocus";
//   frmEPS.btnAlias.skin = "slButtonBlueFocus";
//   frmEPS.btnMob.skin = "slButtonBlueFocus";
//   frmEPS.lblNext.skin = "sknLblNextDisabled";
 // Reset Alias Details //
  frmEPS.txtAliasName.text = "";
  frmEPS.txtAmountAlias.text = "";
  frmEPS.lblIBANAliastxt.text = "";
  frmEPS.lblAliasAddresstxt.text = "";
  frmEPS.lblAliasBank.txt = "";
 // Reset Alias Details //
}
function ResetFormIPSData(){
  frmEPS.lblAccountName1.text = "";
  frmEPS.lblAccountNumber1.text = "";
  frmEPS.flxIcon1.setVisibility(false);
  frmEPS.lblSelectanAccount1.setVisibility(true);
  frmEPS.btnAlias.skin = "sknOrangeBGRNDBOJ";
  frmEPS.btnMob.skin = "slButtonBlueFocus";
  frmEPS.btnIBAN.skin = "slButtonBlueFocus";
  frmEPS.lblNext.skin = "sknLblNextDisabled";
  frmEPS.lblAccountName1.text = "";
  // Reset Alias Details //
  frmEPS.txtAliasName.text = "";
  frmEPS.lblBeneName.text = "";
  frmEPS.txtAmountAlias.text = "";
  frmEPS.lblIBANAliastxt.text = "";
  frmEPS.lbAliasAddresstxt.text = "";
  frmEPS.lblAliasBanktxt.txt = "";
  frmEPS.flxBeneName.setVisibility(false);
  frmEPS.flxAmountAlias.setVisibility(false);
  frmEPS.flxIBANAliass.setVisibility(false);
  frmEPS.flxAliasAddress.setVisibility(false);
  frmEPS.flxAliasBank.setVisibility(false);
 // Reset Alias Details //
 // Reset Mob Details //
  frmEPS.txtAliasNameMob.text = "";
  frmEPS.txtAmountMob.text = "";
  frmEPS.lblIBANMobtxt.text = "";
  frmEPS.lblAddressMobtxt.text = "";
  frmEPS.lblBankMobAliastxt.text = "";
  frmEPS.lblBeneNameMob.text = "";
  frmEPS.flxBeneNameMob.setVisibility(false);
  frmEPS.flxAmountMob.setVisibility(false);
  frmEPS.flxIbanDetailsMob.setVisibility(false);
  frmEPS.flxAddressMob.setVisibility(false);
  frmEPS.flxBankMob.setVisibility(false);
 // Reset Mob Details //
 // Reset Iban Details //  
  frmEPS.txtAmountIBAN.text = "";
  frmEPS.txtIBANAlias.text = "";
  frmEPS.txtAddressAlias.text = "";
  frmEPS.txtIBANBeneName.text="";
  frmEPS.txtBankAlias.text = "";
  frmEPS.tbxBankName.text = "";
  frmEPS.lblBeneNameIBAN.text = "";
  frmEPS.flxUnderlineIBAN.skin = "sknFlxGreyLine";
  frmEPS.flxBeneNameIBAN.setVisibility(false);
  frmEPS.lblBankNameStat.setVisibility(false);
  frmEPS.tbxBankName.setVisibility(false);
  frmEPS.lblBankName.setVisibility(true);
 // Reset Iban Details // 
//   Default flow 
  frmEPS.flxDetailsIBAN.setVisibility(false);
  frmEPS.flxDetailsMobileScroll.setVisibility(false);
  frmEPS.flxDetailsAliasScroll.setVisibility(true); 
  frmEPS.flxConfirmEPS.setVisibility(false); 
  frmEPS.flxMain.setVisibility(true); 
}
function SetupIPSscreen(){
 var AccDetailsSegdata = frmAccountDetailsScreen.segFre.data;
 var dataSelected =  frmAccountDetailsScreen.segFre.selectedRowItems[0];
//  var accName =(dataSelected.anum.length>16)?(dataSelected.anum.substring(0,16)+"..."):dataSelected.anum;
  
  frmEPS.lblAccountName1.text = dataSelected.accountName;
  frmEPS.lblAccountNumber1.text = dataSelected.accountNumber;
  frmEPS.flxIcon1.setVisibility(true);
  frmEPS.lblSelectanAccount1.setVisibility(false);
  //Mai
  validateLblNext();
  frmEPS.show();
}
function SetupAccountsFrom(returenedGbl){
  //var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
  gblTModule = returenedGbl; 
  gotoAccountDetailsScreen(1);
  //kony.boj.accounttype(fromAccounts,gblTModule);
}
function gotoAccountDetailsScreenEPS(val){
  
  //1960 fix
  var fromAccounts="";
  var toAccounts="";
  var accountsData="";
  kony.print("accountsData"+kony.retailBanking.globalData.accountsDashboardData.accountsData);
  if(kony.boj.siri === "fromSiri")
  {
     accountsData=kony.retailBanking.globalData.accountsDashboardData.accountsData;
  }
  else
  {
     fromAccounts = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
     toAccounts	= kony.retailBanking.globalData.accountsDashboardData.toAccounts.slice(0);
  }
  kony.print("fromAccounts :: "+fromAccounts);
  kony.print("toAccounts :: "+toAccounts);

  if(val === 1){
    if(kony.newCARD.applyCardsOption === true || kony.application.getCurrentForm().id === "frmCardLinkedAccounts" || kony.boj.selectedBillerType === "PrePaid"  || kony.boj.siri ==="fromSiri"){
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.common.accountNumber");
    }else{
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyFrom");
    }
    kony.store.setItem("toval",1);
    if((kony.application.getCurrentForm().id === "frmNewBillKA" && frmNewBillKA.btnBillsPayCards.text == "t") ||
       (kony.application.getCurrentForm().id === "frmBills" && frmBills.btnBillsPayCards.text == "t") ||
       (kony.application.getCurrentForm().id === "frmNewBillDetails" && frmNewBillDetails.btnBillsPayCards.text == "t") ){
      if(kony.retailBanking.globalData.prCreditCardPayList.length > 0){

        var tempdata = [];
        var cardData = kony.retailBanking.globalData.prCreditCardPayList;
        var cc = false;
        for(var i=0;i<cardData.length;i++){
          kony.print("cardTypeFlag ::"+cardData[i].cardTypeFlag);
          if(cardData[i].cardTypeFlag === "C"){
            cc = true;
            tempdata.push(cardData[i]);
          }
        }
        if(cc){
          accountsScreenPreshow(tempdata);
          frmAccountDetailsScreen.show();
        }
        else
          customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.nocards"),popupCommonAlertDimiss,"");
      }else
        customVerb_CARDSDETAILS();
    }else{
      if(kony.application.getCurrentForm().id === "frmCardLinkedAccounts"){
        var data = frmCardLinkedAccounts.segCardLinkedAccounts.data;
        var temp = [];
        for(var i in data){
          if(data[i].flxLinkedAccountsEnable.isVisible === true){
            for(var j in fromAccounts){
              if(fromAccounts[j].accountID === data[i].lblAccountNumber.text){
                temp.push(fromAccounts[j]);
                break;
              }
            }
          }
        }
        accountsScreenPreshow(temp);
      }
      //added for 1960
       else{ 
        if(kony.boj.siri ==="fromSiri")
          {
            kony.print("loding accounts for siri");
            accountsScreenPreshow(accountsData);
          }
        else
          {
            accountsScreenPreshow(fromAccounts);
          }
       }
//       frmAccountDetailsScreen.show(); //1 july
    }
    //frmNewTransferKA.flxAcc2.setEnabled(true);

  }
  if(val==2){
    kony.store.setItem("toval",0);

    if(gblTModule!=="send")
    {
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyTo");
      var i =  kony.store.getItem("toAccount");
      for(var j in toAccounts){
        if(toAccounts[j].accountID === i.accountID)
        {
          toAccounts.splice(j,1);
          break;
        }
      }
      accountsScreenPreshow(toAccounts);      
      frmAccountDetailsScreen.show();
    }
    else{
      var data = [];
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyFrom");
      accountsScreenPreshow(fromAccounts);
      frmAccountDetailsScreen.show();
    }
    //alert(JSON.stringify("todata "+i+"index "+j+"data "+data));

  }
}

function getAllAccountsForIPS(){
  if (kony.sdk.mvvm.isNetworkAvailabile())
  {
    if (kony.sdk.mvvm.KonyApplicationContext.getAppInstance())
    {
      ShowLoadingScreen();
      var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var options ={    "access": "online",
                    "objectName": "RBObjects"
                   };
      var serviceName = "RBObjects";
      var modelObj = INSTANCE.getModel("Accounts",serviceName,options);
      var dataObject = new kony.sdk.dto.DataObject("Accounts");
      var serviceOptions = {"dataObject":dataObject,"queryParams":{"custId": custid,"lang": kony.store.getItem("langPrefObj")==="en"?"eng":"ara"}};
      modelObj.fetch(serviceOptions,refreshSucIPS,refreshFailIPS);

    }
    else{
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
    }
  }
  else{
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
  }
}
function refreshSucIPS(response){
  kony.print("refresh accounts:::"+JSON.stringify(response));
  try{
    if(isEmpty(response)){
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup(geti18Value("i18n.common.Attention"), 
                       geti18Value("i18n.alerts.NoAccountsMsg"),
                       popupCommonAlertDimiss, "");
      exceptionLogCall("::Getallaccounts Success::","service response null","service",response);
    }else{
      var segAccountListData = response;
      var fromAccounts = [],toAccounts = [],loansData = [];
      var sectionAccData = [],segDealsData = [],segLoansData = [];
      if((!isEmpty(segAccountListData)) && segAccountListData.length>0)
      {
        for(var i in segAccountListData)
        {
          if(checkAccount(segAccountListData[i]["accountID"])){
            if(!isEmpty(segAccountListData[i]["AccNickName"]))
              segAccountListData[i]["accountName"] =  segAccountListData[i]["AccNickName"];

            if(segAccountListData[i]["accountType"] != "T" && segAccountListData[i]["accountType"] != "L" && segAccountListData[i]["dr_ind"] == "Y" && segAccountListData[i]["productCode"] != "320"){

              fromAccounts.push(segAccountListData[i]);
            }
            if(segAccountListData[i]["accountType"] != "T" && segAccountListData[i]["accountType"] != "L" && segAccountListData[i]["cr_ind"] == "Y" && segAccountListData[i]["productCode"] != "320"){
              toAccounts.push(segAccountListData[i]);
            }
          }else{
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup(geti18Value("i18n.common.Attention"),
                             geti18nkey("i18n.security.loginagain"),
                             logoutpp, "");
            return;
          }
        }
        kony.print("Refreshed fromAccounts:::"+JSON.stringify(fromAccounts));
        kony.print("Refreshed toAccounts:::"+JSON.stringify(toAccounts));
        kony.retailBanking.globalData.accountsDashboardData.fromAccounts = isEmpty(fromAccounts) ? {} : fromAccounts;
        kony.retailBanking.globalData.accountsDashboardData.toAccounts = isEmpty(toAccounts) ? {} : toAccounts;

        if(kony.newCARD.applyCardsOption === true || kony.application.getCurrentForm().id === "frmCardLinkedAccounts"){
          gotoAccountDetailsScreenEPS(1);
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }else if(gblTModule === "AccNumberStatement" || gblTModule === "AccountOrder"){
          accountsScreenPreshow(fromAccounts);
          if(gblTModule === "AccountOrder"){
            frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.my_money.accounts");
          }else{
            frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.my_money.accounts");
          }
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
//           frmAccountDetailsScreen.show(); // edit by ahmad
        }else if(gblTModule == "IPS"){
          if(hasEnoughAccounts()){

            //           frmNewTransferKA.lblCurrency.setVisibility(false);
//             ownAccountPreshow();
//             removeDatafrmTransfersform();
//             makeInlineErrorsTransfersInvisible();
//             if(kony.store.getItem("langPrefObj") !== "ar"){
//               frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.001" + " " + "JOD";
//             }else{
//               frmNewTransferKA.flxInvalidAmountField.text = "JOD" + " 0.001" + " " + geti18Value("i18n.transfer.minAmount")  ;
//             }
//              frmEPS.flxAcc1.onClick = gotoAccountDetailsScreenEPS(1); // 30june
//              frmEPS.btnForward1.onClick = gotoAccountDetailsScreenEPS(1); // 30june
            
//             frmNewTransferKA.flxAcc1.setEnabled(true);
          }
          else{
            customAlertPopup(geti18Value("i18n.common.Attention"), 
                             geti18Value("i18n.Transfers.InsufficientAcc"),
                             popupCommonAlertDimiss, "");

          }
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }
        else if(gblTModule=="send"){

          var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
          var controller = INSTANCE.getFormController("frmShowAllBeneficiary");
          var navObject = new kony.sdk.mvvm.NavigationObject();
          navObject.setRequestOptions("segAllBeneficiary",{"headers":{},"queryParams":{"P_BENE_TYPE":"","custId":custid}});
          controller.loadDataAndShowForm(navObject);
        }
        else if(gblTModule == "billspay"){
          var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
          var controller = INSTANCE.getFormController("frmManagePayeeKA");
          var navObject = new kony.sdk.mvvm.NavigationObject();
          navObject.setRequestOptions("managepayeesegment",{"headers":{},"queryParams":{"custId": custid,
                                                                                        "P_BENE_TYPE": "PRE",
                                                                                        "lang":(kony.store.getItem("langPrefObj")==="en")?"eng":"ara"}});
          controller.loadDataAndShowForm(navObject);
        }



        //frmPaymentDashboard.show();

      }else{
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup(geti18Value("i18n.common.Attention"), 
                         geti18Value("i18n.alerts.NoAccountsMsg"),
                         popupCommonAlertDimiss, "");
        exceptionLogCall("::Getallaccounts Success::","service response null","service",response);
      }
    }
  }catch(e){
    exceptionLogCall("::Getallaccounts Success::","Exception while handling success callback","UI",e);
  }
}

function refreshFailIPS(err){
  kony.application.dismissLoadingScreen();
  customAlertPopup(geti18Value("i18n.Bene.Failed"),geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
  exceptionLogCall("::Getallaccounts Failed::","service failed to respond","service",err);
}

function validateLblNext(){
  kony.print("Validation for the next label is active");
  if(frmEPS.lblSelectanAccount1.isVisible === false){
    if(frmEPS.flxDetailsIBAN.isVisible === true){
      if(!isEmpty(frmEPS.txtAmountIBAN.text.trim()) && !isEmpty(frmEPS.txtIBANAlias.text.trim()) && !isEmpty(frmEPS.txtAddressAlias.text.trim()) && !isEmpty(frmEPS.txtIBANBeneName.text) && frmEPS.flxUnderlineIBAN.skin == "sknFlxGreyLine")
          frmEPS.lblNext.skin = "sknLblNextEnabled"; 
        }else if(frmEPS.flxDetailsAliasScroll.isVisible === true){
      if(frmEPS.txtAmountAlias.text !== null && frmEPS.txtAmountAlias.text.trim() !== "" && frmEPS.lblIBANAliastxt.text !== null && frmEPS.lblIBANAliastxt.text.trim() !== "" && frmEPS.lbAliasAddresstxt.text !== null && frmEPS.lbAliasAddresstxt.text.trim() !== "" && frmEPS.lblAliasBanktxt.text !== null && frmEPS.lblAliasBanktxt.text.trim() !== "" && frmEPS.lblBeneName.text !== null && frmEPS.lblBeneName.text.trim() !== "" )
        frmEPS.lblNext.skin = "sknLblNextEnabled";
         }else if(frmEPS.flxDetailsMobileScroll.isVisible === true){
      if(frmEPS.txtAmountMob.text !== null && frmEPS.txtAmountMob.text.trim() !== "" && frmEPS.lblIBANMobtxt.text !== null && frmEPS.lblIBANMobtxt.text.trim() !== "" && frmEPS.lblAddressMobtxt.text !== null && frmEPS.lblAddressMobtxt.text.trim() !== "" && frmEPS.lblBeneNameMob.text !== null && frmEPS.lblBeneNameMob.text.trim() !== "")
        frmEPS.lblNext.skin = "sknLblNextEnabled";   
         }
  }else{
    frmEPS.lblNext.skin = "sknLblNextDisabled";
  }
  
}

function onClickYesBackIPS(){
  frmPaymentDashboard.show();
  popupCommonAlert.dismiss();
  frmEPS.destroy();
}

function SetupConfirmationScreen(){
  
  var gblFromAcc = "";
 
  if(frmEPS.btnAlias.skin == "sknOrangeBGRNDBOJ" && frmEPS.btnMob.skin == "slButtonBlueFocus" && frmEPS.btnIBAN.skin == "slButtonBlueFocus" ){
   // if Alias Type is Alias //
    frmEPS.flxOtherDetailScroll.setVisibility(true);
    frmEPS.lblFromAccConfText.text = frmEPS.lblAccountName1.text;
    frmEPS.ToAccConfirmationText.text = frmEPS.lblBeneName.text;
    frmEPS.lblAmountConfirmValue.text = frmEPS.txtAmountAlias.text;
    frmEPS.lblIBANConftext.text = frmEPS.lblIBANAliastxt.text;
    frmEPS.lblConfirmAddr.text = frmEPS.lbAliasAddresstxt.text;
    frmEPS.lblConfirmBankName.text = frmEPS.lblAliasBanktxt.text;
    
  }else if(frmEPS.flxDetailsIBAN.isVisibile === true){
    gblFromAcc = frmEPS.btnIBAN.text;
  }else if (frmEPS.btnAlias.skin == "slButtonBlueFocus" && frmEPS.btnMob.skin == "sknOrangeBGRNDBOJ" && frmEPS.btnIBAN.skin == "slButtonBlueFocus"){
   // if Alias Type is Mobile //
    frmEPS.lblFromAccConfText.text = frmEPS.lblAccountName1.text;
    frmEPS.ToAccConfirmationText.text = frmEPS.lblBeneNameMob.text;
    frmEPS.lblAmountConfirmValue.text = frmEPS.txtAmountMob.text;
    frmEPS.lblIBANConftext.text = frmEPS.lblIBANMobtxt.text;
    frmEPS.lblConfirmAddr.text = frmEPS.lblAddressMobtxt.text;
    frmEPS.lblConfirmBankName.text = frmEPS.lblBankMobAliastxt.text;

//      frmEPS.flxConfirmBankName.setVisibility(false);
//      frmEPS.lblIBANConfTitle.text = "Alias Name";
//      frmEPS.lblIBANConftext.text = frmEPS.txtAliasName.text;
//      frmEPS.lblConfirmAddTitle.text = frmEPS.lblAmountAlias.text;
//      frmEPS.lblConfirmAddr.text = frmEPS.txtAmount.text;
   }else if (frmEPS.btnAlias.skin == "slButtonBlueFocus" && frmEPS.btnMob.skin == "slButtonBlueFocus" && frmEPS.btnIBAN.skin == "sknOrangeBGRNDBOJ"){
    
    frmEPS.lblFromAccConfText.text = frmEPS.lblAccountName1.text;
    frmEPS.ToAccConfirmationText.text = frmEPS.txtIBANBeneName.text;
    frmEPS.lblAmountConfirmValue.text = frmEPS.txtAmountIBAN.text;
    frmEPS.lblIBANConftext.text = frmEPS.txtIBANAlias.text;
    frmEPS.lblConfirmAddr.text = frmEPS.txtAddressAlias.text;
    frmEPS.lblConfirmBankName.text = frmEPS.tbxBankName.text;
    
  }
  
  
}
function GetAliasInfo(AliType){
  
  var aliasType="";
  
  if (frmEPS.btnAlias.skin==="sknOrangeBGRNDBOJ")
    {
      aliasType="ALIAS";
    }
  else if(frmEPS.btnMob.skin==="sknOrangeBGRNDBOJ")
  {
    aliasType="MOBL";
  }
        
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJAliasIPS");
  appMFConfiguration.invokeOperation("prGetIpsAliasDetails", {},{"p_customer_id": custid,
                                                                 "p_alias":AliType,
                                                                 "p_alias_type": aliasType,
                                                                 "p_channel":"MOBILE",
                                                                 "p_user_id":"BOJMOB"},
                                     successCalling,
                                     function(error){});
}
function successCalling(response){
//   alert(response);
  
  if (isEmpty(response.beniban) && isEmpty(response.benaddress) && 
      isEmpty(response.benbic) && isEmpty(response.benname))
   {
      customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.CLIQ.AliasError"), popupCommonAlertDimiss, "");
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
   }else
   {
      b_iban = response.beniban;
      bic_num = response.benbic;
      if(frmEPS.btnAlias.skin == sknOrangeBGRNDBOJ){
        frmEPS.flxAmountAlias.setVisibility(true);
        frmEPS.flxBeneName.setVisibility(true);
        //frmEPS.flxIBANAliass.setVisibility(true);
        //frmEPS.flxAliasAddress.setVisibility(true);
        frmEPS.flxAliasBank.setVisibility(true);
        frmEPS.lblIBANAliastxt.text = response.beniban;
        frmEPS.lbAliasAddresstxt.text = response.benaddress;
        frmEPS.lblAliasBanktxt.text = response.benbic;
        frmEPS.lblBeneName.text = response.benname;
      } 
      else if(frmEPS.btnMob.skin == sknOrangeBGRNDBOJ){
        frmEPS.flxAmountMob.setVisibility(true);
        frmEPS.flxBeneNameMob.setVisibility(true);
        //frmEPS.flxIbanDetailsMob.setVisibility(true);
       // frmEPS.flxAddressMob.setVisibility(true);
        frmEPS.flxBankMob.setVisibility(true);  
        frmEPS.lblIBANMobtxt.text = response.beniban;
        frmEPS.lblAddressMobtxt.text = response.benaddress; 
        frmEPS.lblBankMobAliastxt.text = response.benbic;
        frmEPS.lblBeneNameMob.text = response.benname;
      }
      else if(frmEPS.btnIBAN.skin == sknOrangeBGRNDBOJ){
        frmEPS.lblIBANtxt.text = response.beniban;
        frmEPS.lblAddressIBANtext.text = response.benaddress; 
        frmEPS.lbIBankIBANtext.text = response.benname;
      }
   }
//   frmEPS.lblIBANConftext.text = response.beniban;
//   frmEPS.lblConfirmBankName.text = response.benname;
//   frmEPS.lblConfirmAddr.text = response.benaddress;
//   frmEPS.txtAmount.text = response.benbic;
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  
}
function  setupBanknameIPS(data){
  gblWorkflowType = null;
  
  kony.print("data banks ::"+JSON.stringify(data));
  
  frmEPS.tbxBankName.text = data.BankName;
  frmEPS.lblSwiftCode.text=data.SwiftCode;
  frmEPS.lblBankNameStat.setVisibility(true);
  frmEPS.tbxBankName.setVisibility(true);
  frmEPS.lblBankName.setVisibility(false);
  frmEPS.show();
  frmSelectDetailsBene.destroy();
  
}
function returnBankBranchs(detailType, lblName, countryCode){
  kony.boj.detailsForBene.lblToUpdate = lblName;
  kony.boj.detailsForBene.flag = true;
  kony.boj.selectedDetailsType = detailType;
  frmSelectBankName.segDetails.setData([]);

  var ctrydesc = "CTRY_S_DESC"; 
  if(kony.boj.lang === "eng")
    ctrydesc = "CTRY_S_DESC";
  else
    ctrydesc = "CTRY_B_DESC";

  if(detailType === "Country"){
    frmSelectBankName.segDetails.widgetDataMap = { 
      lblData: ctrydesc,
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    kony.boj.selectedDetailsList = sort_ALPHABETICALLY(kony.boj.detailsForBene.Country, ctrydesc);
    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i][ctrydesc] !== undefined && kony.boj.selectedDetailsList[i][ctrydesc] !== ""){
      	kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i][ctrydesc].substring(0, 2).toUpperCase();
      }else{
      	kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CTRY_S_DESC.substring(0, 2).toUpperCase();
      }
    }
  }
  else if(detailType === "City"){
    if(kony.boj.lang === "eng")
      ctrydesc = "CITY_S_DESC";
  	else
      ctrydesc = "CITY_B_DESC";
    frmSelectBankName.segDetails.widgetDataMap = { 
      lblData: ctrydesc,
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    kony.boj.selectedDetailsList = sort_ALPHABETICALLY(kony.boj.detailsForBene[detailType][countryCode], ctrydesc);
    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i][ctrydesc] !== undefined && kony.boj.selectedDetailsList[i][ctrydesc] !== ""){
      	kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i][ctrydesc].substring(0, 2).toUpperCase();
      }else{
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CITY_B_DESC.substring(0, 2).toUpperCase();
      }
    }
  }
  else if(detailType === "Branch"){
    frmSelectBankName.segDetails.widgetDataMap = { 
      lblData: "BRANCH_NAME",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };
    if(gblLaunchModeOBJ.lauchMode  && (!isEmpty(gblLaunchModeOBJ.accno) || !isEmpty(gblLaunchModeOBJ.iban)) && frmAddExternalAccountKA.lblBankBranch.text != kony.i18n.getLocalizedString("i18n.Bene.BankBranch")){
      kony.print("Don nothing:");
      }
    else{
    kony.boj.getBranchList();
     
    }
    return;
    //kony.boj.selectedDetailsList = kony.boj.detailsForBene.Branch;
  }
  else if(detailType === "BankDetail"){
    frmSelectBankName.segDetails.widgetDataMap = { 
      lblData: "BankName",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };
    
    var bankList = [];
    if(kony.application.getCurrentForm().id === "frmAddExternalAccountKA" && kony.boj.addBeneVar.selectedType === "DOM"){
  		for(var p in kony.boj.detailsForBene.BankDetail){
        	if(kony.boj.detailsForBene.BankDetail[p].SwiftCode !== "BJORJOAX")
              bankList.push(kony.boj.detailsForBene.BankDetail[p]);
        }
	}else{
    	bankList = kony.boj.detailsForBene.BankDetail;
    }
    
    kony.boj.selectedDetailsList = sort_ALPHABETICALLY(bankList, "BankName");
  }
  /*hassan OpenDeposite*/
  else if(detailType === "MaturityInstruction"){
    
    
    
    var matTypes=[{"Maturity":geti18Value("i18n.termDeposit.renWithInterest")},{"Maturity":geti18Value("i18n.termDeposit.renWithOutInterest")},{"Maturity":geti18Value("i18n.termDeposit.close")}];
    var matArray=[];
    
    frmSelectBankName.segDetails.widgetDataMap = { 
      lblData: "Maturity",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    kony.boj.selectedDetailsList=matTypes;
    
    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i].Maturity !== undefined && kony.boj.selectedDetailsList[i].Maturity !== ""){
      	kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].Maturity.substring(0, 2).toUpperCase();
      }else{
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CITY_B_DESC.substring(0, 2).toUpperCase();
      }
    }
  }
 
  /*hassan OpenDeposite*/
 else if (detailType === "IPS"){
    
   
 }
  for(var i in kony.boj.selectedDetailsList){
    if(kony.boj.selectedDetailsList[i].icon === undefined){
      kony.boj.selectedDetailsList[i].icon = {
        backgroundColor: kony.boj.getBackGroundColour(i)
      };
    }
    else
      break;
  }
  
  frmSelectBankName.segDetails.setData(kony.boj.selectedDetailsList);
  frmSelectBankName.show();
}
function TransferIPS(){
  var src_acc = frmEPS.lblAccountNumber1.text;
  var alias_name = "";
  var alias_addr = "";
  var alias_amount = "";
  if(frmEPS.btnAlias.skin == sknOrangeBGRNDBOJ){b_iban = frmEPS.lblIBANAliastxt.text;
                                                alias_name = frmEPS.lblBeneName.text;
                                                alias_addr = frmEPS.lbAliasAddresstxt.text; 
                                                alias_amount = frmEPS.txtAmountAlias.text;
                                               }
  if(frmEPS.btnMob.skin == sknOrangeBGRNDBOJ){b_iban = frmEPS.lblIBANMobtxt.text;
                                              alias_name = frmEPS.lblBeneNameMob.text;
                                              alias_addr = frmEPS.lblAddressMobtxt.text;
                                              alias_amount = frmEPS.txtAmountMob.text; }
  if(frmEPS.btnIBAN.skin == sknOrangeBGRNDBOJ){b_iban = frmEPS.txtIBANAlias.text;
                                               alias_name = frmEPS.txtIBANBeneName.text;
                                               alias_addr = frmEPS.txtAddressAlias.text;
                                               alias_amount = frmEPS.txtAmountIBAN.text; 
                                                bic_num=frmEPS.lblSwiftCode.text;
                                              }
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var request_params ={
    "cust_id":custid,
    "src_acc":src_acc,
    "src_br":kony.store.getItem("frmAccount").branchNumber,
    "b_iban":b_iban,
    "b_name":alias_name,
    "b_address":alias_addr,
    "b_bic":bic_num,
    "purpose":"11110",
    "trans_amt": alias_amount,
    "trans_amt_ccy":"JOD",
    "b_channel":"MOBILE",
    "user_id":"BOJMOB"
  };
  kony.print("request_params"+JSON.stringify(request_params));
        var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJIPS");
      	appMFConfiguration.invokeOperation("prIpsTransfer", {},request_params,
                                           successPass,
                                           function(error){kony.print("error"+ JSON.stringify(error));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
}
function successPass(response){
  kony.print("successPass"+ JSON.stringify(response));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  if (response.errorCode==="00000")
    {
       kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"),
       geti18Value("i18.CLIQ.SuccessMSG"),
       geti18Value("i18n.Bene.GotoAccDash"), 
       geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
       "CliqDashboardManage", "", "", " " + " " + "");
       ResetFormIPSData(); // Ahmad 12-7-2020
    }
  else
    {
        var Message = getErrorMessage(response.errorCode);
        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"),
        Message,
        geti18Value("i18n.Bene.GotoAccDash"), 
        geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
        "CliqDashboard", "", "", " " + " " + "");
    }
}
