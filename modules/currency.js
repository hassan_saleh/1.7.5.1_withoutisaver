


gblCurrList = [
  {
    "CURR_CODE":"1",
    "ARABIC_DESC":"دينار اردني",
    "ENGLISH_DESC":"Jordanian Dinar",
    "CURR_ISO":"JOD",
    "DECIMAL_PLACES":"3",
    "IMG_FLAG":"jordanflag.png"
  },{
    "CURR_CODE":"2",
    "ARABIC_DESC":"دولار أمريكي",
    "ENGLISH_DESC":"U.S.Dollars",
    "CURR_ISO":"USD",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"unitedstatesflag.png"
  },

  {
    "CURR_CODE":"3",
    "ARABIC_DESC":"جنيه اسـترليني",
    "ENGLISH_DESC":"Sterling Pound",
    "CURR_ISO":"GBP",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"unitedkingdomflag.png"
  },{
    "CURR_CODE":"5",
    "ARABIC_DESC":"فرنك سويسري",
    "ENGLISH_DESC":"Swiss Franc",
    "CURR_ISO":"CHF",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"switzerlandflag.png"
  },{
    "CURR_CODE":"8",
    "ARABIC_DESC":"ين ياباني",
    "ENGLISH_DESC":"Japanese Yen",
    "CURR_ISO":"JPY",
    "DECIMAL_PLACES":"0",
    "IMG_FLAG":"japanflag.png"
  },{
    "CURR_CODE":"11",
    "ARABIC_DESC":"كرونا دانماركي",
    "ENGLISH_DESC":"Danish Kroner",
    "CURR_ISO":"DKK",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"denmark.png"
  },{
    "CURR_CODE":"12",
    "ARABIC_DESC":"كرونا سويدية",
    "ENGLISH_DESC":"Swedish Krona",
    "CURR_ISO":"SEK",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"swedenflag.png"
  },{
    "CURR_CODE":"13",
    "ARABIC_DESC":"دولار كندي",
    "ENGLISH_DESC":"Canadian Dollar",
    "CURR_ISO":"CAD",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"canadaflag.png"
  },
  {
    "CURR_CODE":"18",
    "ARABIC_DESC":"دولار استرالي",
    "ENGLISH_DESC":"Australian Dollar",
    "CURR_ISO":"AUD",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"australiaflag.png"
  },{
    "CURR_CODE":"21",
    "ARABIC_DESC":"ريال سعودي",
    "ENGLISH_DESC":"Saudi Rial",
    "CURR_ISO":"SAR",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"saudiarabiaflag.png"
  }, {
    "CURR_CODE":"22",
    "ARABIC_DESC":"دينار كويتي",
    "ENGLISH_DESC":"Kuwaiti Dinar",
    "CURR_ISO":"KWD",
    "DECIMAL_PLACES":"3",
    "IMG_FLAG":"kuwaitflag.png"
  },{
    "CURR_CODE":"29",
    "ARABIC_DESC":"درهم اماراتي",
    "ENGLISH_DESC":"UAE Dirham",
    "CURR_ISO":"AED",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"unitedarabemirates.png"
  }, {
    "CURR_CODE":"30",
    "ARABIC_DESC":"ريال قطري",
    "ENGLISH_DESC":"Qatari Riryal",
    "CURR_ISO":"QAR",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"qatarflag.png"
  },{
    "CURR_CODE":"34",
    "ARABIC_DESC":"يورو",
    "ENGLISH_DESC":"Euro Currency Unit",
    "CURR_ISO":"EUR",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"europeanunionflag.png"
  }

];

function setgblcurrencyfullCountryListt(){
  gblCurrList = [
  {
    "CURR_CODE":"1",
    "ARABIC_DESC":"دينار اردني",
    "ENGLISH_DESC":"Jordanian Dinar",
    "CURR_ISO":"JOD",
    "DECIMAL_PLACES":"3",
    "IMG_FLAG":"jordanflag.png"
  },

  {
    "CURR_CODE":"2",
    "ARABIC_DESC":"دولار أمريكي",
    "ENGLISH_DESC":"U.S.Dollars",
    "CURR_ISO":"USD",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"unitedstatesflag.png"
  },

  {
    "CURR_CODE":"3",
    "ARABIC_DESC":"جنيه اسـترليني",
    "ENGLISH_DESC":"Sterling Pound",
    "CURR_ISO":"GBP",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"unitedkingdomflag.png"
  },

  {
    "CURR_CODE":"4",
    "ARABIC_DESC":"مارك ألماني",
    "ENGLISH_DESC":"Deutche Mark",
    "CURR_ISO":"DEM",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"germanyflag.png"
  },

  {
    "CURR_CODE":"5",
    "ARABIC_DESC":"فرنك سويسري",
    "ENGLISH_DESC":"Swiss Franc",
    "CURR_ISO":"CHF",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"switzerlandflag.png"
  },

  {
    "CURR_CODE":"6",
    "ARABIC_DESC":"فرنك فرنسي",
    "ENGLISH_DESC":"French Fanc",
    "CURR_ISO":"FRF",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"franceflag.png"
  },

  {
    "CURR_CODE":"7",
    "ARABIC_DESC":"لير ايطالي",
    "ENGLISH_DESC":"Italian Lira",
    "CURR_ISO":"ITL",
    "DECIMAL_PLACES":"0",
    "IMG_FLAG":"italyflag.png"
  },

  {
    "CURR_CODE":"8",
    "ARABIC_DESC":"ين ياباني",
    "ENGLISH_DESC":"Japanese Yen",
    "CURR_ISO":"JPY",
    "DECIMAL_PLACES":"0",
    "IMG_FLAG":"japanflag.png"
  },

  {
    "CURR_CODE":"9",
    "ARABIC_DESC":"جلدر هولندي",
    "ENGLISH_DESC":"Netherlands Guilder",
    "CURR_ISO":"NLG",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"netherlandsflag.png"
  },

  {
    "CURR_CODE":"10",
    "ARABIC_DESC":"فرنك بلجيكي",
    "ENGLISH_DESC":"Common Belgian Franc",
    "CURR_ISO":"BEF",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"belgiumflag.png"
  },

  {
    "CURR_CODE":"12",
    "ARABIC_DESC":"كرونا سويدية",
    "ENGLISH_DESC":"Swedish Krona",
    "CURR_ISO":"SEK",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"swedenflag.png"
  },

  {
    "CURR_CODE":"11",
    "ARABIC_DESC":"كرونا دانماركي",
    "ENGLISH_DESC":"Danish Kroner",
    "CURR_ISO":"DKK",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"denmark.png"
  },

  {
    "CURR_CODE":"14",
    "ARABIC_DESC":"شلن نمســاوي",
    "ENGLISH_DESC":"Austria Schilling",
    "CURR_ISO":"ATS",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"austriaflag.png"
  },

  {
    "CURR_CODE":"13",
    "ARABIC_DESC":"دولار كندي",
    "ENGLISH_DESC":"Canadian Dollar",
    "CURR_ISO":"CAD",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"canadaflag.png"
  },

  {
    "CURR_CODE":"15",
    "ARABIC_DESC":"بيزيتا اسبانية",
    "ENGLISH_DESC":"Spanish Peseta",
    "CURR_ISO":"ESP",
    "DECIMAL_PLACES":"0",
    "IMG_FLAG":"spainflag.png"
  },

  {
    "CURR_CODE":"21",
    "ARABIC_DESC":"ريال سعودي",
    "ENGLISH_DESC":"Saudi Rial",
    "CURR_ISO":"SAR",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"saudiarabiaflag.png"
  },

  {
    "CURR_CODE":"34",
    "ARABIC_DESC":"يورو",
    "ENGLISH_DESC":"Euro Currency Unit",
    "CURR_ISO":"EUR",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"europeanunionflag.png"
  },

  {
    "CURR_CODE":"19",
    "ARABIC_DESC":"جنية قبرصي",
    "ENGLISH_DESC":"Cyprus Pound",
    "CURR_ISO":"CYP",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"cyprusflag.png"
  },

  {
    "CURR_CODE":"20",
    "ARABIC_DESC":"دراخما يوناني",
    "ENGLISH_DESC":"Greek Drachma",
    "CURR_ISO":"GRD",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"greeceflag.png"
  },

  {
    "CURR_CODE":"18",
    "ARABIC_DESC":"دولار استرالي",
    "ENGLISH_DESC":"Australian Dollar",
    "CURR_ISO":"AUD",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"australiaflag.png"
  },

  {
    "CURR_CODE":"22",
    "ARABIC_DESC":"دينار كويتي",
    "ENGLISH_DESC":"Kuwaiti Dinar",
    "CURR_ISO":"KWD",
    "DECIMAL_PLACES":"3",
    "IMG_FLAG":"kuwaitflag.png"
  },

  {
    "CURR_CODE":"29",
    "ARABIC_DESC":"درهم اماراتي",
    "ENGLISH_DESC":"UAE Dirham",
    "CURR_ISO":"AED",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"unitedarabemirates.png"
  },

  {
    "CURR_CODE":"28",
    "ARABIC_DESC":"دينار بحريني",
    "ENGLISH_DESC":"Bahraini Dinar",
    "CURR_ISO":"BHD",
    "DECIMAL_PLACES":"3",
    "IMG_FLAG":"bahrainflag.png"
  },

  {
    "CURR_CODE":"30",
    "ARABIC_DESC":"ريال قطري",
    "ENGLISH_DESC":"Qatari Riryal",
    "CURR_ISO":"QAR",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"qatarflag.png"
  },

  {
    "CURR_CODE":"24",
    "ARABIC_DESC":"ريال عماني",
    "ENGLISH_DESC":"OMANI RIAL",
    "CURR_ISO":"OMR",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"omanflag.png"
  },

  {
    "CURR_CODE":"25",
    "ARABIC_DESC":"ليرة سوريه",
    "ENGLISH_DESC":"Syrian Pound",
    "CURR_ISO":"SYP",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"syriaflag.png"
  },

  {
    "CURR_CODE":"27",
    "ARABIC_DESC":"جنية مصري",
    "ENGLISH_DESC":"Egyption Pound",
    "CURR_ISO":"EGP",
    "DECIMAL_PLACES":"3",
    "IMG_FLAG":"egyptflag.png"
  },

  {
    "CURR_CODE":"35",
    "ARABIC_DESC":"شيكل اسرائيلي",
    "ENGLISH_DESC":"Israeli Shekel",
    "CURR_ISO":"ILS",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"israelflag.png"
  },

  {
    "CURR_CODE":"23",
    "ARABIC_DESC":"دينار عراقي",
    "ENGLISH_DESC":"Iraqi Dinar",
    "CURR_ISO":"IQD",
    "DECIMAL_PLACES":"3",
    "IMG_FLAG":"iraqflag.png"
  },

  {
    "CURR_CODE":"31",
    "ARABIC_DESC":"ليرة لبنانية",
    "ENGLISH_DESC":"Lebenese Pound",
    "CURR_ISO":"LBP",
    "DECIMAL_PLACES":"3",
    "IMG_FLAG":"lebanonflag.png"
  },

  {
    "CURR_CODE":"32",
    "ARABIC_DESC":"دولار سنغافوري",
    "ENGLISH_DESC":"Singapore Dollar",
    "CURR_ISO":"SGD",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"singaporeflag.png"
  },

  {
    "CURR_CODE":"33",
    "ARABIC_DESC":"ريال يمني",
    "ENGLISH_DESC":"Yemeni Rial",
    "CURR_ISO":"YER",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"yemenflag.png"
  },

  {
    "CURR_CODE":"17",
    "ARABIC_DESC":"ليرة تركي",
    "ENGLISH_DESC":"Turkish Lira",
    "CURR_ISO":"TRL",
    "DECIMAL_PLACES":"0",
    "IMG_FLAG":"turkeyflag.png"
  },

  {
    "CURR_CODE":"16",
    "ARABIC_DESC":"كرونا نرويجي",
    "ENGLISH_DESC":"Norwegian Krone",
    "CURR_ISO":"NOK",
    "DECIMAL_PLACES":"0",
    "IMG_FLAG":"norwayflag.png"
  },

  {
    "CURR_CODE":"26",
    "ARABIC_DESC":"جنية سوداني",
    "ENGLISH_DESC":"Sudanese Pound",
    "CURR_ISO":"SDG",
    "DECIMAL_PLACES":"0",
    "IMG_FLAG":"sudanflag.png"
  },

  {
    "CURR_CODE":"36",
    "ARABIC_DESC":"دولار نيوزلاندي",
    "ENGLISH_DESC":"NEWZELAND DOLAR",
    "CURR_ISO":"NZD",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"newzealand.png"
  },

  {
    "CURR_CODE":"37",
    "ARABIC_DESC":"رينجيتا ماليزي",
    "ENGLISH_DESC":"Malaysian Ringgit",
    "CURR_ISO":"MYR",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"malaysia.png"
  },

  {
    "CURR_CODE":"38",
    "ARABIC_DESC":"درهم مغربي",
    "ENGLISH_DESC":"Moroccan Dirham",
    "CURR_ISO":"MAD",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"moroccoflag.png"
  },

  {
    "CURR_CODE":"39",
    "ARABIC_DESC":"روبيه اندونيسية",
    "ENGLISH_DESC":"Indonesian rupiah",
    "CURR_ISO":"IDR",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"indonesiaflag.png"
  },

  {
    "CURR_CODE":"40",
    "ARABIC_DESC":"دينار تونسي",
    "ENGLISH_DESC":"Tunisian Dinar",
    "CURR_ISO":"TND",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"tunisiaflag.png"
  },

  {
    "CURR_CODE":"41",
    "ARABIC_DESC":"بات تايلندي",
    "ENGLISH_DESC":"Thai Baht",
    "CURR_ISO":"THB",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"thailandflag.png"
  },

  {
    "CURR_CODE":"42",
    "ARABIC_DESC":"دينار ليبي",
    "ENGLISH_DESC":"Lybian Dinar",
    "CURR_ISO":"LYD",
    "DECIMAL_PLACES":"3",
    "IMG_FLAG":"libyaflag.png"
  },
  {
    "CURR_CODE":"43",
    "ARABIC_DESC":"الاسكوادو البرتغالى",
    "ENGLISH_DESC":"PORTUGUESE  ESCUDO",
    "CURR_ISO":"PTE",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"portugalflag.png"
  },

  {
    "CURR_CODE":"45",
    "ARABIC_DESC":"ليرة مالطية",
    "ENGLISH_DESC":"MATLESE LIRA",
    "CURR_ISO":"MTL",
    "DECIMAL_PLACES":"2",
    "IMG_FLAG":"maltaflag.png"
  },

  {
    "CURR_CODE":"46",
    "ARABIC_DESC":"اليوان الصيني",
    "ENGLISH_DESC":"Chinese Yuan Renminbi",
    "CURR_ISO":"CNY",
    "DECIMAL_PLACES":"0",
    "IMG_FLAG":"chinaflag.png"
  },

  {
    "CURR_CODE":"47",
    "ARABIC_DESC":"الروبل الروسي",
    "ENGLISH_DESC":"Russian Ruble",
    "CURR_ISO":"RUB",
    "DECIMAL_PLACES":"0",
    "IMG_FLAG":"russiaflag.png"
  }

];
}

function get_NATIONAL_INTERNATIONAL_CURRENCYLIST(){
	try{
      gblCurrList = [
        {
          "CURR_CODE":"1",
          "ARABIC_DESC":"دينار اردني",
          "ENGLISH_DESC":"Jordanian Dinar",
          "CURR_ISO":"JOD",
          "DECIMAL_PLACES":"3",
          "IMG_FLAG":"jordanflag.png"
        },{
          "CURR_CODE":"2",
          "ARABIC_DESC":"دولار أمريكي",
          "ENGLISH_DESC":"U.S.Dollars",
          "CURR_ISO":"USD",
          "DECIMAL_PLACES":"2",
          "IMG_FLAG":"unitedstatesflag.png"
        },

        {
          "CURR_CODE":"3",
          "ARABIC_DESC":"جنيه اسـترليني",
          "ENGLISH_DESC":"Sterling Pound",
          "CURR_ISO":"GBP",
          "DECIMAL_PLACES":"2",
          "IMG_FLAG":"unitedkingdomflag.png"
        },{
          "CURR_CODE":"5",
          "ARABIC_DESC":"فرنك سويسري",
          "ENGLISH_DESC":"Swiss Franc",
          "CURR_ISO":"CHF",
          "DECIMAL_PLACES":"2",
          "IMG_FLAG":"switzerlandflag.png"
        },{
          "CURR_CODE":"8",
          "ARABIC_DESC":"ين ياباني",
          "ENGLISH_DESC":"Japanese Yen",
          "CURR_ISO":"JPY",
          "DECIMAL_PLACES":"0",
          "IMG_FLAG":"japanflag.png"
        },{
          "CURR_CODE":"11",
          "ARABIC_DESC":"كرونا دانماركي",
          "ENGLISH_DESC":"Danish Kroner",
          "CURR_ISO":"DKK",
          "DECIMAL_PLACES":"2",
          "IMG_FLAG":"denmark.png"
        },{
          "CURR_CODE":"12",
          "ARABIC_DESC":"كرونا سويدية",
          "ENGLISH_DESC":"Swedish Krona",
          "CURR_ISO":"SEK",
          "DECIMAL_PLACES":"2",
          "IMG_FLAG":"swedenflag.png"
        },{
          "CURR_CODE":"13",
          "ARABIC_DESC":"دولار كندي",
          "ENGLISH_DESC":"Canadian Dollar",
          "CURR_ISO":"CAD",
          "DECIMAL_PLACES":"2",
          "IMG_FLAG":"canadaflag.png"
        },
        {
          "CURR_CODE":"18",
          "ARABIC_DESC":"دولار استرالي",
          "ENGLISH_DESC":"Australian Dollar",
          "CURR_ISO":"AUD",
          "DECIMAL_PLACES":"2",
          "IMG_FLAG":"australiaflag.png"
        },{
          "CURR_CODE":"21",
          "ARABIC_DESC":"ريال سعودي",
          "ENGLISH_DESC":"Saudi Rial",
          "CURR_ISO":"SAR",
          "DECIMAL_PLACES":"2",
          "IMG_FLAG":"saudiarabiaflag.png"
        }, {
          "CURR_CODE":"22",
          "ARABIC_DESC":"دينار كويتي",
          "ENGLISH_DESC":"Kuwaiti Dinar",
          "CURR_ISO":"KWD",
          "DECIMAL_PLACES":"3",
          "IMG_FLAG":"kuwaitflag.png"
        },{
          "CURR_CODE":"29",
          "ARABIC_DESC":"درهم اماراتي",
          "ENGLISH_DESC":"UAE Dirham",
          "CURR_ISO":"AED",
          "DECIMAL_PLACES":"2",
          "IMG_FLAG":"unitedarabemirates.png"
        }, {
          "CURR_CODE":"30",
          "ARABIC_DESC":"ريال قطري",
          "ENGLISH_DESC":"Qatari Riryal",
          "CURR_ISO":"QAR",
          "DECIMAL_PLACES":"2",
          "IMG_FLAG":"qatarflag.png"
        },{
          "CURR_CODE":"34",
          "ARABIC_DESC":"يورو",
          "ENGLISH_DESC":"Euro Currency Unit",
          "CURR_ISO":"EUR",
          "DECIMAL_PLACES":"2",
          "IMG_FLAG":"europeanunionflag.png"
        }
      ];
    }catch(e){
    	kony.print("Exception_get_NATIONAL_INTERNATIONAL_CURRENCYLIST ::"+e);
    }
}
function preShowCurrency(){
  frmCurrency.lblTitle.text = geti18Value("i18.Transfer.chooseCurrency");
  frmCurrency.flxHeader.height = "14%";
  frmCurrency.flxHeadMain1.height = "50%";
  frmCurrency.flxSearch.setVisibility(true);
  if(gblCalculateFXLeft || gblCalculateFXRight || gblExchangeRatesFlag || gblTModule === "own"){
   setgblcurrencyfullCountryListt();
  }else if(gblTModule === "send" && gblSelectedBene.benType === "IFB"){
    setgblcurrencyfullCountryListt();
  }else{
  	get_NATIONAL_INTERNATIONAL_CURRENCYLIST();
  }
  if(gblTModule === "countryCode"){
    frmCurrency.lblTitle.text = geti18Value("i18n.common.countrycode");
  	set_COUNTRYCODE_TO_SEGMENT();
  }else if(gblTModule === "loanPostpone"){
    frmCurrency.lblTitle.text = geti18Value("i18n.loanpostpone.noofinspos");
    frmCurrency.flxHeader.height = "9%";
    frmCurrency.flxHeadMain1.height = "90%";
    frmCurrency.flxSearch.setVisibility(false);
  	assign_MNTH_LIST();
  }else
  	setDataInSegment();
}

function setDataInSegment(){

  var langSelected = kony.store.getItem("langPrefObj");
  var Language = langSelected.toUpperCase();
  var masterData =[];
  var i;
  if(Language == "EN"){
    frmCurrency.segCurrency.widgetDataMap = {
      lblDecimal:"DECIMAL_PLACES",
      lblCurrCode:"CURR_CODE",
      lblDescriptionAr:"",
      lblDescriptionEn:"ENGLISH_DESC",
      lblCurrency:"CURR_ISO",
      imgTickIcon:"imgTickIcon",
      imgCountryIcon:"IMG_FLAG"

    };
  }
  else{
    frmCurrency.segCurrency.widgetDataMap = {
      lblDecimal:"DECIMAL_PLACES",
      lblCurrCode:"CURR_CODE",
      lblDescriptionAr:"ARABIC_DESC",
      lblDescriptionEn:"",
      lblCurrency:"CURR_ISO",
      imgTickIcon:"imgTickIcon",
      imgCountryIcon:"IMG_FLAG"    
    };
  }

  //setSectionAt method call,modifying the data at 2nd sectionIndex position.
  var tempArr=[];
  kony.print("gblDefCurr ::"+frmNewTransferKA.lblFromAccCurr.text+" :: lblToAccCurr ::"+frmNewTransferKA.lblToAccCurr.text);
  kony.print("gblCalculateFXLeft::"+gblCalculateFXLeft+"gblCalculateFXRight::"+gblCalculateFXRight+"gblExchangeRatesFlag::"+gblExchangeRatesFlag)
  
   if(gblCalculateFXLeft || gblCalculateFXRight || gblExchangeRatesFlag){
   	if(Language == "EN"){
       frmCurrency.segCurrency.widgetDataMap = {
        lblDecimal:"DECIMAL_PLACES",
        lblCurrCode:"CURR_CODE",
        lblDescriptionAr:"",
        lblDescriptionEn:"ENGLISH_DESC",
        lblCurrency:"CURR_ISO",
        imgTickIcon:"imgTickIcon",
        imgCountryIcon:"IMG_FLAG"
      };
    }else{
    	frmCurrency.segCurrency.widgetDataMap = {
        lblDecimal:"DECIMAL_PLACES",
        lblCurrCode:"CURR_CODE",
        lblDescriptionAr:"ARABIC_DESC",
        lblDescriptionEn:"",
        lblCurrency:"CURR_ISO",
        imgTickIcon:"imgTickIcon",
        imgCountryIcon:"IMG_FLAG"
      };
    }
     if(gblCalculateFXRight){
       var data = "";
       var  from = frmFxRate.lblLeftValue.text;
       for(var q in gblFxRateModifiedArray){
    if(gblFxRateModifiedArray[q].from === from){
      data = gblFxRateModifiedArray[q];
      break;
    }
       }
         
         for(var k in data.to){
      tempArr.push({
         "CURR_CODE":"",
    "ARABIC_DESC":data.to[k].ARABIC_DESC,
    "ENGLISH_DESC": data.to[k].ENGLISH_DESC,
    "CURR_ISO":data.to[k].ToCurrency,
    "DECIMAL_PLACES":"",
    "IMG_FLAG":data.to[k].IMG_FLAG
      }     
      );
     }
  }else{
     for(var j in gblFxRateModifiedArray){
      tempArr.push({
         "CURR_CODE":"",
    "ARABIC_DESC":gblFxRateModifiedArray[j].ARABIC_DESC,
    "ENGLISH_DESC": gblFxRateModifiedArray[j].ENGLISH_DESC,
    "CURR_ISO":gblFxRateModifiedArray[j].from,
    "DECIMAL_PLACES":"",
    "IMG_FLAG":gblFxRateModifiedArray[j].IMG_FLAG
      }     
      );
     }
     }
     kony.print("temp data ::"+JSON.stringify(tempArr));
	 frmCurrency.segCurrency.removeAll();
     frmCurrency.segCurrency.setData(tempArr);
     frmCurrency.flxSearch.isVisible = false;
    }else{
  for(var i in gblCurrList){
    kony.print("currency List ::"+JSON.stringify(gblCurrList[i]));
    if(gblTModule!="own"){
      if(gblSelectedBene.benType != "IFB"){
      	kony.print("CURRENCY :: "+JSON.stringify(kony.boj.detailsForBene.Country[1])+" :: countryName :: "+gblSelectedBene.countryName);
        kony.print("validation ::"+kony.boj.detailsForBene.Country[1].CTRY_CODE == gblSelectedBene.countryName);
        switch(gblCurrList[i].CURR_ISO){
          case "JOD":
          		kony.print("inside JOD");
            	if((get_COUNTRY_DETAILS("2").CTRY_CODE == gblSelectedBene.countryName))
                  tempArr.push(gblCurrList[i]);
            break;
          default:
            	tempArr.push(gblCurrList[i]);
            break;
        }
//         if(CURRENCY !== gblCurrList[i].CURR_ISO )
//           tempArr.push(gblCurrList[i]);
      }else{
          if(gblCurrList[i].CURR_ISO == frmNewTransferKA.lblFromAccCurr.text || gblCurrList[i].CURR_ISO == frmNewTransferKA.lblToAccCurr.text)
              tempArr.push(gblCurrList[i]);
      }
    }else{
    	if(gblCurrList[i].CURR_ISO == frmNewTransferKA.lblFromAccCurr.text || gblCurrList[i].CURR_ISO == frmNewTransferKA.lblToAccCurr.text)
              tempArr.push(gblCurrList[i]);
    }
	if(tempArr.length > 0){
		if(tempArr[tempArr.length-1].CURR_ISO === frmNewTransferKA.lblCurrency.text)
			tempArr[tempArr.length-1].imgTickIcon = "p";
		else
			tempArr[tempArr.length-1].imgTickIcon = "";
	}
    }
      frmCurrency.segCurrency.setData(tempArr);
  kony.print("A :: " +tempArr.length);
  if(tempArr.length >=2){
    frmCurrency.flxSearch.isVisible=true;
  }else{
    frmCurrency.flxSearch.isVisible = false;
  }
  }
  
}


function onClickSegment()
{
  var data = null;
  if(gblCalculateFXLeft || gblCalculateFXRight || gblExchangeRatesFlag){
      data = frmCurrency.segCurrency.selectedRowItems[0];
      kony.print("selectedData:::1:"+JSON.stringify(data));
    
     if(gblExchangeRatesFlag){
       frmFxRate.imgFlag.src = data.IMG_FLAG;
       frmFxRate.lblCountryName.text = data.ENGLISH_DESC;
       frmFxRate.lblCurrency.text = data.CURR_ISO;
       loadSegDataforExchangeRates(data.CURR_ISO);
     }else if(gblCalculateFXLeft){
       frmFxRate.imgLeftFlag.src = data.IMG_FLAG;
       frmFxRate.lblLeftValue.text = data.CURR_ISO;  
       frmFxRate.lblActualValue.text = "1 " + data.CURR_ISO;
       SetDatatoConvertValue(data.CURR_ISO);
      
     }else if(gblCalculateFXRight){
       frmFxRate.imgRightFlag.src = data.IMG_FLAG;
       frmFxRate.lblRightValue.text = data.CURR_ISO;
       frmFxRate.lblEqualentValue.text ="1 " +data.CURR_ISO;
       SetDatatoConvertValue(frmFxRate.lblLeftValue.text,data.CURR_ISO);
     }
      
    }else if(gblTModule === "countryCode"){
    	data = frmCurrency.segCurrency.selectedRowItems[0].mobile_CODE;
    	frmRegisterUser.txtCountryCode.text = data.replace(/\+/g,"");
    	frmRegisterUser.flxLine2.skin = "sknFlxGreenLine";
      	frmRegisterUser.show();
      	frmCurrency.destroy();
    }else if(gblTModule === "loanPostpone"){
    	frmLoanPostpone.lblMonths.text = frmCurrency.segCurrency.selectedRowItems[0].lblMonth;
    	frmLoanPostpone.show();
    	frmCurrency.destroy();
    	gblTModule = "";
    }else{
  var x = frmCurrency.segCurrency.selectedRowItems[0].CURR_ISO;
  if(x !== frmNewTransferKA.lblCurrency.text){
    frmNewTransferKA.lblDecimal.text = frmCurrency.segCurrency.selectedRowItems[0].DECIMAL_PLACES;
    frmNewTransferKA.lblCurrencyCode.text = frmCurrency.segCurrency.selectedRowItems[0].CURR_CODE;
    if(gblTModule == "own" || gblSelectedBene.benType === "DTB" || gblTModule == "send")
    	frmNewTransferKA.lblToAccCurr.text = frmNewTransferKA.lblFromAccCurr.text;
    frmNewTransferKA.lblFromAccCurr.text = frmCurrency.segCurrency.selectedRowItems[0].CURR_ISO;
    frmNewTransferKA.lblCurrency.text = x;
    checkPlaceholder(frmNewTransferKA.lblDecimal.text);
    frmNewTransferKA.txtBox.text=formatAmountwithcomma(frmNewTransferKA.lblHiddenAmount.text,frmNewTransferKA.lblDecimal.text);
    var storeData = isEmpty(x) ? "" : x;
    kony.store.setItem("currency",storeData);
    if(frmNewTransferKA.lblFromAccCurr.text !== frmNewTransferKA.lblToAccCurr.text){
    	getCommisionFee();
    }
    frmNewTransferKA.show();
  }
    }
}


function getDecimalNumForCurr(curr){
  //   alert(" curr :: "+curr);
  //   alert("gblCurrList.length :: "+gblCurrList.length);
  if(!isEmpty(curr)){
    for(var i=0;i<gblCurrList.length;i++){
      kony.print(" gbl object :: "+JSON.stringify(gblCurrList[i]));
      if(gblCurrList[i].CURR_ISO == curr){
        frmNewTransferKA.lblDecimal.text = gblCurrList[i].DECIMAL_PLACES;
        frmNewTransferKA.lblCurrencyCode.text = gblCurrList[i].CURR_CODE;
        //         alert("Dec :: "+frmNewTransferKA.lblDecimal.text);
        //         alert("Curr code :: "+frmNewTransferKA.lblCurrencyCode.text);
        return gblCurrList[i].DECIMAL_PLACES;
      }
    }
  }

  return "";
}

function searchCurrency(data){

  var langSelected = kony.store.getItem("langPrefObj");
  var Language = langSelected.toUpperCase();
  var masterData =[];
  var text;
  var i;


  var tempArr = [];
  if(gblTModule === "countryCode"){
    frmCurrency.segCurrency.widgetDataMap = {
      imgCountryIcon:"flag_SRC",
      CopylblCurrency0c4373e11aa8641:"CopylblCurrency0c4373e11aa8641",
      CopylblDescriptionAr0ca1638a7c3ee4b:"CopylblDescriptionAr0ca1638a7c3ee4b",
      lblMobileCountry:"lblMobileCountry"
    };
  	data=data.toLowerCase();
    if(data !== ""){
      for(var i in country_Mobile_Code){
          text = country_Mobile_Code[i].country_NAME.toLowerCase();
          if(text.indexOf(data) != -1){
              tempArr.push({"flag_SRC":{"src":country_Mobile_Code[i].flag_SRC},
                    "CopylblCurrency0c4373e11aa8641":{"isVisible":false},
                    "CopylblDescriptionAr0ca1638a7c3ee4b":{"isVisible":false},
                    "lblMobileCountry":{"text":country_Mobile_Code[i].country_NAME+" ("+country_Mobile_Code[i].mobile_CODE+")","isVisible":true},
                    "mobile_CODE":country_Mobile_Code[i].mobile_CODE});
          }
      }
    }else{
      for(var i in country_Mobile_Code){
      	tempArr.push({"flag_SRC":{"src":country_Mobile_Code[i].flag_SRC},
                    "CopylblCurrency0c4373e11aa8641":{"isVisible":false},
                    "CopylblDescriptionAr0ca1638a7c3ee4b":{"isVisible":false},
                    "lblMobileCountry":{"text":country_Mobile_Code[i].country_NAME+" ("+country_Mobile_Code[i].mobile_CODE+")","isVisible":true},
                    "mobile_CODE":country_Mobile_Code[i].mobile_CODE});
      }
      //tempArr = country_Mobile_Code;
    }
  }else{
    data=data.toLowerCase();
    if(data != ""){
      var list_COUNTRY = frmCurrency.segCurrency.data;
      for(var i in list_COUNTRY){//gblCurrList
        if(Language == "EN"){
          text = list_COUNTRY[i].ENGLISH_DESC.toLowerCase();
        }
        else{
          text = list_COUNTRY[i].ARABIC_DESC.toLowerCase();
        }
        if(text.indexOf(data) !== -1 || list_COUNTRY[i].CURR_ISO.toLowerCase().indexOf(data) !== -1)
          tempArr.push(list_COUNTRY[i]);
      }
    }
    else{
      //tempArr = gblCurrList;
      setDataInSegment();
      return;
    }

    if(Language == "EN"){
      frmCurrency.segCurrency.widgetDataMap = {
        lblDecimal:"DECIMAL_PLACES",
        lblCurrCode:"CURR_CODE",
        lblDescriptionAr:"",
        lblDescriptionEn:"ENGLISH_DESC",
        lblCurrency:"CURR_ISO",
        imgCountryIcon:"IMG_FLAG"
      };
    }
    else{
      frmCurrency.segCurrency.widgetDataMap = {
        lblDecimal:"DECIMAL_PLACES",
        lblCurrCode:"CURR_CODE",
        lblDescriptionAr:"ARABIC_DESC",
        lblDescriptionEn:"",
        lblCurrency:"CURR_ISO",
        imgCountryIcon:"IMG_FLAG"
      };
    }
  }

  //setSectionAt method call,modifying the data at 2nd sectionIndex position.
  frmCurrency.segCurrency.setData(tempArr);
}

//Currency code and value has been added to show transaction currency for credit card
var currency_CODE = {"004":"AFA","008":"ALL","012":"DZD","024":"AON","031":"AZM","032":"ARS","036":"AUD",
"040":"ATS","044":"BSD","048":"BHD","050":"BDT","051":"AMD","052":"BBD","056":"BEF","060":"BMD","068":"BOB",
"070":"BAD","072":"BWP","076":"BR ","084":"BZD","090":"SBD","096":"BND","100":"BGL","104":"MMK","108":"BIF",
"112":"BYB","116":"KHR","124":"CAD","132":"CVE","136":"KYD","144":"LKR","152":"CLP","156":"CNY","158":"TW",
"170":"COP","174":"KMF","180":"ZRN","188":"CRC","191":"HRK","192":"CUP","196":"CYP","200":"CS ","203":"CZK",
"208":"DKK","214":"DOP","218":"ECS","222":"SVC","230":"ETB","232":"ERN","233":"EEK","238":"FKP","242":"FJD",
"246":"FIM","250":"FRF","262":"DJF","270":"GMD","280":"DEM","288":"GHC","292":"GIP","300":"GRD","320":"GTQ",
"324":"GNF","328":"GYD","332":"HTG","336":"VA ","340":"HNL","344":"HKD","348":"HUF","352":"ISK","356":"INR",
"360":"IDR","364":"IRR","365":"IRA","368":"IQD","372":"IEP","376":"ILS","380":"ITL","388":"JMD","392":"JPY",
"398":"KZT","400":"JOD","404":"KES","408":"KPW","410":"KRW","414":"KWD","417":"KGS","418":"LAK","422":"LBP",
"428":"LVL","430":"LRD","434":"LYD","440":"LTL","442":"LUF","446":"MOP","450":"MGF","454":"MWK","458":"MYR",
"462":"MVR","470":"MTL","478":"MRO","480":"MUR","484":"MXN","492":"MC","496":"MNT","498":"MDL","504":"MAD",
"508":"MZM","512":"OMR","516":"NAD","524":"NPR","528":"NLG","532":"ANG","533":"AWG","548":"VUV","554":"NZD",
"558":"NIO","566":"NGN","578":"NOK","586":"PKR","590":"PAB","598":"PGK","600":"PYG","604":"PEN","608":"PHP",
"616":"PLZ","620":"PTE","624":"GWP","626":"TPE","634":"QAR","642":"ROL","643":"643","646":"RWF","654":"SHP",
"678":"STD","682":"SAR","690":"SCR","694":"SLL","702":"SGD","703":"SKK","704":"VND","705":"SIT","706":"SOS",
"710":"ZAR","716":"ZWD","724":"ESP","736":"SDP","737":"SDA","740":"SRG","748":"SZL","752":"SEK","756":"CHF",
"760":"SYP","762":"TJR","764":"THB","776":"TOP","780":"TTD","784":"AED","788":"TND","792":"TRL","795":"TMM",
"800":"UGX","807":"MKD","810":"RUR","818":"EGP","826":"GBP","834":"TZS","840":"USD","858":"UYU","860":"UZS",
"862":"VEB","882":"WST","890":"YU ","891":"YER","894":"ZMK","901":"TWD","941":"ROS","944":"AZR","946":"RON",
"949":"STR","950":"XAF","951":"XCD","952":"XOF","953":"XPF","954":"XEU","974":"BYR","975":"BGN","976":"CDF",
"977":"BAM","978":"EUR","980":"UAH","981":"GEL","985":"PLN","986":"BRL"};

function getISOCurrency(code){
	kony.print("currency code::"+code);
	return currency_CODE[code] || "JOD";
}

function getCurrency_FROM_ACCOUNT_NUMBER(accountNumber){
	try{
    	accountNumber = parseInt(accountNumber.substring(0,3));
    	setgblcurrencyfullCountryListt();
    	for(var i in gblCurrList){
        	if(parseInt(gblCurrList[i].CURR_CODE) == accountNumber){
            	kony.print("beneficiary currency ::"+gblCurrList[i].CURR_ISO);
            	return gblCurrList[i].CURR_ISO;
            }
        }
    	return CURRENCY;
    }catch(e){
    	kony.print("Exception_getCurrency_FROM_ACCOUNT_NUMBER ::"+e);
    }
}

function getCurrencyCode_FROM_CURRENCY_ISO(currencyISO){
	try{
    	setgblcurrencyfullCountryListt();
    	for(var i in gblCurrList){
        	if(gblCurrList[i].CURR_ISO === currencyISO){
            	return gblCurrList[i].CURR_CODE;
            }
        }
    }catch(e){
    	kony.print("Exception_getCurrencyCode_FROM_CURRENCY_ISO ::"+e);
    }
}

function set_COUNTRYCODE_TO_SEGMENT(){
  try{
    var temp = [];
 	frmCurrency.segCurrency.widgetDataMap = {
      imgCountryIcon:"flag_SRC",
      CopylblCurrency0c4373e11aa8641:"CopylblCurrency0c4373e11aa8641",
      CopylblDescriptionAr0ca1638a7c3ee4b:"CopylblDescriptionAr0ca1638a7c3ee4b",
      lblMobileCountry:"lblMobileCountry"
    };
    for(var i in country_Mobile_Code){
    	temp.push({"flag_SRC":{"src":country_Mobile_Code[i].flag_SRC},
                  "CopylblCurrency0c4373e11aa8641":{"isVisible":false},
                  "CopylblDescriptionAr0ca1638a7c3ee4b":{"isVisible":false},
                  "lblMobileCountry":{"text":country_Mobile_Code[i].country_NAME+" ("+country_Mobile_Code[i].mobile_CODE+")","isVisible":true},
                  "mobile_CODE":country_Mobile_Code[i].mobile_CODE});
    }
    frmCurrency.segCurrency.setData(temp);
  }catch(e){
    kony.print("Exception_set_COUNTRYCODE_TO_SEGMENT ::"+e);
  }
}