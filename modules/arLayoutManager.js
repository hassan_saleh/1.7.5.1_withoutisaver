// Auto generated layout manager module. Do not Modify
var LAYOUTMANAGER = LAYOUTMANAGER || {}


LAYOUTMANAGER.getInstance = function() {


//*********************Private Properties and Methods****************************//
var isDefaultLayout = true;


//*********************Public Properties and Methods****************************//
var layoutManager = {}


//Invoke the following function in the post app init - without fail
//This function is not accessible from Event Editor. See below for alternative API that will be accessible from Event Editor
layoutManager.init = function(){
isDefaultLayout = true;
kony.print("layoutManager : init successful");
}


// Invoke this when you want to switch to the Default Layout for which the App is designed.
// Example: Invoke this for switching to Left To Right (LTR) Layout (for English)
//This function is not accessible from Event Editor. See below for alternative API that will be accessible from Event Editor
layoutManager.switchToDefaultLayout = function(fnCallBack){
kony.print("layoutManager : switchToDefaultLayout : starts");

isDefaultLayout = true;

// Destroy all un necessary data in the current layout
this.destroyForms();

// Re Assign forms/templates to original layout
// Create all the templates specific to En Layout
initializeCopyFBox006162e3b293647(); 
initializeCopyFBox008cccb0b07f240(); 
initializeCopyFBox00cf693ec0bfa43(); 
initializeCopyFBox0103104335ef34f(); 
initializeCopyFBox012eceb99cfd84b(); 
initializeCopyFBox01700ce0918f647(); 
initializeCopyFBox01723dbe350de4d(); 
initializeCopyFBox026f56b76b4484b(); 
initializeCopyFBox02b498f6048be4e(); 
initializeCopyFBox02bb1e4273cee42(); 
initializeCopyFBox02c0c5286ce7241(); 
initializeCopyFBox02ced27cc008242(); 
initializeCopyFBox049997eeafdda45(); 
initializeCopyFBox04fe3a6b6da7141(); 
initializeCopyFBox052ae3f35f03445(); 
initializeCopyFBox062f36a6f2e0640(); 
initializeCopyFBox065827887541745(); 
initializeCopyFBox065c77100453543(); 
initializeCopyFBox06cd620e8898c4b(); 
initializeCopyFBox0764caac34d1b46(); 
initializeCopyFBox078f10462bf2840(); 
initializeCopyFBox080a330c081074a(); 
initializeCopyFBox08b53dc64c5e340(); 
initializeCopyFBox0922676a919a240(); 
initializeCopyFBox0951d0ab1351e40(); 
initializeCopyFBox0a2d38c413c754b(); 
initializeCopyFBox0b31d439f0d864b(); 
initializeCopyFBox0b35b83491a1346(); 
initializeCopyFBox0c41b2b93d06a42(); 
initializeCopyFBox0c9be9d6518174e(); 
initializeCopyFBox0cc8c210db6864a(); 
initializeCopyFBox0cd9dd09f2e7c48(); 
initializeCopyFBox0d03b9a3f466648(); 
initializeCopyFBox0d27de42e5d0f42(); 
initializeCopyFBox0df6147d0b4314c(); 
initializeCopyFlexContainer0c881155c547a42(); 
initializeCopyFlexContainer0cd25f95049404c(); 
initializeCopyFlexContainer0h4325042b7974d(); 
initializeFBox02fdfea8b923649(); 
initializeFBox03cc3ce49523f48(); 
initializeFBox04fb62839f2dd4b(); 
initializeFBox05d48d294848143(); 
initializeFBox07ab22bf87ed448(); 
initializeFBox0a8248f803e1745(); 
initializeFBox0f20caa943a1649(); 
initializeiphoneFooter(); 
initializemainSegmentTemplate(); 
initializemapAtm(); 
initializemapATMBranch(); 
initializemapATMBranchWithoutLocation(); 
initializesegacntstatementstmplt(); 
initializesegaddnewpaye(); 
initializesegBulkPaymentConfirmation(); 
initializesegbulkpaymenttmp(); 
initializesegCheckReOrderKA(); 
initializesegDebitCardTrans(); 
initializesegdirectionInfoKA(); 
initializesegFAQRow(); 
initializesegFAQSection(); 
initializesegGetAllIPSAlias(); 
initializesegInformation(); 
initializesegJMPBene(); 
initializesegListViewKA(); 
initializesegmanagecardlesstmplt(); 
initializesegmanagecardstmplt(); 
initializesegmanagepayeetmplt(); 
initializesegmentAndroidIcon(); 
initializesegmentHeader(); 
initializesegmentheadertmplt(); 
initializesegmentPayPerson(); 
initializesegmentWithChevron(); 
initializesegmentWithDoubleLabel(); 
initializesegmentWithIcon(); 
initializesegmentWithLabel(); 
initializesegmentWithoutIcon(); 
initializesegmentWithSwitch(); 
initializesegMessagesBoldTmplDraftKA(); 
initializesegMessagesBoldTmplKA(); 
initializesegMessagesTmplKA(); 
initializesegphonecnct(); 
initializesegSelectProductKA(); 
initializesegShowAcc(); 
initializesegTmpData(); 
initializesegtmpltchecknocolor(); 
initializesegtransactionchecktmplt(); 
initializesegTransferAddExternal(); 
initializetabBarAccounts(); 
initializetabBarDeposits(); 
initializetabBarMore(); 
initializetabBarTransfer(); 
initializeTemp0da2fb8b2bfdc47(); 
initializetempCurrencySectionHeader(); 
initializetempJomopayPopup(); 
initializetempNUOClassificationTypesKA(); 
initializetempP2PphonePayeeKA(); 
initializetempP2PphonePayeeSectionsKA(); 
initializetempPayPeopleKA(); 
initializetempSectionHeaderP2P(); 
initializetmpAccountDetailsScreen(); 
initializetmpAccountLandingKA(); 
initializetmpAccountSMSDisabled(); 
initializetmpAccountSMSEnabled(); 
initializetmpAccountsPayBill(); 
initializetmpAccountType(); 
initializetmpAccountTypes(); 
initializetmpAccPrecheckNoTransactions(); 
initializetmpAddressSearchResults(); 
initializetmpAlert(); 
initializetmpAllBeneHeader(); 
initializetmpAtmServices(); 
initializetmpAuthHeader(); 
initializetmpBudgetListKA(); 
initializetmpCalculateFx(); 
initializetmpCardNumber(); 
initializetmpCardsReorder(); 
initializetmpChooseAccountsSettings(); 
initializetmpCurrency(); 
initializetmpDeviceRegstrationDisabled(); 
initializetmpDeviceRegstrationEnabled(); 
initializetmpExchangeRates(); 
initializetmpFrequrncyData(); 
initializetmpJomopaycontacts(); 
initializetmpJomopayList(); 
initializetmplateCompanyNameKA(); 
initializetmpLinkedAccounts(); 
initializetmpLocationAtmBranch(); 
initializetmplsegLegendKA(); 
initializetmpMonths(); 
initializetmpNavigationOptKA(); 
initializetmpPOT(); 
initializetmpProductBundle(); 
initializetmpQuickBalanceSeg(); 
initializetmpSearchResultsKA(); 
initializetmpSegAccountsTran(); 
initializetmpSegAccTranNoData(); 
initializetmpSegBotUserInput(); 
initializetmpSegCardsLanding(); 
initializetmpSegLoginMethods(); 
initializetmpSegPaymentHistory(); 
initializetmpSegRequestStatus(); 
initializetmpsegSIList(); 
initializetmpServiceTypeDataPrePaid(); 
initializetmpTransactionBiller(); 
initializetmpTransactionStatus(); 
initializetmpTwoLabelsWithHyphen(); 
initializetmpVersionKA(); 
initializetmSegReleaseNote(); 
initializetransactionTemplateColor(); 
initializetransactionTemplateNoColor(); 
initializeUserWidgets(); 
// Create all the forms specific to En Layout
frmAccountAlertsKAGlobals();
frmAccountDetailKAGlobals();
frmAccountDetailsScreenGlobals();
frmAccountInfoKAGlobals();
frmAccountsLandingKAGlobals();
frmAccountsReorderKAGlobals();
frmAccountTypeGlobals();
frmAddExternalAccountHomeGlobals();
frmAddExternalAccountKAGlobals();
frmAddNewPayeeKAGlobals();
frmAddNewPrePayeeKAGlobals();
frmAlertHistoryGlobals();
frmAlertsKAGlobals();
frmApplyNewCardsConfirmKAGlobals();
frmApplyNewCardsKAGlobals();
frmATMPOSLimitGlobals();
frmAuthorizationAlternativesGlobals();
frmBillerListFormGlobals();
frmBillsGlobals();
frmBulkPaymentConfirmKAGlobals();
frmBulkPaymentKAGlobals();
frmCancelCardKAGlobals();
frmCardlessSendMessageGlobals();
frmCardlessTransactionGlobals();
frmCardLinkedAccountsGlobals();
frmCardLinkedAccountsConfirmGlobals();
frmCardOperationsKAGlobals();
frmCardsAddNicknameGlobals();
frmCardsLandingKAGlobals();
frmCardsListPreferencesGlobals();
frmCardStatementKAGlobals();
frmChatBotsGlobals();
frmCheckingJointGlobals();
frmCheckReOrderListKAGlobals();
frmCheckReOrderSuccessKAGlobals();
frmchequeimagesGlobals();
frmChequeStatusGlobals();
frmConfirmationCardKAGlobals();
frmConfirmationPasswordKAGlobals();
frmConfirmationPersonalDetailsKAGlobals();
frmConfirmationUserNameKAGlobals();
frmConfirmCashWithDrawGlobals();
frmConfirmCashWithDrawQRCodeKAGlobals();
frmConfirmDepositKAGlobals();
frmConfirmNPPRegistrationGlobals();
frmConfirmP2PRegistrationKAGlobals();
frmConfirmPayBillGlobals();
frmConfirmPrePayBillGlobals();
frmConfirmStandingInstructionsGlobals();
frmConfirmTransferKAGlobals();
frmCongratulationsGlobals();
frmContactUsKAGlobals();
frmCreateCredentialsGlobals();
frmCreditCardNumberGlobals();
frmCreditCardPaymentGlobals();
frmCreditCardsKAGlobals();
frmCurrencyGlobals();
frmDefaultLoginMethodKAGlobals();
frmDepositPayLandingKAGlobals();
frmDeviceDeRegistrationKAGlobals();
frmDeviceRegisterationIncorrectPinActicvationKAGlobals();
frmDeviceregistrarionSuccessKAGlobals();
frmDeviceRegistrationGlobals();
frmDeviceRegistrationOptionsKAGlobals();
frmDirectionsKAGlobals();
frmEditAccountSettingsGlobals();
frmEditPayeeKAGlobals();
frmEditPayeeSuccessBillPayKAGlobals();
frmEditPayeeSuccessKAGlobals();
frmEnableInternetTransactionKAGlobals();
frmEnlargeAdKAGlobals();
frmEnrolluserLandingKAGlobals();
frmEnterLocationKAGlobals();
frmEnterPersonalDetailsKAGlobals();
frmEPSGlobals();
frmEstatementConfirmKAGlobals();
frmEstatementLandingKAGlobals();
frmFaceIDCaptureGlobals();
frmFacialAuthEnableGlobals();
frmFilterSIGlobals();
frmFilterTransactionGlobals();
frmFrequencyListGlobals();
frmFxRateGlobals();
frmGetIPSAliasGlobals();
frmInformationDetailsKAGlobals();
frmInformationKAGlobals();
frmInstantCashGlobals();
frmIPSHomeGlobals();
frmIPSManageBeneGlobals();
frmIPSRegistrationGlobals();
frmJoMoPayGlobals();
frmJomoPayAccountListGlobals();
frmJOMOPayAddBillerGlobals();
frmJoMoPayConfirmationGlobals();
frmJomopayContactsGlobals();
frmJoMoPayLandingGlobals();
frmJoMoPayQRConfirmGlobals();
frmJomopayRegCountryGlobals();
frmJoMoPayRegistrationGlobals();
frmLoanPostponeGlobals();
frmLocationDetailsKAGlobals();
frmLocatorATMDetailsKAGlobals();
frmLocatorBranchDetailsKAGlobals();
frmLocatorKAGlobals();
frmLoginAuthSuccessKAGlobals();
frmLoginKAGlobals();
frmManageCardLessGlobals();
frmManageCardsKAGlobals();
frmManagePayeeKAGlobals();
frmMoreGlobals();
frmMoreCheckReorderKAGlobals();
frmMoreFaqKAGlobals();
frmMoreForeignExchangeRatesKAGlobals();
frmMoreInterestRatesKAGlobals();
frmMoreLandingKAGlobals();
frmMorePrivacyPolicyKAGlobals();
frmMoreTermsAndConditionsKAGlobals();
frmMyAccountSettingsKAGlobals();
frmMyMoneyListKAGlobals();
frmNewBillDetailsGlobals();
frmNewBillKAGlobals();
frmNewSubAccountConfirmGlobals();
frmNewSubAccountLandingNewGlobals();
frmNewTransferKAGlobals();
frmNewUserOnboardVerificationKAGlobals();
frmOpenTermDepositGlobals();
frmOrderCheckBookGlobals();
frmOrderChequeBookConfirmGlobals();
frmPayBillHomeGlobals();
frmPayeeDetailsKAGlobals();
frmPayeeTransactionsKAGlobals();
frmPaymentDashboardGlobals();
frmPendingWithdrawSummaryGlobals();
frmPickAProductKAGlobals();
frmPinEntryStep1Globals();
frmPinEntryStep2Globals();
frmPinEntrySuccessGlobals();
frmPostLoginAdvertisementKAGlobals();
frmPOTGlobals();
frmPreferredAccountsKAGlobals();
frmPrePaidPayeeDetailsKAGlobals();
frmProductBundleGlobals();
frmReasonsListScreenGlobals();
frmRecentDepositKAGlobals();
frmRecentTransactionDetailsKAGlobals();
frmRegisterUserGlobals();
frmReleaseNotesKAGlobals();
frmRequestPinCardGlobals();
frmRequestStatementAccountsGlobals();
frmRequestStatementAccountsConfirmGlobals();
frmrequestStatusGlobals();
frmScheduledTransactionDetailsKAGlobals();
frmSearchResultsKAGlobals();
frmSelectBankNameGlobals();
frmSelectDetailsBeneGlobals();
frmSetDefaultPageKAGlobals();
frmSetLocatorDistanceFilterKAGlobals();
frmSettingsKAGlobals();
frmSetupFaceIdSettingsKAGlobals();
frmSetUpPinSettingsGlobals();
frmShowAllBeneficiaryGlobals();
frmSMSNotificationGlobals();
frmStandingInstructionsGlobals();
frmStopCardKAGlobals();
frmSuccessFormKAGlobals();
frmSuccessFormQRCodeKAGlobals();
frmTermsAndConditionsKAGlobals();
frmTermsAndCondtionsGlobals();
frmtransactionChequeKAGlobals();
frmTransactionDetailKAGlobals();
frmTransactionDetailsGlobals();
frmTransactionDetailsPFMKAGlobals();
frmTransferStatusGlobals();
frmUserSettingsMyProfileKAGlobals();
frmUserSettingsPinLoginKAGlobals();
frmUserSettingsSiriGlobals();
frmUserSettingsSIRILIMITGlobals();
frmUserSettingsTouchIdKAGlobals();
frmWebChargeGlobals();
PopupCancelAndReplaceCardGlobals();
popupCommonAlertGlobals();
PopupDeviceRegistrationGlobals();
popupNotificationInfoGlobals();
popupStopCardGlobals();
// Provision for Manually Duplicated forms in En Layout


// Nullify all Arabic layout objects


kony.print("layoutManager : switchToDefaultLayout : ends");
// Proceed with user defined logic after layout reversal
fnCallBack();
}


// Invoke this when you want to reverse the default layout of the App
// Example: Invoke this for switching to Right To Left (RTL) Layout (for Arabic)
//This function is not accessible from Event Editor. See below for alternative API that will be accessible from Event Editor
layoutManager.switchToArabicLayout = function(fnCallBack){
kony.print("layoutManager : switchToArabicLayout : starts");

isDefaultLayout = false;

// Destroy all un necessary data in the current layout
this.destroyForms();

// Create all the templates specific to Ar Layout
//initializeCopyFBox006162e3b293647Ar();  hassan 28/12/020
/*initializeCopyFBox008cccb0b07f240Ar(); 
initializeCopyFBox00cf693ec0bfa43Ar(); 
initializeCopyFBox0103104335ef34fAr(); 
initializeCopyFBox012eceb99cfd84bAr(); 
initializeCopyFBox01700ce0918f647Ar(); 
initializeCopyFBox01723dbe350de4dAr(); 
initializeCopyFBox026f56b76b4484bAr(); 
initializeCopyFBox02b498f6048be4eAr(); 
initializeCopyFBox02bb1e4273cee42Ar(); 
initializeCopyFBox02c0c5286ce7241Ar(); 
initializeCopyFBox02ced27cc008242Ar(); 
initializeCopyFBox049997eeafdda45Ar(); 
initializeCopyFBox04fe3a6b6da7141Ar(); 
initializeCopyFBox052ae3f35f03445Ar(); 
initializeCopyFBox062f36a6f2e0640Ar(); 
initializeCopyFBox065827887541745Ar(); 
initializeCopyFBox065c77100453543Ar(); 
initializeCopyFBox06cd620e8898c4bAr(); 
initializeCopyFBox0764caac34d1b46Ar(); 
initializeCopyFBox078f10462bf2840Ar(); 
initializeCopyFBox080a330c081074aAr(); 
initializeCopyFBox08b53dc64c5e340Ar(); 
initializeCopyFBox0922676a919a240Ar(); 
initializeCopyFBox0951d0ab1351e40Ar(); 
initializeCopyFBox0a2d38c413c754bAr(); 
initializeCopyFBox0b31d439f0d864bAr(); 
initializeCopyFBox0b35b83491a1346Ar(); 
initializeCopyFBox0c41b2b93d06a42Ar(); 
initializeCopyFBox0c9be9d6518174eAr(); 
initializeCopyFBox0cc8c210db6864aAr(); 
initializeCopyFBox0cd9dd09f2e7c48Ar(); 
initializeCopyFBox0d03b9a3f466648Ar(); 
initializeCopyFBox0d27de42e5d0f42Ar(); 
initializeCopyFBox0df6147d0b4314cAr(); 
initializeCopyFlexContainer0c881155c547a42Ar(); 
initializeCopyFlexContainer0cd25f95049404cAr(); 
initializeCopyFlexContainer0h4325042b7974dAr(); 
initializeFBox02fdfea8b923649Ar(); 
initializeFBox03cc3ce49523f48Ar(); 
initializeFBox04fb62839f2dd4bAr(); 
initializeFBox05d48d294848143Ar(); 
initializeFBox07ab22bf87ed448Ar(); 
initializeFBox0a8248f803e1745Ar(); 
initializeFBox0f20caa943a1649Ar(); 
initializeiphoneFooterAr(); 
initializemainSegmentTemplateAr(); 
initializemapAtmAr(); 
initializemapATMBranchAr(); 
initializemapATMBranchWithoutLocationAr(); 
initializesegacntstatementstmpltAr(); 
initializesegaddnewpayeAr(); 
initializesegBulkPaymentConfirmationAr(); 
initializesegbulkpaymenttmpAr(); 
initializesegCheckReOrderKAAr(); 
initializesegDebitCardTransAr(); 
initializesegdirectionInfoKAAr(); 
initializesegFAQRowAr(); 
initializesegFAQSectionAr(); 
initializesegGetAllIPSAliasAr(); 
initializesegInformationAr(); 
initializesegJMPBeneAr(); 
initializesegListViewKAAr(); 
initializesegmanagecardlesstmpltAr(); 
initializesegmanagecardstmpltAr(); 
initializesegmanagepayeetmpltAr(); 
initializesegmentAndroidIconAr(); 
initializesegmentHeaderAr(); 
initializesegmentheadertmpltAr(); 
initializesegmentPayPersonAr(); 
initializesegmentWithChevronAr(); 
initializesegmentWithDoubleLabelAr(); 
initializesegmentWithIconAr(); 
initializesegmentWithLabelAr(); 
initializesegmentWithoutIconAr(); 
initializesegmentWithSwitchAr(); 
initializesegMessagesBoldTmplDraftKAAr(); 
initializesegMessagesBoldTmplKAAr(); 
initializesegMessagesTmplKAAr(); 
initializesegphonecnctAr(); 
initializesegSelectProductKAAr(); 
initializesegShowAccAr(); 
initializesegTmpDataAr(); 
initializesegtmpltchecknocolorAr(); 
initializesegtransactionchecktmpltAr(); 
initializesegTransferAddExternalAr(); 
initializetabBarAccountsAr(); 
initializetabBarDepositsAr(); 
initializetabBarMoreAr(); 
initializetabBarTransferAr(); 
initializeTemp0da2fb8b2bfdc47Ar(); 
initializetempCurrencySectionHeaderAr(); 
initializetempJomopayPopupAr(); 
initializetempNUOClassificationTypesKAAr(); 
initializetempP2PphonePayeeKAAr(); 
initializetempP2PphonePayeeSectionsKAAr(); 
initializetempPayPeopleKAAr(); 
initializetempSectionHeaderP2PAr(); 
initializetmpAccountDetailsScreenAr(); 
initializetmpAccountLandingKAAr(); 
initializetmpAccountSMSDisabledAr(); 
initializetmpAccountSMSEnabledAr(); 
initializetmpAccountsPayBillAr(); 
initializetmpAccountTypeAr(); 
initializetmpAccountTypesAr(); 
initializetmpAccPrecheckNoTransactionsAr(); 
initializetmpAddressSearchResultsAr(); 
initializetmpAlertAr(); 
initializetmpAllBeneHeaderAr(); 
initializetmpAtmServicesAr(); 
initializetmpAuthHeaderAr(); 
initializetmpBudgetListKAAr(); 
initializetmpCalculateFxAr(); 
initializetmpCardNumberAr(); 
initializetmpCardsReorderAr(); 
initializetmpChooseAccountsSettingsAr(); 
initializetmpCurrencyAr(); 
initializetmpDeviceRegstrationDisabledAr(); 
initializetmpDeviceRegstrationEnabledAr(); 
initializetmpExchangeRatesAr(); 
initializetmpFrequrncyDataAr(); 
initializetmpJomopaycontactsAr(); 
initializetmpJomopayListAr(); 
initializetmplateCompanyNameKAAr(); 
initializetmpLinkedAccountsAr(); 
initializetmpLocationAtmBranchAr(); 
initializetmplsegLegendKAAr(); 
initializetmpMonthsAr(); 
initializetmpNavigationOptKAAr(); 
initializetmpPOTAr(); 
initializetmpProductBundleAr(); 
initializetmpQuickBalanceSegAr(); 
initializetmpSearchResultsKAAr(); 
initializetmpSegAccountsTranAr(); 
initializetmpSegAccTranNoDataAr(); 
initializetmpSegBotUserInputAr(); 
initializetmpSegCardsLandingAr(); 
initializetmpSegLoginMethodsAr(); 
initializetmpSegPaymentHistoryAr(); 
initializetmpSegRequestStatusAr(); 
initializetmpsegSIListAr(); 
initializetmpServiceTypeDataPrePaidAr(); 
initializetmpTransactionBillerAr(); 
initializetmpTransactionStatusAr(); 
initializetmpTwoLabelsWithHyphenAr(); 
initializetmpVersionKAAr(); 
initializetmSegReleaseNoteAr(); 
initializetransactionTemplateColorAr(); 
initializetransactionTemplateNoColorAr(); 
initializeUserWidgetsAr(); 

*/
//  hassan 28/12/020
// Re Assign original template variables to Ar objects
/*CopyFBox006162e3b293647 = CopyFBox006162e3b293647Ar;
CopyFBox008cccb0b07f240 = CopyFBox008cccb0b07f240Ar;
CopyFBox00cf693ec0bfa43 = CopyFBox00cf693ec0bfa43Ar;
CopyFBox0103104335ef34f = CopyFBox0103104335ef34fAr;
CopyFBox012eceb99cfd84b = CopyFBox012eceb99cfd84bAr;
CopyFBox01700ce0918f647 = CopyFBox01700ce0918f647Ar;
CopyFBox01723dbe350de4d = CopyFBox01723dbe350de4dAr;
CopyFBox026f56b76b4484b = CopyFBox026f56b76b4484bAr;
CopyFBox02b498f6048be4e = CopyFBox02b498f6048be4eAr;
CopyFBox02bb1e4273cee42 = CopyFBox02bb1e4273cee42Ar;
CopyFBox02c0c5286ce7241 = CopyFBox02c0c5286ce7241Ar;
CopyFBox02ced27cc008242 = CopyFBox02ced27cc008242Ar;
CopyFBox049997eeafdda45 = CopyFBox049997eeafdda45Ar;
CopyFBox04fe3a6b6da7141 = CopyFBox04fe3a6b6da7141Ar;
CopyFBox052ae3f35f03445 = CopyFBox052ae3f35f03445Ar;
CopyFBox062f36a6f2e0640 = CopyFBox062f36a6f2e0640Ar;
CopyFBox065827887541745 = CopyFBox065827887541745Ar;
CopyFBox065c77100453543 = CopyFBox065c77100453543Ar;
CopyFBox06cd620e8898c4b = CopyFBox06cd620e8898c4bAr;
CopyFBox0764caac34d1b46 = CopyFBox0764caac34d1b46Ar;
CopyFBox078f10462bf2840 = CopyFBox078f10462bf2840Ar;
CopyFBox080a330c081074a = CopyFBox080a330c081074aAr;
CopyFBox08b53dc64c5e340 = CopyFBox08b53dc64c5e340Ar;
CopyFBox0922676a919a240 = CopyFBox0922676a919a240Ar;
CopyFBox0951d0ab1351e40 = CopyFBox0951d0ab1351e40Ar;
CopyFBox0a2d38c413c754b = CopyFBox0a2d38c413c754bAr;
CopyFBox0b31d439f0d864b = CopyFBox0b31d439f0d864bAr;
CopyFBox0b35b83491a1346 = CopyFBox0b35b83491a1346Ar;
CopyFBox0c41b2b93d06a42 = CopyFBox0c41b2b93d06a42Ar;
CopyFBox0c9be9d6518174e = CopyFBox0c9be9d6518174eAr;
CopyFBox0cc8c210db6864a = CopyFBox0cc8c210db6864aAr;
CopyFBox0cd9dd09f2e7c48 = CopyFBox0cd9dd09f2e7c48Ar;
CopyFBox0d03b9a3f466648 = CopyFBox0d03b9a3f466648Ar;
CopyFBox0d27de42e5d0f42 = CopyFBox0d27de42e5d0f42Ar;
CopyFBox0df6147d0b4314c = CopyFBox0df6147d0b4314cAr;
CopyFlexContainer0c881155c547a42 = CopyFlexContainer0c881155c547a42Ar;
CopyFlexContainer0cd25f95049404c = CopyFlexContainer0cd25f95049404cAr;
CopyFlexContainer0h4325042b7974d = CopyFlexContainer0h4325042b7974dAr;
FBox02fdfea8b923649 = FBox02fdfea8b923649Ar;
FBox03cc3ce49523f48 = FBox03cc3ce49523f48Ar;
FBox04fb62839f2dd4b = FBox04fb62839f2dd4bAr;
FBox05d48d294848143 = FBox05d48d294848143Ar;
FBox07ab22bf87ed448 = FBox07ab22bf87ed448Ar;
FBox0a8248f803e1745 = FBox0a8248f803e1745Ar;
FBox0f20caa943a1649 = FBox0f20caa943a1649Ar;
footerBack = footerBackAr;
FlexContainer0288ca33be0b34f = FlexContainer0288ca33be0b34fAr;
FlexContainer0jae76598fd8d49 = FlexContainer0jae76598fd8d49Ar;
flxMapATMBranch = flxMapATMBranchAr;
flxMapATMBranchWithoutLocation = flxMapATMBranchWithoutLocationAr;
CopyFlexContainer035bb5165f25443 = CopyFlexContainer035bb5165f25443Ar;
FlexContainer0e88cedf2fc2b40 = FlexContainer0e88cedf2fc2b40Ar;
flxBulkPaymentConfirm = flxBulkPaymentConfirmAr;
flxBulkPaymentSeg = flxBulkPaymentSegAr;
CopyFlexContainer02f94a2abf98249 = CopyFlexContainer02f94a2abf98249Ar;
CopyFlexContainer0a99898df0f2a41 = CopyFlexContainer0a99898df0f2a41Ar;
CopyFlexContainer08af3de49f89842 = CopyFlexContainer08af3de49f89842Ar;
flxFAQRow = flxFAQRowAr;
flxFAQSection = flxFAQSectionAr;
flxMainSeg = flxMainSegAr;
flxInformation = flxInformationAr;
flxSegJMPBene = flxSegJMPBeneAr;
flxListViewKA = flxListViewKAAr;
flxManageCardlessNormal = flxManageCardlessNormalAr;
flxCreditCardTemplateKA = flxCreditCardTemplateKAAr;
flxManagePayeeNormal = flxManagePayeeNormalAr;
CopyFlexContainer09af93ee4ec4c43 = CopyFlexContainer09af93ee4ec4c43Ar;
Copycontainer087f3a0994b334a = Copycontainer087f3a0994b334aAr;
FlexContainer06d150d0ad52f42 = FlexContainer06d150d0ad52f42Ar;
Copycontainer06818659abbaf4b = Copycontainer06818659abbaf4bAr;
container = containerAr;
Copycontainer018037268d2ef46 = Copycontainer018037268d2ef46Ar;
CopyFlexContainer0b2b1c26ffbf74f = CopyFlexContainer0b2b1c26ffbf74fAr;
Copycontainer09742046d0d2944 = Copycontainer09742046d0d2944Ar;
Copycontainer0df0d87f430e14f = Copycontainer0df0d87f430e14fAr;
Copycontainer0743583703d9f4f = Copycontainer0743583703d9f4fAr;
flxSegMsgKA = flxSegMsgKAAr;
flxSegMsg = flxSegMsgAr;
flxSegContainerKA = flxSegContainerKAAr;
Copycontainer0a6274027dfa349 = Copycontainer0a6274027dfa349Ar;
flxSegMain = flxSegMainAr;
flxSeg = flxSegAr;
CopyFlexContainer0cb881afe06de49 = CopyFlexContainer0cb881afe06de49Ar;
CopyFlexContainer011baa9bcad7141 = CopyFlexContainer011baa9bcad7141Ar;
CopyFlexContainer071e4f27a130443 = CopyFlexContainer071e4f27a130443Ar;
Copycontainer03f00119dae464d = Copycontainer03f00119dae464dAr;
tabBarBackground = tabBarBackgroundAr;
CopytabBarBackground0d659dae4d37041 = CopytabBarBackground0d659dae4d37041Ar;
CopytabBarBackground0f4227cb8dba14f = CopytabBarBackground0f4227cb8dba14fAr;
CopytabBarBackground0473dca30560f40 = CopytabBarBackground0473dca30560f40Ar;
HBox0be5e43c45e4748 = HBox0be5e43c45e4748Ar;
flxHeadre = flxHeadreAr;
flxJPPopup = flxJPPopupAr;
OuterFlexContainerMainContentsKA = OuterFlexContainerMainContentsKAAr;
CopyFlexContainer074ba9cd1558b41 = CopyFlexContainer074ba9cd1558b41Ar;
CopyFlexContainer0d4f57045e51b42 = CopyFlexContainer0d4f57045e51b42Ar;
CopyFlexContainer0cf774a7e8bf142 = CopyFlexContainer0cf774a7e8bf142Ar;
CopyFlexContainer0i47e9b94f6b74f = CopyFlexContainer0i47e9b94f6b74fAr;
flxAccountDetailsScreenTemplate = flxAccountDetailsScreenTemplateAr;
yourAccount1 = yourAccount1Ar;
flxSMSNotificationDisabled = flxSMSNotificationDisabledAr;
flxSMSNotificationEnabled = flxSMSNotificationEnabledAr;
yourAccount = yourAccountAr;
CopyyourAccount01dfa1aa140424b = CopyyourAccount01dfa1aa140424bAr;
flxFrequency = flxFrequencyAr;
flxtmpAccPrecheckNoTransactions = flxtmpAccPrecheckNoTransactionsAr;
flxAddressSearchResults = flxAddressSearchResultsAr;
flxAlertDetail = flxAlertDetailAr;
flxTmpHeader = flxTmpHeaderAr;
flxAtmServices = flxAtmServicesAr;
flxMain = flxMainAr;
flxBudgetListKA = flxBudgetListKAAr;
flxTmpCalculateFx = flxTmpCalculateFxAr;
flxCardNumber = flxCardNumberAr;
flxCardsReOrderSettingsTmp = flxCardsReOrderSettingsTmpAr;
flxSegChooseAccounts = flxSegChooseAccountsAr;
tmpFlxCurrency = tmpFlxCurrencyAr;
flxDeviceDetailsDisabled = flxDeviceDetailsDisabledAr;
flxDeviceDetailsEnabled = flxDeviceDetailsEnabledAr;
flxExchangeRatestmp = flxExchangeRatestmpAr;
flxFrequencyTmp = flxFrequencyTmpAr;
flxJomoPayContacts = flxJomoPayContactsAr;
flxJomoPayPastDetails = flxJomoPayPastDetailsAr;
FlexContainer040c5584262324d = FlexContainer040c5584262324dAr;
flxtemplateLinkedAccounts = flxtemplateLinkedAccountsAr;
flxLocationDetails = flxLocationDetailsAr;
CopyFlexContainer07124cb64fc4f45 = CopyFlexContainer07124cb64fc4f45Ar;
flxMonthsTemplate = flxMonthsTemplateAr;
flxNavigationOptKA = flxNavigationOptKAAr;
CopyflxFrequencyTmp0ba944e73638745 = CopyflxFrequencyTmp0ba944e73638745Ar;
flxMainu = flxMainuAr;
flxSegQuickBalance = flxSegQuickBalanceAr;
CopyFlexContainer0125626bedd894d = CopyFlexContainer0125626bedd894dAr;
flxTrans = flxTransAr;
flxNoData = flxNoDataAr;
flxBotUserMsg = flxBotUserMsgAr;
flxCardsLandingTemplate = flxCardsLandingTemplateAr;
flxtmpMain = flxtmpMainAr;
flxtmpPaymentHistoryNewKA = flxtmpPaymentHistoryNewKAAr;
flxtmpSegRequestStatus = flxtmpSegRequestStatusAr;
flxtmpSIList = flxtmpSIListAr;
tmpflxServiceType = tmpflxServiceTypeAr;
flxTransactionDetail = flxTransactionDetailAr;
flxTransactionStatusSegDetails = flxTransactionStatusSegDetailsAr;
flxWrapperKA = flxWrapperKAAr;
Copycontainer08ede9d3afdb846 = Copycontainer08ede9d3afdb846Ar;
flxReleaseNotesTmp = flxReleaseNotesTmpAr;
CopyFlexContainer047db9e4d3aaf43 = CopyFlexContainer047db9e4d3aaf43Ar;
CopyFlexContainer0c9f1eddbc7f547 = CopyFlexContainer0c9f1eddbc7f547Ar;
*/
//  hassan 28/12/020
// Create all the forms specific to Ar Layout
  /*
frmAccountAlertsKAGlobalsAr();
frmAccountDetailKAGlobalsAr();
frmAccountDetailsScreenGlobalsAr();
frmAccountInfoKAGlobalsAr();
frmAccountsLandingKAGlobalsAr();
frmAccountsReorderKAGlobalsAr();
frmAccountTypeGlobalsAr();
frmAddExternalAccountHomeGlobalsAr();
frmAddExternalAccountKAGlobalsAr();
frmAddNewPayeeKAGlobalsAr();
frmAddNewPrePayeeKAGlobalsAr();
frmAlertHistoryGlobalsAr();
frmAlertsKAGlobalsAr();
frmApplyNewCardsConfirmKAGlobalsAr();
frmApplyNewCardsKAGlobalsAr();
frmATMPOSLimitGlobalsAr();
frmAuthorizationAlternativesGlobalsAr();
frmBillerListFormGlobalsAr();
frmBillsGlobalsAr();
frmBulkPaymentConfirmKAGlobalsAr();
frmBulkPaymentKAGlobalsAr();
frmCancelCardKAGlobalsAr();
frmCardlessSendMessageGlobalsAr();
frmCardlessTransactionGlobalsAr();
frmCardLinkedAccountsGlobalsAr();
frmCardLinkedAccountsConfirmGlobalsAr();
frmCardOperationsKAGlobalsAr();
frmCardsAddNicknameGlobalsAr();
frmCardsLandingKAGlobalsAr();
frmCardsListPreferencesGlobalsAr();
frmCardStatementKAGlobalsAr();
frmChatBotsGlobalsAr();
frmCheckingJointGlobalsAr();
frmCheckReOrderListKAGlobalsAr();
frmCheckReOrderSuccessKAGlobalsAr();
frmchequeimagesGlobalsAr();
frmChequeStatusGlobalsAr();
frmConfirmationCardKAGlobalsAr();
frmConfirmationPasswordKAGlobalsAr();
frmConfirmationPersonalDetailsKAGlobalsAr();
frmConfirmationUserNameKAGlobalsAr();
frmConfirmCashWithDrawGlobalsAr();
frmConfirmCashWithDrawQRCodeKAGlobalsAr();
frmConfirmDepositKAGlobalsAr();
frmConfirmNPPRegistrationGlobalsAr();
frmConfirmP2PRegistrationKAGlobalsAr();
frmConfirmPayBillGlobalsAr();
frmConfirmPrePayBillGlobalsAr();
frmConfirmStandingInstructionsGlobalsAr();
frmConfirmTransferKAGlobalsAr();
frmCongratulationsGlobalsAr();
frmContactUsKAGlobalsAr();
frmCreateCredentialsGlobalsAr();
frmCreditCardNumberGlobalsAr();
frmCreditCardPaymentGlobalsAr();
frmCreditCardsKAGlobalsAr();
frmCurrencyGlobalsAr();
frmDefaultLoginMethodKAGlobalsAr();
frmDepositPayLandingKAGlobalsAr();
frmDeviceDeRegistrationKAGlobalsAr();
frmDeviceRegisterationIncorrectPinActicvationKAGlobalsAr();
frmDeviceregistrarionSuccessKAGlobalsAr();
frmDeviceRegistrationGlobalsAr();
frmDeviceRegistrationOptionsKAGlobalsAr();
frmDirectionsKAGlobalsAr();
frmEditAccountSettingsGlobalsAr();
frmEditPayeeKAGlobalsAr();
frmEditPayeeSuccessBillPayKAGlobalsAr();
frmEditPayeeSuccessKAGlobalsAr();
frmEnableInternetTransactionKAGlobalsAr();
frmEnlargeAdKAGlobalsAr();
frmEnrolluserLandingKAGlobalsAr();
frmEnterLocationKAGlobalsAr();
frmEnterPersonalDetailsKAGlobalsAr();
frmEPSGlobalsAr();
frmEstatementConfirmKAGlobalsAr();
frmEstatementLandingKAGlobalsAr();
frmFaceIDCaptureGlobalsAr();
frmFacialAuthEnableGlobalsAr();
frmFilterSIGlobalsAr();
frmFilterTransactionGlobalsAr();
frmFrequencyListGlobalsAr();
frmFxRateGlobalsAr();
frmGetIPSAliasGlobalsAr();
frmInformationDetailsKAGlobalsAr();
frmInformationKAGlobalsAr();
frmInstantCashGlobalsAr();
frmIPSHomeGlobalsAr();
frmIPSManageBeneGlobalsAr();
frmIPSRegistrationGlobalsAr();
frmJoMoPayGlobalsAr();
frmJomoPayAccountListGlobalsAr();
frmJOMOPayAddBillerGlobalsAr();
frmJoMoPayConfirmationGlobalsAr();
frmJomopayContactsGlobalsAr();
frmJoMoPayLandingGlobalsAr();
frmJoMoPayQRConfirmGlobalsAr();
frmJomopayRegCountryGlobalsAr();
frmJoMoPayRegistrationGlobalsAr();
frmLoanPostponeGlobalsAr();
frmLocationDetailsKAGlobalsAr();
frmLocatorATMDetailsKAGlobalsAr();
frmLocatorBranchDetailsKAGlobalsAr();
frmLocatorKAGlobalsAr();
frmLoginAuthSuccessKAGlobalsAr();
frmLoginKAGlobalsAr();
frmManageCardLessGlobalsAr();
frmManageCardsKAGlobalsAr();
frmManagePayeeKAGlobalsAr();
frmMoreGlobalsAr();
frmMoreCheckReorderKAGlobalsAr();
frmMoreFaqKAGlobalsAr();
frmMoreForeignExchangeRatesKAGlobalsAr();
frmMoreInterestRatesKAGlobalsAr();
frmMoreLandingKAGlobalsAr();
frmMorePrivacyPolicyKAGlobalsAr();
frmMoreTermsAndConditionsKAGlobalsAr();
frmMyAccountSettingsKAGlobalsAr();
frmMyMoneyListKAGlobalsAr();
frmNewBillDetailsGlobalsAr();
frmNewBillKAGlobalsAr();
frmNewSubAccountConfirmGlobalsAr();
frmNewSubAccountLandingNewGlobalsAr();
frmNewTransferKAGlobalsAr();
frmNewUserOnboardVerificationKAGlobalsAr();
frmOpenTermDepositGlobalsAr();
frmOrderCheckBookGlobalsAr();
frmOrderChequeBookConfirmGlobalsAr();
frmPayBillHomeGlobalsAr();
frmPayeeDetailsKAGlobalsAr();
frmPayeeTransactionsKAGlobalsAr();
frmPaymentDashboardGlobalsAr();
frmPendingWithdrawSummaryGlobalsAr();
frmPickAProductKAGlobalsAr();
frmPinEntryStep1GlobalsAr();
frmPinEntryStep2GlobalsAr();
frmPinEntrySuccessGlobalsAr();
frmPostLoginAdvertisementKAGlobalsAr();
frmPOTGlobalsAr();
frmPreferredAccountsKAGlobalsAr();
frmPrePaidPayeeDetailsKAGlobalsAr();
frmProductBundleGlobalsAr();
frmReasonsListScreenGlobalsAr();
frmRecentDepositKAGlobalsAr();
frmRecentTransactionDetailsKAGlobalsAr();
frmRegisterUserGlobalsAr();
frmReleaseNotesKAGlobalsAr();
frmRequestPinCardGlobalsAr();
frmRequestStatementAccountsGlobalsAr();
frmRequestStatementAccountsConfirmGlobalsAr();
frmrequestStatusGlobalsAr();
frmScheduledTransactionDetailsKAGlobalsAr();
frmSearchResultsKAGlobalsAr();
frmSelectBankNameGlobalsAr();
frmSelectDetailsBeneGlobalsAr();
frmSetDefaultPageKAGlobalsAr();
frmSetLocatorDistanceFilterKAGlobalsAr();
frmSettingsKAGlobalsAr();
frmSetupFaceIdSettingsKAGlobalsAr();
frmSetUpPinSettingsGlobalsAr();
frmShowAllBeneficiaryGlobalsAr();
frmSMSNotificationGlobalsAr();
frmStandingInstructionsGlobalsAr();
frmStopCardKAGlobalsAr();
frmSuccessFormKAGlobalsAr();
frmSuccessFormQRCodeKAGlobalsAr();
frmTermsAndConditionsKAGlobalsAr();
frmTermsAndCondtionsGlobalsAr();
frmtransactionChequeKAGlobalsAr();
frmTransactionDetailKAGlobalsAr();
frmTransactionDetailsGlobalsAr();
frmTransactionDetailsPFMKAGlobalsAr();
frmTransferStatusGlobalsAr();
frmUserSettingsMyProfileKAGlobalsAr();
frmUserSettingsPinLoginKAGlobalsAr();
frmUserSettingsSiriGlobalsAr();
frmUserSettingsSIRILIMITGlobalsAr();
frmUserSettingsTouchIdKAGlobalsAr();
frmWebChargeGlobalsAr();
PopupCancelAndReplaceCardGlobalsAr();
popupCommonAlertGlobalsAr();
PopupDeviceRegistrationGlobalsAr();
popupNotificationInfoGlobalsAr();
popupStopCardGlobalsAr();
// Provision for Manually Duplicated forms in Ar Layout


// Re Assign original forms variables to Ar objects
frmAccountAlertsKA = frmAccountAlertsKAAr;
frmAccountDetailKA = frmAccountDetailKAAr;
frmAccountDetailsScreen = frmAccountDetailsScreenAr;
frmAccountInfoKA = frmAccountInfoKAAr;
frmAccountsLandingKA = frmAccountsLandingKAAr;
frmAccountsReorderKA = frmAccountsReorderKAAr;
frmAccountType = frmAccountTypeAr;
frmAddExternalAccountHome = frmAddExternalAccountHomeAr;
frmAddExternalAccountKA = frmAddExternalAccountKAAr;
frmAddNewPayeeKA = frmAddNewPayeeKAAr;
frmAddNewPrePayeeKA = frmAddNewPrePayeeKAAr;
frmAlertHistory = frmAlertHistoryAr;
frmAlertsKA = frmAlertsKAAr;
frmApplyNewCardsConfirmKA = frmApplyNewCardsConfirmKAAr;
frmApplyNewCardsKA = frmApplyNewCardsKAAr;
frmATMPOSLimit = frmATMPOSLimitAr;
frmAuthorizationAlternatives = frmAuthorizationAlternativesAr;
frmBillerListForm = frmBillerListFormAr;
frmBills = frmBillsAr;
frmBulkPaymentConfirmKA = frmBulkPaymentConfirmKAAr;
frmBulkPaymentKA = frmBulkPaymentKAAr;
frmCancelCardKA = frmCancelCardKAAr;
frmCardlessSendMessage = frmCardlessSendMessageAr;
frmCardlessTransaction = frmCardlessTransactionAr;
frmCardLinkedAccounts = frmCardLinkedAccountsAr;
frmCardLinkedAccountsConfirm = frmCardLinkedAccountsConfirmAr;
frmCardOperationsKA = frmCardOperationsKAAr;
frmCardsAddNickname = frmCardsAddNicknameAr;
frmCardsLandingKA = frmCardsLandingKAAr;
frmCardsListPreferences = frmCardsListPreferencesAr;
frmCardStatementKA = frmCardStatementKAAr;
frmChatBots = frmChatBotsAr;
frmCheckingJoint = frmCheckingJointAr;
frmCheckReOrderListKA = frmCheckReOrderListKAAr;
frmCheckReOrderSuccessKA = frmCheckReOrderSuccessKAAr;
frmchequeimages = frmchequeimagesAr;
frmChequeStatus = frmChequeStatusAr;
frmConfirmationCardKA = frmConfirmationCardKAAr;
frmConfirmationPasswordKA = frmConfirmationPasswordKAAr;
frmConfirmationPersonalDetailsKA = frmConfirmationPersonalDetailsKAAr;
frmConfirmationUserNameKA = frmConfirmationUserNameKAAr;
frmConfirmCashWithDraw = frmConfirmCashWithDrawAr;
frmConfirmCashWithDrawQRCodeKA = frmConfirmCashWithDrawQRCodeKAAr;
frmConfirmDepositKA = frmConfirmDepositKAAr;
frmConfirmNPPRegistration = frmConfirmNPPRegistrationAr;
frmConfirmP2PRegistrationKA = frmConfirmP2PRegistrationKAAr;
frmConfirmPayBill = frmConfirmPayBillAr;
frmConfirmPrePayBill = frmConfirmPrePayBillAr;
frmConfirmStandingInstructions = frmConfirmStandingInstructionsAr;
frmConfirmTransferKA = frmConfirmTransferKAAr;
frmCongratulations = frmCongratulationsAr;
frmContactUsKA = frmContactUsKAAr;
frmCreateCredentials = frmCreateCredentialsAr;
frmCreditCardNumber = frmCreditCardNumberAr;
frmCreditCardPayment = frmCreditCardPaymentAr;
frmCreditCardsKA = frmCreditCardsKAAr;
frmCurrency = frmCurrencyAr;
frmDefaultLoginMethodKA = frmDefaultLoginMethodKAAr;
frmDepositPayLandingKA = frmDepositPayLandingKAAr;
frmDeviceDeRegistrationKA = frmDeviceDeRegistrationKAAr;
frmDeviceRegisterationIncorrectPinActicvationKA = frmDeviceRegisterationIncorrectPinActicvationKAAr;
frmDeviceregistrarionSuccessKA = frmDeviceregistrarionSuccessKAAr;
frmDeviceRegistration = frmDeviceRegistrationAr;
frmDeviceRegistrationOptionsKA = frmDeviceRegistrationOptionsKAAr;
frmDirectionsKA = frmDirectionsKAAr;
frmEditAccountSettings = frmEditAccountSettingsAr;
frmEditPayeeKA = frmEditPayeeKAAr;
frmEditPayeeSuccessBillPayKA = frmEditPayeeSuccessBillPayKAAr;
frmEditPayeeSuccessKA = frmEditPayeeSuccessKAAr;
frmEnableInternetTransactionKA = frmEnableInternetTransactionKAAr;
frmEnlargeAdKA = frmEnlargeAdKAAr;
frmEnrolluserLandingKA = frmEnrolluserLandingKAAr;
frmEnterLocationKA = frmEnterLocationKAAr;
frmEnterPersonalDetailsKA = frmEnterPersonalDetailsKAAr;
frmEPS = frmEPSAr;
frmEstatementConfirmKA = frmEstatementConfirmKAAr;
frmEstatementLandingKA = frmEstatementLandingKAAr;
frmFaceIDCapture = frmFaceIDCaptureAr;
frmFacialAuthEnable = frmFacialAuthEnableAr;
frmFilterSI = frmFilterSIAr;
frmFilterTransaction = frmFilterTransactionAr;
frmFrequencyList = frmFrequencyListAr;
frmFxRate = frmFxRateAr;
frmGetIPSAlias = frmGetIPSAliasAr;
frmInformationDetailsKA = frmInformationDetailsKAAr;
frmInformationKA = frmInformationKAAr;
frmInstantCash = frmInstantCashAr;
frmIPSHome = frmIPSHomeAr;
frmIPSManageBene = frmIPSManageBeneAr;
frmIPSRegistration = frmIPSRegistrationAr;
frmJoMoPay = frmJoMoPayAr;
frmJomoPayAccountList = frmJomoPayAccountListAr;
frmJOMOPayAddBiller = frmJOMOPayAddBillerAr;
frmJoMoPayConfirmation = frmJoMoPayConfirmationAr;
frmJomopayContacts = frmJomopayContactsAr;
frmJoMoPayLanding = frmJoMoPayLandingAr;
frmJoMoPayQRConfirm = frmJoMoPayQRConfirmAr;
frmJomopayRegCountry = frmJomopayRegCountryAr;
frmJoMoPayRegistration = frmJoMoPayRegistrationAr;
frmLoanPostpone = frmLoanPostponeAr;
frmLocationDetailsKA = frmLocationDetailsKAAr;
frmLocatorATMDetailsKA = frmLocatorATMDetailsKAAr;
frmLocatorBranchDetailsKA = frmLocatorBranchDetailsKAAr;
frmLocatorKA = frmLocatorKAAr;
frmLoginAuthSuccessKA = frmLoginAuthSuccessKAAr;
frmLoginKA = frmLoginKAAr;
frmManageCardLess = frmManageCardLessAr;
frmManageCardsKA = frmManageCardsKAAr;
frmManagePayeeKA = frmManagePayeeKAAr;
frmMore = frmMoreAr;
frmMoreCheckReorderKA = frmMoreCheckReorderKAAr;
frmMoreFaqKA = frmMoreFaqKAAr;
frmMoreForeignExchangeRatesKA = frmMoreForeignExchangeRatesKAAr;
frmMoreInterestRatesKA = frmMoreInterestRatesKAAr;
frmMoreLandingKA = frmMoreLandingKAAr;
frmMorePrivacyPolicyKA = frmMorePrivacyPolicyKAAr;
frmMoreTermsAndConditionsKA = frmMoreTermsAndConditionsKAAr;
frmMyAccountSettingsKA = frmMyAccountSettingsKAAr;
frmMyMoneyListKA = frmMyMoneyListKAAr;
frmNewBillDetails = frmNewBillDetailsAr;
frmNewBillKA = frmNewBillKAAr;
frmNewSubAccountConfirm = frmNewSubAccountConfirmAr;
frmNewSubAccountLandingNew = frmNewSubAccountLandingNewAr;
frmNewTransferKA = frmNewTransferKAAr;
frmNewUserOnboardVerificationKA = frmNewUserOnboardVerificationKAAr;
frmOpenTermDeposit = frmOpenTermDepositAr;
frmOrderCheckBook = frmOrderCheckBookAr;
frmOrderChequeBookConfirm = frmOrderChequeBookConfirmAr;
frmPayBillHome = frmPayBillHomeAr;
frmPayeeDetailsKA = frmPayeeDetailsKAAr;
frmPayeeTransactionsKA = frmPayeeTransactionsKAAr;
frmPaymentDashboard = frmPaymentDashboardAr;
frmPendingWithdrawSummary = frmPendingWithdrawSummaryAr;
frmPickAProductKA = frmPickAProductKAAr;
frmPinEntryStep1 = frmPinEntryStep1Ar;
frmPinEntryStep2 = frmPinEntryStep2Ar;
frmPinEntrySuccess = frmPinEntrySuccessAr;
frmPostLoginAdvertisementKA = frmPostLoginAdvertisementKAAr;
frmPOT = frmPOTAr;
frmPreferredAccountsKA = frmPreferredAccountsKAAr;
frmPrePaidPayeeDetailsKA = frmPrePaidPayeeDetailsKAAr;
frmProductBundle = frmProductBundleAr;
frmReasonsListScreen = frmReasonsListScreenAr;
frmRecentDepositKA = frmRecentDepositKAAr;
frmRecentTransactionDetailsKA = frmRecentTransactionDetailsKAAr;
frmRegisterUser = frmRegisterUserAr;
frmReleaseNotesKA = frmReleaseNotesKAAr;
frmRequestPinCard = frmRequestPinCardAr;
frmRequestStatementAccounts = frmRequestStatementAccountsAr;
frmRequestStatementAccountsConfirm = frmRequestStatementAccountsConfirmAr;
frmrequestStatus = frmrequestStatusAr;
frmScheduledTransactionDetailsKA = frmScheduledTransactionDetailsKAAr;
frmSearchResultsKA = frmSearchResultsKAAr;
frmSelectBankName = frmSelectBankNameAr;
frmSelectDetailsBene = frmSelectDetailsBeneAr;
frmSetDefaultPageKA = frmSetDefaultPageKAAr;
frmSetLocatorDistanceFilterKA = frmSetLocatorDistanceFilterKAAr;
frmSettingsKA = frmSettingsKAAr;
frmSetupFaceIdSettingsKA = frmSetupFaceIdSettingsKAAr;
frmSetUpPinSettings = frmSetUpPinSettingsAr;
frmShowAllBeneficiary = frmShowAllBeneficiaryAr;
frmSMSNotification = frmSMSNotificationAr;
frmStandingInstructions = frmStandingInstructionsAr;
frmStopCardKA = frmStopCardKAAr;
frmSuccessFormKA = frmSuccessFormKAAr;
frmSuccessFormQRCodeKA = frmSuccessFormQRCodeKAAr;
frmTermsAndConditionsKA = frmTermsAndConditionsKAAr;
frmTermsAndCondtions = frmTermsAndCondtionsAr;
frmtransactionChequeKA = frmtransactionChequeKAAr;
frmTransactionDetailKA = frmTransactionDetailKAAr;
frmTransactionDetails = frmTransactionDetailsAr;
frmTransactionDetailsPFMKA = frmTransactionDetailsPFMKAAr;
frmTransferStatus = frmTransferStatusAr;
frmUserSettingsMyProfileKA = frmUserSettingsMyProfileKAAr;
frmUserSettingsPinLoginKA = frmUserSettingsPinLoginKAAr;
frmUserSettingsSiri = frmUserSettingsSiriAr;
frmUserSettingsSIRILIMIT = frmUserSettingsSIRILIMITAr;
frmUserSettingsTouchIdKA = frmUserSettingsTouchIdKAAr;
frmWebCharge = frmWebChargeAr;
PopupCancelAndReplaceCard = PopupCancelAndReplaceCardAr;
popupCommonAlert = popupCommonAlertAr;
PopupDeviceRegistration = PopupDeviceRegistrationAr;
popupNotificationInfo = popupNotificationInfoAr;
popupStopCard = popupStopCardAr;
// Provision for Manually Duplicated forms in Ar Layout
*/
//  hassan 28/12/020
kony.print("layoutManager : switchToArabicLayout : ends");
// Proceed with user defined logic after layout reversal
fnCallBack();
}

// This function is used to destroy all the forms of the application
layoutManager.destroyForms = function(){
kony.print("Inside the destroy forms function");
  //Omar Alnajjar
    frmSmartInfo.destroy();
//   frmIPSRequests.destroy();

frmAccountAlertsKA.destroy();
frmAccountDetailKA.destroy();
frmAccountDetailsScreen.destroy();
frmAccountInfoKA.destroy();
frmAccountsLandingKA.destroy();
frmAccountsReorderKA.destroy();
frmAccountType.destroy();
frmAddExternalAccountHome.destroy();
frmAddExternalAccountKA.destroy();
frmAddNewPayeeKA.destroy();
frmAddNewPrePayeeKA.destroy();
frmAlertHistory.destroy();
frmAlertsKA.destroy();
frmApplyNewCardsConfirmKA.destroy();
frmApplyNewCardsKA.destroy();
frmATMPOSLimit.destroy();
frmAuthorizationAlternatives.destroy();
frmBillerListForm.destroy();
frmBills.destroy();
frmBulkPaymentConfirmKA.destroy();
frmBulkPaymentKA.destroy();
frmCancelCardKA.destroy();
frmCardlessSendMessage.destroy();
frmCardlessTransaction.destroy();
frmCardLinkedAccounts.destroy();
frmCardLinkedAccountsConfirm.destroy();
frmCardOperationsKA.destroy();
frmCardsAddNickname.destroy();
frmCardsLandingKA.destroy();
frmCardsListPreferences.destroy();
frmCardStatementKA.destroy();
frmChatBots.destroy();
frmCheckingJoint.destroy();
frmCheckReOrderListKA.destroy();
frmCheckReOrderSuccessKA.destroy();
frmchequeimages.destroy();
frmChequeStatus.destroy();
frmConfirmationCardKA.destroy();
frmConfirmationPasswordKA.destroy();
frmConfirmationPersonalDetailsKA.destroy();
frmConfirmationUserNameKA.destroy();
frmConfirmCashWithDraw.destroy();
frmConfirmCashWithDrawQRCodeKA.destroy();
frmConfirmDepositKA.destroy();
frmConfirmNPPRegistration.destroy();
frmConfirmP2PRegistrationKA.destroy();
frmConfirmPayBill.destroy();
frmConfirmPrePayBill.destroy();
frmConfirmStandingInstructions.destroy();
frmConfirmTransferKA.destroy();
frmCongratulations.destroy();
frmContactUsKA.destroy();
frmCreateCredentials.destroy();
frmCreditCardNumber.destroy();
frmCreditCardPayment.destroy();
frmCreditCardsKA.destroy();
frmCurrency.destroy();
frmDefaultLoginMethodKA.destroy();
frmDepositPayLandingKA.destroy();
frmDeviceDeRegistrationKA.destroy();
frmDeviceRegisterationIncorrectPinActicvationKA.destroy();
frmDeviceregistrarionSuccessKA.destroy();
frmDeviceRegistration.destroy();
frmDeviceRegistrationOptionsKA.destroy();
frmDirectionsKA.destroy();
frmEditAccountSettings.destroy();
frmEditPayeeKA.destroy();
frmEditPayeeSuccessBillPayKA.destroy();
frmEditPayeeSuccessKA.destroy();
frmEnableInternetTransactionKA.destroy();
frmEnlargeAdKA.destroy();
frmEnrolluserLandingKA.destroy();
frmEnterLocationKA.destroy();
frmEnterPersonalDetailsKA.destroy();
frmEPS.destroy();
frmEstatementConfirmKA.destroy();
frmEstatementLandingKA.destroy();
frmFaceIDCapture.destroy();
frmFacialAuthEnable.destroy();
frmFilterSI.destroy();
frmFilterTransaction.destroy();
frmFrequencyList.destroy();
frmFxRate.destroy();
frmGetIPSAlias.destroy();
frmInformationDetailsKA.destroy();
frmInformationKA.destroy();
frmInstantCash.destroy();
frmIPSHome.destroy();
frmIPSManageBene.destroy();
frmIPSRegistration.destroy();
frmJoMoPay.destroy();
frmJomoPayAccountList.destroy();
frmJOMOPayAddBiller.destroy();
frmJoMoPayConfirmation.destroy();
frmJomopayContacts.destroy();
frmJoMoPayLanding.destroy();
frmJoMoPayQRConfirm.destroy();
frmJomopayRegCountry.destroy();
frmJoMoPayRegistration.destroy();
frmLoanPostpone.destroy();
frmLocationDetailsKA.destroy();
frmLocatorATMDetailsKA.destroy();
frmLocatorBranchDetailsKA.destroy();
frmLocatorKA.destroy();
frmLoginAuthSuccessKA.destroy();
frmLoginKA.destroy();
frmManageCardLess.destroy();
frmManageCardsKA.destroy();
frmManagePayeeKA.destroy();
frmMore.destroy();
frmMoreCheckReorderKA.destroy();
frmMoreFaqKA.destroy();
frmMoreForeignExchangeRatesKA.destroy();
frmMoreInterestRatesKA.destroy();
frmMoreLandingKA.destroy();
frmMorePrivacyPolicyKA.destroy();
frmMoreTermsAndConditionsKA.destroy();
frmMyAccountSettingsKA.destroy();
frmMyMoneyListKA.destroy();
frmNewBillDetails.destroy();
frmNewBillKA.destroy();
frmNewSubAccountConfirm.destroy();
frmNewSubAccountLandingNew.destroy();
frmNewTransferKA.destroy();
frmNewUserOnboardVerificationKA.destroy();
frmOpenTermDeposit.destroy();
frmOrderCheckBook.destroy();
frmOrderChequeBookConfirm.destroy();
frmPayBillHome.destroy();
frmPayeeDetailsKA.destroy();
frmPayeeTransactionsKA.destroy();
frmPaymentDashboard.destroy();
frmPendingWithdrawSummary.destroy();
frmPickAProductKA.destroy();
frmPinEntryStep1.destroy();
frmPinEntryStep2.destroy();
frmPinEntrySuccess.destroy();
frmPostLoginAdvertisementKA.destroy();
frmPOT.destroy();
frmPreferredAccountsKA.destroy();
frmPrePaidPayeeDetailsKA.destroy();
frmProductBundle.destroy();
frmReasonsListScreen.destroy();
frmRecentDepositKA.destroy();
frmRecentTransactionDetailsKA.destroy();
frmRegisterUser.destroy();
frmReleaseNotesKA.destroy();
frmRequestPinCard.destroy();
frmRequestStatementAccounts.destroy();
frmRequestStatementAccountsConfirm.destroy();
frmrequestStatus.destroy();
frmScheduledTransactionDetailsKA.destroy();
frmSearchResultsKA.destroy();
frmSelectBankName.destroy();
frmSelectDetailsBene.destroy();
frmSetDefaultPageKA.destroy();
frmSetLocatorDistanceFilterKA.destroy();
//frmSettingsKA.destroy();
frmSetupFaceIdSettingsKA.destroy();
frmSetUpPinSettings.destroy();
frmShowAllBeneficiary.destroy();
frmSMSNotification.destroy();
frmStandingInstructions.destroy();
frmStopCardKA.destroy();
frmSuccessFormKA.destroy();
frmSuccessFormQRCodeKA.destroy();
frmTermsAndConditionsKA.destroy();
frmTermsAndCondtions.destroy();
frmtransactionChequeKA.destroy();
frmTransactionDetailKA.destroy();
frmTransactionDetails.destroy();
frmTransactionDetailsPFMKA.destroy();
frmTransferStatus.destroy();
frmUserSettingsMyProfileKA.destroy();
frmUserSettingsPinLoginKA.destroy();
frmUserSettingsSiri.destroy();
frmUserSettingsSIRILIMIT.destroy();
frmUserSettingsTouchIdKA.destroy();
frmWebCharge.destroy();
PopupCancelAndReplaceCard.destroy();
popupCommonAlert.destroy();
PopupDeviceRegistration.destroy();
popupNotificationInfo.destroy();
popupStopCard.destroy();
}


// Invoke this when you want to know the current layout being used by the App
//This function is not accessible from Event Editor. 
layoutManager.isDefaultLayout = function(){
return isDefaultLayout;
}




layoutManager.updateMasterData = function(widgetObj, values){
var widgetType = "";
try {
	widgetType = kony.type(widgetObj);
}catch (e) {	
	//swallow, nothing can be done
	kony.print("layoutManager: Cannot Update Master Data. Error : " + e);
	return;
}
kony.print("layoutManager: Master Data update requested for widget type : " + widgetType);
if (widgetType == "kony.ui.CheckBoxGroup" || 
	widgetType == "kony.ui.RadioButtonGroup" ||
	widgetType == "kony.ui.ComboBox" ||
	widgetType == "kony.ui.ListBox" ||
	widgetType == "kony.ui.PickerView") {
kony.print("layoutManager: Master Data update supported");
} else {
	kony.print("layoutManager: Master Data update not supported");
	return;
}
kony.print("layoutManager: Checking -- Is Default Layout ? " + arLayoutManager.isDefaultLayout());
if (!arLayoutManager.isDefaultLayout()) {
	var reverseValues = [];
	try {
		for (var i=(values.length-1);i>=0;i--) {
			reverseValues.push(values[i]);
		}
	kony.print("layoutManager: This is Reverse Layout. Assigning master data in Reverse order");
	widgetObj.masterData = reverseValues;
	}catch (e) {
	// swallow- do nothing
	kony.print("layoutManager: This is Reverse Layout. Assigning master data in AS IS order because of exception " + e);
		widgetObj.masterData = values;
	}
}else {
	kony.print("layoutManager: This is Default Layout. Assigning master data in AS IS order");
	widgetObj.masterData = values;
}
}




layoutManager.getSliderSelectedValue = function(sliderObj){
	var currValue = sliderObj.selectedValue;
	if (!arLayoutManager.isDefaultLayout()) {
		var stepVal = sliderObj.step;
		var minVal = sliderObj.min;
		var maxVal = sliderObj.max;
		var reverseValue = maxVal - (currValue-minVal);
		kony.print("layoutManager: Reverse Layout. So returning reverse value: " + reverseValue);
		return reverseValue;
	} else {
		kony.print("layoutManager: Default Layout. So returning actual value: " + currValue);
		return currValue;
	}
}
layoutManager.updateSliderSelectedValue = function(sliderObj, newValue){
	var stepVal = sliderObj.step;
	var minVal = sliderObj.min;
	var maxVal = sliderObj.max;
	if (newValue<minVal || newValue>maxVal) {
		kony.print("Cannot udpate slider value. " + newValue +" not within min-max limits of the slider : " +minVal+"-"+maxVal);
		return;
	}
	if (!arLayoutManager.isDefaultLayout()) {
		var reverseValue = maxVal - (newValue-minVal);
		kony.print("layoutManager: Reverse Layout. So updating reverse value: " + reverseValue);
		sliderObj.selectedValue = reverseValue;
	} else {
		kony.print("layoutManager: Default Layout. So updating actual value: " + newValue);
		sliderObj.selectedValue = newValue;
	}
}




layoutManager.updatePageViewSegmentData = function(segmentObj, values) {
var widgetType = "";
try {
	widgetType = kony.type(segmentObj);
}catch (e) {	
	//swallow, nothing can be done
	kony.print("layoutManager: Cannot Update Segment Data. Error : " + e);
	return;
}
kony.print("layoutManager: Master Data update requested for widget type : " + widgetType + " values:" + JSON.stringify(values));
if (widgetType == "kony.ui.SegmentUI2") {
	kony.print("layoutManager: Master Data update supported for the segment/view");
} else {
	kony.print("layoutManager: Master Data update not needed for the widget type/view type");
	return;
}
kony.print("layoutManager: Checking -- Is Default Layout ? " + arLayoutManager.isDefaultLayout());
if (!arLayoutManager.isDefaultLayout()) {
	var reverseValues = [];
	try {
		for (var i=(values.length-1);i>=0;i--) {
			reverseValues.push(values[i]);
		}
	kony.print("layoutManager: This is Reverse Layout. Assigning master data in Reverse order");
	segmentObj.setData(reverseValues);
	}catch (e) {
	// swallow- do nothing
	kony.print("layoutManager: This is Reverse Layout. Assigning master data in AS IS order because of exception " + e);
		segmentObj.setData(values);
	}
}else {
	kony.print("layoutManager: This is Default Layout. Assigning master data in AS IS order");
	segmentObj.setData(values);
}
}




layoutManager.updateSliderLeftRightSkins = function(sliderObj, leftSkinValue, rightSkinValue) {
	if (!arLayoutManager.isDefaultLayout()) {
		kony.print("layoutManager: This is Reverse Layout. Assigning left/right skins in reverse order");
		sliderObj.leftSkin = rightSkinValue;
		sliderObj.rightSkin = leftSkinValue;
	}else {
		kony.print("layoutManager: This is Default Layout. Assigning left/right skins in AS IS order");
		sliderObj.leftSkin = leftSkinValue;
		sliderObj.rightSkin = rightSkinValue;
	} 
}


return layoutManager;
}


//Invoke the following function in the post app init - without fail
//This function is accessible from Event Editor
function layoutManagerInit() {
arLayoutManager = LAYOUTMANAGER.getInstance();
arLayoutManager.init();
}


// Invoke this when you want to switch to the Default Layout for which the App is designed.
// Example: Invoke this for switching to Left To Right (LTR) Layout (for English)
//This function is accessible from Event Editor
function switchToDefaultLayout(fnCallBack) {
arLayoutManager.switchToDefaultLayout(fnCallBack);
}


// Invoke this when you want to reverse the default layout of the App
// Example: Invoke this for switching to Right To Left (RTL) Layout (for Arabic)
//This function is accessible from Event Editor
function switchToArabicLayout(fnCallBack) {
arLayoutManager.switchToArabicLayout(fnCallBack);
}
