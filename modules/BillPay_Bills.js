var billType = "";
var data;
var objName = "", customVerbName = "";

function onClickPrePaidQuickPay(){
  frmNewBillKA.btnPrePaid.skin = "slButtonWhiteTab";
  frmNewBillKA.btnPostPaid.skin = "slButtonWhiteTabDisabled";
  frmNewBillKA.flxDueAmount.setVisibility(false);
  frmNewBillKA.flxMinimumAmount.setVisibility(false);
  frmNewBillKA.flxMaximumAmount.setVisibility(false);
  frmNewBillKA.flxIssueDate.setVisibility(false);
  frmNewBillKA.flxDueDate.setVisibility(false);
  frmNewBillKA.flxFee.setVisibility(false);
  frmNewBillKA.btnBillsPayAccounts.text = "t";
  frmNewBillKA.btnBillsPayCards.text = "s";
  //frmNewBillKA.flxTotalAmount.setVisibility(true);
  frmNewBillKA.tbxBillerNumber.text = "";
  frmNewBillKA.flxRadioAccCardsSelection.setVisibility(true);
  frmNewBillKA.flxPaymentMode.setVisibility(true);
  var denodata = kony.store.getItem("denoFlag");
  try{
    if(denodata.deno_flag == "true"){
      frmNewBillKA.flxDenomination.setVisibility(true);
    }
    else{
      frmNewBillKA.flxDenomination.setVisibility(false);
    }
    if(denodata.billing_no_flag === "true"){
      frmNewBillKA.flxBillerNumber.setVisibility(true);
    }
    else{
      frmNewBillKA.flxBillerNumber.setVisibility(false);
    }
    if(denodata.deno_flag == "false"){
      frmNewBillKA.flxAmount.setVisibility(true);
    }
    else{
      frmNewBillKA.flxAmount.setVisibility(false);
    }
  }
  catch(e){
    frmNewBillKA.flxDenomination.setVisibility(false);
    frmNewBillKA.flxBillerNumber.setVisibility(false);
    frmNewBillKA.flxAmount.setVisibility(false);
    exceptionLogCall("onClickPrePaidQuickPay","UI ERROR","UI",e);
  }
  frmNewBillKA.flxAmounttoPay.setVisibility(false);
  gblQuickFlow = "pre";
  //frmNewBillKA.tbxBillerNumber.text = "";
}

function onClickPostPaidQuickPay(){
  frmNewBillKA.btnPrePaid.skin = "slButtonWhiteTabDisabled";
  frmNewBillKA.btnPostPaid.skin = "slButtonWhiteTab";
  frmNewBillKA.flxDueAmount.setVisibility(false);
  frmNewBillKA.flxMinimumAmount.setVisibility(false);
  frmNewBillKA.flxMaximumAmount.setVisibility(false);
  frmNewBillKA.flxIssueDate.setVisibility(false);
  frmNewBillKA.flxDueDate.setVisibility(false);
  frmNewBillKA.flxFee.setVisibility(false);
  frmNewBillKA.flxTotalAmount.setVisibility(false);
  frmNewBillKA.flxDenomination.setVisibility(false);
  frmNewBillKA.flxAmounttoPay.setVisibility(false);
  //frmNewBillKA.flxAmount.setVisibility(true);
  frmNewBillKA.flxBillerNumber.setVisibility(true);
  gblQuickFlow = "post";
  frmNewBillKA.tbxBillerNumber.text = "";
  frmNewBillKA.flxDenomination.isVisible = false;

  frmNewBillKA.lblServiceType.text= kony.i18n.getLocalizedString("i18n.billsPay.ServiceType");
  frmNewBillKA.lblServiceType.skin = "sknLblNextDisabled";
  frmNewBillKA.flxAmount.setVisibility(false);
  frmNewBillKA.flxRadioAccCardsSelection.setVisibility(false);
  frmNewBillKA.flxPaymentMode.setVisibility(false);
}


function nextfrmPrePaidQuickPay(responseData)
{
   var x =kony.store.getItem("PrepaidResponse");
  kony.print("nextfrmPrePaidQuickPay::");
  frmConfirmPayBill.CopylblConfirmEmailTitle0ad38e28e71194e.text = geti18Value("i18n.bills.PaidAmount");//"Paid Amount";
  var data = kony.store.getItem("PrepaidInputs");
   if(data.deno_flag !== undefined && data.deno_flag === "true"){
    frmConfirmPayBill.flxDenomination.setVisibility(true);
    frmConfirmPayBill.flxDueAmount.setVisibility(false);
  }
  else{
    frmConfirmPayBill.flxDenomination.setVisibility(false);
    frmConfirmPayBill.flxAmount.setVisibility(true);
     frmConfirmPayBill.flxDueAmount.setVisibility(true);
  }
  frmConfirmPayBill.flxBillN.setVisibility(true);
  frmConfirmPayBill.flxServiceType.setVisibility(true);
  if(data.billing_no_flag === "false"){
    frmConfirmPayBill.flxBillerNumber.setVisibility(false);
  }
  else{
    frmConfirmPayBill.flxBillerNumber.setVisibility(true);
     frmConfirmPayBill.lblBillerNumber.setVisibility(true);
    frmConfirmPayBill.lblBillerNumber.text = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g,"");
  }
  var tAmount = null;
  if(frmNewBillKA.flxDenomination.isVisible === false)
  	tAmount = parseFloat(x.FeeOutput[0].fees) + parseFloat(frmNewBillKA.tbxAmount.text.replace(/,/g,""));
  else
    tAmount = parseFloat(responseData.p_debit_amt);
  //frmConfirmPayBill.lblTotalAmount.text = formatamountwithCurrency(responseData.p_debit_amt,kony.store.getItem("BillPayfromAcc").currencyCode)+" "+kony.store.getItem("BillPayfromAcc").currencyCode;
  frmConfirmPayBill.lblTotalAmount.text = formatamountwithCurrency(tAmount,CURRENCY)+" "+CURRENCY;
  frmConfirmPayBill.lblConfirmDueAmount.text = formatamountwithCurrency(responseData.p_debit_amt,CURRENCY)+" "+CURRENCY;
  frmConfirmPayBill.flxFeeAmount.setVisibility(true);
//   frmConfirmPayBill.flxTotalAmount.setVisibility(true);
  frmConfirmPayBill.flxDueAmount.setVisibility(true);
  frmConfirmPayBill.flxAmount.setVisibility(false);
  frmConfirmPayBill.flxSourceofFunding.setVisibility(false);
  frmConfirmPayBill.flxSourceofFundAccount.setVisibility(false);
  frmConfirmPayBill.forceLayout();
  frmConfirmPayBill.show();
}


function nextfrmPostPaidQuickPay(data)
{
  //   alert("Post");
  frmConfirmPayBill.flxDenomination.setVisibility(false);
  frmConfirmPayBill.CopylblConfirmEmailTitle0ad38e28e71194e.text = geti18Value("i18n.bills.PaidAmount");//"Paid Amount";
  frmConfirmPayBill.flxAmount.setVisibility(true);
  frmConfirmPayBill.flxBillN.setVisibility(true);
  frmConfirmPayBill.flxServiceType.setVisibility(true);
  frmConfirmPayBill.flxBillerNumber.setVisibility(true);
  frmConfirmPayBill.flxDueAmount.setVisibility(true);
  frmConfirmPayBill.flxFeeAmount.setVisibility(true);
  frmConfirmPayBill.flxTotalAmount.setVisibility(false);
  frmConfirmPayBill.flxAmount.setVisibility(true);
  frmConfirmPayBill.flxSourceofFunding.setVisibility(true);
  frmConfirmPayBill.flxSourceofFundAccount.setVisibility(true);
  loadDetailstoConfirmPostpaid(data);
}


function gotoPrepaidBillForm()
{
  frmBills.flxFee.setVisibility(false);
  frmBills.flxDenomination.setVisibility(true);
  frmBills.flxTotalAmount.setVisibility(false);
  frmBills.flxAmounttoPay.setVisibility(false);
  frmBills.lblTitle.text = geti18Value("i18n.billpayment.prepaidbills");
  frmBills.flxDueAmount.setVisibility(true);
  frmBills.flxMinimumAmount.setVisibility(false);
  frmBills.flxMaxAmount.setVisibility(false);
  frmBills.flxIssueDate.setVisibility(false);
  frmBills.flxDueDate.setVisibility(false);
  gblQuickFlow = "pre";
}

function gotoPostpaidBillForm()
{
  frmBills.flxDenomination.setVisibility(false);
  frmBills.flxFee.setVisibility(true);
  frmBills.flxTotalAmount.setVisibility(false);
  frmBills.flxAmounttoPay.setVisibility(false);
  frmBills.lblTitle.text = geti18Value("i18n.billpayment.postpaidbills");
  frmBills.flxDueAmount.setVisibility(true);
  frmBills.flxMinimumAmount.setVisibility(true);
  frmBills.flxMaxAmount.setVisibility(true);
  frmBills.flxIssueDate.setVisibility(true);
  frmBills.flxDueDate.setVisibility(true);
  frmBills.flxDueAmount.setVisibility(true);
  gblQuickFlow = "post";
}


function onClickServiceTypePrepaid()
{
  frmNewBillKA.flxPopupOuter.setVisibility(true);
}

function billerListPreShow(){
  if(gblBiller != "Denom"){
    frmBillerListForm.segDenom.setVisibility(false);
    frmBillerListForm.segDetails.setVisibility(true);
  }
  else{
    frmBillerListForm.segDenom.setVisibility(true);
    frmBillerListForm.segDetails.setVisibility(false);
  }

}


function onClickPopUpSegmentQuickPay()
{
  data = frmBillerListForm.segDetails.selectedRowItems[0].lblData;
  frmConfirmPayBill.lblConfirmDenomination.text = data;
  //alert(data);
  frmNewBillKA.lblServiceType.text= data;
  frmNewBillKA.lblServiceType.skin = "sknLblBack";

  if(data == "Mix Voucher"){
    data = "Denomination";
  }

  if(data !== undefined && data === "Denomination")
  {
    frmNewBillKA.flxDenomination.setVisibility(true);
    frmNewBillKA.flxAmounttoPay.setVisibility(false);
    frmNewBillKA.flxAmount.setVisibility(false);
    frmConfirmPayBill.flxDenomination.setVisibility(true);
    frmNewBillKA.flxBillerNumber.setVisibility(true);
  }
  else{
    frmNewBillKA.flxBillerNumber.setVisibility(false);
    frmNewBillKA.flxDenomination.setVisibility(false);
    frmNewBillKA.flxAmounttoPay.setVisibility(false);
    frmConfirmPayBill.flxDenomination.setVisibility(false);
    frmNewBillKA.flxDueAmount.setVisibility(false);
    frmNewBillKA.flxFee.setVisibility(false);
    frmNewBillKA.flxMinimumAmount.setVisibility(false);
    frmNewBillKA.flxMaximumAmount.setVisibility(false);
    frmNewBillKA.flxIssueDate.setVisibility(false);
    frmNewBillKA.flxDueDate.setVisibility(false);
    frmNewBillKA.flxBillerNumber.setVisibility(true);
  }
  frmNewBillKA.flxPopupOuter.setVisibility(false);
  frmNewBillKA.lblDenomination.text = geti18Value("i18n.billsPay.Denomination");
  frmNewBillKA.lblDenomination.skin = "sknLblNextDisabled";
  frmNewBillKA.show();

}


function onClickDenom(){
  var data = frmBillerListForm.segDenom.selectedRowItems[0].lblData;
  frmNewBillKA.lblDenomination.text = data;
  frmNewBillKA.lblDenomination.skin = "sknLblBack";
  frmConfirmPayBill.lblConfirmDenomination.text = data;
  onDoneBillerNum();
  frmNewBillKA.show();
} 

function onDoneBillerNum(){

  if(frmNewBillKA.tbxBillerNumber.text !== "" ){
    if(frmNewBillKA.lblDenomination.text == "Denomination" && gblQuickFlow == "pre" ){
      return;
    }
    if(data === "Denomination")
    {
      frmNewBillKA.flxDueAmount.setVisibility(false);
      frmNewBillKA.flxFee.setVisibility(false);
      frmNewBillKA.flxMinimumAmount.setVisibility(false);
      frmNewBillKA.flxMaximumAmount.setVisibility(false);
      frmNewBillKA.flxIssueDate.setVisibility(false);
      frmNewBillKA.flxDueDate.setVisibility(false);
    }
    else{
      frmNewBillKA.flxDueAmount.setVisibility(trues);
      frmNewBillKA.flxFee.setVisibility(false);
      frmNewBillKA.flxMinimumAmount.setVisibility(true);
      frmNewBillKA.flxMaximumAmount.setVisibility(true);
      frmNewBillKA.flxIssueDate.setVisibility(true);
      frmNewBillKA.flxDueDate.setVisibility(true);
    }

  }
  else{
    frmNewBillKA.flxDueAmount.setVisibility(false);
    frmNewBillKA.flxMinimumAmount.setVisibility(false);
    frmNewBillKA.flxMaximumAmount.setVisibility(false);
    frmNewBillKA.flxIssueDate.setVisibility(false);
    frmNewBillKA.flxDueDate.setVisibility(false);
  }
}


function prePaidNewBillDetails(res){
  frmNewBillDetails.lblBNumber.text = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g,"");
  frmNewBillDetails.flxServiceType.setVisibility(false);
  frmNewBillDetails.flxFee.setVisibility(false);
  frmNewBillDetails.btnPrePaid.skin = "slButtonWhiteTab";
  frmNewBillDetails.btnPostPaid.skin = "slButtonWhiteTabDisabled";
  frmNewBillDetails.flxMinimumAmount.setVisibility(false);
  frmNewBillDetails.flxMaximumAmount.setVisibility(false);
  frmNewBillDetails.flxIssueDate.setVisibility(false);
  frmNewBillDetails.flxDueAmount.setVisibility(true);
  frmNewBillDetails.flxDueDate.setVisibility(false);
  //   if(data !== undefined && data === "Denomination"){
  //     frmNewBillDetails.flxDenomination.setVisibility(false);
  //     frmNewBillDetails.lblDenomination.text = frmNewBillKA.lblDenomination.text;
  //     frmNewBillDetails.flxDueAmount.setVisibility(true);
  //     frmNewBillDetails.flxTotalAmount.setVisibility(false);
  //     frmNewBillDetails.flxAmount.setVisibility(false);
  //     frmNewBillDetails.flxAmounttoPay.setVisibility(false);
  //     frmNewBillDetails.flxAmount.setVisibility(false);
  //   }
  //   else{
  frmNewBillDetails.flxDenomination.setVisibility(false);
  frmNewBillDetails.flxDueAmount.setVisibility(true);
  frmNewBillDetails.flxTotalAmount.setVisibility(false);
  frmNewBillDetails.flxAmount.setVisibility(false);
  frmNewBillDetails.flxAmounttoPay.setVisibility(false);
  frmNewBillDetails.flxAmount.setVisibility(true);
  // }
  frmNewBillDetails.flxReadOnlyBillerName.setVisibility(false);
  frmNewBillDetails.flxReadOnlyBillerNumber.setVisibility(false);
  var currency = "";
  if(frmNewBillDetails.btnBillsPayAccounts.text === "t")
    currency = kony.store.getItem("BillPayfromAcc").currencyCode;
  else
    currency = kony.store.getItem("BillPayfromAcc").balance.split(" ")[1];
  frmNewBillDetails.lblDueAmount.text = formatamountwithCurrency(res.FeeOutput[0].DueAmount,currency === undefined?"JOD":currency) + " " + (currency === undefined?"JOD":currency);
  frmNewBillDetails.flxPaymentMode.setVisibility(false);
  frmNewBillDetails.flxRadioAccCardsSelection.setVisibility(false);
  frmNewBillDetails.flxAmount.setVisibility(false);
  frmNewBillDetails.show();
}

function postpaidNewBillDetails(){
  frmNewBillDetails.flxReadOnlyBillerName.setVisibility(false);
  frmNewBillDetails.flxReadOnlyBillerNumber.setVisibility(false);
  frmNewBillDetails.lblBNumber.text = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g,"");
  frmNewBillDetails.flxDenomination.setVisibility(false);
  frmNewBillDetails.flxlblReadOnlyServiceType.setVisibility(false);
  frmNewBillDetails.flxFee.setVisibility(true);
  frmNewBillDetails.btnPrePaid.skin = "slButtonWhiteTabDisabled";
  frmNewBillDetails.btnPostPaid.skin = "slButtonWhiteTab";
  frmNewBillDetails.flxMinimumAmount.setVisibility(true);
  frmNewBillDetails.flxMaximumAmount.setVisibility(true);
  frmNewBillDetails.flxIssueDate.setVisibility(true);
  frmNewBillDetails.flxDueDate.setVisibility(true);
  frmNewBillDetails.flxDueAmount.setVisibility(true);
  frmNewBillDetails.flxTotalAmount.setVisibility(false);
  frmNewBillDetails.flxAmount.setVisibility(true);
  frmNewBillDetails.flxAmounttoPay.setVisibility(false);
  frmNewBillDetails.flxPaymentMode.setVisibility(true);
  frmNewBillDetails.flxRadioAccCardsSelection.setVisibility(true);
  frmNewBillDetails.flxAmount.setVisibility(true);
  //   frmNewBillDetails.show();
  gblTModule = "BillPay";
  check_numberOfAccounts();
}


function populateDatatopostpaidNewBillDetails(res)
{
  var currencyCode = CURRENCY;
  /*if(kony.store.getItem("BillPayfromAcc") !== null && kony.store.getItem("BillPayfromAcc") !== undefined){
    if(frmNewBillDetails.btnBillsPayAccounts.text === "t")
      currencyCode = kony.store.getItem("BillPayfromAcc").currencyCode;
    else
      currencyCode = kony.store.getItem("BillPayfromAcc").balance.split(" ")[1];
  } 
  if(currencyCode === undefined || currencyCode === "")
    currencyCode = "JOD";*/
  frmNewBillDetails.lblHiddenDueAmount.text = res.BillerDetails[0].dueamount;
  frmNewBillDetails.lblBillerStatus.text = res.BillerDetails[0].billstatus;
  frmNewBillDetails.lblFeeonBiller.text = res.BillerDetails[0].feesonbiller;
  frmNewBillDetails.lblDueAmount.text = formatamountwithCurrency(res.BillerDetails[0].dueamount, currencyCode) +" "+currencyCode;
  if(res.BillerDetails[0].dueamount !== "" && parseFloat(res.BillerDetails[0].dueamount) !== 0){
  	frmNewBillDetails.tbxAmount.text = formatamountwithCurrency(res.BillerDetails[0].dueamount, currencyCode);
  }
  frmNewBillDetails.lblIssueDateNewBillDetails.text = formatDateBill(res.BillerDetails[0].issuedate);
  frmNewBillDetails.lblDueDateNewBillDetails.text = formatDateBill(res.BillerDetails[0].duedate);
  frmNewBillDetails.minAmount.text = formatamountwithCurrency(res.BillerDetails[0].lower,currencyCode)+" "+currencyCode;
  frmNewBillDetails.maxAmount.text = formatamountwithCurrency(res.BillerDetails[0].upper,currencyCode)+" "+currencyCode;
//   if(res.feesonbiller !== "false")
    frmNewBillDetails.lblFee.text = formatamountwithCurrency(res.BillerDetails[0].feesamt, currencyCode)+" "+currencyCode;
//   else
//     frmNewBillDetails.lblFee.text = 0;
}




function populateDatattoConfirmPostpaid()
{
  frmConfirmPayBill.lblServiceType.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedType.serv_type_desc;	
}


function onChangingradioBtn(field, target)
{
  if(field.text === "s")
    field.text = "t";
  if(target === geti18nkey("i18n.billsPay.Accounts"))
  {
    frmNewBillDetails.btnBillsPayCards.text = "s";
    frmNewBillDetails.lblPaymentMode.text = geti18nkey("i18n.billsPay.Accounts");
    frmNewBillDetails.flxPaymentModeTypeHolder.setEnabled(true);
  }
  else{
    frmNewBillDetails.btnBillsPayAccounts.text = "s";
    frmNewBillDetails.lblPaymentMode.text = geti18nkey("i18n.billsPay.Cards");
    frmNewBillDetails.flxPaymentModeTypeHolder.setEnabled(true);
    if(kony.retailBanking.globalData.prCardLandinList.length === 0)
      customVerb_CARDSDETAILS();
  }

  //   var selected = frmNewBillDetails.RadioBtnAccCards.selectedKeyValue;
  //   if(selected[0]== 1)
  //   {
  //     frmNewBillDetails.lblPaymentMode.text = geti18nkey("i18n.billsPay.Accounts");
  //     frmNewBillDetails.flxPaymentModeTypeHolder.setEnabled(true);
  //   }
  //   else{
  //     frmNewBillDetails.lblPaymentMode.text = geti18nkey("i18n.billsPay.Cards");
  //     frmNewBillDetails.flxPaymentModeTypeHolder.setEnabled(false);
  //   }
}


function assignDatatoConfirmPostpaid()
{
  var billingNumber = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g,"");
  var acc = kony.store.getItem("BillPayfromAcc");
  if(frmNewBillDetails.btnBillsPayAccounts.text === "t"){
    frmConfirmPayBill.lblAccountNumber.text = acc.accountID;
    frmConfirmPayBill.lblTransferFlag.text = "E";
    frmConfirmPayBill.lblAccBr.text = acc.branchNumber;
    frmConfirmPayBill.lblCurrencyType.text = acc.ccydesc;
    frmConfirmPayBill.lblPaymentMethod.text = "ACTDEB";
  }
  else{
    frmConfirmPayBill.lblPaymentMethod.text = "CCARD";
    frmConfirmPayBill.lblAccountNumber.text = acc.card_num;
    //frmConfirmPayBill.lblCreditCardNumber.text = acc.card_num;
    frmConfirmPayBill.lblTransferFlag.text = "EC";
    frmConfirmPayBill.lblAccBr.text = "";
    frmConfirmPayBill.lblCurrencyType.text = acc.balance.split(" ")[1];
  }

  frmConfirmPayBill.lblBillerCode.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code;
  frmConfirmPayBill.lblCustID.text = custid;
  frmConfirmPayBill.lblLang.text = "ara";
  frmConfirmPayBill.lblSvcType.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedType.code;
  frmConfirmPayBill.lblBillingNumber.text = billingNumber;
  var amount = "";
//   if(acc.currencyCode !== "JOD"){
//     amount = frmNewBillDetails.lblVal.text.substring(0,frmNewBillDetails.lblVal.text.length-4);
//   	frmConfirmPayBill.lblPaidAmnt.text = amount.replace(/,/g,"");
//   }else{
  	frmConfirmPayBill.lblPaidAmnt.text = frmNewBillDetails.tbxAmount.text.replace(/,/g,"");
//   }
  amount = "";
  if(frmNewBillDetails.btnBillsPayAccounts.text === "t")
    if (kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD"){
  //      frmConfirmPayBill.flxLocalAmount.isVisible=true;
      amount = frmNewBillDetails.lblVal.text.replace(/,/g,"");
    }else{
       frmConfirmPayBill.flxLocalAmount.isVisible=false;
      amount = frmNewBillDetails.tbxAmount.text.replace(/,/g,"");
    }
  var pay_CURRENCY = isEmpty(kony.store.getItem("BillPayfromAcc").currencyCode)?"JOD":kony.store.getItem("BillPayfromAcc").currencyCode;
  frmConfirmPayBill.lblPaidAmount.text = formatamountwithCurrency(amount,pay_CURRENCY)+ " " +pay_CURRENCY; // hassan
  frmConfirmPayBill.lblDueAmnt.text = frmNewBillDetails.lblHiddenDueAmount.text;
  frmConfirmPayBill.lblAccessChannel.text = "MOBILE";
  frmConfirmPayBill.lblSendSMSflag.text = "Y";
  frmConfirmPayBill.lblCustInfoFlag.text = "Y";
  frmConfirmPayBill.lblPaymentStatus.text = frmNewBillDetails.lblBillerStatus.text;
  frmConfirmPayBill.lblFeeAmnt.text = frmNewBillDetails.lblFee.text.substring(0, frmNewBillDetails.lblFee.text.length-4);
  frmConfirmPayBill.lblFeeOnBiller.text = frmNewBillDetails.lblFeeonBiller.text;
//   frmConfirmPayBill.lblTransferFlag.text = "E";
  frmConfirmPayBill.lblBillNo.text = billingNumber;
  frmConfirmPayBill.lblfcbdrefNo.text = "";
  frmConfirmPayBill.lblPID.text = "9861033442";
  frmConfirmPayBill.lblMobileNumber.text = "962797545415";
  frmConfirmPayBill.pidType.text = "NAT";
  frmConfirmPayBill.pNation.text = "JO";
}



function loadDetailstoConfirmPostpaid(data)
{
  kony.print("Data ::"+JSON.stringify(data));
  var acc = kony.store.getItem("BillPayfromAcc");
  var currForm = kony.application.getCurrentForm();
  //var amount = currForm["tbxAmount"].text;  // hassan
  var amount, curCode = "JOD";
  if(currForm.btnBillsPayAccounts.text === "t")
    if (kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD"){
         amount = currForm["lblVal"].text;
    	 curCode = kony.store.getItem("BillPayfromAcc").currencyCode;
    }else
         amount = currForm["tbxAmount"].text;
  else
       amount = currForm["tbxAmount"].text;
  
  //1967 fix
  kony.print("loadDetailstoConfirmPostpaid");
  kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory = {"code_desc":frmNewBillKA.lblBillerName.text};
  kony.print("nickname is empty *"+kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc);
  //end
  frmConfirmPayBill.lblBillerCategory.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc;
  frmConfirmPayBill.initialsCategory.text = getInitials(kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc);
  frmConfirmPayBill.lblBillerName.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code_desc;
  frmConfirmPayBill.lblServiceType.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedType.code;
  frmConfirmPayBill.lblBillerNumber.text = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g,"");//"";//1982 fix //currForm.tbxBillerNumber.text.replace(/\s/g,"");
  frmConfirmPayBill.lblConfirmDueAmount.text = setDecimal(currForm.lblDueAmount.text.replace(/,/g,""),3) + " JOD";
  frmConfirmPayBill.lblFeeAmount.text = formatamountwithCurrency(currForm.lblFee.text,"JOD")+" JOD";
  //frmConfirmPayBill.lblPaidAmount.text = formatamountwithCurrency(amount,"JOD")+" JOD";  // hassan
  frmConfirmPayBill.lblPaidAmount.text = formatamountwithCurrency(amount,curCode)+ " " +curCode; // hassan11
  //frmConfirmPayBill.lblTotalAmount.text = formatamountwithCurrency(parseFloat(data.p_debit_amt),"JOD")+" JOD"; // hassan 
  frmConfirmPayBill.lblTotalAmount.text = formatamountwithCurrency(parseFloat(data.p_debit_amt),curCode)+" " + curCode;
  frmConfirmPayBill.flxLocalAmount.isVisble=false;

  if(currForm.btnBillsPayAccounts.text === "t")
    if (kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD"){
  //      frmConfirmPayBill.flxLocalAmount.isVisble=true;
       frmConfirmPayBill.lblVal.text = formatamountwithCurrency(currForm.tbxAmount.text,"JOD")+" JOD"; // hassan 
    }

  if(currForm.btnBillsPayAccounts.text === "t"){
    if(acc.AccNickName !== undefined && acc.AccNickName !== ""){
    	frmConfirmPayBill.lblSOFAccount.text = acc.AccNickName;
    	frmConfirmPayBill.lblSOFAccNum.setVisibility(false);
    }else{
    	frmConfirmPayBill.lblSOFAccount.text = acc.accountName;
    	frmConfirmPayBill.lblSOFAccNum.setVisibility(true);
    }
    frmConfirmPayBill.lblSOFAccNum.text =  acc.hiddenAccountNumber;
  }
  else{
    frmConfirmPayBill.lblSOFAccount.text = acc.name_on_card;
    frmConfirmPayBill.lblSOFAccNum.text =  acc.hiddenAccountNumber;
  }
  frmConfirmPayBill.show();
}


function nextFromNewDetailsScreen()
{
  if(gblQuickFlow === "post")
  {
    if(frmNewBillDetails.lblPaymentMode.text !== geti18nkey("i18n.billsPay.Accounts") && frmNewBillDetails.lblPaymentMode.text !== geti18nkey("i18n.billsPay.Cards") && frmNewBillDetails.tbxAmount.text !=0 && frmNewBillDetails.tbxAmount.text!= "" && frmNewBillDetails.tbxAmount.text!==".")
    {
      frmNewBillDetails.lblNext.skin = "sknLblNextEnabled";
      return 1;
    }
    else
    {
      frmNewBillDetails.lblNext.skin = "sknLblNextDisabled";
      return 0;
    }
  }
  else{
    frmNewBillDetails.lblNext.skin = "sknLblNextEnabled";
    return 1;
  }
}

function resetDropdownBiller(name){
  var tbx = "tbx" + name;
  var lbl = "lbl" + name;
  var line = "flxUnderline" + name;

  frmNewBillKA[tbx].text = "";
  frmNewBillKA[lbl].skin = "sknLblNextDisabled";
  frmNewBillKA[line].skin = "sknFlxGreyLine";

  switch(name){
    case "BillerCategory":frmNewBillKA[lbl].text = geti18Value("i18n.billsPay.BillerCategory");
      break;
    case "BillerName":frmNewBillKA[lbl].text = geti18Value("i18n.billsPay.billerName");
      break;
    case "ServiceType":frmNewBillKA[lbl].text = geti18Value("i18n.billsPay.ServiceType");
      break;
    case "Denomination":frmNewBillKA[lbl].text = geti18Value("i18n.billsPay.Denomination");
      break;
  }
}

function resetBiller(){
  resetDropdownBiller("BillerCategory");

  resetDropdownBiller("BillerName");

  resetDropdownBiller("ServiceType");

  resetDropdownBiller("Denomination");

  frmNewBillKA.tbxBillerNumber.text = "";
  frmNewBillKA.flxUnderlineBillerNumber.skin = "sknFlxGreyLine";
}

function restoreBillerTabDetails(){
  var categoryList = [["GetEfwBillerCategory", "selectedBillerCategory", "BillerCategory"],
                      ["GetEfwBillerList", "selectedBiller", "BillerName"],];

  if(kony.boj.selectedBillerType === "PostPaid")
    categoryList.push(["GetEfwServiceType", "selectedType", "ServiceType"]);
  else if(kony.boj.selectedBillerType === "PrePaid")
    categoryList.push(["GetPPServiceType", "selectedType", "ServiceType"]);
  categoryList.push(["Denomination", "selectedDenoType", "Denomination"]);
	frmNewBillKA.lblserviceTypeHint.text = "";
  for(var i in categoryList){
    //     alert(kony.boj.selectedBillerType);
    //     alert(JSON.stringify(kony.boj.Biller[kony.boj.selectedBillerType][categoryList[i][1]]));
    if(JSON.stringify(kony.boj.Biller[kony.boj.selectedBillerType][categoryList[i][1]]) != "{}"){
      kony.boj.BillerTabSwitch = true;
      kony.boj.Biller[kony.boj.selectedBillerType].Operation = categoryList[i][0];
      kony.boj.onSegmentClickBiller(kony.boj.Biller[kony.boj.selectedBillerType][categoryList[i][1]]);
    }
    else
      resetDropdownBiller(categoryList[i][2]);
  }
}



function assignDatatoConfirmPrepaid()
{
  var x =kony.store.getItem("PrepaidResponse");
  //   alert("x--->"+JSON.stringify(x));
  //   kony.print("x--->"+JSON.stringify(x));
  var res = kony.store.getItem("PrepaidResponse");
  var inputs = kony.store.getItem("PrepaidInputs");
  var billingNumber = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g,"");
  var acc = kony.store.getItem("BillPayfromAcc");	
	kony.print("inputs ::"+JSON.stringify(inputs));
  //if(frmNewBillDetails.btnBillsPayAccounts.text === "t"){// hassan pay from credit card
  if(frmNewBillKA.btnBillsPayAccounts.text === "t"){// hassan pay from credit card 
    frmConfirmPayBill.prAccorCCNo.text = acc.accountID;
    frmConfirmPayBill.lblAccountNumber.text = acc.accountID;
    frmConfirmPayBill.prAccBr.text = acc.branchNumber;
    frmConfirmPayBill.lblPaymentMethod.text = "ACTDEB";
  }
  else{
    frmConfirmPayBill.prAccorCCNo.text = acc.card_num;
    //frmConfirmPayBill.lblAccountNumber.text = acc.card_num;
    //frmConfirmPayBill.lblCreditCardNumber.text = acc.card_num; // hassan pay from credit card
    frmConfirmPayBill.prAccBr.text = "";
    frmConfirmPayBill.lblPaymentMethod.text = "CCARD";
  }

  frmConfirmPayBill.prBillerCode.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code;
  frmConfirmPayBill.prServiceCode.text = inputs.serv_type_code;
  if(inputs.billing_no_flag == "false")
    frmConfirmPayBill.prBillingNo.text = "";
  else
    frmConfirmPayBill.prBillingNo.text = billingNumber;
  if(inputs.deno_flag == "false")
    frmConfirmPayBill.prdenoFlag.text = "";
  else
    frmConfirmPayBill.prdenoFlag.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedDenoType.deno_code;
  frmConfirmPayBill.prValidationCode.text = x.FeeOutput[0].ValidationCode;
  frmConfirmPayBill.prDueAmount.text = x.FeeOutput[0].DueAmount;
  frmConfirmPayBill.prMobileNo.text = "0797545415";//"0797545415";
  frmConfirmPayBill.prPaymentType.text = "A";
  frmConfirmPayBill.lblTransferFlag.text = "P";

  frmConfirmPayBill.lblBillerCode.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code;
  frmConfirmPayBill.lblCustID.text = custid;
  frmConfirmPayBill.lblLang.text = "ara";
  frmConfirmPayBill.lblAccountNumber.text = acc.accountID;
  frmConfirmPayBill.lblAccBr.text = acc.branchNumber;
  frmConfirmPayBill.lblSvcType.text = inputs.serv_type_code;
  frmConfirmPayBill.lblBillingNumber.text = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g,"");
  frmConfirmPayBill.lblPaidAmnt.text = x.p_debit_amt;
  frmConfirmPayBill.lblDueAmnt.text = "";
  frmConfirmPayBill.lblAccessChannel.text = "MOBILE";
  //Change when we have cards service
  frmConfirmPayBill.lblSendSMSflag.text = "N";
  frmConfirmPayBill.lblCustInfoFlag.text = "Y";
  frmConfirmPayBill.lblPaymentStatus.text = "BillNew";
  frmConfirmPayBill.lblFeeAmnt.text = x.FeeOutput[0].fees;
  frmConfirmPayBill.lblFeeOnBiller.text = "";
  frmConfirmPayBill.lblBillNo.text = "";
  frmConfirmPayBill.lblfcbdrefNo.text = "";
  frmConfirmPayBill.lblPID.text = "";
  frmConfirmPayBill.lblMobileNumber.text = "";
  frmConfirmPayBill.pidType.text = "";
  frmConfirmPayBill.pNation.text = "";
  //   alert("ValidationCode "+x.FeeOutput[0].ValidationCode);
  //   alert("lblValidationCode "+frmConfirmPayBill.prValidationCode.text);
  loadDetailstoConfirmPrepaid();
}

function denominationVerification()
{
  var deno = kony.store.getItem("denoArray");
  var selectedService = frmBillerListForm.segDetails.selectedRowItems[0];
  kony.print("denomination Array"+ JSON.stringify(deno));
  kony.print("selected Service"+ JSON.stringify(selectedService));
  var prePaidInputs = {};
  var denodata = kony.store.getItem("denoFlag");
  prePaidInputs.serv_type_code = selectedService.serv_type_code;
  prePaidInputs.billing_no_flag = selectedService.billing_no_flag;
  prePaidInputs.serv_type_code = selectedService.serv_type_code;
  if(selectedService.deno_flag == "false")
  {
    denodata.deno_flag = "false";
    prePaidInputs.deno_flag = "false";
    kony.print("Entered Prepaid flow without denomination");
    if(selectedService.billing_no_flag == "true")
    {
      frmNewBillKA.flxBillerNumber.setVisibility(true);
      if (kony.boj.selectedBillerType === "PrePaid")
          frmAddNewPrePayeeKA.flxBillerNumber.setVisibility(true); //  hassan 28/05/2019
    }
    else
    {
      frmNewBillKA.flxBillerNumber.setVisibility(false);
      if (kony.boj.selectedBillerType === "PrePaid")
         frmAddNewPrePayeeKA.flxBillerNumber.setVisibility(false); //  hassan 28/05/2019
    }
    frmNewBillKA.flxAmount.setVisibility(true);
    frmNewBillKA.flxDenomination.setVisibility(false);
    if (kony.boj.selectedBillerType === "PrePaid")
       frmAddNewPrePayeeKA.flxAmount.setVisibility(true);
       frmAddNewPrePayeeKA.flxDenomination.setVisibility(false); //  hassan 28/05/2019
  }
  else{
    denodata.deno_flag = "true";
    prePaidInputs.deno_flag = "true";
    prePaidInputs.deno_desc = deno.deno_desc;
    kony.print("Entered denomination flow");
    if(selectedService.billing_no_flag == "true"){
      frmNewBillKA.flxBillerNumber.setVisibility(true);
      if (kony.boj.selectedBillerType == "PrePaid")
         frmAddNewPrePayeeKA.flxBillerNumber.setVisibility(true); //  hassan 28/05/2019
    }
    else{
      frmNewBillKA.flxBillerNumber.setVisibility(false);
      if (kony.boj.selectedBillerType == "PrePaid")
         frmAddNewPrePayeeKA.flxBillerNumber.setVisibility(false); //  hassan 28/05/2019
    }
    prePaidInputs.billing_no_flag = selectedService.billing_no_flag;
    var items = deno.bs_boj_efw_pp_denos_ttypeType;
    kony.print("denomination array items "+ JSON.stringify(items));
    var data = [];
    for(var i=0; i< items.length;i++)
    {
      kony.print("item name"+items[i].serv_type_code +"selectedService "+ selectedService.serv_type_code + "denomination "+ items[i].deno_desc);
      if(items[i].serv_type_code === selectedService.serv_type_code && items[i].deno_desc != undefined){
        data.push(items[i]);
      }
    }
    kony.boj.Biller.Denomination  = data;
    frmNewBillKA.flxAmount.setVisibility(false);
    frmNewBillKA.flxDenomination.setVisibility(true);
    if (kony.boj.selectedBillerType == "PrePaid")
       frmAddNewPrePayeeKA.flxAmount.setVisibility(false);
       frmAddNewPrePayeeKA.flxDenomination.setVisibility(true); //  hassan 28/05/2019
    kony.print("data obtained" + JSON.stringify(data));
  }
  denodata.billing_no_flag = selectedService.billing_no_flag;
  var storeDataprePaidInputs = isEmpty(prePaidInputs) ? "" : prePaidInputs;
  var storeDatadenodata = isEmpty(denodata) ? "" : denodata;
  kony.store.setItem("PrepaidInputs",storeDataprePaidInputs);
  kony.store.setItem("denoFlag",storeDatadenodata);
}


function nextFromNewBillScreen()
{
  if( frmNewBillKA.lblBillerCategory.text != geti18Value("i18n.billsPay.BillerCategory") && frmNewBillKA.lblBillerName.text != geti18Value("i18n.billsPay.billerName") &&frmNewBillKA.lblServiceType.text!= geti18Value("i18n.billsPay.ServiceType"))
  {
    var w,x,y,z;
    if(frmNewBillKA.flxBillerNumber.isVisible === true){
      if(frmNewBillKA.tbxBillerNumber.text === "")
        x=0;
      else
        x=1;
    }
    else{
      x=1;
    }

    if(frmNewBillKA.flxDenomination.isVisible === true){
      if(frmNewBillKA.lblDenomination.text === geti18Value("i18n.billsPay.Denomination"))
        y=0;
      else
        y=1;
    }
    else{
      y=1;
    }

    if(frmNewBillKA.flxAmount.isVisible === true){
      if(frmNewBillKA.tbxAmount.text === "")
        z=0;
      else
        z=1;
    }
    else{
      z=1;
    }

    if(frmNewBillKA.flxPaymentMode.isVisible === true){
      if(frmNewBillKA.lblPaymentMode.text === geti18nkey("i18n.billsPay.Accounts")|| frmNewBillKA.lblPaymentMode.text === geti18nkey("i18n.billsPay.Cards"))
        w=0;
      else
        w=1;
    	if(frmNewBillKA.flxAmount.isVisible === true){
          if(frmNewBillKA.tbxAmount.text !== "." && parseFloat(frmNewBillKA.tbxAmount.text) > 0){
            w=1;
          }else w=0;
        }
    }
    else{
      w=1;
    }
	
    if(w&x&y&z){ 
      frmNewBillKA.lblNext.skin = "sknLblNextEnabled";
      frmNewBillKA.lblNext.setEnabled(true); 
    }
    else{
      frmNewBillKA.lblNext.skin = "sknLblNextDisabled";
      frmNewBillKA.lblNext.setEnabled(false);
    }

  }
  else{
    frmNewBillKA.lblNext.skin = "sknLblNextDisabled";
    frmNewBillKA.lblNext.setEnabled(false);
  }
}

function getInitials(Account){
  try{
    var str1 = Account;
    str1=str1.split(" ");
    var s1=str1[0].toUpperCase();
    var s2=str1[1].toUpperCase();
    return (s1[0]+s2[0]);
  }
  catch(e){
    var str1 = Account.substring(0, 2).toUpperCase();
    return str1;
  }
}


function loadDetailstoConfirmPrepaid()
{
  //1967 fix
  kony.print("loadDetailstoConfirmPrepaid");
  kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory = {"code_desc": frmNewBillKA.lblBillerName.text};
  kony.print("nickname is empty *"+kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc);
  //end
	
  var acc = kony.store.getItem("BillPayfromAcc");
  var amntres =  kony.store.getItem("PrepaidResponse");
  var deno = kony.store.getItem("PrepaidInputs");
  frmConfirmPayBill.flxImpDetails.setVisibility(true);
  kony.print("gblQuickFlow is "+gblQuickFlow);
  if (gblQuickFlow !== "preBene"){ // hassan
  frmConfirmPayBill.lblBillerCategory.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc;
  frmConfirmPayBill.initialsCategory.text = getInitials(kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc);
  frmConfirmPayBill.lblBillerName.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code_desc;
  frmConfirmPayBill.lblServiceType.text = frmNewBillKA.lblServiceType.text;
  frmConfirmPayBill.lblBillerNumber.text = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g,"");
  }
  else{
    frmConfirmPayBill.flxImpDetails.setVisibility(false);
    frmConfirmPayBill.lblBillerCategory.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc;
    frmConfirmPayBill.initialsCategory.text = getInitials(kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc);
    frmConfirmPayBill.lblBillerName.text = frmPrePaidPayeeDetailsKA.txtBillerName.text;
    frmConfirmPayBill.lblServiceType.text = frmPrePaidPayeeDetailsKA.txtServiceType.text;
    frmConfirmPayBill.lblBillerNumber.text = frmPrePaidPayeeDetailsKA.txtBillerCode;
  }
  frmConfirmPayBill.lblConfirmDueAmount.text = setDecimal(frmNewBillKA.tbxAmount.text.replace(/,/g,""), 3) + " JOD";
  frmConfirmPayBill.lblFeeAmount.text = setDecimal(amntres.FeeOutput[0].fees, 3) + " JOD";
//   frmConfirmPayBill.lblTotalAmount.text = setDecimal(amntres.FeeOutput[0].DueAmount,3) + " JOD";
  frmConfirmPayBill.lblCurrencyType.text = "JOD";
  //frmConfirmPayBill.flxDueAmount.setVisibility(false);
  if(deno.billing_no_flag === "false"){
    frmConfirmPayBill.flxBillerNumber.setVisibility(false);
  }
  else{
    frmConfirmPayBill.flxBillerNumber.setVisibility(true);
    frmConfirmPayBill.lblBillerNumber.setVisibility(true);
    if (gblQuickFlow !== "preBene") // hassan 
    frmConfirmPayBill.lblBillerNumber.text = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g,"");
    else
      frmConfirmPayBill.lblBillerNumber.text =frmPrePaidPayeeDetailsKA.txtBillingNumber.text;
  }
   if(deno.deno_flag === "false")
    frmConfirmPayBill.flxDenomination.setVisibility(false);
  else{
    frmConfirmPayBill.flxDenomination.setVisibility(true);
    if (gblQuickFlow !== "preBene") // hassan 
    frmConfirmPayBill.lblConfirmDenomination.text = frmNewBillKA.lblDenomination.text;
    else
      frmConfirmPayBill.lblConfirmDenomination.text = frmPrePaidPayeeDetailsKA.txtDenomination.text;
  }
  if (gblQuickFlow !== "preBene") // hassan 
     frmConfirmPayBill.forceLayout();
  else
     frmConfirmPayBill.show();
}


function setDecimal(x, limit){
  if(x===""||x===null||x===undefined)
    x = 0;
    //return "";
  var t = Number.parseFloat(x).toFixed(limit);
  var parts=t.toString().split(".");
  var y= parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
  return y;
}

function onChangingradioBtnNewBillKA(field, target)
{
  //   var selected = frmNewBillKA.RadioBtnAccCards.selectedKeyValue;
  var curForm = kony.application.getCurrentForm();
  if(field.text === "s")
    field.text = "t";
  if(target === geti18nkey("i18n.billsPay.Accounts"))
  {
    curForm.btnBillsPayCards.text = "s";
    curForm.lblPaymentMode.text = geti18nkey("i18n.billsPay.Accounts");
    curForm.flxPaymentModeTypeHolder.setEnabled(true);
  }
  else{
    curForm.btnBillsPayAccounts.text = "s";
    curForm.lblPaymentMode.text = geti18nkey("i18n.billsPay.Cards");
    curForm.flxPaymentModeTypeHolder.setEnabled(true);
    if(kony.retailBanking.globalData.prCardLandinList.length === 0)
      customVerb_CARDSDETAILS();
  }
}

function nextfrmPrePaidBill()
{
  frmConfirmPayBill.CopylblConfirmEmailTitle0ad38e28e71194e.text = geti18Value("i18n.bills.PaidAmount");//"Paid Amount";
   kony.print("nextfrmPrePaidBill::")
  if(data !== undefined && data === "Denomination"){
    frmConfirmPayBill.flxDenomination.setVisibility(true);
    frmConfirmPayBill.flxDueAmount.setVisibility(true);
  }
  else{
    frmConfirmPayBill.flxDenomination.setVisibility(false);
    frmConfirmPayBill.flxAmount.setVisibility(true);
  }
  frmConfirmPayBill.flxBillN.setVisibility(true);
  frmConfirmPayBill.flxServiceType.setVisibility(true);
  frmConfirmPayBill.flxBillerNumber.setVisibility(true);
  frmConfirmPayBill.flxDueAmount.setVisibility(true);
  frmConfirmPayBill.flxFeeAmount.setVisibility(true);
  frmConfirmPayBill.flxTotalAmount.setVisibility(true);
  frmConfirmPayBill.flxAmount.setVisibility(false);
  frmConfirmPayBill.flxSourceofFunding.setVisibility(false);
  frmConfirmPayBill.flxSourceofFundAccount.setVisibility(false);
  //   frmConfirmPayBill.show();
}


function nextfrmPostPaidBill()
{
  //   alert("Post");
  frmConfirmPayBill.flxDenomination.setVisibility(false);
  frmConfirmPayBill.CopylblConfirmEmailTitle0ad38e28e71194e.text = geti18Value("i18n.bills.PaidAmount");//"Paid Amount";
  frmConfirmPayBill.flxAmount.setVisibility(true);
  frmConfirmPayBill.flxBillN.setVisibility(true);
  frmConfirmPayBill.flxServiceType.setVisibility(true);
  frmConfirmPayBill.flxBillerNumber.setVisibility(true);
  frmConfirmPayBill.flxDueAmount.setVisibility(true);
  frmConfirmPayBill.flxFeeAmount.setVisibility(true);
  frmConfirmPayBill.flxTotalAmount.setVisibility(false);
  frmConfirmPayBill.flxAmount.setVisibility(true);
  frmConfirmPayBill.flxSourceofFunding.setVisibility(true);
  frmConfirmPayBill.flxSourceofFundAccount.setVisibility(true);
  //   loadDetailstoConfirmPostpaid();
  //   frmConfirmPayBill.show();
}



function clearNewDetailsScreen()
{
  frmNewBillDetails.tbxAmount.text = "";
  frmNewBillDetails.lblHiddenAmount.text = "";
  frmNewBillDetails.lblPaymentMode.text = geti18nkey("i18n.billsPay.Accounts");
}

function clearNewBillScreen()
{
  frmNewBillKA.tbxAmount.text = "";
  frmNewBillKA.lblPaymentMode.text = geti18nkey("i18n.billsPay.Accounts");
  frmNewBillKA.lblHiddenAmount.text = "";
}

function clearfrmBills()
{
  frmBills.btnBillsPayCards.text = "s";
  frmBills.btnBillsPayAccounts.text = "t";
//   frmBills.tbxAmount.text = "";
  frmBills.lblPaymentMode.text = geti18nkey("i18n.billsPay.Accounts");
  frmBills.lblHiddenAmnt.text = "";
}



function onChangingradioBtnfrmBills(field, target)
{
  if(field.text === "s")
    field.text = "t";
  if(target === geti18nkey("i18n.billsPay.Accounts"))
  {
    frmBills.btnBillsPayCards.text = "s";
    frmBills.lblPaymentMode.text = geti18nkey("i18n.billsPay.Accounts");
    frmBills.flxPaymentModeTypeHolder.setEnabled(true);
  }
  else{
    frmBills.btnBillsPayAccounts.text = "s";
    frmBills.lblPaymentMode.text = geti18nkey("i18n.billsPay.Cards");
    frmBills.flxPaymentModeTypeHolder.setEnabled(true);
    if(kony.retailBanking.globalData.prCardLandinList.length === 0)
      customVerb_CARDSDETAILS();
  }

  //   var selected = frmBills.RadioBtnAccCards.selectedKeyValue;
  //   if(selected[0]== 1)
  //   {
  //     frmBills.lblPaymentMode.text = geti18nkey("i18n.billsPay.Accounts");
  //     frmBills.flxPaymentModeTypeHolder.setEnabled(true);
  //   }
  //   else{
  //     frmBills.lblPaymentMode.text = geti18nkey("i18n.billsPay.Cards");
  //     frmBills.flxPaymentModeTypeHolder.setEnabled(false);
  //   }
}


function validateFrmBills()
{
  frmBills.lblUnderline.skin = "sknFlxGreyLine";
  var z,w;
  if(frmBills.flxAmount.isVisible === true){
    if(frmBills.tbxAmount.text === "")
      z=0;
    else
      z=1;
  }
  else{
    z=1;
  }

  if(frmBills.flxPaymentMode.isVisible === true){
    if(frmBills.lblPaymentMode.text === geti18nkey("i18n.billsPay.Accounts")|| frmBills.lblPaymentMode.text === geti18nkey("i18n.billsPay.Cards"))
      w=0;
    else
      w=1;
  }
  else{
    w=1;
  }
  if(z&w){
    frmBills.btnPayNow.setEnabled(true);
  }
 /* else{
    frmBills.btnPayNow.setEnabled(false);
  }*/
}


function changeUnderlineskin()
{
  if(frmNewBillKA.tbxBillerNumber.text == "")
    frmNewBillKA.flxUnderlineBillerNumber.skin = "sknFlxGreyLine";
  else
    frmNewBillKA.flxUnderlineBillerNumber.skin = "sknFlxGreenLine";
  if(frmNewBillKA.tbxAmount.text == "")
    frmNewBillKA.lblUnderline.skin = "sknFlxGreyLine";
  else
    frmNewBillKA.lblUnderline.skin = "sknFlxGreenLine";
  if(frmNewBillKA.lblPaymentMode.text !=geti18nkey("i18n.billsPay.Accounts") && frmNewBillKA.lblPaymentMode.text != geti18nkey("i18n.billsPay.Cards"))
    frmNewBillKA.flxUnderlinePaymentMode.skin = "sknFlxGreenLine";
  else
    frmNewBillKA.flxUnderlinePaymentMode.skin = "sknFlxGreyLine";
}




function formatDateBill(transactionDate)
{
  var x = transactionDate.substring(8,transactionDate.length)+"/"+transactionDate.substring(5,7)+"/"+transactionDate.substring(0,4);
  return x;
}


function formatDateMMDDYY(transactionDate){
  var x = transactionDate.substring(3,5)+"/"+transactionDate.substring(0,2)+"/"+transactionDate.substring(transactionDate.length - 4,transactionDate.length);
  return x
}

function formatDateDDMMYY(transactionDate){
  kony.print("transactionDate "+transactionDate);
  if(transactionDate.indexOf("-")>=0)
  {
    var x = transactionDate.substring(transactionDate.length - 2,transactionDate.length)+"/"+transactionDate.substring(5,7)+"/"+transactionDate.substring(0,4);
  }
  else
  {
    var x = transactionDate.substring(0,2)+"/"+transactionDate.substring(3,5)+"/"+transactionDate.substring(transactionDate.length - 4,transactionDate.length);
  }
  return x
}

function onClickYesClose(){
  frmNewBillKA.show();
  clearNewDetailsScreen();
  popupCommonAlertDimiss();
}

function back_NEWBILLS(){
  customAlertPopup(geti18Value("i18n.cards.Confirmation"), 
                   geti18Value("i18n.Bene.backFromBeneError"),
                   onClickYesClose, onClickNoClose, 
                   geti18Value("i18n.common.YES"),
                   geti18Value("i18n.common.NO"));
}

function back_NEWBILLSfrmBills(){
  customAlertPopup(geti18Value("i18n.cards.Confirmation"), 
                   geti18Value("i18n.Bene.backFromBeneError"),
                   onClickYesClosefrmBills, onClickNoClose, 
                   geti18Value("i18n.common.YES"),
                   geti18Value("i18n.common.NO"));
}

function onClickYesClosefrmBills(){
  if(loadBillerDetails){
  	billType = "POSTPAID";
	kony.boj.getBillNumberDetails();
  }else{
  	frmPayeeDetailsKA.show();
  }
  clearfrmBills();
  popupCommonAlertDimiss();
}

function onClickYesClose_NEWBILLSPAY(){
  popupCommonAlertDimiss();
    ShowLoadingScreen();
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmManagePayeeKA");
  var navObject = new kony.sdk.mvvm.NavigationObject();
  navObject.setRequestOptions("managepayeesegment",{"headers":{},"queryParams": {"custId": custid,
                              "P_BENE_TYPE": "PRE"
                             }});
  controller.loadDataAndShowForm(navObject);

}

function onClickNoClose(){
  popupCommonAlertDimiss();
}

function back_NEWBILLSPAY(){
  customAlertPopup(geti18nkey("i18n.cards.Confirmation"), 
                   geti18Value("i18n.Bene.backFromBeneError"),
                   onClickYesClose_NEWBILLSPAY, onClickNoClose, 
                   geti18Value("i18n.common.YES"),
                   geti18Value("i18n.common.NO"));
}

function serv_BILLSCOMISSIONCHARGE(amount){
  try{
    var billerDetails = kony.store.getItem("BillPayfromAcc");
    var currencyCode = "1";
    objName = "";
    customVerbName = "";
    kony.print("gblTModule ::"+gblTModule);
    if(gblTModule == "web" || gblTModule === "cards"){
      objName = "Transactions";
      customVerbName = "getFeeData";
    }
    else{
      objName = "Payee";
      customVerbName = "getFeeForBill";
    }
    kony.print("billerDetails ::"+JSON.stringify(billerDetails));
    if(billerDetails !== null && billerDetails !== ""){
      for(var i in gblCurrList){
        if(gblCurrList[i].CURR_ISO === (billerDetails.currencyCode === undefined?billerDetails.balance.split(" ")[1]:billerDetails.currencyCode)){
          currencyCode = gblCurrList[i].CURR_CODE;
        }
      }
      var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var options = {
        "access": "online",
        "objectName": "RBObjects"
      };
      var headers = {};
      var serviceName = "RBObjects";
      var modelObj = INSTANCE.getModel(objName, serviceName, options);
      var dataObject = new kony.sdk.dto.DataObject(objName);
      if(gblTModule == "web" || gblTModule === "cards"){
        dataObject.addField("TransferFlag","T");
        dataObject.addField("p_billId","");
        dataObject.addField("p_billNo","");
        dataObject.addField("p_trnsf_cur",currencyCode);
        dataObject.addField("accountNumber",billerDetails.accountID === undefined?billerDetails.card_num:billerDetails.accountID);
        dataObject.addField("ExternalAccountNumber","");
        dataObject.addField("amount",amount);
      }
      else{
        dataObject.addField("fees","");
        dataObject.addField("p_paymt_type","BillNew");
        dataObject.addField("p_account_br",billerDetails.branchNumber === undefined?"":billerDetails.branchNumber);
        dataObject.addField("p_acc_or_cr",billerDetails.accountID === undefined?billerDetails.card_num:billerDetails.accountID);
        dataObject.addField("amount",amount);
      }

      var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
      };

      if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        kony.print("dataObject ::"+JSON.stringify(dataObject));
        modelObj.customVerb(customVerbName, serviceOptions, serv_BILLSCOMISSIONCHARGECALLBACK, serv_BILLSCOMISSIONCHARGEFAILURECALLBACK);
      } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
      }
    }
  }catch(e){
    kony.print("serv_BILLSCOMISSIONCHARGE ::"+e);
    exceptionLogCall("serv_BILLSCOMISSIONCHARGE","service call execution exception","UI",e);
  }
}

function serv_BILLSCOMISSIONCHARGECALLBACK(response){
  try{
    kony.print("Comission Success ::"+JSON.stringify(response));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    var currForm = kony.application.getCurrentForm();
    if(!isEmpty(response)){
      if(!isEmpty(response.errmsg)){
        if(response.errmsg.length > 0){
          var errorMessage = geti18Value("i18n.common.somethingwentwrong");
          customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage,popupCommonAlertDimiss,"");
          exceptionLogCall("serv_BILLSCOMISSIONCHARGECALLBACK","UI ERROR","SERVICE",response.errmsg);
          if(gblTModule!="web")	
            frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false);
          return;
        }
      } 

      if(isEmpty(response.ref_code) && objName == "Transactions"){
        var errorMessage = geti18Value("i18n.common.somethingwentwrong");
        customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage,popupCommonAlertDimiss,"");
        exceptionLogCall("serv_BILLSCOMISSIONCHARGECALLBACK","UI ERROR","SERVICE",response.ref_code);
        if(gblTModule!="web")	
          frmNewTransferKA.FlexContainer0b7dc31ecc5cf40.setVisibility(false);
        return;

      }

      if(kony.application.getCurrentForm().id === "frmBills"){
        /* Hassan */
        if ((kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD") && (gblQuickFlow !== "postBill")){
      	   gblQuickFlow = "postBillAcc";
        }
        
        if(gblQuickFlow === "postBill"){
          gblQuickFlow = "post";
          frmBills.flxConversionAmt.setVisibility(false);
          if(frmBills.btnBillsPayAccounts.text === "t")
            if(kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD"){
              frmBills.flxConversionAmt.setVisibility(true);
              frmBills.lblVal.text = formatamountwithCurrency(response.p_debit_amt,kony.store.getItem("BillPayfromAcc").currencyCode)+" "+kony.store.getItem("BillPayfromAcc").currencyCode;
              var amount = frmBills.tbxAmount.text.replace(/,/g,"");
              amount = ((parseFloat(response.p_debit_amt)/parseFloat(amount))).toFixed(3);
              frmBills.lblToCurr.text = amount+" "+kony.store.getItem("BillPayfromAcc").currencyCode;
            }
          assignDatatoConfirmPostpaidBill(response);
        }else if(gblQuickFlow === "postBillAcc"){
          gblQuickFlow = "post";
          frmBills.flxConversionAmt.setVisibility(false);
          if(frmBills.btnBillsPayAccounts.text === "t")
            if(kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD"){
              frmBills.flxConversionAmt.setVisibility(true);
              frmBills.lblVal.text = formatamountwithCurrency(response.p_debit_amt,kony.store.getItem("BillPayfromAcc").currencyCode)+" "+kony.store.getItem("BillPayfromAcc").currencyCode;
              var amount = frmBills.tbxAmount.text.replace(/,/g,"");
              amount = ((parseFloat(response.p_debit_amt)/parseFloat(amount))).toFixed(3);
              frmBills.lblToCurr.text = amount+" "+kony.store.getItem("BillPayfromAcc").currencyCode;
            }
        }else{
          assignDatatoConfirmPostpaidBill(response);
        }
      }else if(kony.application.getCurrentForm().id === "frmNewBillDetails"){
        /* hassan */
        frmNewBillDetails.flxConversionAmt.setVisibility(false);
        if(gblQuickFlow === "postBill"){
          gblQuickFlow = "post";
          frmNewBillDetails.flxConversionAmt.setVisibility(false);
          if(frmBills.btnBillsPayAccounts.text === "t")
            if(kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD"){
              frmNewBillDetails.flxConversionAmt.setVisibility(true);
              frmNewBillDetails.lblVal.text = formatamountwithCurrency(response.p_debit_amt,kony.store.getItem("BillPayfromAcc").currencyCode)+" "+kony.store.getItem("BillPayfromAcc").currencyCode;
              var amount = frmNewBillDetails.tbxAmount.text.replace(/,/g,"");
              amount = ((parseFloat(response.p_debit_amt)/parseFloat(amount))).toFixed(3);
              frmNewBillDetails.lblToCurr.text = amount+" "+kony.store.getItem("BillPayfromAcc").currencyCode;
            }
          nextfrmPostPaidQuickPay(response);
        }
        else if(gblQuickFlow === "postBillAcc"){
          gblQuickFlow = "post";
          currForm["flxConversionAmt"].setVisibility(false);
          if(frmBills.btnBillsPayAccounts.text === "t")
            if(kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD"){
              currForm["flxConversionAmt"].setVisibility(true);
              currForm["lblVal"].text = formatamountwithCurrency(response.p_debit_amt,kony.store.getItem("BillPayfromAcc").currencyCode)+" "+kony.store.getItem("BillPayfromAcc").currencyCode;
              var amount = currForm["tbxAmount"].text.replace(/,/g,"");
              amount = ((parseFloat(response.p_debit_amt)/parseFloat(amount))).toFixed(3);
              currForm["lblToCurr"].text = amount+" "+kony.store.getItem("BillPayfromAcc").currencyCode;
            }
        }
        else if(gblQuickFlow === "post"){
          nextfrmPostPaidQuickPay(response);
        }else{
          nextfrmPrePaidQuickPay(response);
        }
      }else if(kony.application.getCurrentForm().id === "frmNewBillKA"){
        nextfrmPrePaidQuickPay(response);
      }else if(kony.application.getCurrentForm().id === "frmBulkPaymentKA" || gblTModule === "BulkPrePayPayment"){
      	kony.print("response ::"+JSON.stringify(response));
        show_COMISSION_CHARGES(response);
      }else if(gblTModule === "BillPrePayNewBillKA"){
      	show_COMISSION_CHARGES_PREPAID_BILLERS(response);
      }else if(gblTModule === "cards"){
        plot_CARDSPAYMENTCONFIRMATIONSCREEN(response.transferResponse[0]);
      }
    }else{
      customAlertPopup(geti18Value("i18n.Bene.Failed"),geti18Value("i18n.common.defaultErrorMsg"),popupCommonAlertDimiss, "");
      kony.print("Empty result - commission");
      kony.application.dismissLoadingScreen();
    }
  }catch(e){
    kony.print("Exception_serv_BILLSCOMISSIONCHARGECALLBACK ::"+e);
    exceptionLogCall("serv_BILLSCOMISSIONCHARGECALLBACK","success call exception","UI",e);
  }
}

function serv_BILLSCOMISSIONCHARGEFAILURECALLBACK(err){
  kony.print("Bill commission Failure ::" + JSON.stringify(err));
  kony.application.dismissLoadingScreen();
  exceptionLogCall("serv_BILLSCOMISSIONCHARGEFAILURECALLBACK","service failure call back logging","SERVICE",e);
  customAlertPopup(geti18Value("i18n.Bene.Failed"),geti18Value("i18n.common.defaultErrorMsg"),popupCommonAlertDimiss, "");
}


function restrictoThreeDecimalsBillPayMent(value, limit){
  if(!isEmpty(value)){
    value = value + "";
  if(value.indexOf(".") !== -1){
var splitValue = value.split('.');
    if(!isEmpty(splitValue[1])){
if(splitValue[1].length>3){
value = splitValue[0] + "." + splitValue[1].substring(0, limit);
}
}
}
}
  return value;
}

function validate_BILLPAY_BILLER(){
	try{
    	var isValid = true;
    	if(frmBills.lblPaymentMode.text === geti18Value("i18n.billsPay.Accounts")){
        	frmBills.flxUnderlinePaymentMode.skin = "skntextFieldDividerOrange";
        	isValid = false;
        }
    	if(frmBills.tbxAmount.text === "" || frmBills.tbxAmount.text === "." || frmBills.tbxAmount.text === null){
        	frmBills.lblUnderline.skin = "skntextFieldDividerOrange";
        	isValid = false;
        }
    	return isValid;
    }catch(e){
    	kony.print("Exception_validate_BILLPAY_BILLER ::"+e);
    }
}

function show_COMISSION_CHARGES_PREPAID_BILLERS(response){
	try{
    	if(check_SUFFICIENT_BALANCE(response.p_debit_amt, kony.store.getItem("BillPayfromAcc").availableBalance)){
          var amount = frmPrePaidPayeeDetailsKA.txtAmount.text, exchangeRate = 0;
//           amount = amount.substring(0,amount.length-4);
          amount = parseFloat(amount.replace(/,/g,""));
          frmPrePaidPayeeDetailsKA.lblVal.text = formatamountwithCurrency(response.p_debit_amt,kony.store.getItem("BillPayfromAcc").currencyCode)+" "+kony.store.getItem("BillPayfromAcc").currencyCode;
          exchangeRate = amount/parseFloat(response.p_debit_amt);
          exchangeRate = setDecimal(exchangeRate, 4);
          frmPrePaidPayeeDetailsKA.lblToCurr.text = exchangeRate+" "+kony.store.getItem("BillPayfromAcc").currencyCode;
          frmPrePaidPayeeDetailsKA.flxConversionAmt.setVisibility(true);
          frmPrePaidPayeeDetailsKA.lblNext.skin = "sknLblNextEnabled";
        }else{
        	customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        	frmPrePaidPayeeDetailsKA.flxConversionAmt.setVisibility(false);
        	frmPrePaidPayeeDetailsKA.lblNext.skin = "sknLblNextDisabled";
          	frmPrePaidPayeeDetailsKA.flxUnderlinePaymentModeBulk.skin = "sknFlxGreyLine";
        }
    }catch(e){
    	kony.print("Exception_show_COMISSION_CHARGES_PREPAID_BILLERS ::"+e);
    }
}