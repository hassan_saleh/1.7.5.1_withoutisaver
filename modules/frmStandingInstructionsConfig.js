var frmStandingInstructionsConfig= {
  "formid": "frmStandingInstructions",
  "frmStandingInstructions": {
    "entity": "StandingInstructions",
    "objectServiceName": "RBObjects",
    "objectServiceOptions" : {"access":"online"},
  },
  "segSIList": {
    "fieldprops": {
      "widgettype": "Segment",
      "entity": "StandingInstructions",
      "additionalFields": ["transfer_type","instruction_number","status","txn_frequency","frequency_code","ccy_code","period","credit_branch_code","credit_bracnh_desc","credit_acc","debit_acc"],
      "field": {
        "lblTransactiondate": {
          "widgettype": "Label",
          "field": "formattedDate"
        },
        "lblTransactionDesc":{
          "widgettype": "Label",
          "field": "transfer_typedesc"
        },
        "lblAmount":{
          "widgettype": "Label",
          "field": "formattedAmount"
        }, 
        "lblInitial":{
          "widgettype": "Label",
          "field": "initial"
        }
      }
    }
  }
};