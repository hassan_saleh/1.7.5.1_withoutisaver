function isAlternateLoginEnabled(flow) {
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  if (kony.sdk.isNetworkAvailable()) {
    fetchApplicationPropertiesWithoutLoading();

    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online"
    };
    var headers = {};
    objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects", options);
    var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");

    var devID = getCurrentDeviceId();
    var langSelected = kony.store.getItem("langPrefObj");
    var Language = langSelected.toUpperCase();
    var Token = Dvfn(kony.store.getItem("soft_token"));

    dataObject.addField("Language", Language);
    dataObject.addField("deviceId", devID);
    dataObject.addField("Token", Token);

    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };

    if(!isEmpty(devID) && !isEmpty(Token)){
      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      objectService.customVerb("getLoginMethod", serviceOptions, function(response) {
        typeAltLoginSuccessCallback(response, flow);
      }, typeAltMobileLoginErrorCallback);
    }else {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }
  } else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}


function typeAltLoginSuccessCallback(response, flowFlag) {
  try {
    var model = kony.os.deviceInfo().model;
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    kony.print("typeAltLoginSuccessCallback-->" + JSON.stringify(response));
    kony.print("flow is : " + flowFlag);
    kony.print("status Code : " + response.statusCode);

    if (response.statusCode !== null && response.statusCode !== "" && response.statusCode == "S0024") {
      custid = response.custId;
      if (kony.application.getCurrentForm().id === "frmLanguageChange") {
        frmLoginKA.show();
      }
      if (flowFlag == "P") {
        if (response.is_pin_enabled !== null && response.is_pin_enabled !== "" && response.is_pin_enabled == "Y") {
          frmLoginKA.loginMainScreen.opacity = 0.2;
          frmLoginKA.loginMainScreen.setVisibility(false);
          frmLoginKA.loginMainScreen.setEnabled(false);
          frmLoginKA.loginMainPINScreen.setVisibility(true);
          frmLoginKA.loginMainPINScreen.opacity = 1.0;
          frmLoginKA.loginMainPINScreen.setEnabled(true);
          frmLoginKA.pinReTry.setVisibility(true);
          frmLoginKA.pinReTry.text = " ";
          clearProgressFlexLogin();
          login_pass = "";

          frmLoginKA.loginMainPINScreen.setVisibility(true);
        } else {
          kony.store.removeItem("isPinSupported");
          setUtilIcons();
          customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.common.pin1"), popupCommonAlertDimiss, "");
        }
      } else if (flowFlag == "T") {

        if (response.is_touch_enabled !== null && response.is_touch_enabled !== "" && response.is_touch_enabled == "Y") {
          if (kony.retailBanking.globalData.deviceInfo.isTouchIDSupported()) {

            var status = kony.localAuthentication.getStatusForAuthenticationMode(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID);
            kony.print("Status for the touch/faceid :: "+ status);
            if (status == 5007 || status == 5005 || status == 5006) {
              if(checkFaceIdIphone()){
                customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.FaceID.notEnabledText"), popupCommonAlertDimiss, "");
              } else {
                customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.TOuch.notEnabledText"), popupCommonAlertDimiss, "");
              }
              kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            } else {
              touchLoginShow();
            }

          } else {
            customAlertPopup(geti18Value("i18n.common.alert"), kony.i18n.getLocalizedString("i18n.Touch.noSupport"), popupCommonAlertDimiss, "");
            kony.store.removeItem("isTouchSupported");
            setUtilIcons();
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
          }
        } else {
          kony.store.removeItem("isTouchSupported");
          setUtilIcons();
          if(checkFaceIdIphone()){
            customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.fingerprint.loginagain"), popupCommonAlertDimiss, "");
          } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.fingerprint.loginagain"), popupCommonAlertDimiss, "");
          }
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }
      }
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } else {
      kony.store.removeItem("isPinSupported");
      kony.store.removeItem("isTouchSupported");
      setUtilIcons();
      customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.fingerprint.loginagain"), popupCommonAlertDimiss, "");//i18n.common.reg
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }


  } catch (err) {
    kony.print("Error in set pin" + err);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    exceptionLogCall("typeAltLoginSuccessCallback", "UI ERROR", "UI", err);

  }
}

function typeAltMobileLoginErrorCallback(err) {
  kony.print("typeAltMobileLoginErrorCallback-->" + "An Error has occured, cant register" + JSON.stringify(err));
  customAlertPopup(geti18Value("i18n.Bene.Failed"), kony.i18n.getLocalizedString("i18n.common.defaultErrorMsg"), popupCommonAlertDimiss, "");
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}