kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.frmEnlargeAdKAViewController = function(){
  var infeedAds = 0;
};

kony.rb.frmEnlargeAdKAViewController.prototype.preshowfrmEnlargedAd =function()
{
  var navManager =  applicationManager.getNavManager();
  var infeedAds =  navManager.getCustomInfo("frmEnlargeAdKA");
  this.infeedAds = infeedAds;
  disableButtonUntilImageDownloads();
  var date = new Date();
  var param=date.getTime();
  frmEnlargeAdKA.imgFailedEnlargedAdKA.src = "loader2.gif";
  frmEnlargeAdKA.imgEnlargeAd.src = getImageURLBasedOnDeviceType(infeedAds.adImageURL)+"?Param="+param;
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();  
};

function onAdDownloadFailure()
{
  kony.print("In Feed Enlarged Ad download Failed");
  frmEnlargeAdKA.imgFailedEnlargedAdKA.src = "failuread.png";
  //wait time
  ShowLoadingScreen();
  var navManager =  applicationManager.getNavManager();
  navManager.navigateTo("frmAccountsLandingKA");
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); 
}
function disableButtonUntilImageDownloads()
{
  frmEnlargeAdKA.flxAdAction.setVisibility(false); 
  frmEnlargeAdKA.imgFailedEnlargedAdKA.setVisibility(true);
  frmEnlargeAdKA.imgEnlargeAd.setVisibility(false);
  frmEnlargeAdKA.imgCloseEnlargedAd.setVisibility(true);
  frmEnlargeAdKA.flxImgOnClickArea.setEnabled(false);
}

kony.rb.frmEnlargeAdKAViewController.prototype.adActionButtonOnClick =function()
{
  ShowLoadingScreen();
  KNYMetricsService.sendCustomMetrics("frmEnlargeAdKA", {"adAction" : "action2EnlargedAd1"});
  var navManager =  applicationManager.getNavManager();
  var infeedAds =  navManager.getCustomInfo("frmEnlargeAdKA");
  kony.application.openURL(infeedAds.adAction2);
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); 
};

kony.rb.frmEnlargeAdKAViewController.prototype.adImageOnClick =function()
{
  ShowLoadingScreen();
  KNYMetricsService.sendCustomMetrics("frmEnlargeAdKA", {"adAction" : "action1EnlargedAd1"});
  var navManager =  applicationManager.getNavManager();
  var infeedAds =  navManager.getCustomInfo("frmEnlargeAdKA");
  kony.application.openURL(infeedAds.adAction1);
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); 
};

kony.rb.frmEnlargeAdKAViewController.prototype.adCloseButtonOnClick =function()
{
  ShowLoadingScreen();
  KNYMetricsService.sendCustomMetrics("frmEnlargeAdKA", {"adAction" : "closeEnlargedAd1"});
  var navManager =  applicationManager.getNavManager();
  navManager.navigateTo("frmAccountsLandingKA");
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); 
};

kony.rb.frmEnlargeAdKAViewController.prototype.onImageDownloadComplete =function(issuccess)
{
  if(issuccess)
    {
      var enLargeAdVC = applicationManager.getEnlargedAdViewController();
      var infeedAds = enLargeAdVC.infeedAds;
      KNYMetricsService.sendCustomMetrics("frmEnlargeAdKA", {"adID" : "enlargedAd1"});
      frmEnlargeAdKA.imgFailedEnlargedAdKA.setVisibility(false);
      frmEnlargeAdKA.imgEnlargeAd.setVisibility(true);
      frmEnlargeAdKA.flxImgOnClickArea.setEnabled(true);
      if (infeedAds.adAction2 && (infeedAds.adAction2 !== null || infeedAds.adAction2 !== ""))
       {
            frmEnlargeAdKA.btnAdAction.text = infeedAds.adTitle;
            frmEnlargeAdKA.btnAdAction.setVisibility(true);
            frmEnlargeAdKA.flxAdAction.setVisibility(true); 
       }
    }
  else
    {
      kony.print("Infeed Enlarged Ad image download failure");
      onAdDownloadFailure();
    }
};