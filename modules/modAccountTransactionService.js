//Type your code here

function getAccountTransactions(queryParams,scopeObj,response){
  kony.print("getAccountTransactions queryParams::::"+JSON.stringify(queryParams));
  //kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
  kony.print("scopeObjgbl :::"+JSON.stringify(scopeObj));
  kony.print("scopeObjgblResponse :::"+JSON.stringify(response));
  function loopingSuccessCallback(resoponse){
    kony.print("response Looping"+JSON.stringify(resoponse)); 
    if(resoponse.LoopDataset !== null && resoponse.LoopDataset !==undefined){
      kony.retailBanking.globalData.accountsTransactionData = resoponse.LoopDataset;
      kony.print("loopingSuccessCallback-----------------data"+JSON.stringify(kony.retailBanking.globalData.accountsTransactionData)); 
    }else{
      kony.print("loopingSuccessCallback No data:::");
    }
    scopeObj.getController().processData(response);
    startChatbotAfterLogin(custid , false);
  }
  function loopingErrorCallback(error){
    kony.print("error looping"+JSON.stringify(error));
    var errorCode = "";
    if(error.code !== null){
      errorCode = error.code;
    }else if (error.opstatus !== null){
      errorCode = error.opstatus;
    }else if (error.errorCode !== null){
      errorCode = error.errorCode;
    }
    var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
    //     var title = geti18nkey("i18n.common.Information");
    customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage,popupCommonAlertDimiss,"");
    kony.retailBanking.globalData.accountsTransactionData = [];
    scopeObj.getController().processData(response);
  }

  //    appMFConfiguration.integrationObj = appMFConfiguration.konysdkObject.getIntegrationService("LoopExample");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("LoopExample");		 

  if (kony.sdk.isNetworkAvailable()) {
    appMFConfiguration.invokeOperation("LoopTransactions", {},queryParams,loopingSuccessCallback,loopingErrorCallback);
  }else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
  }


}

appMFConfiguration = 
  {
  //     appKey:"61f2a242a8c540ed27411384f4f6f633", 
  //     appSecret:"949582febf6fe8e25b3b7d328ce6bbfe",
  //     serviceURL:"http://boj.konylabs.net/authService/100000002/appconfig",

  appKey:"dc21df34809a40f5c8c3525d3b04c86b", 
  appSecret:"e3a0f8b0b49339e145d62e5df867648d",
  serviceURL:"https://mobiledev.bankofjordan.com.jo:7004/authService/100000002/appconfig",

  integrationServices: 
  [
    {
      service:"BOJGetTransactionDetails",
      operations:["prGetTransactionsDetails"]
    },{
      service:"BOJGetDbCardDetails",
      operations:["prGetDbCardDetails"]
    }

  ],

  orchestrationServices: 
  [
    {
      service:"LoopExample",
      operations:["LoopTransactions"]
    },{
      service:"DebitCardList",
      operations:["getDebitCardList"]
    }

  ],

  konysdkObject: null,
  authClient: null,
  integrationObj: null

};


function initIntergrationService(){
  kony.print("initIntergrationService:::");
  try{      
    appMFConfiguration.konysdkObject = new kony.sdk();
    appMFConfiguration.konysdkObject.init(appMFConfiguration.appKey,appMFConfiguration.appSecret,appMFConfiguration.serviceURL,function(res){kony.print("success"+JSON.stringify(res));}, function(err){kony.print("Failed"+JSON.stringify(err));});
  }catch(e){
    exceptionLogCall("initIntergrationService","UI ERROR","UI",e);
    kony.print("Exception_initIntergrationService ::"+e);
  }
}





/*************************
function getDWCardListDetails(scopeObj,response){
  var queryParams = {
                        "custId": custid+":"+custid+":",
                        "loop_count": parseInt(2),
                        "loop_seperator": ":",
              			"CardNum":"",
                        "Cardflag" : "D:W:"

                    };
  kony.print("getDWCardListDetails queryParams::::"+JSON.stringify(queryParams));
   function loopingSuccessCallback(resoponse){
        kony.print("response Looping"+JSON.stringify(resoponse)); 
        if(resoponse.LoopDataset !== null && resoponse.LoopDataset[0].opstatus !==undefined && resoponse.LoopDataset[0].opstatus !== 0){
          kony.retailBanking.globalData.prDWCardDetails = [];
          var errorMessage = geti18nkey("i18n.cards.unabletoretrivedebitcarddetails");
  		  customAlertPopup("", errorMessage,popupCommonAlertDimiss,"");
      	  kony.print("opstatus fail:::");
        }else if(resoponse.LoopDataset !== null && resoponse.LoopDataset !==undefined){
    kony.retailBanking.globalData.prDWCardDetails = resoponse.LoopDataset;
    kony.print("loopingSuccessCallback-----------------data"+JSON.stringify(kony.retailBanking.globalData.prDWCardDetails)); 
    }else{
      kony.retailBanking.globalData.prDWCardDetails = [];
      kony.print("loopingSuccessCallback No data:::");
    }
    scopeObj.getController().processData(response);

      }
  function loopingErrorCallback(error){
    kony.print("error looping"+JSON.stringify(error));
    var errorCode = "";
    if(error.code !== null){
    errorCode = error.code;
  }else if (error.opstatus !== null){
     errorCode = error.opstatus;
  }else if (error.errorCode !== null){
     errorCode = error.errorCode;
  }
    var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
//     var title = geti18nkey("i18n.common.Information");
  customAlertPopup("", errorMessage,popupCommonAlertDimiss,"");
    kony.retailBanking.globalData.prDWCardDetails = [];
   scopeObj.getController().processData(response);
  }

  	var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("DebitCardList");
    //appMFConfiguration.integrationObj = appMFConfiguration.konysdkObject.getIntegrationService("DebitCardList");

  if (kony.sdk.isNetworkAvailable()) {
  appMFConfiguration.invokeOperation("getDebitCardList", {},queryParams,loopingSuccessCallback,loopingErrorCallback);
  }
    else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
    }


}
*******************/
