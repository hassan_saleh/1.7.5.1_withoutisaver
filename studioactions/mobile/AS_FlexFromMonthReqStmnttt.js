function AS_FlexFromMonthReqStmnttt(eventobject) {
    return AS_FlexContainer_c105de35dcfa47e4953a274cca5cb3d3(eventobject);
}

function AS_FlexContainer_c105de35dcfa47e4953a274cca5cb3d3(eventobject) {
    if (frmRequestStatementAccounts.lblYearFromValue.text != geti18Value("i18n.filtertransaction.year")) {
        gblsubacclist = "fromMonth";
        frmAccountType.show();
    } else {
        frmRequestStatementAccounts.lblLineFY.skin = "sknFlxOrangeLine";
    }
}