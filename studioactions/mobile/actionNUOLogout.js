function actionNUOLogout(eventobject) {
    return AS_Button_e602124b007c45f3b458238cac883ffd(eventobject);
}

function AS_Button_e602124b007c45f3b458238cac883ffd(eventobject) {
    function SHOW_ALERT__e95b6673a613446cb90a88cff214bf3c_True() {
        kony.print("Deleted");
    }

    function SHOW_ALERT__e95b6673a613446cb90a88cff214bf3c_False() {}

    function SHOW_ALERT__e95b6673a613446cb90a88cff214bf3c_Callback(response) {
        if (response === true) {
            SHOW_ALERT__e95b6673a613446cb90a88cff214bf3c_True();
        } else {
            SHOW_ALERT__e95b6673a613446cb90a88cff214bf3c_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__e95b6673a613446cb90a88cff214bf3c_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}