function AS_onClickDeviceBackofNewSubAccLanding(eventobject) {
    return AS_Form_fe2fdd9c0fc1424d9b41f0a4fa3f1942(eventobject);
}

function AS_Form_fe2fdd9c0fc1424d9b41f0a4fa3f1942(eventobject) {
    //Omar Alnajjar 
    frmNewSubAccountLandingNew.lblFre.text = geti18Value("i18n.common.accountType");
    frmNewSubAccountLandingNew.lblCurr.text = geti18Value("i18n.common.currencyType");
    frmNewSubAccountLandingNew.lblAccDesc.text = "";
    frmNewSubAccountLandingNew.flxAccDesc.setVisibility(false);
    frmNewSubAccountLandingNew.flxISaverSub.setVisibility(false);
    frmSettingsKA.show();
}