function AS_TextField_a986d7c97fe4444d82b80d281c919f66(eventobject, changedtext) {
    try {
        frmEstatementLandingKA.txtEmail1.text = frmEstatementLandingKA.txtEmail1.text.trim();
        if (frmEstatementLandingKA.txtEmail1.text !== null && frmEstatementLandingKA.txtEmail1.text !== "" && isValidEmaill(frmEstatementLandingKA.txtEmail1.text)) {
            frmEstatementLandingKA.borderBottom1.skin = "skntextFieldDividerGreen";
            frmEstatementLandingKA.lblInvalidEmail1.setVisibility(false);
            if (frmEstatementLandingKA.flxSwitchOnTouchLogin.isVisible && frmEstatementLandingKA.lblTermsandConditionsCheckBox.text === "p") frmEstatementLandingKA.lblNext.skin = "sknLblNextEnabled";
            else frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
        } else {
            frmEstatementLandingKA.borderBottom1.skin = "skntextFieldDivider";
            frmEstatementLandingKA.lblInvalidEmail1.text = geti18Value("i18n.Estmt.invalidEmail");
            frmEstatementLandingKA.lblInvalidEmail1.setVisibility(true);
            frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
        }
    } catch (err) {
        kony.print("err in pwd::" + err);
    }
}