function AS_onEndEditingSwiftAndroid(eventobject, changedtext) {
    return AS_TextField_a692bcdfd67b4e6f86e38c3e7006735d(eventobject, changedtext);
}

function AS_TextField_a692bcdfd67b4e6f86e38c3e7006735d(eventobject, changedtext) {
    if (frmAddExternalAccountKA.txtSwiftCodeKA.text !== "") {
        frmAddExternalAccountKA.flxUnderlineSwiftCode.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineSwiftCode.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblSwiftCode", frmAddExternalAccountKA.txtSwiftCodeKA.text);
}