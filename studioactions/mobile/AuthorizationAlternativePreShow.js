function AuthorizationAlternativePreShow(eventobject) {
    return AS_Form_cf6b6d5d09544b57a668bc3773fce7a2(eventobject);
}

function AS_Form_cf6b6d5d09544b57a668bc3773fce7a2(eventobject) {
    if (frmAuthorizationAlternatives.btnSaveQuickBal.isVisible) {
        fnauthorizationAlternativePreShow("frmSettingsKA");
    } else {
        fnauthorizationAlternativePreShow("frmAlternative");
    }
    frmAuthorizationAlternatives.flxAccounts.accessibilityConfig = {
        "a11yValue": geti18Value("i18n.login.next"),
        "a11yLabel": "",
        "a11yHint": ""
    };
    frmAuthorizationAlternatives.flxSwitchOffTouchLogin.accessibilityConfig = {
        "a11yValue": geti18Value("i18n.common.touchIDEnable"),
        "a11yLabel": "",
        "a11yHint": ""
    };
    frmAuthorizationAlternatives.flxSwitchOnTouchLogin.accessibilityConfig = {
        "a11yValue": geti18Value("i18n.common.touchIDUnable"),
        "a11yLabel": "",
        "a11yHint": ""
    };
}