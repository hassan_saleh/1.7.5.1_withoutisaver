function actOnAddLogout(eventobject) {
    return AS_Button_efd3a97431234a2fa6b1417b473d9ac6(eventobject);
}

function AS_Button_efd3a97431234a2fa6b1417b473d9ac6(eventobject) {
    function SHOW_ALERT_ide_onClick_dc44f1127a654e40b52f2e316d91208b_True() {
        kony.print("Deleted");
    }

    function SHOW_ALERT_ide_onClick_dc44f1127a654e40b52f2e316d91208b_False() {}

    function SHOW_ALERT_ide_onClick_dc44f1127a654e40b52f2e316d91208b_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_dc44f1127a654e40b52f2e316d91208b_True();
        } else {
            SHOW_ALERT_ide_onClick_dc44f1127a654e40b52f2e316d91208b_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_dc44f1127a654e40b52f2e316d91208b_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}