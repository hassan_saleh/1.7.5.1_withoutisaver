function AS_onClickNewAccSubLandingBack(eventobject) {
    return AS_FlexContainer_f190262699e649c79c85a75f18facad6(eventobject);
}

function AS_FlexContainer_f190262699e649c79c85a75f18facad6(eventobject) {
    //Omar Alnajjar 
    frmNewSubAccountLandingNew.lblFre.text = geti18Value("i18n.common.accountType");
    frmNewSubAccountLandingNew.lblCurr.text = geti18Value("i18n.common.currencyType");
    frmNewSubAccountLandingNew.lblAccDesc.text = "";
    frmNewSubAccountLandingNew.flxAccDesc.setVisibility(false);
    frmNewSubAccountLandingNew.flxISaverSub.setVisibility(false);
    frmSettingsKA.show();
}