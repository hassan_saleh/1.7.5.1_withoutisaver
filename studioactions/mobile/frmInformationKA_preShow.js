function frmInformationKA_preShow(eventobject) {
    return AS_Form_c178a8a53fd64652a31293d1a9547e45(eventobject);
}

function AS_Form_c178a8a53fd64652a31293d1a9547e45(eventobject) {
    frmInformationKA.flcContentSivaram.showFadingEdges = false;
    frmInformationKA.flxNewsSivaram.showFadingEdges = false;
    frmInformationKA.flxNewsAndBankInfo.showFadingEdges = false;
    frmInformationKA.flxContactUsSivaram.showFadingEdges = false;
    frmInformationKA.mainContent.showFadingEdges = false;
    frmInformationKA.flxFAQInfo.showFadingEdges = false;
    frmInformationKA.flxFAQMainContent.showFadingEdges = false;
    frmInformationKA.flxSearchInfo.showFadingEdges = false;
    frmInformationKA.flxFAQ.showFadingEdges = false;
    frmInformationKA.flxFAQSearch.showFadingEdges = false;
    var currentAppVersion = appConfig.appVersion;
    frmInformationKA.lblMobile.text = bojcontactnumber;
    frmInformationKA.lblEmail.text = bojemailID;
    frmInformationKA.lblVersionCode.text = "V " + currentAppVersion;
}