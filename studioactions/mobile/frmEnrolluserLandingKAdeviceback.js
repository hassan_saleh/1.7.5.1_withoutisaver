function frmEnrolluserLandingKAdeviceback(eventobject) {
    return AS_Form_359d5f9a8a1c4b529755fedce4d6fa9e(eventobject);
}

function AS_Form_359d5f9a8a1c4b529755fedce4d6fa9e(eventobject) {
    if (gblPayAppFlow && !isEmpty(gblLaunchModeOBJ.appFlow)) {
        openBOJPAYAppLink();
    } else if (gblforceflag) {
        kony.sdk.mvvm.LogoutAction();
    } else {
        if (gblFromModule == "RegisterUser") {
            frmRegisterUser.show();
        } else if (gblFromModule == "ChangeUsername") {
            frmSettingsKA.show();
        } else if (gblFromModule == "ChangePassword") {
            frmSettingsKA.show();
        } else {
            frmEnrolluserLandingKA.lblFlag.text = "F";
            if (gblReqField == "Username") {
                frmLoginKA.show();
            } else {
                frmRegisterUser.show();
            }
        }
        frmNewUserOnboardVerificationKA.destroy();
        //frmEnrolluserLandingKA.destroy();
    }
}