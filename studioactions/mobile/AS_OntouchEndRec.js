function AS_OntouchEndRec(eventobject, x, y) {
    return AS_TextField_e3c04c46178f45fe87dc8bb8d34eba8a(eventobject, x, y);
}

function AS_TextField_e3c04c46178f45fe87dc8bb8d34eba8a(eventobject, x, y) {
    //frmNewTransferKA.txtBox.text=formatAmountwithcomma(frmNewTransferKA.txtBox.text,3);
    var amount = frmNewTransferKA.txtBox.text;
    //amount = parseFloat(amount);
    if (amount === null || amount === "" || amount == "." || (Number(amount) < 0.01) || amount === 0.00) {
        kony.print("Amount is either null or empty");
    } else {
        frmNewTransferKA.lblHiddenAmount.text = frmNewTransferKA.txtBox.text;
        frmNewTransferKA.txtBox.text = formatAmountwithcomma(amount, frmNewTransferKA.lblDecimal.text);
    }
    animateLabel("UP", "lblNumRecurr", frmNewTransferKA.txtNumRecurrences.text);
    enableNext();
}