function AS_onClickPostPaidManagePayee(eventobject) {
    return AS_Button_i5fea5cae22042bc8d50a546431cbf03(eventobject);
}

function AS_Button_i5fea5cae22042bc8d50a546431cbf03(eventobject) {
    frmManagePayeeKA.btnAll.skin = "slButtonWhiteTabDisabled";
    frmManagePayeeKA.btnPrePaid.skin = "slButtonWhiteTabDisabled";
    frmManagePayeeKA.btnPostPaid.skin = "slButtonWhiteTab";
    selectedIndex_BULK_PAYMENT = [];
    validate_BULK_BILL_SELECTION();
    show_BILLER_LIST("postpaid");
}