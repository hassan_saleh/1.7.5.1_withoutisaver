function AS_TextField_hec52fa17d3f4992ad1c262feee08009(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidUsername(frmEnrolluserLandingKA.tbxUsernameKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxGreenLine";
        usernameFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxOrangeLine";
        usernameFlag = false;
    }
    if (usernameFlag === true && passwordFlag === true && confrmPasswordFlag === true) {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextEnabled";
        frmEnrolluserLandingKA.flxProgress1.left = "80%";
    } else {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextDisabled";
        frmEnrolluserLandingKA.flxProgress1.left = "65%";
    }
}