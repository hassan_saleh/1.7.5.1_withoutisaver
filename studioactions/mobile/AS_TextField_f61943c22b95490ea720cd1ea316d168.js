function AS_TextField_f61943c22b95490ea720cd1ea316d168(eventobject, changedtext) {
    try {
        if (frmLoginKA.tbxusernameTextField.text !== null && frmLoginKA.tbxusernameTextField.text !== "") {
            frmLoginKA.borderBottom.skin = "skntextFieldDividerGreen";
        } else {
            frmLoginKA.borderBottom.skin = "skntextFieldDivider";
        }
        animateLabel("DOWN", "lblUserName", frmLoginKA.tbxusernameTextField.text);
        animateLabel("UP", "lblPassword", frmLoginKA.passwordTextField.text);
    } catch (err) {
        kony.print("err in pwd::" + err);
    }
}