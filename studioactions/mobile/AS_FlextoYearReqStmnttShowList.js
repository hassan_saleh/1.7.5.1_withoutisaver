function AS_FlextoYearReqStmnttShowList(eventobject) {
    return AS_FlexContainer_b32c6059d6014401bd57f020787d3968(eventobject);
}

function AS_FlexContainer_b32c6059d6014401bd57f020787d3968(eventobject) {
    if (frmRequestStatementAccounts.lblYearFromValue.text != geti18Value("i18n.filtertransaction.year")) {
        gblsubacclist = "toYear";
        frmAccountType.show();
    } else {
        frmRequestStatementAccounts.lblLineFY.skin = "sknFlxOrangeLine";
    }
}