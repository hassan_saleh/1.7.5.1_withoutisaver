function AS_Label_d89074e7a6fe4dafa25e561aa2b3dfb4(eventobject, x, y) {
    var currentForm = kony.application.getCurrentForm();
    if (currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") {
        currentForm.passwordTextField.text = "";
        currentForm.flxbdrpwd.skin = "skntextFieldDivider";
        currentForm.lblInvalidCredentialsKA.isVisible = false;
        currentForm.lblShowPass.setVisibility(false);
        animateLabel("DOWN", "lblPassword", currentForm.tbxusernameTextField.text);
    }
}