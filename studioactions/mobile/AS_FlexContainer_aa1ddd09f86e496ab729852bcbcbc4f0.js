function AS_FlexContainer_aa1ddd09f86e496ab729852bcbcbc4f0(eventobject) {
    if (frmFilterTransaction.flxDatePicker.isVisible == true) {
        clearForm.filterTransactions();
        if (kony.application.getPreviousForm().id != "frmManageCardsKA") {
            frmFilterTransaction.flxDatePicker.isVisible = false;
            frmFilterTransaction.flxFilterTransaction.isVisible = true;
            frmFilterTransaction.lblFilterHeader.text = kony.i18n.getLocalizedString("i18n.FilterTransaction");
        } else {
            frmManageCardsKA.show();
        }
    } else if (frmFilterTransaction.flxFilterTransaction.isVisible == true) {
        kony.application.getPreviousForm().show();
        clearForm.filterTransactions();
    }
}