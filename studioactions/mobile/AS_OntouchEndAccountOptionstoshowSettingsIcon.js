function AS_OntouchEndAccountOptionstoshowSettingsIcon(eventobject, x, y) {
    return AS_Label_b0ec57afef88412b8352a761dd0300fc(eventobject, x, y);
}

function AS_Label_b0ec57afef88412b8352a761dd0300fc(eventobject, x, y) {
    frmAccountInfoKA.lblListInfoInfo.skin = "lblListOff";
    frmAccountInfoKA.imgDiv.setVisibility(false);
    frmAccountInfoKA.flxTabsWrapper.setVisibility(true); // hassan 28/01/20201
    frmAccountInfoKA.lblAccountOptions.skin = "lblFontSettIconsFont2oN";
    frmAccountInfoKA.imgDivSett.setVisibility(true);
    frmAccountInfoKA.flxLoanPostPone.setVisibility(false);
    /*if(frmAccountInfoKA.lblLoanAccountInfo.text === kony.i18n.getLocalizedString("i18n.accounts.loans")){
  frmAccountInfoKA.flxRequestStatement.setVisibility(false);
  frmAccountInfoKA.flxOrderCheckBook.setVisibility(false);
  frmAccountInfoKA.flxLoanPostPone.setVisibility(true);
  
}else{*/
    frmAccountInfoKA.flxRequestStatement.setVisibility(true);
    frmAccountInfoKA.flxOrderCheckBook.setVisibility(false);
    if (kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)].accountType === "C" && kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)].producttype !== "303" && kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)].producttype !== "441") {
        frmAccountInfoKA.flxOrderCheckBook.setVisibility(true);
    }
    //}
    frmAccountInfoKA.flxAccountOptions.setVisibility(true);
    frmAccountInfoKA.flxLoanInfo.setVisibility(false);
    frmAccountInfoKA.flxDeposits.setVisibility(false);
    frmAccountInfoKA.flxaccountdetailsfordeposits.setVisibility(false);
    frmAccountInfoKA.forceLayout();
}