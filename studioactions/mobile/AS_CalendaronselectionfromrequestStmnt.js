function AS_CalendaronselectionfromrequestStmnt(eventobject, isValidDateSelected) {
    return AS_Calendar_i83644f2115f4f0f9e735cf87f556d5e(eventobject, isValidDateSelected);
}

function AS_Calendar_i83644f2115f4f0f9e735cf87f556d5e(eventobject, isValidDateSelected) {
    var field = frmRequestStatementAccounts.CalTo;
    var date = field.dateComponents[0] < 10 ? "0" + field.dateComponents[0] : field.dateComponents[0];
    var month = field.dateComponents[1] < 10 ? "0" + field.dateComponents[1] : field.dateComponents[1];
    frmRequestStatementAccounts.lblToDate.text = month + "/" + field.dateComponents[2];
    frmRequestStatementAccounts.lblLine3.skin = "sknFlxGreenLine";
    frmRequestStatementAccounts.lblToService.text = month + "/" + field.dateComponents[2];
    frmRequestStatementAccounts.lblToDateHidee.text = month + "/" + date + "/" + field.dateComponents[2];
    frmRequestStatementAccounts.lblTo.setVisibility(true);
    frmRequestStatementAccounts.CalTo.text = "";
    nextSkinCheckForRequestStaement();
}