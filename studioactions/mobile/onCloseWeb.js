function onCloseWeb(eventobject) {
    return AS_Button_a545ccb44f174df1980a57143ed30421(eventobject);
}

function AS_Button_a545ccb44f174df1980a57143ed30421(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18nkey("i18n.creditcard.discarddata"), navigatetomanagecard, popupCommonAlertDimiss, geti18Value("i18n.login.continue"), geti18Value("i18n.common.cancel"));
}