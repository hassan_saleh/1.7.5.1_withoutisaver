function onEndEditingPasswordLang(eventobject, changedtext) {
    return AS_TextField_jd161ebeebbf4aee894d033116710aa5(eventobject, changedtext);
}

function AS_TextField_jd161ebeebbf4aee894d033116710aa5(eventobject, changedtext) {
    try {
        if (frmLanguageChange.passwordTextField.text !== null && frmLanguageChange.passwordTextField.text !== "") {
            frmLanguageChange.flxbdrpwd.skin = "skntextFieldDividerGreen";
        } else {
            frmLanguageChange.flxbdrpwd.skin = "skntextFieldDivider";
        }
        animateLabel("DOWN", "lblPassword", frmLanguageChange.passwordTextField.text);
    } catch (e) {
        kony.print("Exception_Password ::" + e);
    }
}