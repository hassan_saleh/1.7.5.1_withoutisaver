function AS_Button_fd389e2eb47247a6891b5bb124a9f8d7(eventobject) {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmAddExternalAccountKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
    navObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
    //navObject.setRequestOptions("ListCountryKA",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
    navObject.setRequestOptions("form", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        }
    });
    listController.performAction("navigateTo", ["frmAddExternalAccountKA", navObject]);
}