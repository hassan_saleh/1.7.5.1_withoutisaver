function AS_Button_7704ef2413594df0a30a1120cc5b04ce(eventobject) {
    function SHOW_ALERT__62a805a8eee544b09665e4687b51ae25_True() {
        deviceRegFrom = "logout";
        removeSwipe();
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT__62a805a8eee544b09665e4687b51ae25_False() {}

    function SHOW_ALERT__62a805a8eee544b09665e4687b51ae25_Callback(response) {
        if (response === true) {
            SHOW_ALERT__62a805a8eee544b09665e4687b51ae25_True();
        } else {
            SHOW_ALERT__62a805a8eee544b09665e4687b51ae25_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__62a805a8eee544b09665e4687b51ae25_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}