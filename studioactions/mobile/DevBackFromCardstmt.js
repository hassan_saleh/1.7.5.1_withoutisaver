function DevBackFromCardstmt(eventobject) {
    return AS_Form_c5d81e379924459f9bfdfb96628d07b6(eventobject);
}

function AS_Form_c5d81e379924459f9bfdfb96628d07b6(eventobject) {
    if (gblDownloadPDFFlow) {
        if (frmCardStatementKA.flxAccountdetailsSortContainer.top === "8%") animatePopup_Sort();
        kony.application.getPreviousForm().show();
    } else {
        if (frmCardStatementKA.flxCardStatement.isVisible === false) {
            if (frmCardStatementKA.flxCardsSegmentOption.top === "8%") animate_cardsFilterOption();
            frmCardStatementKA.flxCardStatement.isVisible = true;
            frmCardStatementKA.flxCardsFilterScreen.isVisible = false;
            frmCardStatementKA.flxFilterDone.isVisible = false;
        } else {
            if (frmCardStatementKA.flxAccountdetailsSortContainer.top === "8%") animatePopup_Sort();
            kony.application.getPreviousForm().show();
        }
    }
}