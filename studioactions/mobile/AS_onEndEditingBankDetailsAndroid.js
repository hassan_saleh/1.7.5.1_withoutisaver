function AS_onEndEditingBankDetailsAndroid(eventobject, changedtext) {
    return AS_TextField_g4c215638b874c7688961ea25b5765d0(eventobject, changedtext);
}

function AS_TextField_g4c215638b874c7688961ea25b5765d0(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxBeneBankDetails.text !== "") {
        frmAddExternalAccountKA.flxUnderlineBankDetails.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineBankDetails.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblBeneBankDetails", frmAddExternalAccountKA.tbxBeneBankDetails.text);
}