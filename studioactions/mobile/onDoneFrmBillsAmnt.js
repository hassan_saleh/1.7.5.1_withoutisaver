function onDoneFrmBillsAmnt(eventobject, changedtext) {
    return AS_TextField_h004f54d77c84f35b3e36d2809aa27f3(eventobject, changedtext);
}

function AS_TextField_h004f54d77c84f35b3e36d2809aa27f3(eventobject, changedtext) {
    frmBills.lblHiddenAmnt.text = frmBills.tbxAmount.text;
    var amount = frmBills.tbxAmount.text;
    if (amount === null || amount === "" || amount == "." || (Number(amount) < 0.01) || amount === 0.00) {} else {
        if (kony.store.getItem("BillPayfromAcc") !== null && kony.store.getItem("BillPayfromAcc") !== undefined) {
            var currency = "";
            if (frmBills.btnBillsPayAccounts.text === "t") {
                currency = kony.store.getItem("BillPayfromAcc").currencyCode;
                /* hassan */
                gblQuickFlow = "postBillAcc";
                serv_BILLSCOMISSIONCHARGE(frmBills.tbxAmount.text.replace(/,/g, ""));
                /* hassan */
            } else currency = kony.store.getItem("BillPayfromAcc").balance.split(" ")[1];
            frmBills.tbxAmount.text = frmBills.tbxAmount.text; //setDecimal
        }
    }
    if (frmBills.btnBillsPayCards.text === "t") //hassan JIRA 2141
        frmBills.flxConversionAmt.setVisibility(false); //hassan JIRA 2141
}