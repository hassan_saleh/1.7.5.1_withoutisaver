function AS_onEndEditingAddress1iOS(eventobject, changedtext) {
    return AS_TextField_if2730e9888e4ae8b449b211f4c46bc1(eventobject, changedtext);
}

function AS_TextField_if2730e9888e4ae8b449b211f4c46bc1(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxAddress1.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAddress1.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAddress1.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAddress1", frmAddExternalAccountKA.tbxAddress1.text);
}