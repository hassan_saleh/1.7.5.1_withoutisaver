function AS_Button_h91dc950274848ec9a9a6123f5eef02e(eventobject) {
    var currForm = kony.application.getCurrentForm().id;
    if (currForm !== "frmAccountsLandingKA") {
        if (kony.sdk.isNetworkAvailable()) {
            frmAccountsLandingKA.flxDeals.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
            frmAccountsLandingKA.flxAccountsMain.left = "0%"
            frmAccountsLandingKA.flxLoans.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
            frmAccountsLandingKA.flxDeals.zIndex = 1;
            frmAccountsLandingKA.flxLoans.zIndex = 1;
            frmAccountsLandingKA.forceLayout();
            /*var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                  var controller = INSTANCE.getFormController("frmAccountsLandingKA");
                  var navObject = new kony.sdk.mvvm.NavigationObject();
                  navObject.setRequestOptions("segAccountsKA",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
                  controller.loadDataAndShowForm(navObject);*/
            accountDashboardDataCall();
            kony.print("Nav accounts Dadhboard:::");
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.print("same form:::");
    }
}