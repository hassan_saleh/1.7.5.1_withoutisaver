function AS_TextField_NationalIDRegistration_NationalID_onTextChange(eventobject, changedtext) {
    return AS_TextField_gf77d5b26c0045e48a0245fd4238c090(eventobject, changedtext);
}

function AS_TextField_gf77d5b26c0045e48a0245fd4238c090(eventobject, changedtext) {
    if (!/^[a-zA-Z0-9\s]+$/.test(frmRegisterUser.txtNationalID.text)) frmRegisterUser.txtNationalID.text = frmRegisterUser.txtNationalID.text.substring(0, frmRegisterUser.txtNationalID.text.length - 1);
    //   CheckNatIDFieldsVal(frmRegisterUser.txtNationalID.text,frmRegisterUser.flxBorderNationalID.skin);
    if (frmRegisterUser.txtNationalID.text === null || frmRegisterUser.txtNationalID.text === "" || frmRegisterUser.txtNationalID.text.trim() === "") frmRegisterUser.flxBorderNationalID.skin = "skntextFieldDividerOrange";
    else frmRegisterUser.flxBorderNationalID.skin = "skntextFieldDividerGreen";
}