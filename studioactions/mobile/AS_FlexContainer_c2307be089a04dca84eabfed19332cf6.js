function AS_FlexContainer_c2307be089a04dca84eabfed19332cf6(eventobject, x, y) {
    //frmNewTransferKA.txtBox.text=formatAmountwithcomma(frmNewTransferKA.txtBox.text,3);
    var amount = frmNewTransferKA.txtBox.text;
    if (amount === null || amount === "" || amount == "." || (Number(amount) < 0.01) || amount === 0.00) {
        kony.print("Amount is either null or empty");
    } else {
        frmNewTransferKA.lblHiddenAmount.text = frmNewTransferKA.txtBox.text;
        frmNewTransferKA.txtBox.text = formatAmountwithcomma(amount, frmNewTransferKA.lblDecimal.text);
    }
    gotoAccountDetailsScreen(2);
}