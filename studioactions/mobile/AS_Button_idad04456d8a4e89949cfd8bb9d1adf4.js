function AS_Button_idad04456d8a4e89949cfd8bb9d1adf4(eventobject) {
    customAlertPopup(geti18Value("i18n.common.AreYouSure"), "Cancel and Return to the Dashboard payment screen", onClickYesBackIPS, popupCommonAlertDimiss, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
}