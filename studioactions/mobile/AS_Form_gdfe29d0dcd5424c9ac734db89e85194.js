function AS_Form_gdfe29d0dcd5424c9ac734db89e85194(eventobject) {
    frmCheckingJoint.btnFeaturesKA.skin = "skntabSelected";
    frmCheckingJoint.btnChargesKA.skin = "sknbtnbgf9f9f9B1pxe2e2e2";
    frmCheckingJoint.btnInfoKA.skin = "sknbtnbgf9f9f9B1pxe2e2e2";
    frmCheckingJoint.tabDeselectedIndicator2KA.setVisibility(false);
    frmCheckingJoint.tabDeselectedIndicator1KA.setVisibility(false);
    frmCheckingJoint.tabSelectedIndicator.setVisibility(true);
    frmCheckingJoint.RichTextInfoMain.setVisibility(true);
    frmCheckingJoint.RichTextInfo1.setVisibility(false);
    frmCheckingJoint.RichTextInfo2.setVisibility(false);
}