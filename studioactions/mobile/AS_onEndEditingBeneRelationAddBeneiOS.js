function AS_onEndEditingBeneRelationAddBeneiOS(eventobject, changedtext) {
    return AS_TextField_cfc96d1b4ce34e5a8dcf0af87490e395(eventobject, changedtext);
}

function AS_TextField_cfc96d1b4ce34e5a8dcf0af87490e395(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxBeneRelation.text !== "") {
        frmAddExternalAccountKA.flxUnderlineBeneRelation.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineBeneRelation.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblBeneRelation", frmAddExternalAccountKA.tbxBeneRelation.text);
}