function AS_Button_c42e3f7edb294409a22e7c07dbcf34b2(eventobject) {
    function SHOW_ALERT_ide_onClick_d9f029e0da4947d0b56a256614b492cb_True() {
        frmAccountsLandingKA.flxAccntLandingNavOptKA.isVisible = false;
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_ide_onClick_d9f029e0da4947d0b56a256614b492cb_False() {}

    function SHOW_ALERT_ide_onClick_d9f029e0da4947d0b56a256614b492cb_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_d9f029e0da4947d0b56a256614b492cb_True();
        } else {
            SHOW_ALERT_ide_onClick_d9f029e0da4947d0b56a256614b492cb_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_d9f029e0da4947d0b56a256614b492cb_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}