function onSlideATMPOSLimit(eventobject, selectedvalue) {
    return AS_Slider_ba6b7476148346be8b0e3ddb48374e76(eventobject, selectedvalue);
}

function AS_Slider_ba6b7476148346be8b0e3ddb48374e76(eventobject, selectedvalue) {
    if (eventobject.id === "sliderATMLimit") {
        onTextChange_ATMPOSLIMIT(frmATMPOSLimit.txtWithdrawalLimit, frmATMPOSLimit.sliderATMLimit, "onSlide", "ATM");
    } else if (eventobject.id === "sliderPOSLimit") {
        onTextChange_ATMPOSLIMIT(frmATMPOSLimit.txtPOSLimit, frmATMPOSLimit.sliderPOSLimit, "onSlide", "POS");
    }
}